**GDImage: Changes log**
========================


RC 2.0.9:
---------

- Add Image class to shortcut Image_Interface (yes, I'm lazy).
- Move pack cache management from AGif_Unpacker to Image_AGif.
- Move pack cache management from APng_Unpacker to Image_APng.
- Add MemoryHandler stuff.


RC 2.0.8:
---------

- Review MymeTypeGuesser_*.
- Add Transform_Blend_Image.
- Add Transform_Blend_Color.
- Review palette manipulation on Transform_Filter and Transform_Opacity.
- Add Transform_Colorize.
- Rename Transform_FitNFill->setColor() to Transform_FitNFill->setBackground().
- Add Transform_Rotate.
- Update Resource_TrueColor->toPaletteColor() to minimize color palette.
- Uniformize *GDResource usage to *GdResource.
- Add Resource_Abstract->enable/disable() methods as aliases for Resource_Abstract->set/unsetMode().
- Resource_Abstract->set/unsetMode() accepts string as argument.
- Implements interpolation methods and setting on Resource_Abstract.


RC 2.0.7:
---------

- Image_Interface->apply() accepts more than one Transform_Interface as arguments.
- Transform_Multiple->add() accepts more than one Transform_Interface as arguments.
- Review all Transform_* to accept parameters as arguments on constructor.
- Transform_FitNFill does not require a strict Color object.
- Disable alpha blending on Resource_PaletteColor->toTrueColor().
- Add Transform_Opacity.


RC 2.0.6:
---------

- Review Transform_Convolution_Negate to use imagefilter().
- AGif_Factory gets classes to use from configuration.
- AGif_Framer gets frame interface to use from configuration.
- APng_Factory gets classes to use from configuration.
- APng_Framer gets frame interface to use from configuration.
- Factory gets image interfaces, import morphers and export drivers from configuration.
- Review Transform_Filter implementation on palette color.
- Add Transform_Posterize.
- Do not fill with opaque background color anymore on Resource_PaletteColor->toTrueColor().


RC 2.0.5:
---------

- gd_image() implementation.
- "mbstring.func_overload" setting does not trigger warning anymore.


RC 2.0.4:
---------

- Rename all "a_png_default_*" settings to "a_png_*".
- Rename all "a_gif_default_*" settings to "a_gif_*".
- Downgrade default PNG compression level to 4.
- Review generated file name on test cases.
- Add Transform_Flip and Transform_Mirror.
- Optimize AGIF and APNG palette downgrade.
- Rename Resource_Abstract->copyGdResource() to Resource_Abstract->cloneGdResource().
- Add palette copy argument on Resource_PaletteColor->blank().


RC 2.0.3:
---------

- Remove stupid contribution rules.
- Review Transform_Collection implementation, see README.
- Add Transform class to shortcut Transform_Collection.
- Review AGIF optimization.
- Rename "a_gif_default_delay" setting to "a_gif_default_delay_time".
- Rename "a_gif_default_disposal" setting to "a_gif_default_disposal_method".
- Add Image_AGif_Frame->set/getDelayTime() methods.
- Add Image_AGif_Frame->set/getDisposalMethod() methods.
- Add reset() and toImage() method on Image_Interface, review all Image_* classes.
- Add automatic filters ability on Image_Png.
- Review "truecolor" and "palettecolor" parameters implementation on Factory->exportation().
- Add Resource class to shortcut Resource_Abstract (yes, I'm lazy).
- Remove AGif_Composer->_doFramePackHeader() in favor of AGif_Composer->_doPackHeader().
- Rename AGif_Composer->_doPackExtension() to AGif_Composer->_doPack().
- Review AGif_Composer optimization implementation.
- Rename PNG quality parameter to PNG compression to avoid any misunderstood.  
  (see http://php.net/manual/en/function.imagepng.php#106093)
- APNG implementation.


RC 2.0.2:
---------

- Review fromImage() method on Image_Abstract_Single.
- Enable transparent color on Resource_TrueColor::toPaletteColor() method.
- Review Transform_Resize_Abstract method names.
- Simplify transparent restoration on Transform_Convolution_Basic.
- Simplify transparent restoration on Transform_Resize_Abstract.
- Add getBackground() method on Resource_PaletteColor and Resource_TrueColor.
- Review Image_Abstract_Multiple methods name, change "image" to "frame".
- Rename Image_Abstract_SingleResource class to Image_Abstract_Single.
- Rename Image_Abstract_MultipleResource class to Image_Abstract_Multiple.
- Resource_*Color classes to use come from configuration.
- AGIF implementation.


RC 2.0.1:
---------

- Initial state.