<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

$code0 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
        .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath , \'image/png\' ); // will result to PNG24 or PNG8, depending of the resource type';

$code1 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
        .'$final_filepath = \\GDImage\\Factory::export( $image , array('."\n"
        .'  \'driver\' => \'File\' ,'."\n"
        .'  \'file\' => $export_filepath ,'."\n"
        .'  \'truecolor\' => true ,'."\n"
        .') , \'image/png\' ); // will result to PNG24, whatever the resource type is';

$code2 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
        .'$final_filepath = \\GDImage\\Factory::export( $image , array('."\n"
        .'  \'driver\' => \'File\' ,'."\n"
        .'  \'file\' => $export_filepath ,'."\n"
        .'  \'palettecolor\' => true ,'."\n"
        .') , \'image/png\' ); // will result to PNG8, whatever the resource type is';

?>
<html>
<head>
  <title>GDImage: Test 00015 - PNG8 vs PNG24 convertion</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00015 - PNG8 vs PNG24 convertion</h1>
  
  <p><i>
    When exporting to PNG, the library will always preserve the resource color type.<br />
    If the source is a true color resource, the export will result to a PNG24 image and if the source is a palette color resource, the export will result to a PNG8 image.<br />
    If you want to be sure that the export will result to a PNG24 - or a PNG8 - the resource must be converted to true color - respectively to palette color.<br />
  </i></p>
  
  <pre><?php echo htmlspecialchars( $code0 ); ?></pre>
  
  <pre><?php echo htmlspecialchars( $code1 ); ?></pre>
  
  <pre><?php echo htmlspecialchars( $code2 ); ?></pre>
  
  <hr />
  
<?php
  // Test case 1

  $import_filepath = SAMP_DIR.'tulips.256c.gif';
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  $export_filepath = array(
    'png' => TMP_DIR.'00015_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.png' , 
    'png8' => TMP_DIR.'00015_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.8.png' , 
    'png24' => TMP_DIR.'00015_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.24.png' , 
  );
  
  // to PNG
  \GDImage\Factory::export( $image , array( 'driver' => 'File' , 'file' => $export_filepath['png'] ) );
  
  // to PNG8
  \GDImage\Factory::export( $image , array( 'driver' => 'File' , 'file' => $export_filepath['png8'] , 'palettecolor' => true ) );
  
  // to PNG24
  \GDImage\Factory::export( $image , array( 'driver' => 'File' , 'file' => $export_filepath['png24'] , 'truecolor' => true ) );
  
?>
  <h2>Test case 1: GIF</h2>
  
  <table>
    <tr>
      <td rowspan="2" colspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)</figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png']); ?>" alt="<?php echo basename($export_filepath['png']); ?>" title="<?php echo basename($export_filepath['png']); ?>" />
          <figcaption>Default PNG conversion (<?php echo filesize_to_human( $export_filepath['png'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png8']); ?>" alt="<?php echo basename($export_filepath['png8']); ?>" title="<?php echo basename($export_filepath['png8']); ?>" />
          <figcaption>PNG8 conversion (<?php echo filesize_to_human( $export_filepath['png8'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png24']); ?>" alt="<?php echo basename($export_filepath['png24']); ?>" title="<?php echo basename($export_filepath['png24']); ?>" />
          <figcaption>PNG24 conversion (<?php echo filesize_to_human( $export_filepath['png24'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  
  <hr />
  
<?php
  // Test case 2

  $import_filepath = SAMP_DIR.'hydrangeas.128c.8.png';
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  $export_filepath = array(
    'png' => TMP_DIR.'00015_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.png' , 
    'png8' => TMP_DIR.'00015_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.8.png' , 
    'png24' => TMP_DIR.'00015_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.24.png' , 
  );
  
  // to PNG
  \GDImage\Factory::export( $image , array( 'driver' => 'File' , 'file' => $export_filepath['png'] ) );
  
  // to PNG8
  \GDImage\Factory::export( $image , array( 'driver' => 'File' , 'file' => $export_filepath['png8'] , 'palettecolor' => true ) );
  
  // to PNG24
  \GDImage\Factory::export( $image , array( 'driver' => 'File' , 'file' => $export_filepath['png24'] , 'truecolor' => true ) );
  
?>
  <h2>Test case 2: PNG8</h2>
  
  <table>
    <tr>
      <td rowspan="2" colspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)</figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png']); ?>" alt="<?php echo basename($export_filepath['png']); ?>" title="<?php echo basename($export_filepath['png']); ?>" />
          <figcaption>Default PNG conversion (<?php echo filesize_to_human( $export_filepath['png'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png8']); ?>" alt="<?php echo basename($export_filepath['png8']); ?>" title="<?php echo basename($export_filepath['png8']); ?>" />
          <figcaption>PNG8 conversion (<?php echo filesize_to_human( $export_filepath['png8'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png24']); ?>" alt="<?php echo basename($export_filepath['png24']); ?>" title="<?php echo basename($export_filepath['png24']); ?>" />
          <figcaption>PNG24 conversion (<?php echo filesize_to_human( $export_filepath['png24'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  
  <hr />
  
<?php
  // Test case 3

  $import_filepath = SAMP_DIR.'lighthouse.24.png';
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  $export_filepath = array(
    'png' => TMP_DIR.'00015_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.png' , 
    'png8' => TMP_DIR.'00015_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.8.png' , 
    'png24' => TMP_DIR.'00015_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.24.png' , 
  );
  
  // to PNG
  \GDImage\Factory::export( $image , array( 'driver' => 'File' , 'file' => $export_filepath['png'] ) );
  
  // to PNG8
  \GDImage\Factory::export( $image , array( 'driver' => 'File' , 'file' => $export_filepath['png8'] , 'palettecolor' => true ) );
  
  // to PNG24
  \GDImage\Factory::export( $image , array( 'driver' => 'File' , 'file' => $export_filepath['png24'] , 'truecolor' => true ) );
  
?>
  <h2>Test case 3: PNG24</h2>
  
  <table>
    <tr>
      <td rowspan="2" colspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)</figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png']); ?>" alt="<?php echo basename($export_filepath['png']); ?>" title="<?php echo basename($export_filepath['png']); ?>" />
          <figcaption>Default PNG conversion (<?php echo filesize_to_human( $export_filepath['png'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png8']); ?>" alt="<?php echo basename($export_filepath['png8']); ?>" title="<?php echo basename($export_filepath['png8']); ?>" />
          <figcaption>PNG8 conversion (<?php echo filesize_to_human( $export_filepath['png8'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png24']); ?>" alt="<?php echo basename($export_filepath['png24']); ?>" title="<?php echo basename($export_filepath['png24']); ?>" />
          <figcaption>PNG24 conversion (<?php echo filesize_to_human( $export_filepath['png24'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
  </table>  
  
  <hr />
  
<?php
  // Test case 4

  $import_filepath = SAMP_DIR.'chrysanthemum.jpg';
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  $export_filepath = array(
    'png' => TMP_DIR.'00015_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.png' , 
    'png8' => TMP_DIR.'00015_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.8.png' , 
    'png24' => TMP_DIR.'00015_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.24.png' , 
  );
  
  // to PNG
  \GDImage\Factory::export( $image , array( 'driver' => 'File' , 'file' => $export_filepath['png'] ) );
  
  
  // to PNG8
  \GDImage\Factory::export( $image , array( 'driver' => 'File' , 'file' => $export_filepath['png8'] , 'palettecolor' => true ) );
  
  // to PNG24
  \GDImage\Factory::export( $image , array( 'driver' => 'File' , 'file' => $export_filepath['png24'] , 'truecolor' => true ) );
  
?>
  <h2>Test case 4: JPEG</h2>
  
  <table>
    <tr>
      <td rowspan="2" colspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)</figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png']); ?>" alt="<?php echo basename($export_filepath['png']); ?>" title="<?php echo basename($export_filepath['png']); ?>" />
          <figcaption>Default PNG conversion (<?php echo filesize_to_human( $export_filepath['png'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png8']); ?>" alt="<?php echo basename($export_filepath['png8']); ?>" title="<?php echo basename($export_filepath['png8']); ?>" />
          <figcaption>PNG8 conversion (<?php echo filesize_to_human( $export_filepath['png8'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png24']); ?>" alt="<?php echo basename($export_filepath['png24']); ?>" title="<?php echo basename($export_filepath['png24']); ?>" />
          <figcaption>PNG24 conversion (<?php echo filesize_to_human( $export_filepath['png24'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  
  <hr />
  
<?php require '_foot.partial.php'; ?>
  
</body>
</html>
        
