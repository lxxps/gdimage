<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

function colour_type_to_human( $i )
{
  switch( $i )
  {
    case 0: return 'greyscale';
    case 2: return 'truecolor';
    case 3: return 'indexed-color';
    case 4: return 'greyscale with alpha';
    case 6: return 'truecolor with alpha';
  }
}

?>
<html>
<head>
  <title>GDImage: Test 00315 - APNG Optimization</title>
  <?php require '_head.partial.php'; ?>
  <style>
    small { white-space: nowrap; }
  </style>
</head>
<body>
  
  <h1>GDImage: Test 00315 - APNG Optimization</h1>
  
  <p><i>
    For APNG composition, an important setting has to be introduced: APNG Composer Optimization.<br />
    This setting is a bitwise of these values:<br />
    <br />
    - <code>\GDImage\APng_Composer::OPTIMIZE_TRANSPARENT_TO_CROP</code> (1):<br />
    remove transparent pixels on frame borders to reduce frame size, this optimization is not applicable if the blend mode is 0 (no blend) and the previous frame dispose option is 0 (do not dispose) or 2 (restore to previous);<br />
    - <code>\GDImage\APng_Composer::OPTIMIZE_DISPOSAL_TO_CROP</code> (2):<br />
    remove redundant pixels &mdash; from disposed frame to current one &mdash; on frame borders to reduce frame size, this optimization is applicable if the previous frame dispose option is 0 (do not dispose) or 2 (restore to previous);<br />
    - <code>\GDImage\APng_Composer::OPTIMIZE_DISPOSAL_TO_TRANSPARENT</code> (4):<br />
    replace redundant pixels &mdash; from disposed frame to current one &mdash; of the frame by transparent color to reduce frame colors, this optimization is applicable if the blend option is 1 (blend over) and the previous frame dispose option is 0 (do not dispose) or 2 (restore to previous).<br />
    <br />
    These optimizations will reduce the file size but if there is a lot of large frames, the optimization will take ages: we have to analyse each pixels of each frames.<br />
    Using this setting really depends on what you expect from APNG generation: slow and efficient or fast and furious&hellip;<br />
    We advice to enable optimization when APNG are generated from a cron job, and disable it when APNG are generated on demand.<br />
    By default, this setting is set to <code>0</code>, so there is no optimization.<br />
  </i></p>
  
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'apng/preloaders_net_3.png';
  $export_filepath1 = TMP_DIR.'00315_preloaders_net_3aa.png';
  $export_filepath2 = TMP_DIR.'00315_preloaders_net_3ab.png';
  $export_filepath3 = TMP_DIR.'00315_preloaders_net_3ac.png';
  $export_filepath4 = TMP_DIR.'00315_preloaders_net_3ad.png';
  $export_filepath5 = TMP_DIR.'00315_preloaders_net_3ae.png';
  $export_filepath6 = TMP_DIR.'00315_preloaders_net_3af.png';
  $export_filepath7 = TMP_DIR.'00315_preloaders_net_3ag.png';
  $export_filepath8 = TMP_DIR.'00315_preloaders_net_3ah.png';
  
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( \PHP_INT_MAX );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath1 );
  $export_time1 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 0 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath2 );
  $export_time2 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 1 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath3 );
  $export_time3 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 2 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath4 );
  $export_time4 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 4 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath5 );
  $export_time5 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 3 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath6 );
  $export_time6 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 5 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath7 );
  $export_time7 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 6 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath8 );
  $export_time8 = microtime( true ) - $start;
  
  $code = '\\GDImage\\Config::setAPngComposerOptimization( $optimize );'."\n"
         ."\n"
         .'$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'."\n";
  
?>
  <h2>Test case 1: Optimization and Blend Option to 0 (no blend)</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    You will notice that some frames require to overwrote some pixels with a transparent color, this is achieved helping Blend Option to 0 (no blend).<br />
    With this Blend Option, optimization bits <code>1</code> (transparent to crop) and <code>4</code> (disposed to transparent) are not applicable.<br />
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://preloaders.net/" target="_blank">preloaders.net</a>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath1); ?>" alt="<?php echo basename($export_filepath1); ?>" title="<?php echo basename($export_filepath1); ?>" />
          <figcaption>
            All optimizations <i>7</i> (<?php echo filesize_to_human( $export_filepath1 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time1 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath2); ?>" alt="<?php echo basename($export_filepath2); ?>" title="<?php echo basename($export_filepath2); ?>" />
          <figcaption>
            No optimization <i>0</i> (<?php echo filesize_to_human( $export_filepath2 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time2 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath3); ?>" alt="<?php echo basename($export_filepath3); ?>" title="<?php echo basename($export_filepath3); ?>" />
          <figcaption>
            Optimization value <i>1</i> (<?php echo filesize_to_human( $export_filepath3 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time3 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath4); ?>" alt="<?php echo basename($export_filepath4); ?>" title="<?php echo basename($export_filepath4); ?>" />
          <figcaption>
            Optimization value <i>2</i> (<?php echo filesize_to_human( $export_filepath4 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time4 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath5); ?>" alt="<?php echo basename($export_filepath5); ?>" title="<?php echo basename($export_filepath5); ?>" />
          <figcaption>
            Optimization value <i>4</i> (<?php echo filesize_to_human( $export_filepath5 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time5 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath6); ?>" alt="<?php echo basename($export_filepath6); ?>" title="<?php echo basename($export_filepath6); ?>" />
          <figcaption>
            Optimization value <i>3</i> (<?php echo filesize_to_human( $export_filepath6 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time6 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath8); ?>" alt="<?php echo basename($export_filepath8); ?>" title="<?php echo basename($export_filepath8); ?>" />
          <figcaption>
            Optimization value <i>6</i> (<?php echo filesize_to_human( $export_filepath8 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time8 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath7); ?>" alt="<?php echo basename($export_filepath7); ?>" title="<?php echo basename($export_filepath7); ?>" />
          <figcaption>
            Optimization value <i>5</i> (<?php echo filesize_to_human( $export_filepath7 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time7 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'apng/preloaders_net_5.png';
  $export_filepath1 = TMP_DIR.'00315_preloaders_net_5_ba.png';
  $export_filepath2 = TMP_DIR.'00315_preloaders_net_5_bb.png';
  $export_filepath3 = TMP_DIR.'00315_preloaders_net_5_bc.png';
  $export_filepath4 = TMP_DIR.'00315_preloaders_net_5_bd.png';
  $export_filepath5 = TMP_DIR.'00315_preloaders_net_5_be.png';
  $export_filepath6 = TMP_DIR.'00315_preloaders_net_5_bf.png';
  $export_filepath7 = TMP_DIR.'00315_preloaders_net_5_bg.png';
  $export_filepath8 = TMP_DIR.'00315_preloaders_net_5_bh.png';
  
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( \PHP_INT_MAX );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  foreach( $image->getFrames() as $frame ) $frame->setBlendOption( 1 );
  \GDImage\Factory::export( $image , $export_filepath1 );
  $export_time1 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 0 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  foreach( $image->getFrames() as $frame ) $frame->setBlendOption( 1 );
  \GDImage\Factory::export( $image , $export_filepath2 );
  $export_time2 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 1 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  foreach( $image->getFrames() as $frame ) $frame->setBlendOption( 1 );
  \GDImage\Factory::export( $image , $export_filepath3 );
  $export_time3 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 2 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  foreach( $image->getFrames() as $frame ) $frame->setBlendOption( 1 );
  \GDImage\Factory::export( $image , $export_filepath4 );
  $export_time4 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 4 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  foreach( $image->getFrames() as $frame ) $frame->setBlendOption( 1 );
  \GDImage\Factory::export( $image , $export_filepath5 );
  $export_time5 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 3 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  foreach( $image->getFrames() as $frame ) $frame->setBlendOption( 1 );
  \GDImage\Factory::export( $image , $export_filepath6 );
  $export_time6 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 5 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  foreach( $image->getFrames() as $frame ) $frame->setBlendOption( 1 );
  \GDImage\Factory::export( $image , $export_filepath7 );
  $export_time7 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 6 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  foreach( $image->getFrames() as $frame ) $frame->setBlendOption( 1 );
  \GDImage\Factory::export( $image , $export_filepath8 );
  $export_time8 = microtime( true ) - $start;
  
  $code = '\\GDImage\\Config::setAPngComposerOptimization( $optimize );'."\n"
         ."\n"
         .'$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'foreach( $image->getFrames() as $frame ) $frame->setBlendOption( 1 ); // enable blend'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'."\n";
  
?>
  <h2>Test case 2: Optimization and Blend Option to 1 (blend over)</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    You will notice that usage of optimization bits <code>1</code> (transparent to crop) and <code>4</code> (disposed to transparent) make the generation longer.<br />
    This is due to the optimization process that replace every redudant pixels by transparent color (<code>4</code>), then, remove transparent pixels at the border of each frames (<code>1</code>).<br />
    So, it is not a good practice to use optimization bit <code>4</code> (disposed to transparent) without optimization bit <code>2</code> (disposed to crop).
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://preloaders.net/" target="_blank">preloaders.net</a>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath1); ?>" alt="<?php echo basename($export_filepath1); ?>" title="<?php echo basename($export_filepath1); ?>" />
          <figcaption>
            All optimizations <i>7</i> (<?php echo filesize_to_human( $export_filepath1 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time1 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath2); ?>" alt="<?php echo basename($export_filepath2); ?>" title="<?php echo basename($export_filepath2); ?>" />
          <figcaption>
            No optimization <i>0</i> (<?php echo filesize_to_human( $export_filepath2 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time2 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath3); ?>" alt="<?php echo basename($export_filepath3); ?>" title="<?php echo basename($export_filepath3); ?>" />
          <figcaption>
            Optimization value <i>1</i> (<?php echo filesize_to_human( $export_filepath3 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time3 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath4); ?>" alt="<?php echo basename($export_filepath4); ?>" title="<?php echo basename($export_filepath4); ?>" />
          <figcaption>
            Optimization value <i>2</i> (<?php echo filesize_to_human( $export_filepath4 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time4 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath5); ?>" alt="<?php echo basename($export_filepath5); ?>" title="<?php echo basename($export_filepath5); ?>" />
          <figcaption>
            Optimization value <i>4</i> (<?php echo filesize_to_human( $export_filepath5 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time5 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath6); ?>" alt="<?php echo basename($export_filepath6); ?>" title="<?php echo basename($export_filepath6); ?>" />
          <figcaption>
            Optimization value <i>3</i> (<?php echo filesize_to_human( $export_filepath6 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time6 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath8); ?>" alt="<?php echo basename($export_filepath8); ?>" title="<?php echo basename($export_filepath8); ?>" />
          <figcaption>
            Optimization value <i>6</i> (<?php echo filesize_to_human( $export_filepath8 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time8 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath7); ?>" alt="<?php echo basename($export_filepath7); ?>" title="<?php echo basename($export_filepath7); ?>" />
          <figcaption>
            Optimization value <i>5</i> (<?php echo filesize_to_human( $export_filepath7 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time7 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <p><i>
    For this test case, we force the Blend Option to 1 (blend over), but it is not a good practice to do that, especially on true color APNG.<br />
    See test case below&hellip;<br />
  </i></p>
  
  <hr />
  
  
<?php /**/
  // Test case 3

  $import_filepath = SAMP_DIR.'apng/preloaders_net_4.png';
  $export_filepath1 = TMP_DIR.'00315_preloaders_net_4_ba.png';
  $export_filepath2 = TMP_DIR.'00315_preloaders_net_4_bb.png';
  $export_filepath3 = TMP_DIR.'00315_preloaders_net_4_bc.png';
  $export_filepath4 = TMP_DIR.'00315_preloaders_net_4_bd.png';
  $export_filepath5 = TMP_DIR.'00315_preloaders_net_4_be.png';
  $export_filepath6 = TMP_DIR.'00315_preloaders_net_4_bf.png';
  $export_filepath7 = TMP_DIR.'00315_preloaders_net_4_bg.png';
  $export_filepath8 = TMP_DIR.'00315_preloaders_net_4_bh.png';
  
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( \PHP_INT_MAX );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  foreach( $image->getFrames() as $frame ) $frame->setBlendOption( 1 );
  \GDImage\Factory::export( $image , $export_filepath1 );
  $export_time1 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 0 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  foreach( $image->getFrames() as $frame ) $frame->setBlendOption( 1 );
  \GDImage\Factory::export( $image , $export_filepath2 );
  $export_time2 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 1 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  foreach( $image->getFrames() as $frame ) $frame->setBlendOption( 1 );
  \GDImage\Factory::export( $image , $export_filepath3 );
  $export_time3 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 2 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  foreach( $image->getFrames() as $frame ) $frame->setBlendOption( 1 );
  \GDImage\Factory::export( $image , $export_filepath4 );
  $export_time4 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 4 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  foreach( $image->getFrames() as $frame ) $frame->setBlendOption( 1 );
  \GDImage\Factory::export( $image , $export_filepath5 );
  $export_time5 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 3 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  foreach( $image->getFrames() as $frame ) $frame->setBlendOption( 1 );
  \GDImage\Factory::export( $image , $export_filepath6 );
  $export_time6 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 5 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  foreach( $image->getFrames() as $frame ) $frame->setBlendOption( 1 );
  \GDImage\Factory::export( $image , $export_filepath7 );
  $export_time7 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAPngComposerOptimization( 6 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  foreach( $image->getFrames() as $frame ) $frame->setBlendOption( 1 );
  \GDImage\Factory::export( $image , $export_filepath8 );
  $export_time8 = microtime( true ) - $start;
  
  $code = '\\GDImage\\Config::setAPngComposerOptimization( $optimize );'."\n"
         ."\n"
         .'$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'foreach( $image->getFrames() as $frame ) $frame->setBlendOption( 1 ); // enable blend'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'."\n";
  
?>
  <h2>Test case 3: Optimization failure</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    Even with an opaque background, optimization bit 4 (behind to transparent) is not efficient on every APNG.<br />
    In fact, on this true color picture, when pixels are replaced by the transparent color, the ZLIB compression results to something larger than if pixels were maintained.<br />
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://preloaders.net/" target="_blank">preloaders.net</a>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath1); ?>" alt="<?php echo basename($export_filepath1); ?>" title="<?php echo basename($export_filepath1); ?>" />
          <figcaption>
            All optimizations <i>7</i> (<?php echo filesize_to_human( $export_filepath1 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time1 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath2); ?>" alt="<?php echo basename($export_filepath2); ?>" title="<?php echo basename($export_filepath2); ?>" />
          <figcaption>
            No optimization <i>0</i> (<?php echo filesize_to_human( $export_filepath2 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time2 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath3); ?>" alt="<?php echo basename($export_filepath3); ?>" title="<?php echo basename($export_filepath3); ?>" />
          <figcaption>
            Optimization value <i>1</i> (<?php echo filesize_to_human( $export_filepath3 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time3 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath4); ?>" alt="<?php echo basename($export_filepath4); ?>" title="<?php echo basename($export_filepath4); ?>" />
          <figcaption>
            Optimization value <i>2</i> (<?php echo filesize_to_human( $export_filepath4 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time4 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath5); ?>" alt="<?php echo basename($export_filepath5); ?>" title="<?php echo basename($export_filepath5); ?>" />
          <figcaption>
            Optimization value <i>4</i> (<?php echo filesize_to_human( $export_filepath5 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time5 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath6); ?>" alt="<?php echo basename($export_filepath6); ?>" title="<?php echo basename($export_filepath6); ?>" />
          <figcaption>
            Optimization value <i>3</i> (<?php echo filesize_to_human( $export_filepath6 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time6 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath8); ?>" alt="<?php echo basename($export_filepath8); ?>" title="<?php echo basename($export_filepath8); ?>" />
          <figcaption>
            Optimization value <i>6</i> (<?php echo filesize_to_human( $export_filepath8 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time8 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath7); ?>" alt="<?php echo basename($export_filepath7); ?>" title="<?php echo basename($export_filepath7); ?>" />
          <figcaption>
            Optimization value <i>5</i> (<?php echo filesize_to_human( $export_filepath7 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time7 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
  <p><i>
    Keep in mind that this library has been created for hosting that does not have Imagick extension.<br />
    With Imagick, optimizing an APNG should be as simple as these few lines of codes:
  </i></p>
  
  <pre>$img = new Imagick($import_filepath);
$img->optimizeImageLayers();
$img->writeImages($export_filepath, true);</pre>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
