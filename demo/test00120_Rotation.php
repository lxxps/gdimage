<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

?>
<html>
<head>
  <title>GDImage: Test 00120 - Rotation</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00120 - Rotation</h1>
  
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'tulips.256c.gif';
  $export_filepaths = array(
    45 => TMP_DIR.'00120_tulips_45.gif' ,
//    90 => TMP_DIR.'00120_tulips_90.gif' ,
//    135 => TMP_DIR.'00120_tulips_135.gif' ,
//    180 => TMP_DIR.'00120_tulips_180.gif' ,
//    -135 => TMP_DIR.'00120_tulips_-135.gif' ,
//    -90 => TMP_DIR.'00120_tulips_-90.gif' ,
    -45 => TMP_DIR.'00120_tulips_-45.gif' ,
  );
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepaths as $degrees => $export_filepath )
  {
    $clone = clone $image;
    $clone->apply( new \GDImage\Transform_Rotate( $degrees ) );
    \GDImage\Factory::export( $clone , $export_filepath );
  }
  
  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$transform = new \\GDImage\\Transform_Rotate( $degrees );'."\n"
         .'$image->apply( $transform );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
?>
  <h2>Test case 1: Simple rotation on GIF</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>By default, we use transparent color to fill uncovered zone after rotation.</i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <?php foreach( $export_filepaths as $degrees => $export_filepath ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
            <figcaption><?php echo $degrees; ?>&deg;</figcaption>
          </figure>
        </td>
      <?php endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'chrysanthemum.jpg';
  $export_filepaths = array(
    45 => TMP_DIR.'00120_chrysanthemum_45.jpg' ,
//    90 => TMP_DIR.'00120_chrysanthemum_90.jpg' ,
//    135 => TMP_DIR.'00120_chrysanthemum_135.jpg' ,
//    180 => TMP_DIR.'00120_chrysanthemum_180.jpg' ,
//    -135 => TMP_DIR.'00120_chrysanthemum_-135.jpg' ,
//    -90 => TMP_DIR.'00120_chrysanthemum_-90.jpg' ,
    -45 => TMP_DIR.'00120_chrysanthemum_-45.jpg' ,
  );
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepaths as $degrees => $export_filepath )
  {
    $clone = clone $image;
    $clone->apply( new \GDImage\Transform_Rotate( $degrees ) );
    \GDImage\Factory::export( $clone , $export_filepath );
  }
  
  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$transform = new \\GDImage\\Transform_Rotate( $degrees );'."\n"
         .'$image->apply( $transform );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
?>
  <h2>Test case 2: Simple rotation on JPEG</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    You will notice that JPEG get the default color without alpha channel.<br />
    To change the default color, use <code>\GDImage\Config::setColorDefaultValue( $color_representation );</code>.
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <?php foreach( $export_filepaths as $degrees => $export_filepath ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
            <figcaption><?php echo $degrees; ?>&deg;</figcaption>
          </figure>
        </td>
      <?php endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
  
<?php /**/
  // Test case 3

  $import_filepath = SAMP_DIR.'desert.24.png';
  $export_filepaths = array(
    'jpg' => TMP_DIR.'00120_desert_45b.jpg' ,
    'png' => TMP_DIR.'00120_desert_45b.png' ,
    'gif' => TMP_DIR.'00120_desert_45b.gif' ,
  );
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepaths as $degrees => $export_filepath )
  {
    $clone = clone $image;
    $clone->apply( new \GDImage\Transform_Rotate( -45 , 'FFFFFF7F' ) );
    \GDImage\Factory::export( $clone , $export_filepath );
  }
  
  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$transform = new \\GDImage\\Transform_Rotate( $degrees , $color );'."\n"
         .'$image->apply( $transform );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
?>
  <h2>Test case 3: Background color on PNG24 picture</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    A custom color to use as background can be specified.<br />
    You will notice that it will be used as background for the entire picture and not only for uncovered zone.
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <?php foreach( $export_filepaths as $key => $export_filepath ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
            <figcaption>To <?php echo strtoupper( $key ); ?></figcaption>
          </figure>
        </td>
      <?php endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
  
<?php /**/
  // Test case 4

  $import_filepath = SAMP_DIR.'hydrangeas.128c.8.png';
  $export_filepaths = array(
    45 => TMP_DIR.'00120_hydrangeas_45.gif' ,
    90 => TMP_DIR.'00120_hydrangeas_90.gif' ,
    135 => TMP_DIR.'00120_hydrangeas_135.gif' ,
    180 => TMP_DIR.'00120_hydrangeas_180.gif' ,
    -135 => TMP_DIR.'00120_hydrangeas_-135.gif' ,
    -90 => TMP_DIR.'00120_hydrangeas_-90.gif' ,
    -45 => TMP_DIR.'00120_hydrangeas_-45.gif' ,
  );
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepaths as $degrees => $export_filepath )
  {
    $clone = clone $image;
    $clone->apply( new \GDImage\Transform_Rotate( $degrees , null , true ) );
    \GDImage\Factory::export( $clone , $export_filepath );
  }
  
  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$transform = new \\GDImage\\Transform_Rotate( $degrees , null , true );'."\n"
         .'$image->apply( $transform );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
?>
  <h2>Test case 4: Preserve edge after rotation</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <table>
    <tr>
      <td colspan="<?php echo count( $export_filepaths ); ?>">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <?php foreach( $export_filepaths as $key => $export_filepath ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
            <figcaption><?php echo strtoupper( $key ); ?>&deg;</figcaption>
          </figure>
        </td>
      <?php endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
