<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

?>
<html>
<head>
  <title>GDImage: Test 00080 - imageconvolution() leaks</title>
  <?php require '_head.partial.php'; ?>
  <style>
    img[width][height] {
      max-width: none;
      image-rendering: -moz-crisp-edges;         /* Firefox */
      image-rendering:   -o-crisp-edges;         /* Opera */
      image-rendering: -webkit-optimize-contrast;/* Webkit (non-standard naming) */
      image-rendering: crisp-edges;
      -ms-interpolation-mode: nearest-neighbor;  /* IE (non-standard property) */
    }
  </style>
</head>
<body>
  
  <h1>GDImage: Test 00080 - <a href="http://php.net/manual/en/function.imageconvolution.php" target="_blank">imageconvolution()</a> leaks</h1>
  
<?php 

$import_filepath = SAMP_DIR.'patterns/pattern.2c.24.png';
$testa_filepath = TMP_DIR.'00080_pattern_ta.png';
$testb_filepath = TMP_DIR.'00080_pattern_tb.png';

// import
$rsc = imagecreatefrompng( $import_filepath );
imagealphablending( $rsc , false );
// convolution
imageconvolution( $rsc , array(
  array( 0 , 0 , 0 ) ,
  array( 0 , 1 , 0 ) ,
  array( 0 , 0 , 0 ) ,
) , 1 , 0 );
// export
imagesavealpha( $rsc , true );
imagepng( $rsc , $testa_filepath , 9 );
// destroy
imagedestroy( $rsc );

// import
$rsc = imagecreatefrompng( $import_filepath );
imagealphablending( $rsc , false );
// convolution
imageconvolution( $rsc , array(
  array( 0 , 0 , 0 ) ,
  array( 0 , 0 , 1 ) ,
  array( 0 , 0 , 0 ) ,
) , 1 , 0 );
// export
imagesavealpha( $rsc , true );
imagepng( $rsc , $testb_filepath , 9 );
// destroy
imagedestroy( $rsc );

?>
  
  <p><i>
    On this tiny PNG24 picture, you can see how <a href="http://php.net/manual/en/function.imageconvolution.php" target="_blank">imageconvolution()</a> is bad with alpha channel.<br />
  </i></p>
  
  <table>
    <tr>
      <td rowspan="3">
        <figure>
          <img width="120" height="120" src="./samples/patterns/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td>
        <pre>imageconvolution( $rsc , array(
  array( 0 , 0 , 0 ) ,
  array( 0 , 1 , 0 ) ,
  array( 0 , 0 , 0 ) ,
) , 1 , 0 );</pre>
      </td>
      <td>
        <pre>imageconvolution( $rsc , array(
  array( 0 , 0 , 0 ) ,
  array( 0 , 0 , 1 ) ,
  array( 0 , 0 , 0 ) ,
) , 1 , 0 );</pre>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($testa_filepath); ?>" alt="<?php echo basename($testa_filepath); ?>" title="<?php echo basename($testa_filepath); ?>" />
          <figcaption>
            Result<br />
            <i>nothing should change</i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($testb_filepath); ?>" alt="<?php echo basename($testa_filepath); ?>" title="<?php echo basename($testa_filepath); ?>" />
          <figcaption>
            Result<br />
            <i>consider border extension</i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img width="120" height="120" src="./samples/patterns/pattern_expected_case1.2c.24.png" alt="<?php echo basename($testa_filepath); ?>" title="<?php echo basename($testa_filepath); ?>" />
          <figcaption>Expected</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img width="120" height="120" src="./samples/patterns/pattern_expected_case2.2c.24.png" alt="<?php echo basename($testa_filepath); ?>" title="<?php echo basename($testa_filepath); ?>" />
          <figcaption>Expected</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  <p><i>
    Unfortunately, you can try a baunch of things - just like me - you will never get good results with <a href="http://php.net/manual/en/function.imageconvolution.php" target="_blank">imageconvolution()</a>.<br />
    Hopefully, this kind of picture does not represent real life pictures and in most cases <a href="http://php.net/manual/en/function.imageconvolution.php" target="_blank">imageconvolution()</a> does the job… <b>approximatively</b>!<br />
    Just take a look of this few pictures.<br />
  </i></p>
  
<?php 

$import_filepath = SAMP_DIR.'desert.24.png';
$testa_filepath = TMP_DIR.'00080_desert_ta.png';
$testb_filepath = TMP_DIR.'00080_desert_tb.png';

// import
$rsc = imagecreatefrompng( $import_filepath );
// convolution
imagealphablending( $rsc , false );
imageconvolution( $rsc , array(
  array( 1 , 2 , 1 ) ,
  array( 2 , 4 , 2 ) ,
  array( 1 , 2 , 1 ) ,
) , 15 , 0 );
// export
imagesavealpha( $rsc , true );
imagepng( $rsc , $testa_filepath , 9 );
// destroy
imagedestroy( $rsc );

// import
$rsc = imagecreatefrompng( $import_filepath );
// convolution
imagealphablending( $rsc , false );
imageconvolution( $rsc , array(
  array( -1 , -1 , -1 ) ,
  array( -1 , 9 , -1 ) ,
  array( -1 , -1 , -1 ) ,
) , -1 , 255 );
// export
imagesavealpha( $rsc , true );
imagepng( $rsc , $testb_filepath , 9 );
// destroy
imagedestroy( $rsc );

?>
  
  <h2>Test case 1: PNG24</h2>
  
  <p><i>
    Note the short dirty line on left and the dirty top-left pixel.<br />  
    The thin black line along the transparent area comes from wrong alpha calculation.<br />
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($testa_filepath); ?>" alt="<?php echo basename($testa_filepath); ?>" title="<?php echo basename($testa_filepath); ?>" />
          <figcaption>Gaussian blur</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($testb_filepath); ?>" alt="<?php echo basename($testb_filepath); ?>" title="<?php echo basename($testb_filepath); ?>" />
          <figcaption>Sharpen and negate</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php 

$import_filepath = SAMP_DIR.'tulips.256c.gif';
$testaa_filepath = TMP_DIR.'00080_tulips_taa.gif';
$testba_filepath = TMP_DIR.'00080_tulips_tba.gif';
$testab_filepath = TMP_DIR.'00080_tulips_tab.gif';
$testbb_filepath = TMP_DIR.'00080_tulips_tbb.gif';

// import
$rsc = imagecreatefromgif( $import_filepath );
// convolution
imagealphablending( $rsc , false );
imageconvolution( $rsc , array(
  array( 1 , 2 , 1 ) ,
  array( 2 , 4 , 2 ) ,
  array( 1 , 2 , 1 ) ,
) , 15 , 0 );
// export
imagegif( $rsc , $testaa_filepath );
// destroy
imagedestroy( $rsc );

// import
$rsc = imagecreatefromgif( $import_filepath );
// convolution
imagealphablending( $rsc , false );
imageconvolution( $rsc , array(
  array( -1 , -1 , -1 ) ,
  array( -1 , 9 , -1 ) ,
  array( -1 , -1 , -1 ) ,
) , -1 , 255 );
// export
imagegif( $rsc , $testba_filepath );
// destroy
imagedestroy( $rsc );

// import to true color
$tmp = imagecreatefromgif( $import_filepath );
$rsc = imagecreatetruecolor( imagesx( $tmp ) , imagesy( $tmp ) );
if( imagecolortransparent( $tmp ) > -1 )
{
  // fill with transparent
  $transparent = imagecolorsforindex( $tmp , imagecolortransparent( $tmp ) );
  imagealphablending( $rsc , false );
  imagefill( $rsc , 0 , 0 , imagecolorallocatealpha( $rsc , $transparent['red'] , $transparent['green'] , $transparent['blue'] , $transparent['alpha'] ) );
}
// then copy
imagealphablending( $rsc , true );
imagecopy( $rsc , $tmp , 0 , 0 , 0 , 0 , imagesx( $tmp ) , imagesy( $tmp ) );
imagedestroy( $tmp );
// convolution
imagealphablending( $rsc , true ); // true seems to preserve transparent color, but the matrix is not apply on it
imageconvolution( $rsc , array(
  array( 1 , 2 , 1 ) ,
  array( 2 , 4 , 2 ) ,
  array( 1 , 2 , 1 ) ,
) , 15 , 0 );
// export
imagegif( $rsc , $testab_filepath );
// destroy
imagedestroy( $rsc );

// import to true color
$tmp = imagecreatefromgif( $import_filepath );
$rsc = imagecreatetruecolor( imagesx( $tmp ) , imagesy( $tmp ) );
if( imagecolortransparent( $tmp ) > -1 )
{
  // fill with transparent
  $transparent = imagecolorsforindex( $tmp , imagecolortransparent( $tmp ) );
  imagealphablending( $rsc , false );
  imagefill( $rsc , 0 , 0 , imagecolorallocatealpha( $rsc , $transparent['red'] , $transparent['green'] , $transparent['blue'] , $transparent['alpha'] ) );
}
// then copy
imagealphablending( $rsc , true );
imagecopy( $rsc , $tmp , 0 , 0 , 0 , 0 , imagesx( $tmp ) , imagesy( $tmp ) );
imagedestroy( $tmp );
// convolution
imagealphablending( $rsc , true ); // true seems to preserve transparent color, but the matrix is not apply on it
imageconvolution( $rsc , array(
  array( -1 , -1 , -1 ) ,
  array( -1 , 9 , -1 ) ,
  array( -1 , -1 , -1 ) ,
) , -1 , 255 );
// export
imagegif( $rsc , $testbb_filepath );
// destroy
imagedestroy( $rsc );

?>
  
  <h2>Test case 2: GIF</h2>
  
  
  <p><i>
    Note the short dirty line on left and the dirty top-left pixel.<br />
    Via true color, the result looks good and we may be able to &quot;logically&quot; preserve transparency.<br />
    The thin black line along the transparent area comes from wrong alpha calculation and the black background usage.<br />
  </i></p>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($testaa_filepath); ?>" alt="<?php echo basename($testaa_filepath); ?>" title="<?php echo basename($testaa_filepath); ?>" />
          <figcaption>Gaussian blur</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($testba_filepath); ?>" alt="<?php echo basename($testba_filepath); ?>" title="<?php echo basename($testba_filepath); ?>" />
          <figcaption>Sharpen and negate</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($testab_filepath); ?>" alt="<?php echo basename($testab_filepath); ?>" title="<?php echo basename($testab_filepath); ?>" />
          <figcaption>Gaussian blur<br /><i>via true color</i></figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($testbb_filepath); ?>" alt="<?php echo basename($testbb_filepath); ?>" title="<?php echo basename($testbb_filepath); ?>" />
          <figcaption>Sharpen and negate<br /><i>via true color</i></figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php 

$import_filepath = SAMP_DIR.'jellyfish.16c.8.png';
$testaa_filepath = TMP_DIR.'00080_jellyfish_taa.gif';
$testba_filepath = TMP_DIR.'00080_jellyfish_tba.gif';
$testab_filepath = TMP_DIR.'00080_jellyfish_tab.gif';
$testbb_filepath = TMP_DIR.'00080_jellyfish_tbb.gif';

// import
$rsc = imagecreatefrompng( $import_filepath );
// convolution
imagealphablending( $rsc , false );
imageconvolution( $rsc , array(
  array( 1 , 2 , 1 ) ,
  array( 2 , 4 , 2 ) ,
  array( 1 , 2 , 1 ) ,
) , 15 , 0 );
// export
imagesavealpha( $rsc , true );
imagepng( $rsc , $testaa_filepath , 9 );
// destroy
imagedestroy( $rsc );

// import
$rsc = imagecreatefrompng( $import_filepath );
// convolution
imagealphablending( $rsc , false );
imageconvolution( $rsc , array(
  array( -1 , -1 , -1 ) ,
  array( -1 , 9 , -1 ) ,
  array( -1 , -1 , -1 ) ,
) , -1 , 255 );
// export
imagesavealpha( $rsc , true );
imagepng( $rsc , $testba_filepath , 9 );
// destroy
imagedestroy( $rsc );

// import to true color
$tmp = imagecreatefrompng( $import_filepath );
$rsc = imagecreatetruecolor( imagesx( $tmp ) , imagesy( $tmp ) );
if( imagecolortransparent( $tmp ) > -1 )
{
  // fill with transparent
  $transparent = imagecolorsforindex( $tmp , imagecolortransparent( $tmp ) );
  imagealphablending( $rsc , false );
  imagefill( $rsc , 0 , 0 , imagecolorallocatealpha( $rsc , $transparent['red'] , $transparent['green'] , $transparent['blue'] , $transparent['alpha'] ) );
}
// then copy
imagealphablending( $rsc , true );
imagecopy( $rsc , $tmp , 0 , 0 , 0 , 0 , imagesx( $tmp ) , imagesy( $tmp ) );
imagedestroy( $tmp );
// convolution
imagealphablending( $rsc , false );
imageconvolution( $rsc , array(
  array( 1 , 2 , 1 ) ,
  array( 2 , 4 , 2 ) ,
  array( 1 , 2 , 1 ) ,
) , 15 , 0 );
// export
imagesavealpha( $rsc , true );
imagepng( $rsc , $testab_filepath , 9 );
// destroy
imagedestroy( $rsc );

// import to true color
$tmp = imagecreatefrompng( $import_filepath );
$rsc = imagecreatetruecolor( imagesx( $tmp ) , imagesy( $tmp ) );
if( imagecolortransparent( $tmp ) > -1 )
{
  // fill with transparent
  $transparent = imagecolorsforindex( $tmp , imagecolortransparent( $tmp ) );
  imagealphablending( $rsc , false );
  imagefill( $rsc , 0 , 0 , imagecolorallocatealpha( $rsc , $transparent['red'] , $transparent['green'] , $transparent['blue'] , $transparent['alpha'] ) );
}
// then copy
imagealphablending( $rsc , true );
imagecopy( $rsc , $tmp , 0 , 0 , 0 , 0 , imagesx( $tmp ) , imagesy( $tmp ) );
imagedestroy( $tmp );
// convolution
imagealphablending( $rsc , false );
imageconvolution( $rsc , array(
  array( -1 , -1 , -1 ) ,
  array( -1 , 9 , -1 ) ,
  array( -1 , -1 , -1 ) ,
) , -1 , 255 );
// export
imagesavealpha( $rsc , true );
imagepng( $rsc , $testbb_filepath , 9 );
// destroy
imagedestroy( $rsc );

?>
  
  <h2>Test case 3: PNG8</h2>
  
  <p><i>
    Note the short dirty lines on left and the dirty top-left pixel.<br />
    Via true color, the result looks good and we could easily preserve transparency.<br />
  </i></p>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($testaa_filepath); ?>" alt="<?php echo basename($testaa_filepath); ?>" title="<?php echo basename($testaa_filepath); ?>" />
          <figcaption>Gaussian blur</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($testba_filepath); ?>" alt="<?php echo basename($testba_filepath); ?>" title="<?php echo basename($testba_filepath); ?>" />
          <figcaption>Sharpen and negate</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($testab_filepath); ?>" alt="<?php echo basename($testab_filepath); ?>" title="<?php echo basename($testab_filepath); ?>" />
          <figcaption>Gaussian blur<br /><i>via true color</i></figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($testbb_filepath); ?>" alt="<?php echo basename($testbb_filepath); ?>" title="<?php echo basename($testbb_filepath); ?>" />
          <figcaption>Sharpen and negate<br /><i>via true color</i></figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php require '_foot.partial.php'; ?>
  
</body>
</html>
        
