
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
  hr { clear: both; }
  pre { border: 1px black solid; padding: 20px; text-align: left; }
  figure { max-width: 100%; background-image: url(bg.png); display: inline-block; position: relative; padding: 0 20px; margin: 0; box-sizing: border-box; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; }
  figure > img { margin: 20px 0; }
  figcaption { background-color: #fff; font-weight: bold; padding: 5px 10px; margin: 0 -20px; }
  img { max-width: 100%; height: auto; }
  table { width: 100%; border: 0; table-layout: fixed; }
  th { padding: 5px 10px; vertical-align: middle; text-align: center; }
  td { padding: 10px; vertical-align: middle; text-align: center; }
</style>