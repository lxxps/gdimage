<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

$code1 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
        .'$image->apply( new \\GDImage\\Transform_Grayscale() ); // this one is quick'."\n"
        .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

$code2 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
        .'$image->apply( new \\GDImage\\Transform_Sepia() ); // this one takes ages'."\n"
        .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

$code3 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
        .'$transform = new \\GDImage\\Transform_Filter( array('."\n"
        .'  //     R    G    B    A'."\n"
        .'  array( .6 , .4 , 0  , 0  ),'."\n"
        .'  array( 0  , .6 , .4 , 0  ),'."\n"
        .'  array( .4 , 0  , .6 , 0  ),'."\n"
        .'  array( 0  , 0  , 0  , .5 ), // alpha is now half'."\n"
        .') );'."\n"
        .'$image->apply( $transform );'."\n"
        .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

$code4 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
        .'$transform = new \\GDImage\\Transform_Filter( array('."\n"
        .'  //     R    G    B    A'."\n"
        .'  array( .6 , .4 , 0  , 0  ),'."\n"
        .'  array( 0  , .6 , .4 , 0  ),'."\n"
        .'  array( .4 , 0  , .6 , 0  ),'."\n"
        .'  array( 0  , 0  , 0  , .5 ), // alpha is now half'."\n"
        .') );'."\n"
        .'$image->setResource( $image->getResource()->toPaletteColor() ); // convert to palette color in order to optimize performance'."\n"
        .'$image->apply( $transform );'."\n"
        .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

$grayscale = new \GDImage\Transform_Grayscale();

$sepia = new \GDImage\Transform_Sepia();

$custom = new \GDImage\Transform_Filter( array(
  //      R    G   B    A
  array( .6 , .4 , 0  , 0  ),
  array( 0  , .6 , .4 , 0  ),
  array( .4  , 0 , .6 , 0  ),
  array( 0  , 0  , 0  , .5 ), // alpha is now half
) );

// on my local machine, I need more than 5 seconds to filter big PNG24 picture
// and export it
define( 'MAXTIME_TO_TRANSFORM' , 30 );

?>
<html>
<head>
  <title>GDImage: Test 00070 - Color filter on true color</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00070 - Color filter on true color</h1>
  
  <p><i>You will notice that on true color, the filter is really long to process. Each pixel has to be analyzed and converted to expected color.<br />
  The only exception is on \GDImage\Transform_Grayscale() that can use <a href="http://php.net/manual/en/function.imagefilter.php" target="_blank">imagefilter()</a>.<br />
  To optimize performance, you can force the resource to palette color before to apply filter.</i></p>
  
  <pre><?php echo htmlspecialchars( $code1 ); ?></pre>
  <pre><?php echo htmlspecialchars( $code2 ); ?></pre>
  <pre><?php echo htmlspecialchars( $code3 ); ?></pre>
  <pre><?php echo htmlspecialchars( $code4 ); ?></pre>
  
  <hr />
  
<?php
  // Test case 1

  $import_filepath = SAMP_DIR.'chrysanthemum.jpg';
  $export_filepath = array( 
    'grayscale' => TMP_DIR.'00070_chrysanthemum_a1' ,   
    'sepia' => TMP_DIR.'00070_chrysanthemum_a2' , 
    'custom' => TMP_DIR.'00070_chrysanthemum_b1' ,   
    'custom_palette' => TMP_DIR.'00070_chrysanthemum_b2' ,  
  );
  $export_time = array( 
    'grayscale' => 0 ,   
    'sepia' => 0 , 
    'custom' => 0 ,   
    'custom_palette' => 0 ,  
  );
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepath as $key => &$path )
  {
    $clone = clone $image;
    // give me some times to apply the transform
    set_time_limit( \MAXTIME_TO_TRANSFORM );
    $start = microtime( true );
    if( $key === 'custom_palette' )
    {
      $clone->setResource( $clone->getResource()->toPaletteColor() );
      $clone->apply( $custom );
    }
    else
    {
      $clone->apply( ${$key} );
    }
    $export_time[$key] = microtime( true ) - $start;
    $path = \GDImage\Factory::export( $clone , $path );
  }
  
?>
  <h2>Test case 1: JPEG</h2>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['grayscale']); ?>" alt="<?php echo basename($export_filepath['grayscale']); ?>" title="<?php echo basename($export_filepath['grayscale']); ?>" />
          <figcaption>
            Grayscale<br />
            <i>took <?php echo microtime_to_human( $export_time['grayscale'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['sepia']); ?>" alt="<?php echo basename($export_filepath['sepia']); ?>" title="<?php echo basename($export_filepath['sepia']); ?>" />
          <figcaption>
            Sepia<br />
            <i>took <?php echo microtime_to_human( $export_time['sepia'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['custom']); ?>" alt="<?php echo basename($export_filepath['custom']); ?>" title="<?php echo basename($export_filepath['custom']); ?>" />
          <figcaption>
            Custom<br />
            <i>took <?php echo microtime_to_human( $export_time['custom'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['custom_palette']); ?>" alt="<?php echo basename($export_filepath['custom_palette']); ?>" title="<?php echo basename($export_filepath['custom_palette']); ?>" />
          <figcaption>
            Custom via palette color<br />
            <i>took <?php echo microtime_to_human( $export_time['custom_palette'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php
  // Test case 2

  $import_filepath = SAMP_DIR.'lighthouse.24.png';
  $export_filepath = array( 
    'grayscale' => TMP_DIR.'00070_lighthouse_a1' ,   
    'sepia' => TMP_DIR.'00070_lighthouse_a2' , 
    'custom' => TMP_DIR.'00070_lighthouse_b1' ,   
    'custom_palette' => TMP_DIR.'00070_lighthouse_b2' ,  
  );
  $export_time = array( 
    'grayscale' => 0 ,   
    'sepia' => 0 , 
    'custom' => 0 ,   
    'custom_palette' => 0 ,  
  );
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepath as $key => &$path )
  {
    $clone = clone $image;
    // give me some times to apply the transform
    set_time_limit( \MAXTIME_TO_TRANSFORM );
    $start = microtime( true );
    if( $key === 'custom_palette' )
    {
      $clone->setResource( $clone->getResource()->toPaletteColor() );
      $clone->apply( $custom );
    }
    else
    {
      $clone->apply( ${$key} );
    }
    $export_time[$key] = microtime( true ) - $start;
    $path = \GDImage\Factory::export( $clone , $path );
  }
  
?>
  <h2>Test case 2: PNG24</h2>
  
  <p><i>My local server needs beetween 5 and 10 seconds to process color filter and export this PNG24 picture.</i></p>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['grayscale']); ?>" alt="<?php echo basename($export_filepath['grayscale']); ?>" title="<?php echo basename($export_filepath['grayscale']); ?>" />
          <figcaption>
            Grayscale<br />
            <i>took <?php echo microtime_to_human( $export_time['grayscale'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['sepia']); ?>" alt="<?php echo basename($export_filepath['sepia']); ?>" title="<?php echo basename($export_filepath['sepia']); ?>" />
          <figcaption>
            Sepia<br />
            <i>took <?php echo microtime_to_human( $export_time['sepia'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['custom']); ?>" alt="<?php echo basename($export_filepath['custom']); ?>" title="<?php echo basename($export_filepath['custom']); ?>" />
          <figcaption>
            Custom<br />
            <i>took <?php echo microtime_to_human( $export_time['custom'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['custom_palette']); ?>" alt="<?php echo basename($export_filepath['custom_palette']); ?>" title="<?php echo basename($export_filepath['custom_palette']); ?>" />
          <figcaption>
            Custom via palette color<br />
            <i>took <?php echo microtime_to_human( $export_time['custom_palette'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php require '_foot.partial.php'; ?>
  
</body>
</html>
        
