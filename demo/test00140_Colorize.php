<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';
  
$code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
       .'$transform = new \\GDImage\\Transform_Colorize( $color );'."\n"
       .'$image->apply( $transform );'."\n"
       .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

?>
<html>
<head>
  <title>GDImage: Test 00140 - Colorize</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00140 - Colorize</h1>
  
  <pre><?php echo $code; ?></pre>
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'chrysanthemum.jpg';
  $export_filepaths = array(
    'FF0000' => TMP_DIR.'00140_chrysanthemum_a.jpg' ,
    '00FF00' => TMP_DIR.'00140_chrysanthemum_b.jpg' ,
    '0000FF' => TMP_DIR.'00140_chrysanthemum_c.jpg' ,
    '7F7F7F' => TMP_DIR.'00140_chrysanthemum_d.jpg' ,
    '0000003F' => TMP_DIR.'00140_chrysanthemum_e.jpg' ,
    '0000007F' => TMP_DIR.'00140_chrysanthemum_f.jpg' ,
  );
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepaths as $color => $export_filepath )
  {
    $clone = clone $image;
    $clone->apply( new \GDImage\Transform_Colorize( $color ) );
    \GDImage\Factory::export( $clone , $export_filepath );
  }
  
?>
  <h2>Test case 1: Colorize JPEG</h2>
  
  <p><i>Alpha channel has no impact on JPEG colorization.</i></p>
  
  <table>
    <tr>
      <td rowspan="2" colspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture
          </figcaption>
        </figure>
      </td>
      <?php $i = 1; foreach( $export_filepaths as $color => $export_filepath ): ?>
        <?php if( $i === 4 ) print '</tr><tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
            <figcaption>
              Colorize with "<?php echo $color; ?>"
            </figcaption>
          </figure>
        </td>
      <?php $i++; endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'tulips.256c.gif';
  $export_filepaths = array(
    'FF0000' => TMP_DIR.'00140_tulips_a.gif' ,
    '00FF00' => TMP_DIR.'00140_tulips_b.gif' ,
    '0000FF' => TMP_DIR.'00140_tulips_c.gif' ,
    '7F7F7F' => TMP_DIR.'00140_tulips_d.gif' ,
    '0000003F' => TMP_DIR.'00140_tulips_e.gif' ,
    '0000007F' => TMP_DIR.'00140_tulips_f.gif' ,
  );
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepaths as $color => $export_filepath )
  {
    $clone = clone $image;
    $clone->apply( new \GDImage\Transform_Colorize( $color ) );
    \GDImage\Factory::export( $clone , $export_filepath );
  }
  
?>
  <h2>Test case 2: Colorize GIF</h2>
  
  <p><i>Alpha channel has no impact on GIF colorization.</i></p>
  
  <table>
    <tr>
      <td rowspan="2" colspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture
          </figcaption>
        </figure>
      </td>
      <?php $i = 1; foreach( $export_filepaths as $color => $export_filepath ): ?>
        <?php if( $i === 4 ) print '</tr><tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
            <figcaption>
              Colorize with "<?php echo $color; ?>"
            </figcaption>
          </figure>
        </td>
      <?php $i++; endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'lighthouse.24.png';
  $export_filepaths = array(
    'FF0000' => TMP_DIR.'00140_lighthouse_a.png' ,
    '00FF00' => TMP_DIR.'00140_lighthouse_b.png' ,
    '0000FF' => TMP_DIR.'00140_lighthouse_c.png' ,
    '7F7F7F' => TMP_DIR.'00140_lighthouse_d.png' ,
    '0000003F' => TMP_DIR.'00140_lighthouse_e.png' ,
    '0000007F' => TMP_DIR.'00140_lighthouse_f.png' ,
  );
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepaths as $color => $export_filepath )
  {
    $clone = clone $image;
    $clone->apply( new \GDImage\Transform_Colorize( $color ) );
    \GDImage\Factory::export( $clone , $export_filepath );
  }
  
?>
  <h2>Test case 3: Colorize PNG</h2>
  
  <p><i>Colorize with full alpha channel result to a transparent picture, but color information are kept.</i></p>
  
  <table>
    <tr>
      <td rowspan="2" colspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture
          </figcaption>
        </figure>
      </td>
      <?php $i = 1; foreach( $export_filepaths as $color => $export_filepath ): ?>
        <?php if( $i === 4 ) print '</tr><tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
            <figcaption>
              Colorize with "<?php echo $color; ?>"
            </figcaption>
          </figure>
        </td>
      <?php $i++; endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
