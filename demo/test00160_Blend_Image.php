<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';
  
$code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
       .'// background may be an image instance or anything that \\GDImage\\Factory can import'."\n"
       .'$transform = new \\GDImage\\Transform_Blend_Image( $background , $mode );'."\n"
       .'$image->apply( $transform );'."\n"
       .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';



define( 'MAXTIME_TO_TRANSFORM' , 30 );

?>
<html>
<head>
  <title>GDImage: Test 00160 - Blend over picture</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00160 - Blend over picture</h1>
  
  <pre><?php echo $code; ?></pre>
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'tulips.256c.gif';
  $blend_filepath = SAMP_DIR.'patterns/blend_bg1.png';
  $export_time = array();
  
  $blend_mode = array(
//  'normal' => 'Normal', 
  'darken' => 'Darken' , 
//  'multiply' => 'Multiply' , 
//  'color_burn' => 'Color Burn' , 
//  'linear_burn' => 'Linear Burn' ,
  'lighten' => 'Lighten' ,
//  'screen' => 'Screen' ,
//  'color_dodge' => 'Color Dodge' ,
  'linear_dodge' => 'Linear Dodge' ,
//  'overlay' => 'Overlay' ,
//  'soft_light' => 'Soft Light' ,
  'hard_light' => 'Hard Light' ,
//  'vivid_light' => 'Vivid Light' ,
//  'linear_light' => 'Linear Light' ,
//  'pin_light' => 'Pin Light' ,
  'difference' => 'Difference' ,
  'exclusion' => 'Exclusion' ,
);
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  $blend = new \GDImage\Transform_Blend_Image( $blend_filepath );
  
  foreach( array_keys( $blend_mode ) as $mode )
  {
    $clone = clone $image;
    
    // give me some time
    set_time_limit( MAXTIME_TO_TRANSFORM );
    
    $start = microtime( true );
    $blend->setMode( $mode );
    $clone->apply( $blend );
    $export_time[$mode] = microtime( true ) - $start;
    
    $export_filepaths[$mode] = \GDImage\Factory::export( $clone , TMP_DIR.'00160_tulips_'.$mode );
  }
  
?>
  <h2>Test case 1: Transparent color on Palette Color picture</h2>
  
  <p><i>We always attempt to restore a transparent color, even if it may be unexpected.</i></p>
  
  <table>
    <tr>
      <td colspan="3">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture
          </figcaption>
        </figure>
      </td>
      <td colspan="3">
        <figure>
          <img src="./samples/patterns/<?php echo basename($blend_filepath); ?>" alt="<?php echo basename($blend_filepath); ?>" title="<?php echo basename($blend_filepath); ?>" />
          <figcaption>
            Background picture
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <?php $i = 0; foreach( $export_filepaths as $mode => $export_filepath ): ?>
        <?php if( $i !== 0 && $i % 6 === 0 ) print '</tr><tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
            <figcaption>
              <?php echo $blend_mode[$mode]; ?> (<code><?php echo $mode; ?></code>)<br />
              <i>took <?php echo microtime_to_human( $export_time[$mode] ); ?></i>
            </figcaption>
          </figure>
        </td>
      <?php $i++; endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'chrysanthemum.jpg';
  $export_filepaths = array(
    SAMP_DIR.'patterns/blend_bg2a.png' => TMP_DIR.'00160_chrysanthemum_2a.jpg' ,
    SAMP_DIR.'patterns/blend_bg2b.png' => TMP_DIR.'00160_chrysanthemum_2b.jpg' ,  
  );
  $export_time = array();
  
  $blend_mode = array(
    'normal' => 'Normal', 
    'darken' => 'Darken' , 
    'multiply' => 'Multiply' , 
    'color_burn' => 'Color Burn' , 
    'linear_burn' => 'Linear Burn' ,
    'lighten' => 'Lighten' ,
    'screen' => 'Screen' ,
    'color_dodge' => 'Color Dodge' ,
    'linear_dodge' => 'Linear Dodge' ,
    'overlay' => 'Overlay' ,
    'soft_light' => 'Soft Light' ,
    'hard_light' => 'Hard Light' ,
    'vivid_light' => 'Vivid Light' ,
    'linear_light' => 'Linear Light' ,
    'pin_light' => 'Pin Light' ,
    'difference' => 'Difference' ,
    'exclusion' => 'Exclusion' ,
  );
  
  $mode = 'pin_light';
  
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepaths as $blend_filepath => $export_filepath )
  {
    $clone = clone $image;
    
    // give me some time
    set_time_limit( MAXTIME_TO_TRANSFORM );
    
    $start = microtime( true );
    $blend = new \GDImage\Transform_Blend_Image( $blend_filepath , $mode );
    $clone->apply( $blend );
    $export_time[$blend_filepath] = microtime( true ) - $start;
    
    \GDImage\Factory::export( $clone , $export_filepath );
  }
  
?>
  <h2>Test case 2: Background picture with alpha channel</h2>
  
  <p><i>Keep in mind that even transparent colors have color channel values.<br />
  So if you want to control them, the background picture must be fully opaque.</i></p>
  
  <table>
    <tr>
      <td rowspan="2" colspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture
          </figcaption>
        </figure>
      </td>
      <?php foreach( array_keys( $export_filepaths ) as $blend_filepath ): ?>
        <td>
          <figure>
            <img src="./samples/patterns/<?php echo basename($blend_filepath); ?>" alt="<?php echo basename($blend_filepath); ?>" title="<?php echo basename($blend_filepath); ?>" />
            <figcaption>
              Background picture
            </figcaption>
          </figure>
        </td>
      <?php endforeach; ?>
    </tr>
    <tr>
      <?php foreach( $export_filepaths as $blend_filepath => $export_filepath ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
            <figcaption>
              <?php echo $blend_mode[$mode]; ?> (<code><?php echo $mode; ?></code>)<br />
              <i>took <?php echo microtime_to_human( $export_time[$blend_filepath] ); ?></i>
            </figcaption>
          </figure>
        </td>
      <?php endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
