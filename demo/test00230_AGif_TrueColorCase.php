<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

?>
<html>
<head>
  <title>GDImage: Test 00230 - AGIF True Color Case</title>
  <?php require '_head.partial.php'; ?>
  <style>
    small { white-space: nowrap; }
  </style>
</head>
<body>
  
  <h1>GDImage: Test 00230 - AGIF True Color Case</h1>
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'agif/phil_ipal_org.32697c.gif';
  $export_basepath = TMP_DIR.'00230_phil_ipal_org';
  
  // get pack to display usefull info
  $pack = \GDImage\AGif_Factory::unpack( $import_filepath );
  
  // give me some times for this one
  set_time_limit( 30 );
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  $final_filepath = array();
  foreach( $image->getFrames() as $i => $frame )
  {
    $final_filepath[] = \GDImage\Factory::export( $frame , $export_basepath.sprintf( '%02d' , $i ) , 'image/png' );
  }
  $export_time = microtime( true ) - $start;
  
  $nb_cols = 5;
  
  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$final_filepath = array();'."\n"
         .'foreach( $image->getFrames() as $i => $frame )'."\n"
         .'{'."\n"
         .'  $final_filepath[] = \\GDImage\\Factory::export( $frame , $export_basepath.sprintf( \'%02d\' , $i ) , \'image/png\' );'."\n"
         .'}';
  
?>
  <h2>Test case 1: Decomposition</h2>
  
  <p><i>
    A true color GIF is a GIF where all frames has Local Color Table and where the number of repeatitions is unspecified.<br />
    An interesting fact is that number of repeatitions has been introduced by Netscape and is not part of GIF specification, so we can presume that GIF specification was originally designed for that purpose.<br />
    To keep true color on frame export, we have to do it to PNG format.<br />
  </i></p>
  
  <pre><?php echo $code; ?></pre>
  
  <table>
    <tr>
      <td colspan="<?php echo $nb_cols; ?>">
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            from <a href="http://phil.ipal.org/tc.html" target="_blank">phil.ipal.org</a><br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
        <br />
        <small>
          Width: <?php echo $pack['Logical Screen Descriptor']['Logical Screen Width']; ?><br />
          Height: <?php echo $pack['Logical Screen Descriptor']['Logical Screen Height']; ?><br />
          Global Color Table: <?php echo $pack['Logical Screen Descriptor']['Packed']['Global Color Table Flag'] ? 'yes' : 'no'; ?><br />
          Number of Repetitions: <?php echo isset( $pack['Application Extension0']['Number of Repetitions'] ) ? ( $pack['Application Extension0']['Number of Repetitions'] ? $pack['Application Extension0']['Number of Repetitions'] : '&infin;' ) : '<font color="red">none</font>'; ?><br />
        </small>
      </td>
    </tr>
    <?php $i = 0; foreach( $final_filepath as $filepath ): ?>
      <?php $i % $nb_cols === 0 && print '<tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Frame <?php echo $i+1; ?>
            </figcaption>
          </figure>
          <br />
          <small>
            Width: <?php echo $pack['Image Descriptor'.$i]['Image Width']; ?><br />
            Height: <?php echo $pack['Image Descriptor'.$i]['Image Height']; ?><br />
            Left: <?php echo $pack['Image Descriptor'.$i]['Image Left Position']; ?><br />
            Top: <?php echo $pack['Image Descriptor'.$i]['Image Top Position']; ?><br />
            Local Color Table: <?php echo $pack['Image Descriptor'.$i]['Packed']['Local Color Table Flag'] ? 'yes' : 'no'; ?><br />
            <?php if( isset($pack['Graphic Control Extension'.$i]) ): ?>
              Delay Time: <?php echo $pack['Graphic Control Extension'.$i]['Delay Time']; ?><br />
              Disposal Method: <?php echo $pack['Graphic Control Extension'.$i]['Packed']['Disposal Method']; ?><br />
              Transparent Color: <?php echo $pack['Graphic Control Extension'.$i]['Packed']['Transparent Color Flag'] ? 'yes' : 'no'; ?><br />
            <?php endif; ?>
          </small>
        </td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endforeach; ?>
    <?php while( $i % $nb_cols !== 0 ): ?>
      <td></td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endwhile; ?>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 2
  
  // give me a lot of times for this one
  set_time_limit( 240 );

  $import_filepath = SAMP_DIR.'agif/phil_ipal_org.32697c.gif';
  $export_filepath = TMP_DIR.'00230_phil_ipal_org.32697c.gif';
  
  // make sure this setting is true
  \GDImage\Config::setAGifComposerOptimization( \PHP_INT_MAX );
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath );
  $export_time = microtime( true ) - $start;
  
  $code = '\\GDImage\\Config::setAGifComposerOptimization( \PHP_INT_MAX ); // enable optimization'."\n"
         ."\n"
         .'$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

?>
  <h2>Test case 2: Recomposition</h2>
  
  <p><i>
    For true color GIF, AGIF Composer Optimization must be enable.<br />
    This recomposition takes ages. We have to compare 173 frames and look if any pixels has changed!<br />
    The good news is that the generated AGIF has LZW compression.<br />
  </i></p>
  
  <pre><?php echo $code; ?></pre>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://phil.ipal.org/tc.html" target="_blank">phil.ipal.org</a>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
          <figcaption>
            Recomposition (<?php echo filesize_to_human( $export_filepath ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
