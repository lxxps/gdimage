<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

?>
<html>
<head>
  <title>GDImage: Test 00250 - AGIF Edition</title>
  <?php require '_head.partial.php'; ?>
  <style>
    small { white-space: nowrap; }
  </style>
</head>
<body>
  
  <h1>GDImage: Test 00250 - AGIF Edition</h1>
  
<?php /**/
  // Test case 1

  $frame_filepaths = array(
    SAMP_DIR.'chrysanthemum.jpg' ,
    SAMP_DIR.'desert.24.png' ,
    SAMP_DIR.'hydrangeas.128c.8.png' ,
    SAMP_DIR.'lighthouse.24.png' ,
    SAMP_DIR.'tulips.256c.gif' ,
  );
  $export_filepath = TMP_DIR.'00250_agif_2';
  
  // import
  $start = microtime( true );
  ###
  
  // disable optimization
  \GDImage\Config::setAGifComposerOptimization( 0 ); 
  
  // create empty animated GIF
  $image = new \GDImage\Image_AGif();
  
  // add frames
  foreach( $frame_filepaths as $frame_filepath )
  {
    $image->addFrame( \GDImage\Factory::import( $frame_filepath ) );
  }
  
  // update Delay Time to 1s
  $image->getFrame( 0 )->setDelayTime( 100 );
  // set Disposal Method to "no"
  $image->getFrame( 0 )->setDisposalMethod( 1 );
  
  // update Delay Time to 1s
  $image->getFrame( 1 )->setDelayTime( 100 );
  // set Disposal Method to "previous"
  $image->getFrame( 1 )->setDisposalMethod( 3 );
  
  // update Delay Time to 1s
  $image->getFrame( 2 )->setDelayTime( 100 );
  // set Disposal Method to "no"
  $image->getFrame( 2 )->setDisposalMethod( 1 );
  
  // update Delay Time to 1s
  $image->getFrame( 3 )->setDelayTime( 100 );
  // set Disposal Method to "previous"
  $image->getFrame( 3 )->setDisposalMethod( 3 );
  
  // update Delay Time to 1s
  $image->getFrame( 4 )->setDelayTime( 100 );
  // set Disposal Method to "previous"
  $image->getFrame( 4 )->setDisposalMethod( 3 );
  
  // export the animated GIF
  $final_filepath = \GDImage\Factory::export( $image , $export_filepath );
  
  ###
  $export_time = microtime( true ) - $start;

?>
  <h2>Test case 1: AGIF creation from different format</h2>
  
  <pre>// disable optimization
\GDImage\Config::setAGifComposerOptimization( 0 ); 

// create empty animated GIF
$image = new \GDImage\Image_AGif();

// add frames
foreach( $frame_filepaths as $frame_filepath )
{
  $image->addFrame( \GDImage\Factory::import( $frame_filepath ) );
}

// update Delay Time to 1s
$image->getFrame( 0 )->setDelayTime( 100 );
// set Disposal Method to "no"
$image->getFrame( 0 )->setDisposalMethod( 1 );

// update Delay Time to 1s
$image->getFrame( 1 )->setDelayTime( 100 );
// set Disposal Method to "previous"
$image->getFrame( 1 )->setDisposalMethod( 3 );

// update Delay Time to 1s
$image->getFrame( 2 )->setDelayTime( 100 );
// set Disposal Method to "no"
$image->getFrame( 2 )->setDisposalMethod( 1 );

// update Delay Time to 1s
$image->getFrame( 3 )->setDelayTime( 100 );
// set Disposal Method to "previous"
$image->getFrame( 3 )->setDisposalMethod( 3 );

// update Delay Time to 1s
$image->getFrame( 4 )->setDelayTime( 100 );
// set Disposal Method to "previous"
$image->getFrame( 4 )->setDisposalMethod( 3 );

// export the animated GIF
$final_filepath = \GDImage\Factory::export( $image , $export_filepath );</pre>
  
  <p><i>
    Observe Dispoal Method in action: the last frame show the first frame behind third one, wow!
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($frame_filepaths[0]); ?>" alt="<?php echo basename($frame_filepaths[0]); ?>" title="<?php echo basename($frame_filepaths[0]); ?>" />
          <figcaption>
            Original picture (<?php echo strtoupper( pathinfo( $frame_filepaths[0] , PATHINFO_EXTENSION ) ); ?>)<br />
            Disposal Method 1 (do not dispose)<br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($frame_filepaths[1]); ?>" alt="<?php echo basename($frame_filepaths[1]); ?>" title="<?php echo basename($frame_filepaths[1]); ?>" />
          <figcaption>
            Original picture (<?php echo strtoupper( pathinfo( $frame_filepaths[1] , PATHINFO_EXTENSION ) ); ?>)<br />
            Disposal Method 3 (restore to previous)<br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($frame_filepaths[2]); ?>" alt="<?php echo basename($frame_filepaths[2]); ?>" title="<?php echo basename($frame_filepaths[2]); ?>" />
          <figcaption>
            Original picture (<?php echo strtoupper( pathinfo( $frame_filepaths[2] , PATHINFO_EXTENSION ) ); ?>)<br />
            Disposal Method 1 (do not dispose)<br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($frame_filepaths[3]); ?>" alt="<?php echo basename($frame_filepaths[3]); ?>" title="<?php echo basename($frame_filepaths[3]); ?>" />
          <figcaption>
            Original picture (<?php echo strtoupper( pathinfo( $frame_filepaths[0] , PATHINFO_EXTENSION ) ); ?>)<br />
            Disposal Method 3 (restore to previous)<br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($frame_filepaths[4]); ?>" alt="<?php echo basename($frame_filepaths[4]); ?>" title="<?php echo basename($frame_filepaths[4]); ?>" />
          <figcaption>
            Original picture (<?php echo strtoupper( pathinfo( $frame_filepaths[0] , PATHINFO_EXTENSION ) ); ?>)<br />
            Disposal Method 3 (restore to previous)<br />
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td colspan="<?php echo count($frame_filepaths); ?>">
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath); ?>" alt="<?php echo basename($final_filepath); ?>" title="<?php echo basename($final_filepath); ?>" />
          <figcaption>
            Result (<?php echo filesize_to_human( $final_filepath ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'motions/shroomery_org.jpg';
  $export_filepath = TMP_DIR.'00250_shroomery_org';
  
  // import
  $start = microtime( true );
  ###

  // create animated GIF
  $image = new \GDImage\Image_AGif();

  // import source
  $src = \GDImage\Factory::import( $import_filepath );

  // this motion has 9 frames of 143px x 100px
  for( $i = 0; $i < 9; $i++ )
  {
    // create a frame helping AGif_Factory
    $frame = \GDImage\AGif_Factory::frame();

    // create blank resource
    $frame->setResource( $src->getResource()->blank( 143 , 100 ) );
    
    // copy
    imagecopy( $frame->getResource()->getGdResource() , $src->getResource()->getGdResource() , 0 , 0 , 143 * $i , 0 , 143 , 100 );
    
    // finally add frame
    $image->addFrame( $frame );
  }

  // then export
  $final_filepath = \GDImage\Factory::export( $image , $export_filepath );
  
  ###
  $export_time = microtime( true ) - $start;

?>
  <h2>Test case 2: AGIF from motion</h2>
  
  <pre>// create animated GIF
$image = new \GDImage\Image_AGif();

// import source
$src = \GDImage\Factory::import( $import_filepath );

// this motion has 9 frames of 143px x 100px
for( $i = 0; $i < 9; $i++ )
{
  // create a frame helping AGif_Factory
  $frame = \GDImage\AGif_Factory::frame();

  // create blank resource
  $frame->setResource( $src->getResource()->blank( 143 , 100 ) );

  // copy
  imagecopy( $frame->getResource()->getGdResource() , $src->getResource()->getGdResource() , 0 , 0 , 143 * $i , 0 , 143 , 100 );

  // finally add frame
  $image->addFrame( $frame );
}

// then export
$final_filepath = \GDImage\Factory::export( $image , $export_filepath );</pre>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/motions/<?php echo basename( $import_filepath ); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            from <a href="http://www.shroomery.org/ythan/motion.html" target="_blank">shroomery.org</a><br />
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath); ?>" alt="<?php echo basename($final_filepath); ?>" title="<?php echo basename($final_filepath); ?>" />
          <figcaption>
            Result (<?php echo filesize_to_human( $final_filepath ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 3

  $import_filepath = SAMP_DIR.'agif/lesjoiesducode_fr.gif';
  $frame_filepath = SAMP_DIR.'me.gif';
  $export_filepath = TMP_DIR.'00250_lesjoiesducode_fr';
  
  // import
  $start = microtime( true );
  ###
  
  // disable optimization
  \GDImage\Config::setAGifComposerOptimization( 0 ); 
  
  // import animated GIF
  $image = \GDImage\Factory::import( $import_filepath );
  
  // insert the frame at third position
  // by default delay time is 0 (as fast as possible)
  // and disposal method 2 (restore to background)
  $image->addFrame( \GDImage\Factory::import( $frame_filepath ) , 2 );
  
  // we want to remove 10 frames and keep the same duration
  // fetch the frame
  $frame = $image->getFrame( 2 );
  
  // resize frame to fit image size, this is not automatic
  // on export if the frame is larger than first frame
  // the picture is cropped without resizing
  $frame->apply( new \GDImage\Transform_Resize_FitNFill( $image->getWidth() , $image->getHeight() ) );
  
  // go over next 10 frames
  // note that when we unset a frame, frame indexes are rearranged
  $i = 10;
  while( $i-- )
  {
    // increase delay time
    $frame->setDelayTime( $frame->getDelayTime() + $image->getFrame( 3 )->getDelayTime() );
    // unset next frame
    $image->unsetFrame( 3 );
  }
  
  // we also want the previous frame to restore to background
  $image->getFrame( 1 )->setDisposalMethod( 2 );
  
  // export the animated GIF
  $final_filepath = \GDImage\Factory::export( $image , $export_filepath );
  
  ###
  $export_time = microtime( true ) - $start;

?>
  <h2>Test case 3: AGIF frame insertion and deletion</h2>
  
  <pre>// disable optimization
\GDImage\Config::setAGifComposerOptimization( 0 ); 

// import animated GIF
$image = \GDImage\Factory::import( $import_filepath );

// insert the frame at third position
// by default delay time is 0 (as fast as possible)
// and disposal method 2 (restore to background)
$image->addFrame( \GDImage\Factory::import( $frame_filepath ) , 2 );

// we want to remove 10 frames and keep the same duration
// fetch the frame
$frame = $image->getFrame( 2 );

// resize frame to fit image size, this is not automatic
// on export if the frame is larger than first frame
// the picture is cropped without resizing
$frame->apply( new \GDImage\Transform_Resize_FitNFill( $image->getWidth() , $image->getHeight() ) );

// go over next 10 frames
// note that when we unset a frame, frame indexes are rearranged
$i = 10;
while( $i-- )
{
  // increase delay time
  $frame->setDelayTime( $frame->getDelayTime() + $image->getFrame( 3 )->getDelayTime() );
  // unset next frame
  $image->unsetFrame( 3 );
}

// we also want the previous frame to restore to background
$image->getFrame( 1 )->setDisposalMethod( 2 );

// export the animated GIF
$final_filepath = \GDImage\Factory::export( $image , $export_filepath );</pre>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            from <a href="http://lesjoiesducode.fr/" target="_blank">lesjoiesducode.fr</a><br />
            <?php echo imagesize_to_human( $import_filepath ); ?><br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($frame_filepath); ?>" alt="<?php echo basename($frame_filepath); ?>" title="<?php echo basename($frame_filepath); ?>" />
          <figcaption>
            Inserted picture<br />
            <?php echo imagesize_to_human( $frame_filepath ); ?><br />
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath); ?>" alt="<?php echo basename($final_filepath); ?>" title="<?php echo basename($final_filepath); ?>" />
          <figcaption>
            Result (<?php echo filesize_to_human( $final_filepath ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
