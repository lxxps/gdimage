<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


error_reporting( E_ALL );
ini_set( 'display_errors' , true );
ini_set( 'html_errors' , false );

// Do not hesitate to clear the APC cache if you made some changes
// apc_clear_cache();

header('Content-Type: text/html; charset=utf-8');

define( 'SAMP_DIR' , './samples/' );
define( 'TMP_DIR' , './tmp/' );

// a little check to be sure
if( ! is_writeable( TMP_DIR ) ) chmod( TMP_DIR , 0666 );
  
// call composer autoload
require_once( '../vendor/autoload.php' );


/** Usefull functions **/

function microtime_to_human( $time )
{
  if( $time <= 0 ) return '<font color="red">instant</font>';
  if( $time <= 0.001 ) return '<font color="red">'.number_format( $time * 1000000 , 2 ).'µs</font>';
  if( $time <= 0.1 ) return '<font color="red">'.number_format( $time * 1000 , 2 ).'ms</font>';
  return number_format( $time , 2 ).'s';
}

function filesize_to_human( $file )
{
  $size = filesize( $file );
  $units = array( 'b' , 'kb' , 'Mb' , 'Gb' );
  $i = 0;
  while( $size > 1024 ) {
    $size = $size / 1024;
    $i++;
  }  
  return round( $size ).' '.$units[$i];
}  

function imagesize_to_human( $file )
{
  list( $w , $h ) = getimagesize( $file );
  
  return $w.'px &times; '.$h.'px';
}