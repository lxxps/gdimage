<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

// disable optimization
\GDImage\Config::setAGifComposerOptimization( 0 ); 
\GDImage\Config::setAPngComposerOptimization( 0 ); 
  

?>
<html>
<head>
  <title>GDImage: Test 00240 - AGIF Conversion</title>
  <?php require '_head.partial.php'; ?>
  <style>
    small { white-space: nowrap; }
  </style>
</head>
<body>
  
  <h1>GDImage: Test 00240 - AGIF Conversion</h1>
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'agif/littlesvr_ca_1.gif';
  
  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
  $export_filepath = array(
    'png' => TMP_DIR.'00240_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.png' , 
    'jpeg' => TMP_DIR.'00240_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.jpg' , 
  );
  
  $start = microtime( true );
  // import animated GIF
  $image = \GDImage\Factory::import( $import_filepath );
  
  // to JPEG
  \GDImage\Factory::export( $image , $export_filepath['jpeg'] );
  
  // to PNG
  \GDImage\Factory::export( $image , $export_filepath['png'] );
  

?>
  <h2>Test case 1: MIME Type conversion</h2>
  
  <pre><?php echo $code ?></pre>
  
  <p><i>
    As expected, conversion of an animated GIF to PNG results to an APNG.
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://littlesvr.ca/apng/gif_apng_webp4.html" target="_blank">littlesvr.ca</a><br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['jpeg']); ?>" alt="<?php echo basename($export_filepath['jpeg']); ?>" title="<?php echo basename($export_filepath['jpeg']); ?>" />
          <figcaption>To JPEG (<?php echo filesize_to_human( $export_filepath['jpeg'] ); ?>)<br /><br /></figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png']); ?>" alt="<?php echo basename($export_filepath['png']); ?>" title="<?php echo basename($export_filepath['png']); ?>" />
          <figcaption>To PNG (<?php echo filesize_to_human( $export_filepath['png'] ); ?>)<br /><br /></figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'agif/littlesvr_ca_2.gif';
  
  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$single = \\GDImage\\Factory::instance( $mimetype ); // create a \\GDImage\\Image_Abstract_Single instance of expected MIME Type'."\n"
         .'$single->fromImage( $image ); // set resource from imported image'."\n"
         .'// clone to be sure that first AGIF frame will not be altered, also clone here to not clone all AGIF frames'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( clone $single , $export_filepath );';
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  $export_filepath = array(
    'gif' => TMP_DIR.'00240_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.gif' , 
    'png8' => TMP_DIR.'00240_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.8.png' , 
    'png24' => TMP_DIR.'00240_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.24.png' , 
    'jpeg' => TMP_DIR.'00240_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.jpg' , 
  );
  
  // to GIF
  $single = \GDImage\Factory::instance( 'image/gif' );
  $single->fromImage( $image );
  \GDImage\Factory::export( clone $single , $export_filepath['gif'] );
  
  // to PNG8
  $single = \GDImage\Factory::instance( 'image/png' );
  $single->fromImage( $image );
  \GDImage\Factory::export( clone $single , $export_filepath['png8'] );
  
  // to PNG24
  $single = \GDImage\Factory::instance( 'image/png' );
  $single->fromImage( $image );
  // from GIF, we have to force true color
  \GDImage\Factory::export( clone $single , array(
    'driver' => 'file' ,
    'file' => $export_filepath['png24'] ,
    'truecolor' => true ,
  ) );
  
  // to JPEG
  $single = \GDImage\Factory::instance( 'image/jpeg' );
  $single->fromImage( $image );
  \GDImage\Factory::export( clone $single , $export_filepath['jpeg'] );
  
?>
  <h2>Test case 2: AGIF to single</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    As expected, the first frame is extracted when AGIF is converted to a single picture.      
  </i></p>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://littlesvr.ca/apng/gif_apng_webp3.html" target="_blank">littlesvr.ca</a><br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['gif']); ?>" alt="<?php echo basename($export_filepath['gif']); ?>" title="<?php echo basename($export_filepath['gif']); ?>" />
          <figcaption>To GIF (<?php echo filesize_to_human( $export_filepath['gif'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png8']); ?>" alt="<?php echo basename($export_filepath['png8']); ?>" title="<?php echo basename($export_filepath['png8']); ?>" />
          <figcaption>To PNG8 (<?php echo filesize_to_human( $export_filepath['png8'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['jpeg']); ?>" alt="<?php echo basename($export_filepath['jpeg']); ?>" title="<?php echo basename($export_filepath['jpeg']); ?>" />
          <figcaption>To JPEG (<?php echo filesize_to_human( $export_filepath['jpeg'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png24']); ?>" alt="<?php echo basename($export_filepath['png24']); ?>" title="<?php echo basename($export_filepath['png24']); ?>" />
          <figcaption>To PNG24 (<?php echo filesize_to_human( $export_filepath['png24'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
