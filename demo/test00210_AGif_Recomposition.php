<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';
  
$code = '\\GDImage\\Config::setAGifComposerOptimization( \\GDImage\\AGif_Composer::OPTIMIZE_ALL ); // enable optimization'."\n"
       ."\n"
       .'$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
       .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'."\n";

// for this test cases, activate optimization
\GDImage\Config::setAGifComposerOptimization( \GDImage\AGif_Composer::OPTIMIZE_ALL );

?>
<html>
<head>
  <title>GDImage: Test 00210 - AGIF Recomposition</title>
  <?php require '_head.partial.php'; ?>
  <style>
    small { white-space: nowrap; }
  </style>
</head>
<body>
  
  <h1>GDImage: Test 00210 - AGIF Recomposition</h1>
  
  <p><i>
    For test cases below, we can activate all optimizations: there is not a lot of frames and frames sizes are relatively small.<br />
    For more information about AGIF optimization, see <a href="test00215_AGif_Optimization.php">AGIF Optimization</a>.<br />
  </i></p>
  
  <pre><?php echo $code; ?></pre>
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'agif/ajaxload_info.gif';
  $export_filepath = TMP_DIR.'00210_ajaxload_info.gif';
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath );
  $export_time = microtime( true ) - $start;
  
?>
  <h2>Test case 1: Disposal Method 0 (no disposal specified) and Background Color</h2>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://ajaxload.info/" target="_blank">ajaxload.info</a>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
          <figcaption>
            After manipulation (<?php echo filesize_to_human( $export_filepath ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'agif/photoshop.gif';
  $export_filepath = TMP_DIR.'00210_photoshop.gif';
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath );
  $export_time = microtime( true ) - $start;
  
?>
  <h2>Test case 2: Disposal Method 1 (do not dispose) and Logical Screen Descriptor mismatch</h2>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            created helping Photoshop
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
          <figcaption>
            After manipulation (<?php echo filesize_to_human( $export_filepath ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 3

  $import_filepath = SAMP_DIR.'agif/preloaders_net.gif';
  $export_filepath = TMP_DIR.'00210_preloaders_net.gif';
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath );
  $export_time = microtime( true ) - $start;
  
?>
  <h2>Test case 3: Disposal Method 2 (restore to background) and Transparent Color</h2>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://preloaders.net/" target="_blank">preloaders.net</a>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
          <figcaption>
            After manipulation (<?php echo filesize_to_human( $export_filepath ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 4

  $import_filepath = SAMP_DIR.'agif/the-labs_com_c0.gif';
  $export_filepath = TMP_DIR.'00210_thelabs_com_c0.gif';
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath );
  $export_time = microtime( true ) - $start;
  
?>
  <h2>Test case 4: Disposal Method 0 (no disposal specified) and Logical Screen Descriptor mismatch</h2>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://the-labs.com/GIFMerge/" target="_blank">the-labs.com</a>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
          <figcaption>
            After manipulation (<?php echo filesize_to_human( $export_filepath ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 5

  $import_filepath = SAMP_DIR.'agif/gifology_m6.gif';
  $export_filepath = TMP_DIR.'00210_gifology_m6.gif';
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath );
  $export_time = microtime( true ) - $start;
  
?>
  <h2>Test case 5: Disposal Method 1 (do not dispose) mixed with Disposal Method 2 (restore to background)</h2>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://www.theimage.com/animation/pages/disposal3.html" target="_blank">theimage.com</a>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
          <figcaption>
            After manipulation (<?php echo filesize_to_human( $export_filepath ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 6

  $import_filepath = SAMP_DIR.'agif/gifology_m7.gif';
  $export_filepath = TMP_DIR.'00210_gifology_m7.gif';
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath );
  $export_time = microtime( true ) - $start;
  
?>
  <h2>Test case 6: Disposal Method 1 (do not dispose) mixed with Disposal Method 3 (restore to previous)</h2>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://www.theimage.com/animation/pages/disposal3.html" target="_blank">theimage.com</a>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
          <figcaption>
            After manipulation (<?php echo filesize_to_human( $export_filepath ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
