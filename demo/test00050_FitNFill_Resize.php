<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

?>
<html>
<head>
  <title>GDImage: Test 00050 - Fit 'n' Fill resizing</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00050 - Fit 'n' Fill resizing</h1>
  
  <hr />
  
<?php
  // Test case 1

  $import_filepath = array( 
    'gif' => SAMP_DIR.'tulips.256c.gif' ,
    'png8' => SAMP_DIR.'jellyfish.16c.8.png' ,
    'jpeg' => SAMP_DIR.'chrysanthemum.jpg' ,
    'png24' => SAMP_DIR.'lighthouse.24.png' ,
  );
  
  $transform = new \GDImage\Transform_Resize_FitNFill( 320 , 192 );
  
  $export_filepath = array(
    'gif' => TMP_DIR.'00050_tulips_a1.gif' ,
    'png8' => TMP_DIR.'00050_jellyfish_a1.png' ,
    'jpeg' => TMP_DIR.'00050_chrysanthemum_a1.jpg' ,
    'png24' => TMP_DIR.'00050_lighthouse_a1.png' ,
  );
  
  foreach( $import_filepath as $key => $path )
  {
    $image = \GDImage\Factory::import( $path );
    $image->apply( $transform );
    $export_filepath[$key] = \GDImage\Factory::export( $image , $export_filepath[$key] );
  }
  
  $code = '$image = \\GDImage\\Factory::import( $import_path );'."\n"
         .'$transform = new \\GDImage\\Transform_Resize_FitNFill( 320 , 192 );'."\n"
         .'$image->apply( $transform );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
  
?>
  <h2>Test case 1: Fit 'n' Fill resize with transparent color</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>You will notice that JPEG get the default color without alpha channel.<br />
  To change the default color, use <code>\GDImage\Config::setColorDefaultValue( $color_representation );</code>.</i></p>
  
  <table>
    <tr>
      <?php foreach( $import_filepath as $key => $path ): ?>
        <td>
          <figure>
            <img src="./samples/<?php echo basename($path); ?>" alt="<?php echo basename($path); ?>" title="<?php echo basename($path); ?>" />
            <figcaption><?php echo strtoupper( $key ); ?> picture <?php echo imagesize_to_human( $path ); ?></figcaption>
          </figure>
        </td>
      <?php endforeach; ?>
    </tr>
    <tr>
      <?php foreach( $export_filepath as $key => $path ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($path); ?>" alt="<?php echo basename($path); ?>" title="<?php echo basename($path); ?>" />
            <figcaption>
              Fit 'n' Fill to 320px &times; 192px<br />
              <i>result to <?php echo imagesize_to_human( $path ); ?></i>
            </figcaption>
          </figure>
        </td>
      <?php endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
<?php
  // Test case 1

  $import_filepath = array( 
    'gif' => SAMP_DIR.'tulips.256c.gif' ,
    'png8' => SAMP_DIR.'jellyfish.16c.8.png' ,
    'jpeg' => SAMP_DIR.'chrysanthemum.jpg' ,
    'png24' => SAMP_DIR.'lighthouse.24.png' ,
  );
  
  $transform = new \GDImage\Transform_Resize_FitNFill( 320 , 192 , 'FFFFFF' );
  $transform->setSize( 320 , 192 );
  
  $export_filepath = array(
    'gif' => TMP_DIR.'00050_tulips_a2.gif' ,
    'png8' => TMP_DIR.'00050_jellyfish_a2.png' ,
    'jpeg' => TMP_DIR.'00050_chrysanthemum_a2.jpg' ,
    'png24' => TMP_DIR.'00050_lighthouse_a2.png' ,
  );
  
  $import_transparent = array();
  $export_transparent = array();
  
  foreach( $import_filepath as $key => $path )
  {
    $image = \GDImage\Factory::import( $path );
    
    // log transparent
    if( $image->getResource()->isPaletteColor() && $image->getResource()->getTransparent() )
    {
      $import_transparent[$key] = $image->getResource()->getTransparent()->toArray();
    }
    
    $image->apply( $transform );
    
    // log transparent
    if( $image->getResource()->isPaletteColor() && $image->getResource()->getTransparent() )
    {
      $export_transparent[$key] = $image->getResource()->getTransparent()->toArray();
    }
    
    $export_filepath[$key] = \GDImage\Factory::export( $image , $export_filepath[$key] );
  }
  
  $code = '$image = \\GDImage\\Factory::import( $import_path );'."\n"
         .'$transform = new \\GDImage\\Transform_Resize_FitNFill( 320 , 192 , \'FFFFFF\' );'."\n"
         .'$image->apply( $transform );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
?>
  <h2>Test case 2: Fit 'n' Fill resize with white background</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>You will notice that PNG24 picture does not have alpha channel anymore.<br />
  For convenience, transparent color are preserved even if they are not used anymore.</i></p>
  
  <table>
    <tr>
      <?php foreach( $import_filepath as $key => $path ): ?>
        <td>
          <figure>
            <img src="./samples/<?php echo basename($path); ?>" alt="<?php echo basename($path); ?>" title="<?php echo basename($path); ?>" />
            <figcaption>
              <?php echo strtoupper( $key ); ?> picture <?php echo imagesize_to_human( $path ); ?><br />
              <?php if( isset( $import_transparent[$key] ) ): ?>
                <i>transparent:&nbsp;<?php array_unshift( $import_transparent[$key] , 'rgba(%d,%d,%d,%d)' ); echo call_user_func_array( 'sprintf' , $import_transparent[$key] ); ?></i>
              <?php endif; ?><br />
            </figcaption>
          </figure>
        </td>
      <?php endforeach; ?>
    </tr>
    <tr>
      <?php foreach( $export_filepath as $key => $path ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($path); ?>" alt="<?php echo basename($path); ?>" title="<?php echo basename($path); ?>" />
            <figcaption>
              Fit 'n' Fill to 320px &times; 192px<br />
              <?php if( isset( $export_transparent[$key] ) ): ?>
                <i>transparent:&nbsp;<?php array_unshift( $export_transparent[$key] , 'rgba(%d,%d,%d,%d)' ); echo call_user_func_array( 'sprintf' , $export_transparent[$key] ); ?></i>
              <?php endif; ?><br />
            </figcaption>
          </figure>
        </td>
      <?php endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
<?php
  // Test case 3

  $import_filepath = array( 
    'gif' => SAMP_DIR.'tulips.256c.gif' ,
    'png8' => SAMP_DIR.'jellyfish.16c.8.png' ,
    'jpeg' => SAMP_DIR.'chrysanthemum.jpg' ,
    'png24' => SAMP_DIR.'lighthouse.24.png' ,
  );
  
  $transform = new \GDImage\Transform_Resize_FitNFill( 320 , 192 , 'FFFFFF7F' );
  
  $export_filepath = array(
    'gif' => TMP_DIR.'00050_tulips_a3.gif' ,
    'png8' => TMP_DIR.'00050_jellyfish_a3.png' ,
    'jpeg' => TMP_DIR.'00050_chrysanthemum_a3.jpg' ,
    'png24' => TMP_DIR.'00050_lighthouse_a3.png' ,
  );
  
  foreach( $import_filepath as $key => $path )
  {
    $image = \GDImage\Factory::import( $path );
    $image->apply( $transform );
    $export_filepath[$key] = \GDImage\Factory::export( $image , $export_filepath[$key] );
  }
  
  $code = '$image = \\GDImage\\Factory::import( $import_path );'."\n"
         .'$transform = new \\GDImage\\Transform_Resize_FitNFill( 320 , 192 , \'FFFFFF7F\' );'."\n"
         .'$image->apply( $transform );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
?>
  <h2>Test case 3: Fit 'n' Fill resize with white background and full alpha channel</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>You will notice that JPEG lose alpha channel information.</i></p>
  
  <table>
    <tr>
      <?php foreach( $import_filepath as $key => $path ): ?>
        <td>
          <figure>
            <img src="./samples/<?php echo basename($path); ?>" alt="<?php echo basename($path); ?>" title="<?php echo basename($path); ?>" />
            <figcaption>
              <?php echo strtoupper( $key ); ?> picture <?php echo imagesize_to_human( $path ); ?><br />
            </figcaption>
          </figure>
        </td>
      <?php endforeach; ?>
    </tr>
    <tr>
      <?php foreach( $export_filepath as $key => $path ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($path); ?>" alt="<?php echo basename($path); ?>" title="<?php echo basename($path); ?>" />
            <figcaption>
              Fit 'n' Fill to 320px &times; 192px<br />
            </figcaption>
          </figure>
        </td>
      <?php endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
<?php
  // Test case 4

  $import_filepath = SAMP_DIR.'lighthouse.24.png';
  
  $transform = new \GDImage\Transform_Resize_FitNFill( 192 , 240 );
  
  $export_filepath = array(
    'FFFFFF7F' => TMP_DIR.'00050_lighthouse_b1.gif' ,
    '0000007F' => TMP_DIR.'00050_lighthouse_b2.gif' ,
    'FF00007F' => TMP_DIR.'00050_lighthouse_b3.gif' ,
  );
  
  foreach( $export_filepath as $key => $path )
  {
    $image = \GDImage\Factory::import( $import_filepath );
    $transform->setColor( $key );
    $image->apply( $transform );
    $export_filepath[$key] = \GDImage\Factory::export( $image , $export_filepath[$key] );
  }
  
  $code = '$image = \\GDImage\\Factory::import( $import_path );'."\n"
         .'$transform = new \\GDImage\\Transform_Resize_FitNFill( 192 , 240 , $color );'."\n"
         .'$image->apply( $transform );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
?>
  <h2>Test case 4: Fit 'n' Fill resize from PNG24 to GIF</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>Sometimes, <a href="http://php.net/manual/en/function.imagecolormatch.php" target="_blank">imagecolormatch()</a> does not provide a good resampling for alpha channels, so GIF transparent color may be lost.<br />
    This happend when image is filled with a transparent color that can be found in the picture.</i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['0000007F']); ?>" alt="<?php echo basename($export_filepath['0000007F']); ?>" title="<?php echo basename($export_filepath['0000007F']); ?>" />
          <figcaption>
            Fit 'n' Fill<br />
            <i>with transparent black</i><br />
            <small>A big amount of pixels close to black color<br />can be found on the picture<br />
            So the average alpha channel of all colors close to black one<br />results to a black color almost opaque.</small>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['FFFFFF7F']); ?>" alt="<?php echo basename($export_filepath['FFFFFF7F']); ?>" title="<?php echo basename($export_filepath['FFFFFF7F']); ?>" />
          <figcaption>
            Fit 'n' Fill<br />
            <i>with transparent white</i><br />
            <small>A small amount of pixels close to white color<br />can be found on the picture<br />
            So the average alpha channel of all colors close to white one<br />results to a white color almost transparent.</small>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['FF00007F']); ?>" alt="<?php echo basename($export_filepath['FF00007F']); ?>" title="<?php echo basename($export_filepath['FF00007F']); ?>" />
          <figcaption>
            Fit 'n' Fill<br />
            <i>with transparent red</i><br />
            <small>Not any pixels is close to red color.<br />
            So the red color stay fully transparent.<br /><br /><br /></small>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php require '_foot.partial.php'; ?>
  
</body>
</html>
        
