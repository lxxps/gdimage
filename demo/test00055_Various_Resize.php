<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

?>
<html>
<head>
  <title>GDImage: Test 00055 - Various resizing</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00055 - Various resizing</h1>
  
  <hr />
  
<?php
  // Test case 1

  $import_filepath = SAMP_DIR.'lighthouse.24.png';
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  $transform = new \GDImage\Transform_Resize_Crop_Fix( 320 , 320 );
  
  $export_filepath = array();
  $export_position = array();
  
  $positions = array( 
    array( 'TOP' , 'LEFT' , ) ,
    array( 'TOP' ) ,
    array( 'TOP' , 'RIGHT' , ) ,
    array( 'LEFT' ) ,
    array() ,
    array( 'RIGHT' ) ,
    array( 'BOTTOM' , 'LEFT' , ) ,
    array( 'BOTTOM' ) ,
    array( 'BOTTOM' , 'RIGHT' , ) ,
  );

  $i = count( $positions );
  while( $i-- )
  {
     $bit = 0;
     $str = '';
     $j = count( $positions[$i] );
     while( $j-- )
     {
       $str .= constant( '\\GDImage\\Transform_Resize_Crop_Fix::'.$positions[$i][$j] ).' | ';
       $bit |= constant( '\\GDImage\\Transform_Resize_Crop_Fix::'.$positions[$i][$j] );
     }
     $str = rtrim( $str , ' |' );
     
     $clone = clone $image;
     $transform->setPosition( $bit );
     $clone->apply( $transform );
     $export_filepath[$i] = \GDImage\Factory::export( $clone , TMP_DIR.'00055_lighthouse_'.$i );
     $export_position[$i] = $str ? $str : 'none <i>(0)</i>';
  }
  
  $code = '$image = \\GDImage\\Factory::import( $import_path );'."\n"
         .'$transform = new \\GDImage\\Transform_Resize_Crop_Fix( 320 , 320 , $bitwise_position );'."\n"
         .'$image->apply( $transform );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
  
?>
  <h2>Test case 1: Crop resize at fixed position</h2>
  
  <table>
    <tr>
      <?php for( $i = 0; $i < 3; $i++ ): ?>        
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath[$i]); ?>" alt="<?php echo basename($export_filepath[$i]); ?>" title="<?php echo basename($export_filepath[$i]); ?>" />
            <figcaption>To position: <?php echo $export_position[$i]; ?></figcaption>
          </figure>
        </td>
      <?php endfor; ?>
    </tr>
    <tr>
      <?php for( ; $i < 6; $i++ ): ?>        
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath[$i]); ?>" alt="<?php echo basename($export_filepath[$i]); ?>" title="<?php echo basename($export_filepath[$i]); ?>" />
            <figcaption>To position: <?php echo $export_position[$i]; ?></figcaption>
          </figure>
        </td>
      <?php endfor; ?>
    </tr>
    <tr>
      <?php for( ; $i < 9; $i++ ): ?>        
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath[$i]); ?>" alt="<?php echo basename($export_filepath[$i]); ?>" title="<?php echo basename($export_filepath[$i]); ?>" />
            <figcaption>To position: <?php echo $export_position[$i]; ?></figcaption>
          </figure>
        </td>
      <?php endfor; ?>
    </tr>
  </table>
  
  <hr />
  
<?php require '_foot.partial.php'; ?>
  
</body>
</html>
        
