<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


	error_reporting( E_ALL );
	ini_set( 'display_errors' , true );
	ini_set( 'html_errors' , false );
  
  // Do not hesitate to clear the APC cache if you made some changes
  // apc_clear_cache();
  
  header('Content-Type: text/html; charset=utf-8');
  
  define( 'SAMP_DIR' , dirname(__FILE__).DIRECTORY_SEPARATOR.'samples'.DIRECTORY_SEPARATOR );
  define( 'TMP_DIR' , dirname(__FILE__).DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR );

  $transformations = array(
    array(
      'title' => 'Fit \'n\' Fill (300px × 300px, on grey background)' ,
      'code' => '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
               .'$transform = new \\GDImage\\Transform_Resize_FitNFill( 300 , 300 , \'777777\' );'."\n"
               .'$image->apply( $transform );'."\n"
               .'\\GDImage\\Factory::export( $image , $export_filepath );'
               ,
    ) ,
    array(
      'title' => 'Fit resize (300px × 300px)' ,
      'code' => '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
               .'$transform = new \\GDImage\\Transform_Resize_Fit( 300 , 300 );'."\n"
               .'$image->apply( $transform );'."\n"
               .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'
               ,
    ) ,
    array(
      'title' => 'Crop resize (300px × 300px)' ,
      'code' => '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
               .'$transform = new \\GDImage\\Transform_Resize_Crop( 300 , 300 );'."\n"
               .'$image->apply( $transform );'."\n"
               .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'
               ,
    ) ,
//    array(
//      'title' => 'Crop fixed resize (300px × 300px, on top and left corner)' ,
//      'code' => '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
//               .'$transform = new \\GDImage\\Transform_Resize_Crop_Fix();'."\n"
//               .'$transform->setPosition( \GDImage\\Transform_Resize_Crop_Fix::TOP | \GDImage\\Transform_Resize_Crop_Fix::LEFT );'."\n"
//               .'$transform->setSize( 300 , 300 );'."\n"
//               .'$image->apply( $transform );'."\n"
//               .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'
//               ,
//    ) ,
//    array(
//      'title' => 'Borders (10px, red, on left, right and top)' ,
//      'code' => '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
//               .'$transform = new \\GDImage\\Transform_Border();'."\n"
//               .'$transform->setWidth( 10 );'."\n"
//               .'$transform->setColor( new \\GDImage\\Color( "FF0000" ) );'."\n"
//               .'$transform->setBorders( \\GDImage\\Transform_Border::LEFT | \\GDImage\\Transform_Border::RIGHT | \\GDImage\\Transform_Border::TOP );'."\n"
//               .'$image->apply( $transform );'."\n"
//               .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'
//               ,
//    ) ,
//    array(
//      'title' => 'Round corners (50px, top-left and bottom-right)' ,
//      'code' => '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
//               .'$transform = new \\GDImage\\Transform_Corner();'."\n"
//               .'$transform->setRadius( 50 );'."\n"
//               .'$transform->setCorners( \\GDImage\\Transform_Corner::TOP_LEFT | \\GDImage\\Transform_Corner::BOTTOM_RIGHT );'."\n"
//               .'$image->apply( $transform );'."\n"
//               .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'
//               ,
//    ) ,
    array(
      'title' => 'Grayscale' ,
      'code' => '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
               .'$transform = new \\GDImage\\Transform_Grayscale();'."\n"
               .'$image->apply( $transform );'."\n"
               .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'
               ,
    ) ,
    array(
      'title' => 'Sepia' ,
      'code' => '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
               .'$transform = new \\GDImage\\Transform_Sepia();'."\n"
               .'$image->apply( $transform );'."\n"
               .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'
               ,
    ) ,
    array(
      'title' => 'Blur' ,
      'code' => '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
               .'$transform = new \\GDImage\\Transform_Blur();'."\n"
               .'$image->apply( $transform );'."\n"
               .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'
               ,
    ) ,
    array(
      'title' => 'Gaussian blur' ,
      'code' => '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
               .'$transform = new \\GDImage\\Transform_GaussianBlur();'."\n"
               .'$image->apply( $transform );'."\n"
               .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'
               ,
    ) ,
    array(
      'title' => 'Negate' ,
      'code' => '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
               .'$transform = new \\GDImage\\Transform_Negate();'."\n"
               .'$image->apply( $transform );'."\n"
               .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'
               ,
    ) ,
    array(
      'title' => 'Sharpen' ,
      'code' => '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
               .'$transform = new \\GDImage\\Transform_Sharpen();'."\n"
               .'$image->apply( $transform );'."\n"
               .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'
               ,
    ) ,
    array(
      'title' => 'Horizontal flip' ,
      'code' => '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
               .'$transform = new \\GDImage\\Transform_Flip( \'h\' );'."\n"
               .'$image->apply( $transform );'."\n"
               .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'
               ,
    ) ,
    array(
      'title' => 'Bottom mirror' ,
      'code' => '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
               .'$transform = new \\GDImage\\Transform_Mirror( \'b\' );'."\n"
               .'$image->apply( $transform );'."\n"
               .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'
               ,
    ) ,
    array(
      'title' => 'Posterize' ,
      'code' => '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
               .'$transform = new \\GDImage\\Transform_Posterize( 128 );'."\n"
               .'$image->apply( $transform );'."\n"
               .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'
               ,
    ) ,
    array(
      'title' => 'Multiple (grayscale and negate)' ,
      'code' => '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
               .'$transform = new \\GDImage\\Transform_Multiple('."\n"
               .'  new \\GDImage\\Transform_Grayscale() ,'."\n"
               .'  new \\GDImage\\Transform_Negate()'."\n"
               .');'."\n"
               .'$image->apply( $transform );'."\n"
               .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'
               ,
    ) ,
  );

  $_filename  = isset($_GET['f']) ? $_GET['f'] : null;
  $_transform = isset($_GET['t']) ? (int)$_GET['t'] : null;
  if( empty($transformations[$_transform]) ) $_transform = null;
?>

<html>
<head><title>GDImage</title></head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
  hr { clear: both; }
  pre { border: 1px black solid; padding: 20px; }
  figure { background-image: url(bg.png); padding: 20px; display: inline-block; }
  label > figure { padding: 5px; }
</style>
<body>
  
<?php if( $_filename !== null && $_transform !== null ):

require_once( '../src/GDImage/bootstrap.inc.php' );
  
$import_filepath = SAMP_DIR.$_filename;

$export_filepath = TMP_DIR.'00000_'.$_filename;

eval( $transformations[$_transform]['code'] );

?>

<h1>GDImage: Apply tranformation "<?php echo $transformations[$_transform]['title']; ?>" on image "<?php echo $_filename; ?>"</h1>

<pre><?php echo htmlspecialchars( $transformations[$_transform]['code'] ); ?></pre>

<h2><a href="./samples/<?php echo basename($import_filepath); ?>" target="_blank">Original picture</a></h2>

<figure>
  <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
</figure>

<h2><a href="./tmp/<?php echo basename($export_filepath); ?>" target="_blank">Modified picture</a></h2>

<figure>
  <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
</figure>

<br />
<hr />

<?php endif; // endif( $file_name )

$samples = glob( SAMP_DIR.'*.*' );

?>

<h1>GDImage: Select an image and a transformation</h1>
<form method="GET" action=".">
  <h2>Transformation</h2>
  <ul>
    <?php foreach( $transformations as $i => $transform ): ?>
      <li>
        <input type="radio" <?php ( $_transform === $i ) && print 'checked="checked"'; ?> value="<?php echo $i; ?>" name="t" id="t<?php echo $i; ?>"/>
        <label for="t<?php echo $i; ?>"><?php echo $transform['title']; ?></label>
      </li>
    <?php endforeach; ?>
  </ul>
  <h2>Image</h2>
  <ul>
    <?php foreach( $samples as $i => $file ): ?>
      <?php if( ! is_file( $file ) ) continue; // not a file ?>
      <?php if( ! @$info = getimagesize( $file ) ) continue; // not a picture ?>
      <?php $file = basename($file);  ?>
      <li>
        <input type="radio" <?php ( $_filename === $file ) && print 'checked="checked"'; ?> value="<?php echo $file; ?>" name="f" id="f<?php echo $i; ?>"/>
        <label for="f<?php echo $i; ?>" style="vertical-align:-40px;margin-bottom:10px;"><figure><img src="./samples/<?php echo $file; ?>" height="<?php echo min( 96 , $info[1] ); ?>" alt="<?php echo $file; ?>" title="<?php echo $file; ?>" /></figure></label>
      </li>
    <?php endforeach; ?>
  </ul>
  <button type="submit">GO</button>
</form>

<hr />
  
<?php /**/ require '_foot.partial.php'; ?>

</body>
</html>