<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';
  
$code1 = '\\GDImage\\Config::setAPngComposerOptimization( \\GDImage\\APng_Composer::OPTIMIZE_ALL ); // enable optimization'."\n"
       ."\n"
       .'$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
       .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'."\n";
  
$code2 = '\\GDImage\\Config::setAPngComposerOptimization( \\GDImage\\APng_Composer::OPTIMIZE_ALL ); // enable optimization'."\n"
       ."\n"
       .'$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
       .'$final_filepath = \\GDImage\\Factory::export( $image , array('."\n"
       .'  \'driver\' => \'File\' ,'."\n"
       .'  \'file\' => $export_filepath ,'."\n"
       .'  $compression ,'."\n"
       .') );';

// for this test cases, activate optimization
\GDImage\Config::setAPngComposerOptimization( \GDImage\APng_Composer::OPTIMIZE_ALL );

?>
<html>
<head>
  <title>GDImage: Test 00210 - AGIF Recomposition</title>
  <?php require '_head.partial.php'; ?>
  <style>
    small { white-space: nowrap; }
  </style>
</head>
<body>
  
  <h1>GDImage: Test 00310 - APNG Recomposition</h1>
  
  <p><i>
    For test cases below, we can activate all optimizations: there is not a lot of frames and frames sizes are relatively small.<br />
    For more information about APNG optimization, see <a href="test00315_APng_Optimization.php">APNG Optimization</a>.<br />
  </i></p>
  
  <pre><?php echo $code1; ?></pre>
  
  <pre><?php echo $code2; ?></pre>
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'apng/preloaders_net_1.png';
  
  $export_filepath1 = TMP_DIR.'00310_preloaders_net_1a.png';
  $export_filepath2 = TMP_DIR.'00310_preloaders_net_1b.png';
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath1 );
  $export_time1 = microtime( true ) - $start;
  
  // export
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , array(
    'driver' => 'File' ,
    'file' => $export_filepath2 ,
    9 ,
  ) );
  $export_time2 = microtime( true ) - $start;
  
?>
  <h2>Test case 1: Dispose option 0 (do not dispose) and True color with alpha</h2>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://preloaders.net/" target="_blank">preloaders.net</a><br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath1); ?>" alt="<?php echo basename($export_filepath1); ?>" title="<?php echo basename($export_filepath1); ?>" />
          <figcaption>
            Default recomposition (<?php echo filesize_to_human( $export_filepath1 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time1 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath2); ?>" alt="<?php echo basename($export_filepath2); ?>" title="<?php echo basename($export_filepath2); ?>" />
          <figcaption>
            With maximum compression (<?php echo filesize_to_human( $export_filepath2 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time2 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'apng/apngcreator_1.png';
  
  $export_filepath1 = TMP_DIR.'00310_apngcreator_1a.png';
  $export_filepath2 = TMP_DIR.'00310_apngcreator_1b.png';
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath1 );
  $export_time1 = microtime( true ) - $start;
  
  // export
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , array(
    'driver' => 'File' ,
    'file' => $export_filepath2 ,
    9 ,
  ) );
  $export_time2 = microtime( true ) - $start;
  
?>
  <h2>Test case 2: Dispose option 0 (do not dispose), True color with alpha and Blend option 1 (over)</h2>
  
  <p><i>
    You may notice that even if Blend Option has been disabled on decomposition, the recomposition still visually exact.<br />
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from APNG_Creator
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath1); ?>" alt="<?php echo basename($export_filepath1); ?>" title="<?php echo basename($export_filepath1); ?>" />
          <figcaption>
            Default recomposition (<?php echo filesize_to_human( $export_filepath1 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time1 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath2); ?>" alt="<?php echo basename($export_filepath2); ?>" title="<?php echo basename($export_filepath2); ?>" />
          <figcaption>
            With maximum compression (<?php echo filesize_to_human( $export_filepath2 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time2 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 3

  $import_filepath = SAMP_DIR.'apng/wikipedia_org.png';
  
  $export_filepath1 = TMP_DIR.'00310_wikipedia_orga.png';
  $export_filepath2 = TMP_DIR.'00310_wikipedia_orgb.png';
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath1 );
  $export_time1 = microtime( true ) - $start;
  
  // export
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , array(
    'driver' => 'File' ,
    'file' => $export_filepath2 ,
    9 ,
  ) );
  $export_time2 = microtime( true ) - $start;
  
?>
  <h2>Test case 3: Mixing Dispose option, True color with alpha and IHDR Width and Height mismatch</h2>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://en.wikipedia.org/wiki/APNG" target="_blank">wikipedia.org</a><br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath1); ?>" alt="<?php echo basename($export_filepath1); ?>" title="<?php echo basename($export_filepath1); ?>" />
          <figcaption>
            Default recomposition (<?php echo filesize_to_human( $export_filepath1 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time1 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath2); ?>" alt="<?php echo basename($export_filepath2); ?>" title="<?php echo basename($export_filepath2); ?>" />
          <figcaption>
            With maximum compression (<?php echo filesize_to_human( $export_filepath2 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time2 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 4

  $import_filepath = SAMP_DIR.'apng/philip_html5_org_006.png';
  
  $export_filepath1 = TMP_DIR.'00310_philip_html5_org_006a.png';
  $export_filepath2 = TMP_DIR.'00310_philip_html5_org_006b.png';
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath1 );
  $export_time1 = microtime( true ) - $start;
  
  // export
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , array(
    'driver' => 'File' ,
    'file' => $export_filepath2 ,
    9 ,
  ) );
  $export_time2 = microtime( true ) - $start;
  
?>
  <h2>Test case 4: One frame and default image</h2>
  
  <p><i>
    To see the default image, use Internet Explorer, Chrome or Opera.
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="https://philip.html5.org/tests/apng/tests.html" target="_blank">philip.html5.org</a><br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath1); ?>" alt="<?php echo basename($export_filepath1); ?>" title="<?php echo basename($export_filepath1); ?>" />
          <figcaption>
            Default recomposition (<?php echo filesize_to_human( $export_filepath1 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time1 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath2); ?>" alt="<?php echo basename($export_filepath2); ?>" title="<?php echo basename($export_filepath2); ?>" />
          <figcaption>
            With maximum compression (<?php echo filesize_to_human( $export_filepath2 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time2 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
