<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';
  
$code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
       .'$transform = new \\GDImage\\Transform_Opacity( $pct ) ); // from 0 to 100'."\n"
       .'$image->apply( $transform );'."\n"
       .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

?>
<html>
<head>
  <title>GDImage: Test 00075 - Opacity</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00075 - Opacity</h1>
  
  <pre><?php echo $code; ?></pre>
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'chrysanthemum.jpg';
  $export_filepaths = array(
    80 => TMP_DIR.'00075_chrysanthemum_80.png' ,
    60 => TMP_DIR.'00075_chrysanthemum_60.png' ,
    40 => TMP_DIR.'00075_chrysanthemum_40.png' ,
    20 => TMP_DIR.'00075_chrysanthemum_20.png' ,
  );
  
  $image = \GDImage\Factory::import( $import_filepath );
  $opacity = new \GDImage\Transform_Opacity();
  
  foreach( $export_filepaths as $pct => $export_filepath )
  {
    $clone = clone $image;
    $clone->apply( new \GDImage\Transform_Opacity( $pct ) );
    \GDImage\Factory::export( $clone , $export_filepath );
  }
  
?>
  <h2>Test case 1: Opacity on JPEG</h2>
  
  <p><i>Obviously, export has to be done to PNG24.</i></p>
  
  <table>
    <tr>
      <td rowspan="2" colspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture
          </figcaption>
        </figure>
      </td>
      <?php $i = 1; foreach( $export_filepaths as $nb => $export_filepath ): ?>
        <?php if( $i === 3 ) print '</tr><tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
            <figcaption>
              Opacity <?php echo $nb; ?>%
            </figcaption>
          </figure>
        </td>
      <?php $i++; endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'tulips.256c.gif';
  $export_filepaths = array(
    80 => TMP_DIR.'00075_tulips_80.png' ,
    60 => TMP_DIR.'00075_tulips_60.png' ,
    40 => TMP_DIR.'00075_tulips_40.png' ,
    20 => TMP_DIR.'00075_tulips_20.png' ,
  );
  
  $image = \GDImage\Factory::import( $import_filepath );
  $opacity = new \GDImage\Transform_Opacity();
  
  foreach( $export_filepaths as $pct => $export_filepath )
  {
    $clone = clone $image;
    $clone->apply( new \GDImage\Transform_Opacity( $pct ) );
    \GDImage\Factory::export( $clone , $export_filepath );
  }
  
?>
  <h2>Test case 2: Opacity on GIF</h2>
  
  <p><i>Obviously, export has to be done to PNG8.</i></p>
  
  <table>
    <tr>
      <td rowspan="2" colspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture
          </figcaption>
        </figure>
      </td>
      <?php $i = 1; foreach( $export_filepaths as $nb => $export_filepath ): ?>
        <?php if( $i === 3 ) print '</tr><tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
            <figcaption>
              Opacity <?php echo $nb; ?>%
            </figcaption>
          </figure>
        </td>
      <?php $i++; endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 3

  $import_filepath = SAMP_DIR.'jellyfish.16c.8.png';
  $export_filepaths = array(
    'png' => TMP_DIR.'00075_jellyfish_0.png' ,
    'jpg' => TMP_DIR.'00075_jellyfish_0.jpg' ,
  );
  $export_colors = array();
  
  $image = \GDImage\Factory::import( $import_filepath );
  $opacity = new \GDImage\Transform_Opacity( 0 );
  
  foreach( $export_filepaths as $nb => $export_filepath )
  {
    $clone = clone $image;
    $clone->apply( $opacity );
    \GDImage\Factory::export( $clone , $export_filepath );
    // log colors
    $export_colors[$nb] = $clone->getResource()->countColors();
  }
  
?>
  <h2>Test case 3: Opacity on PNG8</h2>
  
  <p><i>
    Note that even on fully transparent transformation, color information stay available.<br />
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepaths['png']); ?>" alt="<?php echo basename($export_filepaths['png']); ?>" title="<?php echo basename($export_filepaths['png']); ?>" />
          <figcaption>
            To PNG<br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepaths['jpg']); ?>" alt="<?php echo basename($export_filepaths['jpg']); ?>" title="<?php echo basename($export_filepaths['jpg']); ?>" />
          <figcaption>
            To JPG<br />
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
