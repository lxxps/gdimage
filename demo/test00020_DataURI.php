<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

?>
<html>
<head>
  <title>GDImage: Test 00020 - Data URI support</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00020 - Data URI support</h1>
  
  <hr />
  
<?php
  // Test case 1

  $import_filepath = SAMP_DIR.'tulips.256c.gif';
  
  $image = \GDImage\Factory::import( $import_filepath );
  // to data URI
  $data_uri = \GDImage\Factory::export( $image , 'data' );

  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$data_uri = \\GDImage\\Factory::export( $image , \'data\' );';
  
?>
  <h2>Test case 1: Image to data URI</h2>
  
  <pre><?php echo htmlspecialchars( $code ); ?></pre>
  
  <p><i>You will notice that the MIME Type is added and binary data encoded in base64 (default settings).</i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td>
        <pre style="overflow-x: scroll;"><?php echo implode( "\n" , str_split( $data_uri , ceil( strlen( $data_uri ) / 15 ) ) ); ?></pre>
        <b>To data URI</b>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php
  // Test case 2

  $data_uri = 'data:image/jpeg;base64,'.base64_encode( file_get_contents( SAMP_DIR.'chrysanthemum.jpg' ) );

  $image = \GDImage\Factory::import( $data_uri );
  
  // to file without extension
  $export_filepath = TMP_DIR.'00020_chrysanthemum';
  $export_filepath = \GDImage\Factory::export( $image , $export_filepath );

  $code = '$image = \\GDImage\\Factory::import( $data_uri );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath_without_extension );';
  
?>
  <h2>Test case 2: Data URI to image</h2>
  
  <pre><?php echo htmlspecialchars( $code ); ?></pre>
  
  <p><i>You will notice that an extension has been added to the final file path (".jpeg" for image/jpeg).</i></p>
  
  <table>
    <tr>
      <td>
        <pre style="overflow-x: scroll;"><?php echo implode( "\n" , str_split( $data_uri , ceil( strlen( $data_uri ) / 15 ) ) ); ?></pre>
        <b>Original data URI</b>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
          <figcaption>To file (guess extension)</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php
  // Test case 3

  $data_uri = 'data:,'.urlencode( file_get_contents( SAMP_DIR.'desert.24.png' ) );

  $image = \GDImage\Factory::import( $data_uri );
  
  // to file without extension
  $export_filepath = TMP_DIR.'00020_desert';
  $export_filepath = \GDImage\Factory::export( $image , $export_filepath );

  $code = '$image = \\GDImage\\Factory::import( $data_uri );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath_without_extension );';
  
?>
  <h2>Test case 3: Data URI without MIME Type and without base64 encoding</h2>
  
  <pre><?php echo htmlspecialchars( $code ); ?></pre>
  
  <p><i>You will notice that an extension has been added to the final file path (".png" for image/png).</i></p>
  
  <table>
    <tr>
      <td>
        <pre style="overflow-x: scroll;"><?php echo implode( "\n" , str_split( $data_uri , ceil( strlen( $data_uri ) / 15 ) ) ); ?></pre>
        <b>Original data URI</b>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
          <figcaption>To file (guess extension)</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php
  // Test case 3

  $data_uri = 'data:image/gif;base64,'.base64_encode( file_get_contents( SAMP_DIR.'lighthouse.24.png' ) );

  $image = \GDImage\Factory::import( $data_uri );
  
  // to file without extension
  $export_filepath = TMP_DIR.'00020_lighthouse';
  $export_filepath = \GDImage\Factory::export( $image , $export_filepath );

  $code = '$image = \\GDImage\\Factory::import( $data_uri );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath_without_extension );';
  
?>
  <h2>Test case 4: Data URI with incorrect MIME Type</h2>
  
  <pre><?php echo htmlspecialchars( $code ); ?></pre>
  
  <p><i>You will notice that the MIME Type from the data URI is prefered to a MIME Type detection on binary data.</i></p>
  
  <table>
    <tr>
      <td>
        <pre style="overflow-x: scroll;"><?php echo implode( "\n" , str_split( $data_uri , ceil( strlen( $data_uri ) / 15 ) ) ); ?></pre>
        <b>Original data URI</b>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
          <figcaption>To file (guess extension)</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php
  // Test case 5

  $import_filepath = SAMP_DIR.'hydrangeas.128c.8.png';
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  // to data URI
  $data_uri = \GDImage\Factory::export( $image , array( 'driver' => 'data' , 'base64' => false , 'mimetype' => true ) , 'image/jpeg' );

  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$data_uri = \\GDImage\\Factory::export( $image , array('."\n"
         .'  \'driver\' => \'data\' ,'."\n"
         .'  \'base64\' => false ,'."\n"
         .'  \'mimetype\' => true ,'."\n"
         .') , \'image/jpeg\' );';
  
?>
  <h2>Test case 5: Export as JPEG to data URI without base64 encoding but with MIME Type.</h2>
  
  <pre><?php echo htmlspecialchars( $code ); ?></pre>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td>
        <pre style="overflow-x: scroll;"><?php echo implode( "\n" , str_split( $data_uri , ceil( strlen( $data_uri ) / 15 ) ) ); ?></pre>
        <b>To data URI</b>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php
  // Test case 6

  $data_uri = 'data://image/png;base64,'.base64_encode( file_get_contents( SAMP_DIR.'jellyfish.16c.8.png' ) );

  $image = \GDImage\Factory::import( $data_uri );
  
  // to file without extension
  $export_filepath = TMP_DIR.'00020_jellyfish.png';
  $export_filepath = \GDImage\Factory::export( $image , $export_filepath );

  $code = '$image = \\GDImage\\Factory::import( $data_uri );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
?>
  <h2>Test case 6: <i>data://</i> PHP wrapper</h2>
  
  <pre><?php echo htmlspecialchars( $code ); ?></pre>
  
  <p><i>The library does not import this string helping stream wrapper. We prefer to treat it like a data URI.</i></p>
  
  <table>
    <tr>
      <td>
        <pre style="overflow-x: scroll;"><?php echo implode( "\n" , str_split( $data_uri , ceil( strlen( $data_uri ) / 15 ) ) ); ?></pre>
        <b>Original data URI</b>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
          <figcaption>To PNG</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php require '_foot.partial.php'; ?>
  
</body>
</html>
        
