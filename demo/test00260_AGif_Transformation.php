<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

?>
<html>
<head>
  <title>GDImage: Test 00260 - AGIF Transformation</title>
  <?php require '_head.partial.php'; ?>
  <style>
    small { white-space: nowrap; }
  </style>
</head>
<body>
  
  <h1>GDImage: Test 00260 - AGIF Transformation</h1>
  
  <p><i>Here comes interesting things: transform an animated GIF.</i></p>
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'agif/littlesvr_ca_1.gif';
  $export_filepath = TMP_DIR.'00260_littlesvr_ca_1';
  
  // import
  $start = microtime( true );
  ###

  // disable optimization
  \GDImage\Config::setAGifComposerOptimization( 0 ); 
  
  // import animated GIF
  $image = \GDImage\Factory::import( $import_filepath );
  
  // do multiple transform
  $transform = new \GDImage\Transform_Multiple();
  
  // add resizing
  $transform->add( new \GDImage\Transform_Resize_Crop( 200 , 200 ) );
  
  // add sepia
  $transform->add( new \GDImage\Transform_Sepia() );
  
  // apply multiple transform
  $image->apply( $transform );
  
  // export the animated GIF
  $final_filepath = \GDImage\Factory::export( $image , $export_filepath );
  
  ###
  $export_time = microtime( true ) - $start;

?>
  <h2>Test case 1: Contact (<?php echo $image->countFrames(); ?> frames)</h2>
  
  <pre>// disable optimization
\GDImage\Config::setAGifComposerOptimization( 0 );

// import animated GIF
$image = \GDImage\Factory::import( $import_filepath );

// do multiple transform
$transform = new \GDImage\Transform_Multiple();
  
// add resizing
$transform->add( new \GDImage\Transform_Resize_Crop( 200 , 200 ) );

// add sepia
$transform->add( new \GDImage\Transform_Sepia() );

// apply multiple transform
$image->apply( $transform );

// export the animated GIF
$final_filepath = \GDImage\Factory::export( $image , $export_filepath );</pre>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://littlesvr.ca/apng/gif_apng_webp4.html" target="_blank">littlesvr.ca</a><br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath); ?>" alt="<?php echo basename($final_filepath); ?>" title="<?php echo basename($final_filepath); ?>" />
          <figcaption>
            Result (<?php echo filesize_to_human( $final_filepath ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'agif/littlesvr_ca_2.gif';
  $export_filepath = TMP_DIR.'00260_littlesvr_ca_2';
  
  // import
  $start = microtime( true );
  set_time_limit( 60 );
  ###

  // disable optimization
  \GDImage\Config::setAGifComposerOptimization( 0 );
  
  // import animated GIF
  $image = \GDImage\Factory::import( $import_filepath );
  
  // apply custom filter
  $filter = new \GDImage\Transform_Filter( array(
    array( 1 , 0 , 0 , 0 ), // keep red
    array( 0 , 0 , 0 , 0 ), // remove green
    array( 0 , 0 , 0 , 0 ), // remove blue
    array( 0 , 0 , 0 , 1 ), // keep alpha
  ) );
  $image->apply( $filter );
  
  // export the animated GIF
  $final_filepath = \GDImage\Factory::export( $image , $export_filepath );
  
  ###
  $export_time = microtime( true ) - $start;

?>
  <h2>Test case 2: Blade Runner (<?php echo $image->countFrames(); ?> frames)</h2>
  
  <pre>// disable optimization
\GDImage\Config::setAGifComposerOptimization( 0 );

// import animated GIF
$image = \GDImage\Factory::import( $import_filepath );

// apply custom filter
$filter = new \GDImage\Transform_Filter( array(
  array( 1 , 0 , 0 , 0 ), // keep red
  array( 0 , 0 , 0 , 0 ), // remove green
  array( 0 , 0 , 0 , 0 ), // remove blue
  array( 0 , 0 , 0 , 1 ), // keep alpha
) );
$image->apply( $filter );

// export the animated GIF
$final_filepath = \GDImage\Factory::export( $image , $export_filepath );</pre>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://littlesvr.ca/apng/gif_apng_webp3.html" target="_blank">littlesvr.ca</a><br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath); ?>" alt="<?php echo basename($final_filepath); ?>" title="<?php echo basename($final_filepath); ?>" />
          <figcaption>
            Result (<?php echo filesize_to_human( $final_filepath ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 3

  $import_filepath = SAMP_DIR.'agif/glob_bargeo_fr.gif';
  $export_filepath = TMP_DIR.'00260_glob_bargeo_fr';
  
  // import
  $start = microtime( true );
  set_time_limit( 60 );
  ###

  // enable optimization
  \GDImage\Config::setAGifComposerOptimization( true );
  
  // import animated GIF
  $image = \GDImage\Factory::import( $import_filepath );
  
  // apply grayscale
  $image->apply( new \GDImage\Transform_Grayscale() );
  
  // apply negate
  $image->apply( new \GDImage\Transform_Negate() );
  
  // export the animated GIF
  $final_filepath = \GDImage\Factory::export( $image , $export_filepath );
  
  ###
  $export_time = microtime( true ) - $start;

?>
  <h2>Test case 3: Beetle (<?php echo $image->countFrames(); ?> frames)</h2>
  
  <pre>// enable optimization
\GDImage\Config::setAGifComposerOptimization( true ); 

// import animated GIF
$image = \GDImage\Factory::import( $import_filepath );
  
// apply grayscale
$image->apply( new \GDImage\Transform_Grayscale() );

// apply negate
$image->apply( new \GDImage\Transform_Negate() );

// export the animated GIF
$final_filepath = \GDImage\Factory::export( $image , $export_filepath );</pre>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://glob.bargeo.fr/" target="_blank">glob.bargeo.fr</a><br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath); ?>" alt="<?php echo basename($final_filepath); ?>" title="<?php echo basename($final_filepath); ?>" />
          <figcaption>
            Result (<?php echo filesize_to_human( $final_filepath ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
