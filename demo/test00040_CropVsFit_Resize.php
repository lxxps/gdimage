<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

$code1 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
        .'$transform = new \\GDImage\\Transform_Resize_Crop( 320 , 288 );'."\n"
        .'$image->apply( $transform );'."\n"
        .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

$code2 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
        .'$transform = new \\GDImage\\Transform_Resize_Crop( 288 , 320 );'."\n"
        .'$image->apply( $transform );'."\n"
        .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

$code3 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
        .'$transform = new \\GDImage\\Transform_Resize_Fit( 320 , 288 );'."\n"
        .'$image->apply( $transform );'."\n"
        .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

$code4 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
        .'$transform = new \\GDImage\\Transform_Resize_Fit( 288 , 320 );'."\n"
        .'$image->apply( $transform );'."\n"
        .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

$crop1 = new \GDImage\Transform_Resize_Crop( 288 , 320 );

$crop2 = new \GDImage\Transform_Resize_Crop( 320 , 288 );

$fit1 = new \GDImage\Transform_Resize_Fit( 288 , 320 );

$fit2 = new \GDImage\Transform_Resize_Fit( 320 , 288 );

?>
<html>
<head>
  <title>GDImage: Test 00040 - Crop resizing vs Fit resizing</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00040 - Crop resizing vs Fit resizing</h1>
  
  <pre><?php echo htmlspecialchars( $code1 ); ?></pre>
  <pre><?php echo htmlspecialchars( $code2 ); ?></pre>
  <pre><?php echo htmlspecialchars( $code3 ); ?></pre>
  <pre><?php echo htmlspecialchars( $code4 ); ?></pre>
  
  <hr />
  
<?php
  // Test case 1

  $import_filepath = SAMP_DIR.'chrysanthemum.jpg';
  $export_filepath = array( 
    'crop1' => TMP_DIR.'00040_chrysanthemum_a1' ,   
    'crop2' => TMP_DIR.'00040_chrysanthemum_a2' , 
    'fit1' => TMP_DIR.'00040_chrysanthemum_b1' ,   
    'fit2' => TMP_DIR.'00040_chrysanthemum_b2' ,  
  );
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepath as $key => &$path )
  {
    $clone = clone $image;
    $clone->apply( ${$key} );
    $path = \GDImage\Factory::export( $clone , $path );
  }
  
?>
  <h2>Test case 1: JPEG</h2>
  
  <table>
    <tr>
      <td colspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture <?php echo imagesize_to_human( $import_filepath ); ?></figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['crop1']); ?>" alt="<?php echo basename($export_filepath['crop1']); ?>" title="<?php echo basename($export_filepath['crop1']); ?>" />
          <figcaption>Crop to 288px &times; 320px</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['fit1']); ?>" alt="<?php echo basename($export_filepath['fit1']); ?>" title="<?php echo basename($export_filepath['fit1']); ?>" />
          <figcaption>
            Fit to 288px &times; 320px<br />
            <i>result to <?php echo imagesize_to_human( $export_filepath['fit1'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['crop2']); ?>" alt="<?php echo basename($export_filepath['crop2']); ?>" title="<?php echo basename($export_filepath['crop2']); ?>" />
          <figcaption>Crop to 320px &times; 288px</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['fit2']); ?>" alt="<?php echo basename($export_filepath['fit2']); ?>" title="<?php echo basename($export_filepath['fit2']); ?>" />
          <figcaption>
            Fit to 320px &times; 288px<br />
            <i>result to <?php echo imagesize_to_human( $export_filepath['fit2'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php
  // Test case 2

  $import_filepath = SAMP_DIR.'tulips.256c.gif';
  $export_filepath = array( 
    'crop1' => TMP_DIR.'00040_tulips_a1' ,   
    'crop2' => TMP_DIR.'00040_tulips_a2' , 
    'fit1' => TMP_DIR.'00040_tulips_b1' ,   
    'fit2' => TMP_DIR.'00040_tulips_b2' ,  
  );
  $export_transparent = array( 
    'crop1' => null ,   
    'crop2' => null , 
    'fit1' => null ,   
    'fit2' => null ,  
  );
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepath as $key => &$path )
  {
    $clone = clone $image;
    $clone->apply( ${$key} );
    $path = \GDImage\Factory::export( $clone , $path );
    $export_transparent[$key] = $clone->getResource()->getTransparent()->toArray();
  }
  
?>
  <h2>Test case 2: GIF with transparent color</h2>
  
  <p><i>You will notice that transparent color is preserved but has changed because of resampling.</i></p>
  
  <table>
    <tr>
      <td colspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture <?php echo imagesize_to_human( $import_filepath ); ?><br />
            <i>transparent color: <?php $t = $image->getResource()->getTransparent()->toArray(); array_unshift( $t , 'rgba(%d,%d,%d,%d)' ); echo call_user_func_array( 'sprintf' , $t ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['crop1']); ?>" alt="<?php echo basename($export_filepath['crop1']); ?>" title="<?php echo basename($export_filepath['crop1']); ?>" />
          <figcaption>
            Crop to 288px &times; 320px<br />
            <i>transparent color: <?php array_unshift( $export_transparent['crop1'] , 'rgba(%d,%d,%d,%d)' ); echo call_user_func_array( 'sprintf' , $export_transparent['crop1'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['fit1']); ?>" alt="<?php echo basename($export_filepath['fit1']); ?>" title="<?php echo basename($export_filepath['fit1']); ?>" />
          <figcaption>
            Fit to 288px &times; 320px<br />
            <i>transparent color: <?php array_unshift( $export_transparent['fit1'] , 'rgba(%d,%d,%d,%d)' ); echo call_user_func_array( 'sprintf' , $export_transparent['fit1'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['crop2']); ?>" alt="<?php echo basename($export_filepath['crop2']); ?>" title="<?php echo basename($export_filepath['crop2']); ?>" />
          <figcaption>
            Crop to 320px &times; 288px<br />
            <i>transparent color: <?php array_unshift( $export_transparent['crop2'] , 'rgba(%d,%d,%d,%d)' ); echo call_user_func_array( 'sprintf' , $export_transparent['crop2'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['fit2']); ?>" alt="<?php echo basename($export_filepath['fit2']); ?>" title="<?php echo basename($export_filepath['fit2']); ?>" />
          <figcaption>
            Fit to 320px &times; 288px<br />
            <i>transparent color: <?php array_unshift( $export_transparent['fit2'] , 'rgba(%d,%d,%d,%d)' ); echo call_user_func_array( 'sprintf' , $export_transparent['fit2'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php
  // Test case 3

  $import_filepath = SAMP_DIR.'hydrangeas.128c.8.png';
  $export_filepath = array( 
    'crop1' => TMP_DIR.'00040_hydrangeas_a1' ,   
    'crop2' => TMP_DIR.'00040_hydrangeas_a2' , 
    'fit1' => TMP_DIR.'00040_hydrangeas_b1' ,   
    'fit2' => TMP_DIR.'00040_hydrangeas_b2' ,  
  );
  $export_colors = array( 
    'crop1' => 0 ,   
    'crop2' => 0 , 
    'fit1' => 0 ,   
    'fit2' => 0 ,  
  );
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepath as $key => &$path )
  {
    $clone = clone $image;
    $clone->apply( ${$key} );
    $path = \GDImage\Factory::export( $clone , $path );
    $export_colors[$key] = $clone->getResource()->countColors();
  }
  
?>
  <h2>Test case 3: PNG8 with <?php echo $image->getResource()->countColors(); ?> colors</h2>
  
  <p><i>You will notice that the number of colors of the palette has been increased for a better result.</i></p>
  
  <table>
    <tr>
      <td colspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture <?php echo imagesize_to_human( $import_filepath ); ?><br />
            <i>number of colors: <?php echo $image->getResource()->countColors(); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['crop1']); ?>" alt="<?php echo basename($export_filepath['crop1']); ?>" title="<?php echo basename($export_filepath['crop1']); ?>" />
          <figcaption>
            Crop to 288px &times; 320px<br />
            <i>number of colors: <?php echo $export_colors['crop1']; ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['fit1']); ?>" alt="<?php echo basename($export_filepath['fit1']); ?>" title="<?php echo basename($export_filepath['fit1']); ?>" />
          <figcaption>
            Fit to 288px &times; 320px<br />
            <i>number of colors: <?php echo $export_colors['fit1']; ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['crop2']); ?>" alt="<?php echo basename($export_filepath['crop2']); ?>" title="<?php echo basename($export_filepath['crop2']); ?>" />
          <figcaption>
            Crop to 320px &times; 288px<br />
            <i>number of colors: <?php echo $export_colors['crop2']; ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['fit2']); ?>" alt="<?php echo basename($export_filepath['fit2']); ?>" title="<?php echo basename($export_filepath['fit2']); ?>" />
          <figcaption>
            Fit to 320px &times; 288px<br />
            <i>number of colors: <?php echo $export_colors['fit2']; ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php
  // Test case 5

  $import_filepath = SAMP_DIR.'jellyfish.16c.8.png';
  $export_filepath = array( 
    'crop1' => TMP_DIR.'00040_jellyfish_a1.jpg' ,   
    'crop2' => TMP_DIR.'00040_jellyfish_a2.jpg' , 
    'fit1' => TMP_DIR.'00040_jellyfish_b1.jpg' ,   
    'fit2' => TMP_DIR.'00040_jellyfish_b2.jpg' ,  
  );
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepath as $key => &$path )
  {
    $clone = clone $image;
    $clone->apply( ${$key} );
    $path = \GDImage\Factory::export( $clone , $path );
  }
  
?>
  <h2>Test case 4: PNG8 to JPEG</h2>
  
  <p><i>You will notice that transparent color is preserved without alpha channel.</i></p>
  
  <table>
    <tr>
      <td colspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture <?php echo imagesize_to_human( $import_filepath ); ?></figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['crop1']); ?>" alt="<?php echo basename($export_filepath['crop1']); ?>" title="<?php echo basename($export_filepath['crop1']); ?>" />
          <figcaption>Crop to 288px &times; 320px</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['fit1']); ?>" alt="<?php echo basename($export_filepath['fit1']); ?>" title="<?php echo basename($export_filepath['fit1']); ?>" />
          <figcaption>Fit to 288px &times; 320px</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['crop2']); ?>" alt="<?php echo basename($export_filepath['crop2']); ?>" title="<?php echo basename($export_filepath['crop2']); ?>" />
          <figcaption>Crop to 320px &times; 288px</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['fit2']); ?>" alt="<?php echo basename($export_filepath['fit2']); ?>" title="<?php echo basename($export_filepath['fit2']); ?>" />
          <figcaption>Fit to 320px &times; 288px</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php
  // Test case 5

  $import_filepath = SAMP_DIR.'lighthouse.24.png';
  $export_filepath = array( 
    'crop1' => TMP_DIR.'00040_lighthouse_a1' ,   
    'crop2' => TMP_DIR.'00040_lighthouse_a2' , 
    'fit1' => TMP_DIR.'00040_lighthouse_b1' ,   
    'fit2' => TMP_DIR.'00040_lighthouse_b2' ,  
  );
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepath as $key => &$path )
  {
    $clone = clone $image;
    $clone->apply( ${$key} );
    $path = \GDImage\Factory::export( $clone , $path );
  }
  
?>
  <h2>Test case 5: PNG24</h2>
  
  <table>
    <tr>
      <td colspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture <?php echo imagesize_to_human( $import_filepath ); ?></figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['crop1']); ?>" alt="<?php echo basename($export_filepath['crop1']); ?>" title="<?php echo basename($export_filepath['crop1']); ?>" />
          <figcaption>Crop to 288px &times; 320px</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['fit1']); ?>" alt="<?php echo basename($export_filepath['fit1']); ?>" title="<?php echo basename($export_filepath['fit1']); ?>" />
          <figcaption>Fit to 288px &times; 320px</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['crop2']); ?>" alt="<?php echo basename($export_filepath['crop2']); ?>" title="<?php echo basename($export_filepath['crop2']); ?>" />
          <figcaption>Crop to 320px &times; 288px</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['fit2']); ?>" alt="<?php echo basename($export_filepath['fit2']); ?>" title="<?php echo basename($export_filepath['fit2']); ?>" />
          <figcaption>Fit to 320px &times; 288px</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php require '_foot.partial.php'; ?>
  
</body>
</html>
        
