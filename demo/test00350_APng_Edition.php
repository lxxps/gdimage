<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

?>
<html>
<head>
  <title>GDImage: Test 00350 - APNG Edition</title>
  <?php require '_head.partial.php'; ?>
  <style>
    small { white-space: nowrap; }
  </style>
</head>
<body>
  
  <h1>GDImage: Test 00350 - APNG Edition</h1>
  
<?php /**/
  // Test case 1

  $frame_filepaths = array(
    SAMP_DIR.'chrysanthemum.jpg' ,
    SAMP_DIR.'desert.24.png' ,
    SAMP_DIR.'hydrangeas.128c.8.png' ,
    SAMP_DIR.'lighthouse.24.png' ,
    SAMP_DIR.'tulips.256c.gif' ,
  );
  $export_filepath = TMP_DIR.'00350_apng_2';
  
  // import
  $start = microtime( true );
  ###
  
  // disable optimization
  \GDImage\Config::setAPngComposerOptimization( 0 ); 
  
  // create empty APNG
  $image = new \GDImage\Image_APng();
  
  // add frames
  foreach( $frame_filepaths as $frame_filepath )
  {
    $image->addFrame( \GDImage\Factory::import( $frame_filepath ) );
  }
  
  // update Delay to 1s
  $image->getFrame( 0 )->setDelayNumerator( 1 );
  $image->getFrame( 0 )->setDelayDenominator( 1 );
  // set Dispose Option to "no"
  $image->getFrame( 0 )->setDisposeOption( 0 );
  // set Blend Option to "no"
  $image->getFrame( 0 )->setBlendOption( 0 );
  
  // update Delay to 1s
  $image->getFrame( 1 )->setDelayNumerator( 1 );
  $image->getFrame( 1 )->setDelayDenominator( 1 );
  // set Dispose Option to "previous"
  $image->getFrame( 1 )->setDisposeOption( 2 );
  // set Blend Option to "no"
  $image->getFrame( 1 )->setBlendOption( 0 );
  
  // update Delay to 1s
  $image->getFrame( 2 )->setDelayNumerator( 1 );
  $image->getFrame( 2 )->setDelayDenominator( 1 );
  // set Dispose Option to "previous"
  $image->getFrame( 2 )->setDisposeOption( 2 );
  // set Blend Option to "over"
  $image->getFrame( 2 )->setBlendOption( 1 );
  
  // update Delay to 1s
  $image->getFrame( 3 )->setDelayNumerator( 1 );
  $image->getFrame( 3 )->setDelayDenominator( 1 );
  // set Dispose Option to "no"
  $image->getFrame( 3 )->setDisposeOption( 0 );
  // set Blend Option to "over"
  $image->getFrame( 3 )->setBlendOption( 1 );
  
  // update Delay to 1s
  $image->getFrame( 4 )->setDelayNumerator( 1 );
  $image->getFrame( 4 )->setDelayDenominator( 1 );
  // set Dispose Option to "previous"
  $image->getFrame( 4 )->setDisposeOption( 2 );
  // set Blend Option to "over"
  $image->getFrame( 4 )->setBlendOption( 1 );
  
  // export the animated PNG
  $final_filepath = \GDImage\Factory::export( $image , $export_filepath );
  
  ###
  $export_time = microtime( true ) - $start;

?>
  <h2>Test case 1: APNG creation from different format</h2>
  
  <pre>// disable optimization
\GDImage\Config::setAPngComposerOptimization( 0 ); 

// create empty APNG
$image = new \GDImage\Image_APng();

// add frames
foreach( $frame_filepaths as $frame_filepath )
{
  $image->addFrame( \GDImage\Factory::import( $frame_filepath ) );
}

// update Delay to 1s
$image->getFrame( 0 )->setDelayNumerator( 1 );
$image->getFrame( 0 )->setDelayDenominator( 1 );
// set Dispose Option to "no"
$image->getFrame( 0 )->setDisposeOption( 0 );
// set Blend Option to "no"
$image->getFrame( 0 )->setBlendOption( 0 );

// update Delay to 1s
$image->getFrame( 1 )->setDelayNumerator( 1 );
$image->getFrame( 1 )->setDelayDenominator( 1 );
// set Dispose Option to "previous"
$image->getFrame( 1 )->setDisposeOption( 2 );
// set Blend Option to "no"
$image->getFrame( 1 )->setBlendOption( 0 );

// update Delay to 1s
$image->getFrame( 2 )->setDelayNumerator( 1 );
$image->getFrame( 2 )->setDelayDenominator( 1 );
// set Dispose Option to "previous"
$image->getFrame( 2 )->setDisposeOption( 2 );
// set Blend Option to "over"
$image->getFrame( 2 )->setBlendOption( 1 );

// update Delay to 1s
$image->getFrame( 3 )->setDelayNumerator( 1 );
$image->getFrame( 3 )->setDelayDenominator( 1 );
// set Dispose Option to "no"
$image->getFrame( 3 )->setDisposeOption( 0 );
// set Blend Option to "over"
$image->getFrame( 3 )->setBlendOption( 1 );

// update Delay to 1s
$image->getFrame( 4 )->setDelayNumerator( 1 );
$image->getFrame( 4 )->setDelayDenominator( 1 );
// set Dispose Option to "previous"
$image->getFrame( 4 )->setDisposeOption( 2 );
// set Blend Option to "over"
$image->getFrame( 4 )->setBlendOption( 1 );

// export the animated PNG
$final_filepath = \GDImage\Factory::export( $image , $export_filepath );</pre>
  
  <p><i>
    Observe interactions beetween Dispose Option and Blend Option: the last frame show the first frame behind fourth one, wow!
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($frame_filepaths[0]); ?>" alt="<?php echo basename($frame_filepaths[0]); ?>" title="<?php echo basename($frame_filepaths[0]); ?>" />
          <figcaption>
            Original picture (<?php echo strtoupper( pathinfo( $frame_filepaths[0] , PATHINFO_EXTENSION ) ); ?>)<br />
            Dispose Option 0 (do not dispose)<br />
            Blend Option 0 (no blend)<br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($frame_filepaths[1]); ?>" alt="<?php echo basename($frame_filepaths[1]); ?>" title="<?php echo basename($frame_filepaths[1]); ?>" />
          <figcaption>
            Original picture (<?php echo strtoupper( pathinfo( $frame_filepaths[1] , PATHINFO_EXTENSION ) ); ?>)<br />
            Dispose Option 2 (restore to previous)<br />
            Blend Option 0 (no blend)<br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($frame_filepaths[2]); ?>" alt="<?php echo basename($frame_filepaths[2]); ?>" title="<?php echo basename($frame_filepaths[2]); ?>" />
          <figcaption>
            Original picture (<?php echo strtoupper( pathinfo( $frame_filepaths[2] , PATHINFO_EXTENSION ) ); ?>)<br />
            Dispose Option 2 (restore to previous)<br />
            Blend Option 1 (blend over)<br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($frame_filepaths[3]); ?>" alt="<?php echo basename($frame_filepaths[3]); ?>" title="<?php echo basename($frame_filepaths[3]); ?>" />
          <figcaption>
            Original picture (<?php echo strtoupper( pathinfo( $frame_filepaths[0] , PATHINFO_EXTENSION ) ); ?>)<br />
            Dispose Option 0 (do not dispose)<br />
            Blend Option 1 (blend over)<br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($frame_filepaths[4]); ?>" alt="<?php echo basename($frame_filepaths[4]); ?>" title="<?php echo basename($frame_filepaths[4]); ?>" />
          <figcaption>
            Original picture (<?php echo strtoupper( pathinfo( $frame_filepaths[0] , PATHINFO_EXTENSION ) ); ?>)<br />
            Dispose Option 2 (restore to previous)<br />
            Blend Option 1 (blend over)<br />
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td colspan="<?php echo count($frame_filepaths); ?>">
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath); ?>" alt="<?php echo basename($final_filepath); ?>" title="<?php echo basename($final_filepath); ?>" />
          <figcaption>
            Result (<?php echo filesize_to_human( $final_filepath ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'motions/shroomery_org.jpg';
  $export_filepath = TMP_DIR.'00350_shroomery_org';
  
  // import
  $start = microtime( true );
  ###

  // create APNG
  $image = new \GDImage\Image_APng();

  // import source
  $src = \GDImage\Factory::import( $import_filepath );

  // this motion has 9 frames of 143px x 100px
  for( $i = 0; $i < 9; $i++ )
  {
    // create a frame helping APng_Factory
    $frame = \GDImage\APng_Factory::frame();

    // create blank resource
    $frame->setResource( $src->getResource()->blank( 143 , 100 ) );
    
    // copy
    imagecopy( $frame->getResource()->getGdResource() , $src->getResource()->getGdResource() , 0 , 0 , 143 * $i , 0 , 143 , 100 );
    
    // finally add frame
    $image->addFrame( $frame );
  }

  // then export
  $final_filepath = \GDImage\Factory::export( $image , $export_filepath );
  
  ###
  $export_time = microtime( true ) - $start;

?>
  <h2>Test case 2: APNG from motion</h2>
  
  <pre>// create APNG
$image = new \GDImage\Image_APng();

// import source
$src = \GDImage\Factory::import( $import_filepath );

// this motion has 9 frames of 143px x 100px
for( $i = 0; $i < 9; $i++ )
{
  // create a frame helping APng_Factory
  $frame = \GDImage\APng_Factory::frame();

  // create blank resource
  $frame->setResource( $src->getResource()->blank( 143 , 100 ) );

  // copy
  imagecopy( $frame->getResource()->getGdResource() , $src->getResource()->getGdResource() , 0 , 0 , 143 * $i , 0 , 143 , 100 );

  // finally add frame
  $image->addFrame( $frame );
}

// then export
$final_filepath = \GDImage\Factory::export( $image , $export_filepath );</pre>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/motions/<?php echo basename( $import_filepath ); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            from <a href="http://www.shroomery.org/ythan/motion.html" target="_blank">shroomery.org</a><br />
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath); ?>" alt="<?php echo basename($final_filepath); ?>" title="<?php echo basename($final_filepath); ?>" />
          <figcaption>
            Result (<?php echo filesize_to_human( $final_filepath ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 3

  $import_filepath = SAMP_DIR.'apng/tumblr_com.png';
  $frame_filepath = SAMP_DIR.'me.gif';
  $export_filepath = TMP_DIR.'00350_tumblr_com';
  
  // import
  $start = microtime( true );
  ###
  
  // disable optimization
  \GDImage\Config::setAPngComposerOptimization( 0 ); 
  
  // import APNG
  $image = \GDImage\Factory::import( $import_filepath );
  
  // insert the frame at third position
  // by default Delay Numerator is 0 (as fast as possible)
  // Delay Denominator is 100 (0.01s)
  // Dispose Option 1 (restore to background)
  // and Blend Option 0 (no blend)
  $image->addFrame( \GDImage\Factory::import( $frame_filepath ) , 2 );
  
  // we want to remove 10 frames and keep the same duration
  // fetch the frame
  $frame = $image->getFrame( 2 );
  
  // resize frame to fit image size, this is not automatic
  // on export if the frame is larger than first frame
  // the picture is cropped without resizing
  $frame->apply( new \GDImage\Transform_Resize_FitNFill( $image->getWidth() , $image->getHeight() ) );
  
  // go over next 10 frames
  // note that when we unset a frame, frame indexes are rearranged
  $i = 10;
  while( $i-- )
  {
    // on APNG, to keep same delay, we need to care about Delay Denominator
    $frame->setDelayNumerator( $frame->getDelayNumerator()
      + ( $frame->getDelayDenominator() * $image->getFrame( 3 )->getDelayNumerator() / $image->getFrame( 3 )->getDelayDenominator() ) );
    
    // unset next frame
    $image->unsetFrame( 3 );
  }
  
  // we also want the previous frame to restore to background
  $image->getFrame( 1 )->setDisposeOption( 2 );
  
  // export the APNG
  $final_filepath = \GDImage\Factory::export( $image , $export_filepath );
  
  ###
  $export_time = microtime( true ) - $start;

?>
  <h2>Test case 3: APNG frame insertion and deletion</h2>
  
  <pre>// disable optimization
\GDImage\Config::setAPngComposerOptimization( 0 ); 

// import APNG
$image = \GDImage\Factory::import( $import_filepath );

// insert the frame at third position
// by default Delay Numerator is 0 (as fast as possible)
// Delay Denominator is 100 (0.01s)
// Dispose Option 1 (restore to background)
// and Blend Option 0 (no blend)
$image->addFrame( \GDImage\Factory::import( $frame_filepath ) , 2 );

// we want to remove 10 frames and keep the same duration
// fetch the frame
$frame = $image->getFrame( 2 );

// resize frame to fit image size, this is not automatic
// on export if the frame is larger than first frame
// the picture is cropped without resizing
$frame->apply( new \GDImage\Transform_Resize_FitNFill( $image->getWidth() , $image->getHeight() ) );

// go over next 10 frames
// note that when we unset a frame, frame indexes are rearranged
$i = 10;
while( $i-- )
{
  // on APNG, to keep same delay, we need to care about Delay Denominator
  $frame->setDelayNumerator( $frame->getDelayNumerator()
    + ( $frame->getDelayDenominator() * $image->getFrame( 3 )->getDelayNumerator() / $image->getFrame( 3 )->getDelayDenominator() ) );

  // unset next frame
  $image->unsetFrame( 3 );
}

// we also want the previous frame to restore to background
$image->getFrame( 1 )->setDisposeOption( 2 );

// export the APNG
$final_filepath = \GDImage\Factory::export( $image , $export_filepath );</pre>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            from <a href="https://www.tumblr.com/tagged/apng" target="_blank">tumblr.com</a><br />
            <?php echo imagesize_to_human( $import_filepath ); ?><br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($frame_filepath); ?>" alt="<?php echo basename($frame_filepath); ?>" title="<?php echo basename($frame_filepath); ?>" />
          <figcaption>
            Inserted picture<br />
            <?php echo imagesize_to_human( $frame_filepath ); ?><br />
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath); ?>" alt="<?php echo basename($final_filepath); ?>" title="<?php echo basename($final_filepath); ?>" />
          <figcaption>
            Result (<?php echo filesize_to_human( $final_filepath ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 4

  $import_filepath = SAMP_DIR.'apng/apngcreator_2.png';
  $df_filepath = SAMP_DIR.'me.gif';
  $export_filepath = TMP_DIR.'00350_apngcreator_2';
  
  // import
  $start = microtime( true );
  ###
  
  // disable optimization
  \GDImage\Config::setAPngComposerOptimization( 0 ); 
  
  // import APNG
  $image = \GDImage\Factory::import( $import_filepath );
  
  // insert the frame as default image
  $df = \GDImage\Factory::import( $df_filepath );
  
  // resize default image to fit frames size, this is not automatic
  // on export the default image will determine the size of the APNG
  $transform = new \GDImage\Transform_Resize_FitNFill(); 
  $transform->setSize( $image->getWidth() , $image->getHeight() );
  $df->apply( $transform );
  
  // insert default image
  $image->setDefaultImage( $df );
  
  // export the APNG
  $final_filepath = \GDImage\Factory::export( $image , $export_filepath );
  
  ###
  $export_time = microtime( true ) - $start;

?>
  <h2>Test case 4: APNG default image</h2>

  <pre>// disable optimization
\GDImage\Config::setAPngComposerOptimization( 0 ); 

// import APNG
$image = \GDImage\Factory::import( $import_filepath );

// insert the frame as default image
$df = \GDImage\Factory::import( $df_filepath );

// resize default image to fit frames size, this is not automatic
// on export the default image will determine the size of the APNG
$transform = new \GDImage\Transform_Resize_FitNFill(); 
$transform->setSize( $image->getWidth() , $image->getHeight() );
$df->apply( $transform );

// insert default image
$image->setDefaultImage( $df );

// export the APNG
$final_filepath = \GDImage\Factory::export( $image , $export_filepath );</pre>
  
  <p><i>
    To see the default image, use Internet Explorer, Chrome or Opera.
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            from APNG_Creator<br />
            <?php echo imagesize_to_human( $import_filepath ); ?><br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($df_filepath); ?>" alt="<?php echo basename($df_filepath); ?>" title="<?php echo basename($df_filepath); ?>" />
          <figcaption>
            Default picture<br />
            <?php echo imagesize_to_human( $df_filepath ); ?><br />
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath); ?>" alt="<?php echo basename($final_filepath); ?>" title="<?php echo basename($final_filepath); ?>" />
          <figcaption>
            Result (<?php echo filesize_to_human( $final_filepath ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
