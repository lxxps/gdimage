<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';
  
$code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
       .'$transform = new \\GDImage\\Transform_Posterize( $nb );'."\n"
       .'$image->apply( $transform );'."\n"
       .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

?>
<html>
<head>
  <title>GDImage: Test 00130 - Posterize</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00130 - Posterize</h1>
  
  <pre><?php echo $code; ?></pre>
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'chrysanthemum.jpg';
  $export_filepaths = array(
    128 => TMP_DIR.'00130_chrysanthemum_128.jpg' ,
    64 => TMP_DIR.'00130_chrysanthemum_64.jpg' ,
    32 => TMP_DIR.'00130_chrysanthemum_32.jpg' ,
    16 => TMP_DIR.'00130_chrysanthemum_16.jpg' ,
    8 => TMP_DIR.'00130_chrysanthemum_8.jpg' ,
    4 => TMP_DIR.'00130_chrysanthemum_4.jpg' ,
  );
  $export_colors = array();
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepaths as $nb => $export_filepath )
  {
    $clone = clone $image;
    $clone->apply( new \GDImage\Transform_Posterize( $nb ) );
    \GDImage\Factory::export( $clone , $export_filepath );
    // log colors
    $export_colors[$nb] = $clone->getResource()->countColors();
  }
  
?>
  <h2>Test case 1: Posterize JPEG</h2>
  
  <p><i>Even if they are exported to JPEG, image resource were palette color resource.</i></p>
  
  <table>
    <tr>
      <td rowspan="2" colspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture
          </figcaption>
        </figure>
      </td>
      <?php $i = 1; foreach( $export_filepaths as $nb => $export_filepath ): ?>
        <?php if( $i === 4 ) print '</tr><tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
            <figcaption>
              To <?php echo $nb; ?> colors<br />
              <i>result to <?php echo $export_colors[$nb]; ?> colors</i>
            </figcaption>
          </figure>
        </td>
      <?php $i++; endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'tulips.256c.gif';
  $export_filepaths = array(
    128 => TMP_DIR.'00130_tulips_128.gif' ,
    64 => TMP_DIR.'00130_tulips_64.gif' ,
    32 => TMP_DIR.'00130_tulips_32.gif' ,
    16 => TMP_DIR.'00130_tulips_16.gif' ,
    8 => TMP_DIR.'00130_tulips_8.gif' ,
    4 => TMP_DIR.'00130_tulips_4.gif' ,
  );
  $export_colors = array();
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepaths as $nb => $export_filepath )
  {
    $clone = clone $image;
    $clone->apply( new \GDImage\Transform_Posterize( $nb ) );
    \GDImage\Factory::export( $clone , $export_filepath );
    // log colors
    $export_colors[$nb] = $clone->getResource()->countColors();
  }
  
?>
  <h2>Test case 2: Reducing GIF colors</h2>
  
  <p><i>Most of the time, GD is able to preserve transparency.</i></p>
  
  <table>
    <tr>
      <td rowspan="2" colspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            <i><?php echo $image->getResource()->countColors(); ?> colors</i>
          </figcaption>
        </figure>
      </td>
      <?php $i = 1; foreach( $export_filepaths as $nb => $export_filepath ): ?>
        <?php if( $i === 4 ) print '</tr><tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
            <figcaption>
              To <?php echo $nb; ?> colors<br />
              <i>result to <?php echo $export_colors[$nb]; ?> colors</i>
            </figcaption>
          </figure>
        </td>
      <?php $i++; endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 3

  $import_filepath = SAMP_DIR.'lighthouse.24.png';
  $export_filepaths = array(
    256 => TMP_DIR.'00130_lighthouse_256.png' ,
    128 => TMP_DIR.'00130_lighthouse_128.png' ,
    64 => TMP_DIR.'00130_lighthouse_64.png' ,
    32 => TMP_DIR.'00130_lighthouse_32.png' ,
    16 => TMP_DIR.'00130_lighthouse_16.png' ,
    8 => TMP_DIR.'00130_lighthouse_8.png' ,
  );
  $export_colors = array();
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepaths as $nb => $export_filepath )
  {
    $clone = clone $image;
    $clone->apply( new \GDImage\Transform_Posterize( $nb ) );
    \GDImage\Factory::export( $clone , $export_filepath );
    // log colors
    $export_colors[$nb] = $clone->getResource()->countColors();
  }
  
?>
  <h2>Test case 3: Posterize PNG24</h2>
  
  <p><i>
    Posterize PNG24 result to PNG8 picture.<br />
    You will notice that GD does not provide systematically the expected number of colors.<br />
  </i></p>
  
  <table>
    <tr>
      <td rowspan="2" colspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
          </figcaption>
        </figure>
      </td>
      <?php $i = 1; foreach( $export_filepaths as $nb => $export_filepath ): ?>
        <?php if( $i === 4 ) print '</tr><tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
            <figcaption>
              To <?php echo $nb; ?> colors<br />
              <i>result to <?php echo $export_colors[$nb]; ?> colors</i>
            </figcaption>
          </figure>
        </td>
      <?php $i++; endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
