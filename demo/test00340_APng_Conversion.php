<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

// disable optimization
\GDImage\Config::setAGifComposerOptimization( 0 ); 
\GDImage\Config::setAPngComposerOptimization( 0 ); 
  

?>
<html>
<head>
  <title>GDImage: Test 00340 - APNG Conversion</title>
  <?php require '_head.partial.php'; ?>
  <style>
    small { white-space: nowrap; }
  </style>
</head>
<body>
  
  <h1>GDImage: Test 00340 - APNG Conversion</h1>
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'apng/littlesvr_ca_1.png';
  
  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
  $export_filepath = array(
    'gif' => TMP_DIR.'00340_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.gif' , 
    'jpeg' => TMP_DIR.'00340_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.jpg' , 
  );
  
  $start = microtime( true );
  // import animated PNG
  $image = \GDImage\Factory::import( $import_filepath );
  
  // to JPEG
  \GDImage\Factory::export( $image , $export_filepath['jpeg'] );
  
  // to GIF
  \GDImage\Factory::export( $image , $export_filepath['gif'] );
  

?>
  <h2>Test case 1: MIME Type conversion</h2>
  
  <pre><?php echo $code ?></pre>
  
  <p><i>
    As expected, conversion of an APNG to GIF results to an animated GIF.
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://littlesvr.ca/apng/gif_apng_webp4.html" target="_blank">littlesvr.ca</a><br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['jpeg']); ?>" alt="<?php echo basename($export_filepath['jpeg']); ?>" title="<?php echo basename($export_filepath['jpeg']); ?>" />
          <figcaption>To JPEG (<?php echo filesize_to_human( $export_filepath['jpeg'] ); ?>)<br /><br /></figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['gif']); ?>" alt="<?php echo basename($export_filepath['gif']); ?>" title="<?php echo basename($export_filepath['gif']); ?>" />
          <figcaption>To GIF (<?php echo filesize_to_human( $export_filepath['gif'] ); ?>)<br /><br /></figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'apng/littlesvr_ca_2.png';
  
  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$single = \\GDImage\\Factory::instance( $mimetype ); // create a \\GDImage\\Image_Abstract_Single instance of expected MIME Type'."\n"
         .'$single->fromImage( $image ); // set resource from imported image'."\n"
         .'// clone to be sure that first AGIF frame will not be altered, also clone here to not clone all AGIF frames'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( clone $single , $export_filepath );';
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  $export_filepath = array(
    'gif' => TMP_DIR.'00340_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.gif' , 
    'png8' => TMP_DIR.'00340_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.8.png' , 
    'png24' => TMP_DIR.'00340_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.24.png' , 
    'jpeg' => TMP_DIR.'00340_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.jpg' , 
  );
  
  // to GIF
  $single = \GDImage\Factory::instance( 'image/gif' );
  $single->fromImage( $image );
  \GDImage\Factory::export( clone $single , $export_filepath['gif'] );
  
  // to PNG8
  $single = \GDImage\Factory::instance( 'image/png' );
  $single->fromImage( $image );
  \GDImage\Factory::export( clone $single , $export_filepath['png8'] );
  
  // to PNG24
  $single = \GDImage\Factory::instance( 'image/png' );
  $single->fromImage( $image );
  // from APNG with indexed color, we have to force true color
  \GDImage\Factory::export( clone $single , array(
    'driver' => 'file' ,
    'file' => $export_filepath['png24'] ,
    'truecolor' => true ,
  ) );
  
  // to JPEG
  $single = \GDImage\Factory::instance( 'image/jpeg' );
  $single->fromImage( $image );
  \GDImage\Factory::export( clone $single , $export_filepath['jpeg'] );
  
?>
  <h2>Test case 2: APNG to single</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    When an APNG is converted to single picture, we use the default image. In that case, the default image is part of the animation.<br />    
  </i></p>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://littlesvr.ca/apng/gif_apng_webp3.html" target="_blank">littlesvr.ca</a><br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['gif']); ?>" alt="<?php echo basename($export_filepath['gif']); ?>" title="<?php echo basename($export_filepath['gif']); ?>" />
          <figcaption>To GIF (<?php echo filesize_to_human( $export_filepath['gif'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png8']); ?>" alt="<?php echo basename($export_filepath['png8']); ?>" title="<?php echo basename($export_filepath['png8']); ?>" />
          <figcaption>To PNG8 (<?php echo filesize_to_human( $export_filepath['png8'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['jpeg']); ?>" alt="<?php echo basename($export_filepath['jpeg']); ?>" title="<?php echo basename($export_filepath['jpeg']); ?>" />
          <figcaption>To JPEG (<?php echo filesize_to_human( $export_filepath['jpeg'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png24']); ?>" alt="<?php echo basename($export_filepath['png24']); ?>" title="<?php echo basename($export_filepath['png24']); ?>" />
          <figcaption>To PNG24 (<?php echo filesize_to_human( $export_filepath['png24'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'apng/philip_html5_org_006.png';
  
  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$single = \\GDImage\\Factory::instance( $mimetype ); // create a \\GDImage\\Image_Abstract_Single instance of expected MIME Type'."\n"
         .'$single->fromImage( $image ); // set resource from imported image'."\n"
         .'// clone to be sure that first AGIF frame will not be altered, also clone here to not clone all AGIF frames'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( clone $single , $export_filepath );';
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  $export_filepath = array(
    'gif' => TMP_DIR.'00340_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.gif' , 
    'png8' => TMP_DIR.'00340_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.8.png' , 
    'png24' => TMP_DIR.'00340_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.24.png' , 
    'jpeg' => TMP_DIR.'00340_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.jpg' , 
  );
  
  // to GIF
  $single = \GDImage\Factory::instance( 'image/gif' );
  $single->fromImage( $image );
  \GDImage\Factory::export( clone $single , $export_filepath['gif'] );
  
  // to PNG8
  $single = \GDImage\Factory::instance( 'image/png' );
  $single->fromImage( $image );
  // from APNG with true color, we have to force palette color
  \GDImage\Factory::export( clone $single , array(
    'driver' => 'file' ,
    'file' => $export_filepath['png8'] ,
    'palettecolor' => true ,
  ) );
  
  // to PNG24
  $single = \GDImage\Factory::instance( 'image/png' );
  $single->fromImage( $image );
  \GDImage\Factory::export( clone $single , $export_filepath['png24'] );
  
  // to JPEG
  $single = \GDImage\Factory::instance( 'image/jpeg' );
  $single->fromImage( $image );
  \GDImage\Factory::export( clone $single , $export_filepath['jpeg'] );
  
?>
  <h2>Test case 2: APNG to single with default image</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    When an APNG is converted to single picture, we use the default image. In that case, the default image is not part of the animation.<br />
    To see the default image, use Internet Explorer, Chrome or Opera.    
  </i></p>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="https://philip.html5.org/tests/apng/tests.html" target="_blank">philip.html5.org</a><br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['gif']); ?>" alt="<?php echo basename($export_filepath['gif']); ?>" title="<?php echo basename($export_filepath['gif']); ?>" />
          <figcaption>To GIF (<?php echo filesize_to_human( $export_filepath['gif'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png8']); ?>" alt="<?php echo basename($export_filepath['png8']); ?>" title="<?php echo basename($export_filepath['png8']); ?>" />
          <figcaption>To PNG8 (<?php echo filesize_to_human( $export_filepath['png8'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['jpeg']); ?>" alt="<?php echo basename($export_filepath['jpeg']); ?>" title="<?php echo basename($export_filepath['jpeg']); ?>" />
          <figcaption>To JPEG (<?php echo filesize_to_human( $export_filepath['jpeg'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png24']); ?>" alt="<?php echo basename($export_filepath['png24']); ?>" title="<?php echo basename($export_filepath['png24']); ?>" />
          <figcaption>To PNG24 (<?php echo filesize_to_human( $export_filepath['png24'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
