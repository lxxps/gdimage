<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

// deactivate filter to speed up results
\GDImage\Config::setImagePngFiltersTrueColor( \PNG_FILTER_NONE );

?>
<html>
<head>
  <title>GDImage: Test 00110 - Flip and Mirror</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00110 - Flip and Mirror</h1>
  
  
<?php
  // Test case 1

  $import_filepath = SAMP_DIR.'tulips.256c.gif';
  $export_filepath = array(
    'v' => TMP_DIR.'00110_tulips_v.gif' ,
    'h' => TMP_DIR.'00110_tulips_h.gif' ,
    'vh' => TMP_DIR.'00110_tulips_vh.gif' ,
  );
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  $clone = clone $image;
  $clone->apply( new \GDImage\Transform_Flip( 'v' ) );
  \GDImage\Factory::export( $clone , $export_filepath['v'] );
  
  $clone = clone $image;
  $clone->apply( new \GDImage\Transform_Flip( 'h' ) );
  \GDImage\Factory::export( $clone , $export_filepath['h'] );
  
  $clone = clone $image;
  $clone->apply( new \GDImage\Transform_Flip( 'vh' ) );
  \GDImage\Factory::export( $clone , $export_filepath['vh'] );
  
  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$transform = new \\GDImage\\Transform_Flip( "v" | "h" | "vh" | "hv" );'."\n"
         .'$image->apply( $transform );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
?>
  <h2>Test case 1: Flip</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['v']); ?>" alt="<?php echo basename($export_filepath['v']); ?>" title="<?php echo basename($export_filepath['v']); ?>" />
          <figcaption>Vertical flip</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['h']); ?>" alt="<?php echo basename($export_filepath['h']); ?>" title="<?php echo basename($export_filepath['h']); ?>" />
          <figcaption>Horizontal flip</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['vh']); ?>" alt="<?php echo basename($export_filepath['vh']); ?>" title="<?php echo basename($export_filepath['vh']); ?>" />
          <figcaption>Both flip</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'desert.24.png';
  $export_filepath = array(
    'l' => TMP_DIR.'00110_desert_l.png' ,
    'r' => TMP_DIR.'00110_desert_r.png' ,
    't' => TMP_DIR.'00110_desert_t.png' ,
    'b' => TMP_DIR.'00110_desert_b.png' ,
  );
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  $clone = clone $image;
  $clone->apply( new \GDImage\Transform_Mirror( 'l' ) );
  \GDImage\Factory::export( $clone , $export_filepath['l'] );
  
  $clone = clone $image;
  $clone->apply( new \GDImage\Transform_Mirror( 'r' ) );
  \GDImage\Factory::export( $clone , $export_filepath['r'] );
  
  $clone = clone $image;
  $clone->apply( new \GDImage\Transform_Mirror( 't' ) );
  \GDImage\Factory::export( $clone , $export_filepath['t'] );
  
  $clone = clone $image;
  $clone->apply( new \GDImage\Transform_Mirror( 'b' ) );
  \GDImage\Factory::export( $clone , $export_filepath['b'] );
  
  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$transform = new \\GDImage\\Transform_Mirror( "l" | "r" | "t" | "b" );'."\n"
         .'$image->apply( $transform );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
?>
  <h2>Test case 2: Single mirror</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <table>
    <tr>
      <td rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['l']); ?>" alt="<?php echo basename($export_filepath['l']); ?>" title="<?php echo basename($export_filepath['l']); ?>" />
          <figcaption>Left mirror</figcaption>
        </figure>
      </td>
      <td rowspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['t']); ?>" alt="<?php echo basename($export_filepath['t']); ?>" title="<?php echo basename($export_filepath['t']); ?>" />
          <figcaption>Top mirror</figcaption>
        </figure>
      </td>
      <td rowspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['b']); ?>" alt="<?php echo basename($export_filepath['b']); ?>" title="<?php echo basename($export_filepath['b']); ?>" />
          <figcaption>Bottom mirror</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['r']); ?>" alt="<?php echo basename($export_filepath['r']); ?>" title="<?php echo basename($export_filepath['r']); ?>" />
          <figcaption>Right mirror</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 3

  $import_filepath = SAMP_DIR.'hydrangeas.128c.8.png';
  $export_filepath = array(
    'lr' => TMP_DIR.'00110_hydrangeas_lr.png' ,
    'tb' => TMP_DIR.'00110_hydrangeas_tb.png' ,
  );
  
  $image = \GDImage\Factory::import( $import_filepath );
  $mirror = new \GDImage\Transform_Mirror();
  
  $clone = clone $image;
  $mirror->setMode( \GDImage\Transform_Mirror::LEFT | \GDImage\Transform_Mirror::RIGHT );
  $clone->apply( $mirror );
  \GDImage\Factory::export( $clone , $export_filepath['lr'] );
  
  $clone = clone $image;
  $mirror->setMode( \GDImage\Transform_Mirror::TOP | \GDImage\Transform_Mirror::BOTTOM );
  $clone->apply( $mirror );
  \GDImage\Factory::export( $clone , $export_filepath['tb'] );
  
  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'// mode bits are: 1 for left, 2 for right, 4 for top, 8 for bottom'."\n"
         .'$transform = new \\GDImage\\Transform_Mirror( 3 | 12 );'."\n"
         .'$image->apply( $transform );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
?>
  <h2>Test case 3: Double mirror</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td colspan="3">
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['lr']); ?>" alt="<?php echo basename($export_filepath['lr']); ?>" title="<?php echo basename($export_filepath['lr']); ?>" />
          <figcaption>Left and right mirror</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['tb']); ?>" alt="<?php echo basename($export_filepath['tb']); ?>" title="<?php echo basename($export_filepath['tb']); ?>" />
          <figcaption>Top and bottom mirror</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 4

  $import_filepath = SAMP_DIR.'lighthouse.24.png';
  $export_filepath = array(
    'lt' => TMP_DIR.'00110_lighthouse_lt.png' ,
    'lb' => TMP_DIR.'00110_lighthouse_lb.png' ,
    'rt' => TMP_DIR.'00110_lighthouse_rt.png' ,
    'rb' => TMP_DIR.'00110_lighthouse_rb.png' ,
  );
  
  $image = \GDImage\Factory::import( $import_filepath );
  $mirror = new \GDImage\Transform_Mirror();
  
  $clone = clone $image;
  $mirror->setMode( \GDImage\Transform_Mirror::LEFT | \GDImage\Transform_Mirror::TOP );
  $clone->apply( $mirror );
  \GDImage\Factory::export( $clone , $export_filepath['lt'] );
  
  $clone = clone $image;
  $mirror->setMode( \GDImage\Transform_Mirror::LEFT | \GDImage\Transform_Mirror::BOTTOM );
  $clone->apply( $mirror );
  \GDImage\Factory::export( $clone , $export_filepath['lb'] );
  
  $clone = clone $image;
  $mirror->setMode( \GDImage\Transform_Mirror::RIGHT | \GDImage\Transform_Mirror::TOP );
  $clone->apply( $mirror );
  \GDImage\Factory::export( $clone , $export_filepath['rt'] );
  
  $clone = clone $image;
  $mirror->setMode( \GDImage\Transform_Mirror::RIGHT | \GDImage\Transform_Mirror::BOTTOM );
  $clone->apply( $mirror );
  \GDImage\Factory::export( $clone , $export_filepath['rb'] );
  
  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'// mode bits are: 1 for left, 2 for right, 4 for top, 8 for bottom'."\n"
         .'$transform = new \\GDImage\\Transform_Mirror( 5 | 9 | 6 | 10 );'."\n"
         .'$image->apply( $transform );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
?>
  <h2>Test case 4: Quadruple mirror</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <table>
    <tr>
      <td rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['lt']); ?>" alt="<?php echo basename($export_filepath['lt']); ?>" title="<?php echo basename($export_filepath['lt']); ?>" />
          <figcaption>Left and top mirror</figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['rt']); ?>" alt="<?php echo basename($export_filepath['rt']); ?>" title="<?php echo basename($export_filepath['rt']); ?>" />
          <figcaption>Right and top mirror</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['lb']); ?>" alt="<?php echo basename($export_filepath['lb']); ?>" title="<?php echo basename($export_filepath['lb']); ?>" />
          <figcaption>Left and bottom mirror</figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['rb']); ?>" alt="<?php echo basename($export_filepath['rb']); ?>" title="<?php echo basename($export_filepath['rb']); ?>" />
          <figcaption>Right and bottom mirror</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 5

  $import_filepath = SAMP_DIR.'chrysanthemum.jpg';
  $export_filepath = TMP_DIR.'00110_chrysanthemum_lrtb.png';
  
  $image = \GDImage\Factory::import( $import_filepath );
  $image->apply( new \GDImage\Transform_Mirror( \PHP_INT_MAX ) ); // all
  \GDImage\Factory::export( $image , $export_filepath );
  
  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$transform = new \\GDImage\\Transform_Mirror( \PHP_INT_MAX ); // all bits'."\n"
         .'$image->apply( $transform );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
?>
  <h2>Test case 4: All mirror</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
          <figcaption>All mirror</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
