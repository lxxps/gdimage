<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

$code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
       .'$final_filepath = array();'."\n"
       .'foreach( $image->getFrames() as $i => $frame )'."\n"
       .'{'."\n"
       .'  $final_filepath[] = \\GDImage\\Factory::export( $frame , $export_basepath.sprintf( \'%02d\' , $i ) );'."\n"
       .'}';
?>
<html>
<head>
  <title>GDImage: Test 00200 - AGIF Decomposition</title>
  <?php require '_head.partial.php'; ?>
  <style>
    small { white-space: nowrap; }
  </style>
</head>
<body>
  
  <h1>GDImage: Test 00200 - AGIF Decomposition</h1>
  
  <p><i>
    Some AGIF use a smart optimization to reduce file size: helping Disposal Method 1 (do not dispose), they overwrote only pixels that have changed.<br />
    Photoshop goes further and adjust the frame dimensions and position to not have to define areas that do not change.<br />
    On AGIF decomposition, we want each frames at a "visible" state: the optimization belong to the class that will create the AGIF.<br />
  </i></p>
  
  <pre><?php echo $code; ?></pre>
  
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'agif/ajaxload_info.gif';
  $export_basepath = TMP_DIR.'00200_ajaxload_info';
  
  // get pack to display usefull info
  $pack = \GDImage\AGif_Factory::unpack( $import_filepath );
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  $final_filepath = array();
  foreach( $image->getFrames() as $i => $frame )
  {
    $final_filepath[] = \GDImage\Factory::export( $frame , $export_basepath.sprintf( '%02d' , $i ) );
  }
  $export_time = microtime( true ) - $start;
  
  $nb_cols = 8;
?>
  <h2>Test case 1: Disposal Method 0 (no disposal specified) and Background Color</h2>
  
  <p><i>
    Use Global Color Table and Background Color.<br />
    Each frame fits Logical Screen Descriptor.<br />
  </i></p>
  
  <table>
    <tr>
      <td colspan="<?php echo $nb_cols; ?>">
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            from <a href="http://ajaxload.info/" target="_blank">ajaxload.info</a><br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
        <br />
        <small>
          Width: <?php echo $pack['Logical Screen Descriptor']['Logical Screen Width']; ?><br />
          Height: <?php echo $pack['Logical Screen Descriptor']['Logical Screen Height']; ?><br />
          Global Color Table: <?php echo $pack['Logical Screen Descriptor']['Packed']['Global Color Table Flag'] ? 'yes' : 'no'; ?><br />
          Number of Repetitions: <?php echo isset( $pack['Application Extension0']['Number of Repetitions'] ) ? ( $pack['Application Extension0']['Number of Repetitions'] ? $pack['Application Extension0']['Number of Repetitions'] : '&infin;' ) : 'none'; ?><br />
        </small>
      </td>
    </tr>
    <?php $i = 0; foreach( $final_filepath as $filepath ): ?>
      <?php $i % $nb_cols === 0 && print '<tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Frame <?php echo $i+1; ?><br />
            </figcaption>
          </figure>
          <br />
          <small>
            Width: <?php echo $pack['Image Descriptor'.$i]['Image Width']; ?><br />
            Height: <?php echo $pack['Image Descriptor'.$i]['Image Height']; ?><br />
            Left: <?php echo $pack['Image Descriptor'.$i]['Image Left Position']; ?><br />
            Top: <?php echo $pack['Image Descriptor'.$i]['Image Top Position']; ?><br />
            Local Color Table: <?php echo $pack['Image Descriptor'.$i]['Packed']['Local Color Table Flag'] ? 'yes' : 'no'; ?><br />
            <?php if( isset($pack['Graphic Control Extension'.$i]) ): ?>
              Delay Time: <?php echo $pack['Graphic Control Extension'.$i]['Delay Time']; ?><br />
              Disposal Method: <?php echo $pack['Graphic Control Extension'.$i]['Packed']['Disposal Method']; ?><br />
              Transparent Color: <?php echo $pack['Graphic Control Extension'.$i]['Packed']['Transparent Color Flag'] ? 'yes' : 'no'; ?><br />
            <?php endif; ?>
          </small>
        </td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endforeach; ?>
    <?php while( $i % $nb_cols !== 0 ): ?>
      <td></td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endwhile; ?>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'agif/photoshop.gif';
  $export_basepath = TMP_DIR.'00200_photoshop';
  
  // get pack to display usefull info
  $pack = \GDImage\AGif_Factory::unpack( $import_filepath );
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  $final_filepath = array();
  foreach( $image->getFrames() as $i => $frame )
  {
    $final_filepath[] = \GDImage\Factory::export( $frame , $export_basepath.sprintf( '%02d' , $i ) );
  }
  $export_time = microtime( true ) - $start;
  
  $nb_cols = 8;
?>
  <h2>Test case 2: Disposal Method 1 (do not dispose) and Logical Screen Descriptor mismatch</h2>
  
  <p><i>
    Use Global Color Table and Transparent Color for each frame.<br />
    Except for the first frame, other frames does not fit Logical Screen Descriptor (various Image Width, Image Height, Image Top Position, Image Left Position).<br />
    <!-- More information at <a href="http://www.w3.org/Graphics/GIF/spec-gif89a.txt" target="_blank">http://www.w3.org/Graphics/GIF/spec-gif89a.txt</a>. -->
  </i></p>
  
  <table>
    <tr>
      <td colspan="<?php echo $nb_cols; ?>">
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            created helping Photoshop<br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
        <br />
        <small>
          Width: <?php echo $pack['Logical Screen Descriptor']['Logical Screen Width']; ?><br />
          Height: <?php echo $pack['Logical Screen Descriptor']['Logical Screen Height']; ?><br />
          Global Color Table: <?php echo $pack['Logical Screen Descriptor']['Packed']['Global Color Table Flag'] ? 'yes' : 'no'; ?><br />
          Number of Repetitions: <?php echo isset( $pack['Application Extension0']['Number of Repetitions'] ) ? ( $pack['Application Extension0']['Number of Repetitions'] ? $pack['Application Extension0']['Number of Repetitions'] : '&infin;' ) : 'none'; ?><br />
        </small>
      </td>
    </tr>
    <?php $i = 0; foreach( $final_filepath as $filepath ): ?>
      <?php $i % $nb_cols === 0 && print '<tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Frame <?php echo $i+1; ?>
            </figcaption>
          </figure>
          <br />
          <small>
            Width: <?php echo $pack['Image Descriptor'.$i]['Image Width']; ?><br />
            Height: <?php echo $pack['Image Descriptor'.$i]['Image Height']; ?><br />
            Left: <?php echo $pack['Image Descriptor'.$i]['Image Left Position']; ?><br />
            Top: <?php echo $pack['Image Descriptor'.$i]['Image Top Position']; ?><br />
            Local Color Table: <?php echo $pack['Image Descriptor'.$i]['Packed']['Local Color Table Flag'] ? 'yes' : 'no'; ?><br />
            <?php if( isset($pack['Graphic Control Extension'.$i]) ): ?>
              Delay Time: <?php echo $pack['Graphic Control Extension'.$i]['Delay Time']; ?><br />
              Disposal Method: <?php echo $pack['Graphic Control Extension'.$i]['Packed']['Disposal Method']; ?><br />
              Transparent Color: <?php echo $pack['Graphic Control Extension'.$i]['Packed']['Transparent Color Flag'] ? 'yes' : 'no'; ?><br />
            <?php endif; ?>
          </small>
        </td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endforeach; ?>
    <?php while( $i % $nb_cols !== 0 ): ?>
      <td></td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endwhile; ?>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 3

  $import_filepath = SAMP_DIR.'agif/preloaders_net.gif';
  $export_basepath = TMP_DIR.'00200_preloaders_net';
  
  // get pack to display usefull info
  $pack = \GDImage\AGif_Factory::unpack( $import_filepath );
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  $final_filepath = array();
  foreach( $image->getFrames() as $i => $frame )
  {
    $final_filepath[] = \GDImage\Factory::export( $frame , $export_basepath.sprintf( '%02d' , $i ) );
  }
  $export_time = microtime( true ) - $start;
  
  $nb_cols = 8;
?>
  <h2>Test case 3: Disposal Method 2 (restore to background) and Transparent Color</h2>
  
  <p><i>
    Use Global Color Table and Transparent Color, with some frames with Local Color Table.<br />
    Each frame fits Logical Screen Descriptor.<br />
  </i></p>
  
  <table>
    <tr>
      <td colspan="<?php echo $nb_cols; ?>">
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            from <a href="http://preloaders.net/" target="_blank">preloaders.net</a><br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
        <br />
        <small>
          Width: <?php echo $pack['Logical Screen Descriptor']['Logical Screen Width']; ?><br />
          Height: <?php echo $pack['Logical Screen Descriptor']['Logical Screen Height']; ?><br />
          Global Color Table: <?php echo $pack['Logical Screen Descriptor']['Packed']['Global Color Table Flag'] ? 'yes' : 'no'; ?><br />
          Number of Repetitions: <?php echo isset( $pack['Application Extension0']['Number of Repetitions'] ) ? ( $pack['Application Extension0']['Number of Repetitions'] ? $pack['Application Extension0']['Number of Repetitions'] : '&infin;' ) : 'none'; ?><br />
        </small>
      </td>
    </tr>
    <?php $i = 0; foreach( $final_filepath as $filepath ): ?>
      <?php $i % $nb_cols === 0 && print '<tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Frame <?php echo $i+1; ?>
            </figcaption>
          </figure>
          <br />
          <small>
            Width: <?php echo $pack['Image Descriptor'.$i]['Image Width']; ?><br />
            Height: <?php echo $pack['Image Descriptor'.$i]['Image Height']; ?><br />
            Left: <?php echo $pack['Image Descriptor'.$i]['Image Left Position']; ?><br />
            Top: <?php echo $pack['Image Descriptor'.$i]['Image Top Position']; ?><br />
            Local Color Table: <?php echo $pack['Image Descriptor'.$i]['Packed']['Local Color Table Flag'] ? 'yes' : 'no'; ?><br />
            <?php if( isset($pack['Graphic Control Extension'.$i]) ): ?>
              Delay Time: <?php echo $pack['Graphic Control Extension'.$i]['Delay Time']; ?><br />
              Disposal Method: <?php echo $pack['Graphic Control Extension'.$i]['Packed']['Disposal Method']; ?><br />
              Transparent Color: <?php echo $pack['Graphic Control Extension'.$i]['Packed']['Transparent Color Flag'] ? 'yes' : 'no'; ?><br />
            <?php endif; ?>
          </small>
        </td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endforeach; ?>
    <?php while( $i % $nb_cols !== 0 ): ?>
      <td></td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endwhile; ?>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 4

  $import_filepath = SAMP_DIR.'agif/the-labs_com_c0.gif';
  $export_basepath = TMP_DIR.'00200_thelabs_com_c0';
  
  // get pack to display usefull info
  $pack = \GDImage\AGif_Factory::unpack( $import_filepath );
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  $final_filepath = array();
  foreach( $image->getFrames() as $i => $frame )
  {
    $final_filepath[] = \GDImage\Factory::export( $frame , $export_basepath.sprintf( '%02d' , $i ) );
  }
  $export_time = microtime( true ) - $start;
  
  $nb_cols = 5;
?>
  <h2>Test case 4: Disposal Method 0 (no disposal specified) and Logical Screen Descriptor mismatch</h2>
  
  <table>
    <tr>
      <td colspan="<?php echo $nb_cols; ?>">
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            from <a href="http://the-labs.com/GIFMerge/" target="_blank">the-labs.com</a><br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
        <br />
        <small>
          Width: <?php echo $pack['Logical Screen Descriptor']['Logical Screen Width']; ?><br />
          Height: <?php echo $pack['Logical Screen Descriptor']['Logical Screen Height']; ?><br />
          Global Color Table: <?php echo $pack['Logical Screen Descriptor']['Packed']['Global Color Table Flag'] ? 'yes' : 'no'; ?><br />
          Number of Repetitions: <?php echo isset( $pack['Application Extension0']['Number of Repetitions'] ) ? ( $pack['Application Extension0']['Number of Repetitions'] ? $pack['Application Extension0']['Number of Repetitions'] : '&infin;' ) : 'none'; ?><br />
        </small>
      </td>
    </tr>
    <?php $i = 0; foreach( $final_filepath as $filepath ): ?>
      <?php $i % $nb_cols === 0 && print '<tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Frame <?php echo $i+1; ?><br />
            </figcaption>
          </figure>
          <br />
          <small>
            Width: <?php echo $pack['Image Descriptor'.$i]['Image Width']; ?><br />
            Height: <?php echo $pack['Image Descriptor'.$i]['Image Height']; ?><br />
            Left: <?php echo $pack['Image Descriptor'.$i]['Image Left Position']; ?><br />
            Top: <?php echo $pack['Image Descriptor'.$i]['Image Top Position']; ?><br />
            Local Color Table: <?php echo $pack['Image Descriptor'.$i]['Packed']['Local Color Table Flag'] ? 'yes' : 'no'; ?><br />
            <?php if( isset($pack['Graphic Control Extension'.$i]) ): ?>
              Delay Time: <?php echo $pack['Graphic Control Extension'.$i]['Delay Time']; ?><br />
              Disposal Method: <?php echo $pack['Graphic Control Extension'.$i]['Packed']['Disposal Method']; ?><br />
              Transparent Color: <?php echo $pack['Graphic Control Extension'.$i]['Packed']['Transparent Color Flag'] ? 'yes' : 'no'; ?><br />
            <?php endif; ?>
          </small>
        </td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endforeach; ?>
    <?php while( $i % $nb_cols !== 0 ): ?>
      <td></td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endwhile; ?>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 5

  $import_filepath = SAMP_DIR.'agif/gifology_m6.gif';
  $export_basepath = TMP_DIR.'00200_gifology_m6';
  
  // get pack to display usefull info
  $pack = \GDImage\AGif_Factory::unpack( $import_filepath );
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  $final_filepath = array();
  foreach( $image->getFrames() as $i => $frame )
  {
    $final_filepath[] = \GDImage\Factory::export( $frame , $export_basepath.sprintf( '%02d' , $i ) );
  }
  $export_time = microtime( true ) - $start;
  
  $nb_cols = 6;
?>
  <h2>Test case 5: Disposal Method 1 (do not dispose) mixed with Disposal Method 2 (restore to background)</h2>
  
  <table>
    <tr>
      <td colspan="<?php echo $nb_cols; ?>">
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            from <a href="http://www.theimage.com/animation/pages/disposal3.html" target="_blank">theimage.com</a><br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
        <br />
        <small>
          Width: <?php echo $pack['Logical Screen Descriptor']['Logical Screen Width']; ?><br />
          Height: <?php echo $pack['Logical Screen Descriptor']['Logical Screen Height']; ?><br />
          Global Color Table: <?php echo $pack['Logical Screen Descriptor']['Packed']['Global Color Table Flag'] ? 'yes' : 'no'; ?><br />
          Number of Repetitions: <?php echo isset( $pack['Application Extension0']['Number of Repetitions'] ) ? ( $pack['Application Extension0']['Number of Repetitions'] ? $pack['Application Extension0']['Number of Repetitions'] : '&infin;' ) : 'none'; ?><br />
        </small>
      </td>
    </tr>
    <?php $i = 0; foreach( $final_filepath as $filepath ): ?>
      <?php $i % $nb_cols === 0 && print '<tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Frame <?php echo $i+1; ?>
            </figcaption>
          </figure>
          <br />
          <small>
            Width: <?php echo $pack['Image Descriptor'.$i]['Image Width']; ?><br />
            Height: <?php echo $pack['Image Descriptor'.$i]['Image Height']; ?><br />
            Left: <?php echo $pack['Image Descriptor'.$i]['Image Left Position']; ?><br />
            Top: <?php echo $pack['Image Descriptor'.$i]['Image Top Position']; ?><br />
            Local Color Table: <?php echo $pack['Image Descriptor'.$i]['Packed']['Local Color Table Flag'] ? 'yes' : 'no'; ?><br />
            <?php if( isset($pack['Graphic Control Extension'.$i]) ): ?>
              Delay Time: <?php echo $pack['Graphic Control Extension'.$i]['Delay Time']; ?><br />
              Disposal Method: <?php echo $pack['Graphic Control Extension'.$i]['Packed']['Disposal Method']; ?><br />
              Transparent Color: <?php echo $pack['Graphic Control Extension'.$i]['Packed']['Transparent Color Flag'] ? 'yes' : 'no'; ?><br />
            <?php endif; ?>
          </small>
        </td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endforeach; ?>
    <?php while( $i % $nb_cols !== 0 ): ?>
      <td></td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endwhile; ?>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 6

  $import_filepath = SAMP_DIR.'agif/gifology_m7.gif';
  $export_basepath = TMP_DIR.'00200_gifology_m7';
  
  // get pack to display usefull info
  $pack = \GDImage\AGif_Factory::unpack( $import_filepath );
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  $final_filepath = array();
  foreach( $image->getFrames() as $i => $frame )
  {
    $final_filepath[] = \GDImage\Factory::export( $frame , $export_basepath.sprintf( '%02d' , $i ) );
  }
  $export_time = microtime( true ) - $start;
  
  $nb_cols = 6;
?>
  <h2>Test case 6: Disposal Method 1 (do not dispose) mixed with Disposal Method 3 (restore to previous)</h2>
  
  <table>
    <tr>
      <td colspan="<?php echo $nb_cols; ?>">
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            from <a href="http://www.theimage.com/animation/pages/disposal3.html" target="_blank">theimage.com</a><br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
        <br />
        <small>
          Width: <?php echo $pack['Logical Screen Descriptor']['Logical Screen Width']; ?><br />
          Height: <?php echo $pack['Logical Screen Descriptor']['Logical Screen Height']; ?><br />
          Global Color Table: <?php echo $pack['Logical Screen Descriptor']['Packed']['Global Color Table Flag'] ? 'yes' : 'no'; ?><br />
          Number of Repetitions: <?php echo isset( $pack['Application Extension0']['Number of Repetitions'] ) ? ( $pack['Application Extension0']['Number of Repetitions'] ? $pack['Application Extension0']['Number of Repetitions'] : '&infin;' ) : 'none'; ?><br />
        </small>
      </td>
    </tr>
    <?php $i = 0; foreach( $final_filepath as $filepath ): ?>
      <?php $i % $nb_cols === 0 && print '<tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Frame <?php echo $i+1; ?>
            </figcaption>
          </figure>
          <br />
          <small>
            Width: <?php echo $pack['Image Descriptor'.$i]['Image Width']; ?><br />
            Height: <?php echo $pack['Image Descriptor'.$i]['Image Height']; ?><br />
            Left: <?php echo $pack['Image Descriptor'.$i]['Image Left Position']; ?><br />
            Top: <?php echo $pack['Image Descriptor'.$i]['Image Top Position']; ?><br />
            Local Color Table: <?php echo $pack['Image Descriptor'.$i]['Packed']['Local Color Table Flag'] ? 'yes' : 'no'; ?><br />
            <?php if( isset($pack['Graphic Control Extension'.$i]) ): ?>
              Delay Time: <?php echo $pack['Graphic Control Extension'.$i]['Delay Time']; ?><br />
              Disposal Method: <?php echo $pack['Graphic Control Extension'.$i]['Packed']['Disposal Method']; ?><br />
              Transparent Color: <?php echo $pack['Graphic Control Extension'.$i]['Packed']['Transparent Color Flag'] ? 'yes' : 'no'; ?><br />
            <?php endif; ?>
          </small>
        </td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endforeach; ?>
    <?php while( $i % $nb_cols !== 0 ): ?>
      <td></td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endwhile; ?>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
