<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

function colour_type_to_human( $i )
{
  switch( $i )
  {
    case 0: return 'greyscale';
    case 2: return 'truecolor';
    case 3: return 'indexed-color';
    case 4: return 'greyscale with alpha';
    case 6: return 'truecolor with alpha';
  }
}

?>
<html>
<head>
  <title>GDImage: Test 00320 - APNG Concrete Case</title>
  <?php require '_head.partial.php'; ?>
  <style>
    small { white-space: nowrap; }
  </style>
</head>
<body>
  
  <h1>GDImage: Test 00320 - APNG Concrete Case</h1>
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'apng/mozilla_org.png';
  $export_basepath = TMP_DIR.'00320_mozilla_org';
  
  // get pack to display usefull info
  $pack = \GDImage\APng_Factory::unpack( $import_filepath );
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  $final_filepath = array();
  if( $image->hasDefaultImage() )
  {
    $final_filepath[-1] = \GDImage\Factory::export( $image->getDefaultImage() , $export_basepath.'-1' );
  }
  foreach( $image->getFrames() as $i => $frame )
  {
    $final_filepath[] = \GDImage\Factory::export( $frame , $export_basepath.sprintf( '%02d' , $i ) );
  }
  $export_time = microtime( true ) - $start;
  
  $nb_cols = 7;

  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$final_filepath = array();'."\n"
         .'foreach( $image->getFrames() as $i => $frame )'."\n"
         .'{'."\n"
         .'  $final_filepath[] = \\GDImage\\Factory::export( $frame , $export_basepath.sprintf( \'%02d\' , $i ) );'."\n"
         .'}';
  
?>
  <h2>Test case 1: Decomposition</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    Use true color with alpha.<br />
    All frames fits IHDR width and height.<br />
  </i></p>
  
  <table>
    <tr>
      <td colspan="<?php echo $nb_cols; ?>">
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            from <a href="https://people.mozilla.org/~dolske/apng/demo.html" target="_blank">people.mozilla.org</a><br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
        <br />
        <small>
          Width: <?php echo $pack['IHDR']['Width']; ?><br />
          Height: <?php echo $pack['IHDR']['Height']; ?><br />
          Color type: <?php echo $pack['IHDR']['Colour type']; ?> <i>(<?php echo colour_type_to_human( $pack['IHDR']['Colour type'] ); ?>)</i><br />
          <?php if( isset( $pack['tRNS'] ) ): ?>
            <?php if( $pack['IHDR']['Colour type'] === 3 ): ?>
              Number of colors with alpha channel: <?php echo count($pack['tRNS']['Colors']); ?><br />
            <?php elseif( $pack['IHDR']['Colour type'] === 0 || $pack['IHDR']['Colour type'] === 2 ): ?>
              Transparent color: yes<br />
            <?php endif; ?>
          <?php endif; ?>
          Number of frames: <?php echo $pack['acTL']['Number of frames']; ?><br />
          Number of plays: <?php echo $pack['acTL']['Number of plays'] ? $pack['acTL']['Number of plays'] : '&infin;'; ?><br />
        </small>
      </td>
    </tr>
    <?php if( isset($final_filepath[-1]) ): $filepath = array_shift( $final_filepath ); ?>
      <tr>
        <td colspan="<?php echo $nb_cols; ?>">
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Default image
            </figcaption>
          </figure>
        </td>
      </tr>
    <?php endif; ?>
    <?php $i = 0; foreach( $final_filepath as $filepath ): $ix = ( $i - isset($pack['fcTL-1']) ); ?>
      <?php $i % $nb_cols === 0 && print '<tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Frame <?php echo $i+1; ?><br />
            </figcaption>
          </figure>
          <br />
          <small>
            Width: <?php echo $pack['fcTL'.$ix]['Width']; ?><br />
            Height: <?php echo $pack['fcTL'.$ix]['Height']; ?><br />
            X-offset: <?php echo $pack['fcTL'.$ix]['X-offset']; ?><br />
            Y-offset: <?php echo $pack['fcTL'.$ix]['Y-offset']; ?><br />
            Delay numerator: <?php echo $pack['fcTL'.$ix]['Delay numerator']; ?><br />
            Delay denominator: <?php echo $pack['fcTL'.$ix]['Delay denominator']; ?><br />
            Dispose option: <?php echo $pack['fcTL'.$ix]['Dispose option']; ?><br />
            Blend option: <?php echo $pack['fcTL'.$ix]['Blend option']; ?><br />
          </small>
        </td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endforeach; ?>
    <?php while( $i % $nb_cols !== 0 ): ?>
      <td></td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endwhile; ?>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'apng/mozilla_org.png';
  $export_filepath1 = TMP_DIR.'00320_mozilla_org_a.png';
  $export_filepath2 = TMP_DIR.'00320_mozilla_org_b.png';
  $export_filepath3 = TMP_DIR.'00320_mozilla_org_c.png';
  
  // give me some time
  set_time_limit( 120 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Config::setAPngComposerOptimization( \GDImage\APng_Composer::OPTIMIZE_ALL );
  \GDImage\Factory::export( $image , $export_filepath1 );
  $export_time1 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Config::setAPngComposerOptimization( \GDImage\APng_Composer::OPTIMIZE_CROP );
  \GDImage\Factory::export( $image , $export_filepath2 );
  $export_time2 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Config::setAPngComposerOptimization( \GDImage\APng_Composer::OPTIMIZE_NO );
  \GDImage\Factory::export( $image , $export_filepath3 );
  $export_time3 = microtime( true ) - $start;
  
  $code = '\\GDImage\\Config::setAGifComposerOptimization( $optimize );'."\n"
         ."\n"
         .'$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'."\n";
?>
  <h2>Test case 2: Optimization</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    Dispose Option to 1 (restore to background) and Blend Option to 0 (no blend) make optimization unefficient: we can only crop some transparent border pixels on a few frames.<br />
    You will notice that even without opimization, the animated picture is smaller than the original one. This is due to PNG filter usage that &mdash; in that case &mdash; reduce IDAT size.<br />
  </i></p>
  
  <table>
    <tr>
      <td rowspan="2">
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="https://people.mozilla.org/~dolske/apng/demo.html" target="_blank">people.mozilla.org</a><br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath1); ?>" alt="<?php echo basename($export_filepath1); ?>" title="<?php echo basename($export_filepath1); ?>" />
          <figcaption>
            Full optimization (<?php echo filesize_to_human( $export_filepath1 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time1 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath2); ?>" alt="<?php echo basename($export_filepath2); ?>" title="<?php echo basename($export_filepath2); ?>" />
          <figcaption>
            Crop optimization (<?php echo filesize_to_human( $export_filepath2 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time2 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath3); ?>" alt="<?php echo basename($export_filepath3); ?>" title="<?php echo basename($export_filepath3); ?>" />
          <figcaption>
            No optimization (<?php echo filesize_to_human( $export_filepath3 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time3 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 3

  $import_filepath = SAMP_DIR.'apng/littlesvr_ca_5.png';
  $export_basepath = TMP_DIR.'00320_littlesvr_ca_5.png';
  
  // get pack to display usefull info
  $pack = \GDImage\APng_Factory::unpack( $import_filepath );
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  $final_filepath = array();
  if( $image->hasDefaultImage() )
  {
    $final_filepath[-1] = \GDImage\Factory::export( $image->getDefaultImage() , $export_basepath.'-1' );
  }
  foreach( $image->getFrames() as $i => $frame )
  {
    $final_filepath[] = \GDImage\Factory::export( $frame , $export_basepath.sprintf( '%02d' , $i ) );
  }
  $export_time = microtime( true ) - $start;
  
  $nb_cols = 6;

  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$final_filepath = array();'."\n"
         .'foreach( $image->getFrames() as $i => $frame )'."\n"
         .'{'."\n"
         .'  $final_filepath[] = \\GDImage\\Factory::export( $frame , $export_basepath.sprintf( \'%02d\' , $i ) );'."\n"
         .'}';
  
?>
  <h2>Test case 3: Decomposition</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    Use indexed-color with one transparent color.<br />
    Except first frame, frames does not fit IHDR Width and Height.<br />
    Optimization done helping Dispose Option to 0 (do not dispose) or 2 (restore to previous) and Blend Option to 1 (over).<br />
    You will also notice some strange 1&times;1 frames: they are fully transparent and seems to be used for a kind of "temporization"&hellip;<br />
  </i></p>
  
  <table>
    <tr>
      <td colspan="<?php echo $nb_cols; ?>">
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            from <a href="http://littlesvr.ca/apng/gif_apng_webp5.html" target="_blank">littlesvr.ca</a><br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
        <br />
        <small>
          Width: <?php echo $pack['IHDR']['Width']; ?><br />
          Height: <?php echo $pack['IHDR']['Height']; ?><br />
          Color type: <?php echo $pack['IHDR']['Colour type']; ?> <i>(<?php echo colour_type_to_human( $pack['IHDR']['Colour type'] ); ?>)</i><br />
          <?php if( isset( $pack['tRNS'] ) ): ?>
            <?php if( $pack['IHDR']['Colour type'] === 3 ): ?>
              Number of colors with alpha channel: <?php echo count($pack['tRNS']['Colors']); ?><br />
            <?php elseif( $pack['IHDR']['Colour type'] === 0 || $pack['IHDR']['Colour type'] === 2 ): ?>
              Transparent color: yes<br />
            <?php endif; ?>
          <?php endif; ?>
          Number of frames: <?php echo $pack['acTL']['Number of frames']; ?><br />
          Number of plays: <?php echo $pack['acTL']['Number of plays'] ? $pack['acTL']['Number of plays'] : '&infin;'; ?><br />
        </small>
      </td>
    </tr>
    <?php if( isset($final_filepath[-1]) ): $filepath = array_shift( $final_filepath ); ?>
      <tr>
        <td colspan="<?php echo $nb_cols; ?>">
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Default image
            </figcaption>
          </figure>
        </td>
      </tr>
    <?php endif; ?>
    <?php $i = 0; foreach( $final_filepath as $filepath ): $ix = ( $i - isset($pack['fcTL-1']) ); ?>
      <?php $i % $nb_cols === 0 && print '<tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Frame <?php echo $i+1; ?><br />
            </figcaption>
          </figure>
          <br />
          <small>
            Width: <?php echo $pack['fcTL'.$ix]['Width']; ?><br />
            Height: <?php echo $pack['fcTL'.$ix]['Height']; ?><br />
            X-offset: <?php echo $pack['fcTL'.$ix]['X-offset']; ?><br />
            Y-offset: <?php echo $pack['fcTL'.$ix]['Y-offset']; ?><br />
            Delay numerator: <?php echo $pack['fcTL'.$ix]['Delay numerator']; ?><br />
            Delay denominator: <?php echo $pack['fcTL'.$ix]['Delay denominator']; ?><br />
            Dispose option: <?php echo $pack['fcTL'.$ix]['Dispose option']; ?><br />
            Blend option: <?php echo $pack['fcTL'.$ix]['Blend option']; ?><br />
          </small>
        </td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endforeach; ?>
    <?php while( $i % $nb_cols !== 0 ): ?>
      <td></td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endwhile; ?>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 4

  $import_filepath = SAMP_DIR.'apng/littlesvr_ca_5.png';
  $export_filepath1 = TMP_DIR.'00320_littlesvr_ca_5_a.png';
  $export_filepath2 = TMP_DIR.'00320_littlesvr_ca_5_b.png';
  $export_filepath3 = TMP_DIR.'00320_littlesvr_ca_5_c.png';
  
  // give me some time
  set_time_limit( 120 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Config::setAPngComposerOptimization( \GDImage\APng_Composer::OPTIMIZE_ALL );
  \GDImage\Factory::export( $image , $export_filepath1 );
  $export_time1 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Config::setAPngComposerOptimization( \GDImage\APng_Composer::OPTIMIZE_CROP );
  \GDImage\Factory::export( $image , $export_filepath2 );
  $export_time2 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Config::setAPngComposerOptimization( \GDImage\APng_Composer::OPTIMIZE_NO );
  \GDImage\Factory::export( $image , $export_filepath3 );
  $export_time3 = microtime( true ) - $start;
  
  $code = '\\GDImage\\Config::setAGifComposerOptimization( $optimize );'."\n"
         ."\n"
         .'$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'."\n";
?>
  <h2>Test case 4: Optimization</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    On that picture, everything is done to make all optimizations applicable: indexed color usage, transparent color available, Dispose Option to 0 or 2 and Blend Option to 1.<br />
    You will notice that even with full opimization, the animated picture is bigger than the original one. This is probably due to PNG filters usage that &mdash; in that case &mdash; increase IDAT size.<br />
    On indexed color, determining optimal PNG filters is a very long way: even order of colors in the palette can optimize filters usage.<br />
    For more information about PNG filters, see <a href="http://www.smashingmagazine.com/2009/07/15/clever-png-optimization-techniques/" target="_blank">http://www.smashingmagazine.com/2009/07/15/clever-png-optimization-techniques/</a>.
  </i></p>
  
  <table>
    <tr>
      <td rowspan="2">
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://littlesvr.ca/apng/gif_apng_webp5.html" target="_blank">littlesvr.ca</a><br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath1); ?>" alt="<?php echo basename($export_filepath1); ?>" title="<?php echo basename($export_filepath1); ?>" />
          <figcaption>
            Full optimization (<?php echo filesize_to_human( $export_filepath1 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time1 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath2); ?>" alt="<?php echo basename($export_filepath2); ?>" title="<?php echo basename($export_filepath2); ?>" />
          <figcaption>
            Crop optimization (<?php echo filesize_to_human( $export_filepath2 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time2 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath3); ?>" alt="<?php echo basename($export_filepath3); ?>" title="<?php echo basename($export_filepath3); ?>" />
          <figcaption>
            No optimization (<?php echo filesize_to_human( $export_filepath3 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time3 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
