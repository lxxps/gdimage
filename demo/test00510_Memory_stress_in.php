<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

define( 'MAXTIME_TO_TRANSFORM' , 30 );
?>
<html>
<head>
  <title>GDImage: Test 00510 - Memory stress in</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00510 - Memory stress in</h1>
  
  <p><i>
    You will probably never see it in action, but GDImage is able to balance memory usage on GD resources.<br />
    This is to avoid Fatal Error on memory allocation above "memory_limit" INI setting, especially when this setting not alterable on the hosting solution.<br />
    GDImage has an internal memory limit (<code>\GDImage\Config::getMemoryHandlerLimit()</code>) to handle GD resources, the default value is 50% of "memory_limit" INI setting, within 6M and "memory_limit" - 16M.<br />
    Before to reach this limit, GD will balance GD resources (and only GD resources) to files and free up some memory for new allocation.<br />
    It is important to understand that we do not care about memory usage of PHP interpreter or anything around, but only about <b>approximative memory allocated to GD resources</b> by the PHP script.<br />
  </i></p>
  <p><i>
    The memory handler observe GD resource from image and never from resource.<br />
    So, if you create a new resource from scratch, the memory handler will not be aware of it and will never try to balance it.<br />
    Also, on transformation context, the resource to modify is not attached to the memory handler, so it will never try to balance it.<br />
  </i></p>
  
<?php /**/

?>
  <h2>Test case 1: Importing many images at the same time</h2>
  
  <p><i>
    Even with a limit of 4M, GDImage is able to manage many images simultaneously.<br />
    This test case has been realized with an unrealistic "memory_limit" INI setting of 32M.
  </i></p>
  
  <pre><?php 
  print sprintf( 'Memory limit: %s' , ini_get('memory_limit') )."\n";
    
  \GDImage\Config::setMemoryHandlerLimit( 4 * 1024 * 1024 );
  print sprintf( 'GD limit: %s' , \GDImage\MemoryHandler::format( \GDImage\Config::getMemoryHandlerLimit() ) )."\n"; 
  
  print sprintf( 'Initial memory usage: %s' , \GDImage\MemoryHandler::format( memory_get_usage( false ) ) )."\n";  
  print sprintf( 'Initial GD usage: %s' , \GDImage\MemoryHandler::format( \GDImage\MemoryHandler::usage() ) )."\n";
  print "\n";
  
  $files = glob( SAMP_DIR.'*.*' );
  // reduce glob to real file
  $images = array();
  for( $i = 0, $imax = count( $files ); $i < $imax; $i++  )
  {
    print sprintf( '%d.' , $i + 1 )."\n";
    print sprintf( 'Import image "%s" (%s)' , basename( $files[$i] ) , \GDImage\MemoryHandler::format( filesize( $files[$i] ) ) )."\n";
    
    $images[$i] = \GDImage\Factory::import( $files[$i] );

    print sprintf( 'Memory usage: %s' , \GDImage\MemoryHandler::format( memory_get_usage( false ) ) )."\n";
    print sprintf( 'Approximated GD usage: %s' , \GDImage\MemoryHandler::format( \GDImage\MemoryHandler::usage() ) )."\n";
    print "\n";
  }
  
  for( $i = 0, $imax = count( $files ); $i < $imax; $i++  )
  {
    print sprintf( '%d.' , $i + 1 )."\n";
    print sprintf( 'Apply grayscale transformation' )."\n";
    
    set_time_limit( \MAXTIME_TO_TRANSFORM );
    $images[$i]->apply( new \GDImage\Transform_Grayscale() );
    
    print sprintf( 'Memory usage: %s' , \GDImage\MemoryHandler::format( memory_get_usage( false ) ) )."\n";
    print sprintf( 'Approximated GD usage: %s' , \GDImage\MemoryHandler::format( \GDImage\MemoryHandler::usage() ) )."\n";
    print "\n";
    
    print sprintf( 'Apply fit \'n\' fill transformation' )."\n";
    
    set_time_limit( \MAXTIME_TO_TRANSFORM );
    $images[$i]->apply( new \GDImage\Transform_Resize_FitNFill( 200 , 200 , 0 ) );
    
    print sprintf( 'Memory usage: %s' , \GDImage\MemoryHandler::format( memory_get_usage( false ) ) )."\n";
    print sprintf( 'Approximated GD usage: %s' , \GDImage\MemoryHandler::format( \GDImage\MemoryHandler::usage() ) )."\n";
    print "\n";
  }
  
  for( $i = 0, $imax = count( $files ); $i < $imax; $i++  )
  {
    print sprintf( '%d.' , $i + 1 )."\n";
    print sprintf( 'Export to "%s"' , '00510_'.basename( $files[$i] ) )."\n";
    
    \GDImage\Factory::export( $images[$i] , TMP_DIR.'00510_'.basename( $files[$i] ) );
    
    print sprintf( 'Memory usage: %s' , \GDImage\MemoryHandler::format( memory_get_usage( false ) ) )."\n";
    print sprintf( 'Approximated GD usage: %s' , \GDImage\MemoryHandler::format( \GDImage\MemoryHandler::usage() ) )."\n";
    print "\n";
  }
  
  unset( $images );
  print sprintf( 'Memory usage after unset: %s' , \GDImage\MemoryHandler::format( memory_get_usage( false ) ) )."\n";
  print sprintf( 'GD usage after unset: %s' , \GDImage\MemoryHandler::format( \GDImage\MemoryHandler::usage() ) )."\n";
  ?></pre>
  
  <table>
    <tr>
      <?php foreach( $files as $file ): ?>
        <td>
          <figure>
            <img src="./samples/<?php echo basename($file); ?>" alt="<?php echo basename($file); ?>" title="<?php echo basename($file); ?>" />
            <figcaption>
              Original picture
            </figcaption>
          </figure>
        </td>
      <?php endforeach; ?>
    </tr>
    <tr>
      <?php foreach( $files as $file ): ?>
        <td>
          <figure>
            <img src="./tmp/00510_<?php echo basename($file); ?>" alt="<?php echo '00510_'.basename($file); ?>" title="<?php echo '00510_'.basename($file); ?>" />
            <figcaption>
              Result
            </figcaption>
          </figure>
        </td>
      <?php endforeach; ?>
    </tr>
  </table>
  
  <?php unset( $files, $file, $i , $imax ); ?>
  
  <hr />
  
<?php /**/
?>
  
  <h2>Test case 2: Manipulating animated picture</h2>
  
  <p><i>
    Even with a limit of 12M, GDImage is able to manage 75 frames of a single animated PNG.<br />
    This test case has been realized with an unrealistic "memory_limit" INI setting of 32M.
  </i></p>
  
  <pre><?php 
  print sprintf( 'Memory limit: %s' , ini_get('memory_limit') )."\n";
    
  \GDImage\Config::setMemoryHandlerLimit( 12 * 1024 * 1024 );
  print sprintf( 'GD limit: %s' , \GDImage\MemoryHandler::format( \GDImage\Config::getMemoryHandlerLimit() ) )."\n";
  
  print sprintf( 'Initial memory usage: %s' , \GDImage\MemoryHandler::format( memory_get_usage( false ) ) )."\n";
  print sprintf( 'Initial GD usage: %s' , \GDImage\MemoryHandler::format( \GDImage\MemoryHandler::usage() ) )."\n";
  print "\n";
    
  $file = SAMP_DIR.'apng/littlesvr_ca_2.png';
  print sprintf( 'Import image "%s" (%s)' , $file , \GDImage\MemoryHandler::format( filesize( $file ) ) )."\n";
  
  $image = \GDImage\Factory::import( $file );
  
  print sprintf( 'Memory usage: %s' , \GDImage\MemoryHandler::format( memory_get_usage( false ) ) )."\n";
  print sprintf( 'Approximated GD usage: %s' , \GDImage\MemoryHandler::format( \GDImage\MemoryHandler::usage() ) )."\n";
  print "\n";
  
  print sprintf( 'Apply crop transformation' )."\n";
  
  $image->apply( new \GDImage\Transform_Resize_Crop( 200 , 200 ) );
  
  print sprintf( 'Memory usage: %s' , \GDImage\MemoryHandler::format( memory_get_usage( false ) ) )."\n";
  print sprintf( 'Approximated GD usage: %s' , \GDImage\MemoryHandler::format( \GDImage\MemoryHandler::usage() ) )."\n";
  print "\n";
  
  print sprintf( 'Apply negate transformation' )."\n";
  
  $image->apply( new \GDImage\Transform_Negate() );
  
  print sprintf( 'Memory usage: %s' , \GDImage\MemoryHandler::format( memory_get_usage( false ) ) )."\n";
  print sprintf( 'Approximated GD usage: %s' , \GDImage\MemoryHandler::format( \GDImage\MemoryHandler::usage() ) )."\n";
  print "\n";
  
  print sprintf( 'Export to "%s"' , '00510_'.basename($file) )."\n";
  
  \GDImage\Factory::export( $image , TMP_DIR.'00510_'.basename($file) );
  
  print sprintf( 'Memory usage: %s' , \GDImage\MemoryHandler::format( memory_get_usage( false ) ) )."\n";
  print sprintf( 'Approximated GD usage: %s' , \GDImage\MemoryHandler::format( \GDImage\MemoryHandler::usage() ) )."\n";
  print "\n";
  
  unset( $image );
  print sprintf( 'Memory usage after unset: %s' , \GDImage\MemoryHandler::format( memory_get_usage( false ) ) )."\n";
  print sprintf( 'GD usage after unset: %s' , \GDImage\MemoryHandler::format( \GDImage\MemoryHandler::usage() ) )."\n";
  
  ?></pre>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/apng/<?php echo basename($file); ?>" alt="<?php echo basename($file); ?>" title="<?php echo basename($file); ?>" />
          <figcaption>
            Original picture
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/00510_<?php echo basename($file); ?>" alt="<?php echo '00510_'.basename($file); ?>" title="<?php echo '00510_'.basename($file); ?>" />
          <figcaption>
            Result
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <?php unset( $file ); ?>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
