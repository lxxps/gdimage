<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

?>
<html>
<head>
  <title>GDImage: Test 00030 - Various import/export</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00030 - Various import/export</h1>
  
  <hr />
  
<?php
  // Test case 1

  $import_filepath = 'http://lorempixel.com/512/384/nature/1/lorempixel%20dot%20com/';
  
  $export_filepath = TMP_DIR.'00030_lorempixel';
  
  $image = \GDImage\Factory::import( $import_filepath );
  $export_filepath = \GDImage\Factory::export( $image , $export_filepath );

  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
?>
  <h2>Test case 1: Import with <i>http://</i> stream wrapper</h2>
  
  <p>
    <i>Some precisions about supported wrappers:<br />
    - ANY wrapper that allows reading can be used to import image;<br />
    - ANY wrapper that allows writing can be used to export image.<br />
    Note that custom stream wrappers are allowed.</i>
  </p>
  
  <pre><?php echo htmlspecialchars( $code ); ?></pre>
  
  <p>
    <i>You will notice that an extension has been added to the final file path (".jpeg" for image/jpeg).</i>
  </p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="<?php echo $import_filepath; ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
          <figcaption>To file (guess extension)</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php
  // Test case 2

  $data_64 = base64_encode( file_get_contents( SAMP_DIR.'hydrangeas.128c.8.png' ) );
  
  $export_filepath = TMP_DIR.'00030_hydrangeas';
  
  $image = \GDImage\Factory::import( $data_64 );
  $export_filepath = \GDImage\Factory::export( $image , $export_filepath );

  $code = '$image = \\GDImage\\Factory::import( $data_64 );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
?>
  <h2>Test case 2: Import from base64</h2>
  
  <pre><?php echo htmlspecialchars( $code ); ?></pre>
  
  <p><i>You will notice that an extension has been added to the final file path (".png" for image/png).</i></p>
  
  <table>
    <tr>
      <td>
        <pre style="overflow-x: scroll;"><?php echo implode( "\n" , str_split( $data_64 , ceil( strlen( $data_64 ) / 15 ) ) ); ?></pre>
        <b>Original base64 data</b>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
          <figcaption>To file (guess extension)</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php
  // Test case 3
  
  $import_filepath = SAMP_DIR.'desert.24.png';
  
  $image = \GDImage\Factory::import( $import_filepath );
  $data_64 = \GDImage\Factory::export( $image , 'base64' );

  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$data_64 = \\GDImage\\Factory::export( $image , \'base64\' );';
  
?>
  <h2>Test case 3: Export to base64</h2>
  
  <pre><?php echo htmlspecialchars( $code ); ?></pre>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td>
        <pre style="overflow-x: scroll;"><?php echo implode( "\n" , str_split( $data_64 , ceil( strlen( $data_64 ) / 15 ) ) ); ?></pre>
        <b>To base64</b>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php require '_foot.partial.php'; ?>
  
</body>
</html>
        
