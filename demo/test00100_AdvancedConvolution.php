<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

// for this test cases, especially because of alpha restoring
// we nedd some times one each transform
define( 'MAXTIME_TO_TRANSFORM' , 15 );

?>
<html>
<head>
  <title>GDImage: Test 00100 - Advanced convolution</title>
  <?php require '_head.partial.php'; ?>
  <style>
    img[width][height] {
      max-width: none;
      image-rendering: -moz-crisp-edges;         /* Firefox */
      image-rendering:   -o-crisp-edges;         /* Opera */
      image-rendering: -webkit-optimize-contrast;/* Webkit (non-standard naming) */
      image-rendering: crisp-edges;
      -ms-interpolation-mode: nearest-neighbor;  /* IE (non-standard property) */
    }
  </style>
</head>
<body>
  
  <h1>GDImage: Test 00100 - Advanced convolution</h1>
  
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'patterns/pattern.4c.24.png';
  $export_filepath = array(
    'extend' => array(
      'calculate' => TMP_DIR.'00100_pattern_aa' ,
      'preserve' => TMP_DIR.'00100_pattern_ab' ,
      'no' => TMP_DIR.'00100_pattern_ac' ,
    ) ,
    'wrap' => array(
      'calculate' => TMP_DIR.'00100_pattern_ba' ,
      'preserve' => TMP_DIR.'00100_pattern_bb' ,
      'no' => TMP_DIR.'00100_pattern_bc' ,
    ) ,
    'crop' => array(
      'calculate' => TMP_DIR.'00100_pattern_ca' ,
      'preserve' => TMP_DIR.'00100_pattern_cb' ,
      'no' => TMP_DIR.'00100_pattern_cc' ,
    ) ,
    'transparent' => array(
      'calculate' => TMP_DIR.'00100_pattern_da' ,
      'preserve' => TMP_DIR.'00100_pattern_db' ,
      'no' => TMP_DIR.'00100_pattern_dc' ,
    ) ,
  );
  
  $code1 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
          .'$transform = new \\GDImage\\Transform_Convolution_Advanced( array('."\n"
          .'  // shift to left'."\n"
          .'  array( 0 , 0 , 0 ) ,'."\n"
          .'  array( 0 , 0 , 1 ) ,'."\n"
          .'  array( 0 , 0 , 0 ) ,'."\n"
          .') );'."\n"
          .'$transform->setEdgeMode( \'extend\' | \'wrap\' | \'crop\' | \'transparent\' );'."\n"
          .'$transform->setAlphaMode( \'calculate\' | \'preserve\' | \'no\' );'."\n"
          .'$image->apply( $transform );'."\n"
          .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  
  $transform = new \GDImage\Transform_Convolution_Advanced( array(
    array( 0 , 0 , 0 ) ,
    array( 0 , 0 , 1 ) ,
    array( 0 , 0 , 0 ) ,
  ) );
      
  foreach( $export_filepath as $edge => &$subexport )
  {
    foreach( $subexport as $alpha => &$path )
    {
      $clone = clone $image;
      // give me some times to apply the transform
      set_time_limit( \MAXTIME_TO_TRANSFORM );
      $transform->setEdgeMode( $edge );
      $transform->setAlphaMode( $alpha );
      $clone->apply( $transform );
      $path = \GDImage\Factory::export( $clone , $path );
    }
  }
  
  $transform = new \GDImage\Transform_Convolution_Basic( array(
    array( 0 , 0 , 0 ) ,
    array( 0 , 0 , 1 ) ,
    array( 0 , 0 , 0 ) ,
  ) );
  $image->apply( $transform );
  $export_filepath['basic'] = \GDImage\Factory::export( $image , TMP_DIR.'00100_pattern_oo' );
  
  
?>
  <h2>Test case 1: Tiny PNG24</h2>
  
  <p><i>
    On big pictures, the difference with basic convolution may not clearly visible, so let's see them on a tiny PNG 24 picture.<br />
    <br />
    - Edge extend: The nearest border pixels are conceptually extended as far as necessary to provide values for the convolution (default);<br />
    - Edge wrap: The image is conceptually wrapped (or tiled) and values are taken from the opposite edge or corner;<br />
    - Edge crop: Any pixel in the output image which would require values from beyond the edge is skipped - cropped;<br />
    - Edge transparent: Any pixel in the output image which would require values from beyond the edge is replace by a transparent color.<br />
    <br />
    - Alpha calculate: Alpha channel is calculated helping convolution matrix where offset is divided by 2 (default);<br />
    - Alpha preserve: Alpha channel is preserved;<br />
    - Alpha no: Alpha channel is removed.<br />
  </i></p>
  
  <pre><?php echo $code1; ?></pre>
  
  <table>
    <tr>
      <td colspan="3">
        <figure>
          <img width="120" height="120" src="./samples/patterns/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td colspan="3">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['basic']); ?>" alt="<?php echo basename($export_filepath['basic']); ?>" title="<?php echo basename($export_filepath['basic']); ?>" />
          <figcaption>Basic convolution</figcaption>
        </figure>
        <br /><b><i>result to "extend" and "preserve"</b></i>
      </td>
    </tr>
    <?php /**/ ?>
    <tr>
      <td colspan="2">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['extend']['calculate']); ?>" alt="<?php echo basename($export_filepath['extend']['calculate']); ?>" title="<?php echo basename($export_filepath['extend']['calculate']); ?>" />
          <figcaption>
            Edge extend<br />
            Alpha calculate
          </figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['extend']['preserve']); ?>" alt="<?php echo basename($export_filepath['extend']['preserve']); ?>" title="<?php echo basename($export_filepath['extend']['preserve']); ?>" />
          <figcaption>
            Edge extend<br />
            Alpha preserve
          </figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['extend']['no']); ?>" alt="<?php echo basename($export_filepath['extend']['no']); ?>" title="<?php echo basename($export_filepath['extend']['no']); ?>" />
          <figcaption>
            Edge extend<br />
            Alpha no
          </figcaption>
        </figure>
      </td>
    </tr>
    <?php /**/ ?>
    <tr>
      <td colspan="2">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['wrap']['calculate']); ?>" alt="<?php echo basename($export_filepath['wrap']['calculate']); ?>" title="<?php echo basename($export_filepath['wrap']['calculate']); ?>" />
          <figcaption>
            Edge wrap<br />
            Alpha calculate
          </figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['wrap']['preserve']); ?>" alt="<?php echo basename($export_filepath['wrap']['preserve']); ?>" title="<?php echo basename($export_filepath['wrap']['preserve']); ?>" />
          <figcaption>
            Edge wrap<br />
            Alpha preserve
          </figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['wrap']['no']); ?>" alt="<?php echo basename($export_filepath['wrap']['no']); ?>" title="<?php echo basename($export_filepath['wrap']['no']); ?>" />
          <figcaption>
            Edge wrap<br />
            Alpha no
          </figcaption>
        </figure>
      </td>
    </tr>
    <?php /**/ ?>
    <tr>
      <td colspan="2">
        <figure>
          <img width="100" height="120" src="./tmp/<?php echo basename($export_filepath['crop']['calculate']); ?>" alt="<?php echo basename($export_filepath['crop']['calculate']); ?>" title="<?php echo basename($export_filepath['crop']['calculate']); ?>" />
          <figcaption>
            Edge crop<br />
            Alpha calculate
          </figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img width="100" height="120" src="./tmp/<?php echo basename($export_filepath['crop']['preserve']); ?>" alt="<?php echo basename($export_filepath['crop']['preserve']); ?>" title="<?php echo basename($export_filepath['crop']['preserve']); ?>" />
          <figcaption>
            Edge crop<br />
            Alpha preserve
          </figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img width="100" height="120" src="./tmp/<?php echo basename($export_filepath['crop']['no']); ?>" alt="<?php echo basename($export_filepath['crop']['no']); ?>" title="<?php echo basename($export_filepath['crop']['no']); ?>" />
          <figcaption>
            Edge crop<br />
            Alpha no
          </figcaption>
        </figure>
      </td>
    </tr>
    <?php /**/ ?>
    <tr>
      <td colspan="2">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['transparent']['calculate']); ?>" alt="<?php echo basename($export_filepath['transparent']['calculate']); ?>" title="<?php echo basename($export_filepath['transparent']['calculate']); ?>" />
          <figcaption>
            Edge transparent<br />
            Alpha calculate
          </figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['transparent']['preserve']); ?>" alt="<?php echo basename($export_filepath['transparent']['preserve']); ?>" title="<?php echo basename($export_filepath['transparent']['preserve']); ?>" />
          <figcaption>
            Edge transparent<br />
            Alpha preserve
          </figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['transparent']['no']); ?>" alt="<?php echo basename($export_filepath['transparent']['no']); ?>" title="<?php echo basename($export_filepath['transparent']['no']); ?>" />
          <figcaption>
            Edge transparent<br />
            Alpha no
          </figcaption>
        </figure>
      </td>
    </tr>
    <?php /**/ ?>
  </table>
  
  <hr />
  
<?php /**/

  $import_filepath = SAMP_DIR.'patterns/pattern.4c.gif';
  $export_filepath = array(
    'extend' => array(
      'calculate' => TMP_DIR.'00100_pattern_aa' ,
      'preserve' => TMP_DIR.'00100_pattern_ab' ,
      'no' => TMP_DIR.'00100_pattern_ac' ,
    ) ,
    'wrap' => array(
      'calculate' => TMP_DIR.'00100_pattern_ba' ,
      'preserve' => TMP_DIR.'00100_pattern_bb' ,
      'no' => TMP_DIR.'00100_pattern_bc' ,
    ) ,
    'crop' => array(
      'calculate' => TMP_DIR.'00100_pattern_ca' ,
      'preserve' => TMP_DIR.'00100_pattern_cb' ,
      'no' => TMP_DIR.'00100_pattern_cc' ,
    ) ,
    'transparent' => array(
      'calculate' => TMP_DIR.'00100_pattern_da' ,
      'preserve' => TMP_DIR.'00100_pattern_db' ,
      'no' => TMP_DIR.'00100_pattern_dc' ,
    ) ,
  );
  
  $code1 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
          .'$transform = new \\GDImage\\Transform_Convolution_Advanced( array('."\n"
          .'  // shift to top and right'."\n"
          .'  array( 0 , 0 , 0 ) ,'."\n"
          .'  array( 0 , 0 , 0 ) ,'."\n"
          .'  array( 1 , 0 , 0 ) ,'."\n"
          .') );'."\n"
          .'$transform->setEdgeMode( \'extend\' | \'wrap\' | \'crop\' | \'transparent\' );'."\n"
          .'$transform->setAlphaMode( \'calculate\' | \'preserve\' | \'no\' );'."\n"
          .'$image->apply( $transform );'."\n"
          .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  
  $transform = new \GDImage\Transform_Convolution_Advanced( array(
    array( 0 , 0 , 0 ) ,
    array( 0 , 0 , 0 ) ,
    array( 1 , 0 , 0 ) ,
  ) );
      
  foreach( $export_filepath as $edge => &$subexport )
  {
    foreach( $subexport as $alpha => &$path )
    {
      $clone = clone $image;
      // give me some times to apply the transform
      set_time_limit( \MAXTIME_TO_TRANSFORM );
      $transform->setEdgeMode( $edge );
      $transform->setAlphaMode( $alpha );
      $clone->apply( $transform );
      $path = \GDImage\Factory::export( $clone , $path );
    }
  }
  
  $transform = new \GDImage\Transform_Convolution_Basic( array(
    array( 0 , 0 , 0 ) ,
    array( 0 , 0 , 0 ) ,
    array( 1 , 0 , 0 ) ,
  ) );
  $image->apply( $transform );
  $export_filepath['basic'] = \GDImage\Factory::export( $image , TMP_DIR.'00100_pattern_oo' );
  
  
?>
  <h2>Test case 2: Tiny GIF with transparent color</h2>
  
  <p><i>
    You will notice that most of alpha calculations end to a loss of transparent color.<br />
    This is due to our process to convert true color to palette color resource with transparent color detection on colors with alpha channel.<br />
    On this "not real life" picture, pixels are too close to provide a good result on alpha restoration with <a href="http://php.net/manual/en/function.imagecolormatch.php" target="_blank">imagecolormatch()</a>.<br />
  </i></p>
  
  <pre><?php echo $code1; ?></pre>
  
  <table>
    <tr>
      <td colspan="3">
        <figure>
          <img width="120" height="120" src="./samples/patterns/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td colspan="3">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['basic']); ?>" alt="<?php echo basename($export_filepath['basic']); ?>" title="<?php echo basename($export_filepath['basic']); ?>" />
          <figcaption>Basic convolution</figcaption>
        </figure>
        <br /><b><i>result to "extend" and "preserve"</b></i>
      </td>
    </tr>
    <?php /**/ ?>
    <tr>
      <td colspan="2">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['extend']['calculate']); ?>" alt="<?php echo basename($export_filepath['extend']['calculate']); ?>" title="<?php echo basename($export_filepath['extend']['calculate']); ?>" />
          <figcaption>
            Edge extend<br />
            Alpha calculate
          </figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['extend']['preserve']); ?>" alt="<?php echo basename($export_filepath['extend']['preserve']); ?>" title="<?php echo basename($export_filepath['extend']['preserve']); ?>" />
          <figcaption>
            Edge extend<br />
            Alpha preserve
          </figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['extend']['no']); ?>" alt="<?php echo basename($export_filepath['extend']['no']); ?>" title="<?php echo basename($export_filepath['extend']['no']); ?>" />
          <figcaption>
            Edge extend<br />
            Alpha no
          </figcaption>
        </figure>
      </td>
    </tr>
    <?php /**/ ?>
    <tr>
      <td colspan="2">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['wrap']['calculate']); ?>" alt="<?php echo basename($export_filepath['wrap']['calculate']); ?>" title="<?php echo basename($export_filepath['wrap']['calculate']); ?>" />
          <figcaption>
            Edge wrap<br />
            Alpha calculate
          </figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['wrap']['preserve']); ?>" alt="<?php echo basename($export_filepath['wrap']['preserve']); ?>" title="<?php echo basename($export_filepath['wrap']['preserve']); ?>" />
          <figcaption>
            Edge wrap<br />
            Alpha preserve
          </figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['wrap']['no']); ?>" alt="<?php echo basename($export_filepath['wrap']['no']); ?>" title="<?php echo basename($export_filepath['wrap']['no']); ?>" />
          <figcaption>
            Edge wrap<br />
            Alpha no
          </figcaption>
        </figure>
      </td>
    </tr>
    <?php /**/ ?>
    <tr>
      <td colspan="2">
        <figure>
          <img width="100" height="100" src="./tmp/<?php echo basename($export_filepath['crop']['calculate']); ?>" alt="<?php echo basename($export_filepath['crop']['calculate']); ?>" title="<?php echo basename($export_filepath['crop']['calculate']); ?>" />
          <figcaption>
            Edge crop<br />
            Alpha calculate
          </figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img width="100" height="100" src="./tmp/<?php echo basename($export_filepath['crop']['preserve']); ?>" alt="<?php echo basename($export_filepath['crop']['preserve']); ?>" title="<?php echo basename($export_filepath['crop']['preserve']); ?>" />
          <figcaption>
            Edge crop<br />
            Alpha preserve
          </figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img width="100" height="100" src="./tmp/<?php echo basename($export_filepath['crop']['no']); ?>" alt="<?php echo basename($export_filepath['crop']['no']); ?>" title="<?php echo basename($export_filepath['crop']['no']); ?>" />
          <figcaption>
            Edge crop<br />
            Alpha no
          </figcaption>
        </figure>
      </td>
    </tr>
    <?php /**/ ?>
    <tr>
      <td colspan="2">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['transparent']['calculate']); ?>" alt="<?php echo basename($export_filepath['transparent']['calculate']); ?>" title="<?php echo basename($export_filepath['transparent']['calculate']); ?>" />
          <figcaption>
            Edge transparent<br />
            Alpha calculate
          </figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['transparent']['preserve']); ?>" alt="<?php echo basename($export_filepath['transparent']['preserve']); ?>" title="<?php echo basename($export_filepath['transparent']['preserve']); ?>" />
          <figcaption>
            Edge transparent<br />
            Alpha preserve
          </figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img width="120" height="120" src="./tmp/<?php echo basename($export_filepath['transparent']['no']); ?>" alt="<?php echo basename($export_filepath['transparent']['no']); ?>" title="<?php echo basename($export_filepath['transparent']['no']); ?>" />
          <figcaption>
            Edge transparent<br />
            Alpha no
          </figcaption>
        </figure>
      </td>
    </tr>
    <?php /**/ ?>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 3

  $import_filepath = SAMP_DIR.'chrysanthemum.jpg';
  $export_filepath = TMP_DIR.'00100_chrysanthemum';
  
  $code = 
  '$transform = new \\GDImage\\Transform_Convolution_Advanced( array('."\n"
 .'  // a simple blur'."\n"
 .'  array( 0 , 1 , 0 ) ,'."\n"
 .'  array( 1 , 4 , 1 ) ,'."\n"
 .'  array( 0 , 1 , 0 ) ,'."\n"
 .') , null , null , \'transparent\' );'."\n"
 ."\n"
 .'$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
 .'$image->apply( $transform );'."\n"
 .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'."\n";
  
  $transform = new \GDImage\Transform_Convolution_Advanced( array(
    array( 0 , 1 , 0 ) ,
    array( 1 , 4 , 1 ) ,
    array( 0 , 1 , 0 ) ,
  ) , null , null , 'transparent' );
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  // give me some times to apply the transform
  set_time_limit( \MAXTIME_TO_TRANSFORM );
  $image->apply( $transform );
  $export_filepath = \GDImage\Factory::export( $image , $export_filepath );
  
?>
  <h2>Test case 3: JPEG and edge transparent</h2>
  
  <pre><?php echo $code; ?></pre>
  <p><i>
    You will notice the thin black line arround picture, this is due to the default color value and JPEG format that do not store alpha channel.<br />
    To change this default color, use <code>\GDImage\Config::setColorDefaultValue( $color_representation );</code>.
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
          <figcaption>Custom blur</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 4

  $import_filepath = SAMP_DIR.'lighthouse.24.png';
  $export_filepath = TMP_DIR.'00100_lighthouse';
  
  $code = 
  '$transform = new \\GDImage\\Transform_Convolution_Advanced( array('."\n"
 .'  // a negative blur'."\n"
 .'  array( 0 , 1 , 0 ) ,'."\n"
 .'  array( 1 , 4 , 1 ) ,'."\n"
 .'  array( 0 , 1 , 0 ) ,'."\n"
 .') , -8 , 255 , null , \'calculate\' );'."\n"
 ."\n"
 .'$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
 .'$image->apply( $transform );'."\n"
 .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'."\n";
  
  $transform = new \GDImage\Transform_Convolution_Advanced( array(
    array( 0 , 1 , 0 ) ,
    array( 1 , 4 , 1 ) ,
    array( 0 , 1 , 0 ) ,
  ) , -8 , 255 , null , 'calculate' );
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  // give me some times to apply the transform
  set_time_limit( \MAXTIME_TO_TRANSFORM );
  $image->apply( $transform );
  $export_filepath = \GDImage\Factory::export( $image , $export_filepath );
  
?>
  <h2>Test case 4: PNG24 and alpha calculate with "negate" convolution</h2>
  
  <pre><?php echo $code; ?></pre>
  <p><i>
    With alpha calculate, the offset is also apply on alpha channel, that result to a "negation" of alpha channels.<br />
    So with alpha calculation on "negate" convolution, some pictures can result to one transparent color.<br />
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
          <figcaption>Negative custom blur</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 4

  $import_filepath = SAMP_DIR.'jellyfish.16c.8.png';
  $export_filepath = TMP_DIR.'00100_jellyfish';
  
  $code = 
  '$transform = new \\GDImage\\Transform_Convolution_Advanced( array('."\n"
 .'  // a simple blur'."\n"
 .'  array( 0 , 1 , 0 ) ,'."\n"
 .'  array( 1 , 4 , 1 ) ,'."\n"
 .'  array( 0 , 1 , 0 ) ,'."\n"
 .') , null , null , \'wrap\' , \'calculate\' );'."\n"
 ."\n"
 .'$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
 .'$image->apply( $transform );'."\n"
 .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'."\n";
  
  $transform = new \GDImage\Transform_Convolution_Advanced( array(
    array( 0 , 1 , 0 ) ,
    array( 1 , 4 , 1 ) ,
    array( 0 , 1 , 0 ) ,
  ) , null , null , 'wrap' , 'calculate' );
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  // give me some times to apply the transform
  set_time_limit( \MAXTIME_TO_TRANSFORM );
  $image->apply( $transform );
  $export_filepath = \GDImage\Factory::export( $image , $export_filepath );
  
?>
  <h2>Test case 5: PNG8, edge wrap and alpha calculate</h2>
  
  <pre><?php echo $code; ?></pre>
  <p><i>
    You will notice a dirty line on left that comes from edge wrap with alpha calculate.<br />
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
          <figcaption>Custom blur</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
