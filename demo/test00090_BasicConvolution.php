<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

//$code1 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
//        .'$image->apply( new \\GDImage\\Transform_Blur() );'."\n"
//        .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

$code1 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
        .'$image->apply( new \\GDImage\\Transform_GaussianBlur() );'."\n"
        .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

$code2 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
        .'$image->apply( new \\GDImage\\Transform_Negate() );'."\n"
        .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

$code3 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
        .'$image->apply( new \\GDImage\\Transform_Sharpen() );'."\n"
        .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

$code4 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
        .'// this transformation sharpen and negate the picture'."\n"
        .'$transform = new \\GDImage\\Transform_Convolution( array('."\n"
        .'  array( -1 , -1 , -1 ) ,'."\n"
        .'  array( -1 , 9 , -1 ) ,'."\n"
        .'  array( -1 , -1 , -1 ) ,'."\n"
        .') , -1 , 255 ); // matrix, divisor, offset'."\n"
        .'$image->apply( $transform );'."\n"
        .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

$blur = new \GDImage\Transform_Blur();
\GDImage\Transform_Collection::set( 'blur' , $blur );

$gaussianblur = new \GDImage\Transform_GaussianBlur();
\GDImage\Transform_Collection::set( 'gaussianblur' , $gaussianblur );

$negate = new \GDImage\Transform_Negate();
\GDImage\Transform_Collection::set( 'negate' , $negate );

$sharpen = new \GDImage\Transform_Sharpen();
\GDImage\Transform_Collection::set( 'sharpen' , $sharpen );

$custom = new \GDImage\Transform_Convolution( array(
  array( -1 , -1 , -1 ) ,
  array( -1 , 9 , -1 ) ,
  array( -1 , -1 , -1 ) ,
) , -1 , 255 );
\GDImage\Transform_Collection::set( 'custom' , $custom );

// for this test cases, especially because of alpha restoring
// we nedd some times one each transform
define( 'MAXTIME_TO_TRANSFORM' , 20 );

?>
<html>
<head>
  <title>GDImage: Test 00090 - Basic convolution</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00090 - Basic convolution</h1>
  
  <p><i>
    You will notice that convolution is a long process.<br />
    The only exception is on \GDImage\Transform_Negate() that can use <a href="http://php.net/manual/en/function.imagefilter.php" target="_blank">imagefilter()</a>.<br />
    Note that other convolution filters provide same leaks than <a href="http://php.net/manual/en/function.imageconvolution.php" target="_blank">imageconvolution()</a> so it is not a good idea to use them.<br />
    In our implementation of image convolution, the number of non-opaque pixels have direct impact on time to apply the convolution.<br />
  </i></p>
  
  <pre><?php echo htmlspecialchars( $code1 ); ?></pre>
  <pre><?php echo htmlspecialchars( $code2 ); ?></pre>
  <pre><?php echo htmlspecialchars( $code3 ); ?></pre>
  <pre><?php echo htmlspecialchars( $code4 ); ?></pre>
  
  <hr />
  
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'chrysanthemum.jpg';
  $export_filepath = array( 
    'gaussianblur' => TMP_DIR.'00090_chrysanthemum_a1' ,   
    'sharpen' => TMP_DIR.'00090_chrysanthemum_a2' , 
    'negate' => TMP_DIR.'00090_chrysanthemum_b1' ,   
    'custom' => TMP_DIR.'00090_chrysanthemum_b2' ,  
  );
  $export_time = array();
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepath as $key => &$path )
  {
    $clone = clone $image;
    
    // give me some times to apply the transform
    set_time_limit( \MAXTIME_TO_TRANSFORM );
    
    $start = microtime( true );
    $clone->apply( \GDImage\Transform_Collection::get( $key ) );
    $export_time[$key] = microtime( true ) - $start;
    
    $path = \GDImage\Factory::export( $clone , $path );
  }
  
?>
  <h2>Test case 1: JPEG</h2>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['gaussianblur']); ?>" alt="<?php echo basename($export_filepath['gaussianblur']); ?>" title="<?php echo basename($export_filepath['gaussianblur']); ?>" />
          <figcaption>
            Gaussian blur<br />
            <i>took <?php echo microtime_to_human( $export_time['gaussianblur'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['sharpen']); ?>" alt="<?php echo basename($export_filepath['sharpen']); ?>" title="<?php echo basename($export_filepath['sharpen']); ?>" />
          <figcaption>
            Sharpen<br />
            <i>took <?php echo microtime_to_human( $export_time['sharpen'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['negate']); ?>" alt="<?php echo basename($export_filepath['negate']); ?>" title="<?php echo basename($export_filepath['negate']); ?>" />
          <figcaption>
            Negate<br />
            <i>took <?php echo microtime_to_human( $export_time['negate'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['custom']); ?>" alt="<?php echo basename($export_filepath['custom']); ?>" title="<?php echo basename($export_filepath['custom']); ?>" />
          <figcaption>
            Custom<br />
            <i>took <?php echo microtime_to_human( $export_time['custom'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'lighthouse.24.png';
  $export_filepath = array( 
    'gaussianblur' => TMP_DIR.'00090_lighthouse_a1' ,   
    'sharpen' => TMP_DIR.'00090_lighthouse_a2' , 
    'negate' => TMP_DIR.'00090_lighthouse_b1' ,   
    'custom' => TMP_DIR.'00090_lighthouse_b2' ,  
  );
  $export_time = array();
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepath as $key => &$path )
  {
    $clone = clone $image;
    
    // give me some times to apply the transform
    set_time_limit( \MAXTIME_TO_TRANSFORM );
    
    $start = microtime( true );
    $clone->apply( \GDImage\Transform_Collection::get( $key ) );
    $export_time[$key] = microtime( true ) - $start;
    
    $path = \GDImage\Factory::export( $clone , $path );
  }
  
?>
  <h2>Test case 2: PNG24</h2>
  
  <p><i>
    
  </i></p>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['gaussianblur']); ?>" alt="<?php echo basename($export_filepath['gaussianblur']); ?>" title="<?php echo basename($export_filepath['gaussianblur']); ?>" />
          <figcaption>
            Gaussian blur<br />
            <i>took <?php echo microtime_to_human( $export_time['gaussianblur'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['sharpen']); ?>" alt="<?php echo basename($export_filepath['sharpen']); ?>" title="<?php echo basename($export_filepath['sharpen']); ?>" />
          <figcaption>
            Sharpen<br />
            <i>took <?php echo microtime_to_human( $export_time['sharpen'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['negate']); ?>" alt="<?php echo basename($export_filepath['negate']); ?>" title="<?php echo basename($export_filepath['negate']); ?>" />
          <figcaption>
            Negate<br />
            <i>took <?php echo microtime_to_human( $export_time['negate'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['custom']); ?>" alt="<?php echo basename($export_filepath['custom']); ?>" title="<?php echo basename($export_filepath['custom']); ?>" />
          <figcaption>
            Custom<br />
            <i>took <?php echo microtime_to_human( $export_time['custom'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 3

  $import_filepath = SAMP_DIR.'desert.24.png';
  $export_filepath = array( 
    'gaussianblur' => TMP_DIR.'00090_desert_a1' ,   
    'sharpen' => TMP_DIR.'00090_desert_a2' , 
    'negate' => TMP_DIR.'00090_desert_b1' ,   
    'custom' => TMP_DIR.'00090_desert_b2' ,  
  );
  $export_time = array();
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepath as $key => &$path )
  {
    $clone = clone $image;
    
    // give me some times to apply the transform
    set_time_limit( \MAXTIME_TO_TRANSFORM );
    
    $start = microtime( true );
    $clone->apply( \GDImage\Transform_Collection::get( $key ) );
    $export_time[$key] = microtime( true ) - $start;
    
    $path = \GDImage\Factory::export( $clone , $path );
  }
?>
  <h2>Test case 3: PNG24 with full alpha channel</h2>
  
  <p><i>
    You will notice that there is no more dirty pixels.<br />
  </i></p>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['gaussianblur']); ?>" alt="<?php echo basename($export_filepath['gaussianblur']); ?>" title="<?php echo basename($export_filepath['gaussianblur']); ?>" />
          <figcaption>
            Gaussian blur<br />
            <i>took <?php echo microtime_to_human( $export_time['gaussianblur'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['sharpen']); ?>" alt="<?php echo basename($export_filepath['sharpen']); ?>" title="<?php echo basename($export_filepath['sharpen']); ?>" />
          <figcaption>
            Sharpen<br />
            <i>took <?php echo microtime_to_human( $export_time['sharpen'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['negate']); ?>" alt="<?php echo basename($export_filepath['negate']); ?>" title="<?php echo basename($export_filepath['negate']); ?>" />
          <figcaption>
            Negate<br />
            <i>took <?php echo microtime_to_human( $export_time['negate'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['custom']); ?>" alt="<?php echo basename($export_filepath['custom']); ?>" title="<?php echo basename($export_filepath['custom']); ?>" />
          <figcaption>
            Custom<br />
            <i>took <?php echo microtime_to_human( $export_time['custom'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 4

  $import_filepath = SAMP_DIR.'tulips.256c.gif';
  $export_filepath = array( 
    'gaussianblur' => TMP_DIR.'00090_tulips_a1' ,   
    'sharpen' => TMP_DIR.'00090_tulips_a2' , 
    'negate' => TMP_DIR.'00090_tulips_b1' ,   
    'custom' => TMP_DIR.'00090_tulips_b2' ,  
  );
  $export_time = array();
  
  $export_transparent = array( 
    'gaussianblur' => null ,   
    'sharpen' => null , 
    'negate' => null ,   
    'custom' => null ,  
  );
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepath as $key => &$path )
  {
    $clone = clone $image;
    
    // give me some times to apply the transform
    set_time_limit( \MAXTIME_TO_TRANSFORM );
    
    $start = microtime( true );
    $clone->apply( \GDImage\Transform_Collection::get( $key ) );
    $export_time[$key] = microtime( true ) - $start;
    $export_transparent[$key] = $clone->getResource()->getTransparent()->toArray();
    
    $path = \GDImage\Factory::export( $clone , $path );
  }
  
?>
  <h2>Test case 4: GIF with transparent color</h2>
  
  <p><i>
    You will notice that the transparent color has changed but still close to the orginal one.<br />
  </i></p>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            <i>transparent color: <?php $t = $image->getResource()->getTransparent()->toArray(); array_unshift( $t , 'rgba(%d,%d,%d,%d)' ); echo call_user_func_array( 'sprintf' , $t ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['gaussianblur']); ?>" alt="<?php echo basename($export_filepath['gaussianblur']); ?>" title="<?php echo basename($export_filepath['gaussianblur']); ?>" />
          <figcaption>
            Gaussian blur<br />
            <i>took <?php echo microtime_to_human( $export_time['gaussianblur'] ); ?></i><br />
            <i>transparent color: <?php array_unshift( $export_transparent['gaussianblur'] , 'rgba(%d,%d,%d,%d)' ); echo call_user_func_array( 'sprintf' , $export_transparent['gaussianblur'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['sharpen']); ?>" alt="<?php echo basename($export_filepath['sharpen']); ?>" title="<?php echo basename($export_filepath['sharpen']); ?>" />
          <figcaption>
            Sharpen<br />
            <i>took <?php echo microtime_to_human( $export_time['sharpen'] ); ?></i><br />
            <i>transparent color: <?php array_unshift( $export_transparent['sharpen'] , 'rgba(%d,%d,%d,%d)' ); echo call_user_func_array( 'sprintf' , $export_transparent['sharpen'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['negate']); ?>" alt="<?php echo basename($export_filepath['negate']); ?>" title="<?php echo basename($export_filepath['negate']); ?>" />
          <figcaption>
            Negate<br />
            <i>took <?php echo microtime_to_human( $export_time['negate'] ); ?></i><br />
            <i>transparent color: <?php array_unshift( $export_transparent['negate'] , 'rgba(%d,%d,%d,%d)' ); echo call_user_func_array( 'sprintf' , $export_transparent['negate'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['custom']); ?>" alt="<?php echo basename($export_filepath['custom']); ?>" title="<?php echo basename($export_filepath['custom']); ?>" />
          <figcaption>
            Custom<br />
            <i>took <?php echo microtime_to_human( $export_time['custom'] ); ?></i><br />
            <i>transparent color: <?php array_unshift( $export_transparent['custom'] , 'rgba(%d,%d,%d,%d)' ); echo call_user_func_array( 'sprintf' , $export_transparent['custom'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 5

  $import_filepath = SAMP_DIR.'hydrangeas.128c.8.png';
  $export_filepath = array( 
    'gaussianblur' => TMP_DIR.'00090_hydrangeas_a1' ,   
    'sharpen' => TMP_DIR.'00090_hydrangeas_a2' , 
    'negate' => TMP_DIR.'00090_hydrangeas_b1' ,   
    'custom' => TMP_DIR.'00090_hydrangeas_b2' ,  
  );
  $export_time = array();
  
  $export_color = array( 
    'gaussianblur' => null ,   
    'sharpen' => null , 
    'negate' => null ,   
    'custom' => null ,  
  );
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepath as $key => &$path )
  {
    $clone = clone $image;
    
    // give me some times to apply the transform
    set_time_limit( \MAXTIME_TO_TRANSFORM );
    
    $start = microtime( true );
    $clone->apply( \GDImage\Transform_Collection::get( $key ) );
    $export_time[$key] = microtime( true ) - $start;
    $export_color[$key] = $clone->getResource()->countColors();
    
    $path = \GDImage\Factory::export( $clone , $path );
  }
  
?>
  <h2>Test case 5: PNG8 with 128 colors</h2>
  
  <p><i>
    You will notice that the number of colors has logically changed.<br />
  </i></p>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            <i>number of colors: <?php echo $image->getResource()->countColors(); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['gaussianblur']); ?>" alt="<?php echo basename($export_filepath['gaussianblur']); ?>" title="<?php echo basename($export_filepath['gaussianblur']); ?>" />
          <figcaption>
            Gaussian blur<br />
            <i>took <?php echo microtime_to_human( $export_time['gaussianblur'] ); ?></i><br />
            <i>number of colors: <?php echo $export_color['gaussianblur']; ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['sharpen']); ?>" alt="<?php echo basename($export_filepath['sharpen']); ?>" title="<?php echo basename($export_filepath['sharpen']); ?>" />
          <figcaption>
            Sharpen<br />
            <i>took <?php echo microtime_to_human( $export_time['sharpen'] ); ?></i><br />
            <i>number of colors: <?php echo $export_color['sharpen']; ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['negate']); ?>" alt="<?php echo basename($export_filepath['negate']); ?>" title="<?php echo basename($export_filepath['negate']); ?>" />
          <figcaption>
            Negate<br />
            <i>took <?php echo microtime_to_human( $export_time['negate'] ); ?></i><br />
            <i>number of colors: <?php echo $export_color['negate']; ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['custom']); ?>" alt="<?php echo basename($export_filepath['custom']); ?>" title="<?php echo basename($export_filepath['custom']); ?>" />
          <figcaption>
            Custom<br />
            <i>took <?php echo microtime_to_human( $export_time['custom'] ); ?></i><br />
            <i>number of colors: <?php echo $export_color['custom']; ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 6

  $import_filepath = SAMP_DIR.'jellyfish.16c.8.png';
  $export_filepath = array( 
    'gaussianblur' => TMP_DIR.'00090_jellyfish_a1.jpg' ,   
    'sharpen' => TMP_DIR.'00090_jellyfish_a2.jpg' , 
    'negate' => TMP_DIR.'00090_jellyfish_b1.jpg' ,   
    'custom' => TMP_DIR.'00090_jellyfish_b2.jpg' ,  
  );
  $export_time = array();
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepath as $key => &$path )
  {
    $clone = clone $image;
    
    // give me some times to apply the transform
    set_time_limit( \MAXTIME_TO_TRANSFORM );
    
    $start = microtime( true );
    $clone->apply( \GDImage\Transform_Collection::get( $key ) );
    $export_time[$key] = microtime( true ) - $start;
    
    $path = \GDImage\Factory::export( $clone , $path );
  }
  
?>
  <h2>Test case 6: PNG8 to JPEG</h2>
  
  <p><i>
    You will notice that the transparent color has lost alpha channel due to JPEG export.<br />
    Also, there is a lot of transparent pixels that makes the convolution slower.<br />
  </i></p>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['gaussianblur']); ?>" alt="<?php echo basename($export_filepath['gaussianblur']); ?>" title="<?php echo basename($export_filepath['gaussianblur']); ?>" />
          <figcaption>
            Gaussian blur<br />
            <i>took <?php echo microtime_to_human( $export_time['gaussianblur'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['sharpen']); ?>" alt="<?php echo basename($export_filepath['sharpen']); ?>" title="<?php echo basename($export_filepath['sharpen']); ?>" />
          <figcaption>
            Sharpen<br />
            <i>took <?php echo microtime_to_human( $export_time['sharpen'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['negate']); ?>" alt="<?php echo basename($export_filepath['negate']); ?>" title="<?php echo basename($export_filepath['negate']); ?>" />
          <figcaption>
            Negate<br />
            <i>took <?php echo microtime_to_human( $export_time['negate'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['custom']); ?>" alt="<?php echo basename($export_filepath['custom']); ?>" title="<?php echo basename($export_filepath['custom']); ?>" />
          <figcaption>
            Custom<br />
            <i>took <?php echo microtime_to_human( $export_time['custom'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
