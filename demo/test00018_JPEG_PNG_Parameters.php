<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

?>
<html>
<head>
  <title>GDImage: Test 00018 - JPEG and PNG parameters</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00018 - JPEG and PNG parameters</h1>
  
  <hr />
  
<?php
  // Test case 1

  $import_filepath = SAMP_DIR.'lighthouse.24.png';
  
  $export_basepath = TMP_DIR.'00018_lighthouse';
  
  $export_filepath = array();
  $export_time = array();
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  // default quality
  
  $start = microtime( true );
  $export_filepath['df'] = \GDImage\Factory::export( $image , $export_basepath.'_adf' , 'image/jpeg' );
  $export_time['df'] = microtime( true ) - $start;
  
  for( $i = 0; $i <= 100; $i += 10 )
  {
    $start = microtime( true );
    $export_filepath[$i] = \GDImage\Factory::export( $image , array(
      'driver' => 'File' ,
      'file' => $export_basepath.'_a'.$i ,
      $i ,
    ) , 'image/jpeg' );
    $export_time[$i] = microtime( true ) - $start;
  }
  
  
$code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
       .'$final_filepath = \\GDImage\\Factory::export( $image , array('."\n"
       .'  \'driver\' => \'File\' ,'."\n"
       .'  \'file\' => $export_filepath ,'."\n"
       .'  $quality ,'."\n"
       .') , \'image/jpeg\' );';
  
?>
  <h2>Test case 1: JPEG quality</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    You will notice that time to generate JPEG does not depend on quality, but maximum quality file is really big compared to the high quality one.<br />
    The default quality for JPEG export can be modified helping <code>\GDImage\Config::setImageJpegQuality( $quality )</code> setting.<br />
  </i></p>
  
  <table>
    <tr>
      <td rowspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['df']); ?>" alt="<?php echo basename($export_filepath['df']); ?>" title="<?php echo basename($export_filepath['df']); ?>" />
          <figcaption>
            Default quality <?php echo \GDImage\Config::getImageJpegQuality(); ?> (<?php echo filesize_to_human( $export_filepath['df'] ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time['df'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <?php for( $i = 0; $i < 60; $i += 10 ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath[$i]); ?>" alt="<?php echo basename($export_filepath[$i]); ?>" title="<?php echo basename($export_filepath[$i]); ?>" />
            <figcaption>
              Quality <?php echo $i; ?> (<?php echo filesize_to_human( $export_filepath[$i] ); ?>)<br />
              <i>took <?php echo microtime_to_human( $export_time[$i] ); ?></i>
            </figcaption>
          </figure>
        </td>
      <?php endfor; ?>
    </tr>
    <tr>
      <?php for(; $i <= 100; $i += 10 ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath[$i]); ?>" alt="<?php echo basename($export_filepath[$i]); ?>" title="<?php echo basename($export_filepath[$i]); ?>" />
            <figcaption>
              Quality <?php echo $i; ?> (<?php echo filesize_to_human( $export_filepath[$i] ); ?>)<br />
              <i>took <?php echo microtime_to_human( $export_time[$i] ); ?></i>
            </figcaption>
          </figure>
        </td>
      <?php endfor; ?>
      <td></td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'chrysanthemum.jpg';
  
  $export_basepath = TMP_DIR.'00018_chrysanthemum';
  
  $export_filepath = array();
  $export_time = array();
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  // default quality
  $start = microtime( true );
  $export_filepath['df'] = \GDImage\Factory::export( $image , $export_basepath.'_bdf' , 'image/png' );
  $export_time['df'] = microtime( true ) - $start;
  
  for( $i = 0; $i < 10; $i++ )
  {
    $start = microtime( true );
    $export_filepath[$i] = \GDImage\Factory::export( $image , array(
      'driver' => 'File' ,
      'file' => $export_basepath.'_b'.$i ,
      $i ,
    ) , 'image/png' );
    $export_time[$i] = microtime( true ) - $start;
  }
  
  
$code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
       .'$final_filepath = \\GDImage\\Factory::export( $image , array('."\n"
       .'  \'driver\' => \'File\' ,'."\n"
       .'  \'file\' => $export_filepath ,'."\n"
       .'  $compression ,'."\n"
       .') , \'image/png\' );';
  
?>
  <h2>Test case 2: PNG24 compression</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    You will notice that time to generate PNG24 depends on compression and above a certain compression level, we do not earn many bytes compared to the time spent.<br />
    The default compression for PNG export can be modified helping <code>\GDImage\Config::setImagePngCompression( $compression )</code> setting.<br />
    Keep in mind that this setting will also be used for APNG.
  </i></p>
  
  <table>
    <tr>
      <td rowspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['df']); ?>" alt="<?php echo basename($export_filepath['df']); ?>" title="<?php echo basename($export_filepath['df']); ?>" />
          <figcaption>
            Default compression <?php echo \GDImage\Config::getImagePngCompression(); ?> (<?php echo filesize_to_human( $export_filepath['df'] ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time['df'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <?php for( $i = 0; $i < 5; $i++ ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath[$i]); ?>" alt="<?php echo basename($export_filepath[$i]); ?>" title="<?php echo basename($export_filepath[$i]); ?>" />
            <figcaption>
              Compression <?php echo $i; ?> (<?php echo filesize_to_human( $export_filepath[$i] ); ?>)<br />
              <i>took <?php echo microtime_to_human( $export_time[$i] ); ?></i>
            </figcaption>
          </figure>
        </td>
      <?php endfor; ?>
    </tr>
    <tr>
      <?php for(; $i < 10; $i++ ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath[$i]); ?>" alt="<?php echo basename($export_filepath[$i]); ?>" title="<?php echo basename($export_filepath[$i]); ?>" />
            <figcaption>
              Compression <?php echo $i; ?> (<?php echo filesize_to_human( $export_filepath[$i] ); ?>)<br />
              <i>took <?php echo microtime_to_human( $export_time[$i] ); ?></i>
            </figcaption>
          </figure>
        </td>
      <?php endfor; ?>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 3

  $import_filepath = SAMP_DIR.'tulips.256c.gif';
  
  $export_basepath = TMP_DIR.'00018_tulips';
  
  $export_filepath = array();
  $export_time = array();
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  // default compression
  $start = microtime( true );
  $export_filepath['df'] = \GDImage\Factory::export( $image , $export_basepath.'_cdf' , 'image/png' );
  $export_time['df'] = microtime( true ) - $start;
  
  for( $i = 0; $i < 10; $i++ )
  {
    $start = microtime( true );
    $export_filepath[$i] = \GDImage\Factory::export( $image , array(
      'driver' => 'File' ,
      'file' => $export_basepath.'_c'.$i ,
      'palettecolor' => true ,
      $i ,
    ) , 'image/png' );
    $export_time[$i] = microtime( true ) - $start;
  }
  
  
$code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
       .'$final_filepath = \\GDImage\\Factory::export( $image , array('."\n"
       .'  \'driver\' => \'File\' ,'."\n"
       .'  \'file\' => $export_filepath ,'."\n"
       .'  \'palettecolor\' => true ,'."\n"
       .'  $compression ,'."\n"
       .') , \'image/png\' );';
  
?>
  <h2>Test case 3: PNG8 compression</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    In contrast to PNG24, compression level has minor impact on time to generate PNG8 picture.<br />
  </i></p>
  
  <table>
    <tr>
      <td rowspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['df']); ?>" alt="<?php echo basename($export_filepath['df']); ?>" title="<?php echo basename($export_filepath['df']); ?>" />
          <figcaption>
            Default compression <?php echo \GDImage\Config::getImagePngCompression(); ?> (<?php echo filesize_to_human( $export_filepath['df'] ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time['df'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <?php for( $i = 0; $i < 5; $i++ ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath[$i]); ?>" alt="<?php echo basename($export_filepath[$i]); ?>" title="<?php echo basename($export_filepath[$i]); ?>" />
            <figcaption>
              Compression <?php echo $i; ?> (<?php echo filesize_to_human( $export_filepath[$i] ); ?>)<br />
              <i>took <?php echo microtime_to_human( $export_time[$i] ); ?></i>
            </figcaption>
          </figure>
        </td>
      <?php endfor; ?>
    </tr>
    <tr>
      <?php for(; $i < 10; $i++ ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath[$i]); ?>" alt="<?php echo basename($export_filepath[$i]); ?>" title="<?php echo basename($export_filepath[$i]); ?>" />
            <figcaption>
              Compression <?php echo $i; ?> (<?php echo filesize_to_human( $export_filepath[$i] ); ?>)<br />
              <i>took <?php echo microtime_to_human( $export_time[$i] ); ?></i>
            </figcaption>
          </figure>
        </td>
      <?php endfor; ?>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 4

  $import_filepath = SAMP_DIR.'desert.24.png';
  
  $export_basepath = TMP_DIR.'00018_desert';
  
  $export_filepath = array();
  $export_time = array();
  
  $export_filters = array(
    \PNG_NO_FILTER => '\PNG_NO_FILTER' ,
    \PNG_FILTER_NONE => '\PNG_FILTER_NONE' ,
    \PNG_FILTER_SUB => '\PNG_FILTER_SUB' ,
    \PNG_FILTER_UP => '\PNG_FILTER_UP' ,
    \PNG_FILTER_AVG => '\PNG_FILTER_AVG' ,
    \PNG_FILTER_PAETH => '\PNG_FILTER_PAETH' ,
    \PNG_ALL_FILTERS => '\PNG_ALL_FILTERS' ,
  );
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  // default filters
  $start = microtime( true );
  $export_filepath['df'] = \GDImage\Factory::export( $image , $export_basepath.'_ddf' , 'image/png' );
  $export_time['df'] = microtime( true ) - $start;
  
  foreach( array_keys( $export_filters ) as $filters )
  {
    $start = microtime( true );
    $export_filepath[$filters] = \GDImage\Factory::export( $image , array(
      'driver' => 'File' ,
      'file' => $export_basepath.'_d'.$filters ,
      \GDImage\Config::getImagePngCompression() ,
      $filters ,
    ) , 'image/png' );
    $export_time[$filters] = microtime( true ) - $start;
  }
  
  
$code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
       .'$final_filepath = \\GDImage\\Factory::export( $image , array('."\n"
       .'  \'driver\' => \'File\' ,'."\n"
       .'  \'file\' => $export_filepath ,'."\n"
       .'  $compression ,'."\n"
       .'  $filters ,'."\n"
       .') , \'image/png\' );';
  
?>
  <h2>Test case 4: PNG filters on PNG24</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    PNG filters are used to improve the compressibility of image data in PNG file.<br />
    The complexity of filter to apply has a minor impact on time to generate the PNG.<br />
    Also, a filter can be associated for each lines, that's why using <code>\PNG_FILTER_ALL</code> tell GD to look for the best filter to use on each lines, and, obviously, it takes more time than using only one filter.<br />
    For more information see <a href="http://www.w3.org/TR/PNG/#7Filtering" target="_blank">http://www.w3.org/TR/PNG/#7Filtering</a>.<br />
    <br />
    The default filters for PNG export depends on color type (see <a href="http://www.w3.org/TR/PNG/#12Filter-selection" target="_blank">http://www.w3.org/TR/PNG/#12Filter-selection</a>).<br />
    For true color picture, it can be modified helping <code>\GDImage\Config::setImagePngFiltersTrueColor( $filters )</code> setting.<br />
    Keep in mind that this setting will also be used on APNG.<br />
  </i></p>
  
  <table>
    <tr>
      <td rowspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['df']); ?>" alt="<?php echo basename($export_filepath['df']); ?>" title="<?php echo basename($export_filepath['df']); ?>" />
          <figcaption>
            Default <code><?php echo $export_filters[\GDImage\Config::getImagePngFiltersTrueColor()]; ?></code> (<?php echo filesize_to_human( $export_filepath['df'] ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time['df'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <?php $i = 0; reset($export_filters); ?>
      <?php while( list( $value , $text ) = each( $export_filters ) ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath[$value]); ?>" alt="<?php echo basename($export_filepath[$value]); ?>" title="<?php echo basename($export_filepath[$value]); ?>" />
            <figcaption>
              <code><?php echo $text; ?></code> (<?php echo filesize_to_human( $export_filepath[$value] ); ?>)<br />
              <i>took <?php echo microtime_to_human( $export_time[$value] ); ?></i>
            </figcaption>
          </figure>
        </td>
        <?php $i++; if( $i > 3 ) break; ?>
      <?php endwhile; ?>
    </tr>
    <tr>
      <?php while( list( $value , $text ) = each( $export_filters ) ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath[$value]); ?>" alt="<?php echo basename($export_filepath[$value]); ?>" title="<?php echo basename($export_filepath[$value]); ?>" />
            <figcaption>
              <code><?php echo $text; ?></code> (<?php echo filesize_to_human( $export_filepath[$value] ); ?>)<br />
              <i>took <?php echo microtime_to_human( $export_time[$value] ); ?></i>
            </figcaption>
          </figure>
        </td>
      <?php endwhile; ?>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 5

  $import_filepath1 = SAMP_DIR.'lighthouse.24.png';
  $import_filepath2 = SAMP_DIR.'chrysanthemum.jpg';
  $import_filepath3 = SAMP_DIR.'desert.24.png';
  
  $export_basepath1 = TMP_DIR.'00018_lighthouse';
  $export_basepath2 = TMP_DIR.'00018_chrysanthemum';
  $export_basepath3 = TMP_DIR.'00018_desert';
  
  $export_filepath1 = array();
  $export_filepath2 = array();
  $export_filepath3 = array();
  $export_time1 = array();
  $export_time2 = array();
  $export_time3 = array();
  
  $export_filters = array(
    \PNG_FILTER_NONE => '\PNG_FILTER_NONE' ,
    \PNG_FILTER_SUB => '\PNG_FILTER_SUB' ,
    \PNG_FILTER_UP => '\PNG_FILTER_UP' ,
    \PNG_FILTER_AVG => '\PNG_FILTER_AVG' ,
    \PNG_FILTER_PAETH => '\PNG_FILTER_PAETH' ,
  );
  
  for( $i = 1; $i <= 3; $i++ )
  {
    $image = \GDImage\Factory::import( ${'import_filepath'.$i} );

    // default filters
    $start = microtime( true );
    ${'export_filepath'.$i}['df'] = \GDImage\Factory::export( $image , ${'export_basepath'.$i}.'_edf' , 'image/png' );
    ${'export_time'.$i}['df'] = microtime( true ) - $start;

    foreach( array_keys( $export_filters ) as $filters )
    {
      $start = microtime( true );
      ${'export_filepath'.$i}[$filters] = \GDImage\Factory::export( $image , array(
        'driver' => 'File' ,
        'file' => ${'export_basepath'.$i}.'_e'.$filters ,
        \GDImage\Config::getImagePngCompression() ,
        $filters ,
      ) , 'image/png' );
      ${'export_time'.$i}[$filters] = microtime( true ) - $start;
    }
  }
  
  
$code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
       .'$final_filepath = \\GDImage\\Factory::export( $image , array('."\n"
       .'  \'driver\' => \'File\' ,'."\n"
       .'  \'file\' => $export_filepath ,'."\n"
       .'  $compression ,'."\n"
       .'  $filters ,'."\n"
       .') , \'image/png\' );';
  
?>
  <h2>Test case 5: Standalone PNG filter comparison on PNG24</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    You could found better PNG filters for a particular picture, but it is never true on all pictures.<br />
  </i></p>
  
  <table>
    <?php for( $i = 1; $i <= 3; $i++ ): ?>
      <tr>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename(${'export_filepath'.$i}['df']); ?>" alt="<?php echo basename(${'export_filepath'.$i}['df']); ?>" title="<?php echo basename(${'export_filepath'.$i}['df']); ?>" />
            <figcaption>
              Default (<?php echo filesize_to_human( ${'export_filepath'.$i}['df'] ); ?>)<br />
              <i>took <?php echo microtime_to_human( ${'export_time'.$i}['df'] ); ?></i>
            </figcaption>
          </figure>
        </td>
        <?php reset( $export_filters ); ?>
        <?php while( list( $value , $text ) = each( $export_filters ) ): ?>
          <td>
            <figure>
              <img src="./tmp/<?php echo basename(${'export_filepath'.$i}[$value]); ?>" alt="<?php echo basename(${'export_filepath'.$i}[$value]); ?>" title="<?php echo basename(${'export_filepath'.$i}[$value]); ?>" />
              <figcaption>
                <code><?php echo $text; ?></code> (<?php echo filesize_to_human( ${'export_filepath'.$i}[$value] ); ?>)<br />
                <i>took <?php echo microtime_to_human( ${'export_time'.$i}[$value] ); ?></i>
              </figcaption>
            </figure>
          </td>
        <?php endwhile; ?>
      </tr>
    <?php endfor; ?>
      
  </table>
  
  <hr />
  
<?php /**/
  // Test case 6

  $import_filepath1 = SAMP_DIR.'lighthouse.24.png';
  $import_filepath2 = SAMP_DIR.'chrysanthemum.jpg';
  $import_filepath3 = SAMP_DIR.'desert.24.png';
  
  $export_basepath1 = TMP_DIR.'00018_lighthouse';
  $export_basepath2 = TMP_DIR.'00018_chrysanthemum';
  $export_basepath3 = TMP_DIR.'00018_desert';
  
  $export_filepath1 = array();
  $export_filepath2 = array();
  $export_filepath3 = array();
  $export_time1 = array();
  $export_time2 = array();
  $export_time3 = array();
  
  $export_filters = array(
    \PNG_FILTER_SUB | \PNG_FILTER_UP => '\PNG_FILTER_SUB | \PNG_FILTER_UP' ,
    \PNG_FILTER_SUB | \PNG_FILTER_AVG => '\PNG_FILTER_SUB | \PNG_FILTER_AVG' ,
    \PNG_FILTER_SUB | \PNG_FILTER_PAETH => '\PNG_FILTER_SUB | \PNG_FILTER_PAETH' ,
    \PNG_FILTER_AVG | \PNG_FILTER_PAETH => '\PNG_FILTER_AVG | \PNG_FILTER_PAETH' ,
    \PNG_ALL_FILTERS => '\PNG_ALL_FILTERS' ,
  );
  
  for( $i = 1; $i <= 3; $i++ )
  {
    $image = \GDImage\Factory::import( ${'import_filepath'.$i} );

    // default filters
    $start = microtime( true );
    ${'export_filepath'.$i}['df'] = \GDImage\Factory::export( $image , ${'export_basepath'.$i}.'_fdf' , 'image/png' );
    ${'export_time'.$i}['df'] = microtime( true ) - $start;

    foreach( array_keys( $export_filters ) as $filters )
    {
      $start = microtime( true );
      ${'export_filepath'.$i}[$filters] = \GDImage\Factory::export( $image , array(
        'driver' => 'File' ,
        'file' => ${'export_basepath'.$i}.'_f'.$filters ,
        \GDImage\Config::getImagePngCompression() ,
        $filters ,
      ) , 'image/png' );
      ${'export_time'.$i}[$filters] = microtime( true ) - $start;
    }
  }
  
  
$code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
       .'$final_filepath = \\GDImage\\Factory::export( $image , array('."\n"
       .'  \'driver\' => \'File\' ,'."\n"
       .'  \'file\' => $export_filepath ,'."\n"
       .'  $compression ,'."\n"
       .'  $filters ,'."\n"
       .') , \'image/png\' );';
  
?>
  <h2>Test case 6: Multiple PNG filters comparison on PNG24</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    Your best bet to optimize file size on true color PNG is <code>\PNG_ALL_FILTERS</code>.<br />
  </i></p>
  
  <table>
    <?php for( $i = 1; $i <= 3; $i++ ): ?>
      <tr>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename(${'export_filepath'.$i}['df']); ?>" alt="<?php echo basename(${'export_filepath'.$i}['df']); ?>" title="<?php echo basename(${'export_filepath'.$i}['df']); ?>" />
            <figcaption style="white-space: nowrap">
              Default<br />
              (<?php echo filesize_to_human( ${'export_filepath'.$i}['df'] ); ?>)<br />
              <i>took <?php echo microtime_to_human( ${'export_time'.$i}['df'] ); ?></i>
            </figcaption>
          </figure>
        </td>
        <?php reset( $export_filters ); ?>
        <?php while( list( $value , $text ) = each( $export_filters ) ): ?>
          <td>
            <figure>
              <img src="./tmp/<?php echo basename(${'export_filepath'.$i}[$value]); ?>" alt="<?php echo basename(${'export_filepath'.$i}[$value]); ?>" title="<?php echo basename(${'export_filepath'.$i}[$value]); ?>" />
              <figcaption style="white-space: nowrap">
                <code><?php echo $text; ?></code><br />
                (<?php echo filesize_to_human( ${'export_filepath'.$i}[$value] ); ?>)<br />
                <i>took <?php echo microtime_to_human( ${'export_time'.$i}[$value] ); ?></i>
              </figcaption>
            </figure>
          </td>
        <?php endwhile; ?>
      </tr>
    <?php endfor; ?>
      
  </table>
  
  <hr />
  
<?php /**/
  // Test case 7

  $import_filepath = SAMP_DIR.'hydrangeas.128c.8.png';
  
  $export_basepath = TMP_DIR.'00018_hydrangeas';
  
  $export_filepath = array();
  $export_time = array();
  
  $export_filters = array(
    \PNG_NO_FILTER => '\PNG_NO_FILTER' ,
    \PNG_FILTER_NONE => '\PNG_FILTER_NONE' ,
    \PNG_FILTER_SUB => '\PNG_FILTER_SUB' ,
    \PNG_FILTER_UP => '\PNG_FILTER_UP' ,
    \PNG_FILTER_AVG => '\PNG_FILTER_AVG' ,
    \PNG_FILTER_PAETH => '\PNG_FILTER_PAETH' ,
    \PNG_ALL_FILTERS => '\PNG_ALL_FILTERS' ,
  );
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  // default filters
  $start = microtime( true );
  $export_filepath['df'] = \GDImage\Factory::export( $image , $export_basepath.'_gdf' , 'image/png' );
  $export_time['df'] = microtime( true ) - $start;
  
  foreach( array_keys( $export_filters ) as $filters )
  {
    $start = microtime( true );
    $export_filepath[$filters] = \GDImage\Factory::export( $image , array(
      'driver' => 'File' ,
      'file' => $export_basepath.'_g'.$filters ,
      'palettecolor' => true ,
      \GDImage\Config::getImagePngCompression() ,
      $filters ,
    ) , 'image/png' );
    $export_time[$filters] = microtime( true ) - $start;
  }
  
  
$code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
       .'$final_filepath = \\GDImage\\Factory::export( $image , array('."\n"
       .'  \'driver\' => \'File\' ,'."\n"
       .'  \'file\' => $export_filepath ,'."\n"
       .'  \'palettecolor\' => true ,'."\n"
       .'  $compression ,'."\n"
       .'  $filters ,'."\n"
       .') , \'image/png\' );';
  
?>
  <h2>Test case 7: PNG filters on PNG8</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    For palette color picture, PNG filters can be modified helping <code>\GDImage\Config::setImagePngFiltersPaletteColor( $filters )</code> setting.<br />
    You will notice that on palette color pictures, any PNG filters provide bigger file.
  </i></p>
  
  <table>
    <tr>
      <td rowspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['df']); ?>" alt="<?php echo basename($export_filepath['df']); ?>" title="<?php echo basename($export_filepath['df']); ?>" />
          <figcaption>
            Default <code><?php echo $export_filters[\GDImage\Config::getImagePngFiltersPaletteColor()]; ?></code> (<?php echo filesize_to_human( $export_filepath['df'] ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time['df'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <?php $i = 0; reset($export_filters); ?>
      <?php while( list( $value , $text ) = each( $export_filters ) ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath[$value]); ?>" alt="<?php echo basename($export_filepath[$value]); ?>" title="<?php echo basename($export_filepath[$value]); ?>" />
            <figcaption>
              <code><?php echo $text; ?></code> (<?php echo filesize_to_human( $export_filepath[$value] ); ?>)<br />
              <i>took <?php echo microtime_to_human( $export_time[$value] ); ?></i>
            </figcaption>
          </figure>
        </td>
        <?php $i++; if( $i > 3 ) break; ?>
      <?php endwhile; ?>
    </tr>
    <tr>
      <?php while( list( $value , $text ) = each( $export_filters ) ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath[$value]); ?>" alt="<?php echo basename($export_filepath[$value]); ?>" title="<?php echo basename($export_filepath[$value]); ?>" />
            <figcaption>
              <code><?php echo $text; ?></code> (<?php echo filesize_to_human( $export_filepath[$value] ); ?>)<br />
              <i>took <?php echo microtime_to_human( $export_time[$value] ); ?></i>
            </figcaption>
          </figure>
        </td>
      <?php endwhile; ?>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
