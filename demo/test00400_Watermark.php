<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

?>
<html>
<head>
  <title>GDImage: Test 00400 - The Watermark Case</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00400 - The Watermark Case</h1>
  
  <p><i>
    Watermarks are something that cannot be parameterized: sometimes it is a picture, sometimes a text, sometimes it must be at a fixed size and position, sometimes it musts cover the entire picture with alpha channel…<br />
    Because of this versatility, we cannot create a "watermark" transformation and claim that it works exactly as expected.<br />
    Hopefully, helping GDImage, you can easly create a custom transformation that will do the watermark that you want over any kind of picture.<br />
  </i></p>
  
<?php /**/
  // Test case 1

  ###

  class MyWatermark1 implements \GDImage\Transform_Interface
  {
    /**
     * Apply transformation to a resource.
     * Return false if the transformation fails.
     *
     * @param \GDImage\Resource_Abstract &$rsc
     * @return boolean Success flag
     * @access public
     */
    public function __invoke( \GDImage\Resource_Abstract &$rsc )
    {
      // at first import watermark
      $watermark = \GDImage\Factory::import( SAMP_DIR.'me.gif' );
      
      // convert the watermark to true color for better smoothing
      $watermark->setResource( $watermark->getResource()->toTrueColor() );
      
      // resize the watermark
      // we want it to be at maximum 50px width but not more than 10% of the original picture
      // we also it to has a maximum height of 20% of the original picture
      $width = $rsc->getWidth() * 0.1;
      if( $width > 50 ) $width = 50;
      $watermark->apply( new \GDImage\Transform_Resize_Fit( $width , $rsc->getHeight() * 0.2 ) );

      // apply a little opacity on the watermark
      // doing it on the resized picture will be quicker than on the original one
      $watermark->apply( new \GDImage\Transform_Opacity( 60 ) );
      
      
      // now we want to place the watermark at the bottom right corner
      // at 25px from right but not more than 2% of the original picture
      $right = $rsc->getWidth() * 0.02;
      if( $right > 25 ) $right = 25;
      // use same value for bottom
      $bottom = $right;
      
      // \GDImage is not a full abstraction of GD functions
      // so sometimes, we must use some GD function
      
      // before to copy, never forgot to do it on true color
      $tmp = $rsc->toTrueColor();
      
      // enable alpha blending
      $tmp->enable( 'alphablending' );
      
      if( ! imagecopy( $tmp->getGdResource() , $watermark->getResource()->getGdResource() , 
        // determine position from top-left corner
        $tmp->getWidth() - $watermark->getWidth() - $right , $tmp->getHeight() - $watermark->getHeight() - $bottom , 
        // copy entire source
        0 , 0 , $watermark->getWidth() , $watermark->getHeight() ) )
      {
        return false; // if it fails, we fail
      }
      
      // restore palette color ability
      if( $rsc->isPaletteColor() )
      {
        $tmp2 = $tmp->toPaletteColor();

        // if we were unable to find any transparent color anymore
        // we may have trouble with AGIF manipulation
        if( $rsc->getTransparent() && ( ! $tmp2->getTransparent() ) )
        {
          if( $tmp2->countColors() === 256 )
          {
            // redo palette conversion with one slot for the transparent color
            $tmp2 = $tmp->toPaletteColor( 255 );
          }

          // assign transparent
          $tmp2->setTransparent( $rsc->getTransparent() );
        }

        // reassign tmp
        $tmp = $tmp2;
      }
      
      
      // finally replace original resource
      $rsc = $tmp;

      // roger roll
      return true;
    }
  }
  
  ###

  $import_filepaths = array(
    SAMP_DIR.'chrysanthemum.jpg' ,
    SAMP_DIR.'tulips.256c.gif' ,
    SAMP_DIR.'jellyfish.16c.8.png' ,
    SAMP_DIR.'agif/lesjoiesducode_fr.gif' ,
  );
  $export_filepaths = array(
    TMP_DIR.'00400_chrysanthemum_a.jpg' ,  
    TMP_DIR.'00400_tulips_a.gif' , 
    TMP_DIR.'00400_jellyfish_a.png' , 
    TMP_DIR.'00400_lesjoiesducode_fr_a.gif' , 
  );

  // disable optimization
  \GDImage\Config::setAGifComposerOptimization( 0 ); 
  
  foreach( $import_filepaths as $key => $import_filepath )
  {
    // import image
    $image = \GDImage\Factory::import( $import_filepath );
    $image->apply( new MyWatermark1() );
    \GDImage\Factory::export( $image , $export_filepaths[$key] );
  }

?>
  <h2>Test case 1: Watermark on bottom right corner</h2>
  
  <pre>class MyWatermark implements \GDImage\Transform_Interface
{
  /**
   * Apply transformation to a resource.
   * Return false if the transformation fails.
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access public
   */
  public function __invoke( \GDImage\Resource_Abstract &$rsc )
  {
    // at first import watermark
    $watermark = \GDImage\Factory::import( $watermark_filepath );
      
    // convert the watermark to true color for better smoothing
    $watermark->setResource( $watermark->getResource()->toTrueColor() );

    // resize the watermark
    // we want it to be at maximum 50px width but not more than 10% of the original picture
    // we also it to has a maximum height of 20% of the original picture
    $width = $rsc->getWidth() * 0.1;
    if( $width > 50 ) $width = 50;
    $watermark->apply( new \GDImage\Transform_Resize_Fit( $width , $rsc->getHeight() * 0.2 ) );

    // apply a little opacity on the watermark
    // doing it on the resized picture will be quicker than on the original one
    $watermark->apply( new \GDImage\Transform_Opacity( 60 ) );


    // now we want to place the watermark at the bottom right corner
    // at 25px from right but not more than 2% of the original picture
    $right = $rsc->getWidth() * 0.02;
    if( $right > 25 ) $right = 25;
    // use same value for bottom
    $bottom = $right;

    // \GDImage is not a full abstraction of GD functions
    // so sometimes, we must use some GD function

    // before to copy, never forgot to do it on true color
    $tmp = $rsc->toTrueColor();

    // enable alpha blending
    $tmp->enable( 'alphablending' );

    if( ! imagecopy( $tmp->getGdResource() , $watermark->getResource()->getGdResource() , 
      // determine position from top-left corner
      $tmp->getWidth() - $watermark->getWidth() - $right , $tmp->getHeight() - $watermark->getHeight() - $bottom , 
      // copy entire source
      0 , 0 , $watermark->getWidth() , $watermark->getHeight() ) )
    {
      return false; // if it fails, we fail
    }

      
    // restore palette color ability
    if( $rsc->isPaletteColor() )
    {
      $tmp2 = $tmp->toPaletteColor();

      // if we were unable to find any transparent color anymore
      // we may have trouble with AGIF manipulation
      if( $rsc->getTransparent() && ( ! $tmp2->getTransparent() ) )
      {
        if( $tmp2->countColors() === 256 )
        {
          // redo palette conversion with one slot for the transparent color
          $tmp2 = $tmp->toPaletteColor( 255 );
        }

        // assign transparent
        $tmp2->setTransparent( $rsc->getTransparent() );
      }

      // reassign tmp
      $tmp = $tmp2;
    }


    // finally replace original resource
    $rsc = $tmp;

    // roger roll
    return true;
  }
}</pre>
  
  <table>
    <tr>
      <?php foreach( $import_filepaths as $import_filepath ): ?>
        <td>
          <figure>
            <img src="<?php echo str_replace( SAMP_DIR , './samples/' , $import_filepath ); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
            <figcaption>
              Original picture
            </figcaption>
          </figure>
        </td>
      <?php endforeach; ?>
    </tr>
    <tr>
      <?php foreach( $export_filepaths as $export_filepath ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
            <figcaption>
              Result
            </figcaption>
          </figure>
        </td>
      <?php endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 2

  ###

  class MyWatermark2 implements \GDImage\Transform_Interface
  {
    /**
     * Apply transformation to a resource.
     * Return false if the transformation fails.
     *
     * @param \GDImage\Resource_Abstract &$rsc
     * @return boolean Success flag
     * @access public
     */
    public function __invoke( \GDImage\Resource_Abstract &$rsc )
    {
      // at first import watermark
      $watermark = \GDImage\Factory::import( SAMP_DIR.'me.gif' );
      
      // posterize the watermark will convert it to palette color 
      // and colors manipuation will be quicker
      $watermark->apply( new \GDImage\Transform_Posterize() );
      
      // these transformations are fast on palette color picture
      // apply grayscale
      $watermark->apply( new \GDImage\Transform_Grayscale() );
      // apply 30% opacity
      $watermark->apply( new \GDImage\Transform_Opacity( 30 ) );
      
      // resize the watermark
      
      // we want it to fit 80% of width/height of the picture
      $watermark->apply( new \GDImage\Transform_Resize_Fit( ( $rsc->getWidth() * 0.8 ) , ( $rsc->getHeight() * 0.8 ) ) );
      
      
      // now we want to place the watermark at the center of the original picture
      
      // \GDImage is not a full abstraction of GD functions
      // so sometimes, we must use some GD function
      
      // before to copy, never forgot to do it on true color
      $tmp = $rsc->toTrueColor();
      
      // enable alpha blending
      $tmp->enable( 'alphablending' );
      
      if( ! imagecopy( $tmp->getGdResource() , $watermark->getResource()->getGdResource() , 
        // determine position from top-left corner
        ( $tmp->getWidth() - $watermark->getWidth() ) / 2 , ( $tmp->getHeight() - $watermark->getHeight() ) / 2 , 
        // copy entire source
        0 , 0 , $watermark->getWidth() , $watermark->getHeight() ) )
      {
        return false; // if it fails, we fail
      }
      
      
      // restore palette color ability
      if( $rsc->isPaletteColor() )
      {
        $tmp2 = $tmp->toPaletteColor();

        // if we were unable to find any transparent color anymore
        // we may have trouble with AGIF manipulation
        if( $rsc->getTransparent() && ( ! $tmp2->getTransparent() ) )
        {
          if( $tmp2->countColors() === 256 )
          {
            // redo palette conversion with one slot for the transparent color
            $tmp2 = $tmp->toPaletteColor( 255 );
          }

          // assign transparent
          $tmp2->setTransparent( $rsc->getTransparent() );
        }

        // reassign tmp
        $tmp = $tmp2;
      }
      
      
      // finally replace original resource
      $rsc = $tmp;

      // roger roll
      return true;
    }
  }
  
  ###

  $import_filepaths = array(
    SAMP_DIR.'chrysanthemum.jpg' ,
    SAMP_DIR.'tulips.256c.gif' ,
    SAMP_DIR.'jellyfish.16c.8.png' ,
    SAMP_DIR.'agif/lesjoiesducode_fr.gif' ,
  );
  $export_filepaths = array(
    TMP_DIR.'00400_chrysanthemum_b.jpg' ,  
    TMP_DIR.'00400_tulips_b.gif' , 
    TMP_DIR.'00400_jellyfish_b.png' , 
    TMP_DIR.'00400_lesjoiesducode_fr_b.gif' , 
  );

  // disable optimization
  \GDImage\Config::setAGifComposerOptimization( 0 ); 
  
  foreach( $import_filepaths as $key => $import_filepath )
  {
    // import image
    $image = \GDImage\Factory::import( $import_filepath );
    $image->apply( new MyWatermark2() );
    \GDImage\Factory::export( $image , $export_filepaths[$key] );
  }

?>
  <h2>Test case 2: Watermark on full picture</h2>
  
  <pre>class MyWatermark implements \GDImage\Transform_Interface
{
  /**
   * Apply transformation to a resource.
   * Return false if the transformation fails.
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access public
   */
  public function __invoke( \GDImage\Resource_Abstract &$rsc )
  {
    // at first import watermark
    $watermark = \GDImage\Factory::import( $watermark_filepath );
      
    // posterize the watermark will convert it to palette color 
    // and colors manipuation will be quicker
    $watermark->apply( new \GDImage\Transform_Posterize() );
      
    // these transformations are fast on palette color picture
    // apply grayscale
    $watermark->apply( new \GDImage\Transform_Grayscale() );
    // apply 30% opacity
    $watermark->apply( new \GDImage\Transform_Opacity( 30 ) );

    // resize the watermark

    // we want it to fit 80% of width/height of the picture
    $watermark->apply( new \GDImage\Transform_Resize_Fit( ( $rsc->getWidth() * 0.8 ) , ( $rsc->getHeight() * 0.8 ) ) );


    // now we want to place the watermark at the center of the original picture

    // \GDImage is not a full abstraction of GD functions
    // so sometimes, we must use some GD function

    // before to copy, never forgot to do it on true color
    $tmp = $rsc->toTrueColor();

    // enable alpha blending
    $tmp->enable( 'alphablending' );

    if( ! imagecopy( $tmp->getGdResource() , $watermark->getResource()->getGdResource() , 
      // determine position from top-left corner
      ( $tmp->getWidth() - $watermark->getWidth() ) / 2 , ( $tmp->getHeight() - $watermark->getHeight() ) / 2 , 
      // copy entire source
      0 , 0 , $watermark->getWidth() , $watermark->getHeight() ) )
    {
      return false; // if it fails, we fail
    }

      
    // restore palette color ability
    if( $rsc->isPaletteColor() )
    {
      $tmp2 = $tmp->toPaletteColor();

      // if we were unable to find any transparent color anymore
      // we may have trouble with AGIF manipulation
      if( $rsc->getTransparent() && ( ! $tmp2->getTransparent() ) )
      {
        if( $tmp2->countColors() === 256 )
        {
          // redo palette conversion with one slot for the transparent color
          $tmp2 = $tmp->toPaletteColor( 255 );
        }

        // assign transparent
        $tmp2->setTransparent( $rsc->getTransparent() );
      }

      // reassign tmp
      $tmp = $tmp2;
    }


    // finally replace original resource
    $rsc = $tmp;

    // roger roll
    return true;
  }
}</pre>
  
  <table>
    <tr>
      <?php foreach( $import_filepaths as $import_filepath ): ?>
        <td>
          <figure>
            <img src="<?php echo str_replace( SAMP_DIR , './samples/' , $import_filepath ); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
            <figcaption>
              Original picture
            </figcaption>
          </figure>
        </td>
      <?php endforeach; ?>
    </tr>
    <tr>
      <?php foreach( $export_filepaths as $export_filepath ): ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
            <figcaption>
              Result
            </figcaption>
          </figure>
        </td>
      <?php endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
