<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

$code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
       .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

?>
<html>
<head>
  <title>GDImage: Test 00010 - File convertion</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00010 - File convertion</h1>
  
  <pre><?php echo htmlspecialchars( $code ); ?></pre>
  
  <hr />
  
<?php
  // Test case 1

  $import_filepath = SAMP_DIR.'tulips.256c.gif';
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  $c = $image->getResource()->countColors();
  list( $r, $g, $b, $a ) = $image->getResource()->getTransparent()->toArray();
  
  $export_filepath = array(
    'gif' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.gif' , 
    'png8' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.8.png' , 
    'png24' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.24.png' , 
    'jpeg' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.jpg' , 
  );
  
  // to GIF
  \GDImage\Factory::export( $image , $export_filepath['gif'] );
  
  // to PNG8
  \GDImage\Factory::export( $image , $export_filepath['png8'] );
  
  // to PNG24
  // from GIF, we have to force true color
  \GDImage\Factory::export( $image , array(
    'driver' => 'File' ,
    'file' => $export_filepath['png24'] ,
    'truecolor' => true ,
  ) );
  
  // to JPEG
  \GDImage\Factory::export( $image , $export_filepath['jpeg'] );
  
?>
  <h2>Test case 1: GIF with <?php echo $c; ?> colors and transparent color <?php echo sprintf( 'rgba(%d,%d,%d,%d)' , $r , $g , $b , $a ); ?></h2>
  
  <p><i>You will notice that the JPEG picture preserve the transparent color without alpha channel.</i></p>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['gif']); ?>" alt="<?php echo basename($export_filepath['gif']); ?>" title="<?php echo basename($export_filepath['gif']); ?>" />
          <figcaption>To GIF (<?php echo filesize_to_human( $export_filepath['gif'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png8']); ?>" alt="<?php echo basename($export_filepath['png8']); ?>" title="<?php echo basename($export_filepath['png8']); ?>" />
          <figcaption>To PNG8 (<?php echo filesize_to_human( $export_filepath['png8'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['jpeg']); ?>" alt="<?php echo basename($export_filepath['jpeg']); ?>" title="<?php echo basename($export_filepath['jpeg']); ?>" />
          <figcaption>To JPEG (<?php echo filesize_to_human( $export_filepath['jpeg'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png24']); ?>" alt="<?php echo basename($export_filepath['png24']); ?>" title="<?php echo basename($export_filepath['png24']); ?>" />
          <figcaption>To PNG24 (<?php echo filesize_to_human( $export_filepath['png24'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  
  <hr />
  
<?php
  // Test case 2

  $import_filepath = SAMP_DIR.'hydrangeas.128c.8.png';
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  $c = $image->getResource()->countColors();
  list( $r, $g, $b, $a ) = $image->getResource()->getTransparent()->toArray();
  
  $export_filepath = array(
    'gif' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.gif' , 
    'png8' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.8.png' , 
    'png24' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.24.png' , 
    'jpeg' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.jpg' , 
  );
  
  // to GIF
  \GDImage\Factory::export( $image , $export_filepath['gif'] );
  
  // to PNG8
  \GDImage\Factory::export( $image , $export_filepath['png8'] );
  
  // to PNG24
  // from PNG8, we have to to force true color
  \GDImage\Factory::export( $image , array(
    'driver' => 'File' ,
    'file' => $export_filepath['png24'] ,
    'truecolor' => true ,
  ) );
  
  // to JPEG
  \GDImage\Factory::export( $image , $export_filepath['jpeg'] );
  
?>
  <h2>Test case 2: PNG8 with <?php echo $c; ?> colors and transparent color <?php echo sprintf( 'rgba(%d,%d,%d,%d)' , $r , $g , $b , $a ); ?></h2>
  
  <p><i>You will notice that the JPEG picture preserve the transparent color without alpha channel.</i></p>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['gif']); ?>" alt="<?php echo basename($export_filepath['gif']); ?>" title="<?php echo basename($export_filepath['gif']); ?>" />
          <figcaption>To GIF (<?php echo filesize_to_human( $export_filepath['gif'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png8']); ?>" alt="<?php echo basename($export_filepath['png8']); ?>" title="<?php echo basename($export_filepath['png8']); ?>" />
          <figcaption>To PNG8 (<?php echo filesize_to_human( $export_filepath['png8'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['jpeg']); ?>" alt="<?php echo basename($export_filepath['jpeg']); ?>" title="<?php echo basename($export_filepath['jpeg']); ?>" />
          <figcaption>To JPEG (<?php echo filesize_to_human( $export_filepath['jpeg'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png24']); ?>" alt="<?php echo basename($export_filepath['png24']); ?>" title="<?php echo basename($export_filepath['png24']); ?>" />
          <figcaption>To PNG24 (<?php echo filesize_to_human( $export_filepath['png24'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  
  <hr />
  
<?php
  // Test case 3

  $import_filepath = SAMP_DIR.'jellyfish.16c.8.png';
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  $c = $image->getResource()->countColors();
  list( $r, $g, $b, $a ) = $image->getResource()->getTransparent()->toArray();
  
  $export_filepath = array(
    'gif' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.gif' , 
    'png8' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.8.png' , 
    'png24' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.24.png' , 
    'jpeg' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.jpg' , 
  );
  
  // to GIF
  \GDImage\Factory::export( $image , $export_filepath['gif'] );
  
  // to PNG8
  \GDImage\Factory::export( $image , $export_filepath['png8'] );
  
  // to PNG24
  // from PNG8, we have to to force true color
  \GDImage\Factory::export( $image , array(
    'driver' => 'File' ,
    'file' => $export_filepath['png24'] ,
    'truecolor' => true ,
  ) );
  
  // to JPEG
  \GDImage\Factory::export( $image , $export_filepath['jpeg'] );
  
?>
  <h2>Test case 3: PNG8 with <?php echo $c; ?> colors and transparent color <?php echo sprintf( 'rgba(%d,%d,%d,%d)' , $r , $g , $b , $a ); ?></h2>
  
  <p><i>You will notice that the JPEG picture preserve the transparent color without alpha channel.</i></p>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['gif']); ?>" alt="<?php echo basename($export_filepath['gif']); ?>" title="<?php echo basename($export_filepath['gif']); ?>" />
          <figcaption>To GIF (<?php echo filesize_to_human( $export_filepath['gif'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png8']); ?>" alt="<?php echo basename($export_filepath['png8']); ?>" title="<?php echo basename($export_filepath['png8']); ?>" />
          <figcaption>To PNG8 (<?php echo filesize_to_human( $export_filepath['png8'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['jpeg']); ?>" alt="<?php echo basename($export_filepath['jpeg']); ?>" title="<?php echo basename($export_filepath['jpeg']); ?>" />
          <figcaption>To JPEG (<?php echo filesize_to_human( $export_filepath['jpeg'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png24']); ?>" alt="<?php echo basename($export_filepath['png24']); ?>" title="<?php echo basename($export_filepath['png24']); ?>" />
          <figcaption>To PNG24 (<?php echo filesize_to_human( $export_filepath['png24'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  
  <hr />
  
<?php
  // Test case 4

  $import_filepath = SAMP_DIR.'lighthouse.24.png';
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  $export_filepath = array(
    'gif' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.gif' , 
    'png8' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.8.png' , 
    'png24' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.24.png' , 
    'jpeg' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.jpg' , 
  );
  
  // to GIF
  \GDImage\Factory::export( $image , $export_filepath['gif'] );
  
  // to PNG8
  // from PNG24, we have to force palette color
  \GDImage\Factory::export( $image , array(
    'driver' => 'File' ,
    'file' => $export_filepath['png8'] ,
    'palettecolor' => true ,
  ) );
  
  // to PNG24
  \GDImage\Factory::export( $image , $export_filepath['png24'] );
  
  // to JPEG
  \GDImage\Factory::export( $image , $export_filepath['jpeg'] );
  
?>
  <h2>Test case 4: PNG24 with 75% alpha</h2>
  
  <p><i>You will notice that even if PNG8 picture is reduced to 256 colors, some of these colors kept alpha channel.</i></p>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['gif']); ?>" alt="<?php echo basename($export_filepath['gif']); ?>" title="<?php echo basename($export_filepath['gif']); ?>" />
          <figcaption>To GIF (<?php echo filesize_to_human( $export_filepath['gif'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png8']); ?>" alt="<?php echo basename($export_filepath['png8']); ?>" title="<?php echo basename($export_filepath['png8']); ?>" />
          <figcaption>To PNG8 (<?php echo filesize_to_human( $export_filepath['png8'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['jpeg']); ?>" alt="<?php echo basename($export_filepath['jpeg']); ?>" title="<?php echo basename($export_filepath['jpeg']); ?>" />
          <figcaption>To JPEG (<?php echo filesize_to_human( $export_filepath['jpeg'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png24']); ?>" alt="<?php echo basename($export_filepath['png24']); ?>" title="<?php echo basename($export_filepath['png24']); ?>" />
          <figcaption>To PNG24 (<?php echo filesize_to_human( $export_filepath['png24'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  
  <hr />
  
<?php
  // Test case 5

  $import_filepath = SAMP_DIR.'desert.24.png';
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  $export_filepath = array(
    'gif' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.gif' , 
    'png8' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.8.png' , 
    'png24' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.24.png' , 
    'jpeg' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.jpg' , 
  );
  
  // to GIF
  \GDImage\Factory::export( $image , $export_filepath['gif'] );
  
  // to PNG8
  // from PNG24, we have to force palette color
  \GDImage\Factory::export( $image , array(
    'driver' => 'File' ,
    'file' => $export_filepath['png8'] ,
    'palettecolor' => true ,
  ) );
  
  // to PNG24
  \GDImage\Factory::export( $image , $export_filepath['png24'] );
  
  // to JPEG
  \GDImage\Factory::export( $image , $export_filepath['jpeg'] );
  
?>
  <h2>Test case 5: PNG24 with 100% alpha</h2>
  
  <p><i>On JPEG picture, you will notice a white background that probably comes from my Photoshop export.</i></p>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['gif']); ?>" alt="<?php echo basename($export_filepath['gif']); ?>" title="<?php echo basename($export_filepath['gif']); ?>" />
          <figcaption>To GIF (<?php echo filesize_to_human( $export_filepath['gif'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png8']); ?>" alt="<?php echo basename($export_filepath['png8']); ?>" title="<?php echo basename($export_filepath['png8']); ?>" />
          <figcaption>To PNG8 (<?php echo filesize_to_human( $export_filepath['png8'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['jpeg']); ?>" alt="<?php echo basename($export_filepath['jpeg']); ?>" title="<?php echo basename($export_filepath['jpeg']); ?>" />
          <figcaption>To JPEG (<?php echo filesize_to_human( $export_filepath['jpeg'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png24']); ?>" alt="<?php echo basename($export_filepath['png24']); ?>" title="<?php echo basename($export_filepath['png24']); ?>" />
          <figcaption>To PNG24 (<?php echo filesize_to_human( $export_filepath['png24'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  
  <hr />
  
<?php
  // Test case 6

  $import_filepath = SAMP_DIR.'chrysanthemum.jpg';
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  $export_filepath = array(
    'gif' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.gif' , 
    'png8' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.8.png' , 
    'png24' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.24.png' , 
    'jpeg' => TMP_DIR.'00010_'.pathinfo( $import_filepath , PATHINFO_FILENAME ).'.jpg' , 
  );
  
  // to GIF
  \GDImage\Factory::export( $image , $export_filepath['gif'] );
  
  // to PNG8
  // from JPEG, we have to force palette color
  \GDImage\Factory::export( $image , array(
    'driver' => 'File' ,
    'file' => $export_filepath['png8'] ,
    'palettecolor' => true ,
  ) );
  
  // to PNG24
  \GDImage\Factory::export( $image , $export_filepath['png24'] );
  
  // to JPEG
  \GDImage\Factory::export( $image , $export_filepath['jpeg'] );
  
?>
  <h2>Test case 6: JPEG (simple case)</h2>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['gif']); ?>" alt="<?php echo basename($export_filepath['gif']); ?>" title="<?php echo basename($export_filepath['gif']); ?>" />
          <figcaption>To GIF (<?php echo filesize_to_human( $export_filepath['gif'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png8']); ?>" alt="<?php echo basename($export_filepath['png8']); ?>" title="<?php echo basename($export_filepath['png8']); ?>" />
          <figcaption>To PNG8 (<?php echo filesize_to_human( $export_filepath['png8'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['jpeg']); ?>" alt="<?php echo basename($export_filepath['jpeg']); ?>" title="<?php echo basename($export_filepath['jpeg']); ?>" />
          <figcaption>To JPEG (<?php echo filesize_to_human( $export_filepath['jpeg'] ); ?>)</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['png24']); ?>" alt="<?php echo basename($export_filepath['png24']); ?>" title="<?php echo basename($export_filepath['png24']); ?>" />
          <figcaption>To PNG24 (<?php echo filesize_to_human( $export_filepath['png24'] ); ?>)</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php require '_foot.partial.php'; ?>
  
</body>
</html>
        
