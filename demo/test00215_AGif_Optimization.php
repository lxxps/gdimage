<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

?>
<html>
<head>
  <title>GDImage: Test 00215 - AGIF Optimization</title>
  <?php require '_head.partial.php'; ?>
  <style>
    small { white-space: nowrap; }
  </style>
</head>
<body>
  
  <h1>GDImage: Test 00215 - AGIF Optimization</h1>
  
  <p><i>
    For AGIF composition, an important setting has to be introduced: AGIF Composer Optimization.<br />
    This setting is a bitwise of these values:<br />
    <br />
    - <code>\GDImage\AGif_Composer::OPTIMIZE_TRANSPARENT_TO_CROP</code> (1):<br />
    remove transparent pixels on frame borders to reduce frame size, allways applicable;<br />
    - <code>\GDImage\AGif_Composer::OPTIMIZE_DISPOSAL_TO_CROP</code> (2):<br />
    remove redundant pixels &mdash; from disposed frame to current one &mdash; on frame borders to reduce frame size, this optimization is not applicable if the previous frame disposal method is 2 (restore to background);<br />
    - <code>\GDImage\AGif_Composer::OPTIMIZE_DISPOSAL_TO_TRANSPARENT</code> (4):<br />
    replace redundant pixels &mdash; from disposed frame to current one &mdash; of the frame by transparent color to reduce frame colors, this optimization is not applicable if the previous frame disposal method is 2 (restore to background);.<br />
    <br />
    These optimizations will reduce the file size but if there is a lot of large frames, the optimization will take ages: we have to analyse each pixels of each frames.<br />
    Using this setting really depends on what you expect from AGIF generation: slow and efficient or fast and furious&hellip;<br />
    We advice to enable optimization when AGIF are generated from a cron job, and disable it when APNG are generated on demand.<br />
    By default, this setting is set to <code>0</code>, so there is no optimization.<br />
  </i></p>
  
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'agif/ajaxload_info.gif';
  $export_filepath1 = TMP_DIR.'00215_ajaxload_info_a.gif';
  $export_filepath2 = TMP_DIR.'00215_ajaxload_info_b.gif';
  $export_filepath3 = TMP_DIR.'00215_ajaxload_info_c.gif';
  $export_filepath4 = TMP_DIR.'00215_ajaxload_info_d.gif';
  $export_filepath5 = TMP_DIR.'00215_ajaxload_info_e.gif';
  $export_filepath6 = TMP_DIR.'00215_ajaxload_info_f.gif';
  $export_filepath7 = TMP_DIR.'00215_ajaxload_info_g.gif';
  $export_filepath8 = TMP_DIR.'00215_ajaxload_info_h.gif';
  
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAGifComposerOptimization( \PHP_INT_MAX );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath1 );
  $export_time1 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAGifComposerOptimization( 0 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath2 );
  $export_time2 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAGifComposerOptimization( 1 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath3 );
  $export_time3 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAGifComposerOptimization( 2 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath4 );
  $export_time4 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAGifComposerOptimization( 4 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath5 );
  $export_time5 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAGifComposerOptimization( 3 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath6 );
  $export_time6 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAGifComposerOptimization( 5 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath7 );
  $export_time7 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAGifComposerOptimization( 6 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath8 );
  $export_time8 = microtime( true ) - $start;
  
  $code = '\\GDImage\\Config::setAGifComposerOptimization( $optimize );'."\n"
         ."\n"
         .'$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'."\n";
  
?>
  <h2>Test case 1: Optimization on similar frames with opaque background</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://ajaxload.info/" target="_blank">ajaxload.info</a>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath1); ?>" alt="<?php echo basename($export_filepath1); ?>" title="<?php echo basename($export_filepath1); ?>" />
          <figcaption>
            All optimizations <i>7</i> (<?php echo filesize_to_human( $export_filepath1 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time1 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath2); ?>" alt="<?php echo basename($export_filepath2); ?>" title="<?php echo basename($export_filepath2); ?>" />
          <figcaption>
            No optimization <i>0</i> (<?php echo filesize_to_human( $export_filepath2 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time2 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath3); ?>" alt="<?php echo basename($export_filepath3); ?>" title="<?php echo basename($export_filepath3); ?>" />
          <figcaption>
            Optimization value <i>1</i> (<?php echo filesize_to_human( $export_filepath3 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time3 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath4); ?>" alt="<?php echo basename($export_filepath4); ?>" title="<?php echo basename($export_filepath4); ?>" />
          <figcaption>
            Optimization value <i>2</i> (<?php echo filesize_to_human( $export_filepath4 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time4 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath5); ?>" alt="<?php echo basename($export_filepath5); ?>" title="<?php echo basename($export_filepath5); ?>" />
          <figcaption>
            Optimization value <i>4</i> (<?php echo filesize_to_human( $export_filepath5 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time5 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath6); ?>" alt="<?php echo basename($export_filepath6); ?>" title="<?php echo basename($export_filepath6); ?>" />
          <figcaption>
            Optimization value <i>3</i> (<?php echo filesize_to_human( $export_filepath6 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time6 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath8); ?>" alt="<?php echo basename($export_filepath8); ?>" title="<?php echo basename($export_filepath8); ?>" />
          <figcaption>
            Optimization value <i>6</i> (<?php echo filesize_to_human( $export_filepath8 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time8 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath7); ?>" alt="<?php echo basename($export_filepath7); ?>" title="<?php echo basename($export_filepath7); ?>" />
          <figcaption>
            Optimization value <i>5</i> (<?php echo filesize_to_human( $export_filepath7 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time7 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'agif/preloaders_net.gif';
  $export_filepath1 = TMP_DIR.'00215_preloaders_net_a.gif';
  $export_filepath2 = TMP_DIR.'00215_preloaders_net_b.gif';
  $export_filepath3 = TMP_DIR.'00215_preloaders_net_c.gif';
  $export_filepath4 = TMP_DIR.'00215_preloaders_net_d.gif';
  $export_filepath5 = TMP_DIR.'00215_preloaders_net_e.gif';
  $export_filepath6 = TMP_DIR.'00215_preloaders_net_f.gif';
  $export_filepath7 = TMP_DIR.'00215_preloaders_net_g.gif';
  $export_filepath8 = TMP_DIR.'00215_preloaders_net_h.gif';
  
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAGifComposerOptimization( \PHP_INT_MAX );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath1 );
  $export_time1 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAGifComposerOptimization( 0 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath2 );
  $export_time2 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAGifComposerOptimization( 1 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath3 );
  $export_time3 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAGifComposerOptimization( 2 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath4 );
  $export_time4 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAGifComposerOptimization( 4 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath5 );
  $export_time5 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAGifComposerOptimization( 3 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath6 );
  $export_time6 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAGifComposerOptimization( 5 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath7 );
  $export_time7 = microtime( true ) - $start;
  
  // give me some time
  set_time_limit( 120 );
  // enable optimization
  \GDImage\Config::setAGifComposerOptimization( 6 );
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath8 );
  $export_time8 = microtime( true ) - $start;
  
  $code = '\\GDImage\\Config::setAGifComposerOptimization( $optimize );'."\n"
         ."\n"
         .'$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'."\n";
  
?>
  <h2>Test case 2: Optimization on distinct frames with transparent background</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://preloaders.net/" target="_blank">preloaders.net</a>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath1); ?>" alt="<?php echo basename($export_filepath1); ?>" title="<?php echo basename($export_filepath1); ?>" />
          <figcaption>
            All optimizations <i>7</i> (<?php echo filesize_to_human( $export_filepath1 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time1 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath2); ?>" alt="<?php echo basename($export_filepath2); ?>" title="<?php echo basename($export_filepath2); ?>" />
          <figcaption>
            No optimization <i>0</i> (<?php echo filesize_to_human( $export_filepath2 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time2 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath3); ?>" alt="<?php echo basename($export_filepath3); ?>" title="<?php echo basename($export_filepath3); ?>" />
          <figcaption>
            Optimization value <i>1</i> (<?php echo filesize_to_human( $export_filepath3 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time3 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath4); ?>" alt="<?php echo basename($export_filepath4); ?>" title="<?php echo basename($export_filepath4); ?>" />
          <figcaption>
            Optimization value <i>2</i> (<?php echo filesize_to_human( $export_filepath4 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time4 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath5); ?>" alt="<?php echo basename($export_filepath5); ?>" title="<?php echo basename($export_filepath5); ?>" />
          <figcaption>
            Optimization value <i>4</i> (<?php echo filesize_to_human( $export_filepath5 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time5 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath6); ?>" alt="<?php echo basename($export_filepath6); ?>" title="<?php echo basename($export_filepath6); ?>" />
          <figcaption>
            Optimization value <i>3</i> (<?php echo filesize_to_human( $export_filepath6 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time6 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath8); ?>" alt="<?php echo basename($export_filepath8); ?>" title="<?php echo basename($export_filepath8); ?>" />
          <figcaption>
            Optimization value <i>6</i> (<?php echo filesize_to_human( $export_filepath8 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time8 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath7); ?>" alt="<?php echo basename($export_filepath7); ?>" title="<?php echo basename($export_filepath7); ?>" />
          <figcaption>
            Optimization value <i>5</i> (<?php echo filesize_to_human( $export_filepath7 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time7 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'agif/photoshop.gif';
  $export_filepath1 = TMP_DIR.'00215_photoshop_a.gif';
  $export_filepath2 = TMP_DIR.'00215_photoshop_b.gif';
  
  // give me some time
  set_time_limit( 120 );
  
  // enable optimization
  \GDImage\Config::setAGifComposerOptimization( \GDImage\AGif_Composer::OPTIMIZE_ALL );
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath1 );
  $export_time1 = microtime( true ) - $start;
  
  // import
  $start = microtime( true );
  \GDImage\Config::setAGifComposerOptimization( \GDImage\AGif_Composer::OPTIMIZE_ALL );
  $image = \GDImage\Factory::import( $import_filepath );
  foreach( $image->getFrames() as $frame ) $frame->setDisposalMethod( 2 );
  \GDImage\Factory::export( $image , $export_filepath2 );
  $export_time2 = microtime( true ) - $start;
  
  $code1 = '\\GDImage\\Config::setAGifComposerOptimization( \\GDImage\\AGif_Composer::OPTIMIZE_ALL ); // enable optimization'."\n"
          ."\n"
          .'$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
          .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'."\n";
  
  $code2 = '\\GDImage\\Config::setAGifComposerOptimization( \\GDImage\\AGif_Composer::OPTIMIZE_ALL ); // enable optimization'."\n"
          ."\n"
          .'$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
          .'foreach( $image->getFrames() as $frame ) $frame->setDisposalMethod( 2 ); // restore to background'."\n"
          .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'."\n";
?>
  <h2>Test case 3: Disposal Method impact on optimized recomposition</h2>
  
  <pre><?php echo $code1; ?></pre>
  
  <pre><?php echo $code2; ?></pre>
  
  <p><i>
    You will notice that Disposal Method 1 (do not dispose) results to an optimization: the generation takes ages but the file size is acceptable.<br />
    With Disposal Method 2 (restore to background), there is no optimization and we can reduce drastically the AGIF generation time, but the file size is bigger.<br />
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath1); ?>" alt="<?php echo basename($export_filepath1); ?>" title="<?php echo basename($export_filepath1); ?>" />
          <figcaption>
            Disposal method 1 (<?php echo filesize_to_human( $export_filepath1 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time1 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath2); ?>" alt="<?php echo basename($export_filepath2); ?>" title="<?php echo basename($export_filepath2); ?>" />
          <figcaption>
            Disposal method 2 (<?php echo filesize_to_human( $export_filepath2 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time2 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
  <p><i>
    Keep in mind that this library has been created for hosting that does not have Imagick extension.<br />
    With Imagick, optimizing an animated GIF should be as simple as these few lines of codes:
  </i></p>
  
  <pre>$img = new Imagick($import_filepath);
$img->optimizeImageLayers();
$img->writeImages($export_filepath, true);</pre>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
