<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

\GDImage\Config::helper();

?>
<html>
<head>
  <title>GDImage: Test 00500 - gd_image() helper</title>
  <?php require '_head.partial.php'; ?>
  <style>
    small { white-space: nowrap; }
  </style>
</head>
<body>
  
  <h1>GDImage: Test 00500 - gd_image() helper</h1>
  
  <pre>// load \gd_image() helper
\GDImage\Config::helper();</pre>
  
<?php /**/
  // Test case 1

  // copy to tmp directory
  copy( SAMP_DIR.'tulips.256c.gif' , TMP_DIR.'00500_tulips.256c.gif' );
  $import_filepath = TMP_DIR.'00500_tulips.256c.gif';
  $final_filepath = array(
    gd_image( $import_filepath , '_suffix1' ) ,
    gd_image( $import_filepath , '_suffix2' , new \GDImage\Transform_Grayscale() ) ,
  );

?>
  <h2>Test case 1: Save to same directory with suffix</h2>
  
  <pre>$final_filepath = gd_image( $import_filepath , '_suffix1' );
$final_filepath = gd_image( $import_filepath , '_suffix2' , new \GDImage\Transform_Grayscale() );</pre>
  
  <p><i>
    Suffix must start by "_" or "-".<br />
    Suffix has always priority against transformation.<br />
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            <i><?php echo basename($import_filepath); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath[0]); ?>" alt="<?php echo basename($final_filepath[0]); ?>" title="<?php echo basename($final_filepath[0]); ?>" />
          <figcaption>
            Result<br />
            <i><?php echo basename($final_filepath[0]); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath[1]); ?>" alt="<?php echo basename($final_filepath[1]); ?>" title="<?php echo basename($final_filepath[1]); ?>" />
          <figcaption>
            Result<br />
            <i><?php echo basename($final_filepath[1]); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 2

  // copy to tmp directory
  copy( SAMP_DIR.'hydrangeas.128c.8.png' , TMP_DIR.'00500_hydrangeas.128c.8.png' );
  $import_filepath = TMP_DIR.'00500_hydrangeas.128c.8.png';
  
  \GDImage\Transform_Collection::set( '200x200' , new \GDImage\Transform_Resize_Crop( 200 , 200 ) );
  $final_filepath = gd_image( $import_filepath , null , '200x200' );

?>
  <h2>Test case 2: Save to same directory with automatic suffix from registered transformation</h2>
  
  <pre>\GDImage\Transform_Collection::set( '200x200' , new \GDImage\Transform_Resize_Crop( 200 , 200 ) );
(...)
$final_filepath = gd_image( $import_filepath , null , '200x200' );</pre>
  
  <p><i>
    Keep in mind that you can register a multiple transformation in a single key.<br />
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            <i><?php echo basename($import_filepath); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath); ?>" alt="<?php echo basename($final_filepath); ?>" title="<?php echo basename($final_filepath); ?>" />
          <figcaption>
            Result<br />
            <i><?php echo basename($final_filepath); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 3

  // copy to tmp directory
  copy( SAMP_DIR.'jellyfish.16c.8.png' , TMP_DIR.'00500_jellyfish.16c.8.png' );
  $import_filepath = TMP_DIR.'00500_jellyfish.16c.8.png';
  
  $final_filepath = array(
    gd_image( $import_filepath , null , 'Sepia' ) ,
    gd_image( $import_filepath , null , 'Transform_Grayscale' ) ,
    gd_image( $import_filepath , null , '\\GDImage\\Transform_Negate' ) ,
  );

?>
  <h2>Test case 3: Save to same directory with automatic suffix from "named" transformations</h2>
  
  <pre>$final_filepath = gd_image( $import_filepath , null , 'Sepia' );
$final_filepath = gd_image( $import_filepath , null , 'Transform_Grayscale' );
$final_filepath = gd_image( $import_filepath , null , '\\GDImage\\Transform_Negate' );</pre>
  
  <p><i>
    Note that "named" transformations cannot received parameters but it also works with custom transformations.<br />
    This behavior also apply with transformation instance.<br />
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            <i><?php echo basename($import_filepath); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath[0]); ?>" alt="<?php echo basename($final_filepath[0]); ?>" title="<?php echo basename($final_filepath[0]); ?>" />
          <figcaption>
            Result<br />
            <i><?php echo basename($final_filepath[0]); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath[1]); ?>" alt="<?php echo basename($final_filepath[1]); ?>" title="<?php echo basename($final_filepath[1]); ?>" />
          <figcaption>
            Result<br />
            <i><?php echo basename($final_filepath[1]); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath[2]); ?>" alt="<?php echo basename($final_filepath[2]); ?>" title="<?php echo basename($final_filepath[2]); ?>" />
          <figcaption>
            Result<br />
            <i><?php echo basename($final_filepath[2]); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 4

  $import_filepath = SAMP_DIR.'desert.24.png';
  
  \GDImage\Transform_Collection::set( 'fliph' , new \GDImage\Transform_Flip( 'h' ) );
  $final_filepath = gd_image( $import_filepath , TMP_DIR , 'fliph' );

?>
  <h2>Test case 4: Save to another directory with automatic suffix from registered transformation</h2>
  
  <pre>\GDImage\Transform_Collection::set( 'fliph' , new \GDImage\Transform_Flip( 'h' ) );
(...)
$final_filepath = gd_image( $import_filepath , $directory , 'fliph' );</pre>
  
  <p><i>You will notice that the file is generated only once.</i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            <i><?php echo basename($import_filepath); ?></i><br />
            <i><?php echo date( 'Y/m/d H:i:s' , filemtime( $import_filepath ) ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath); ?>" alt="<?php echo basename($final_filepath); ?>" title="<?php echo basename($final_filepath); ?>" />
          <figcaption>
            Result<br />
            <i><?php echo basename($final_filepath); ?></i><br />
            <i><?php echo date( 'Y/m/d H:i:s' , filemtime( $final_filepath ) ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 5

  $import_filepath = SAMP_DIR.'chrysanthemum.jpg';
  $export_filepath = TMP_DIR.'00500_chrysanthemum';
  $final_filepath = gd_image( $import_filepath , $export_filepath , 'Negate' );

?>
  <h2>Test case 5: Save to another file with extension guessing.</h2>
  
  <pre>$final_filepath = gd_image( $import_filepath , $export_filepath_without_extension , 'Negate' );</pre>
  
  <p><i>
    You will notice that the file is generated only once.<br />
    Also, when a file name is specified, even without extension, no suffix is added.
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            <i><?php echo basename($import_filepath); ?></i><br />
            <i><?php echo date( 'Y/m/d H:i:s' , filemtime( $import_filepath ) ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath); ?>" alt="<?php echo basename($final_filepath); ?>" title="<?php echo basename($final_filepath); ?>" />
          <figcaption>
            Result<br />
            <i><?php echo basename($final_filepath); ?></i><br />
            <i><?php echo date( 'Y/m/d H:i:s' , filemtime( $final_filepath ) ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 6

  $import_filepath = SAMP_DIR.'lighthouse.24.png';
  $export_filepath = TMP_DIR.'00500_lighthouse.gif';
  
  $transform = new \GDImage\Transform_Resize_FitNFill( 200 , 200 , 'FF00007F' );
  $final_filepath = gd_image( $import_filepath , $export_filepath , $transform );

?>
  <h2>Test case 6: Save to another directory with another extension.</h2>
  
  <pre>$transform = new \GDImage\Transform_Resize_FitNFill( 200 , 200 , 'FF00007F' );
$final_filepath = gd_image( $import_filepath , $export_filepath , $transform );</pre>
  
  <p><i>You will notice that the file is generated only once.</i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            <i><?php echo basename($import_filepath); ?></i><br />
            <i><?php echo date( 'Y/m/d H:i:s' , filemtime( $import_filepath ) ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath); ?>" alt="<?php echo basename($final_filepath); ?>" title="<?php echo basename($final_filepath); ?>" />
          <figcaption>
            Result<br />
            <i><?php echo basename($final_filepath); ?></i><br />
            <i><?php echo date( 'Y/m/d H:i:s' , filemtime( $final_filepath ) ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 7

  $import_filepath = 'http://lorempixel.com/512/384/nature/2/lorempixel%20dot%20com/';
  $final_filepath = gd_image( $import_filepath , TMP_DIR , 'Sharpen' );
  
  // lorempixel never returns a Last-modified header, sad...
  
?>
  <h2>Test case 7: Save from HTTP to directory with transformation.</h2>
  
  <pre>$final_filepath = gd_image( $import_filepath , $directory , 'Sharpen' );</pre>
  
  <p><i>
    Unfortunately, <a href="http://lorempixel.com/" target="_blank">lorempixel.com</a> does not return a "Last-modified" header, if it were the case, result would be generated once.
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="<?php echo $import_filepath; ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            <i><?php echo $import_filepath; ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo urlencode( basename($final_filepath) ); ?>" alt="<?php echo basename($final_filepath); ?>" title="<?php echo basename($final_filepath); ?>" />
          <figcaption>
            Result<br />
            <i><?php echo basename($final_filepath); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 8

  $data_uri = 'data:image/gif;base64,'.base64_encode( file_get_contents( SAMP_DIR.'tulips.256c.gif' ) );
  $temporary_filepath = gd_image( $data_uri , null );
  copy( $temporary_filepath , TMP_DIR.basename( $temporary_filepath ) );

?>
  <h2>Test case 8: Save from data URI to temporary directory.</h2>
  
  <pre>$temporary_filepath = gd_image( $data_uri , null );</pre>
  
  <table>
    <tr>
      <td>
        <pre style="overflow-x: scroll;"><?php echo implode( "\n" , str_split( $data_uri , ceil( strlen( $data_uri ) / 15 ) ) ); ?></pre>
        <b>Original data URI</b>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($temporary_filepath); ?>" alt="<?php echo basename($temporary_filepath); ?>" title="<?php echo basename($temporary_filepath); ?>" />
          <figcaption>
            Result<br />
            <i><?php echo basename($temporary_filepath); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 9

  $base64 = base64_encode( file_get_contents( SAMP_DIR.'hydrangeas.128c.8.png' ) );
  $final_filepath = gd_image( $base64 , TMP_DIR , 'Sepia' );

?>
  <h2>Test case 9: Save from base64 data to directory with transformation.</h2>
  
  <pre>$final_filepath = gd_image( $base64 , $directory , 'Sepia' );</pre>
  
  <table>
    <tr>
      <td>
        <pre style="overflow-x: scroll;"><?php echo implode( "\n" , str_split( $base64 , ceil( strlen( $base64 ) / 15 ) ) ); ?></pre>
        <b>Original base64 data</b>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($final_filepath); ?>" alt="<?php echo basename($final_filepath); ?>" title="<?php echo basename($final_filepath); ?>" />
          <figcaption>
            Result<br />
            <i><?php echo basename($final_filepath); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 10

  $import_filepath = 'http://lorempixel.com/512/384/nature/3/lorempixel%20dot%20com/';
  $data_uri = gd_image( $import_filepath , array( 
    'driver' => 'data' , 
    'mimetype' => false ,
  ) );

?>
  <h2>Test case 10: HTTP to data URI without MIME Type.</h2>
  
  <pre>$data_uri = gd_image( $import_filepath , array( 
  'driver' => 'data' , 
  'mimetype' => false ,
) );</pre>
  
  <p><i>
    As expected, any available export can be used. 
  </i></p>
  
  <table>
    <tr>
      <td>
        <figure>
          <img src="<?php echo $import_filepath; ?>" alt="<?php echo basename($final_filepath); ?>" title="<?php echo basename($final_filepath); ?>" />
          <figcaption>
            Original<br />
            <i><?php echo $import_filepath; ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <pre style="overflow-x: scroll;"><?php echo implode( "\n" , str_split( $data_uri , ceil( strlen( $data_uri ) / 15 ) ) ); ?></pre>
        <b>Result data URI</b>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
