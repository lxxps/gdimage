<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

$code1 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
        .'$image->apply( new \\GDImage\\Transform_Grayscale() );'."\n"
        .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

$code2 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
        .'$image->apply( new \\GDImage\\Transform_Sepia() );'."\n"
        .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

$code3 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
        .'$transform = new \\GDImage\\Transform_Filter( array('."\n"
        .'  //     R    G    B    A'."\n"
        .'  array( 1  , 0  , 0  , 0 ), // red stay red'."\n"
        .'  array( 0  , 0  , 0  , 0 ), // green is removed'."\n"
        .'  array( 0  , 0  , 1  , 0 ), // blue stay blue'."\n"
        .'  array( 0  , .2 , 0  , 0 ), // green become 20% alpha'."\n"
        .') );'."\n"
        .'$image->apply( $transform );'."\n"
        .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

$code4 = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
        .'$transform = new \\GDImage\\Transform_Filter( array('."\n"
        .'  //     R    G    B    A'."\n"
        .'  array( .6 , .4 , 0  , 0  ),'."\n"
        .'  array( 0  , .6 , .4 , 0  ),'."\n"
        .'  array( .4 , 0  , .6 , 0  ),'."\n"
        .'  array( 0  , 0  , 0  , .5 ), // alpha is now half'."\n"
        .') );'."\n"
        .'$image->apply( $transform );'."\n"
        .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

$grayscale = new \GDImage\Transform_Grayscale();

$sepia = new \GDImage\Transform_Sepia();

$custom1 = new \GDImage\Transform_Filter( array(
  //     R    G    B    A
  array( 1  , 0  , 0  , 0 ), // red is removed
  array( 0  , 0  , 0  , 0 ), // green is removed
  array( 0  , 0  , 1  , 0 ), // blue stay blue
  array( 0  , .2 , 0  , 0 ), // green become 20% alpha
) );

$custom2 = new \GDImage\Transform_Filter( array(
  //      R    G   B    A
  array( .6 , .4 , 0  , 0  ),
  array( 0  , .6 , .4 , 0  ),
  array( .4  , 0 , .6 , 0  ),
  array( 0  , 0  , 0  , .5 ), // alpha is now half
) );

?>
<html>
<head>
  <title>GDImage: Test 00060 - Color filter on palette color</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00060 - Color filter on palette color</h1>
  
  <p><i>You will notice that on palette color, the filter is quick. In fact, we just need to analyze colors from palette and not for each pixels.</i></p>
  
  <pre><?php echo htmlspecialchars( $code1 ); ?></pre>
  <pre><?php echo htmlspecialchars( $code2 ); ?></pre>
  <pre><?php echo htmlspecialchars( $code3 ); ?></pre>
  <pre><?php echo htmlspecialchars( $code4 ); ?></pre>
  
  <hr />
  
<?php
  // Test case 1

  $import_filepath = SAMP_DIR.'hydrangeas.128c.8.png';
  $export_filepath = array( 
    'grayscale' => TMP_DIR.'00060_hydrangeas_a1' ,   
    'sepia' => TMP_DIR.'00060_hydrangeas_a2' , 
    'custom1' => TMP_DIR.'00060_hydrangeas_b1' ,   
    'custom2' => TMP_DIR.'00060_hydrangeas_b2' ,  
  );
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepath as $key => &$path )
  {
    $clone = clone $image;
    $clone->apply( ${$key} );
    $path = \GDImage\Factory::export( $clone , $path );
  }
  
?>
  <h2>Test case 1: PNG8 with <?php echo $image->getResource()->countColors() ; ?> colors</h2>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['grayscale']); ?>" alt="<?php echo basename($export_filepath['grayscale']); ?>" title="<?php echo basename($export_filepath['grayscale']); ?>" />
          <figcaption>Grayscale</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['sepia']); ?>" alt="<?php echo basename($export_filepath['sepia']); ?>" title="<?php echo basename($export_filepath['sepia']); ?>" />
          <figcaption>Sepia</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['custom1']); ?>" alt="<?php echo basename($export_filepath['custom1']); ?>" title="<?php echo basename($export_filepath['custom1']); ?>" />
          <figcaption>Custom 1</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['custom2']); ?>" alt="<?php echo basename($export_filepath['custom2']); ?>" title="<?php echo basename($export_filepath['custom2']); ?>" />
          <figcaption>Custom 2</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php
  // Test case 2

  $import_filepath = SAMP_DIR.'tulips.256c.gif';
  $export_filepath = array( 
    'grayscale' => TMP_DIR.'00060_tulips_a1' ,   
    'sepia' => TMP_DIR.'00060_tulips_a2' , 
    'custom1' => TMP_DIR.'00060_tulips_b1' ,   
    'custom2' => TMP_DIR.'00060_tulips_b2' ,  
  );
  $export_transparent = array();
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  list( $r, $g, $b, $a ) = $image->getResource()->getTransparent()->toArray();
  
  foreach( $export_filepath as $key => &$path )
  {
    $clone = clone $image;
    $clone->apply( ${$key} );
    $path = \GDImage\Factory::export( $clone , $path );
    $export_transparent[$key] = $clone->getResource()->getTransparent()->toArray();
  }
  
?>
  <h2>Test case 2: GIF with <?php echo $image->getResource()->countColors() ; ?> colors</h2>
  
  <p><i>You will notice that the transparent color has also been modified.</i></p>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            <i>transparent color: <?php echo sprintf( 'rgba(%d,%d,%d,%d)' , $r , $g , $b , $a ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['grayscale']); ?>" alt="<?php echo basename($export_filepath['grayscale']); ?>" title="<?php echo basename($export_filepath['grayscale']); ?>" />
          <figcaption>
            Grayscale<br />
            <i>transparent color: <?php array_unshift( $export_transparent['grayscale'] , 'rgba(%d,%d,%d,%d)' ); echo call_user_func_array( 'sprintf' , $export_transparent['grayscale'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['sepia']); ?>" alt="<?php echo basename($export_filepath['sepia']); ?>" title="<?php echo basename($export_filepath['sepia']); ?>" />
          <figcaption>
            Sepia<br />
            <i>transparent color: <?php array_unshift( $export_transparent['sepia'] , 'rgba(%d,%d,%d,%d)' ); echo call_user_func_array( 'sprintf' , $export_transparent['sepia'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['custom1']); ?>" alt="<?php echo basename($export_filepath['custom1']); ?>" title="<?php echo basename($export_filepath['custom1']); ?>" />
          <figcaption>
            Custom 1<br />
            <i>transparent color: <?php array_unshift( $export_transparent['custom1'] , 'rgba(%d,%d,%d,%d)' ); echo call_user_func_array( 'sprintf' , $export_transparent['custom1'] ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['custom2']); ?>" alt="<?php echo basename($export_filepath['custom2']); ?>" title="<?php echo basename($export_filepath['custom2']); ?>" />
          <figcaption>
            Custom 2<br />
            <i>transparent color: <?php array_unshift( $export_transparent['custom2'] , 'rgba(%d,%d,%d,%d)' ); echo call_user_func_array( 'sprintf' , $export_transparent['custom2'] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php
  // Test case 2

  $import_filepath = SAMP_DIR.'jellyfish.16c.8.png';
  $export_filepath = array( 
    'grayscale' => TMP_DIR.'00060_jellyfish_a1' ,   
    'sepia' => TMP_DIR.'00060_jellyfish_a2' , 
    'custom1' => TMP_DIR.'00060_jellyfish_b1' ,   
    'custom2' => TMP_DIR.'00060_jellyfish_b2' ,  
  );
  
  // import
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( $export_filepath as $key => &$path )
  {
    $clone = clone $image;
    $clone->apply( ${$key} );
    $path = \GDImage\Factory::export( $clone , $path );
  }
  
?>
  <h2>Test case 3: PNG8 with <?php echo $image->getResource()->countColors() ; ?> colors</h2>
  
  <table>
    <tr>
      <td colspan="2" rowspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>Original picture</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['grayscale']); ?>" alt="<?php echo basename($export_filepath['grayscale']); ?>" title="<?php echo basename($export_filepath['grayscale']); ?>" />
          <figcaption>Grayscale</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['sepia']); ?>" alt="<?php echo basename($export_filepath['sepia']); ?>" title="<?php echo basename($export_filepath['sepia']); ?>" />
          <figcaption>Sepia</figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['custom1']); ?>" alt="<?php echo basename($export_filepath['custom1']); ?>" title="<?php echo basename($export_filepath['custom1']); ?>" />
          <figcaption>Custom 1</figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath['custom2']); ?>" alt="<?php echo basename($export_filepath['custom2']); ?>" title="<?php echo basename($export_filepath['custom2']); ?>" />
          <figcaption>Custom 2</figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php require '_foot.partial.php'; ?>
  
</body>
</html>
        
