<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

$files = glob( dirname(__FILE__).DIRECTORY_SEPARATOR.'test*.php' );

sort( $files , SORT_STRING );

function file_to_human( $file )
{
  $matches = array();
  if( preg_match( '~test([0-9]+)_(.+)\.php~' , $file , $matches ) )
  {
    return 'Test '.$matches[1].': '.$matches[2];
  }
  
  return $file;
}

?>

<nav>
  <h3>All tests</h3>
  <ul>
    <?php foreach( $files as $file ): ?>
      <li><a href="./<?php echo basename( $file ); ?>"><?php echo file_to_human( basename( $file ) ); ?></a></li>
    <?php endforeach; ?>
  </ul>
</nav>

