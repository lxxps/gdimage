<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

?>
<html>
<head>
  <title>GDImage: Test 00220 - AGIF Concrete Case</title>
  <?php require '_head.partial.php'; ?>
  <style>
    small { white-space: nowrap; }
  </style>
</head>
<body>
  
  <h1>GDImage: Test 00220 - AGIF Concrete Case</h1>
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'agif/glob_bargeo_fr.gif';
  $export_basepath = TMP_DIR.'00220_glob_bargeo_fr';
  
  // get pack to display usefull info
  $pack = \GDImage\AGif_Factory::unpack( $import_filepath );
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  $final_filepath = array();
  foreach( $image->getFrames() as $i => $frame )
  {
    $final_filepath[] = \GDImage\Factory::export( $frame , $export_basepath.sprintf( '%02d' , $i ) );
  }
  $export_time = microtime( true ) - $start;
  
  $nb_cols = 5;

  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$final_filepath = array();'."\n"
         .'foreach( $image->getFrames() as $i => $frame )'."\n"
         .'{'."\n"
         .'  $final_filepath[] = \\GDImage\\Factory::export( $frame , $export_basepath.sprintf( \'%02d\' , $i ) );'."\n"
         .'}';
  
?>
  <h2>Test case 1: Decomposition</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    Use Global Color Table for each frame.<br />
    Use Transparent Color for each frame except first one.<br />
    Each frame fits Logical Screen Descriptor.<br />
    The animation is optimized helping Disposal Method 1 (do not dispose).<br />
    The Background Color Index is not 0.<br />
    <!-- More information at <a href="http://www.w3.org/Graphics/GIF/spec-gif89a.txt" target="_blank">http://www.w3.org/Graphics/GIF/spec-gif89a.txt</a>. -->
  </i></p>
  
  <table>
    <tr>
      <td colspan="<?php echo $nb_cols; ?>">
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            from <a href="http://glob.bargeo.fr/" target="_blank">glob.bargeo.fr</a><br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
        <br />
        <small>
          Width: <?php echo $pack['Logical Screen Descriptor']['Logical Screen Width']; ?><br />
          Height: <?php echo $pack['Logical Screen Descriptor']['Logical Screen Height']; ?><br />
          Global Color Table: <?php echo $pack['Logical Screen Descriptor']['Packed']['Global Color Table Flag'] ? 'yes' : 'no'; ?><br />
          Number of Repetitions: <?php echo isset( $pack['Application Extension0']['Number of Repetitions'] ) ? ( $pack['Application Extension0']['Number of Repetitions'] ? $pack['Application Extension0']['Number of Repetitions'] : '&infin;' ) : 'none'; ?><br />
        </small>
      </td>
    </tr>
    <?php $i = 0; foreach( $final_filepath as $filepath ): ?>
      <?php $i % $nb_cols === 0 && print '<tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Frame <?php echo $i+1; ?>
            </figcaption>
          </figure>
          <br />
          <small>
            Width: <?php echo $pack['Image Descriptor'.$i]['Image Width']; ?><br />
            Height: <?php echo $pack['Image Descriptor'.$i]['Image Height']; ?><br />
            Left: <?php echo $pack['Image Descriptor'.$i]['Image Left Position']; ?><br />
            Top: <?php echo $pack['Image Descriptor'.$i]['Image Top Position']; ?><br />
            Local Color Table: <?php echo $pack['Image Descriptor'.$i]['Packed']['Local Color Table Flag'] ? 'yes' : 'no'; ?><br />
            <?php if( isset($pack['Graphic Control Extension'.$i]) ): ?>
              Delay Time: <?php echo $pack['Graphic Control Extension'.$i]['Delay Time']; ?><br />
              Disposal Method: <?php echo $pack['Graphic Control Extension'.$i]['Packed']['Disposal Method']; ?><br />
              Transparent Color: <?php echo $pack['Graphic Control Extension'.$i]['Packed']['Transparent Color Flag'] ? 'yes' : 'no'; ?><br />
            <?php endif; ?>
          </small>
        </td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endforeach; ?>
    <?php while( $i % $nb_cols !== 0 ): ?>
      <td></td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endwhile; ?>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'agif/glob_bargeo_fr.gif';
  $export_filepath1 = TMP_DIR.'00220_glob_bargeo_fra.gif';
  $export_filepath2 = TMP_DIR.'00220_glob_bargeo_frb.gif';
  $export_filepath3 = TMP_DIR.'00220_glob_bargeo_frc.gif';
  
  // give me some time
  set_time_limit( 120 );
  
  // import
  $start = microtime( true );
  \GDImage\Config::setAGifComposerOptimization( \GDImage\AGif_Composer::OPTIMIZE_ALL );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath1 );
  $export_time1 = microtime( true ) - $start;
  
  // import
  $start = microtime( true );
  \GDImage\Config::setAGifComposerOptimization( \GDImage\AGif_Composer::OPTIMIZE_CROP );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath2 );
  $export_time2 = microtime( true ) - $start;
  
  // import
  $start = microtime( true );
  \GDImage\Config::setAGifComposerOptimization( \GDImage\AGif_Composer::OPTIMIZE_NO );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath3 );
  $export_time3 = microtime( true ) - $start;
  
  $code = '\\GDImage\\Config::setAGifComposerOptimization( $optimize );'."\n"
         ."\n"
         .'$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'."\n";
?>
  <h2>Test case 2: Optimization</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    You will notice that crop optimization (3) is not efficient in that case: the changing area is too large compare to the frame area.<br />
  </i></p>
  
  <table>
    <tr>
      <td rowspan="2">
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://glob.bargeo.fr/" target="_blank">glob.bargeo.fr</a><br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath1); ?>" alt="<?php echo basename($export_filepath1); ?>" title="<?php echo basename($export_filepath1); ?>" />
          <figcaption>
            Full optimization (<?php echo filesize_to_human( $export_filepath1 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time1 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath2); ?>" alt="<?php echo basename($export_filepath2); ?>" title="<?php echo basename($export_filepath2); ?>" />
          <figcaption>
            Crop optimization (<?php echo filesize_to_human( $export_filepath2 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time2 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath3); ?>" alt="<?php echo basename($export_filepath3); ?>" title="<?php echo basename($export_filepath3); ?>" />
          <figcaption>
            No optimization (<?php echo filesize_to_human( $export_filepath3 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time3 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 3

  $import_filepath = SAMP_DIR.'agif/lesjoiesducode_fr.gif';
  $export_basepath = TMP_DIR.'00220_lesjoiesducode_fr';
  
  // get pack to display usefull info
  $pack = \GDImage\AGif_Factory::unpack( $import_filepath );
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  $final_filepath = array();
  foreach( $image->getFrames() as $i => $frame )
  {
    $final_filepath[] = \GDImage\Factory::export( $frame , $export_basepath.sprintf( '%02d' , $i ) );
  }
  $export_time = microtime( true ) - $start;
  
  $nb_cols = 5;

  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$final_filepath = array();'."\n"
         .'foreach( $image->getFrames() as $i => $frame )'."\n"
         .'{'."\n"
         .'  $final_filepath[] = \\GDImage\\Factory::export( $frame , $export_basepath.sprintf( \'%02d\' , $i ) );'."\n"
         .'}';
  
?>
  <h2>Test case 3: Decomposition</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    Use Global Color Table for each frame.<br />
    Use Transparent Color for each frame except first one.<br />
    Each frame fits Logical Screen Descriptor.<br />
    The animation is optimized helping Disposal Method 1 (do not dispose).<br />
    <!-- More information at <a href="http://www.w3.org/Graphics/GIF/spec-gif89a.txt" target="_blank">http://www.w3.org/Graphics/GIF/spec-gif89a.txt</a>. -->
  </i></p>
  
  <table>
    <tr>
      <td colspan="<?php echo $nb_cols; ?>">
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            from <a href="http://lesjoiesducode.fr/" target="_blank">lesjoiesducode.fr</a><br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
        <br />
        <small>
          Width: <?php echo $pack['Logical Screen Descriptor']['Logical Screen Width']; ?><br />
          Height: <?php echo $pack['Logical Screen Descriptor']['Logical Screen Height']; ?><br />
          Global Color Table: <?php echo $pack['Logical Screen Descriptor']['Packed']['Global Color Table Flag'] ? 'yes' : 'no'; ?><br />
          Number of Repetitions: <?php echo isset( $pack['Application Extension0']['Number of Repetitions'] ) ? ( $pack['Application Extension0']['Number of Repetitions'] ? $pack['Application Extension0']['Number of Repetitions'] : '&infin;' ) : 'none'; ?><br />
        </small>
      </td>
    </tr>
    <?php $i = 0; foreach( $final_filepath as $filepath ): ?>
      <?php $i % $nb_cols === 0 && print '<tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Frame <?php echo $i+1; ?>
            </figcaption>
          </figure>
          <br />
          <small>
            Width: <?php echo $pack['Image Descriptor'.$i]['Image Width']; ?><br />
            Height: <?php echo $pack['Image Descriptor'.$i]['Image Height']; ?><br />
            Left: <?php echo $pack['Image Descriptor'.$i]['Image Left Position']; ?><br />
            Top: <?php echo $pack['Image Descriptor'.$i]['Image Top Position']; ?><br />
            Local Color Table: <?php echo $pack['Image Descriptor'.$i]['Packed']['Local Color Table Flag'] ? 'yes' : 'no'; ?><br />
            <?php if( isset($pack['Graphic Control Extension'.$i]) ): ?>
              Delay Time: <?php echo $pack['Graphic Control Extension'.$i]['Delay Time']; ?><br />
              Disposal Method: <?php echo $pack['Graphic Control Extension'.$i]['Packed']['Disposal Method']; ?><br />
              Transparent Color: <?php echo $pack['Graphic Control Extension'.$i]['Packed']['Transparent Color Flag'] ? 'yes' : 'no'; ?><br />
            <?php endif; ?>
          </small>
        </td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endforeach; ?>
    <?php while( $i % $nb_cols !== 0 ): ?>
      <td></td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endwhile; ?>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 4

  $import_filepath = SAMP_DIR.'agif/lesjoiesducode_fr.gif';
  $export_filepath1 = TMP_DIR.'00220_lesjoiesducode_fra.gif';
  $export_filepath2 = TMP_DIR.'00220_lesjoiesducode_frb.gif';
  $export_filepath3 = TMP_DIR.'00220_lesjoiesducode_frc.gif';
  
  // give me some time
  set_time_limit( 120 );
  
  // import
  $start = microtime( true );
  \GDImage\Config::setAGifComposerOptimization( \GDImage\AGif_Composer::OPTIMIZE_ALL );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath1 );
  $export_time1 = microtime( true ) - $start;
  
  // import
  $start = microtime( true );
  \GDImage\Config::setAGifComposerOptimization( \GDImage\AGif_Composer::OPTIMIZE_CROP );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath2 );
  $export_time2 = microtime( true ) - $start;
  
  // import
  $start = microtime( true );
  \GDImage\Config::setAGifComposerOptimization( \GDImage\AGif_Composer::OPTIMIZE_NO );
  $image = \GDImage\Factory::import( $import_filepath );
  \GDImage\Factory::export( $image , $export_filepath3 );
  $export_time3 = microtime( true ) - $start;
  
  $code = '\\GDImage\\Config::setAGifComposerOptimization( $optimize );'."\n"
         ."\n"
         .'$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );'."\n";
?>
  <h2>Test case 4: Optimization</h2>
  
  <pre><?php echo $code; ?></pre>
  
  <p><i>
    You will notice that on this animated picture, crop optimization (3) is the best compromise beetween speed and effectiveness, even if it take ages.<br />
  </i></p>
  
  <table>
    <tr>
      <td rowspan="2">
        <figure>
          <img src="./samples/agif/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture (<?php echo filesize_to_human( $import_filepath ); ?>)<br />
            from <a href="http://lesjoiesducode.fr/" target="_blank">lesjoiesducode.fr</a><br />
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath1); ?>" alt="<?php echo basename($export_filepath1); ?>" title="<?php echo basename($export_filepath1); ?>" />
          <figcaption>
            Full optimization (<?php echo filesize_to_human( $export_filepath1 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time1 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath2); ?>" alt="<?php echo basename($export_filepath2); ?>" title="<?php echo basename($export_filepath2); ?>" />
          <figcaption>
            Crop optimization (<?php echo filesize_to_human( $export_filepath2 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time2 ); ?></i>
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath3); ?>" alt="<?php echo basename($export_filepath3); ?>" title="<?php echo basename($export_filepath3); ?>" />
          <figcaption>
            No optimization (<?php echo filesize_to_human( $export_filepath3 ); ?>)<br />
            <i>took <?php echo microtime_to_human( $export_time3 ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
