<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';

$code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
       .'$final_filepath = array();'."\n"
       .'foreach( $image->getFrames() as $i => $frame )'."\n"
       .'{'."\n"
       .'  $final_filepath[] = \\GDImage\\Factory::export( $frame , $export_basepath.sprintf( \'%02d\' , $i ) );'."\n"
       .'}';

function colour_type_to_human( $i )
{
  switch( $i )
  {
    case 0: return 'greyscale';
    case 2: return 'truecolor';
    case 3: return 'indexed-color';
    case 4: return 'greyscale with alpha';
    case 6: return 'truecolor with alpha';
  }
}

?>
<html>
<head>
  <title>GDImage: Test 00300 - APNG Decomposition</title>
  <?php require '_head.partial.php'; ?>
  <style>
    small { white-space: nowrap; }
  </style>
</head>
<body>
  
  <h1>GDImage: Test 00300 - APNG Decomposition</h1>
  
  <p><i>
    Some APNG use a smart optimization to reduce file size: helping Dispose option 0 (do not dispose), they overwrote only pixels that have changed.<br />
    With the Dispose option 0 (do not dispose) or 2 (dispose to previous), APNG allows frames to merge colors from alpha channel helping Blend option 1 (over).<br />
    On APNG decomposition, we want each frames at a "visible" state: the optimization belong to the class that will create the APNG.<br />
    Another ability of APNGs is that they can have a default image that is not part of the animation. This image will be used by browser that does not support APNG.<br />
  </i></p>
  
  <pre><?php echo $code; ?></pre>
  
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'apng/preloaders_net_1.png';
  $export_basepath = TMP_DIR.'00300_preloaders_net_1';
  
  // get pack to display usefull info
  $pack = \GDImage\APng_Factory::unpack( $import_filepath );
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  $final_filepath = array();
  if( $image->hasDefaultImage() )
  {
    $final_filepath[-1] = \GDImage\Factory::export( $image->getDefaultImage() , $export_basepath.'-1' );
  }
  foreach( $image->getFrames() as $i => $frame )
  {
    $final_filepath[] = \GDImage\Factory::export( $frame , $export_basepath.sprintf( '%02d' , $i ) );
  }
  $export_time = microtime( true ) - $start;
  
  $nb_cols = 8;
?>
  <h2>Test case 1: Dispose option 0 (do not dispose) and True color with alpha</h2>
  
  <p><i>
    Use true color with alpha channel.<br />
    Each frame fits IHDR Width and Height.<br />
  </i></p>
  
  <table>
    <tr>
      <td colspan="<?php echo $nb_cols; ?>">
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            from <a href="http://preloaders.net/" target="_blank">preloaders.net</a><br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
        <br />
        <small>
          Width: <?php echo $pack['IHDR']['Width']; ?><br />
          Height: <?php echo $pack['IHDR']['Height']; ?><br />
          Color type: <?php echo $pack['IHDR']['Colour type']; ?> <i>(<?php echo colour_type_to_human( $pack['IHDR']['Colour type'] ); ?>)</i><br />
          <?php if( isset( $pack['tRNS'] ) ): ?>
            <?php if( $pack['IHDR']['Colour type'] === 3 ): ?>
              Number of colors with alpha channel: <?php echo count($pack['tRNS']['Colors']); ?><br />
            <?php elseif( $pack['IHDR']['Colour type'] === 0 || $pack['IHDR']['Colour type'] === 2 ): ?>
              Transparent color: yes<br />
            <?php endif; ?>
          <?php endif; ?>
          Number of frames: <?php echo $pack['acTL']['Number of frames']; ?><br />
          Number of plays: <?php echo $pack['acTL']['Number of plays'] ? $pack['acTL']['Number of plays'] : '&infin;'; ?><br />
        </small>
      </td>
    </tr>
    <?php if( isset($final_filepath[-1]) ): $filepath = array_shift( $final_filepath ); ?>
      <tr>
        <td colspan="<?php echo $nb_cols; ?>">
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Default image
            </figcaption>
          </figure>
        </td>
      </tr>
    <?php endif; ?>
    <?php $i = 0; foreach( $final_filepath as $filepath ): $ix = ( $i - isset($pack['fcTL-1']) ); ?>
      <?php $i % $nb_cols === 0 && print '<tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Frame <?php echo $i+1; ?><br />
            </figcaption>
          </figure>
          <br />
          <small>
            Width: <?php echo $pack['fcTL'.$ix]['Width']; ?><br />
            Height: <?php echo $pack['fcTL'.$ix]['Height']; ?><br />
            X-offset: <?php echo $pack['fcTL'.$ix]['X-offset']; ?><br />
            Y-offset: <?php echo $pack['fcTL'.$ix]['Y-offset']; ?><br />
            Delay numerator: <?php echo $pack['fcTL'.$ix]['Delay numerator']; ?><br />
            Delay denominator: <?php echo $pack['fcTL'.$ix]['Delay denominator']; ?><br />
            Dispose option: <?php echo $pack['fcTL'.$ix]['Dispose option']; ?><br />
            Blend option: <?php echo $pack['fcTL'.$ix]['Blend option']; ?><br />
          </small>
        </td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endforeach; ?>
    <?php while( $i % $nb_cols !== 0 ): ?>
      <td></td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endwhile; ?>
  </table>
  
  <hr />
  
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'apng/apngcreator_1.png';
  $export_basepath = TMP_DIR.'00300_apngcreator_1';
  
  // get pack to display usefull info
  $pack = \GDImage\APng_Factory::unpack( $import_filepath );
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  $final_filepath = array();
  if( $image->hasDefaultImage() )
  {
    $final_filepath[-1] = \GDImage\Factory::export( $image->getDefaultImage() , $export_basepath.'-1' );
  }
  foreach( $image->getFrames() as $i => $frame )
  {
    $final_filepath[] = \GDImage\Factory::export( $frame , $export_basepath.sprintf( '%02d' , $i ) );
  }
  $export_time = microtime( true ) - $start;
  
  $nb_cols = 3;
?>
  <h2>Test case 2: Dispose option 0 (do not dispose), True color with alpha and Blend option 1 (blend over)</h2>
  
  <p><i>
    Use true color with alpha channel.<br />
    Each frame fits IHDR Width and Height.<br />
    Note that even when Blend Option is 1 (over), the frames are merged to keep animation at a "visible" state.<br />
    Due to semi-transparency, after decomposition, the frames cannot be merged again, and the blend option is automatically set to 0 (no blend).<br />
  </i></p>
  
  <table>
    <tr>
      <td colspan="<?php echo $nb_cols; ?>">
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            from APNG_Creator<br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
        <br />
        <small>
          Width: <?php echo $pack['IHDR']['Width']; ?><br />
          Height: <?php echo $pack['IHDR']['Height']; ?><br />
          Color type: <?php echo $pack['IHDR']['Colour type']; ?> <i>(<?php echo colour_type_to_human( $pack['IHDR']['Colour type'] ); ?>)</i><br />
          <?php if( isset( $pack['tRNS'] ) ): ?>
            <?php if( $pack['IHDR']['Colour type'] === 3 ): ?>
              Number of colors with alpha channel: <?php echo count($pack['tRNS']['Colors']); ?><br />
            <?php elseif( $pack['IHDR']['Colour type'] === 0 || $pack['IHDR']['Colour type'] === 2 ): ?>
              Transparent color: yes<br />
            <?php endif; ?>
          <?php endif; ?>
          Number of frames: <?php echo $pack['acTL']['Number of frames']; ?><br />
          Number of plays: <?php echo $pack['acTL']['Number of plays'] ? $pack['acTL']['Number of plays'] : '&infin;'; ?><br />
        </small>
      </td>
    </tr>
    <?php if( isset($final_filepath[-1]) ): $filepath = array_shift( $final_filepath ); ?>
      <tr>
        <td colspan="<?php echo $nb_cols; ?>">
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Default image
            </figcaption>
          </figure>
        </td>
      </tr>
    <?php endif; ?>
    <?php $i = 0; foreach( $final_filepath as $filepath ): $ix = ( $i - isset($pack['fcTL-1']) ); ?>
      <?php $i % $nb_cols === 0 && print '<tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Frame <?php echo $i+1; ?><br />
            </figcaption>
          </figure>
          <br />
          <small>
            Width: <?php echo $pack['fcTL'.$ix]['Width']; ?><br />
            Height: <?php echo $pack['fcTL'.$ix]['Height']; ?><br />
            X-offset: <?php echo $pack['fcTL'.$ix]['X-offset']; ?><br />
            Y-offset: <?php echo $pack['fcTL'.$ix]['Y-offset']; ?><br />
            Delay numerator: <?php echo $pack['fcTL'.$ix]['Delay numerator']; ?><br />
            Delay denominator: <?php echo $pack['fcTL'.$ix]['Delay denominator']; ?><br />
            Dispose option: <?php echo $pack['fcTL'.$ix]['Dispose option']; ?><br />
            Blend option: <?php echo $pack['fcTL'.$ix]['Blend option']; ?><br />
          </small>
        </td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endforeach; ?>
    <?php while( $i % $nb_cols !== 0 ): ?>
      <td></td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endwhile; ?>
  </table>
  
  <hr />
  
  
<?php /**/
  // Test case 3

  $import_filepath = SAMP_DIR.'apng/wikipedia_org.png';
  $export_basepath = TMP_DIR.'00300_wikipedia_org';
  
  // get pack to display usefull info
  $pack = \GDImage\APng_Factory::unpack( $import_filepath );
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  $final_filepath = array();
  if( $image->hasDefaultImage() )
  {
    $final_filepath[-1] = \GDImage\Factory::export( $image->getDefaultImage() , $export_basepath.'-1' );
  }
  foreach( $image->getFrames() as $i => $frame )
  {
    $final_filepath[] = \GDImage\Factory::export( $frame , $export_basepath.sprintf( '%02d' , $i ) );
  }
  $export_time = microtime( true ) - $start;
  
  $nb_cols = 7;
?>
  <h2>Test case 3: Mixing Dispose option, True color with alpha and IHDR Width and Height mismatch</h2>
  
  <p><i>
    Use true color with alpha channel.<br />
    Except first frame, frames does not fit IHDR Width and Height.<br />
  </i></p>
  
  <table>
    <tr>
      <td colspan="<?php echo $nb_cols; ?>">
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            from <a href="http://en.wikipedia.org/wiki/APNG" target="_blank">wikipedia.org</a><br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
        <br />
        <small>
          Width: <?php echo $pack['IHDR']['Width']; ?><br />
          Height: <?php echo $pack['IHDR']['Height']; ?><br />
          Color type: <?php echo $pack['IHDR']['Colour type']; ?> <i>(<?php echo colour_type_to_human( $pack['IHDR']['Colour type'] ); ?>)</i><br />
          <?php if( isset( $pack['tRNS'] ) ): ?>
            <?php if( $pack['IHDR']['Colour type'] === 3 ): ?>
              Number of colors with alpha channel: <?php echo count($pack['tRNS']['Colors']); ?><br />
            <?php elseif( $pack['IHDR']['Colour type'] === 0 || $pack['IHDR']['Colour type'] === 2 ): ?>
              Transparent color: yes<br />
            <?php endif; ?>
          <?php endif; ?>
          Number of frames: <?php echo $pack['acTL']['Number of frames']; ?><br />
          Number of plays: <?php echo $pack['acTL']['Number of plays'] ? $pack['acTL']['Number of plays'] : '&infin;'; ?><br />
        </small>
      </td>
    </tr>
    <?php if( isset($final_filepath[-1]) ): $filepath = array_shift( $final_filepath ); ?>
      <tr>
        <td colspan="<?php echo $nb_cols; ?>">
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Default image
            </figcaption>
          </figure>
        </td>
      </tr>
    <?php endif; ?>
    <?php $i = 0; foreach( $final_filepath as $filepath ): $ix = ( $i - isset($pack['fcTL-1']) ); ?>
      <?php $i % $nb_cols === 0 && print '<tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Frame <?php echo $i+1; ?><br />
            </figcaption>
          </figure>
          <br />
          <small>
            Width: <?php echo $pack['fcTL'.$ix]['Width']; ?><br />
            Height: <?php echo $pack['fcTL'.$ix]['Height']; ?><br />
            X-offset: <?php echo $pack['fcTL'.$ix]['X-offset']; ?><br />
            Y-offset: <?php echo $pack['fcTL'.$ix]['Y-offset']; ?><br />
            Delay numerator: <?php echo $pack['fcTL'.$ix]['Delay numerator']; ?><br />
            Delay denominator: <?php echo $pack['fcTL'.$ix]['Delay denominator']; ?><br />
            Dispose option: <?php echo $pack['fcTL'.$ix]['Dispose option']; ?><br />
            Blend option: <?php echo $pack['fcTL'.$ix]['Blend option']; ?><br />
          </small>
        </td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endforeach; ?>
    <?php while( $i % $nb_cols !== 0 ): ?>
      <td></td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endwhile; ?>
  </table>
  
  <hr />
  
  
<?php /**/
  // Test case 4

  $import_filepath = SAMP_DIR.'apng/philip_html5_org_006.png';
  $export_basepath = TMP_DIR.'00300_philip_html5_org_006';
  
  // get pack to display usefull info
  $pack = \GDImage\APng_Factory::unpack( $import_filepath );
  
  // import
  $start = microtime( true );
  $image = \GDImage\Factory::import( $import_filepath );
  $final_filepath = array();
  if( $image->hasDefaultImage() )
  {
    $final_filepath[-1] = \GDImage\Factory::export( $image->getDefaultImage() , $export_basepath.'-1' );
  }
  foreach( $image->getFrames() as $i => $frame )
  {
    $final_filepath[] = \GDImage\Factory::export( $frame , $export_basepath.sprintf( '%02d' , $i ) );
  }
  $export_time = microtime( true ) - $start;
  
  $nb_cols = 1;
?>
  <h2>Test case 4: One frame and default image</h2>
  
  <p><i>
    To see the default image, use Internet Explorer, Chrome or Opera.
  </i></p>
  
  <table>
    <tr>
      <td colspan="<?php echo $nb_cols; ?>">
        <figure>
          <img src="./samples/apng/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture<br />
            from <a href="https://philip.html5.org/tests/apng/tests.html" target="_blank">philip.html5.org</a><br />
            <i>took <?php echo microtime_to_human( $export_time ); ?></i>
          </figcaption>
        </figure>
        <br />
        <small>
          Width: <?php echo $pack['IHDR']['Width']; ?><br />
          Height: <?php echo $pack['IHDR']['Height']; ?><br />
          Color type: <?php echo $pack['IHDR']['Colour type']; ?> <i>(<?php echo colour_type_to_human( $pack['IHDR']['Colour type'] ); ?>)</i><br />
          <?php if( isset( $pack['tRNS'] ) ): ?>
            <?php if( $pack['IHDR']['Colour type'] === 3 ): ?>
              Number of colors with alpha channel: <?php echo count($pack['tRNS']['Colors']); ?><br />
            <?php elseif( $pack['IHDR']['Colour type'] === 0 || $pack['IHDR']['Colour type'] === 2 ): ?>
              Transparent color: yes<br />
            <?php endif; ?>
          <?php endif; ?>
          Number of frames: <?php echo $pack['acTL']['Number of frames']; ?><br />
          Number of plays: <?php echo $pack['acTL']['Number of plays'] ? $pack['acTL']['Number of plays'] : '&infin;'; ?><br />
        </small>
      </td>
    </tr>
    <?php if( isset($final_filepath[-1]) ): $filepath = array_shift( $final_filepath ); ?>
      <tr>
        <td colspan="<?php echo $nb_cols; ?>">
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Default image
            </figcaption>
          </figure>
        </td>
      </tr>
    <?php endif; ?>
    <?php $i = 0; foreach( $final_filepath as $filepath ): $ix = ( $i - isset($pack['fcTL-1']) ); ?>
      <?php $i % $nb_cols === 0 && print '<tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($filepath); ?>" alt="<?php echo basename($filepath); ?>" title="<?php echo basename($filepath); ?>" />
            <figcaption>
              Frame <?php echo $i+1; ?><br />
            </figcaption>
          </figure>
          <br />
          <small>
            Width: <?php echo $pack['fcTL'.$ix]['Width']; ?><br />
            Height: <?php echo $pack['fcTL'.$ix]['Height']; ?><br />
            X-offset: <?php echo $pack['fcTL'.$ix]['X-offset']; ?><br />
            Y-offset: <?php echo $pack['fcTL'.$ix]['Y-offset']; ?><br />
            Delay numerator: <?php echo $pack['fcTL'.$ix]['Delay numerator']; ?><br />
            Delay denominator: <?php echo $pack['fcTL'.$ix]['Delay denominator']; ?><br />
            Dispose option: <?php echo $pack['fcTL'.$ix]['Dispose option']; ?><br />
            Blend option: <?php echo $pack['fcTL'.$ix]['Blend option']; ?><br />
          </small>
        </td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endforeach; ?>
    <?php while( $i % $nb_cols !== 0 ): ?>
      <td></td>
      <?php $i % $nb_cols === ( $nb_cols - 1 ) && print '</tr>'; ?>
    <?php $i++; endwhile; ?>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
