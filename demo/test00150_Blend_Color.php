<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require '_config.inc.php';
  
$code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
       .'$transform = new \\GDImage\\Transform_Blend_Color( $color , $mode );'."\n"
       .'$image->apply( $transform );'."\n"
       .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';

$blend_mode = array(
  'normal' => 'Normal', 
  'darken' => 'Darken' , 
  'multiply' => 'Multiply' , 
  'color_burn' => 'Color Burn' , 
  'linear_burn' => 'Linear Burn' ,
  'lighten' => 'Lighten' ,
  'screen' => 'Screen' ,
  'color_dodge' => 'Color Dodge' ,
  'linear_dodge' => 'Linear Dodge' ,
  'overlay' => 'Overlay' ,
  'soft_light' => 'Soft Light' ,
  'hard_light' => 'Hard Light' ,
  'vivid_light' => 'Vivid Light' ,
  'linear_light' => 'Linear Light' ,
  'pin_light' => 'Pin Light' ,
  'difference' => 'Difference' ,
  'exclusion' => 'Exclusion' ,
);

define( 'MAXTIME_TO_TRANSFORM' , 30 );

?>
<html>
<head>
  <title>GDImage: Test 00150 - Blend over color</title>
  <?php require '_head.partial.php'; ?>
</head>
<body>
  
  <h1>GDImage: Test 00150 - Blend over color</h1>
  
  <pre><?php echo $code; ?></pre>
  
<?php /**/
  // Test case 1

  $import_filepath = SAMP_DIR.'tulips.256c.gif';
  $export_filepaths = array();
  $export_time = array();
  
  $color = 'CD0032';
  
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( array_keys( $blend_mode ) as $mode )
  {
    $clone = clone $image;
    
    // give me some time
    set_time_limit( MAXTIME_TO_TRANSFORM );
    
    $start = microtime( true );
    $clone->apply( new \GDImage\Transform_Blend_Color( $color , $mode ) );
    $export_time[$mode] = microtime( true ) - $start;
    
    $export_filepaths[$mode] = \GDImage\Factory::export( $clone , TMP_DIR.'00150_tulips_'.$mode );
  }
  
?>
  <h2>Test case 1: Blend mode on Palette Color picture</h2>
  
  <p><i>On Palette Color picture, the blend operation is quick: we can calculate colors from the palette and not on every pixels.</i></p>
  
  <table>
    <tr>
      <td colspan="3">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture
          </figcaption>
        </figure>
      </td>
      <td colspan="3">
        <figure>
          <div style="display:inline-block;width:100px;height:100px;margin:20px;background-color:#<?php echo $color; ?>"></div>
          <figcaption>
            Color for blend: <?php echo strtoupper( $color ); ?>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <?php $i = 0; foreach( $export_filepaths as $mode => $export_filepath ): ?>
        <?php if( $i !== 0 && $i % 6 === 0 ) print '</tr><tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
            <figcaption>
              <?php echo $blend_mode[$mode]; ?> (<code><?php echo $mode; ?></code>)<br />
              <i>took <?php echo microtime_to_human( $export_time[$mode] ); ?></i>
            </figcaption>
          </figure>
        </td>
      <?php $i++; endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 2

  $import_filepath = SAMP_DIR.'chrysanthemum.jpg';
  $export_filepaths = array();
  $export_time = array();
  
  $color = '32CD00';
  
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  foreach( array_keys( $blend_mode ) as $mode )
  {
    $clone = clone $image;
    
    // give me some time
    set_time_limit( MAXTIME_TO_TRANSFORM );
    
    $start = microtime( true );
    $clone->apply( new \GDImage\Transform_Blend_Color( $color , $mode ) );
    $export_time[$mode] = microtime( true ) - $start;
    
    $export_filepaths[$mode] = \GDImage\Factory::export( $clone , TMP_DIR.'00150_chrysanthemum_'.$mode );
  }
  
?>
  <h2>Test case 2: Blend mode on True Color picture</h2>
  
  <p><i>On True Color picture, the blend operation is a long process: we have to apply blend calculation on every pixels.</i></p>
  
  <table>
    <tr>
      <td colspan="3">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture
          </figcaption>
        </figure>
      </td>
      <td colspan="3">
        <figure>
          <div style="display:inline-block;width:100px;height:100px;margin:20px;background-color:#<?php echo $color; ?>"></div>
          <figcaption>
            Color for blend: <?php echo strtoupper( $color ); ?>
          </figcaption>
        </figure>
      </td>
    </tr>
    <tr>
      <?php $i = 0; foreach( $export_filepaths as $mode => $export_filepath ): ?>
        <?php if( $i !== 0 && $i % 6 === 0 ) print '</tr><tr>'; ?>
        <td>
          <figure>
            <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
            <figcaption>
              <?php echo $blend_mode[$mode]; ?> (<code><?php echo $mode; ?></code>)<br />
              <i>took <?php echo microtime_to_human( $export_time[$mode] ); ?></i>
            </figcaption>
          </figure>
        </td>
      <?php $i++; endforeach; ?>
    </tr>
  </table>
  
  <hr />
  
<?php /**/
  // Test case 3

  $import_filepath = SAMP_DIR.'jellyfish.16c.8.png';
  $export_filepath = TMP_DIR.'00150_jellyfish_custom.png';
  $export_time = array();
  
  $color = '3200CD';
  
  $image = \GDImage\Factory::import( $import_filepath );
  
  // give me some time
  set_time_limit( MAXTIME_TO_TRANSFORM );

  $start = microtime( true );
  $image->apply( new \GDImage\Transform_Blend_Color( $color , function( $a , $b ){ 
    // avoid 0 divisor
    if( $a === 0 ) return 0;
    
    return 255 - ( ( 255 - $b ) * 255 / $a );
  } ) );
  $export_time[$mode] = microtime( true ) - $start;
    
  \GDImage\Factory::export( $image , $export_filepath );
  
  $code = '$image = \\GDImage\\Factory::import( $import_filepath );'."\n"
         .'$transform = new \\GDImage\\Transform_Blend_Color( $color , $callable );'."\n"
         .'$image->apply( $transform );'."\n"
         .'$final_filepath = \\GDImage\\Factory::export( $image , $export_filepath );';
  
?>
  <h2>Test case 3: Custom blend mode</h2>
  
  <p><i>You can also specify any callable thing to use for blend.<br />
  Note that a kind of caching is done on calculation, so if your callable use randomization, the randomization will be done only once by color channel value pairs.</i></p>
  
  <table>
    <tr>
      <td colspan="2">
        <figure>
          <img src="./samples/<?php echo basename($import_filepath); ?>" alt="<?php echo basename($import_filepath); ?>" title="<?php echo basename($import_filepath); ?>" />
          <figcaption>
            Original picture
          </figcaption>
        </figure>
      </td>
      <td>
        <figure>
          <div style="display:inline-block;width:100px;height:100px;margin:20px;background-color:#<?php echo $color; ?>"></div>
          <figcaption>
            Color for blend: <?php echo strtoupper( $color ); ?>
          </figcaption>
        </figure>
      </td>
      <td colspan="2">
        <figure>
          <img src="./tmp/<?php echo basename($export_filepath); ?>" alt="<?php echo basename($export_filepath); ?>" title="<?php echo basename($export_filepath); ?>" />
          <figcaption>
            Custom blend
            
        <pre>function( $a , $b )
{ 
  // avoid 0 divisor
  if( $a === 0 ) return 0; 
  return 255 - ( ( 255 - $b ) * 255 / $a ); 
}</pre>
            <i>took <?php echo microtime_to_human( $export_time[$mode] ); ?></i>
          </figcaption>
        </figure>
      </td>
    </tr>
  </table>
  
  <hr />
  
<?php /**/ require '_foot.partial.php'; ?>
  
</body>
</html>
        
