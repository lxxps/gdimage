**POOP: Permissive Oriented Object Programming**
=================================================


POOP (Permissive Oriented Object Programming) is a recommendation that advocates 
to voluntarily omit `protected` and `private` visibilities to use only `public`.

- POOP is not dirty
- POOP is not useless
- POOP is not dangerous
- POOP is not hard

**POOP is not poop.**

---------------------------------------

This project follows POOP pattern, that means that every property or method 
prefixed by a single underscore "_" should be considered as protected: **do
not use them if you do not know what you are doing**.  
*PHP magic methods exception: Methods that starts with two underscores "__" are 
"magic" and can be used safely.*

POOP pattern facilitates developments and enhancements because you can bypass 
all setter/getter to access any proprety directly and call any method from 
anywhere.  
When bypass a property or call a pseudo-protected method, your code 
is **out of standard usage**. In other words, if you break the code by this way, 
it is not an issue and you have to correct it by yourself.