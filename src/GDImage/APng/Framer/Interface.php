<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Interface for APNG framer.
 * 
 * Framer are used to create frame instance used over all APng classes.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage apng
 * @author     Loops <pierrotevrard@gmail.com>
 * @interface
 */
interface APng_Framer_Interface
{
  /**
   * Function used to create a blank frame instance.
   * 
   * @param none
   * @return \GDImage\Image_APng_Frame
   * @access public
   * @throws \GDImage\Exception_APng
   * @static
   */
  static public function frame();
}
