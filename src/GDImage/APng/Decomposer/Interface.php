<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Interface for APNG decomposer.
 * 
 * Decomposer are used to create an array of \GDImage\Image_APng_Frame
 * from a PNG pack. The pack comes from a \GDImage\APng_Unpacker_Interface.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage apng
 * @author     Loops <pierrotevrard@gmail.com>
 * @interface
 */
interface APng_Decomposer_Interface
{
  /**
   * Method to create an array of \GDImage\Image_APng_Frame
   * from an PNG pack.
   * 
   * @see http://www.w3.org/TR/PNG/
   * @see https://wiki.mozilla.org/APNG_Specification
   * 
   * @param array $pack PNG pack from \GDImage\APng_Unpacker_Interface
   * @return array Array of \GDImage\Image_APng_Frame
   * @access public
   * @throws \GDImage\Exception_APng
   * @static
   */
  static public function decompose( array $pack );
  
}
