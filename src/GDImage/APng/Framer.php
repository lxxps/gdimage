<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration settings:
 * - "a_png_frame_interface": \GDImage\Image_Interface to use as frame for animated GIF
 */
if( ! Config::hasAPngFrameInterface() ) {
  Config::setAPngFrameInterface( '\\GDImage\\Image_APng_Frame' );
}

/**
 * Class to cerate frame instance used over all APng classes.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage apng
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\APng_Unpacker_Interface
 */
class APng_Framer implements APng_Framer_Interface
{
  
  /**
   * Function used to create a blank frame instance.
   * 
   * @param none
   * @return \GDImage\Image_APng_Frame
   * @access public
   * @throws \GDImage\Exception_APng
   * @static
   */
  static public function frame()
  {
    // nothing fantastic
    $class = Config::getAPngFrameInterface();
    return new $class();
  }
}
