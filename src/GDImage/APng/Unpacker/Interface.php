<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Interface for PNG unpacker.
 * 
 * Unpacker are used to create an array of data (PNG pack) from PNG file or 
 * binary data. The pack will be used by a GDImage\APng_Decomposer_Interface to 
 * create PNG frame.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage apng
 * @author     Loops <pierrotevrard@gmail.com>
 * @interface
 */
interface APng_Unpacker_Interface
{
  /**
   * Function used to unpack PNG file or data to a big array of data.
   * 
   * A key for PNG SIgnature must be present at first place of the array.
   * 
   * Each keys store a PNG Chunk with at least 5 keys: 
   * - _raw: represent raw data for the Chunk;
   * - Length: Chunk Data length;
   * - Name: Chunk name;
   * - Data: Chunk data;
   * - CRC: Chunk CRC validation.
   * @see http://www.w3.org/TR/PNG/#5Chunk-layout
   * 
   * Some important Chunks may be shortcuted by the Chunk name and, may be with
   * an incremental number used to identify the Chunk occurence (ie. "fdAT3" for
   * the fourth "fdAT" Chunk found).
   * These Chunk may contains extra keys with important data. This extra key 
   * names are the same than in PNG specification, but may not be the same
   * than in APNG specification.
   * IDAT and fdAT Chunks will contains an array of Chunk to represent the fact
   * that they can be a combination of several consecutive Chunks.
   * 
   * @see http://www.w3.org/TR/PNG/
   * @see https://wiki.mozilla.org/APNG_Specification
   * 
   * @param string $filepath_or_binary Filepath or binary data
   * @return array PNG pack
   * @access public
   * @throws \GDImage\Exception_APng
   * @static
   */
  static public function unpack( $filepath_or_binary );
}
