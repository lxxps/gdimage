<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Class to unpack animated PNG.
 * 
 * An important thing about APNG frame, is that, from the disposal method, 
 * the previous frame is visible behind transparent color of the current one. 
 * Most of loader use this ability to reduce the file size to minimum.
 * 
 * In our case, we want the frame at its visible state, with the previous frame 
 * behind if necessary. The ability to reduce filesize belong to the "APng_Packer"
 * used to build the APNG binary.
 * 
 * For convenience, this file does not validate Chunk order or Chunk integrity.
 * The PNG specification is relatively simple to implements and we just validate 
 * and unpack interesting Chunks.
 * 
 * Note that multiple consecutive fdAT chunks are all preceded by the Sequence 
 * number, so this number does not represent frame index.
 * @see https://wiki.mozilla.org/APNG_Specification#Chunk_Sequence_Numbers
 * 
 * Also, if there is no fcTL before IDAT, IDAT is not part of the animation. 
 * The difficulty to identify fcTL for IDAT and fcTL for fdAT is solved by 
 * using -1 as incremental number for fcTL associated with IDAT: if a Chunk in 
 * the pack is identified by "fcTL-1" key, that means that it is the fcTL chunk 
 * for IDAT and that IDAT is part of the animation.
 * @see https://wiki.mozilla.org/APNG_Specification#Chunk_Sequence_Numbers
 * 
 * 
 * Watch out: Some method names are case sensitive.
 * 
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage apng
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\APng_Unpacker_Interface
 */
class APng_Unpacker implements APng_Unpacker_Interface
{
  
  /**
   * Function used to unpack PNG file or data to a big array of data.
   * 
   * A key for PNG Signature must be present at first place of the array.
   * 
   * Each keys store a PNG Chunk with at least 5 keys: 
   * - _raw: represent raw data for the Chunk;
   * - Length: Chunk Data length;
   * - Name: Chunk name;
   * - Data: Chunk data;
   * - CRC: Chunk CRC validation.
   * @see http://www.w3.org/TR/PNG/#5Chunk-layout
   * 
   * Some important Chunks may be shortcuted by the Chunk name and, may be with
   * an incremental number used to identify the Chunk occurence (ie. "fdAT3" for
   * the fourth "fdAT" Chunk found).
   * 
   * These Chunk may contains extra keys with important data. This extra key 
   * names are the same than in PNG specification, but may not be the same
   * than in APNG specification.
   * 
   * IDAT and fdAT Chunks will contains an array of Chunk to represent the fact
   * that they can be a combination of several consecutive Chunks.
   * 
   * @see http://www.w3.org/TR/PNG/
   * @see https://wiki.mozilla.org/APNG_Specification
   * 
   * @param string $filepath_or_binary Filepath or binary data
   * @return array PNG pack
   * @access public
   * @throws \GDImage\Exception_APng
   * @static
   */
  static public function unpack( $filepath_or_binary )
  {
    // create unpacker instance
    $unpacker = new static();
    // invoke it
    return $unpacker( $filepath_or_binary );
  }
  
  /**
   * File handle
   * 
   * Note that this handle is allways read forward, never backward.
   * 
   * @var resource
   * @access protected
   */
  public $_handle;
  
  /**
   * Local data storage, also known as PNG pack
   * 
   * @var array
   * @access protected
   */
  public $_png_pack;
  
  /**
   * In order to keep order of appearence, we need to have counter for some
   * particular Chunk found in content
   * 
   * @var array
   * @access protected
   */
  public $_counters;

  /**
   * Function used to unpack PNG file or data to a big array of data.
   * 
   * For convenience, keys correspond to PNG specification Chunk name.
   * 
   * Each keys contain Chunk Data with the same name than PNG specification
   * with allways a "_raw" key to represent raw data of the chunk.
   * 
   * Some important Chunks may be shortcuted by the Chunk name and, may be with
   * an incremental number used to identify the Chunk occurence (ie. "fdAT3" for
   * the fourth "fdAT" Chunk found).
   * These Chunk may contains extra keys with important data. This extra key 
   * names are the same than in PNG specification, but may not be the same
   * than in APNG specification.
   * IDAT and fdAT Chunks will contains an array of Chunk to represent the fact
   * that they can be a combination of several consecutive Chunks.
   * 
   * @see http://www.w3.org/TR/PNG/
   * @see https://wiki.mozilla.org/APNG_Specification
   * 
   * @param string $filepath_or_binary Filepath or binary data
   * @return array PNG pack
   * @access public
   * @throws \GDImage\Exception_APng
   */
  public function __invoke( $filepath_or_binary )
  {
    // reset data
    $this->_png_pack = array();
    
    // reset counters
    $this->_counters = array();
    
    // open file handle
    
    // is it a readable file?
    if( ! $this->_handle = @ fopen( $filepath_or_binary , 'rb' ) )
    {
      // if it is not a file, write it to temporary file
      $this->_handle = tmpfile();
      fwrite( $this->_handle , $filepath_or_binary );
      fseek( $this->_handle , 0 );
      // now we can read it
    }
    
    
    // try/catch to close handle when fails
    try
    {
      // note that the handle is allways read forward, never backward
      
      // just signature
      $this->_unpackSignature();
      
      // then any number of Chunk of any type may appears
      // @see http://www.w3.org/TR/PNG/#5Chunk-layout
      do
      {
        // initialize chunk
        $chunk = array( '_raw' => '' );
        // first four bytes are Chunk Data length
        // the comes four bytes for Chunk name
        $buffer = fread( $this->_handle , 8 );
        $chunk += unpack( 'NLength/A4Name' , $buffer );
        // update _raw
        $chunk['_raw'] .= $buffer;
        
        if( $chunk['Length'] > 0 )
        {
          // from Chunk Length, we can read data
          $chunk['Data'] = fread( $this->_handle , $chunk['Length'] );
          // update _raw
          $chunk['_raw'] .= $chunk['Data'];
        }
        else
        {
          $chunk['Data'] = '';
        }
        
        // then come CRC
        $buffer = fread( $this->_handle , 4 );
        $chunk += unpack( 'A4CRC' , $buffer );
        // update _raw
        $chunk['_raw'] .= $buffer;
        
        // look for a special method to parse the Chunk
        $method = '_unpack'.$chunk['Name'];
        if( method_exists( $this , $method ) )
        {
          // call the method
          $this->$method( $chunk );
        }
        else
        {
          // some of chunk cannot be multiple, so we want shortcut to them
          // @see http://www.w3.org/TR/PNG/#table53
          if( in_array( $chunk['Name'] , array( 
            // critical chunks
            'IHDR' ,
            'PLTE' ,
            'IEND' ,
            // ancillary chunks
            'cHRM' ,
            'gAMA' ,
            'iCCP' ,
            'sBIT' ,
            'sRGB' ,
            'bKGD' ,
            'hIST' ,
            'tRNS' ,
            'pHYs' ,
            'tIME' ,
          ) , true ) )
          {
            // add the chunk to pack
            $this->_png_pack[$chunk['Name']] = $chunk;
          }
          else
          {
            // use counter
            if( ! isset($this->_counters[$chunk['Name']]) ) $this->_counters[$chunk['Name']] = 0;
            
            $this->_png_pack[$chunk['Name'].($this->_counters[$chunk['Name']]++)] = $chunk; // post-increment
          }
        }
        
        // detect IEND chunk
        if( $chunk['Name'] === 'IEND' )
        {
          break;
        }
        
      } while( ! feof( $this->_handle ) );
      
      // close handle
      fclose( $this->_handle );
      
      // return data
      return $this->_png_pack;
    }
    catch( \Exception $e )
    {
      // close handle
      fclose( $this->_handle );
      // re-throw
      throw $e;
    }
  }
  
  // PNG specification
  
  /**
   * Unpack and validate Signature data.
   * Must be call at first.
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_APng
   * @see http://www.w3.org/TR/PNG/#5PNG-file-signature
   */
  public function _unpackSignature()
  {
    // Signature
    // @see http://www.w3.org/TR/PNG/#5PNG-file-signature
    $buffer = fread( $this->_handle , 8 );
    
    if( strlen( $buffer ) < 8 )
    {
      // invalid header 
      throw new Exception_APng( array( get_class( $this ) ) , 7000 );
    }
    
    // validate
    if( $buffer !== "\x89\x50\x4E\x47\x0D\x0A\x1A\x0A" )
    {
      // invalid signature
      throw new Exception_APng( array( get_class( $this ) , $buffer , urlencode( "\x89\x50\x4E\x47\x0D\x0A\x1A\x0A" ) ) , 7001 );
    }
    
    // raw data, nothing else
    $this->_png_pack['Signature'] = $buffer;
  }
  
  
  /**
   * Unpack and validate IHDR Chunk.
   * Sould be the first Chunk after signature.
   * 
   * @param array $chunk
   * @return void
   * @access protected
   * @throws \GDImage\Exception_APng
   * @see http://www.w3.org/TR/PNG/#11IHDR
   */
  public function _unpackIHDR( array $chunk )
  {
    if( $chunk['Length'] !== 13 )
    {
      // invalid Chunk length
      throw new Exception_APng( array( get_class( $this ) , 'IHDR' , $chunk['Length'] , 13 ) , 7005 );
    }
    
    // now we can unpack Chunk data
    $chunk += unpack( 
        'NWidth' // width, 4 bytes
      .'/NHeight' // height, 4 bytes
      .'/CBit depth' // Bit depth, 1 byte
      .'/CColour type' // Colour type, 1 byte
      .'/CCompression method' // Compression method, 1 byte
      .'/CFilter method' // Filter method, 1 byte
      .'/CInterlace method' // Interlace method, 1 byte
    , $chunk['Data'] );
    
    $this->_png_pack['IHDR'] = $chunk;
  }
  
  /**
   * Unpack and validate IDAT Chunk.
   * Multiple IDAT Chunks may follow.
   * In that case, the Image Data is the concatenation of these chunks.
   * 
   * @param array $chunk
   * @return void
   * @access protected
   * @throws \GDImage\Exception_APng
   * @see http://www.w3.org/TR/PNG/#11IDAT
   */
  public function _unpackIDAT( array $chunk )
  {
    // IDAT can be represented by multiple consecutive chunk
    $this->_png_pack['IDAT'][] = $chunk;
  }
  
  /**
   * Unpack and validate IEND Chunk.
   * Just assign shorcut key for convenience.
   * 
   * @param array $chunk
   * @return void
   * @access protected
   * @throws \GDImage\Exception_APng
   * @see http://www.w3.org/TR/PNG/#11IEND
   */
  public function _unpackIEND( array $chunk )
  {
    $this->_png_pack['IEND'] = $chunk;
  }
  
  /**
   * Unpack and validate PLTE Chunk.
   * PLTE data depends of the coulour type defined in IHDR Chunk.
   * Specification claims that they can contains at maximum 256 colors.
   * 
   * @param array $chunk
   * @return void
   * @access protected
   * @throws \GDImage\Exception_APng
   * @see http://www.w3.org/TR/PNG/#11PLTE
   */
  public function _unpackPLTE( array $chunk )
  {
    $chunk['Colors'] = array();

    // unpack
    $format = 'C*'; // all bytes are integer
    $raw = unpack( $format , $chunk['Data'] );

    // each 3 bytes represents a RGB color
    $i = floor( $chunk['Length'] / 3 );
    while( $i-- )
    {
      $j = 3*$i;
      // note that unpack( 'C*' , $buffer ) return an array that starts at index 1
      $chunk['Colors'][$i] = array( 'Red' => $raw[$j+1] , 'Green' => $raw[$j+2] , 'Blue' => $raw[$j+3] );
    }
      
    $this->_png_pack['PLTE'] = $chunk;
  }
  
  /**
   * Unpack and validate tRNS Chunk.
   * tRNS data depends of the coulour type defined in IHDR Chunk.
   * 
   * @param array $chunk
   * @return void
   * @access protected
   * @throws \GDImage\Exception_APng
   * @see http://www.w3.org/TR/PNG/#11tRNS
   */
  public function _unpacktRNS( array $chunk )
  {
    switch( $this->_png_pack['IHDR']['Colour type'] )
    {
      case 0:
        // 2 bytes to represent color to use as transparent
        // this value will be used for Red, Green and Blue component
        $raw = unpack( 'n' , $chunk['Data'] );
        // note that unpack( 'n' , $buffer ) return an array that starts at index 1
        $chunk['Transparent'] = array( 'Red' => $raw[1] , 'Green' => $raw[1] , 'Blue' => $raw[1] );
        
        break;
      case 2:
        // 2 bytes for each component to represent color to use as transparent
        $raw = unpack( 'nnn' , $chunk['Data'] );
        // note that unpack( 'nnn' , $buffer ) return an array that starts at index 1
        $chunk['Transparent'] = array( 'Red' => $raw[1] , 'Green' => $raw[2] , 'Blue' => $raw[3] );
        break;
      case 3:
        // each byte represent alpha value of the corresponding color in the palette
        // note that from PNG point of view, 0 is transparent and 255 is opaque
        
        // unpack
        $format = 'C*'; // all bytes are integer
        $raw = unpack( $format , $chunk['Data'] );
        
        $i = $chunk['Length'];
        while( $i-- )
        {
          // note that unpack( 'C*' , $buffer ) return an array that starts at index 1
          $chunk['Colors'][$i] = array( 'Alpha' => $raw[$i+1] );
        }
        
        break;
      default:
        // invalid coulour type
        throw new Exception_APng( array( get_class( $this ) , 'tRNS' , $this->_png_pack['IHDR']['Colour type'] ) , 7007 );
    }
      
    $this->_png_pack['tRNS'] = $chunk;
  }
  
  /**
   * Unpack and validate bKGD Chunk.
   * bKGD data depends of the coulour type defined in IHDR Chunk.
   * 
   * @param array $chunk
   * @return void
   * @access protected
   * @throws \GDImage\Exception_APng
   * @see http://www.w3.org/TR/PNG/#11bKGD
   */
  public function _unpackbKGD( array $chunk )
  {
    switch( $this->_png_pack['IHDR']['Colour type'] )
    {
      case 0:
      case 4:
        // 2 bytes to represent color to use as transparent
        // this value will be used for Red, Green and Blue component
        $raw = unpack( 'n' , $chunk['Data'] );
        // note that unpack( 'n' , $buffer ) return an array that starts at index 1
        $chunk['Background'] = array( 'Red' => $raw[1] , 'Green' => $raw[1] , 'Blue' => $raw[1] );
        
        break;
      case 2:
      case 6:
        // 2 bytes for each component to represent color to use as transparent
        $raw = unpack( 'nnn' , $chunk['Data'] );
        // note that unpack( 'nnn' , $buffer ) return an array that starts at index 1
        $chunk['Background'] = array( 'Red' => $raw[1] , 'Green' => $raw[2] , 'Blue' => $raw[3] );
        break;
      case 3:
        // 1 byte to represent color index in the palette
        
        // unpack
        $raw = unpack( 'C' , $chunk['Data'] );
        $chunk['Background'] = $raw[1];
        
        break;
      default:
        // invalid coulour type
        throw new Exception_APng( array( get_class( $this ) , 'bKGD' , $this->_png_pack['IHDR']['Colour type'] ) , 7007 );
    }
      
    $this->_png_pack['bKGD'] = $chunk;
  }
  
  // APNG specification
  
  /**
   * Unpack and validate acTL Chunk.
   * Sould be found after IHDR Chunk.
   * 
   * @param array $chunk
   * @return void
   * @access protected
   * @throws \GDImage\Exception_APng
   * @see https://wiki.mozilla.org/APNG_Specification#.60acTL.60:_The_Animation_Control_Chunk
   */
  public function _unpackacTL( array $chunk )
  {
    if( $chunk['Length'] !== 8 )
    {
      // invalid Chunk length
      throw new Exception_APng( array( get_class( $this ) , 'acTL' , $chunk['Length'] , 13 ) , 7005 );
    }
    
    // now we can unpack Chunk data
    $chunk += unpack( 
        'NNumber of frames' // Number of frames, 4 bytes
      .'/NNumber of plays' // Number of plays, 4 bytes
    , $chunk['Data'] );
    
    $this->_png_pack['acTL'] = $chunk;
  }
  
  /**
   * Unpack and validate fcTL Chunk.
   * Sould be found before IDAT or fdAT Chunk.
   * 
   * Note that if a fcTL Chunk is found before IDAT, that means that IDAT is 
   * part of animation. But, it can be any chunks beetween these two chunks.
   * 
   * The difficulty to identify fcTL for IDAT and fcTL for fdAT is solved by 
   * using -1 as incremental number for fcTL associated with IDAT.
   * 
   * @param array $chunk
   * @return void
   * @access protected
   * @throws \GDImage\Exception_APng
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  public function _unpackfcTL( array $chunk )
  {
    if( $chunk['Length'] !== 26 )
    {
      // invalid Chunk length
      throw new Exception_APng( array( get_class( $this ) , 'fcTL' , $chunk['Length'] , 26 ) , 7005 );
    }
    
    // now we can unpack Chunk data
    $chunk += unpack( 
        'NSequence number' // 4 bytes
      .'/NWidth' // 4 bytes
      .'/NHeight' // 4 bytes
      .'/NX-offset' // 4 bytes
      .'/NY-offset' // 4 bytes
      .'/nDelay numerator' // 2 bytes
      .'/nDelay denominator' // 2 bytes
      .'/CDispose option' // 1 byte
      .'/CBlend option' // 1 byte
    , $chunk['Data'] );
    
    
    // if there is no IDAT Chunk in the pack, this Chunk is associated with 
    // IDAT and IDAT is part of the animation
    if( ! isset($this->_png_pack['IDAT']) )
    {
      // initialize counter
      // yes, we can be sure that this counter is not set
      $this->_counters['fcTL'] = -1;
    }
    else
    {
      // in other case, fcTL is associated to a fdAT and IDAT may not be part of the animation
      
      // initialize counter
      if( ! isset( $this->_counters['fcTL'] ) ) $this->_counters['fcTL'] = 0;
    }

    // append fcTL with current counter value
    // then increment this counter
    $this->_png_pack['fcTL'.($this->_counters['fcTL']++)] = $chunk;
  }
  
  /**
   * Unpack and validate fdAT Chunk.
   * 
   * Multiple fdAT Chunks may be consecutive, in that case, the Image Data is 
   * the concatenation of these chunks. But the first byte of the each fdAT 
   * Chunk Data is always a Sequence Number that must not be interpreted as 
   * frame number.
   * 
   * @param array $chunk
   * @return void
   * @access protected
   * @throws \GDImage\Exception_APng
   * @see https://wiki.mozilla.org/APNG_Specification#.60fdAT.60:_The_Frame_Data_Chunk
   */
  public function _unpackfdAT( array $chunk )
  {
    // fdAT must have at least a Sequence number
    if( $chunk['Length'] < 4 )
    {
      // invalid Chunk length for fdAT
      throw new Exception_APng( array( get_class( $this ) , 'fdAT' , $chunk['Length'] , 4 ) , 7006 );
    }

    // extract sequence number
    $chunk += unpack( 'NSequence number' , substr( $chunk['Data'] , 0 , 4 ) );

    // add some IDAT Chunk
    $chunk['IDAT Length'] = $chunk['Length'] - 4;
    $chunk['IDAT Data'] = substr( $chunk['Data'] , 4 );
    
    // we must look if the previous chunk was not a fdAT
    // if it is, that's means that this fdAT is the continuation of Image Data
    end( $this->_png_pack );
    $last = key( $this->_png_pack );
    reset( $this->_png_pack );
    
    // is it a consecutive fdAT?
    if( strpos( $last , 'fdAT' ) !== 0 )
    {
      // initialize counter
      if( ! isset( $this->_counters['fdAT'] ) ) $this->_counters['fdAT'] = 0;
    
      // add chunk to the pack
      $this->_png_pack['fdAT'.($this->_counters['fdAT']++)][] = $chunk;
    }
    else
    {
      // this chunk is the continuation of last fdAT Chunk
      
      // add chunk to the pack using previous counter
      $this->_png_pack['fdAT'.( $this->_counters['fdAT'] - 1)][] = $chunk;
    }
  }
}