<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration settings:
 * - "a_png_composer_optimization": optimization flags, bitwise from
 *   - 1: APng_Composer::OPTIMIZE_TRANSPARENT_TO_CROP
 *   - 2: APng_Composer::OPTIMIZE_DISPOSAL_TO_CROP
 *   - 4: APng_Composer::OPTIMIZE_DISPOSAL_TO_TRANSPARENT
 *   can be very slow on concrete APNG, but provide smaller file size
 * 
 * For convenience, this setting get same name that AGIF one.
 * 
 * @see \GDImage\APng_Composer::_doFrameDowngradefcTL()
 * @see \GDImage\APng_Composer::_doFrameDowngradeDisposeBehindToCrop()
 * @see \GDImage\APng_Composer::_doFrameDowngradeDisposeBehindToTransparent()
 */
if( ! Config::hasAPngComposerOptimization() ) {
  Config::setAPngComposerOptimization( 0 ); // by default no optimization
}

/**
 * Class used to create a PNG pack from an array of \GDImage\Image_Interface. 
 * 
 * Each frame will be converted to \GDImage\Image_APng_Frame when necessary.
 * The frames may be manipulated (crop, color palette, transparent pixels) to 
 * fit expected results.
 * 
 * 
 * For convenience, Chunk are not returned in order of appearence, 
 * it is not necessary for the Packer class.
 * 
 * Each keys store a PNG Chunk with at least 5 keys: 
 * - _raw: represent raw data for the Chunk;
 * - Length: Chunk Data length;
 * - Name: Chunk name;
 * - Data: Chunk data;
 * - CRC: Chunk CRC validation.
 * @see http://www.w3.org/TR/PNG/#5Chunk-layout
 * 
 * IDAT and fdAT Chunks will contains an array of Chunk to represent the fact
 * that they can be a combination of several consecutive Chunks.
 * 
 * @see http://www.w3.org/TR/PNG/
 * @see https://wiki.mozilla.org/APNG_Specification
 * 
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage agif
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\APng_Composer_Interface
 */
class APng_Composer implements APng_Composer_Interface
{
  /**
   * Constant to activate all optimization.
   * 
   * @var integer
   * @const
   */
  const OPTIMIZE_ALL = \PHP_INT_MAX;
  
  /**
   * Constant for "crop" optimization.
   * 
   * @var integer
   * @const
   */
  const OPTIMIZE_CROP = 3;
  
  /**
   * Constant to deactivate all optimization.
   * 
   * @var integer
   * @const
   */
  const OPTIMIZE_NO = 0;
  
  /**
   * Constant for optimization, used as bitwse.
   * 
   * Represent a crop of transparent borders of the current frame.
   * 
   * @var integer
   * @const
   */
  const OPTIMIZE_TRANSPARENT_TO_CROP = 1;
  
  /**
   * Constant for optimization, used as bitwse.
   * 
   * Represent a crop of redundant pixels from disposed frame to the 
   * current frame.
   * 
   * @var integer
   * @const
   */
  const OPTIMIZE_DISPOSAL_TO_CROP = 2;
  
  /**
   * Constant for optimization, used as bitwse.
   * 
   * Represent a replacement with a transparent color on redundant pixels from 
   * disposed frame to the current frame.
   * 
   * @var integer
   * @const
   */
  const OPTIMIZE_DISPOSAL_TO_TRANSPARENT = 4;
  
  /**
   * Method to create a PNG pack from an array of \GDImage\Image_Interface.
   * 
   * For convenience, Chunk to use will be returned in order of appearence.
   * 
   * Each keys store a PNG Chunk with at least 5 keys: 
   * - _raw: represent raw data for the Chunk;
   * - Length: Chunk Data length;
   * - Name: Chunk name;
   * - Data: Chunk data;
   * - CRC: Chunk CRC validation.
   * @see http://www.w3.org/TR/PNG/#5Chunk-layout
   * 
   * IDAT and fdAT Chunks will contains an array of Chunk to represent the fact
   * that they can be a combination of several consecutive Chunks.
   * 
   * @see http://www.w3.org/TR/PNG/
   * @see https://wiki.mozilla.org/APNG_Specification
   * 
   * 
   * If there is a frame at index -1, this frame will be considered as 
   * default image and not part of animation.
   * @see https://wiki.mozilla.org/APNG_Specification#Chunk_Sequence_Numbers
   * 
   * 
   * For the number of plays, 0 means infinite.
   * 
   * 
   * @param array $frames Array of \GDImage\Image_Interface
   * @param [integer] $loops Number of plays
   * @param [integer] $compression PNG compression level
   * @param [integer] $filters PNG filters
   * @param [mixed] ... Extra arguments (not defined yet)
   * @return array GIF pack
   * @access public
   * @throws Exception_APng
   * @static
   * @implements \GDImage\APng_Composer_Interface
   */
  static public function compose( array $frames , $loops = 0 , $compression = null , $filters = null )
  {
    // create instance
    $composer = new static();
    // invoke it and return PNG pack
    $args = func_get_args();
    return call_user_func_array( $composer , $args );
  }
  
  /**
   * PNG pack
   * 
   * @see http://www.w3.org/TR/PNG/
   * @see https://wiki.mozilla.org/APNG_Specification
   * @var array
   * @access protected
   */
  public $_png_pack;
  
  /**
   * Array of frames
   * 
   * @var array Array of \GDImage\Image_APng_Frame
   * @access protected
   */
  public $_frames;
  
  /**
   * First index of frames array.
   * Can be -1 iin case of default image that is not part of the animation
   * or 0 otherwise.
   * 
   * @var integer
   * @access protected
   */
  public $_first_frameindex;
  
  /**
   * Residual resource used for disposal.
   * It may be a \GDImage\Resource_TrueColor.
   * When using dispose option 0 or 2, we must get the original frame before 
   * any manipulation.
   * 
   * @var \GDImage\Resource_Abstract
   * @access protected
   */
  public $_disposal_rsc;
  
  /**
   * Resource used to store palette of color.
   * APNG all share the same palette, so we need to know if we can use one.
   * 
   * If on not first index, this resource is null, that's means that we have to 
   * use true color imagess for the APNG.
   * 
   * We use a 16x16 \GDImage\Resource_PaletteColor.
   * 
   * @var \GDImage\Resource_PaletteColor
   * @access protected
   */
  public $_plte_rsc;
  
  /**
   * Sequence number.
   * 
   * @see https://wiki.mozilla.org/APNG_Specification#Chunk_Sequence_Numbers
   * 
   * @var integer
   * @access protected
   */
  public $_sequence;
  
  /**
   * Method to create a PNG pack from an array of \GDImage\Image_Interface.
   *
   * The pack creation result in four steps:
   * - at first, make sure all frame extends Image_APng_Frame, redundant 
   *   with \GDImage\Image_APng->_prepareFrame(), but not wrong;
   * - on second step, downgrade the frame resource to eliminate 
   *   useless/redondant information and determine palette/true color usage;
   * - third step is to create pack from these resource.
   * 
   * If there is a frame at index -1, it will be considered as a default picture
   * that is not part of the animation. Overwise frame 0 will be default picture 
   * but will be part of the animation.
   * 
   * @param array $frames Array of \GDImage\Image_Interface
   * @param [integer] $loops Number of plays
   * @param [integer] $compression PNG compression level
   * @param [integer] $filters PNG filters
   * @param [mixed] ... Extra arguments (not defined yet)
   * @return array PNG pack
   * @access public
   * @throws Exception_APng
   */
  public function __invoke( array $frames , $loops = 0 , $compression = null , $filters = null )
  {
    // intro
    // reset pack
    $this->_png_pack = array();
    // initialize frames
    $this->_frames = array();
    
    // initialize first frame index
    $this->_first_frameindex = 0;
    if( isset($frames[-1]) ) $this->_first_frameindex = -1;
    
    // step 1
    // convert frame to \GDImage\Image_APng_Frame
    // create empty frame for comparison
    $base = APng_Factory::frame();
    
    // look for frame conversion
    for( $i = $this->_first_frameindex, $imax = count( $frames ) + $this->_first_frameindex; $i < $imax; $i++ )
    {
      // clone it here
      // at this point, we do not care about forward frames
      if( $frames[$i] instanceof $base )
      {
        // just clone it
        $this->_frames[$i] = clone $frames[$i];
      }
      else
      {
        // clone base
        $this->_frames[$i] = clone $base;
        // keep in mind that fromImage() will NOT clone the frame
        // so we do it before
        $this->_frames[$i]->fromImage( clone $frames[$i] );
      }
    }
    
    // step 3
    // now we can downgrade the frames
    for( $i = $this->_first_frameindex, $imax = count( $frames ) + $this->_first_frameindex; $i < $imax; $i++ )
    {
      $this->_doFrameDowngrade( $this->_frames[$i] , $i );
    }
    
    // note that this process may fail with palette color merge
    // so a custom exception exists to redo this process with true color
    try
    {
      // step 3
      // initialize sequence number
      $this->_sequence = 0;
      // then create pack for frame
      for( $i = $this->_first_frameindex, $imax = count( $frames ) + $this->_first_frameindex; $i < $imax; $i++ )
      {
        $this->_doFramePack( $this->_frames[$i] , $i , $compression , $filters  );
      }
    }
    // look at the end of this file for this local exception declaration
    catch( APng_Composer_Exception_RedoFramePackWithTrueColor $e )
    {
      // make sure all frames will be converted to true color
      $this->_png_pack['IHDR']['Colour type'] = 6;
      // unset these chunks
      unset( $this->_png_pack['PLTE'] , $this->_png_pack['tRNS'] );
      
      // step 3
      // initialize sequence number
      $this->_sequence = 0;
      // then create pack for frame
      for( $i = $this->_first_frameindex, $imax = count( $frames ) + $this->_first_frameindex; $i < $imax; $i++ )
      {
        $this->_doFramePack( $this->_frames[$i] , $i , $compression , $filters );
      }
    }
    
    
    // pass extra arguments to the doPack methods
    $args = func_get_args();
    // remove frames arguments
    array_shift( $args );
    // note that we voluntarily keep compression and filters arguments
    // even if they seems useless
    // then call _doPack() method
    call_user_func_array( array( $this , '_doPack' ) , $args );
    
    // outro
    // free resources
    // $this->_frames = array(); // we want to keep this one for debug
    $this->_disposal_rsc = null;
    $this->_indexedcolor_rsc = null;
    
    // Terminated
    return $this->_png_pack;
  }
  
  // FRAME DOWNGRADE
  
  /**
   * Method to do frame downgrades.
   * 
   * This method return true if something has been done on the frame, that does 
   * not mean that false is an error.
   *
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index
   * @return boolean True if something has been done, false otherwise
   * @access protected
   * @throws Exception_APng
   */
  public function _doFrameDowngrade( Image_APng_Frame $frame , $index )
  {
    $flag = false;
    
    // order is important!
    // downgrade IHDR
    $flag = $this->_doFrameDowngradeIHDR( $frame , $index ) || $flag;
    // downgrade Disposal Method
    $flag = $this->_doFrameDowngradeDisposeOption( $frame , $index ) || $flag;
    // downgrade fcTL
    $flag = $this->_doFrameDowngradefcTL( $frame , $index ) || $flag;
    // downgrade PLTE
    $flag = $this->_doFrameDowngradePLTE( $frame , $index ) || $flag;
    
    // return flag
    return $flag;
  }
  
  /**
   * Downgrade the frame to not be out of IHDR.
   * 
   * By convenience IHDR Width and Height correspond to Width and 
   * Height of the first frame.
   * 
   * fcTL data for the corresponding frame will be updated in 
   * the PNG pack.
   * 
   * On first frame, IHDR data will be updated in 
   * the PNG pack.
   *
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/TR/PNG/#11IHDR
   */
  public function _doFrameDowngradeIHDR( Image_APng_Frame $frame , $index )
  {    
    if( $index === $this->_first_frameindex )
    {
      $w = $frame->getWidth();
      $h = $frame->getHeight();
      
      // update IHDR
      // in fact these values will be only used to earn precious µs on each other frames
      // they will be overwrote in self::_doFramePackIHDR()
      $this->_png_pack['IHDR']['Width'] = $w;
      $this->_png_pack['IHDR']['Height'] = $h;
      
      // if the index is -1, this frame is a default image that is not part of 
      // animation and there is no fcTL chunk for IDAT
      // if not, the first frame is the default image and ther is fcTL for IDAT
      // this is represented by using -1 as fcTL counter
      if( $index === 0 )
      {
        // update fcTL data for this frame
        $this->_png_pack['fcTL-1']['Width'] = $w; 
        $this->_png_pack['fcTL-1']['Height'] = $h;
        $this->_png_pack['fcTL-1']['X-offset'] = 0; 
        $this->_png_pack['fcTL-1']['Y-offset'] = 0;
      }
      
      return false;
    }
    
    $sw = $frame->getWidth();
    $sh = $frame->getHeight();
    // use IHDR Width and Height
    $dw = $this->_png_pack['IHDR']['Width'];
    $dh = $this->_png_pack['IHDR']['Height'];
    
    // if smaller or equal, nothing to do
    if( $sw <= $dw && $sh <= $dh ) return false;
    
    // do not use min() or max() to compare two values, it sounds like using a tank to open a door
    if( $sw < $dw ) { $minw = $sw; $maxw = $dw; }
    else { $minw = $dw; $maxw = $sw; }
    if( $sh < $dh ) { $minw = $sh; $maxw = $dh; }
    else { $minh = $dh; $maxw = $sh; }
    
    // we have to crop the resource
    $dst = $frame->getResource()->blank( $minw , $minh );
    
    // if copy failed, return false
    if( ! imagecopy( $dst->getGdResource() , $frame->getResource()->getGdResource() , 0 , 0 , 0 , 0 , $minw , $minh ) ) return false;
    
    // propagate modes
    $dst->setModes( $frame->getResource()->getModes() );
    
    // set new resource
    $frame->setResource( $dst );
    
    // fcTL index is not trivial
    // if there is a default picture, it is the same value than index
    // but if there is no default picture, the fcLT chunk counter will
    // start at index -1 for the first frame used as default picture
    $fcTL_index = $index - ( 1 + $this->_first_frameindex );
    
    $this->_png_pack['fcTL'.$fcTL_index]['Width'] = $minw; 
    $this->_png_pack['fcTL'.$fcTL_index]['Height'] = $minh;
    $this->_png_pack['fcTL'.$fcTL_index]['X-offset'] = 0; 
    $this->_png_pack['fcTL'.$fcTL_index]['Y-offset'] = 0; 
    
    // Oh mama, mama, mama
    // I just shot a man down
    return true;
  }
  
  /**
   * Adjust frame content from the PREVIOUS frame Dispose option.
   * 
   * This method will assign disposal ressource when necessary.
   *
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  public function _doFrameDowngradeDisposeOption( Image_APng_Frame $frame , $index )
  {
    // if the frame is the default image, we must ignore it
    if( $index === -1 ) return false;
    
    // now, depending of Dispose option to apply, we may want to remove some pixels informations
    $method = '_doFrameDowngradeDisposeOption'.$frame->getDisposeOption();
    if( ! method_exists( $this , $method ) )
    {
      // Unknown Disposal Method
      throw new Exception_APng( array( get_class( $this ) , $frame->getDisposeOption() , $index ) , 7061 );
    }
    
    // we may not want any optimization
    // because it can be very slow
    if( ! ( Config::getAPngComposerOptimization() & ( self::OPTIMIZE_DISPOSAL_TO_CROP | self::OPTIMIZE_DISPOSAL_TO_TRANSPARENT ) ) ) return false;
    
    // apply Disposal Method
    return $this->$method( $frame , $index );
  }
  
  /**
   * Manipulate resource from Dispose option 0 (no).
   * 
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  public function _doFrameDowngradeDisposeOption0( Image_APng_Frame $frame , $index )
  {
    // get a snapshot of current frame before downgrade
    // this snapshot will be used as next disposal resource
    $snapshot = clone $frame->getResource();
    
    $flag = $this->_doFrameDowngradeDisposeBehind( $frame , $index );
    
    // now assing snapshot
    $this->_disposal_rsc = $snapshot;
    
    return $flag;
  }
  
  /**
   * Manipulate resource from Dispose option 1 (background).
   * 
   * For this Disposal Method, there is nothing to do: all pixels will be 
   * reset before to display the frame.
   * 
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  public function _doFrameDowngradeDisposeOption1( Image_APng_Frame $frame , $index )
  {
    // for this one, we want to reset disposal resource
    $this->_disposal_rsc = null;
    
    // nothing else to do
    return false;
  }
  
  /**
   * Manipulate resource from Disposal Method 2 (previous).
   * 
   * For this Disposal Method, there is nothing to do: all pixels will be 
   * reset before to display the frame.
   * 
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index
   * @return null|Resource_Abstract
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  public function _doFrameDowngradeDisposeOption2( Image_APng_Frame $frame , $index )
  {
    // for this one, the disposal resource will not change
    return $this->_doFrameDowngradeDisposeBehind( $frame , $index );
  }
  
  /**
   * Remove redundant pixel from disposal resource to a specific frame.
   * 
   * Depending of the Blend Option, one of these optimization can be done:
   * 
   * Blend Option 0: We can remove every redundant pixel from the border of 
   * the disposal resource to a specific frame, but not redundant pixel inside 
   * the frame area.
   * 
   * Blend Option 1: We can remove every redundant pixel from the disposal 
   * resource to a specific frame.
   * 
   * Unlink animated GIF, APNG the frame behind is draw only out of the current 
   * frame area (fcTL Width, Height, X-offset and Y-offset).
   * Every transparent pixels in the current frame area are not drawn above 
   * the behind frame.
   * 
   * Used in _doFrameDowngradeDisposeOption0() and _doFrameDowngradeDisposeOption1().
   * 
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  public function _doFrameDowngradeDisposeBehind( Image_APng_Frame $frame , $index )
  {
    // if there is no disposal resource, we cannot do anything
    if( ! $this->_disposal_rsc ) return false;
    
    // depending on the blend mode, we can do one of this optimization
    $method = '_doFrameDowngradeDisposeBehindBlend'.$frame->getBlendOption();
    if( ! method_exists( $this , $method ) )
    {
      // Unknown Disposal Method
      throw new Exception_APng( array( get_class( $this ) , $frame->getBlendOption() , $index ) , 7062 );
    }
    
    return $this->$method( $frame , $index );
  }
    
  /**
   * Remove every redundant pixel from the border of the disposal 
   * resource to a specific frame, but not redundant pixel inside the frame area.
   * 
   * Used in _doFrameDowngradeDisposeBlend0() and _doFrameDowngradeDisposeBlend1().
   * 
   * Note that for this optimization, we do not need any transparent color.
   * 
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  public function _doFrameDowngradeDisposeBehindToCrop( Image_APng_Frame $frame , $index )
  {
    // we may not want this optimization
    if( ! ( Config::getAPngComposerOptimization() & self::OPTIMIZE_DISPOSAL_TO_CROP ) ) return false;
    
    // for this method, we do not need to care about transparency
    // we just crop the image, nothing else
    
    // shortcut
    $frame_rsc = $frame->getResource();
    
    // we must be sure that resource is awaken before to play with colors
    $frame_rsc->awakeGdResource();
    
    // note we care about previous offsets, even if it is not necessary

    // fcTL index is not trivial
    // if there is a default picture, it is the same value than index
    // but if there is no default picture, the fcLT chunk counter will
    // start at index -1 for the first frame used as default picture
    $fcTL_index = $index - ( 1 + $this->_first_frameindex );
    
    // shortcut
    $x_off = 0;
    $y_off = 0;
    if( isset($this->_png_pack['fcTL'.$fcTL_index]['X-offset']) ) $x_off = $this->_png_pack['fcTL'.$fcTL_index]['X-offset'];
    if( isset($this->_png_pack['fcTL'.$fcTL_index]['Y-offset']) ) $y_off = $this->_png_pack['fcTL'.$fcTL_index]['Y-offset'];
    
    // we muste care about blend option over
    $blend_over = $frame->getBlendOption() === Image_APng_Frame::BLEND_OVER;
    
    // width and height
    $xmax = $frame->getWidth();
    $ymax = $frame->getHeight();
    
    // initial values
    $x_lft = 0;
    $x_rgt = $xmax-1;
    $y_top = 0;
    $y_btm = $ymax-1;
    
    $flag = false;
    
    
    // look for right 
    $x = $xmax;
    while( $x-- ) // post decrement, from right to left, start at $xmax-1, stop at -1
    {
      $y = $ymax;
      while( $y-- ) // post decrement, from bottom to top, start at $ymax-1, stop at -1
      {
        // fetch frame color
        $c1 = $frame_rsc->getColorAt( $x , $y );
        
        // blend over and fully transparent? we can continue 
        if( $blend_over && $c1->getAlpha() === Color::ALPHA_TRANSPARENT ) continue;
        
        // fetch disposal color
        $c2 = $this->_disposal_rsc->getColorAt( $x + $x_off , $y + $y_off );
        
        // if both pixels are fully transparent consider them as identical and continue
        if( $c1->getAlpha() === Color::ALPHA_TRANSPARENT && $c2->getAlpha() === Color::ALPHA_TRANSPARENT ) continue;
        
        // if not, compare properties
        if( $c1 != $c2 )
        {
          $x_rgt = $x;
          
          // we can stop both loops
          break 2;
        }
      }
    }
    
    
    // a little check to look if all pixels are redudant
    if( $y < 0 && $x < 0 )
    {
      // in that case, reduce the frame to a 1x1 image
      
      // create blank resource
      $dst = $frame->getResource()->blank( 1 , 1 );
      
      // nothing to copy, the resource will be filled with transparent color

      // propagate modes
      $dst->setModes( $frame->getResource()->getModes() );

      // set new resource
      $frame->setResource( $dst );

      $this->_png_pack['fcTL'.$fcTL_index]['Width'] = 1; 
      $this->_png_pack['fcTL'.$fcTL_index]['Height'] = 1;
      $this->_png_pack['fcTL'.$fcTL_index]['X-offset'] = 0; 
      $this->_png_pack['fcTL'.$fcTL_index]['Y-offset'] = 0; 

      // My head is a jungle, jungle
      return true;
    }
    
    
    // now, we can assign some default for other values
    $x_lft = $x; // same value than $x_rgt
    $y_top = $y;
    $y_bot = $y;
    
    // now proceed by spiraloid square
    
    // look for top 
    // we have updated $y_top default to its maximal, so we can stop at this value
    $y = -1;
    while( (++$y) < $y_top ) // pre increment, from top to bottom, start at 0, stop at $y_top
    {
      // at this point, we know what there is nothing to look above or equal to $x_rgt+1
      // we want to look from right to left, to determine next $x_lft maximal
      $x = $x_rgt+1;
      while( $x-- ) // pre decrement, from right to left, start at $x_rgt, stop at -1
      {
        // fetch frame color
        $c1 = $frame_rsc->getColorAt( $x , $y );
        
        // blend over and fully transparent? we can continue 
        if( $blend_over && $c1->getAlpha() === Color::ALPHA_TRANSPARENT ) continue;
        
        // fetch disposal color
        $c2 = $this->_disposal_rsc->getColorAt( $x + $x_off , $y + $y_off );
        
        // if both pixels are fully transparent consider them as identical and continue
        if( $c1->getAlpha() === Color::ALPHA_TRANSPARENT && $c2->getAlpha() === Color::ALPHA_TRANSPARENT ) continue;
        
        // if not, compare properties
        if( $c1 != $c2 )
        {
          $y_top = $y;
          
          // we can stop both loops
          break 2;
        }
      }
    }
    
    // at this point, left search may be reduced
    if( $x < $x_lft ) $x_lft = $x;

    // look for left 
    // we have updated $x_lft default value to its maximal, so we can stop at this value
    $x = -1;
    while( (++$x) < $x_lft ) // pre increment, from left to right, start at 0, stop at $x_lft
    {
      // we can be sure that we will not found anything below or equal to $y_top-1
      // we want to look from top to bottom, to determine next $y_bot minimal
      $y = $y_top-1;
      while( (++$y) < $ymax ) // post increment, from top to bottom, start at $y_top, stop at $ymax
      {
        // fetch frame color
        $c1 = $frame_rsc->getColorAt( $x , $y );
        
        // blend over and fully transparent? we can continue 
        if( $blend_over && $c1->getAlpha() === Color::ALPHA_TRANSPARENT ) continue;
        
        // fetch disposal color
        $c2 = $this->_disposal_rsc->getColorAt( $x + $x_off , $y + $y_off );
        
        // if both pixels are fully transparent consider them as identical and continue
        if( $c1->getAlpha() === Color::ALPHA_TRANSPARENT && $c2->getAlpha() === Color::ALPHA_TRANSPARENT ) continue;
        
        // if not, compare properties
        if( $c1 != $c2 )
        {
          $x_lft = $x;
          
          // we can stop both loops
          break 2;
        }
      }
    }
    
    // at this point, bottom search may be reduced
    if( $y > $y_bot ) $y_bot = $y;
    
    // look for bottom 
    // we have updated $y_bot default value to its minimal, so we can stop at this value
    $y = $ymax;
    while( (--$y) > $y_bot ) // pre decrement, from bottom to top, start at $ymax-1, stop at $y_bot
    {
      // we can be sure that we will not found anything below or equal to $x_lft-1
      // we can be sure that we will not found anything above or equal to $x_rgt+1
      $x = $x_rgt+1;
      while( ($x--) > $x_lft ) // post decrement, from right to left, start at $x_rgt, stop at $x_lft-1
      {
        // fetch frame color
        $c1 = $frame_rsc->getColorAt( $x , $y );
        
        // blend over and fully transparent? we can continue 
        if( $blend_over && $c1->getAlpha() === Color::ALPHA_TRANSPARENT ) continue;
        
        // fetch disposal color
        $c2 = $this->_disposal_rsc->getColorAt( $x + $x_off , $y + $y_off );
        
        // if both pixels are fully transparent consider them as identical and continue
        if( $c1->getAlpha() === Color::ALPHA_TRANSPARENT && $c2->getAlpha() === Color::ALPHA_TRANSPARENT ) continue;
        
        // if not, compare properties
        if( $c1 != $c2 )
        {
          $y_btm = $y;
          
          // we can stop both loops
          break 2;
        }
      }
    }
    
    
    // any crop?
    if( $x_lft === 0 && $x_rgt === $xmax - 1 && $y_top === 0 && $y_btm === $ymax - 1 ) return false; 
    
    // get width and height
    $w = $x_rgt - $x_lft + 1;
    $h = $y_btm - $y_top + 1;
    
    // crop resource
    $dst = $frame->getResource()->blank( $w , $h );
    
    // if copy failed, return false
    if( ! imagecopy( $dst->getGdResource() , $frame->getResource()->getGdResource() , 0 , 0 , $x_lft , $y_top , $w , $h ) ) return false;
    
    // propagate modes
    $dst->setModes( $frame->getResource()->getModes() );
    
    // set new resource
    $frame->setResource( $dst );
    
    $this->_png_pack['fcTL'.$fcTL_index]['Width'] = $w; 
    $this->_png_pack['fcTL'.$fcTL_index]['Height'] = $h;
    $this->_png_pack['fcTL'.$fcTL_index]['X-offset'] = $x_lft + $x_off; 
    $this->_png_pack['fcTL'.$fcTL_index]['Y-offset'] = $y_top + $y_off;
    
    // Donc, ca commence par une bifle pour t'edenter
    // Et puis ca finit par une Koenigsbier eventee
    return true;
  }
    
  /**
   * Replace redundant pixel from the disposal resource to a specific frame 
   * with transparent color.
   * 
   * Used in _doFrameDowngradeDisposeBlend1().
   * 
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  public function _doFrameDowngradeDisposeBehindToTransparent( Image_APng_Frame $frame , $index )
  {
    // we may not want this optimization
    if( ! ( Config::getAPngComposerOptimization() & self::OPTIMIZE_DISPOSAL_TO_TRANSPARENT ) ) return false;
    
    // we must be sure that GD resource is awaken before to play with colors
    $frame->getResource()->awakeGdResource();
    
    // no transparent, we cannot do anything else
    // 
    // on true color, there is allways a transparent color
    // @see \GDImage\Resource_TrueColor
    // 
    // on palette color, if there is no transparent color, creating one will 
    // grow up palette size that may not be expected
    if( ! ( $transparent = $frame->getResource()->getTransparent() ) ) return false;
    
    // now look on each pixels to replace it if it is the same than disposal resource
    // at this point, width and height of the frame has been cropped to fit in IHDR size
    // @see _doFrameDowngradeIHDR()
    
    // note we must care about previous offsets
    // @see _doFrameDowngradeDisposeBehindToCrop()
    
    // fcTL index is not trivial
    // if there is a default picture, it is the same value than index
    // but if there is no default picture, the fcLT chunk counter will
    // start at index -1 for the first frame used as default picture
    $fcTL_index = $index - ( 1 + $this->_first_frameindex );
    
    // shortcut
    $x_off = 0;
    $y_off = 0;
    if( isset($this->_png_pack['fcTL'.$fcTL_index]['X-offset']) ) $x_off = $this->_png_pack['fcTL'.$fcTL_index]['X-offset'];
    if( isset($this->_png_pack['fcTL'.$fcTL_index]['Y-offset']) ) $y_off = $this->_png_pack['fcTL'.$fcTL_index]['Y-offset'];
    
    // shortcut
    $frame_rsc = $frame->getResource();
    
    // width and height
    $xmax = $frame->getWidth();
    $ymax = $frame->getHeight();
    
    $flag = false;
    
    $x = $xmax;
    while( $x-- )
    {
      $y = $ymax;
      while( $y-- )
      {
        // keep in mind that this optimization is applicable only when 
        // Blend Option is 1
        
        // fetch frame color
        $c1 = $frame_rsc->getColorAt( $x , $y );
       
        // transparent? do nothing
        if( $c1->getAlpha() === Color::ALPHA_TRANSPARENT ) continue;
        
        // fetch disposal color
        $c2 = $this->_disposal_rsc->getColorAt( $x + $x_off , $y + $y_off );
        
        // transparent? do nothing
        if( $c2->getAlpha() === Color::ALPHA_TRANSPARENT ) continue;
        
        // compare object properties
        if( $c1 == $c2 )
        {
          // they are equal, replace pixels in frame
          $frame_rsc->setColorAt( $transparent , $x , $y );
          
          // set flag to true
          $flag = true;
        }
      }
    }
    
    // Donc, ca commence par une bifle pour t'edenter
    // Et puis ca finit par une Koenigsbier eventee
    return $flag;
  }
    
  /**
   * Do Dispose optimization when Blend Mode is 0.
   * 
   * We can remove every redundant pixel from the border of the disposal 
   * resource to a specific frame, but not redundant pixel inside the frame area.
   * 
   * Unlink animated GIF, on APNG with Blend Option to 0, the frame behind is 
   * draw only out of the current frame area (fcTL Width, Height, X-offset and
   * Y-offset).
   * 
   * Every transparent pixels in the current frame area are not drawn above 
   * the behind frame.
   * 
   * Used in _doFrameDowngradeDisposeOption0() and _doFrameDowngradeDisposeOption1().
   * 
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  public function _doFrameDowngradeDisposeBehindBlend0( Image_APng_Frame $frame , $index )
  {
    return $this->_doFrameDowngradeDisposeBehindToCrop( $frame , $index );
  }
    
  /**
   * Do Dispose optimization when Blend Mode is 1.
   * 
   * Remove redundant pixel from the disposal resource to a specific frame.
   * 
   * Used in _doFrameDowngradeDisposeOption0() and _doFrameDowngradeDisposeOption1().
   * 
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  public function _doFrameDowngradeDisposeBehindBlend1( Image_APng_Frame $frame , $index )
  {
    // crop
    $flag = $this->_doFrameDowngradeDisposeBehindToCrop( $frame , $index );
    
    return $this->_doFrameDowngradeDisposeBehindToTransparent( $frame , $index ) || $flag;
  }
  
  /**
   * Downgrade the frame to remove borders that are fully
   * transparent or with background color.
   * 
   * At this point, redondant pixels have been moved out.
   * 
   * fcTL chunk for the corresponding frame will be updated in 
   * the PNG pack.
   *
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  public function _doFrameDowngradefcTL( Image_APng_Frame $frame , $index )
  {
    // we may not want this optimization
    if( ! ( Config::getAPngComposerOptimization() & self::OPTIMIZE_TRANSPARENT_TO_CROP ) ) return false;
    
    // on first frame, do not alter fcTL
    // it is not clear in the specification, but if the default image is not
    // part of the animation the fcTL MAY differ from IHDR
    if( $index === $this->_first_frameindex ) return false;
    
    // if the blend option is 0 (no blend) and previous frame dispose option 
    // not 1 (restore to background), we must keep transparent pixels
    // at the border of the image
    // hopefully, an optimization should have be done before
    // @see _doFrameDowngradeDisposeBehindBlend0()
    if( $frame->getBlendOption() !== Image_APng_Frame::BLEND_OVER )
    {
      // look for previous frame diposal
      // if there is no previous frame, we should be able to remove transparent 
      // border, it is not clear in the specification, but if the first frame
      // is not the default image, its fcTL may differ from IHDR
      if( $index > 0 && $this->_frames[$index-1]->getDisposeOption() !== Image_APng_Frame::DISPOSE_BACKGROUND )
      {
        // do not remove transparent border
        return false;
      }
    }
    
    // ok, we can remove transparent border
    // - the frame has blend option to 1 (blend over) 
    // - or the frame is part of the animation and not the default image
    // - or the previous frame has dispose option to 1 (restore to background)
    
    
    // for this method, we do not need to care about transparency
    // we just crop the image, nothing else
    
    // keep in mind that the resource may be a true color resource,
    // so only compare alpha channel
    
    // shortcut
    $frame_rsc = $frame->getResource();
    
    // we must be sure that resource is awaken before to play with colors
    $frame_rsc->awakeGdResource();
    
    // width and height
    $xmax = $frame->getWidth();
    $ymax = $frame->getHeight();
    
    // initial values
    $x_lft = 0;
    $x_rgt = $xmax-1;
    $y_top = 0;
    $y_btm = $ymax-1;
    
    // look for right 
    $x = $xmax;
    while( $x-- ) // post decrement, from right to left, start at $xmax-1, stop at -1
    {
      $y = $ymax;
      while( $y-- ) // post decrement, from bottom to top, start at $ymax-1, stop at -1
      {
        if( $frame_rsc->getColorAt( $x , $y )->getAlpha() !== Color::ALPHA_TRANSPARENT )
        {
          $x_rgt = $x;
          
          // we can stop both loops
          break 2;
        }
      }
    }
    
    
    // a little check to look if all pixels are transparent
    if( $y < 0 && $x < 0 )
    {
      // in that case, reduce the frame to a 1x1 image
      
      // create blank resource
      $dst = $frame->getResource()->blank( 1 , 1 );
      
      // nothing to copy, the resource will be filled with transparent color

      // propagate modes
      $dst->setModes( $frame->getResource()->getModes() );

      // set new resource
      $frame->setResource( $dst );

      // fcTL index is not trivial
      // if there is a default picture, it is the same value than index
      // but if there is no default picture, the fcLT chunk counter will
      // start at index -1 for the first frame used as default picture
      $fcTL_index = $index - ( 1 + $this->_first_frameindex );

      $this->_png_pack['fcTL'.$fcTL_index]['Width'] = 1; 
      $this->_png_pack['fcTL'.$fcTL_index]['Height'] = 1;
      $this->_png_pack['fcTL'.$fcTL_index]['X-offset'] = 0; 
      $this->_png_pack['fcTL'.$fcTL_index]['Y-offset'] = 0; 

      // My head is a jungle, jungle
      return true;
    }
    
    
    // now, we can assign some default for other values
    $x_lft = $x; // same value than $x_rgt
    $y_top = $y;
    $y_bot = $y;
    
    // now proceed by spiraloid square
    
    // look for top 
    // we have updated $y_top default to its maximal, so we can stop at this value
    $y = -1;
    while( (++$y) < $y_top ) // pre increment, from top to bottom, start at 0, stop at $y_top
    {
      // at this point, we know what there is nothing to look above or equal to $x_rgt+1
      // we want to look from right to left, to determine next $x_lft maximal
      $x = $x_rgt+1;
      while( $x-- ) // pre decrement, from right to left, start at $x_rgt, stop at -1
      {
        if( $frame_rsc->getColorAt( $x , $y )->getAlpha() !== Color::ALPHA_TRANSPARENT )
        {
          $y_top = $y;
          
          // we can stop both loops
          break 2;
        }
      }
    }
    
    // at this point, left search may be reduced
    if( $x < $x_lft ) $x_lft = $x;

    // look for left 
    // we have updated $x_lft default value to its maximal, so we can stop at this value
    $x = -1;
    while( (++$x) < $x_lft ) // pre increment, from left to right, start at 0, stop at $x_lft
    {
      // we can be sure that we will not found anything below or equal to $y_top-1
      // we want to look from top to bottom, to determine next $y_bot minimal
      $y = $y_top-1;
      while( (++$y) < $ymax ) // post increment, from top to bottom, start at $y_top, stop at $ymax
      {
        if( $frame_rsc->getColorAt( $x , $y )->getAlpha() !== Color::ALPHA_TRANSPARENT )
        {
          $x_lft = $x;
          
          // we can stop both loops
          break 2;
        }
      }
    }
    
    // at this point, bottom search may be reduced
    if( $y > $y_bot ) $y_bot = $y;
    
    // look for bottom 
    // we have updated $y_bot default value to its minimal, so we can stop at this value
    $y = $ymax;
    while( (--$y) > $y_bot ) // pre decrement, from bottom to top, start at $ymax-1, stop at $y_bot
    {
      // we can be sure that we will not found anything below or equal to $x_lft-1
      // we can be sure that we will not found anything above or equal to $x_rgt+1
      $x = $x_rgt+1;
      while( ($x--) > $x_lft ) // post decrement, from right to left, start at $x_rgt, stop at $x_lft-1
      {
        if( $frame_rsc->getColorAt( $x , $y )->getAlpha() !== Color::ALPHA_TRANSPARENT )
        {
          $y_btm = $y;
          
          // we can stop both loops
          break 2;
        }
      }
    }
    
    
    // any crop?
    if( $x_lft === 0 && $x_rgt === $xmax - 1 && $y_top === 0 && $y_btm === $ymax - 1 ) return false; 
    
    // get width and height
    $w = $x_rgt - $x_lft + 1;
    $h = $y_btm - $y_top + 1;
    
    // crop resource
    $dst = $frame->getResource()->blank( $w , $h );
    
    // if copy failed, return false
    if( ! imagecopy( $dst->getGdResource() , $frame->getResource()->getGdResource() , 0 , 0 , $x_lft , $y_top , $w , $h ) ) return false;
    
    // propagate modes
    $dst->setModes( $frame->getResource()->getModes() );
    
    // set new resource
    $frame->setResource( $dst );
    
    
    // fcTL index is not trivial
    // if there is a default picture, it is the same value than index
    // but if there is no default picture, the fcLT chunk counter will
    // start at index -1 for the first frame used as default picture
    $fcTL_index = $index - ( 1 + $this->_first_frameindex );
    
    $this->_png_pack['fcTL'.$fcTL_index]['Width'] = $w; 
    $this->_png_pack['fcTL'.$fcTL_index]['Height'] = $h;
    
    // for offsets we must care about previous assignments 
    // @see _doFrameDowngradeIHDR() and _doFrameDowngradeDisposeBehindToCrop()
    if( ! isset($this->_png_pack['fcTL'.$fcTL_index]['X-offset']) ) $this->_png_pack['fcTL'.$fcTL_index]['X-offset'] = 0;
    if( ! isset($this->_png_pack['fcTL'.$fcTL_index]['Y-offset']) ) $this->_png_pack['fcTL'.$fcTL_index]['Y-offset'] = 0;
    
    $this->_png_pack['fcTL'.$fcTL_index]['X-offset'] += $x_lft; 
    $this->_png_pack['fcTL'.$fcTL_index]['Y-offset'] += $y_top; 
    
    // Qui t'a dit qu'j'serais clodo si j'quittais l'ecole, quittais l'ecole, qui-qui-quittais l'ecole ?
    return true;
  }
  
  /**
   * Downgrade the frame to look if we can use a palette color APNG.
   * 
   * At this point, the frame has been reduced to a minimal size.
   * 
   * On APNG, all frames must share the same palette color, if not, we must use 
   * true color.
   * 
   * Also, PNG can use alpha channel for every indexed color so we can identify 
   * each color as a combination of RGB data + alpha channel.
   * 
   * PLTE will be populate here but reset in method _doFramePackPLTE().
   *
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/TR/PNG/#11PLTE
   * @see http://www.w3.org/TR/PNG/#11tRNS
   */
  public function _doFrameDowngradePLTE( Image_APng_Frame $frame , $index )
  {
    // if we are in true color case, we can skip everything
    if( isset($this->_png_pack['IHDR']['Colour type']) 
     && $this->_png_pack['IHDR']['Colour type'] === 6 ) return false;
    
    // in the resource is a true color
    // assign true color type on IHDR chunk
    if( $frame->getResource()->isTrueColor() )
    {
      $this->_png_pack['IHDR']['Colour type'] = 6;
      
      // stop right now
      return false;
    }
      
    // we are in palette color case
    if( $index === $this->_first_frameindex )
    {
      // assign color type on IHDR chunk
      $this->_png_pack['IHDR']['Colour type'] = 3;
    
      // create plte resource
      // 16x16 will give us 256 pixels, that is exactly the expected limit
      // dunno if it is necessary but it is not wrong...
      $this->_plte_rsc = Resource_PaletteColor::createFromSize( 16 , 16 );
    
      // we must be sure that GD resource is awaken before to play with colors
      $frame->getResource()->awakeGdResource();
      
      // do it in the same order the the palette
      // on palette color the first allocated color will be background color
      // @see http://php.net/manual/en/function.imagecolorallocate.php
      for( $i = 0, $imax = $frame->getResource()->countColors(); $i < $imax; $i++ )
      {
        $c = $frame->getResource()->getColor( $i );
        
        // keep in mind that APNG can store alpha channel on palette colors
        
        // assign color to global table resource
        // this resource will be used to copy the palette to other frames
        $this->_plte_rsc->setColor( $c , true );
      }
      
      return true;
    }
    
    // other frames
    
    // we must be sure that GD resource is awaken before to play with colors
    $frame->getResource()->awakeGdResource();
      
    // on other frame, we want to compare colors to see if additionnal colors 
    // can fit in the palette
    $i = $frame->getResource()->countColors();
    // counter
    $cnt = $this->_plte_rsc->countColors();
    
    while( $i-- )
    {
      $c = $frame->getResource()->getColor( $i );
        
      // keep in mind that APNG can store alpha channel on palette colors

      // look if color is in palette with exact match
      if( $this->_plte_rsc->findColor( $c , true ) !== false )
      {
        // yes it is!
        continue;
      }
      
      // this color may be added to the palette
      if( $cnt < 256 )
      {
        // this color can be added to the palette
        $this->_plte_rsc->setColor( $c , true );
        // update count
        $cnt++;
        // continue
        continue;
      }
      
      // if we come here, we are not able to populate PLTE table
      // on APNG, all frames must become true color
      // update IHDR to true color
      $this->_png_pack['IHDR']['Colour type'] = 6;
      
      // stop right now
      return false;
    }

    // now that the palette is completed, we must be sure that the current 
    // resource use correct color indexes
    // this technic will optimize our chance to do palette match 
    // in _doFramePackPLTE()
    
    // it is quicker to do it on an innocent resource than with $this->_plte_rsc->blank()
    $dst = Resource_PaletteColor::createFromSize( $frame->getWidth() , $frame->getHeight() );
    
    // copy PLTE palette
    imagepalettecopy( $dst->getGdResource() , $this->_plte_rsc->getGdResource() );
    
    // we must be sure that GD resource is awaken before to play with colors
    $frame->getResource()->awakeGdResource();
    
    // if there was a transparent color, restore it and fill picture with
    if( $transparent = $frame->getResource()->getTransparent() )
    {
      // keep in mind that alpha blending does not work on palette color
      // so we must fill the entire picture with transparent color to overwrote background color
      imagefill( $dst->getGdResource() , 0 , 0 , $dst->setTransparent( $transparent ) );
    }
    
    // then paste the current frame to this new picture
    
    // if it fails, we will have a big issue...
    if( ! imagecopy( $dst->getGdResource() , $frame->getResource()->getGdResource() , 0 , 0 , 0 , 0 , $frame->getWidth() , $frame->getHeight() ) )
    {
      // oups...

      // update IHDR to true color
      $this->_png_pack['IHDR']['Colour type'] = 6;

      return false;
    }

    // assign new resource
    $frame->setResource( $dst );

    // I got the right temperature for shelter you from the storm
    return true;
  }
  
  // FRAME PACK
  
  /**
   * Populate PNG pack data from a specific frame.
   * 
   * Note that GD only use critical Chunk and tRNS when necessary.
   *
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index
   * @param [integer] $compression PNG compression level
   * @param [integer] $filters PNG filters
   * @return boolean True if something has been done
   * @access protected
   */
  public function _doFramePack( Image_APng_Frame $frame , $index , $compression = null , $filters = null )
  {    
    // before to create binary look if the frame have to be converted to true color
    if( $this->_png_pack['IHDR']['Colour type'] === 6 && $frame->getResource()->isPaletteColor() )
    {
      $frame->setResource( $frame->getResource()->toTrueColor() );
    }
    // kepp in mind that to use indexed color, all frames must use palette color
    // so the opposite check is useless
            
    // create binary for the frame and unpack it
    $binary = $frame->toBinary( $compression , $filters );
    $pack = APng_Factory::unpack( $binary );
    
    $flag = false;
    $flag = $this->_doFramePackIHDR( $pack , $index ) || $flag;
    $flag = $this->_doFramePackPLTE( $pack , $index ) || $flag;
    $flag = $this->_doFramePackfcTL( $pack , $index ) || $flag;
    $flag = $this->_doFramePackfdAT( $pack , $index ) || $flag;
    
    return $flag;
  }
  
  /**
   * Populate PNG pack with IHDR data.
   *
   * @param array $frame_pack Current frame PNG pack
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/TR/PNG/#11IHDR
   */
  public function _doFramePackIHDR( array $frame_pack , $index )
  {    
    if( $index === $this->_first_frameindex )
    {
      // for the first frame, we can keep IHDR AS IS
      // no matter if we overwrote previous stuff
      $this->_png_pack['IHDR'] = $frame_pack['IHDR'];
      
      return true;
    }
    
    return false;
  }
  
  /**
   * Populate PNG pack with PLTE.
   * 
   * Unfortunately, on export, GD will remove not used colors, so a if the next
   * frame has less colors than the previous one, the PLTE will change drastically.
   * 
   * If we cannot merge palette colors, we will have to use true color.
   * 
   * Because tRNS is related to the number of colors in the palette, it must be 
   * check here.
   * 
   * @param array $frame_pack Current frame PNG pack
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/TR/PNG/#11PLTE
   * @see http://www.w3.org/TR/PNG/#11tRNS
   */
  public function _doFramePackPLTE( array $frame_pack , $index )
  {    
    // on true color, nothing to do
    if( $this->_png_pack['IHDR']['Colour type'] === 6 ) return false;
    
    if( $index === $this->_first_frameindex )
    {
      // on first frame, just copy the PLTE and tRNS chunks
      $this->_png_pack['PLTE'] = $frame_pack['PLTE'];
      $this->_png_pack['tRNS'] = isset($frame_pack['tRNS']) ? $frame_pack['tRNS'] : 
              // dummy chunk with elementary stuff
              array( 'Length' => 0 , 'Name' => 'tRNS' , 'Data' => '' );
      
      return true;
    }
    
    // create a dummy chunk for frame pack if necessary
    if( empty($frame_pack['tRNS']) )
    {
      $frame_pack['tRNS'] = array( 'Length' => 0 , 'Name' => 'tRNS' , 'Data' => '' );
    }
    
    // on other frames, if this data is set to 0, we may be able to copy PLTE 
    // and tRNS chunk to the PNG pack
    // @see self::_doFrameDowngradePLTE()
    
    // unfortunately, even with the same color palette, the frame can get less color
    // than previous one, this will fuck up the PLTE table
    // I did not found any solution for that now

    // we can use a smart match to maximize PLTE usage
      
    // shortcuts
    $frame_PLTE = $frame_pack['PLTE']['Data'];
    $pack_PLTE  = $this->_png_pack['PLTE']['Data'];
    
    $frame_tRNS = $frame_pack['tRNS']['Data'];
    $pack_tRNS = $this->_png_pack['tRNS']['Data'];
    
    // to compare tRNS correctly, we must be sure than an alpha channel is 
    // associated to each color
    
    $c = floor( $frame_pack['PLTE']['Length'] / 3 );
    // the tRNS Length cannot be bigger than the number of colors
    // keep in mind that 0 means fully transparent and 255 fully opaque
    // @see http://www.w3.org/TR/PNG/#11tRNS
    $frame_tRNS .= str_repeat( "\xFF" , $c - $frame_pack['tRNS']['Length'] );
    
    $c = floor( $this->_png_pack['PLTE']['Length'] / 3 );
    // the tRNS Length cannot be bigger than the number of colors
    // keep in mind that 0 means fully transparent and 255 fully opaque
    // @see http://www.w3.org/TR/PNG/#11tRNS
    $pack_tRNS .= str_repeat( "\xFF" , $c - $this->_png_pack['tRNS']['Length'] );
    
      
    // we want the biggest PLTE
    if( $frame_pack['PLTE']['Length'] > $this->_png_pack['PLTE']['Length'] )
    {
      // if the PLTE in the pack is the present at the
      // begining of the PLTE of the frame, we MAY use it
      if( strpos( $frame_PLTE , $pack_PLTE ) !== 0 ) 
      {
        // cannot use PLTE for the frame, color palette will be corrupted
        
        // we have to reverse all previous frames to true color
        // and may redo the entire frame pack process... sounds bad
        
        // throw this local Exception (look at the end of this file for class declaration)
        throw new APng_Composer_Exception_RedoFramePackWithTrueColor( sprintf( 'Failed in %s::%s() at line %d' , __CLASS__ , __METHOD__ , __LINE__ ) );
      }
        
      // the PLTE of the frame seems usable and bigger
      
      // now we must check tRNS
      
      // because, we make sure than both tRNS data fit number of colors of the palette
      // we are sure than the frame tRNS is bigger than the pack tRNS
      
      // if the tRNS in the pack is the present at the
      // begining of the tRNS of the frame, we CAN use it
      if( strpos( $frame_tRNS , $pack_tRNS ) !== 0 ) 
      {
        // cannot use tRNS for the frame, alpha channel will be corrupted
        
        // we have to reverse all previous frames to true color
        // and may redo the entire frame pack process... sounds bad
        
        // throw this local Exception (look at the end of this file for class declaration)
        throw new APng_Composer_Exception_RedoFramePackWithTrueColor( sprintf( 'Failed in %s::%s() at line %d' , __CLASS__ , __METHOD__ , __LINE__ ) );
      }
      
      // everything looks fine, update pack chunks to frame ones
      $this->_png_pack['PLTE'] = $frame_pack['PLTE'];
      $this->_png_pack['tRNS'] = $frame_pack['tRNS'];
      
      // On a rendu fous nos daronnes 
      // Sauf que Sofiane en avait pas 
      return true;
    }
    
    // if the PLTE of the frame is the present at the
    // begining of the PLTE in the pack, we MAY use it
    if( strpos( $pack_PLTE , $frame_PLTE ) !== 0 ) 
    {
      // cannot use PLTE in the pack, color palette will be corrupted

      // we have to reverse all previous frames to true color
      // and may redo the entire frame pack process... sounds bad

      // throw this local Exception (look at the end of this file for class declaration)
      throw new APng_Composer_Exception_RedoFramePackWithTrueColor( sprintf( 'Failed in %s::%s() at line %d' , __CLASS__ , __METHOD__ , __LINE__ ) );
    }
    
    // now we must check tRNS

    // because, we make sure than both tRNS data fit number of colors of the palette
    // we are sure than the frame tRNS is bigger than the pack tRNS

    // if the tRNS of the frame is the present at the
    // begining of the tRNS in the pack, we CAN use it
    if( strpos( $pack_tRNS , $frame_tRNS ) !== 0 ) 
    {
      // cannot use tRNS in the pack, alpha channel will be corrupted

      // we have to reverse all previous frames to true color
      // and may redo the entire frame pack process... sounds bad

      // throw this local Exception (look at the end of this file for class declaration)
      throw new APng_Composer_Exception_RedoFramePackWithTrueColor( sprintf( 'Failed in %s::%s() at line %d' , __CLASS__ , __METHOD__ , __LINE__ ) );
    }
      
    // the PLTE in the pack is usable and bigger than the PLTE of the frame
    // the tRNS in the pack is usable and bigger than the tRNS of the frame
    // we do not have to update anything
    return true;
  }
  
  /**
   * Populate PNG pack with fcTL data.
   * 
   * For this one, we must care about previous data.
   * @see self::_doFrameDowngradeIHDR()
   * @see self::_doFrameDowngradefcTL()
   *
   * @param array $frame_pack Current frame PNG pack
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  public function _doFramePackfcTL( array $frame_pack , $index )
  {    
    // on default image that is not part of the animation, do nothing
    if( $index === -1 ) return false;
    
    // fcTL index is not trivial
    // if there is a default picture, it is the same value than index
    // but if there is no default picture, the fcLT chunk counter will
    // start at index -1 for the first frame used as default picture
    $fcTL_index = $index - ( 1 + $this->_first_frameindex );
    
    // do not overwtore previous data
    
    // create shortcuts
    if( ! isset($this->_png_pack['fcTL'.$fcTL_index]) ) $this->_png_pack['fcTL'.$fcTL_index] = array();
    $png_subpack = &$this->_png_pack['fcTL'.$fcTL_index]; // do it with reference
    
    if( ! isset($png_subpack['X-offset']) ) $png_subpack['X-offset'] = 0;
    if( ! isset($png_subpack['Y-offset']) ) $png_subpack['Y-offset'] = 0;
    if( ! isset($png_subpack['Width']) ) $png_subpack['Width'] = $frame_pack['IHDR']['Width'];
    if( ! isset($png_subpack['Height']) ) $png_subpack['Height'] = $frame_pack['IHDR']['Height'];
    
    // shortcut to frame
    $frame = $this->_frames[$index];
    
    if( ! isset($png_subpack['Delay numerator']) ) $png_subpack['Delay numerator'] = $frame->getDelayNumerator();
    if( ! isset($png_subpack['Delay denominator']) ) $png_subpack['Delay denominator'] = $frame->getDelayDenominator();
    if( ! isset($png_subpack['Dispose option']) ) $png_subpack['Dispose option'] = $frame->getDisposeOption();
    if( ! isset($png_subpack['Blend option']) ) $png_subpack['Blend option'] = $frame->getBlendOption();
    
    // force sequence number
    $png_subpack['Sequence number'] = $this->_sequence++; // post increment
    
    // never wrong
    $png_subpack['Name'] = 'fcTL';
    
    // ok
    return true;
  }
  
  /**
   * Populate PNG pack with fdAT (or IDAT) data.
   *
   * @param array $frame_pack Current frame GIF pack
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#.60fdAT.60:_The_Frame_Data_Chunk
   */
  public function _doFramePackfdAT( array $frame_pack , $index )
  {    
    // GD has a strange habit to split IDAT Data in 8192 length, that is really
    // low compare too the maximum Data length and we can earn some precious
    // bytes on big APNG pictures
    // unfortunately, strpos() is not able to cut a string longer than PHP_INT_MAX
    $frame_IDAT = $frame_pack['IDAT'];
    
    // determine max length
    $l_max = 0xFFFFFFFF;
    if( $l_max > PHP_INT_MAX ) $l_max = PHP_INT_MAX;
    
    for( $i = 0, $imax = count( $frame_IDAT ); $i < $imax; $i++ )
    {
      $l = $l_max - $frame_IDAT[$i]['Length'];
      
      if( $l > 0 && ( $i+1 ) < $imax )
      {
        // we can merge this chunk with the next one
        $frame_IDAT[$i]['Data'] .= substr( $frame_IDAT[$i+1]['Data'] , 0 , $l );
        // unset this "_raw" and CRC data
        unset( $frame_IDAT[$i]['_raw'] , $frame_IDAT[$i]['CRC'] );
        
        if( $l < $frame_IDAT[$i+1]['Length'] )
        {
          // the next chunk will kept some data
          $frame_IDAT[$i]['Length'] = $l_max;
          $frame_IDAT[$i+1]['Length'] -= $l;
          $frame_IDAT[$i+1]['Data'] = substr( $frame_IDAT[$i+1]['Data'] , $l );
          // unset this "_raw" and CRC data
          unset( $frame_IDAT[$i+1]['_raw'] , $frame_IDAT[$i+1]['CRC'] );
        }
        else
        {
          // the next chunk will be completly empty
          $frame_IDAT[$i]['Length'] += $frame_IDAT[$i+1]['Length'];
          // remove next chunk
          array_splice( $frame_IDAT , $i+1 , 1 );
          // decrement i and imax
          $i--; $imax--;
        }
      }
    }
    
    if( $index === $this->_first_frameindex ) 
    {
      // use it as IDAT
      $this->_png_pack['IDAT'] = $frame_IDAT;
      
      return true;
    }
    
    
    // if not, use it as fdAT
    // note that with multiple fdAT Chunk, the sequence number must increment
    // for each of these fdAT
    // @see https://philip.html5.org/tests/apng/tests.html#basic-split-fdat
    
    
    // fdAT index is not trivial
    // if there is a default picture, it is the same value than index
    // but if there is no default picture, the frame at index 1 will be 
    // the first fdAT chunk
    $fdAT_index = $index - ( 1 + $this->_first_frameindex );
    $this->_png_pack['fdAT'.$fdAT_index] = array();
    
    for( $i = 0, $imax = count( $frame_IDAT ); $i < $imax; $i++ )
    {
      // create a base Chunk with mandatory stuff
      $fdAT = array( 
        'Name' => 'fdAT' ,
        'IDAT Length' => $frame_IDAT[$i]['Length'] ,
        'IDAT Data' => $frame_IDAT[$i]['Data'] ,
        'Sequence number' => $this->_sequence++ , // post-increment
      );
      
      // because we will add the sequence number, the IDAT Data part may 
      // exceed maximum Length (4 bytes - 1)
      if( $fdAT['IDAT Length'] > 0xFFFFFFFE )
      {
        // extract extra character
        $extra = substr( $fdAT['IDAT Data'] , 0xFFFFFFFE );
        // remove them from current fdAT
        $fdAT['IDAT Data'] = substr( $fdAT['IDAT Data'] , 0 , 0xFFFFFFFE );
        // adjust IDAT Length
        $fdAT['IDAT Length'] = 0xFFFFFFFE;
        
        // now we want to add these extra characters to the next Chunk, if any
        
        if( empty($frame_IDAT[$i+1]) )
        {
          // create dummy Chunk with necessary stuff
          $frame_IDAT[$i+1] = array( 'Length' => 0 , 'Data' => '' );
          // adjust imax
          $imax++;
        }
        
        // add extra characters
        $frame_IDAT[$i+1]['Length'] += strlen( $extra );
        $frame_IDAT[$i+1]['Data'] .= $extra;
      }
      // in other case, there is nothing to do
       
      // finally add the "sub" Chunk to the fdAT section
      $this->_png_pack['fdAT'.$fdAT_index][] = $fdAT;
    }
    
    // Got kiss myself I'm so pretty
    return true;
  }
  
  // GLOBAL PACK
  
  /**
   * Method to add extra pack information.
   * For now, it treat PNG Signature and acTL chunk.
   * 
   *
   * @param integer $loops Number of repeatitions
   * @param [integer] $compression PNG compression level
   * @param [integer] $filters PNG filters
   * @param [mixed] ... Extra arguments
   * @return boolean Success
   * @access protected
   * @throws Exception_APng
   */
  public function _doPack( $loops = 0 , $compression = null , $filters = null )
  {
    $flag = false;
    
    $flag = $this->_doPackSignature() || $flag;
    $flag = $this->_doPackacTL( $loops ) || $flag;
    
    // some other shits may come here
    
    $flag = $this->_doPackIEND() || $flag;
    return $flag;
  }
  
  /**
   * Populate PNG pack with Signature.
   *
   * @param none
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/TR/PNG/#5PNG-file-signature
   */
  public function _doPackSignature()
  {  
    // create Signature and add it at first
    $this->_png_pack = array( 'Signature' => "\x89\x50\x4E\x47\x0D\x0A\x1A\x0A" ) + $this->_png_pack;
      
    return true;
  }
  
  /**
   * Populate PNG pack with acTL pack.
   *
   * @param integer $loops Number of repeatitions
   * @param integer $index Index of the application extension
   * @return boolean Success
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#.60acTL.60:_The_Animation_Control_Chunk
   */
  public function _doPackacTL( $loops = 0 )
  {
    // APNG allways have a number of loops
    
    $chunk = array(
      'Name' => 'acTL' ,
      // note that default image that is not part of the animation does not
      // belong to number of frames
      'Number of frames' => count( $this->_frames ) + $this->_first_frameindex ,
      // make it integer
      'Number of plays' => $loops | 0 ,
    );
    
    // if there is only one frame that is a default image, we must remove
    // the acTL chunk: 0 is not valid for number of frames
    if( $chunk['Number of frames'] < 1 ) return false;
    
    // the acTL chunk must comes before any IDAT and fcTL chunk
    // so we can add it after IHDR
    
    
//    // create temporary pack
//    $tmp = $this->_png_pack;
//    // look for IHDR chunk
//    $pos = array_search( 'IHDR' , array_keys( $tmp ) , true );
//    // get PNG pack from begining to IHDR Chunk and reduce temporary pack
//    $this->_png_pack = array_splice( $tmp , 0 , $pos + 1 );
//    // add acTL Chunk
//    $this->_png_pack['acTL'] = $chunk;
//    // then add the rest of the PNG pack
//    $this->_png_pack += $tmp;
    
    // all that code in one line just because it is possible
    $this->_png_pack = array_splice( $this->_png_pack , 0 , array_search( 'IHDR' , array_keys( $this->_png_pack ) , true ) + 1 )
                     + array( 'acTL' => $chunk ) + $this->_png_pack;
    
    return true;
  }
  
  /**
   * Populate PNG pack with IEND.
   *
   * @param none
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/TR/PNG/#5PNG-file-signature
   */
  public function _doPackIEND()
  {  
    // create IEND and add it at the end
    $this->_png_pack['IEND'] = array( 'Length' => 0 , 'Name' => 'IEND' , 'Data' => '' );
      
    return true;
  }
}

/**
 * A local exception class, should not be used anywhere else.
 * 
 * It seems smarter to declare this custom exception here, because it
 * should never be used out of the APng_Composer class.
 * 
 * This exception give us ability to re-pack frame when palette color merge fails.
 * @see APng_Composer->_doFramePackPLTE()
 */
class APng_Composer_Exception_RedoFramePackWithTrueColor extends \Exception {}
