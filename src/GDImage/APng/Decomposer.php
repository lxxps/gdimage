<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Class to create frames from a animated PNG pack.
 * @see http://www.w3.org/TR/PNG/
 * @see https://wiki.mozilla.org/APNG_Specification
 * @see \GDImage\APng_Unpacker
 * 
 * An important thing about APNG delay time is that it can be 0, and the PNG
 * still animated: the browser determione itself the minimal delay to use.
 * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
 * 
 * From APNG specification, the IDAT Chunk represent the first frame and all 
 * frames are preceded by an fcTL Chunk. In our case, we want to be able to 
 * decompose classic PNG files too.
 * 
 * An important thing about APNG frame, is that, from the Dispose option
 * the previous frame is visible behind transparent color of the current one. 
 * Most of loader use this ability to reduce the file size to minimum.
 * 
 * In our case, we want the frame at its visible state, with the previous frame 
 * behind if necessary. The ability to reduce filesize belong to the 
 * "APng_Composer" used to build the APNG binary.
 * 
 * Note that there is no local color table for APNG file, so if there is a color
 * palette, its definition is applied for all frames. We may encouter issue to 
 * restore this palette on "APng_Composer" but it is not the problem of the
 * "APng_Decomposer".
 * 
 * An important thing about APNG, is the Blend option, that, combined with 
 * Dispose option set to "no" or "previous" will merge alpha transparency. 
 * Keep in mind that we want frame at their "visible" state, so we will not be 
 * able to restore the two frames separately.
 * 
 * Note that multiple consecutive fdAT chunks are all preceded by the Sequence 
 * number, so this number does not represent frame index.
 * @see https://wiki.mozilla.org/APNG_Specification#Chunk_Sequence_Numbers
 * 
 * Also, if there is no fcTL before IDAT, IDAT is not part of the animation. 
 * The difficulty to identify fcTL for IDAT and fcTL for fdAT is solved by 
 * using -1 as incremental number for fcTL associated with IDAT: if a Chunk in 
 * the pack is identified by "fcTL-1" key, that means that it is the fcTL chunk 
 * for IDAT and that IDAT is part of the animation.
 * @see https://wiki.mozilla.org/APNG_Specification#Chunk_Sequence_Numbers
 * 
 * To represent that on frames array, if a frame as the index "-1", it is the
 * default image and it is not part of the animation. So if there is a Chunk 
 * "fcTL-1" they WILL NOT be a frame at index "-1" and IDAT Chunk will be 
 * considered as part of the animation.
 * 
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage apng
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\APng_Decomposer_Interface
 */
class APng_Decomposer implements APng_Decomposer_Interface
{
  
  /**
   * Method to create an array of \GDImage\Image_APng_Frame
   * from an animated PNG pack.
   * 
   * Note that frame at index "-1" has to be considered as default image and
   * not part of the animation. This specific frame must implements 
   * \GDImage\Image_Interface but must not extends \GDImage\Image_APng_Frame.
   * 
   * @see http://www.w3.org/TR/PNG/
   * @see https://wiki.mozilla.org/APNG_Specification
   * @see \GDImage\APng_Unpacker
   * 
   * @param array PNG pack from \GDImage\APng_Unpacker
   * @return array Array of \GDImage\Image_APng_Frame
   * @access public
   * @throws \GDImage\Exception_APng
   * @static
   * @implements \GDImage\APng_Decomposer_Interface
   */
  static public function decompose( array $pack )
  {
    // create instance
    $decomposer = new static();
    // invoke it and return frames
    return $decomposer( $pack );
  }
  
  /**
   * Current PNG pack
   * 
   * @see \GDImage\APng_Unpacker
   * @var array
   * @access protected
   */
  public $_png_pack;
  
  /**
   * Array of extracted image
   * 
   * @var array Array of \GDImage\Image_APng_Frame
   * @access protected
   */
  public $_frames;
  
  /**
   * Method to create an array of \GDImage\Image_APng_Frame
   * from an animated PNG pack.
   *
   * @param array $pack
   * @return array Array of \GDImage\Image_APng_Frame
   * @access public
   * @throws \GDImage\Exception_APng
   */
  public function __invoke( array $pack )
  {
    // reset frames array
    $this->_frames = array();
    // assign pack
    $this->_png_pack = $pack;
    
    // make some minimal checks on pack
    if( empty($this->_png_pack['IHDR']) )
    {
      throw new Exception_APng( array( get_class( $this ) ) , 7025 );
    }
    if( empty($this->_png_pack['IDAT']) )
    {
      throw new Exception_APng( array( get_class( $this ) ) , 7026 );
    }
    
    // make a check on fcTL
    // exatcly one fcTL is required before each fdAT
    // not that we do not care about "fcTl-1" (frame control for IDAT)
    $fcTL_count = 0;
    $fdAT_count = 0;
    while( isset($this->_png_pack['fdAT'.$fdAT_count]) )
    {
      // fcTL Chunk also exists for IDAT
      // so the incrementation is always one up
      $fcTL_count += isset($this->_png_pack['fcTL'.$fdAT_count]);
      
      $fdAT_count++;
    }
    
    // fcTL Chunk also exists for IDAT
    // so the incrementation is always one up
    if( $fcTL_count !== $fdAT_count )
    {
      // oups, no fcTL for all frames
      throw new Exception_APng( array( get_class( $this ) , $fdAT_count , $fcTL_count ) , 7027 );
    }
    
    // everything looks fine, process creation
    
    // look for IDAT as default picture
    if( isset($this->_png_pack['fcTL-1']) )
    {
      // IDAT is part of the animation 
      $this->_frames[] = $this->_doFrameIDAT();
    }
    else
    {
      // IDAT is the default image
      $this->_frames[-1] = $this->_doDefaultIDAT();
    }
    
    for( $i = 0; $i < $fdAT_count; $i++ )
    {
      $this->_frames[] = $this->_doFrame( $i );
    }
    
    return $this->_frames;
  }
  
  /**
   * Method to create an \GDImage\Image_Interface instance 
   * from animated PNG pack with IDAT chunk.
   *
   * @param none
   * @return \GDImage\Image_Interface
   * @access protected
   * @throws \GDImage\Exception_APng
   */
  public function _doDefaultIDAT()
  {
    // damn, we have to recompile pack for that
    // this is necessary to avoid infinite loop
    // create frame instance
    $frame = APng_Factory::frame();
    
    // create GD resource from binary
    if( $this->_doFrameBinaryIDAT( $frame ) )
    {
      // now import frame resource to get best Image_Interface for that PNG
      return Factory::import( $frame->getResource() );
    }
    
    // an error occurs on binary creation
    throw new Exception_APng( array( get_class( $this ) ) , 7028 );
  }
  
  /**
   * Method to create an \GDImage\Image_APng_Frame instance 
   * from animated PNG pack with IDAT chunk.
   *
   * @param none
   * @return \GDImage\Image_APng_Frame
   * @access protected
   * @throws \GDImage\Exception_APng
   */
  public function _doFrameIDAT()
  {
    // create frame instance
    $frame = APng_Factory::frame();
    
    // create GD resource from binary
    if( $this->_doFrameBinaryIDAT( $frame ) )
    {
      // on first frame there is nothing "visual" to do
      // @see https://wiki.mozilla.org/APNG_Specification#.60acTL.60:_The_Animation_Control_Chunk
      
      // we just want to look for fcTL chunk for this frame
      // and update some data
      $fcTL = $this->_png_pack['fcTL-1'];

      $frame->setDelayNumerator( $fcTL['Delay numerator'] );
      $frame->setDelayDenominator( $fcTL['Delay denominator'] );
      $frame->setDisposeOption( $fcTL['Dispose option'] );
      $frame->setBlendOption( $fcTL['Blend option'] );
      
      return $frame;
    }
    
    // an error occurs on binary creation
    throw new Exception_APng( array( get_class( $this ) ) , 7028 );
  }
  
  /**
   * Create binary part for Ancillary chunks.
   *
   * @param none
   * @return string
   * @access protected
   * @see http://www.w3.org/TR/PNG/#table53
   */
  public function _doFrameAncillaryBinary()
  {
    $binary = '';
    
    // @see http://www.w3.org/TR/PNG/#table53
    
    // chunks to appears before PLTE and IDAT
    // all of these chunks are single
    $chunks = array( 'cHRM', 'gAMA' , 'iCCP' , 'sBIT' , 'sRGB' );
    for( $i = 0, $imax = count( $chunks ); $i < $imax; $i++ )
    {
      if( isset( $this->_png_pack[$chunks[$i]] ) )
      {
        // add to binary
        $binary .= $this->_png_pack[$chunks[$i]]['_raw'];
      }
    }
    
    // then comes PLTE
    if( isset( $this->_png_pack['PLTE'] ) )
    {
      // add to binary
      $binary .= $this->_png_pack['PLTE']['_raw'];
    }
    
    // then some other chunks may come after PLTE and before IDAT
    $chunks = array( 'tRNS' , 'hIST' , 'bKGD' );
    for( $i = 0, $imax = count( $chunks ); $i < $imax; $i++ )
    {
      if( isset( $this->_png_pack[$chunks[$i]] ) )
      {
        // add to binary
        $binary .= $this->_png_pack[$chunks[$i]]['_raw'];
      }
    }
    
    // finally some chunks may comes before IDAT
    // dunno if they have sense...
    // pHYs chunk, single
    if( isset( $this->_png_pack['pHYs'] ) )
    {
      // add to binary
      $binary .= $this->_png_pack['pHYs']['_raw'];
    }
    // sPLT chunk, multiple
    $i = 0;
    while( isset($this->_png_pack['sPLT'.$i]) )
    {
      $binary .= $this->_png_pack['sPLT'.$i]['_raw'];
      $i++;
    }
    
    // we are not interested in other chunks
    return $binary;
  }
    
  
  /**
   * Create binary resource from pack with IDAT chunk.
   *
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @return boolean Success
   * @access protected
   * @throws \GDImage\Exception_APng
   */
  public function _doFrameBinaryIDAT( Image_APng_Frame $frame )
  {
    $binary = '';
    
    // Signature
    $binary .= $this->_png_pack['Signature'];
            
    // IHDR
    $binary .= $this->_png_pack['IHDR']['_raw'];
    
    // then may comes some Ancillary chunks.
    $binary .= $this->_doFrameAncillaryBinary();
            
    // IDAT
    for( $i = 0, $imax = count( $this->_png_pack['IDAT'] ); $i < $imax; $i++ )
    { 
      $binary .= $this->_png_pack['IDAT'][$i]['_raw'];
    }
    
    // IEND
    $binary .= $this->_png_pack['IEND']['_raw'];
    
    // return flag
    return $frame->fromBinary( $binary );
  }
  
  /**
   * Method to create an \GDImage\Image_APng_Frame instance 
   * from animated PNG pack.
   *
   * @param integer $index Image index in the pack
   * @return \GDImage\Image_APng_Frame
   * @access protected
   * @throws \GDImage\Exception_APng
   */
  public function _doFrame( $index )
  {
    // create frame instance
    $frame = APng_Factory::frame();
    
    // create GD resource from binary, only if previous stuff is ok
    if( $this->_doFrameBinary( $frame , $index ) )
    {
      // do frame upgrade to fit "visual" appearence
      // also update frame information
      $this->_doFrameUpgrade( $frame , $index );
      
      return $frame;
    }
    
    // an error occurs on binary creation
    throw new Exception_APng( array( get_class( $this ) , $index ) , 7029 );
  }
  
  /**
   * Create binary resource from pack.
   * Note that GD is unable to create PNG picture if the image width and height 
   * does not fit IHDR.
   *
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean Success
   * @access protected
   * @throws \GDImage\Exception_APng
   */
  public function _doFrameBinary( Image_APng_Frame $frame , $index )
  {
    $binary = '';
    
    // Signature
    $binary .= $this->_png_pack['Signature'];
            
    // shortcuts
    $IHDR = $this->_png_pack['IHDR'];
    $fcTL = $this->_png_pack['fcTL'.$index];
    
    // check width and height
    if( $IHDR['Width'] !== $fcTL['Width'] || $IHDR['Height'] !== $fcTL['Height'] )
    {
      // we have to create valid IHDR for GD
      
      // pack data
      // @see \GDImage\APng_Unpacker->_unpackChunkIHDR()
      $IHDR_data = pack( 'NNCCCCC'
        , $fcTL['Width']     
        , $fcTL['Height'] 
        , $IHDR['Bit depth']
        , $IHDR['Colour type']
        , $IHDR['Compression method']
        , $IHDR['Filter method']
        , $IHDR['Interlace method']
      );
      
      // then pack chunk to binary
      $binary .= pack( 'NA4A*N'
        , $IHDR['Length'] // The length of IHDR chunk is allways fixed to 13
        , $IHDR['Name'] // useless, but not wrong
        , $IHDR_data
        , crc32( $IHDR['Name'] . $IHDR_data )
      );
    }
    else
    {
      // we can use default IHDR
      $binary .= $IHDR['_raw'];
    }
    
    // then may comes some Ancillary chunks.
    $binary .= $this->_doFrameAncillaryBinary();
            
    // fdAT chunks always contain a Sequence Number
    // it should have been removed from "IDAT Data" and "IDAT Length" on Unpacker
    for( $i = 0, $imax = count( $this->_png_pack['fdAT'.$index] ); $i < $imax; $i++ )
    { 
      // shortcut
      $chunk = $this->_png_pack['fdAT'.$index][$i];
      
      // create each IDAT data from Chunk
      $binary .= pack( 'NA4A*N' 
        , $chunk['IDAT Length']
        , 'IDAT'
        , $chunk['IDAT Data']
        , crc32( 'IDAT' . $chunk['IDAT Data'] )
      );
    }
    
    // IEND
    $binary .= $this->_png_pack['IEND']['_raw'];
    
    // return flag
    return $frame->fromBinary( $binary );
  }
  
  /**
   * Upgrade resource from pack information.
   * 
   * GD has some interesting abilities with GIF:
   * - on import, not used colors will be removed and the transparent color 
   *   may be lost;
   * - it ignores the logical screen height and width.
   * 
   * We want the frame to be complete, so when Disposal Method is 1,
   * we want to complete transparent pixels with the previous frame image.
   * 
   * Because of that "residual" image ability, some animated GIF with local 
   * color table may generate frames with true color resource.
   * 
   * This method return true if something has been done on the frame, that does 
   * not mean that false is an error.
   *
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something have been done, false otherwise
   * @access protected
   * @throws \GDImage\Exception_APng
   */
  public function _doFrameUpgrade( Image_APng_Frame $frame , $index )
  {
    $flag = false;
    
    // order is important!
    // upgrade Interlace Method
    $flag = $this->_doFrameUpgradeInterlaceMethod( $frame , $index ) || $flag;
    // upgrade Delay numerator and denominator
    $flag = $this->_doFrameUpgradeDelay( $frame , $index ) || $flag;
    // upgrade Blend option
    $flag = $this->_doFrameUpgradeBlendOption( $frame , $index ) || $flag;
//    // upgrade Transparent Color
//    $flag = $this->_doFrameUpgradeTransparentColor( $frame , $index ) || $flag;
//    // upgrade Background Color
//    $flag = $this->_doFrameUpgradeBackgroundColor( $frame , $index ) || $flag;
    // upgrade to fit IHDR width and height
    $flag = $this->_doFrameUpgradeIHDR( $frame , $index ) || $flag;
    // upgrade Dispose option
    $flag = $this->_doFrameUpgradeDisposeOption( $frame , $index ) || $flag;
    
    return $flag;
  }
  
  /**
   * Assign Interlace Method from pack information.
   * 
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/TR/PNG/#11IHDR
   */
  public function _doFrameUpgradeInterlaceMethod( Image_APng_Frame $frame , $index )
  {
    // look for interlace method
    if( ! $this->_png_pack['IHDR']['Interlace method'] ) return false;
    
    // activate interlace mode
    $frame->getResource()->setMode( Resource_Abstract::MODE_INTERLACE );
      
    return true;
  }
  
  /**
   * Assign Delay numerator and denominator from pack information.
   * 
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  public function _doFrameUpgradeDelay( Image_APng_Frame $frame , $index )
  {
    // fcTL exists for sure
    $frame->setDelayNumerator( $this->_png_pack['fcTL'.$index]['Delay numerator'] );
    $frame->setDelayDenominator( $this->_png_pack['fcTL'.$index]['Delay denominator'] );

    return true;
  }
  
  /**
   * Assign Blend option from pack information.
   * 
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  public function _doFrameUpgradeBlendOption( Image_APng_Frame $frame , $index )
  {
    // Blend Option "no"
    
    if( $this->_png_pack['fcTL'.$index]['Blend option'] === Image_APng_Frame::BLEND_NO )
    {
      // Blend Option to "no" can allways be used
      $frame->setBlendOption( Image_APng_Frame::BLEND_NO );
      
      return true;
    }
    
    // Blend Option "over"
    
    // note than even id blend option is 1, the decomposer will merge frames
    // to see them at a visible state
    // so if the current frame has any semi-transparency, we will never be able
    // restore it at current state
    
    // unfortunately, in true color case, this semi-transparency behavior is 
    // really long to determine, because we have to analyse each pixels
    // so, we just do the job based on colour type
    // @see http://www.w3.org/TR/PNG/#6Colour-values
    
    if( $this->_png_pack['fcTL'.$index]['Blend option'] === Image_APng_Frame::BLEND_OVER )
    {
      switch( $this->_png_pack['IHDR']['Colour type'] )
      {
        case 0: // greyscale, no alpha
        case 2: // truecolor, no alpha
          // we can use blend option "over" even if there is a tRNS chunk
          // @see http://www.w3.org/TR/PNG/#11tRNS
          $frame->setBlendOption( Image_APng_Frame::BLEND_OVER );
          
          return true;
          
        case 4: // greyscale, with alpha
        case 6: // truecolor, with alpha
          // we cannot use blend option "over"
          $frame->setBlendOption( Image_APng_Frame::BLEND_NO );
          
          return false;
          
        case 3: // indexed-colour
          // in indexed color case, we have to look on tRNS chunk
          // to determine if one of the color has semi-transparency
          // @see http://www.w3.org/TR/PNG/#11tRNS
          
          if( empty($this->_png_pack['tRNS']['Colors']) )
          {
            // no transparency, it is useless but not wrong...
            $frame->setBlendOption( Image_APng_Frame::BLEND_OVER );
          
            return true;
          }
          
          // keep in mind that 0 means fully transparent and 255 fully opaque
          // we are just intersted with value beetween this two ones
          $i = count($this->_png_pack['tRNS']['Colors']);
          while( $i-- )
          {
            $a = $this->_png_pack['tRNS']['Colors'][$i]['Alpha'];
            
            // in this two cases, continue
            if( $a === 0 || $a === 255 ) continue;
            
            // in any other case, break
            break;
          }
          
          // if we have read the entire array, we are ok
          if( $i < 0 ) // $i was 0 (stop loop) and post-decremented
          {
            $frame->setBlendOption( Image_APng_Frame::BLEND_OVER );
            
            return true;
          }
          
          // in other case, that's means that there is a semi-transparent color
          $frame->setBlendOption( Image_APng_Frame::BLEND_NO );

          return false;
          
        default:
          // invalid colour type
          throw new Exception_APng( array( get_class( $this ) , $this->_png_pack['IHDR']['Colour type'] , $index ) , 7031 );
      }
    }
    
    // unknown Blend Option
    
    // a new Blend Option? just assign it
    $frame->setBlendOption( $this->_png_pack['fcTL'.$index]['Blend option'] );

    return true;
  }
  
  /**
   * Adjust image to fit IHDR Width and Height from pack information.
   *
   * In order to stay able to resize correctly we want the new Resource 
   * to fit IHDR Width and Height
   * 
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   */
  public function _doFrameUpgradeIHDR( Image_APng_Frame $frame , $index )
  {
    $dw = $this->_png_pack['IHDR']['Width'];
    $dh = $this->_png_pack['IHDR']['Height'];
    // fcTL exists for sure
    $sw = $this->_png_pack['fcTL'.$index]['Width'];
    $sh = $this->_png_pack['fcTL'.$index]['Height'];
    $dx = $this->_png_pack['fcTL'.$index]['X-offset'];
    $dy = $this->_png_pack['fcTL'.$index]['Y-offset'];
    
    // same size and position, nothing to do
    if( $dw === $sw && $dh === $sh && $dx === 0 && $dy === 0 ) return false;
    
    // create blank resource
    $dst = $frame->getResource()->blank( $dw , $dh );

    // copy source to expected position, if false we fail
    if( ! imagecopy( $dst->getGdResource() , $frame->getResource()->getGdResource() , $dx , $dy , 0 , 0 , $sw , $sh ) ) return false;
    
    // propagate modes
    $dst->setModes( $frame->getResource()->getModes() );
    
    // set new resource
    $frame->setResource( $dst );
    
    // we did it!
    return true;
  }
  
  /**
   * Assign Dispose option from pack information.
   * Also adjust frame content from the PREVIOUS frame Dispose option.
   * 
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  public function _doFrameUpgradeDisposeOption( Image_APng_Frame $frame , $index )
  {
    // do some assignement
    // fcTL exists for sure
    $frame->setDisposeOption( $this->_png_pack['fcTL'.$index]['Dispose option'] );
    
    // at this point, we must do some stuff from disposal method
    
    // from specification: 
    // 
    // 0: no disposal is done on this frame before rendering the next; the 
    //    contents of the output buffer are left as is.
    // 1: the frame's region of the output buffer is to be cleared to fully 
    //    transparent black before rendering the next frame.
    // 2: the frame's region of the output buffer is to be reverted to the 
    //    previous contents before rendering the next frame.
    
    // note that in case of dispose option 0 and 2 we must care about 
    // Blend option too
    
    // from sepcification
    // 0: all color components of the frame, including alpha, overwrite the 
    //    current contents of the frame's output buffer region.
    // 1: the frame should be composited onto the output buffer based on its 
    //    alpha, using a simple OVER operation as described in the "Alpha 
    //    Channel Processing" section of the PNG specification [PNG-1.2].

    // so to know how area behind frame has to be restored, we must know the 
    // Dispose option of the PREVIOUS frame
    
    // first frame, do nothing
    // note that we must not care about frame at index "-1": it is default image
    // 
    // keep in mind that IDAT image may be part of the animation so we may have 
    // an image at current index in that case
    $ix = $index + ( empty( $this->_frames[-1] ) );
    
    if( $ix === 0 ) return false;
    
    // ok let's process
    $previous_frame = $this->_frames[$ix-1];
    
    $method = '_doFrameUpgradeDisposeOption'.$previous_frame->getDisposal();
    
    if( ! method_exists( $this , $method ) )
    {
      // Unknown Disposal Method
      throw new Exception_APng( array( get_class( $this ) , $previous_frame->getDisposal() , $index-1 ) , 7030 );
    }
    
    return $this->$method( $frame , $index );
  }
  
  
  /**
   * Adjust frame content from Dispose option 0 (no).
   * Keep in mind that the Dispose option fetch from the 
   * PREVIOUS frame (index-1).
   * 
   * From specification:
   * 0: no disposal is done on this frame before rendering the next; the 
   *    contents of the output buffer are left as is.
   * 
   * That's means that we have to keep the previous frame behind the current one.
   * 
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  public function _doFrameUpgradeDisposeOption0( Image_APng_Frame $frame , $index )
  {
    // keep in mind that IDAT image may be part of the animation so we may have 
    // an image at current index in that case
    $ix = $index + ( empty( $this->_frames[-1] ) );
    
    // attemp merge behind with previous frame
    return $this->_doFrameUpgradeDisposeTransparentToBehind( $frame , $index , $ix-1 );
  }
  
  /**
   * Adjust frame content from Dispose option 1 (background).
   * Keep in mind that the Disposal Method is fetch from the 
   * PREVIOUS frame (index-1).
   * 
   * From specification:
   * 1: the frame's region of the output buffer is to be cleared to fully 
   *    transparent black before rendering the next frame.
   * 
   * The specification are clear: we have to fill picture with a black 
   * transparent color behind. But if our frame is a palette color without 
   * transparent color, i did not know what to do.
   * 
   * 
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   * @see http://the-labs.com/GIFMerge/#manipulation_offset_position
   */
  public function _doFrameUpgradeDisposeOption1( Image_APng_Frame $frame , $index )
  {
    // we must be sure that GD resource is awaken before to play with colors
    $frame->getResource()->awakeGdResource();
    
    // if current frame has transparent color, we have nothing to do
    // keep in mind that filling the transparent color on every resource 
    // creation is our standard way to create resource
    if( $frame->getResource()->getTransparent() !== false ) return false;
    
    // note that true color resource always has a transparent color
    if( $frame->countColors() < 256 )
    {
      // we have a palette resource without transparent but with some space for a 
      // transparent color: just do it
      $blank = $frame->getResource()->blank();
      // assign transparent color but do not use default color, use specification
      // note that $i cannot be false
      $i = $blank->setTransparent( new Color( array( 0 , 0 , 0 , 127 ) ) );
      // then, fill the resource with the transparent color
      imagefill( $blank->getGdResource() , 0 , 0 , $i );
      
      // finally copy the frame resource to this one
      // if fail, return false
      if( ! imagecopy( $blank->getGdResource() , $frame->getResource()->getGdResource() , 0 , 0 , 0 , 0 , $frame->getWidth() , $frame->getHeight() ) ) return false;
      
      // assign new ressource, will destroy previous one
      $frame->setResource( $blank );
      
      // Beautiful girls all over the world
      // I could be chasing but my time would be wasted
      // They got nothing on you baby
      return true;
    }
    
    // NOTE: Since I do not have any sample to test Dispose option 1 with a 
    // full palette color frame without tRNS and with specific fcTL x-offset and
    // y-offset, I can't extrapolate this part
    return false;
  }
  
  /**
   * Adjust frame content from Dispose option 2 (previous).
   * Keep in mind that the Disposal Method is fetch from the 
   * PREVIOUS frame (index-1).
   * 
   * From specification:
   * 2: the frame's region of the output buffer is to be reverted to the 
   *    previous contents before rendering the next frame.
   * 
   * This case is the most complex, we have to look for a previous 
   * frame with a Dispose option set to 0 (do not dispose) and copy new frame 
   * above this one.
   * 
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  public function _doFrameUpgradeDisposeOption2( Image_APng_Frame $frame , $index )
  {
    // look for a previous frame with Dispose option to 0
    // note that we must not care about frame at index "-1": it is default image
    
    // keep in mind that IDAT image may be part of the animation so we may have 
    // an image at current index in that case
    $previous_index = $index + ( empty( $this->_frames[-1] ) );
    
    while( $previous_index-- )
    {
      if( $this->_frames[$previous_index]->getDisposal() === Image_APng_Frame::DISPOSAL_NO )
      {
        // found!
        break;
      }
    }
    
    // not found :-(
    if( $previous_index < 0 ) return false;
    
    // attemp merge behind
    return $this->_doFrameUpgradeDisposeTransparentToBehind( $frame , $index , $previous_index );
  }
  
  /**
   * Merge a frame at specified index BEHIND current one.
   * 
   * Used in _doFrameUpgradeDisposeOption0() and _doFrameUpgradeDisposeOption2().
   * 
   * @param \GDImage\Image_APng_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @param integer $behind_index Index of the frame to merge behind
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  public function _doFrameUpgradeDisposeTransparentToBehind( Image_APng_Frame $frame , $index , $behind_index )
  {
    // clone behind frame resource to not alter it
    $behind_rsc = clone $this->_frames[$behind_index]->getResource();
    
    // we want to copy current frame resource to the new resource
    
    // note that helping _doFrameUpgradeIHDR, the two resource have the same size
    // but we want to be sure that we copy only the expected area to the new location
    // this avoid frame without transparent color to overwrote entire area with background color
    
    $sw = $this->_png_pack['fcTL'.$index]['Width'];
    $sh = $this->_png_pack['fcTL'.$index]['Height'];
    $dx = $this->_png_pack['fcTL'.$index]['X-offset'];
    $dy = $this->_png_pack['fcTL'.$index]['Y-offset'];
    
    // before to copy look for blend option
    if( $this->_png_pack['fcTL'.$index]['Blend option'] )
    {
      $behind_rsc->setMode( Resource_Abstract::MODE_ALPHABLENDING );
    }
    else
    {
      $behind_rsc->unsetMode( Resource_Abstract::MODE_ALPHABLENDING );
    }
    
    // if copy failed, return false
    // do not worry about transparent color, on APNG, the transparent color is the same for all resource
    if( ! imagecopy( $behind_rsc->getGdResource() , $frame->getResource()->getGdResource() , $dx , $dy , $dx , $dy , $sw , $sh ) ) return false;
    
    
    // propagate modes
    $behind_rsc->setModes( $frame->getResource()->getModes() );
    
    // set new resource
    $frame->setResource( $behind_rsc );
    
    // Heartbreakers gonna break, break, break
    // And the fakers gonna fake, fake, fake
    // Baby I'm just gonna shake, shake, shake
    return true;
  }
}
