<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Interface for APNG composer.
 * 
 * Composer are used to create a PNG pack from an array of 
 * \GDImage\Image_Interface. The frames may be manipulated to fit expected 
 * results.
 * 
 * Each keys store a PNG Chunk with at least 5 keys: 
 * - _raw: represent raw data for the Chunk;
 * - Length: Chunk Data length;
 * - Name: Chunk name;
 * - Data: Chunk data;
 * - CRC: Chunk CRC validation.
 * @see http://www.w3.org/TR/PNG/#5Chunk-layout
 * 
 * Some important Chunks may be shortcuted by the Chunk name and, may be with
 * an incremental number used to identify the Chunk occurence (ie. "fdAT3" for
 * the fourth "fdAT" Chunk found).
 * 
 * IDAT and fdAT Chunks will contains an array of Chunk to represent the fact
 * that they can be a combination of several consecutive Chunks.
 * 
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage apng
 * @author     Loops <pierrotevrard@gmail.com>
 * @interface
 */
interface APng_Composer_Interface
{
  /**
   * Method to create a PNG pack from an array of \GDImage\Image_Interface.
   * 
   * 
   * @param array $frames Array of \GDImage\Image_Interface
   * @return array PNG pack
   * @access public
   * @throws \GDImage\Exception_APng
   * @static
   */
  static public function compose( array $frames );
  
}
