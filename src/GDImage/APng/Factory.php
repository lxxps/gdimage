<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration settings:
 * - "a_png_factory_framer": \GDImage\APng_Framer_Interface class name in use
 * - "a_png_factory_unpacker": \GDImage\APng_Unpacker_Interface class name in use
 * - "a_png_factory_decomposer": \GDImage\APng_Decomposer_Interface class name in use
 * - "a_png_factory_composer": \GDImage\APng_Composer_Interface class name in use
 * - "a_png_factory_packer": \GDImage\APng_Packer_Interface class name in use
 */
if( ! Config::hasAPngFactoryFramer() ) {
  Config::setAPngFactoryFramer( '\\GDImage\\APng_Framer' );
}
if( ! Config::hasAPngFactoryUnpacker() ) {
  Config::setAPngFactoryUnpacker( '\\GDImage\\APng_Unpacker' );
}
if( ! Config::hasAPngFactoryDecomposer() ) {
  Config::setAPngFactoryDecomposer( '\\GDImage\\APng_Decomposer' );
}
if( ! Config::hasAPngFactoryComposer() ) {
  Config::setAPngFactoryComposer( '\\GDImage\\APng_Composer' );
}
if( ! Config::hasAPngFactoryPacker() ) {
  Config::setAPngFactoryPacker( '\\GDImage\\APng_Packer' );
}

/**
 * Class to manage animated PNG unpacker, packer, decomposer and composer.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage apng
 * @author     Loops <pierrotevrard@gmail.com>
 */
class APng_Factory
{
  
  // APNG frame management
  
  /**
   * Create a blank frame instance and return it.
   * 
   * @param [...] Extra arguments
   * @return \GDImage\Image_APng_Frame
   * @access public
   * @throws \GDImage\Exception_APng
   * @static
   */
  static public function frame()
  {
    $args = func_get_args();
    return call_user_func_array( array( Config::getAPngFactoryFramer() , 'frame' ) , $args );
  }
  
  // processing APNG
  
  /**
   * Call unpack method.
   * 
   * @param string $filepath_or_binary Filepath or binary data
   * @param [...] Extra arguments
   * @return array PNG pack
   * @access public
   * @throws \GDImage\Exception_APng
   * @static
   */
  static public function unpack( $filepath_or_binary )
  {
    $args = func_get_args();
    return call_user_func_array( array( Config::getAPngFactoryUnpacker() , 'unpack' ) , $args );
  }
  
  /**
   * Call decompose method.
   * 
   * @param array $pack PNG pack
   * @param [...] Extra arguments
   * @return array Array of Image_APng_Frame
   * @access public
   * @throws \GDImage\Exception_APng
   * @static
   */
  static public function decompose( array $pack )
  {
    $args = func_get_args();
    return call_user_func_array( array( Config::getAPngFactoryDecomposer() , 'decompose' ) , $args );
  }
  
  /**
   * Call compose method.
   * 
   * @param array $frames Array of Image_APng_Frame
   * @param [...] Extra arguments
   * @return array PNG Pack
   * @access public
   * @throws \GDImage\Exception_APng
   * @static
   */
  static public function compose( array $frames )
  {
    $args = func_get_args();
    return call_user_func_array( array( Config::getAPngFactoryComposer() , 'compose' ) , $args );
  }
  
  /**
   * Call pack method.
   * 
   * @param array $pack PNG Pack
   * @param [...] Extra arguments
   * @return string Binary data
   * @access public
   * @throws \GDImage\Exception_APng
   * @static
   */
  static public function pack( array $pack )
  {
    $args = func_get_args();
    return call_user_func_array( array( Config::getAPngFactoryPacker() , 'pack' ) , $args );
  }
  
  // debug
  
  static public function ___PngPackRec( $data )
  {
    if( is_array( $data ) )
    {
      foreach( $data as &$value )
      {
        // recursive
        $value = static::___PngPackRec( $value );
      }
    }

    if( is_string( $data ) )
    {
      // do not encode from " " to "~"
      $data = htmlspecialchars( preg_replace_callback( '~[^ -\\~]~' , function( $m ){ return urlencode( $m[0] ); } , $data ) );
    }

    return $data;
  }
  
  static public function printPngPack()
  {
    $args = func_get_args();
    for( $i = 0, $imax = count( $args ); $i < $imax; $i++ )
    {
      print '<pre>';
      print_r( static::___PngPackRec( $args[$i] ) );
      print '</pre>';
    }
  }
}
