<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Class to create binary data from a PNG pack.
 * 
 * Each keys store a PNG Chunk with at least 5 keys: 
 * - _raw: represent raw data for the Chunk;
 * - Length: Chunk Data length;
 * - Name: Chunk name (mandatory);
 * - Data: Chunk data (mandatory);
 * - CRC: Chunk CRC validation.
 * @see http://www.w3.org/TR/PNG/#5Chunk-layout
 * 
 * "_raw" key will allways be prefered from other data.
 * 
 * Some important Chunks may be shortcuted by the Chunk name and, may be with
 * an incremental number used to identify the Chunk occurence (ie. "fdAT3" for
 * the fourth "fdAT" Chunk found).
 * 
 * IDAT and fdAT Chunks can contain an array of Chunk to represent the fact
 * that they can are a combination of several consecutive Chunks.
 * 
 * For performance reason, we do not validate every single chunk of the pack.
 * 
 * 
 * Watch out: Some method names are case sensitive.
 * 
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage apng
 * @author     Loops <pierrotevrard@gmail.com>
 * @abstract
 * @implements \GDImage\APng_Packer_Interface
 */
class APng_Packer implements APng_Packer_Interface
{
  
  /**
   * Function used to create binary data from a PNG pack.
   * 
   * Each keys store a PNG Chunk with at least 5 keys: 
   * - _raw: represent raw data for the Chunk;
   * - Length: Chunk Data length;
   * - Name: Chunk name;
   * - Data: Chunk data;
   * - CRC: Chunk CRC validation.
   * @see http://www.w3.org/TR/PNG/#5Chunk-layout
   * 
   * Some important Chunks may be shortcuted by the Chunk name and, may be with
   * an incremental number used to identify the Chunk occurence (ie. "fdAT3" for
   * the fourth "fdAT" Chunk found).
   * 
   * IDAT and fdAT Chunks will contains an array of Chunk to represent the fact
   * that they can be a combination of several consecutive Chunks.
   * 
   * @param array PNG pack
   * @return string Binary data
   * @access public
   * @throws \GDImage\Exception_APng
   * @static
   * @implements \GDImage\APng_Packer_Interface
   */
  static public function pack( array $pack )
  {
    // create instance
    $packer = new static();
    // invoke it and return binary data
    return $packer( $pack );
  }
  
  /**
   * Binary data.
   * 
   * @var string
   * @access protected
   */
  public $_binary;
  
  /**
   * Current PNG pack
   * 
   * @var array
   * @access protected
   */
  public $_png_pack;

  /**
   * Function used to pack PNG array to binary data.
   * 
   * Note that not some keys may contains a "_raw" value, this value will be 
   * prefereed instead of any chunk creation.
   * 
   * @see http://www.w3.org/TR/PNG/#5Chunk-layout
   * 
   * @param array PNG pack
   * @return string Binary data
   * @access public
   * @throws \GDImage\Exception_APng
   */
  public function __invoke( array $pack )
  {
    // reset binary
    $this->_binary = '';
    
    // assign pack
    $this->_png_pack = $pack;
    
    // do the job
    
    // pack signature and IHDR
    $this->_packSignature(); // single
    $this->_packIHDR(); // single
    
    // acTL (APNG)
    $this->_packacTL(); // single
    
    // then follow http://www.w3.org/TR/PNG/#figure52
    $this->_packtIME(); // single
    $this->_packzTXt(); // multiple
    $this->_packtEXt(); // multiple
    $this->_packiTXt(); // multiple
    $this->_packpHYs(); // single
    $this->_packsPLT(); // multiple
    $this->_packiCCP(); // single
    $this->_packsRGB(); // single
    $this->_packsBIT(); // single
    $this->_packgAMA(); // single
    $this->_packcHRM(); // single
    
    // then come PLTE
    $this->_packPLTE(); // single
    
    // then these one can be used
    $this->_packtRNS(); // single
    $this->_packhIST(); // single
    $this->_packbKGD(); // single
    
    // do special fcTL and IDAT
    if( isset($this->_png_pack['fcTL-1']) )
    {
      $this->_packfcTL( -1 ); // single by index
    }
    $this->_packIDAT(); // single
    
    // do frames
    $i = 0;
    while( isset($this->_png_pack['fcTL'.$i]) && isset($this->_png_pack['fdAT'.$i]) )
    {
      // fcTL and fdAT
      $this->_packfcTL( $i ); // single by index
      $this->_packfdAT( $i ); // single by index
      
      $i++;
    }
    
    // pack IEND
    $this->_packIEND(); // single
    
    return $this->_binary;
  }
  
  /**
   * Pack Header data.
   * Must be call at first.
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_APng
   * @see http://www.w3.org/TR/PNG/#5PNG-file-signature
   */
  public function _packSignature()
  {
    $this->_binary .= $this->_png_pack['Signature'];
  }
  
  /**
   * Pack IHDR data.
   * Must be call immediately after _packSignature().
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_APng
   * @see http://www.w3.org/TR/PNG/#11IHDR
   */
  public function _packIHDR()
  {
    $chunk = $this->_png_pack['IHDR'];
    
    if( empty($chunk['Data']) )
    {
      $chunk['Data'] = pack( 'NNCCCCC'
        , $chunk['Width']
        , $chunk['Height']
        , $chunk['Bit depth']
        , $chunk['Colour type']
        , $chunk['Compression method']
        , $chunk['Filter method']
        , $chunk['Interlace method']
      );
    }
    
    $this->_packChunk( $chunk );
  }
  
  /**
   * Pack acTL data.
   * Must be call before fcTL and IDAT.
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_APng
   * @see https://wiki.mozilla.org/APNG_Specification#.60acTL.60:_The_Animation_Control_Chunk
   */
  public function _packacTL()
  {
    if( isset( $this->_png_pack['acTL'] ) )
    {
      $chunk = $this->_png_pack['acTL'];

      if( empty($chunk['Data']) )
      {
        $chunk['Data'] = pack( 'NN'
          , $chunk['Number of frames']
          , $chunk['Number of plays']
        );
      }

      $this->_packChunk( $chunk );
    }
  }
  
  /**
   * Pack fcTL data.
   * This chunk require an index.
   * If index is -1, it means that this chunk is relative to with IDAT data.
   * 
   * @param integer $index
   * @return void
   * @access protected
   * @throws \GDImage\Exception_APng
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  public function _packfcTL( $index )
  {
    $chunk = $this->_png_pack['fcTL'.$index];

    if( empty($chunk['Data']) )
    {
      $chunk['Data'] = pack( 'NNNNNnnCC'
        , $chunk['Sequence number']
        , $chunk['Width']
        , $chunk['Height']
        , $chunk['X-offset']
        , $chunk['Y-offset']
        , $chunk['Delay numerator']
        , $chunk['Delay denominator']
        , $chunk['Dispose option']
        , $chunk['Blend option']
      );
    }

    $this->_packChunk( $chunk );
  }
  
  /**
   * Pack IDAT data.
   * 
   * This key may contains an array of chunk to represent Image Data that go
   * over multiple Chunks.
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_APng
   * @see http://www.w3.org/TR/PNG/#11IDAT
   */
  public function _packIDAT()
  {
    $chunks = $this->_png_pack['IDAT'];
    
    reset( $chunks );
    $key = key( $chunks );
    reset( $chunks );
    
    if( is_numeric( $key ) )
    {
      // consider multiple chunks
      foreach( $chunks as $chunk )
      {
        $this->_packChunk( $chunk );
      }
    }
    else
    {
      $this->_packChunk( $chunks );
    }
  }
  
  /**
   * Pack fdAT data.
   * This chunk require an index.
   * 
   * This key may contains an array of chunk to represent Image Data that go
   * over multiple Chunks.
   * 
   * @param integer $index
   * @return void
   * @access protected
   * @throws \GDImage\Exception_APng
   * @see https://wiki.mozilla.org/APNG_Specification#.60fdAT.60:_The_Frame_Data_Chunk
   */
  public function _packfdAT( $index )
  {
    $chunks = $this->_png_pack['fdAT'.$index];
    
    reset( $chunks );
    $key = key( $chunks );
    reset( $chunks );
    
    if( is_numeric( $key ) )
    {
      // consider multiple chunks
      foreach( $chunks as $chunk )
      {
        if( empty($chunk['Data']) )
        {
          $chunk['Data'] = pack( 'NA*'
            , $chunk['Sequence number']
            , $chunk['IDAT Data']
          );
        }

        $this->_packChunk( $chunk );
      }
    }
    else
    {
      // consider a single chunk
      if( empty($chunks['Data']) )
      {
        $chunks['Data'] = pack( 'NA*'
          , $chunks['Sequence number']
          , $chunks['IDAT Data']
        );
      }

      $this->_packChunk( $chunks );
    }
  }
  
  /**
   * Pack IEND data.
   * Call at the end.
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_APng
   * @see http://www.w3.org/TR/PNG/#11IEND
   */
  public function _packIEND()
  {
    $this->_packChunk( 
      array( 'Name' => 'IEND' , 
             'Data' => '' )
    );
  }
  
  /**
   * A general method to pack chunks by their name.
   * This method cover single or multiple chunk.
   * Chunk with same name are added in order of appearence.
   * 
   * @param string $name Chunk name
   * @return void
   * @access protected
   * @throws \GDImage\Exception_APng
   * @see http://www.w3.org/TR/PNG/#5Chunk-layout
   */
  public function _packChunkByName( $name )
  {
    foreach( $this->_png_pack as $key => $chunk )
    {
      $chunk_name = substr( $key , 0 , 4 );
      
      // stay flexible about key name
      if( $chunk_name === $name || ( isset($chunk['Name']) && $chunk['Name'] === $name ) )
      {
        // process chunk
        $this->_packChunk( $chunk );
      }
    }
  }
  
  /**
   * A general method to pack a chunk.
   * 
   * Each keys store a PNG Chunk with 5 keys: 
   * - _raw: represent raw data for the Chunk;
   * - Length: Chunk Data length;
   * - Name: Chunk name (mandatory);
   * - Data: Chunk data (mandatory);
   * - CRC: Chunk CRC validation.
   * @see http://www.w3.org/TR/PNG/#5Chunk-layout
   * 
   * "_raw" data have priority over any generation.
   * 
   * @param array $chunk
   * @return void
   * @access protected
   * @throws \GDImage\Exception_APng
   * @see http://www.w3.org/TR/PNG/#5Chunk-layout
   */
  public function _packChunk( array $chunk )
  {
    if( isset( $chunk['_raw'] ) )
    {
      // _raw has priority
      $this->_binary .= $chunk['_raw'];
    }
    else
    {
      // validate empty data
      if( empty($chunk['Length']) ) $chunk['Length'] = strlen( $chunk['Data'] );
      if( empty($chunk['CRC']) ) $chunk['CRC'] = crc32( $chunk['Name'] . $chunk['Data'] );

      $this->_binary .= pack( 'NA4A*N'
        , $chunk['Length']
        , $chunk['Name']
        , $chunk['Data']
        , $chunk['CRC']
      );
    }
  }
  
  /**
   * Magic method to do standard packing thing.
   *
   * @param string $method Method name
   * @param array $args Arguments array
   * @return mixed
   * @access public
   */
  public function __call( $method , $args )
  {
  	if( strpos( $method , '_pack' ) === 0 )
  	{
  		// do pack that will cover multiple or single cases
      array_unshift( $args , substr( $method , 5 ) );
  		call_user_func_array( array( $this , '_packChunkByName' ) , $args );
  	}
  	else
  	{
  		// restore Fatal Error
  		trigger_error( sprintf( 'Call to undefined method %s::%s()' , get_class( $this ) , $method ) , E_USER_ERROR );
  	}
  }
}
