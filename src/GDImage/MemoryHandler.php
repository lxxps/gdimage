<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration settings:
 * - "memory_handler_gd_overhead": An approximative ratio to determine GD 
 *   resource memory usage from width and height, same than 
 *   "resource_true_color_gd_overhead"
 * - "memory_handler_limit": A limit (in bytes) for GD resource allocation, 
 *   default is 6 < 50% of memory_limit < memory_limit - 16
 */
if( ! Config::hasMemoryHandlerGdOverhead() ) {
  Config::setMemoryHandlerGdOverhead( 5.0846 );
}
// we need some calculation for that, so do it on constructor
// if( ! Config::hasMemoryHandlerLimit() ) {
//   Config::setMemoryHandlerLimit( '50%' ); // 50% of memory_limit
// }

/**
 * A class to handle GD resource creation and memory checks.
 * 
 * This class has ability to balance memory usage with saving some GD resources
 * to file and reload them on demand.
 * 
 * The purpose is to not go over memory_limit PHP setting, so there is no 
 * trouble with using memory_get_usage(): we do not care about all the memory 
 * used by the interpreter.
 * 
 * Note that MemoryHandler attachment belong to images and not to resources, so 
 * when a temporary resource is created, the MemoryHandler is not aware of this 
 * resource and will not try to sleep it.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage memory
 * @author     Loops <pierrotevrard@gmail.com>
 * @abstract
 */
class MemoryHandler
{

  /**
   * Current \GDImage\MemoryHandler instance.
   *
   * @var \GDImage\MemoryHandler Current instance
   * @access protected
   */
  public static $_instance;

  /**
   * Method to retrieve \GDImage\MemoryHandler instance.
   *
   * @param none
   * @return \GDImage\MemoryHandler
   * @access public
   * @static
   */
  public static function getInstance()
  {
  	if( ! static::$_instance )
  	{
  		static::$_instance = new static();
  	}
  	return static::$_instance;
  }
  
  /**
   * Get current memory usage of GD resource.
   * 
   * @param none
   * @return integer Memory usage in bytes
   * @access public
   * @static
   */
  public static function usage()
  {
    return static::getInstance()->getUsage();
  }
  
  /**
   * Register a resource instance.
   * 
   * Note that the same resource can be used over severals image instances,
   * so this method returns an identifier for MemoryHandler.
   * 
   * Identifier cannot be 0.
   * 
   * @param \GDImage\Resource_Abstract $rsc 
   * @return integer Identifier for the resource
   * @access public
   * @static
   */
  public static function register( Resource_Abstract $rsc )
  {
    return static::getInstance()->registering( $rsc );
  }
  
  /**
   * Unregister a resource instance from its identifier.
   * 
   * @param integer Identifier
   * @return boolean Success flag
   * @access public
   * @static
   */
  public static function unregister( $id )
  {
    return static::getInstance()->unregistering( $id );
  }
  
  /**
   * Check required memory for some given bytes, a size, filepath or binary data.
   * If possible, will try to sleep resources in order to free up 
   * some memory.
   * 
   * Size correspond to width followed by x followed by height (0x0).
   * 
   * This class is usefull when we attempt to create binary data.
   * 
   * This function will throw a \GDImage\Exception if there is not enough 
   * memory to get expected bytes.
   * 
   * Second parameter can be set to true to avoid this exception.
   * 
   * @param mixed $stuff
   * @param [boolean] $silently Avoid exception flag
   * @return boolean True if ok
   * @access public
   * @throws \GDImage\Exception_Memory
   * @static
   */
  public static function release( $stuff , $silently = false )
  {
    try
    {
      return static::getInstance()->releasing( $stuff );
    }
    catch( Exception $e )
    {
      if( $silently ) return false;
      
      // re-throw
      throw $e;
    }
  }
  
  /**
   * Sleep all resources to free maximum memory.
   * 
   * @param none
   * @return void
   * @access public
   * @static
   */
  public static function free()
  {
    return static::getInstance()->freeing();
  }
  
  /**
   * Notify memory handler that this resource has been balanced.
   * 
   * @param Resource_Abstract $rsc 
   * @param integer $multiplier (-1 or 1)
   * @return void
   * @access public
   * @static
   */
  public static function touch( Resource_Abstract $rsc , $multiplier )
  {
    return static::getInstance()->touching( $rsc , $multiplier );
  }
  
  /**
   * Format a integer in human readable size representation.
   * 
   * @param integer Bytes
   * @return boolean Success flag
   * @access public
   * @static
   */
  public static function format( $bytes )
  {
    $units = array( 'b' , 'k' , 'M' , 'G' , 'T' );
    $i = 0;
    $bytes = round( $bytes );
    while( ( $bytes / 1024 ) > 1 )
    {
      $bytes = round( $bytes / 1024 , 2 );
      $i++;
    }
    return $bytes.$units[$i];
  }
  
  /**
   * Format human readable bytes representation in human readable one.
   * 
   * @param string $size
   * @return boolean Success flag
   * @access public
   * @static
   */
  public static function unformat( $size )
  {
    $bytes = substr( $size , 0 , -1 );
    switch( strtolower( substr( $size , -1 ) ) )
    {
	    case 't':
        $bytes *= 1024;
        // continue
	    case 'g':
        $bytes *= 1024;
        // continue
      case 'm':
        $bytes *= 1024;
        // continue
      case 'k':
        $bytes *= 1024;
    }
    return $bytes;
  }
  

  /**
   * Stack of images.
   * 
   * Each key is the resource identifier associated to the Resource_Abstract 
   * instance.
   *
   * @var array Stack of Resource_Abstract.
   * @access protected
   */
  public $_stack = array();
  
  /**
   * Last identifier registers.
   * 
   * Will inscrease for each registered resource.
   * 
   * @var integer
   * @access protected
   */
  public $_i = 0;
  
  /**
   * Current GD resource memory usage, in bytes.
   *
   * @var integer
   * @access protected
   */
  public $_usage = 0;
  
  /**
   * Constructor
   * 
   * Initialize memory_limit.
   * 
   * @param none
   * @return void
   * @access public
   */
  public function __construct()
  {
    // determine default limit from memory limit
    if( ! Config::hasMemoryHandlerLimit() )
    {
      // 50% of memory_limit
      $limit = ini_get('memory_limit');
      if( $limit == -1 ) $limit = '128M';
      
      $limit = static::unformat( $limit );
      
      $l = floor( $limit * .5 );
      // we want to keep at least 16M for script
      $l16 = 16 * 1024 * 1024;
      if( ( $l + $l16 ) > $limit ) $l = $limit - $l16;
      // we want to keep at least 6M for resource storage
      $l6 = 6 * 1024 * 1024;
      if( $l < $l6 ) $l = $l6;
      
      Config::setMemoryHandlerLimit( $l );
    }
  }
  
  /**
   * Get current GD resource memory usage (approximation).
   * 
   * @param none
   * @return integer Memory usage in bytes
   * @access public
   */
  public function getUsage()
  {
    return $this->_usage;
  }
  
  /**
   * Register a resource instance.
   * 
   * @param \GDImage\Resource_Abstract $rsc 
   * @return integer Resource identifier
   * @access public
   */
  public function registering( Resource_Abstract $rsc )
  {
    // in not in array, update usage
    if( ! in_array( $rsc , $this->_stack , true ) ) $this->_usage += $rsc->getMemoryUsage( true );
            
    // preincrement
    $this->_stack[++$this->_i] = $rsc;
    
    return $this->_i;
  }
  
  /**
   * Unregister a resource instance by its identifier
   * 
   * @param integer $id Resource identifier
   * @return boolean Success flag
   * @access public
   */
  public function unregistering( $id )
  {
    if( isset( $this->_stack[$id] ) )
    {
      // local reference
      $rsc = $this->_stack[$id];
      
      unset( $this->_stack[$id] );
      
      // in not in array anymore, update usage
      if( ! in_array( $rsc , $this->_stack , true ) ) $this->_usage -= $rsc->getMemoryUsage( true );
      
      return true;
    }
    
    // not found
    return false;
  }
  
  /**
   * Free maximum resource.
   * 
   * @param none
   * @return void
   * @access public
   */
  public function freeing()
  {
    foreach( $this->_stack as $rsc ) $rsc->sleepGDResource();
    $this->_usage = 0;
  }
  
  /**
   * Notify that a resource has changed.
   * 
   * @param \GDImage\Resource_Abstract $rsc 
   * @param integer $multiplier (-1 or 1)
   * @return void
   * @access public
   */
  public function touching( Resource_Abstract $rsc , $multiplier )
  {
    // in not in array anymore, not interested
    if( in_array( $rsc , $this->_stack , true ) )
    {
      $this->_usage += $rsc->getMemoryUsage( false ) * $multiplier;
    }
  }
  
  /**
   * Check required memory for some given bytes, size, filepath or binary data.
   * 
   * If possible, will try to sleep resources in order to free up some memory.
   * 
   * Size correspond to width followed by x followed by height (0x0).
   * 
   * This function will throw a \GDImage\Exception if there is not enough 
   * memory to get expected bytes.
   * 
   * @param mixed $stuff
   * @return boolean True if ok
   * @access public
   * @throws \GDImage\Exception_Memory
   */
  public function releasing( $stuff )
  {
    $bytes = null;
    
    // determine bytes
    if( is_numeric( $stuff ) )
    {
      $bytes = $stuff | 0;
    }
    elseif( is_string( $stuff ) )
    {
      // match size
      $matches = array();

      if( preg_match( '~^([0-9]+)x([0-9]+)$~i' , $stuff , $matches ) )
      {
        list( , $w , $h ) = $matches;
        $bytes = $w * $h * Config::getMemoryHandlerGdOverhead();
      }
      // no notice please, I do not even know if it is a file
      elseif( $info = @ getimagesize( $stuff ) )
      {
        list( $w , $h ) = $info;
        $bytes = $w * $h * Config::getMemoryHandlerGdOverhead();
      }
      // no notice please, I do not even know if it is a binary string
      elseif( $info = @ getimagesizefromstring( $stuff ) )
      {
        list( $w , $h ) = $info;
        $bytes = $w * $h * Config::getMemoryHandlerGdOverhead();
      }
    }
    
    if( $bytes === null )
    {
      // we fails
      throw new Exception_Memory( array( get_class( $this ) , $stuff ) , 8010 );
    }
    
    
    // release memory
    
    // we want at list this size available
    $limit = Config::getMemoryHandlerLimit() - $bytes;
    
    // at this point, we can watch for memory usage if it is not above this limit
    reset( $this->_stack ); // reset to be safe
    while( $this->_usage >= $limit && ( $rsc = current( $this->_stack ) ) )
    {
      // sleep resource, will automatically update usage
      $rsc->sleepGdResource();
      
      // move forward
      next( $this->_stack );
    }
    reset( $this->_stack ); // for the next developer that will use this array
    
    if( $this->_usage >= $limit )
    {
      // not enough memory
      throw new Exception_Memory( array( get_class( $this ) , static::format( $bytes ) , static::format( Config::getMemoryHandlerLimit() ) , static::format( $this->_usage ) , static::format( $bytes ) ) , 8000 );
    }
    
    return true;
  }
}

/**
 * Make sure getimagesizefromstring() is available in current namespace.
 * getimagesizefromstring() is available since PHP 5.4.0.
 */
if( ! ( function_exists( '\\getimagesizefromstring' ) || function_exists( __NAMESPACE__.'\\getimagesizefromstring' ) ) )
{
  // for this one, a fallback to getimagesize() is feasible (and easy)
  // @see http://php.net/manual/en/function.getimagesizefromstring.php
  function getimagesizefromstring( $imagedata , &$imageinfo = array() )
  {
    // create temporary file
    $tmp = tempnam( Config::getTemporaryDirectory() , Config::getTemporaryPrefix() );
    
    if( file_put_contents( $tmp , $imagedata ) !== false )
    {
      // fallback to getimagesize()
      $ret = getimagesize( $tmp , $imageinfo );
      
      // destroy temporary file
      unlink( $tmp );
      
      // return result
      return $ret;
    }
      
    // destroy temporary file
    unlink( $tmp );
    
    // fails to put contents
    return false;
  }
}
