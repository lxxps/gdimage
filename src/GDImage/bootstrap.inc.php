<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

// GDImage library Bootstrap
if( ! extension_loaded('gd') ) trigger_error( 'GDImage library require "gd" extension to be loaded, see http://php.net/manual/en/book.image.php' , E_USER_ERROR );
if( version_compare( PHP_VERSION , '5.3.0' ) < 0 ) trigger_error( 'GDImage library at least PHP 5.3.0' , E_USER_ERROR );

// Require autoload class
require_once dirname(__FILE__) . '/Autoloader.php';
\GDImage\Autoloader::getInstance()->register();
