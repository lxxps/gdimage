<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Export to file driver
 *
 * @package    GDImage
 * @subpackage factory
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Factory_ExportDriver_Interface
 * @abstract
 */
abstract class Factory_ExportDriver_Abstract implements Factory_ExportDriver_Interface
{
  
  /**
   * Local settings.
   * This property should contains default settings in use on the driver.
   * 
   * @var array
   * @access protected
   */
  public $_settings = array();
  
  /**
   * Extra export parameters to pass to 
   * \GDImage\Image_Interface->toFile()/toBinary() method.
   * 
   * @var array
   * @access protected
   */
  public $_parameters = array();
  
  /**
   * Construct driver from settings.
   * 
   * Stuff can be a string or an array with settings.
   * 
   * May throw exception if settings are not valid.
   *
   * @param mixed $settings
   * @return void
   * @access public
   * @throws \GDImage\Exception_Factory
   * @implements \GDImage\Factory_ExportDriver_Interface
   */
  public function __construct( $settings )
  {
    if( is_array( $settings ) )
    {
      // sum settings with default values
      $this->_settings = $settings + $this->_settings;
      
      // extract parameter from settings
      array_walk( $settings , array( $this , '_settingToParameter' ) );
    }
  }
  
  /**
   * Walk method to extract extra parameters from settings
   * 
   * @param mixed $value
   * @param mixed $key
   * @return void
   * @access protected
   */
  public function _settingToParameter( $val , $key )
  {
    if( is_numeric( $key ) )
    {
      $this->_parameters[] = $val;
    }
  }
  
  /**
   * Returns MIME Type to use if available from settings.
   * 
   * Most of drivers will return null.
   *
   * @param none
   * @return string
   * @access public
   * @implements \GDImage\Factory_ExportDriver_Interface
   */
  public function getMimeType()
  {
    return null;
  }
  
  /**
   * Fetch binary data from an image and extra parameters.
   * 
   * @param \GDImage\Image_Interface $image
   * @param mixed $key
   * @return string
   * @access protected
   */
  public function _getBinary( Image_Interface $image )
  {
    return call_user_func_array( array( $image , 'toBinary' ) , $this->_parameters );
  }

}
