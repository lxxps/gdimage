<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Export to file driver
 *
 * @package    GDImage
 * @subpackage factory
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Factory_ExportDriver_Interface
 * @extends    \GDImage\Factory_ExportDriver_Abstract
 */
class Factory_ExportDriver_File extends Factory_ExportDriver_Abstract
{
  /**
   * Local settings.
   * This property should contains default settings in use on the driver.
   * 
   * @var array
   * @access protected
   */
  public $_settings = array(
    // we need a file setting for this export
    'file' => null ,
  );
  
  /**
   * Detect if the driver has to be used to export stuff.
   * 
   * Note that a Factory::DRIVER_* constant is not necessary 
   * to detect a driver.
   *
   * @param string $stuff
   * @return boolean 
   * @access public
   * @static
   * @implements \GDImage\Factory_ExportDriver_Interface
   */
  public static function detect( $stuff )
  {
    
    // any other solution to detect path?
    return ( $pathinfo = pathinfo( $stuff ) ) 
           && isset( $pathinfo['dirname'] ) && isset( $pathinfo['basename'] )
           // by convenience we won't accept stuff if dirname is not really specified
           && ( substr( $stuff , 0 , strlen( $pathinfo['dirname'] ) ) === $pathinfo['dirname'] 
              // except if an extension can be found
              || isset($pathinfo['extension']) )
          ;
  }
  
  /**
   * Construct driver from settings.
   * 
   * This driver can consider string as file path.
   * 
   * Also, if no file path is given, this driver will throw an exception.
   *
   * @param mixed $settings
   * @return void
   * @access public
   * @throws \GDImage\Exception_Factory
   * @implements \GDImage\Factory_ExportDriver_Interface
   */
  public function __construct( $settings )
  {
    if( is_string( $settings ) )
    {
      // consider string as file
      $settings = array( 'file' => $settings );
    }
    
    parent::__construct( $settings );
    
    if( empty($this->_settings['file']) )
    {
      throw new Exception_Factory( array( get_class( $this ) , 'file' ) , 3060 );
    }
  }
  
  /**
   * Returns MIME Type to use if available from settings.
   * 
   * For this driver, a MIME type can be determined from file extension.
   *
   * @param none
   * @return string
   * @access public
   * @implements \GDImage\Factory_ExportDriver_Interface
   */
  public function getMimeType()
  {
    if( $ext = pathinfo( $this->_settings['file'] , \PATHINFO_EXTENSION ) )
    {
      // we can determine a MIME Type from this extension
      $guesser = new MimeTypeGuesser_FromFile_Extension();
      
      // invoke guesser
      $mime = $guesser( $ext );
      if( $mime ) return $mime;
    }
    
    return null;
  }
  
  /**
   * Returns expected result from an \GDImage\Image_Interface.
   * Returns null if the driver fails.
   * 
   * Note that at this point, the image has already been converted if necessary.
   *
   * @param \GDImage\Image_Interface $image
   * @return string 
   * @access public
   */
  public function __invoke( Image_Interface $image )
  {
    $filepath = $this->_settings['file'];
    
    // build file path if there is some missing extension
    if( ! pathinfo( $filepath , PATHINFO_EXTENSION ) )
    {
      // look for extension to use for mime type
      
      // we can determine a MIME Type from this extension
      $guesser = new MimeTypeGuesser_FromFile_Extension();
      
      // do reverse look up
      // call static method getMimeType() in not-static context, it works
      $ext = $guesser->reverse( $image->getMimeType() );
      
      // complete filepath (avoid double dot)
      $filepath = rtrim( $filepath , '.' ).'.'.$ext;
    }
    
    // get toFile() parameters
    $args = $this->_parameters;
    // prepend filepath
    array_unshift( $args , $filepath );
    // call
    if( call_user_func_array( array( $image , 'toFile' ) , $args ) )
    {
      // return correct filepath
      return $filepath;
    }
    
    // oups, we fail
    return false;
  }

}
