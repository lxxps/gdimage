<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Export to binary data driver
 *
 * @package    GDImage
 * @subpackage factory
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Factory_ExportDriver_Interface
 * @extends    \GDImage\Factory_ExportDriver_Binary
 */
class Factory_ExportDriver_Output extends Factory_ExportDriver_Binary
{
  /**
   * Local settings.
   * This property should contains default settings in use on the driver.
   * 
   * @var array
   * @access protected
   */
  public $_settings = array(
    // sent mime type header
    'mimetype' => true ,
  );
  
  /**
   * Detect if the driver has to be used to export stuff.
   * 
   * Note that a Factory::DRIVER_* constant is not necessary 
   * to detect a driver.
   *
   * @param string $stuff
   * @return boolean 
   * @access public
   * @static
   * @implements \GDImage\Factory_ExportDriver_Interface
   */
  public static function detect( $stuff )
  {
    // only key detection is available for this driver
    return in_array( strtolower( $stuff ) , array( Factory::DRIVER_OUTPUT , ) , true );
  }
  
  /**
   * Returns expected result from an \GDImage\Image_Interface.
   * Returns null if the driver fails.
   * 
   * Note that at this point, the image has already been converted if necessary.
   *
   * @param \GDImage\Image_Interface $image
   * @return string 
   * @access public
   */
  public function __invoke( Image_Interface $image )
  {
    // fetch binary data
    if( ! ( $binary = parent::__invoke( $image ) ) )
    {
      return false;
    }
    
    // send header
    if( $this->_settings['mimetype'] )
    {
      // call static method in not-static context, it works!
      header( sprintf( 'Content-Type: %s' , $image->getMimeType() ) );
    }
    
    // echo data
    echo $binary;
    
    // finally return data
    return $data;
  }

}
