<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Interface for \GDImage\Factory_ExportDriver_* instances
 *
 * @package    GDImage
 * @subpackage factory
 * @author     Loops <pierrotevrard@gmail.com>
 * @interface
 */
interface Factory_ExportDriver_Interface
{
  
  /**
   * Detect if the driver has to be used to export stuff.
   * 
   * Note that a Factory::DRIVER_* constant is not necessary 
   * to detect a driver.
   *
   * @param string $stuff
   * @return boolean 
   * @access public
   * @static
   */
  public static function detect( $stuff );
  
  /**
   * Construct driver from settings.
   * 
   * Stuff can be a string or an array with settings.
   * 
   * May throw exception if settings are not valid.
   *
   * @param mixed $settings
   * @return void
   * @access public
   * @throws \GDImage\Exception_Factory
   */
  public function __construct( $settings );
  
  /**
   * Returns MIME Type to use if available from settings.
   * 
   * Most of drivers will return null.
   *
   * @param none
   * @return string
   * @access public
   */
  public function getMimeType();
  
  /**
   * Returns expected result from an \GDImage\Image_Interface.
   * Returns null if the driver fails.
   * 
   * Note that at this point, the image has already been converted if necessary.
   *
   * @param \GDImage\Image_Interface $image
   * @return mixed 
   * @access public
   */
  public function __invoke( Image_Interface $image );

}
