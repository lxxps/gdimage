<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Factory driver to import from data URI scheme.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage factory
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Factory_ImportMorpher_Interface
 * @extends    \GDImage\Factory_ImportMorpher_Binary
 */
class Factory_ImportMorpher_DataUri extends Factory_ImportMorpher_Binary
{
  
  /**
   * Detect if the driver has to be used to import stuff.
   *
   * @param mixed $stuff Anything
   * @return boolean 
   * @access public
   * @static
   * @implements \GDImage\Factory_ImportMorpher_Interface
   */
  public static function detect( $stuff )
  {
    // should be enough
    return is_string( $stuff ) && substr( $stuff , 0 , 5 ) === 'data:';
  }
  
  /**
   * MIME Type found in data URI
   * 
   * @var string
   * @access protected
   */
  public $_mime;
  
  /**
   * Construct morpher from stuff.
   * 
   * May check valid stuff.
   *
   * @param mixed $stuff
   * @return void
   * @access public
   * @throws \GDImage\Exception_Factory
   * @implements \GDImage\Factory_ImportMorpher_Interface
   */
  public function __construct( $stuff )
  {
    //   dataurl    := "data:" [ mediatype ] [ ";base64" ] "," data
    //   mediatype  := [ type "/" subtype ] *( ";" parameter )
    //   data       := *urlchar
    //   parameter  := attribute "=" value
    
    // just do it simple
    
    // check data scheme (should be true
    if( substr( $stuff , 0 , 5 ) !== 'data:' )
    {
      // invalid
      throw new Exception_Factory( array( get_class( $this ) , substr( func_get_arg(0) , 0 , 32 ).'[...]' ) , 3010 );
    }
    
    // check for last coma (cannot start by coma too)
    if( ! ( $pos = strrpos( $stuff , ',' ) ) )
    {
      // invalid
      throw new Exception_Factory( array( get_class( $this ) , substr( func_get_arg(0) , 0 , 32 ).'[...]' ) , 3010 );
    }
    
    // separate data from parameters
    $param  = substr( $stuff , 5 , $pos - 5 ); // remove data: stuff, do not get coma
    $data   = substr( $stuff , $pos + 1 ); // do not get coma
    
    // explode parameters
    $param = explode( ';' , $param );
    
    // look if the data URI has a MIME Type
    $this->_mime = reset( $param );
    
    // to stay compatible with PHP data:// wrapper, we want to trim out '//' 
    // stuff that may appears at the begin of the MIME type definition
    // @see http://php.net/manual/fr/wrappers.data.php
    if( strpos( $this->_mime , '//' ) === 0 )
    {
      $this->_mime = substr( $this->_mime , 2 );
    }
    
    if( ! preg_match( '~^[a-z0-9\\-]+/[a-z0-9\\-+.]+$~' , $this->_mime ) )
    {
      // does not looks like correct mime type, do not use it
      $this->_mime = null;
    }
    
    // look for base64 string at the end of the scheme
    if( end( $param ) === 'base64' )
    {
      // do base64 decoding
      // no need to urldecode the base64 string
      if( ! ( $data = base64_decode( $data ) ) )
      {
        throw new Exception_Factory( array( get_class( $this ) , substr( func_get_arg(0) , 0 , 32 ).'[...]' ) , 3020 );
      }
    }
    else
    {
      // we want to urldecode the string
      $data = urldecode( $data );
    }
    
    // call parent method
    parent::__construct( $data );
  }

  
  /**
   * Returns the MIME Type of the stuff.
   * Returns null if MIME Type cannot be determined.
   * 
   * Note that this method is not always called especially when user want 
   * an expected MIME Type to be used.
   *
   * @param none
   * @return string 
   * @access public
   * @implements \GDImage\Factory_ImportMorpher_Interface
   */
  public function getMimeType()
  {
    // by convenience, we trust data URI MIME Type
    if( $this->_mime ) return $this->_mime;
    
    return parent::getMimeType();
  }
}
