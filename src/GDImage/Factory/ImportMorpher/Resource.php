<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Factory driver to import from \GDImage\Resource_Abstract.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage factory
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Factory_ImportMorpher_Interface
 * @extends    \GDImage\Factory_ImportMorpher_Image
 */
class Factory_ImportMorpher_Resource extends Factory_ImportMorpher_Image
{
  
  /**
   * Detect if the driver has to be used to import stuff.
   *
   * @param mixed $stuff Anything
   * @return boolean 
   * @access public
   * @static
   * @implements \GDImage\Factory_ImportMorpher_Interface
   */
  public static function detect( $stuff )
  {
    // check valid resource
    return is_object( $stuff ) && $stuff instanceof Resource_Abstract;
  }
  
  /**
   * Construct morpher from stuff.
   * 
   * May check valid stuff.
   *
   * @param mixed $stuff
   * @return void
   * @access public
   * @throws \GDImage\Exception_Factory
   * @implements \GDImage\Factory_ImportMorpher_Interface
   */
  public function __construct( $stuff )
  {
    // get default image instance to use
    $image = Factory::instance();
    $image->setResource( $stuff );
    parent::__construct( $image );
  }

}
