<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Interface for \GDImage\Factory_ImportMorpher_* instances
 *
 * @package    GDImage
 * @subpackage factory
 * @author     Loops <pierrotevrard@gmail.com>
 * @interface
 */
interface Factory_ImportMorpher_Interface
{
  
  /**
   * Detect if the driver has to be used to import stuff.
   *
   * @param mixed $stuff Anything
   * @return boolean 
   * @access public
   * @static
   */
  public static function detect( $stuff );
  
  /**
   * Construct morpher from stuff.
   * 
   * May check valid stuff.
   *
   * @param mixed $stuff
   * @return void
   * @access public
   * @throws \GDImage\Exception_Factory
   */
  public function __construct( $stuff );
  
  /**
   * Returns the MIME Type of the stuff.
   * Returns null if MIME Type cannot be determined.
   * 
   * Note that this method is not always called especially when user want 
   * an expected MIME Type to be used.
   *
   * @param none
   * @return string 
   * @access public
   */
  public function getMimeType();
  
  /**
   * Returns the file path to use for \GDImage\Factory->importation().
   * Returns null if no file path should be used.
   *
   * @param none
   * @return string 
   * @access public
   * @static
   */
  public function getFile();
  
  /**
   * Returns the binary data to use for \GDImage\Factory->importation().
   * Returns null if no binary data should be used.
   *
   * @param none
   * @return string 
   * @access public
   */
  public function getBinary();
  
  /**
   * Returns the \GDImage\Image_Interface instance to use for 
   * \GDImage\Factory->importation().
   * Returns null if no \GDImage\Image_Interface instance should be used.
   *
   * @param none
   * @return \GDImage\Image_Interface
   * @access public
   */
  public function getImage();

}
