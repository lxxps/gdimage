<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Factory driver to import from GD resource.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage factory
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Factory_ImportMorpher_Interface
 * @extends \GDImage\Factory_ImportMorpher_Resource
 */
class Factory_ImportMorpher_GdResource extends Factory_ImportMorpher_Resource
{
  
  /**
   * Detect if the driver has to be used to import stuff.
   *
   * @param mixed $stuff Anything
   * @return boolean 
   * @access public
   * @static
   * @implements \GDImage\Factory_ImportMorpher_Interface
   */
  public static function detect( $stuff )
  {
    // check valid resource
    return is_resource( $stuff ) && get_resource_type( $stuff ) === 'gd';
  }
  
  /**
   * Construct morpher from stuff.
   * 
   * May check valid stuff.
   *
   * @param mixed $stuff
   * @return void
   * @access public
   * @throws \GDImage\Exception_Factory
   * @implements \GDImage\Factory_ImportMorpher_Interface
   */
  public function __construct( $stuff )
  {
    // construct from resource
    parent::__construct( Resource_Abstract::createFromGdResource( $stuff ) );
  }

}
