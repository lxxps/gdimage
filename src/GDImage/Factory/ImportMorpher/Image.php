<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Factory driver to import from \GDImage\Resource_Abstract.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage factory
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Factory_ImportMorpher_Interface
 */
class Factory_ImportMorpher_Image implements Factory_ImportMorpher_Interface
{
  
  /**
   * Detect if the driver has to be used to import stuff.
   *
   * @param mixed $stuff Anything
   * @return boolean 
   * @access public
   * @static
   * @implements \GDImage\Factory_ImportMorpher_Interface
   */
  public static function detect( $stuff )
  {
    // check valid resource
    return is_object( $stuff ) && $stuff instanceof Image_Interface;
  }
  
  /**
   * Resource
   * 
   * @var \GDImage\Image_Interface
   * @access protected
   */
  public $_image;
  
  /**
   * Construct morpher from stuff.
   * 
   * May check valid stuff.
   *
   * @param mixed $stuff
   * @return void
   * @access public
   * @throws \GDImage\Exception_Factory
   * @implements \GDImage\Factory_ImportMorpher_Interface
   */
  public function __construct( $stuff )
  {
    // nothing else
    $this->_image = $stuff;
  }
  
  /**
   * Returns the MIME Type of the stuff.
   * Returns null if MIME Type cannot be determined.
   * 
   * Note that this method is not always called especially when user want 
   * an expected MIME Type to be used.
   * 
   *
   * @param none
   * @return string 
   * @access public
   * @implements \GDImage\Factory_ImportMorpher_Interface
   */
  public function getMimeType()
  {
    // return MIME Type expected by the image resource
    return $this->_image->getMimeType();
  }
  
  /**
   * Returns the file path to use for \GDImage\Factory->importation().
   * Returns null if no file path should be used.
   *
   * @param none
   * @return string 
   * @access public
   * @implements \GDImage\Factory_ImportMorpher_Interface
   */
  public function getFile()
  {
    return null;
  }
  
  /**
   * Returns the binary data to use for \GDImage\Factory->importation().
   * Returns null if no binary data should be used.
   *
   * @param none
   * @return string 
   * @access public
   * @implements \GDImage\Factory_ImportMorpher_Interface
   */
  public function getBinary()
  {
    return null;
  }
  
  /**
   * Returns the \GDImage\Image_Interface instance to use for 
   * \GDImage\Factory->importation().
   * Returns null if no \GDImage\Image_Interface instance should be used.
   *
   * @param none
   * @return \GDImage\Image_Interface
   * @access public
   */
  public function getImage()
  {
    return $this->_image;
  }

}
