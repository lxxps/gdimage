<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Factory driver to import from files.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage factory
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Factory_ImportMorpher_Interface
 */
class Factory_ImportMorpher_File implements Factory_ImportMorpher_Interface
{
  
  /**
   * Detect if the driver has to be used to import stuff.
   * 
   * Note that we do not check if the file exists, we just check if 
   * the stuff looks like a file path.
   *
   * @param mixed $stuff Anything
   * @return boolean 
   * @access public
   * @static
   * @implements \GDImage\Factory_ImportMorpher_Interface
   */
  public static function detect( $stuff )
  {
    // not a string, stop right now
    if( ! is_string( $stuff ) ) return false;
    
    
    // check URL like string
    // note that we allow ANY valid stream wrappers to be used
    if( 
        ( $parse = parse_url( $stuff ) )
        // we need at least a scheme and a host
        && isset($parse['scheme']) && isset($parse['host']) 
        // PHP support data:// wrapper, but we may not want to use it
        // @see http://php.net/manual/fr/wrappers.data.php
        && $parse['scheme'] !== 'data'
        // just to be sure that scheme can be used
        && in_array( $parse['scheme'] , stream_get_wrappers() , true ) 
      )
    {
      // ok, it is a valid URL
      return true;
    }
    
    
    // check local file
    // basename just strip last part of the filepath, if any
    if( basename( $stuff ) && realpath( $stuff ) )
    {
      return true;
    }
    
    // none of physical or virtual path, stop
    return false;
  }
  
  /**
   * File path
   * 
   * @var string
   * @access protected
   */
  public $_filepath;
  
  /**
   * Construct morpher from stuff.
   * 
   * May check valid stuff.
   *
   * @param mixed $stuff
   * @return void
   * @access public
   * @throws \GDImage\Exception_Factory
   * @implements \GDImage\Factory_ImportMorpher_Interface
   */
  public function __construct( $stuff )
  {
    // Check file exists
    // We cannot use is_file() or something like that because
    // some wrappers does not support the stat() family
    // @see http://www.php.net/manual/en/wrappers.http.php
    // So, use fopen() with read flag
    if( false === ( $h = @ fopen( $stuff , 'rb' ) ) )
    {
      // invalid
      throw new Exception_Factory( array( get_class( $this ) , func_get_arg(0) ) , 3030 );
    }
    // close handle right now
    fclose( $h );
    
    // looks ok
    $this->_filepath = $stuff;
  }
  
  /**
   * Returns the MIME Type of the stuff.
   * Returns null if MIME Type cannot be determined.
   * 
   * Note that this method is not always called especially when user want 
   * an expected MIME Type to be used.
   *
   * @param none
   * @return string 
   * @access public
   * @implements \GDImage\Factory_ImportMorpher_Interface
   */
  public function getMimeType()
  {
    return MimeTypeGuesser_Collection::fromFile( $this->_filepath );
  }
  
  /**
   * Returns the file path to use for \GDImage\Factory->importation().
   * Returns null if no file path should be used.
   *
   * @param none
   * @return string 
   * @access public
   * @implements \GDImage\Factory_ImportMorpher_Interface
   */
  public function getFile()
  {
    return $this->_filepath;
  }
  
  /**
   * Returns the binary data to use for \GDImage\Factory->importation().
   * Returns null if no binary data should be used.
   *
   * @param none
   * @return string 
   * @access public
   * @implements \GDImage\Factory_ImportMorpher_Interface
   */
  public function getBinary()
  {
    return null;
  }
  
  /**
   * Returns the \GDImage\Image_Interface instance to use for 
   * \GDImage\Factory->importation().
   * Returns null if no \GDImage\Image_Interface instance should be used.
   *
   * @param none
   * @return \GDImage\Image_Interface
   * @access public
   */
  public function getImage()
  {
    return null;
  }

}
