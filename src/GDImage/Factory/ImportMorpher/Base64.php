<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Factory driver to import from base64 data.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage factory
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Factory_ImportMorpher_Interface
 * @extends \GDImage\Factory_ImportMorpher_Binary
 */
class Factory_ImportMorpher_Base64 extends Factory_ImportMorpher_Binary
{
  
  /**
   * Detect if the driver has to be used to import stuff.
   *
   * @param mixed $stuff Anything
   * @return boolean 
   * @access public
   * @static
   * @implements \GDImage\Factory_ImportMorpher_Interface
   */
  public static function detect( $stuff )
  {
    // detect base64 encoding if there is not any character out 
    // of the base64 alphabet
    // @see http://tools.ietf.org/html/rfc2045#section-6.8
    return is_string( $stuff ) && ( ! preg_match( '~[^A-Za-z0-9+/=]~' , $stuff ) );
  }
  
  /**
   * Construct morpher from stuff.
   * 
   * May check valid stuff.
   *
   * @param mixed $stuff
   * @return void
   * @access public
   * @throws \GDImage\Exception_Factory
   * @implements \GDImage\Factory_ImportMorpher_Interface
   */
  public function __construct( $stuff )
  {
    // do base64 decoding
    if( ! ( $stuff = base64_decode( $stuff ) ) )
    {
      throw new Exception_Factory( array( get_class( $this ) , substr( func_get_arg(0) , 0 , 32 ).'[...]' ) , 3020 );
    }
    
    // call parent constructor with decoded data
    parent::__construct( $stuff );
  }
  
}
