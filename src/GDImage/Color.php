<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration setting "color_default_value"
 * 
 * This color can be any color representation.
 */
if( ! Config::hasColorDefaultValue() ) {
  Config::setColorDefaultValue( '0000007F' );
}

/**
 * Class to manage color RGBA representation.
 * For convenience, we always consider color with alpha channel.
 * 
 *  - Hexadecimal representation: "RRGGBB" or "RRGGBBAA"
 *  - Integer representation: ( R << 16 ) + ( G << 8 ) + B or ( A << 24 ) + ( R << 16 ) + ( G << 8 ) + B
 *  - Array representation: array( R , G , B ) or array( R , G , B , A )
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage color
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Color
{
  /**
   * Useful constant to represent alpha channel value
   * that is fully transparent
   * 
   * @var integer
   * @const
   */
  const ALPHA_TRANSPARENT = 0x7F;
          
  /**
   * Red value, from 0 to 255
   *
   * @var integer Red
   * @access protected
   */
  public $_r = 0;
  
  /**
   * Green value, from 0 to 255
   *
   * @var integer Green
   * @access protected
   */
  public $_g = 0;
  
  /**
   * Blue value, from 0 to 255
   *
   * @var integer Blue
   * @access protected
   */
  public $_b = 0;
  
  /**
   * Alpha channel, from 0 to 127
   *
   * @var integer Alpha
   * @access protected
   */
  public $_a  = 0;
  
  /**
   * \GDImage\Color constructor.
   * Argument can be an integer (color identifier),
   * a string (hexadecimal representation), or 
   * an array ( array( red , green , blue ) ).
   *
   * @param mixed $color Color
   * @return void
   * @access public
   */
  public function __construct( $color = null )
  {
  	if( $color === null )
  	{
  		// Get default color values from configuration
  		$this->set( Config::getColorDefaultValue() );
  	}
  	else
  	{
  		$this->set( $color );
  	}
  }
  
  /**
   * Method to assign color information.
   * Argument can be an integer (color identifier), a string (hexadecimal 
   * representation), or an array ( array( red , green , blue [, alpha] ) ).
   *
   * @param mixed $color Color
   * @return void
   * @access public
   */
  public function set( $color )
  {
  	// Append new color informations
    if( is_string( $color ) )
    {
      $this->_setFromString( $color );
    }
    elseif( is_numeric( $color ) )
    {
    	$this->_setFromInteger( $color | 0 );
    }
    elseif( is_array( $color ) )
    {
    	$this->_setFromArray( $color );
    }
    else
    {
    	throw new Exception( sprintf( 'Color cannot be extracted from "%s"' , var_export( $color , true ) ) , 9301 );
    }
  }
  
  /**
   * Method to change red information.
   *
   * @param integer $red
   * @return void
   * @access public
   */
  public function setRed( $red )
  {
    // Make sure maximum value is 255, also force integer.
    $this->_r = max( 0 , min( $red | 0 , 0xFF ) );
  }
  
  /**
   * Method to get red information.
   *
   * @param none
   * @return integer Red
   * @access public
   */
  public function getRed()
  {
  	return $this->_r;
  }
  
  /**
   * Method to change green information.
   *
   * @param integer $v
   * @return void
   * @access public
   */
  public function setGreen( $green )
  {
    // Make sure maximum value is 255, also force integer.
    $this->_g = max( 0 , min( $green | 0 , 0xFF ) );
  }
  
  /**
   * Method to get green information.
   *
   * @param none
   * @return integer Green
   * @access public
   */
  public function getGreen()
  {
  	return $this->_g;
  }
  
  /**
   * Method to change blue information.
   *
   * @param integer $blue
   * @return void
   * @access public
   */
  public function setBlue( $blue )
  {
    // Make sure maximum value is 255, also force integer.
    $this->_b = max( 0 , min( $blue | 0 , 0xFF ) );
  }
  
  /**
   * Method to get blue information.
   *
   * @param none
   * @return integer Blue
   * @access public
   */
  public function getBlue()
  {
  	return $this->_b;
  }
  
  /**
   * Method to assign alpha channel.
   *
   * @param integer $alpha
   * @return void
   * @access public
   */
  public function setAlpha( $alpha )
  {
    // Make sure maximum value is 127, also force integer.
  	$this->_a = max( 0 , min( $alpha | 0 , 0x7F ) );
  }
  
  /**
   * Method to get alpha channel.
   *
   * @param none
   * @return integer Alpha
   * @access public
   */
  public function getAlpha()
  {
  	return $this->_a;
  }
  
  /**
   * Method to assign color informations from a string
   * (hexadecimal representation).
   *
   * @param string $color Color
   * @return void
   * @access protected
   */
  public function _setFromString( $color )
  {
    // There is only two hexa characters to convert, so 
    // the maximum will be 255, nothing higher
  	$this->_r = hexdec( substr( $color , 0 , 2 ) );
  	$this->_g = hexdec( substr( $color , 2 , 2 ) );
  	$this->_b = hexdec( substr( $color , 4 , 2 ) );
  	$this->_a = hexdec( substr( $color , 6 , 2 ) ) & 0x7F;
  }
  
  /**
   * Method to assign color informations from a integer
   * (color identifier).
   *
   * @param integer $color Color
   * @return void
   * @access protected
   */
  public function _setFromInteger( $color )
  {
  	$this->_r = ( $color >> 16 ) & 0xFF;
  	$this->_g = ( $color >> 8 ) & 0xFF;
  	$this->_b = ( $color >> 0 ) & 0xFF;
  	$this->_a = ( $color >> 24 ) & 0x7F;
  }
  
  /**
   * Method to assign color informations from a array
   * (array( red , green , blue )).
   *
   * @param array $color Color
   * @return void
   * @access protected
   */
  public function _setFromArray( array $color )
  {
    if( count($color) > 3 ) list( $this->_r , $this->_g , $this->_b , $this->_a ) = $color;
    else list( $this->_r , $this->_g , $this->_b ) = $color;
    
    // Make sure maximum value is 255, also force integer.
    $this->_r = max( 0 , min( $this->_r | 0 , 0xFF ) );
    $this->_g = max( 0 , min( $this->_g | 0 , 0xFF ) );
    $this->_b = max( 0 , min( $this->_b | 0 , 0xFF ) );
    $this->_a = max( 0 , min( $this->_a | 0 , 0x7F ) );
  }
  
  /**
   * Returns the hexadecimal representation of the color.
   *
   * @param none
   * @return string
   * @access public
   */
  public function toString()
  {
    // by convenience all characters to uppercase
    return sprintf( '%02X%02X%02X%02X' , $this->_r , $this->_g , $this->_b , $this->_a );
  }
  
  /**
   * Alias for lazy bastards that do not want to write toString().
   *
   * @param none
   * @return string
   * @access public
   */
  public function toStr()
  {
    return $this->toString();
  }
  
  /**
   * Magic __toString function().
   * Allias for toString()
   *
   * @param none
   * @return string
   * @access public
   */
  public function __toString()
  {
    return $this->toString();
  }
  
  /**
   * Returns the integer representation of the color.
   *
   * @param none
   * @return string
   * @access public
   */
  public function toInteger()
  {
  	return ( $this->_r << 16 ) + ( $this->_g << 8 ) + ( $this->_b << 0 ) + ( $this->_a << 24 );
  }
  
  /**
   * Alias for lazy bastards that do not want to write toInteger().
   *
   * @param none
   * @return string
   * @access public
   */
  public function toInt()
  {
    return $this->toInteger();
  }
  
  /**
   * Pseudo magic method to convert to integer
   * I can predict that one day this magic method will exist.
   *
   * @param none
   * @return integer
   * @access public
   */
  public function __toInteger()
  {
    return $this->toInteger();
  }
  
  /**
   * Returns the array representation of the color.
   * Returns numeric and named key in order to use list() statement
   * or access to a specific information.
   *
   * @param none
   * @return string
   * @access public
   */
  public function toArray()
  {
  	return array(
      $this->_r , $this->_g , $this->_b , $this->_a ,
      'red' => $this->_r , 'green' => $this->_g , 'blue' => $this->_b , 'alpha' => $this->_a ,
    );
  }
  
  /**
   * Pseudo magic method to convert to array
   * I can predict that one day this magic method will exist.
   *
   * @param none
   * @return array
   * @access public
   */
  public function __toArray()
  {
    return $this->toArray();
  }
}