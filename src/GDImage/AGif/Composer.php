<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration settings:
 * - "a_gif_composer_optimization": optimization flags, bitwise from
 *   - 1: AGif_Composer::OPTIMIZE_TRANSPARENT_TO_CROP
 *   - 2: AGif_Composer::OPTIMIZE_DISPOSAL_TO_CROP
 *   - 4: AGif_Composer::OPTIMIZE_DISPOSAL_TO_TRANSPARENT
 * 
 * For convenience, this setting get same name that AGIF one.
 * 
 * @see \GDImage\APng_Composer::_doFrameDowngradeImageDescriptor()
 * @see \GDImage\APng_Composer::_doFrameDowngradeDisposeBehindToCrop()
 * @see \GDImage\APng_Composer::_doFrameDowngradeDisposeBehindToTransparent()
 */
if( ! Config::hasAGifComposerOptimization() ) {
  Config::setAGifComposerOptimization( 0 ); // none
}

/**
 * Class used to create a GIF pack from an array of \GDImage\Image_Interface. 
 * 
 * Each frame will be converted to \GDImage\Image_AGif_Frame when necessary.
 * The frames may be manipulated (crop, color palette, transparent pixels) to 
 * fit expected results.
 * 
 * 
 * For convenience, keys in pack correspond to GIF specification Block name.
 * 
 * Each keys contain Block Data with the same name than GIF specification
 * with allways a "_raw" key to represent raw data of the block.
 *  
 * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt
 * 
 * 
 * If a frame is a true color resource, the Composer will NOT try to split
 * it into multiple frames to create a true color GIF. But if the frames are
 * already splitted, it will be able to generate a true color GIF.
 * If you want to create true color GIF from a single true color resource, 
 * splitted frames must be ready before to pass to the Composer.
 * 
 * It seems that the first frame of animated GIF always fit Logical Screen Descriptor.
 * We want to keep this behavior so the base width and height used by the composer
 * will be the width and height of the first frame.
 * 
 * Note: you may think that using an 2D array to store colors data for resource
 * will be faster, but it is not (dunno why).
 * 
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage agif
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\AGif_Composer_Interface
 */
class AGif_Composer implements AGif_Composer_Interface
{
  /**
   * Constant to activate all optimization.
   * 
   * @var integer
   * @const
   */
  const OPTIMIZE_ALL = \PHP_INT_MAX;
  
  /**
   * Constant to deactivate all optimization.
   * 
   * @var integer
   * @const
   */
  const OPTIMIZE_NO = 0;
  
  /**
   * Constant for crop optimization.
   * 
   * @var integer
   * @const
   */
  const OPTIMIZE_CROP = 3;
  
  /**
   * Constant for optimization, used as bitwse.
   * 
   * Represent a crop of transparent borders of the current frame.
   * 
   * @var integer
   * @const
   */
  const OPTIMIZE_TRANSPARENT_TO_CROP = 1;
  
  /**
   * Constant for optimization, used as bitwse.
   * 
   * Represent a crop of redundant pixels from disposed frame to the 
   * current frame.
   * 
   * @var integer
   * @const
   */
  const OPTIMIZE_DISPOSAL_TO_CROP = 2;
  
  /**
   * Constant for optimization, used as bitwse.
   * 
   * Represent a replacement with a transparent color on redundant pixels from 
   * disposed frame to the current frame.
   * 
   * @var integer
   * @const
   */
  const OPTIMIZE_DISPOSAL_TO_TRANSPARENT = 4;
  
  /**
   * Method to create a GIF pack from an array of \GDImage\Image_Interface.
   * 
   * 
   * For convenience, keys in pack correspond to GIF specification Block name.
   * 
   * Each keys contain Block Data with the same name than GIF specification
   * with allways a "_raw" key to represent raw data of the block.
   *  
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt
   * 
   * For the number of repeatition, 0 means infinite, -1 none.
   * 
   * 
   * @param array $frames Array of \GDImage\Image_Interface
   * @param [integer] $loops Number of repeatitions (NETSCAPE 2.0 specific)
   * @param [mixed] ... Extra arguments (not defined yet)
   * @return array GIF pack
   * @access public
   * @throws Exception_AGif
   * @static
   * @implements \GDImage\AGif_Composer_Interface
   */
  static public function compose( array $frames , $loops = 0 )
  {
    // create instance
    $composer = new static();
    // invoke it and return GIF pack
    $args = func_get_args();
    return call_user_func_array( $composer , $args );
  }
  
  /**
   * GIF pack
   * 
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt
   * @var array
   * @access protected
   */
  public $_gif_pack;
  
  /**
   * Array of frames
   * 
   * @var array Array of \GDImage\Image_AGif_Frame
   * @access protected
   */
  public $_frames;
  
  /**
   * Residual resource used for disposal.
   * It may be a \GDImage\Resource_TrueColor.
   * When using disposal 0, 1 or 3, we must get the original frame before 
   * any manipulation.
   * 
   * @var \GDImage\Resource_Abstract
   * @access protected
   */
  public $_disposal_rsc;
  
  /**
   * Resource used to store palette of Global Color Table.
   * If we have a global color table to use, the color index must match 
   * beetween each frame using it.
   * We use a 16x16 \GDImage\Resource_PaletteColor.
   * 
   * @var \GDImage\Resource_PaletteColor
   * @access protected
   */
  public $_globalcolortable_rsc;
  
  /**
   * Method to create a GIF pack from an array of \GDImage\Image_Interface.
   *
   * The pack creation result in three steps:
   * - at first, make sure all frame extends Image_AGif_Frame, redundant 
   *   with \GDImage\Image_AGif->_prepareFrame(), but not wrong;
   * - on second step downgrade the frame resource to eliminate 
   *   useless/redondant information and determine global/local table usage;
   * - third step is to create pack from these resource.
   * 
   * The first frame as a special meaning on Animated GIF, so it is special 
   * for the Composer.
   * 
   * @param array $frames Array of \GDImage\Image_Interface
   * @param [integer] $loops Number of repeatitions (NETSCAPE 2.0 specific)
   * @param [mixed] ... Extra arguments (not defined yet)
   * @return array GIF pack
   * @access public
   * @throws Exception_AGif
   */
  public function __invoke( array $frames , $loops = 0 )
  {
    // intro
    // reset pack
    $this->_gif_pack = array();
    // initialize frames
    $this->_frames = array();
    
    // step 1
    // create empty frame for comparison
    $base = AGif_Factory::frame();
    
    // look for frame conversion
    for( $i = 0, $imax = count( $frames ); $i < $imax; $i++ )
    {
      // clone it here
      // at this point, we do not care about forward frames
      if( $frames[$i] instanceof $base )
      {
        // just clone it
        $this->_frames[$i] = clone $frames[$i];
      }
      else
      {
        // clone base
        $this->_frames[$i] = clone $base;
        // keep in mind that fromImage() will NOT clone the frame
        // so we do it before
        $this->_frames[$i]->fromImage( clone $frames[$i] );
      }
    }
    
    // step 2
    // now we can downgrade the frames
    for( $i = 0, $imax = count( $this->_frames ); $i < $imax; $i++ )
    {
      $this->_doFrameDowngrade( $this->_frames[$i] , $i );
    }
    
    // step 3
    // then create pack for frame
    for( $i = 0, $imax = count( $this->_frames ); $i < $imax; $i++ )
    {
      $this->_doFramePack( $this->_frames[$i] , $i );
    }
    
    // pass extra arguments to the _doPack methods
    $args = func_get_args();
    // remove frames arguments
    array_shift( $args ); 
    // then call _doPack() method
    call_user_func_array( array( $this , '_doPack' ) , $args );
    
    // outro
    // free resources
    // $this->_frames = array(); // we want to keep this one for debug
    $this->_disposal_rsc = null;
    $this->_globalcolortable_rsc = null;
    
    // Terminated
    return $this->_gif_pack;
  }
  
  // FRAME DOWNGRADE
  
  /**
   * Method to do frame downgrades.
   * 
   * This method return true if something has been done on the frame, that does 
   * not mean that false is an error.
   *
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index
   * @return boolean True if something has been done, false otherwise
   * @access protected
   * @throws Exception_AGif
   */
  public function _doFrameDowngrade( Image_AGif_Frame $frame , $index )
  {
    $flag = false;
    
    // order is important!
    // downgrade Logical Screen Descriptor
    $flag = $this->_doFrameDowngradeLogicalScreen( $frame , $index ) || $flag;
    // downgrade Disposal Method
    $flag = $this->_doFrameDowngradeDisposalMethod( $frame , $index ) || $flag;
    // downgrade Image Descriptor
    $flag = $this->_doFrameDowngradeImageDescriptor( $frame , $index ) || $flag;
    // downgrade Global Color Table
    $flag = $this->_doFrameDowngradeGlobalColorTable( $frame , $index ) || $flag;
    
    // return flag
    return $flag;
  }
  
  /**
   * Downgrade the frame to not be out of Logical Screen Descriptor.
   * 
   * By convenience Logical Screen Width and Height correspond to Width and 
   * Height of the first frame.
   * 
   * Image Descriptor data for the corresponding frame will be updated in 
   * the GIF pack.
   * 
   * On first frame, Logical Screen Descriptor data will be updated in 
   * the GIF pack.
   *
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 18
   */
  public function _doFrameDowngradeLogicalScreen( Image_AGif_Frame $frame , $index )
  {    
    if( $index === 0 )
    {
      $w = $frame->getWidth();
      $h = $frame->getHeight();
      
      // update Logical Screen Descriptor
      // in fact these values will be ony used to eran precious µs on each other frames
      // they will be overwrote in self::_doFramePackLogicalScreen()
      $this->_gif_pack['Logical Screen Descriptor']['Logical Screen Width'] = $w;
      $this->_gif_pack['Logical Screen Descriptor']['Logical Screen Height'] = $h;
      
      // update Image Descriptor data for this frame
      $this->_gif_pack['Image Descriptor0']['Image Left Position'] = 0; 
      $this->_gif_pack['Image Descriptor0']['Image Top Position'] = 0; 
      $this->_gif_pack['Image Descriptor0']['Image Width'] = $w; 
      $this->_gif_pack['Image Descriptor0']['Image Height'] = $h;
      
      return false;
    }
    
    $sw = $frame->getWidth();
    $sh = $frame->getHeight();
    // use Logical Screen Width and Height
    $dw = $this->_gif_pack['Logical Screen Descriptor']['Logical Screen Width'];
    $dh = $this->_gif_pack['Logical Screen Descriptor']['Logical Screen Height'];
    
    // if smaller or equal, nothing to do
    if( $sw <= $dw && $sh <= $dh ) return false;
    
    // do not use min() or max() to compare two values, it sounds like using a tank to open a door
    if( $sw < $dw ) { $minw = $sw; $maxw = $dw; }
    else { $minw = $dw; $maxw = $sw; }
    if( $sh < $dh ) { $minw = $sh; $maxw = $dh; }
    else { $minh = $dh; $maxw = $sh; }
    
    // we have to crop the resource
    $dst = $frame->getResource()->blank( $minw , $minh );
    
    // if copy failed, return false
    if( ! imagecopy( $dst->getGdResource() , $frame->getResource()->getGdResource() , 0 , 0 , 0 , 0 , $minw , $minh ) ) return false;
    
    // propagate modes
    $dst->setModes( $frame->getResource()->getModes() );
    
    // set new resource
    $frame->setResource( $dst );
    
    // update Image Descriptor data for this frame
    $this->_gif_pack['Image Descriptor'.$index]['Image Left Position'] = 0; 
    $this->_gif_pack['Image Descriptor'.$index]['Image Top Position'] = 0; 
    $this->_gif_pack['Image Descriptor'.$index]['Image Width'] = $minw; 
    $this->_gif_pack['Image Descriptor'.$index]['Image Height'] = $minh;
    
    // I never sleep, cause sleep is the cousin of death
    return true;
  }
  
  /**
   * Adjust frame content from the PREVIOUS frame Disposal Method.
   * 
   * This method will assign disposal ressource when necessary
   * 
   * In fact, when a frame is true color, we want to save it before any palette 
   * conversion in order to stay able to compare pixels from the previous frame 
   * to the next frame.
   *
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  public function _doFrameDowngradeDisposalMethod( Image_AGif_Frame $frame , $index )
  {
    // now, depending of Disposal Method to apply, we may want to remove some pixels informations
    $method = '_doFrameDowngradeDisposalMethod'.$frame->getDisposalMethod();
    if( ! method_exists( $this , $method ) )
    {
      // Unknown Disposal Method
      throw new Exception_AGif( array( get_class( $this ) , $frame->getDisposalMethod() , $index ) , 6061 );
    }
    
    // we may not want disposal optimization
    // because it can be very slow
    if( ! ( Config::getAGifComposerOptimization() & ( self::OPTIMIZE_DISPOSAL_TO_CROP | self::OPTIMIZE_DISPOSAL_TO_TRANSPARENT ) ) ) return false;
    
    // apply Disposal Method
    return $this->$method( $frame , $index );
  }
  
  /**
   * Manipulate resource from Disposal Method 0 (unspecified).
   * 
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  public function _doFrameDowngradeDisposalMethod0( Image_AGif_Frame $frame , $index )
  {
    // it seems that disposal method to 0 has same meaning thatn disposal method to 1
    // @see http://the-labs.com/GIFMerge/#manipulation_offset_position
    return $this->_doFrameDowngradeDisposalMethod1( $frame , $index );
    
//    // nothing to do
//    return true;
  }
  
  /**
   * Manipulate resource from Disposal Method 1 (no).
   * 
   * We want to remove redondant pixel information from disposal resource
   * and current frame resource.
   * 
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  public function _doFrameDowngradeDisposalMethod1( Image_AGif_Frame $frame , $index )
  {
    // get a snapshot of current frame before downgrade
    // this snapshot will be used as next disposal resource
    $snapshot = clone $frame->getResource();
    
    $flag = $this->_doFrameDowngradeDisposalBehind( $frame , $index );
    
    // now assing snapshot
    $this->_disposal_rsc = $snapshot;
    
    return $flag;
  }
  
  /**
   * Manipulate resource from Disposal Method 2 (background).
   * 
   * For this Disposal Method, there is nothing to do: all pixels will be 
   * reset before to display the frame.
   * 
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index
   * @return null|Resource_Abstract
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  public function _doFrameDowngradeDisposalMethod2( Image_AGif_Frame $frame , $index )
  {
    // for this one, we want to reset disposal resource
    $this->_disposal_rsc = null;
    
    // nothing else to do
    return false;
  }
  
  /**
   * Manipulate resource from Disposal Method 3 (previous).
   * 
   * We want to remove redondant pixel information from disposal resource
   * and current frame resource.
   * 
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index
   * @return null|Resource_Abstract
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  public function _doFrameDowngradeDisposalMethod3( Image_AGif_Frame $frame , $index )
  {
    // for this one, the disposal resource will not change
    return $this->_doFrameDowngradeDisposalBehind( $frame , $index );
  }
  
  /**
   * Remove redundant pixel from disposal resource to a specific frame.
   * 
   * Used in _doFrameDowngradeDisposalMethod1() and _doFrameDowngradeDisposalMethod3().
   * 
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  public function _doFrameDowngradeDisposalBehind( Image_AGif_Frame $frame , $index )
  {
    // if there is no disposal resource, we cannot do anything
    if( ! $this->_disposal_rsc ) return false;
    
    $flag = $this->_doFrameDowngradeDisposalBehindToCrop( $frame , $index );
    
    return $this->_doFrameDowngradeDisposalBehindToTransparent( $frame , $index ) || $flag;
  }
    
  /**
   * Remove every redundant pixel from the border of the disposal 
   * resource to a specific frame, but not redundant pixel inside the frame area.
   * 
   * Note that for this optimization, we do not need any transparent color.
   * 
   * Used in _doFrameDowngradeDisposalBehind().
   * 
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  public function _doFrameDowngradeDisposalBehindToCrop( Image_AGif_Frame $frame , $index )
  {
    // we may not want this optimization
    if( ! ( Config::getAGifComposerOptimization() & self::OPTIMIZE_DISPOSAL_TO_CROP ) ) return false;
    
    // for this method, we do not need to care about transparency
    // we just crop the image, nothing else
    
    // shortcut
    $frame_rsc = $frame->getResource();
    
    // we must be sure that GD resource is awaken before to play with colors
    $frame_rsc->awakeGdResource();
    
    // note we care about previous offsets, even if it is not necessary
    
    // shortcut
    $x_off = 0;
    $y_off = 0;
    if( isset($this->_gif_pack['Image Descriptor'.$index]['Image Left Position']) ) $x_off = $this->_gif_pack['Image Descriptor'.$index]['Image Left Position'];
    if( isset($this->_gif_pack['Image Descriptor'.$index]['Image Top Position']) ) $y_off = $this->_gif_pack['Image Descriptor'.$index]['Image Top Position'];
    
    // width and height
    $xmax = $frame->getWidth();
    $ymax = $frame->getHeight();
    
    // initial values
    $x_lft = 0;
    $x_rgt = $xmax-1;
    $y_top = 0;
    $y_btm = $ymax-1;
    
    $flag = false;
    
    // look for right 
    $x = $xmax;
    while( $x-- ) // post decrement, from right to left, start at $xmax-1, stop at -1
    {
      $y = $ymax;
      while( $y-- ) // post decrement, from bottom to top, start at $ymax-1, stop at -1
      {
        // fetch frame color
        $c1 = $frame_rsc->getColorAt( $x , $y );
        
        // fully transparent? we can continue 
        if( $c1->getAlpha() === Color::ALPHA_TRANSPARENT ) continue;
        
        // fetch disposal color
        $c2 = $this->_disposal_rsc->getColorAt( $x + $x_off , $y + $y_off );
        
        // fully transparent? we have to stop
        if( $c1->getAlpha() === Color::ALPHA_TRANSPARENT )
        {
          $x_rgt = $x;
          
          // we can stop both loops
          break 2;
        }
        
        // if not, consider both color has opaque
        $c1->setAlpha( 0 );
        $c2->setAlpha( 0 );
        
        // compare object properties
        if( $c1 != $c2 )
        {
          $x_rgt = $x;
          
          // we can stop both loops
          break 2;
        }
      }
    }
    
    
    // a little check to look if all pixels are redudant
    if( $y < 0 && $x < 0 )
    {
      // in that case, reduce the frame to a 1x1 image
      
      // create blank resource
      $dst = $frame->getResource()->blank( 1 , 1 );
      
      // nothing to copy, the resource will be filled with transparent color

      // propagate modes
      $dst->setModes( $frame->getResource()->getModes() );

      // set new resource
      $frame->setResource( $dst );
      
      // update Image Descriptor data for this frame
      $this->_gif_pack['Image Descriptor'.$index]['Image Left Position'] = 0; 
      $this->_gif_pack['Image Descriptor'.$index]['Image Top Position'] = 0; 
      $this->_gif_pack['Image Descriptor'.$index]['Image Width'] = 1; 
      $this->_gif_pack['Image Descriptor'.$index]['Image Height'] = 1;

      // I just can't get enough
      return true;
    }
    
    
    // now, we can assign some default for other values
    $x_lft = $x; // same value than $x_rgt
    $y_top = $y;
    $y_bot = $y;
    
    // now proceed by spiraloid square
    
    // look for top 
    // we have updated $y_top default to its maximal, so we can stop at this value
    $y = -1;
    while( (++$y) < $y_top ) // pre increment, from top to bottom, start at 0, stop at $y_top
    {
      // at this point, we know what there is nothing to look above or equal to $x_rgt+1
      // we want to look from right to left, to determine next $x_lft maximal
      $x = $x_rgt+1;
      while( $x-- ) // pre decrement, from right to left, start at $x_rgt, stop at -1
      {
        // fetch frame color
        $c1 = $frame_rsc->getColorAt( $x , $y );
        
        // fully transparent? we can continue 
        if( $c1->getAlpha() === Color::ALPHA_TRANSPARENT ) continue;
        
        // fetch disposal color
        $c2 = $this->_disposal_rsc->getColorAt( $x + $x_off , $y + $y_off );
        
        // fully transparent? we have to stop
        if( $c1->getAlpha() === Color::ALPHA_TRANSPARENT )
        {
          $y_top = $y;
          
          // we can stop both loops
          break 2;
        }
        
        // if not, consider both color has opaque
        $c1->setAlpha( 0 );
        $c2->setAlpha( 0 );
        
        // compare object properties
        if( $c1 != $c2 )
        {
          $y_top = $y;
          
          // we can stop both loops
          break 2;
        }
      }
    }
    
    // at this point, left search may be reduced
    if( $x < $x_lft ) $x_lft = $x;

    // look for left 
    // we have updated $x_lft default value to its maximal, so we can stop at this value
    $x = -1;
    while( (++$x) < $x_lft ) // pre increment, from left to right, start at 0, stop at $x_lft
    {
      // we can be sure that we will not found anything below or equal to $y_top-1
      // we want to look from top to bottom, to determine next $y_bot minimal
      $y = $y_top-1;
      while( (++$y) < $ymax ) // post increment, from top to bottom, start at $y_top, stop at $ymax
      {
        // fetch frame color
        $c1 = $frame_rsc->getColorAt( $x , $y );
        
        // fully transparent? we can continue 
        if( $c1->getAlpha() === Color::ALPHA_TRANSPARENT ) continue;
        
        // fetch disposal color
        $c2 = $this->_disposal_rsc->getColorAt( $x + $x_off , $y + $y_off );
        
        // fully transparent? we have to stop
        if( $c1->getAlpha() === Color::ALPHA_TRANSPARENT )
        {
          $x_lft = $x;
          
          // we can stop both loops
          break 2;
        }
        
        // if not, consider both color has opaque
        $c1->setAlpha( 0 );
        $c2->setAlpha( 0 );
        
        // compare object properties
        if( $c1 != $c2 )
        {
          $x_lft = $x;
          
          // we can stop both loops
          break 2;
        }
      }
    }
    
    // at this point, bottom search may be reduced
    if( $y > $y_bot ) $y_bot = $y;
    
    // look for bottom 
    // we have updated $y_bot default value to its minimal, so we can stop at this value
    $y = $ymax;
    while( (--$y) > $y_bot ) // pre decrement, from bottom to top, start at $ymax-1, stop at $y_bot
    {
      // we can be sure that we will not found anything below or equal to $x_lft-1
      // we can be sure that we will not found anything above or equal to $x_rgt+1
      $x = $x_rgt+1;
      while( ($x--) > $x_lft ) // post decrement, from right to left, start at $x_rgt, stop at $x_lft-1
      {
        // fetch frame color
        $c1 = $frame_rsc->getColorAt( $x , $y );
        
        // fully transparent? we can continue 
        if( $c1->getAlpha() === Color::ALPHA_TRANSPARENT ) continue;
        
        // fetch disposal color
        $c2 = $this->_disposal_rsc->getColorAt( $x + $x_off , $y + $y_off );
        
        // fully transparent? we have to stop
        if( $c1->getAlpha() === Color::ALPHA_TRANSPARENT )
        {
          $y_btm = $y;
          
          // we can stop both loops
          break 2;
        }
        
        // if not, consider both color has opaque
        $c1->setAlpha( 0 );
        $c2->setAlpha( 0 );
        
        // compare object properties
        if( $c1 != $c2 )
        {
          $y_btm = $y;
          
          // we can stop both loops
          break 2;
        }
      }
    }
    
    
    // any crop?
    if( $x_lft === 0 && $x_rgt === $xmax - 1 && $y_top === 0 && $y_btm === $ymax - 1 ) return false; 
    
    // get width and height
    $w = $x_rgt - $x_lft + 1;
    $h = $y_btm - $y_top + 1;
    
    // crop resource
    $dst = $frame->getResource()->blank( $w , $h );
    
    // if copy failed, return false
    if( ! imagecopy( $dst->getGdResource() , $frame->getResource()->getGdResource() , 0 , 0 , $x_lft , $y_top , $w , $h ) ) return false;
    
    // propagate modes
    $dst->setModes( $frame->getResource()->getModes() );
    
    // set new resource
    $frame->setResource( $dst );
    
    // update Image Descriptor data for this frame
    $this->_gif_pack['Image Descriptor'.$index]['Image Left Position'] = $x_lft + $x_off; 
    $this->_gif_pack['Image Descriptor'.$index]['Image Top Position'] = $y_top + $x_off; 
    $this->_gif_pack['Image Descriptor'.$index]['Image Width'] = $w; 
    $this->_gif_pack['Image Descriptor'.$index]['Image Height'] = $h;   
    
    // Vois tu, tu me rends folle
    // Summer, summer, summer, summer love
    return true;
  }
  
  /**
   * Remove redundant pixel from disposal resource to a specific frame.
   * 
   * Used in _doFrameDowngradeDisposalMethod1() and _doFrameDowngradeDisposalMethod3().
   * 
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  public function _doFrameDowngradeDisposalBehindToTransparent( Image_AGif_Frame $frame , $index )
  {
    // we may not want this optimization
    if( ! ( Config::getAGifComposerOptimization() & self::OPTIMIZE_DISPOSAL_TO_TRANSPARENT ) ) return false;
    
    // we must be sure that GD resource is awaken before to play with colors
    $frame->getResource()->awakeGdResource();
    
    // no transparent, we cannot do anything
    // 
    // on true color, there is allways a transparent color
    // @see \GDImage\Resource_TrueColor
    // 
    // on palette color, if there is no transparent color, creating one will 
    // grow up palette size that may not be expected
    // 
    // also, on palette color, I do not think that the background color can be 
    // used in replacement
    if( ! ( $transparent = $frame->getResource()->getTransparent() ) ) return false;
    
    
    // now look on each pixels to replace it if it is the same than disposal resource
    // at this point, width and height of the frame has been cropped to fit in Logical Screen Descriptor size
    // @see _doFrameDowngradeLogicalScreen()
    
    // note we must care about previous offsets
    // @see _doFrameDowngradeDisposeBehindToCrop()
    
    // shortcut
    $x_off = 0;
    $y_off = 0;
    if( isset($this->_gif_pack['Image Descriptor'.$index]['Image Left Position']) ) $x_off = $this->_gif_pack['Image Descriptor'.$index]['Image Left Position'];
    if( isset($this->_gif_pack['Image Descriptor'.$index]['Image Top Position']) ) $y_off = $this->_gif_pack['Image Descriptor'.$index]['Image Top Position'];
    
    // shortcut
    $frame_rsc = $frame->getResource();
    
    // width and height
    $xmax = $frame->getWidth();
    $ymax = $frame->getHeight();
    
    $flag = false;
    
    $x = $xmax;
    while( $x-- )
    {
      $y = $ymax;
      while( $y-- )
      {
        // fetch frame color
        $c1 = $frame_rsc->getColorAt( $x , $y );
       
        // transparent? do nothing
        if( $c1->getAlpha() === Color::ALPHA_TRANSPARENT ) continue;
        
        // fetch disposal color
        $c2 = $this->_disposal_rsc->getColorAt( $x + $x_off , $y + $y_off );
        
        // transparent? do nothing
        if( $c2->getAlpha() === Color::ALPHA_TRANSPARENT ) continue;
        
        // not transparent, consider both as opaque
        $c1->setAlpha( 0 );
        $c2->setAlpha( 0 );
        
        // compare object properties
        if( $c1 == $c2 )
        {
          // they are equal, replace pixels in frame
          $frame_rsc->setColorAt( $transparent , $x , $y );
          
          // set flag to true
          $flag = true;
        }
      }
    }
    
    // La vie n'est qu'un jeu d'echecs
    // Et a la fin, le roi et le pion vont dans la meme boite
    return $flag;
  }
  
  /**
   * Downgrade the frame to remove borders that are fully
   * transparent or with background color.
   * 
   * At this point, redondant pixels have been moved out.
   * 
   * Image Descriptor data for the corresponding frame will be updated in 
   * the GIF pack.
   *
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 20
   */
  public function _doFrameDowngradeImageDescriptor( Image_AGif_Frame $frame , $index )
  {
    // we may not want this optimization
    if( ! ( Config::getAGifComposerOptimization() & self::OPTIMIZE_TRANSPARENT_TO_CROP ) ) return false;
    
    // on first frame, do nothing
    if( $index === 0 ) return false;
    
    // for this method, we do not need to care about transparency
    // we just crop the image, nothing else
    
    // keep in mind that the resource may be a true color resource,
    // so only compare alpha channel
    
    // shortcut
    $frame_rsc = $frame->getResource();
    
    // we must be sure that GD resource is awaken before to play with colors
    $frame_rsc->awakeGdResource();
    
    // width and height
    $xmax = $frame->getWidth();
    $ymax = $frame->getHeight();
    
    // initial values
    $x_lft = 0;
    $x_rgt = $xmax-1;
    $y_top = 0;
    $y_btm = $ymax-1;
    
    // look for right 
    $x = $xmax;
    while( $x-- ) // post decrement, from right to left, start at $xmax-1, stop at -1
    {
      $y = $ymax;
      while( $y-- ) // post decrement, from bottom to top, start at $ymax-1, stop at -1
      {
        if( $frame_rsc->getColorAt( $x , $y )->getAlpha() !== Color::ALPHA_TRANSPARENT )
        {
          $x_rgt = $x;
          
          // we can stop both loops
          break 2;
        }
      }
    }
    
    
    // a little check to look if all pixels are transparent
    if( $y < 0 && $x < 0 )
    {
      // in that case, reduce the frame to a 1x1 image
      
      // create blank resource
      $dst = $frame->getResource()->blank( 1 , 1 );
      
      // nothing to copy, the resource will be filled with transparent color

      // propagate modes
      $dst->setModes( $frame->getResource()->getModes() );

      // set new resource
      $frame->setResource( $dst );
      
      // update Image Descriptor data for this frame
      $this->_gif_pack['Image Descriptor'.$index]['Image Left Position'] = 0; 
      $this->_gif_pack['Image Descriptor'.$index]['Image Top Position'] = 0; 
      $this->_gif_pack['Image Descriptor'.$index]['Image Width'] = 1; 
      $this->_gif_pack['Image Descriptor'.$index]['Image Height'] = 1;

      // My head is a jungle, jungle
      return true;
    }
    
    
    // now, we can assign some default for other values
    $x_lft = $x; // same value than $x_rgt
    $y_top = $y;
    $y_bot = $y;
    
    // now proceed by spiraloid square
    
    // look for top 
    // we have updated $y_top default to its maximal, so we can stop at this value
    $y = -1;
    while( (++$y) < $y_top ) // pre increment, from top to bottom, start at 0, stop at $y_top
    {
      // at this point, we know what there is nothing to look above or equal to $x_rgt+1
      // we want to look from right to left, to determine next $x_lft maximal
      $x = $x_rgt+1;
      while( $x-- ) // pre decrement, from right to left, start at $x_rgt, stop at -1
      {
        if( $frame_rsc->getColorAt( $x , $y )->getAlpha() !== Color::ALPHA_TRANSPARENT )
        {
          $y_top = $y;
          
          // we can stop both loops
          break 2;
        }
      }
    }
    
    // at this point, left search may be reduced
    if( $x < $x_lft ) $x_lft = $x;

    // look for left 
    // we have updated $x_lft default value to its maximal, so we can stop at this value
    $x = -1;
    while( (++$x) < $x_lft ) // pre increment, from left to right, start at 0, stop at $x_lft
    {
      // we can be sure that we will not found anything below or equal to $y_top-1
      // we want to look from top to bottom, to determine next $y_bot minimal
      $y = $y_top-1;
      while( (++$y) < $ymax ) // post increment, from top to bottom, start at $y_top, stop at $ymax
      {
        if( $frame_rsc->getColorAt( $x , $y )->getAlpha() !== Color::ALPHA_TRANSPARENT )
        {
          $x_lft = $x;
          
          // we can stop both loops
          break 2;
        }
      }
    }
    
    // at this point, bottom search may be reduced
    if( $y > $y_bot ) $y_bot = $y;
    
    // look for bottom 
    // we have updated $y_bot default value to its minimal, so we can stop at this value
    $y = $ymax;
    while( (--$y) > $y_bot ) // pre decrement, from bottom to top, start at $ymax-1, stop at $y_bot
    {
      // we can be sure that we will not found anything below or equal to $x_lft-1
      // we can be sure that we will not found anything above or equal to $x_rgt+1
      $x = $x_rgt+1;
      while( ($x--) > $x_lft ) // post decrement, from right to left, start at $x_rgt, stop at $x_lft-1
      {
        if( $frame_rsc->getColorAt( $x , $y )->getAlpha() !== Color::ALPHA_TRANSPARENT )
        {
          $y_btm = $y;
          
          // we can stop both loops
          break 2;
        }
      }
    }
    
    
    // any crop?
    if( $x_lft === 0 && $x_rgt === $xmax - 1 && $y_top === 0 && $y_btm === $ymax - 1 ) return false; 
    
    // get width and height
    $w = $x_rgt - $x_lft + 1;
    $h = $y_btm - $y_top + 1;
    
    // crop resource
    $dst = $frame->getResource()->blank( $w , $h );
    
    // if copy failed, return false
    if( ! imagecopy( $dst->getGdResource() , $frame->getResource()->getGdResource() , 0 , 0 , $x_lft , $y_top , $w , $h ) ) return false;
    
    // propagate modes
    $dst->setModes( $frame->getResource()->getModes() );
    
    // set new resource
    $frame->setResource( $dst );
    
    // update Image Descriptor data for this frame
    $this->_gif_pack['Image Descriptor'.$index]['Image Width'] = $w; 
    $this->_gif_pack['Image Descriptor'.$index]['Image Height'] = $h;
    
    // for offsets we must care about previous assignments 
    // @see _doFrameDowngradeDisposeBehindToCrop()
    if( ! isset($this->_gif_pack['Image Descriptor'.$index]['Image Left Position']) ) $this->_gif_pack['Image Descriptor'.$index]['Image Left Position'] = 0;
    if( ! isset($this->_gif_pack['Image Descriptor'.$index]['Image Top Position']) ) $this->_gif_pack['Image Descriptor'.$index]['Image Top Position'] = 0;
    $this->_gif_pack['Image Descriptor'.$index]['Image Left Position'] += $x_lft; 
    $this->_gif_pack['Image Descriptor'.$index]['Image Top Position'] += $y_top; 
    
    // We're beautiful like diamonds in the sky
    return true;
  }
  
  /**
   * Downgrade the frame to look if we can use Global Color table.
   * 
   * At this point, the frame has been reduced to a minimal size.
   * 
   * The first frame will allways be considered as using Global Color Table.
   * 
   * Global Color Table data will be updated in the GIF pack.
   * 
   * Do not forgot that GD exclude not used colors on imagegif() import, so there 
   * is no need to check if a color is present more than once a time, except 
   * for the transparent color...
   * 
   * For transparent color it is a little bit more complex.
   * The RGB data of a transparent color may be present twice in the same table
   * once for a real color, once for a transparent one
   * So the RGB data of the transparent color of a frame may be foundable
   * in the Global Color Table but we may have to add the RGB data twice if 
   * the frame use same RGB data for a real color.
   * This distinction will be difficult to implement and will probably result to 
   * a loose of performance.
   * To simplify that, we identify each transparent color as a combination of
   * RGB data + transparent alpha channel (127). Real colors will be identified 
   * by a combination of RGB data + opaque alpha channel (0).
   * 
   * Image Descriptor data for the corresponding frame 
   * will be updated in the GIF pack.
   * 
   * Global Color Table will be populate here but reset in method 
   * _doFramePackGlobalColorTable().
   *
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 19
   */
  public function _doFrameDowngradeGlobalColorTable( Image_AGif_Frame $frame , $index )
  {
    // at this point, the frame resource must be converted to palette color
    if( $frame->getResource()->isTrueColor() )
    {
      $frame->setResource( $frame->getResource()->toPaletteColor( 256 , false ) );
    }
    
    if( $index === 0 )
    {
      // create global color table resource
      // 16x16 will give us 256 pixels, that is exactly the expected limit
      // dunno if it is necessary but it is not wrong...
      $this->_globalcolortable_rsc = Resource_PaletteColor::createFromSize( 16 , 16 );
    
      // we must be sure that GD resource is awaken before to play with colors
      $frame->getResource()->awakeGdResource();
      
      // keep in mind transparent color
      $transparent = $frame->getResource()->getTransparent();
      
      // do it in the same order the the palette
      // on palette color the first allocated color will be background color
      // @see http://php.net/manual/en/function.imagecolorallocate.php
      for( $i = 0, $imax = $frame->getResource()->countColors(); $i < $imax; $i++ )
      {
        $c = $frame->getResource()->getColor( $i );
        
        // if the color is not the transparent one, undo alpha
        // not strict match to compare only properties
        if( $c != $transparent ) $c->setAlpha( 0 );
        
        // assign color to global table resource
        // this resource will be used to copy the palette to other frames
        $this->_globalcolortable_rsc->setColor( $c , true );
      }
        
      // update Image Descriptor to Global Color Table
      $this->_gif_pack['Image Descriptor0']['Packed']['Local Color Table Flag'] = 0;
      
      
      return true;
    }
    
    // other frames
    
    // we must be sure that GD resource is awaken before to play with colors
    $frame->getResource()->awakeGdResource();
    
    // keep in mind transparent color
    $transparent = $frame->getResource()->getTransparent();
      
    // on other frame, we want to compare colors to see if additionnal colors 
    // can fit in the Global Color Table
    
    $i = $frame->getResource()->countColors();
    // colors to add to global color table
    $to_add = array();
    // counter
    $cnt = $this->_globalcolortable_rsc->countColors();
    
    while( $i-- )
    {
      $c = $frame->getResource()->getColor( $i );
        
      // if the color is not the transparent one, undo alpha
      // not strict match to compare only properties
      if( $c != $transparent ) $c->setAlpha( 0 );

      // look if color is in palette with exact match
      if( $this->_globalcolortable_rsc->findColor( $c , true )  )
      {
        // yes it is!
        continue;
      }
      
      if( $cnt < 256 )
      {
        // this color may be added to global table
        // but we do not want to add it now
        $to_add[] = $c;
        // update counter
        $cnt++;
        // continue
        continue;
      }
      
      // if we come here, we are not able to populate Global Color Table
      // fortunately, on AGIF, each frame can have a local color table
      
      // update Image Descriptor to Local Color Table
      $this->_gif_pack['Image Descriptor'.$index]['Packed']['Local Color Table Flag'] = 1;
      
      // nothing to change
      return false;
    }
    
    // update global color table with new colors
    // we do not care about order of appearence
    $i = count( $to_add );
    while( $i-- )
    {
      // assign color to global table resource
      $this->_globalcolortable_rsc->setColor( $to_add[$i] , true );
    }

    // now that Global Color Table is completed, we must be sure that the 
    // current resource use correct color indexes
    // this technic will optimize our chance to do Global Color Table match 
    // in _doFramePackGlobalColorTable()
    
    // it is quicker to do it on an innocent resource than with $this->_globalcolortable_rsc->blank()
    $dst = Resource_PaletteColor::createFromSize( $frame->getWidth() , $frame->getHeight() );
   
    // copy Global Color palette
    imagepalettecopy( $dst->getGdResource() , $this->_globalcolortable_rsc->getGdResource() );
    
    // if there was a transparent color, restore it and fill picture with
    if( $transparent )
    {
      // keep in mind that alpha blending does not work on palette color
      // so we must fill the entire picture with transparent color to overwrote background color
      imagefill( $dst->getGdResource() , 0 , 0 , $dst->setTransparent( $transparent ) );
    }
    
    // then paste the current frame to this new picture
    
    // if it fails, we will have a big issue...
    if( ! imagecopy( $dst->getGdResource() , $frame->getResource()->getGdResource() , 0 , 0 , 0 , 0 , $frame->getWidth() , $frame->getHeight() ) )
    {
      // oups...

      // update Image Descriptor to Local Color Table
      $this->_gif_pack['Image Descriptor'.$index]['Packed']['Local Color Table Flag'] = 1;

      return false;
    }

    // update Image Descriptor to Global Color Table
    $this->_gif_pack['Image Descriptor'.$index]['Packed']['Local Color Table Flag'] = 0;

    // assign new resource
    $frame->setResource( $dst );

    // C'est la monnaie qui dirige le monde, c'est la monnaie qui dirige la Terre
    // Et qu'on le veuille ou non, c'est comme ca, on ne peut rien y faire
    return true;
  }
  
  // FRAME PACK
  
  /**
   * Populate GIF pack data from a specific frame.
   * 
   * In this method, we need to care about previous data on Image Descriptor,
   * Graphic Control Extension and so on.
   *
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   */
  public function _doFramePack( Image_AGif_Frame $frame , $index )
  {    
    // create binary for the frame and unpack it
    $binary = $frame->toBinary();
    $pack = AGif_Factory::unpack( $binary );
    
    $flag = false;
    $flag = $this->_doFramePackLogicalScreen( $pack , $index ) || $flag;
    $flag = $this->_doFramePackGlobalColorTable( $pack , $index ) || $flag;
    $flag = $this->_doFramePackImageDescriptor( $pack , $index ) || $flag;
    $flag = $this->_doFramePackLocalColorTable( $pack , $index ) || $flag;
    $flag = $this->_doFramePackTableBasedImageData( $pack , $index ) || $flag;
    $flag = $this->_doFramePackGraphicControlExtension( $pack , $index ) || $flag;
    
    return $flag;
  }
  
  /**
   * Populate GIF pack with Logical Screen Descriptor data.
   *
   * @param array $frame_pack Current frame GIF pack
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 18
   */
  public function _doFramePackLogicalScreen( array $frame_pack , $index )
  {    
    if( $index === 0 )
    {
      // for the first frame, we can keep Logical Screen Descriptor AS IS
      // no matter if we overwrote previous stuff
      $this->_gif_pack['Logical Screen Descriptor'] = $frame_pack['Logical Screen Descriptor'];
      // remove "_raw" and "_binary" stuff, they may be altered
      unset( $this->_gif_pack['Logical Screen Descriptor']['_raw'] );
      unset( $this->_gif_pack['Logical Screen Descriptor']['Packed']['_binary'] );
      
      return true;
    }
    
    return false;
  }
  
  /**
   * Populate GIF pack with Global Color Table data.
   * 
   * Unfortunately, on export, GD will remove not used colors, so a if the next
   * frame has less colors than the previous one, the Global Color Table will 
   * change drastically. 
   * 
   * So if we cannot merge the two Global Color Table, we use a Local Color Table.
   * 
   *
   * @param array $frame_pack Current frame GIF pack
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 19
   */
  public function _doFramePackGlobalColorTable( array $frame_pack , $index )
  {    
    if( $index === 0 )
    {
      // keep in mind that first frame allways use Global Color Table by default
      // and that the default Logical Screen Descriptor is the same than the first frame
      
      // but we want to look if it is not the only one with the Global Color Table
      // @see self::_doFrameDowngradeGlobalColorTable()
      
      $i = count( $this->_frames );
      while( $i-- )
      {
        // this data has been previously populated
        // @see self::_doFrameDowngradeGlobalColorTable()
        if( $this->_gif_pack['Image Descriptor'.$i]['Packed']['Local Color Table Flag'] === 0 )
        {
          // stop now
          break;
        }
      }
      
      // now look if i is greater than 0
      if( $i > 0 )
      {
        // if true, another frame using global color table has been found
        $this->_gif_pack['Global Color Table'] = $frame_pack['Global Color Table'];
        // keep "_raw" declaration for this one
        
        // nothing else to do, the default Logical Screen Descriptor is the same 
        // than the first frame GIF pack
        
        return true;
      }
      
      // if not, it means that it has just found the first frame as using Global Color Table
      // so just make this frame using Local Color Table
      $this->_gif_pack['Image Descriptor0']['Packed']['Local Color Table Flag'] = 1;
      
      // we also have to update some Logical Screen Descriptor data
      $this->_gif_pack['Logical Screen Descriptor']['Background Color Index'] = 0;
      $this->_gif_pack['Logical Screen Descriptor']['Packed']['Global Color Table Flag'] = 0;
      $this->_gif_pack['Logical Screen Descriptor']['Packed']['Size of Global Color Table'] = 0;
      
      // also remove Global Color Table from pack
      unset( $this->_gif_pack['Global Color Table'] );
      
      return false;
    }
    
    // on other frames, if this data is set to 0, we may be able to copy 
    // Global Color Table to the GIF pack
    // @see self::_doFrameDowngradeGlobalColorTable()
    if( $this->_gif_pack['Image Descriptor'.$index]['Packed']['Local Color Table Flag'] === 0 )
    {
      // unfortunately, even with the same color palette, the frame can get less color
      // than previous one, this will fuck up the global color table
      // I did not found any solution for that now
      
      // we can use a smart match to maximize Global Color Table usage
      // GD will fill Global Table Size with a black color "\x00"
      // we may have minor case where a used black color is at the end of 
      // the Global Color Table... need sample
      
      // so we can reduce both table with that
      $frame_gct = preg_replace( '~(\\x00\\x00\\x00)+$~' , '' , $frame_pack['Global Color Table']['_raw'] );
      $pack_gct = preg_replace( '~(\\x00\\x00\\x00)+$~' , '' , $this->_gif_pack['Global Color Table']['_raw'] );
      
//      // so we can reduce both table with that
//      $global_frame = rtrim( $frame_pack['Global Color Table'] , "\0" );
//      $global_pack = rtrim( $this->_gif_pack['Global Color Table'] , "\0" );
//      
//      // we may have remove green or blue component with "\0" value
//      if( ( $m = strlen( $global_frame ) % 3 ) !== 0 ) $global_frame .= str_repeat( "\0" , $m );
//      if( ( $m = strlen( $global_pack ) % 3 ) !== 0 ) $global_pack .= str_repeat( "\0" , $m );
      
//      AGif_Factory::printGifPack( array( $global_frame , $global_pack ) );
      
      // we want the biggest Global Color Table
      if( strlen( $frame_gct ) > strlen( $pack_gct ) )
      {
        // if the Global Color Table in the pack is the present at the
        // begining of the Global Color Table of the frame, we can use it
        if( strpos( $frame_gct , $pack_gct ) !== 0 ) 
        {
          // cannot use Global Color Table for the frame, color palette will be corrupted
          $this->_gif_pack['Image Descriptor'.$index]['Packed']['Local Color Table Flag'] = 1;
          return false;
        }
        
        // the Global Color Table of the frame is usable and bigger
        // update Global Color Table
        
        // by this way, we will be able to keep the Global Color Table that
        // has the maximum number of colors
        $this->_gif_pack['Global Color Table'] = $frame_pack['Global Color Table'];
        
        // we also have to update some Logical Screen Descriptor data
        // but only this one
        $this->_gif_pack['Logical Screen Descriptor']['Packed']['Size of Global Color Table'] = $frame_pack['Logical Screen Descriptor']['Packed']['Size of Global Color Table'];
      
        return true;
      }
      
      // if the Global Color Table of the frame is the present at the
      // begining of the Global Color Table in the pack, we can use it
      if( strpos( $pack_gct , $frame_gct ) !== 0 ) 
      {
        // cannot use Global Color Table for the frame, color palette will be corrupted
        $this->_gif_pack['Image Descriptor'.$index]['Packed']['Local Color Table Flag'] = 1;
        return false;
      }
      
      // the Global Color Table of the frame is usable but smaller
      // DO NOT update Global Color Table
      return true; // yes, nothing has changed but we will use Global Color Table
    }
    
    return false;
  }
  
  /**
   * Populate GIF pack with Image Descriptor data.
   * 
   * For this one, we must care about previous data.
   * @see self::_doFrameDowngradeLogicalScreen()
   * @see self::_doFrameDowngradeImageDescriptor()
   *
   * @param array $frame_pack Current frame GIF pack
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 20
   */
  public function _doFramePackImageDescriptor( array $frame_pack , $index )
  {    
    // do not overwtore previous data
    
    // create shortcuts
    if( ! isset($this->_gif_pack['Image Descriptor'.$index]) ) $this->_gif_pack['Image Descriptor'.$index] = array();
    $gif_subpack = &$this->_gif_pack['Image Descriptor'.$index]; // do it with reference
    $frame_subpack = $frame_pack['Image Descriptor0']; // reference is useless for this one
    
    if( ! isset($gif_subpack['Image Left Position']) ) $gif_subpack['Image Left Position'] = $frame_subpack['Image Left Position'];
    if( ! isset($gif_subpack['Image Top Position']) ) $gif_subpack['Image Top Position'] = $frame_subpack['Image Top Position'];
    if( ! isset($gif_subpack['Image Width']) ) $gif_subpack['Image Width'] = $frame_subpack['Image Width'];
    if( ! isset($gif_subpack['Image Height']) ) $gif_subpack['Image Height'] = $frame_subpack['Image Height'];
    
    if( ! isset($gif_subpack['Packed']['Local Color Table Flag']) ) $gif_subpack['Packed']['Local Color Table Flag'] = $frame_subpack['Packed']['Local Color Table Flag'];
    if( ! isset($gif_subpack['Packed']['Interlace Flag']) ) $gif_subpack['Packed']['Interlace Flag'] = $frame_subpack['Packed']['Interlace Flag'];
    if( ! isset($gif_subpack['Packed']['Sort Flag']) ) $gif_subpack['Packed']['Sort Flag'] = $frame_subpack['Packed']['Sort Flag'];
    if( ! isset($gif_subpack['Packed']['Reserved']) ) $gif_subpack['Packed']['Reserved'] = $frame_subpack['Packed']['Reserved'];
    if( ! isset($gif_subpack['Packed']['Size of Local Color Table']) ) $gif_subpack['Packed']['Size of Local Color Table'] = $frame_subpack['Packed']['Size of Local Color Table'];
      
    return true;
  }
  
  /**
   * Populate GIF pack with Table Based Image Data data.
   *
   * @param array $frame_pack Current frame GIF pack
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 20
   */
  public function _doFramePackTableBasedImageData( array $frame_pack , $index )
  {    
    // nothing special, keep it AS IS
    $this->_gif_pack['Table Based Image Data'.$index] = $frame_pack['Table Based Image Data0'];
    return true;
  }
  
  /**
   * Populate GIF pack with Local Color Table data.
   * 
   * Image Descriptor has been previously filled, but we may want to move Global Color Table
   * to Local Color Table
   *
   * @param array $frame_pack Current frame GIF pack
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 21
   */
  public function _doFramePackLocalColorTable( array $frame_pack , $index )
  {   
    // look for local or global table
    // keep in mind that this flag may have been altered
    // @see self::_doFramePackGlobalColorTable()
    if( $this->_gif_pack['Image Descriptor'.$index]['Packed']['Local Color Table Flag'] === 1 )
    {
      // we must move the Global Color Table of this one to a Local Color Table
      
      // fortunately, GD seems to preserve the color palette on export
      // even when not all colors are used on the export
      // that's exaclty what we want
      $this->_gif_pack['Local Color Table'.$index] = $frame_pack['Global Color Table'];
      
      // we also have to update some Image Descriptor data
      $this->_gif_pack['Image Descriptor'.$index]['Packed']['Size of Local Color Table'] = $frame_pack['Logical Screen Descriptor']['Packed']['Size of Global Color Table'];
      
      return true;
    }
    
    // nothing to do 
    // by default, GD use Global Color Table for every export
    // @see self::_doFramePackGlobalColorTable()
    return false;
  }
  
  /**
   * Populate GIF pack with Graphic Control Extension data.
   * 
   * No matter if data where previously saved in this one, we will overwrote them.
   *
   * @param array $frame_pack Current frame GIF pack
   * @param integer $index Frame index
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 21
   */
  public function _doFramePackGraphicControlExtension( array $frame_pack , $index )
  {
    // the Graphic Control Extension can be preserved but it may not be set
    if( isset($frame_pack['Graphic Control Extension0']) )
    {
      $this->_gif_pack['Graphic Control Extension'.$index] = $frame_pack['Graphic Control Extension0'];
      // remove "_raw" and "_binary" stuff
      unset( $this->_gif_pack['Graphic Control Extension'.$index]['_raw'] );
      unset( $this->_gif_pack['Graphic Control Extension'.$index]['Packed']['_binary'] );
    }
    else
    {
      // create a blank "Graphic Control Extension"
      $this->_gif_pack['Graphic Control Extension'.$index] = array(
        'Packed' => array(
          'Reserved' => 0 ,
          'Disposal Method' => 0 ,
          'User Input Flag' => 0 ,
          'Transparent Color Flag' => 0 ,
        ) ,
        'Delay Time' => 0 ,
        'Transparent Color Index' => 0 ,
      );
    }
      
    // some of these data must be overwrote
    
    // shortcut to frame
    $frame = $this->_frames[$index];
    
    $this->_gif_pack['Graphic Control Extension'.$index]['Delay Time'] = $frame->getDelayTime();
    $this->_gif_pack['Graphic Control Extension'.$index]['Packed']['Disposal Method'] = $frame->getDisposalMethod();
      
    return true;
  }
  
  // GLOBAL PACK
  
  /**
   * Method to add extra pack information.
   * For now, it do Header data and treat number of repeatitions (NETSCAPE 2.0) specific.
   * 
   *
   * @param integer $loops Number of repeatitions
   * @param [mixed] ... Extra arguments
   * @return boolean Success
   * @access protected
   * @throws Exception_AGif
   */
  public function _doPack( $loops = 0 )
  {
    $flag = false;
    
    $flag = $this->_doPackHeader() || $flag;
    $flag = $this->_doPackApplicationExtensionNetscape20( $loops ) || $flag;
    
    // some other shits may come here
    
    $flag = $this->_doPackTrailer() || $flag;
    return $flag;
  }
  
  /**
   * Populate GIF pack with Header data.
   *
   * @param none
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 17
   */
  public function _doPackHeader()
  {  
    // create Header
    $this->_gif_pack['Header'] = array( 'Signature' => 'GIF' , 'Version' => '89a' );
      
    return true;
  }
  
  /**
   * Method to add number of repeatitions (NETSCAPE 2.0) specific.
   *
   * @param integer $loops Number of repeatitions
   * @param integer $index Index of the application extension
   * @return boolean Success
   * @access protected
   * @see http://en.wikipedia.org/wiki/GIF#Animated_GIF
   */
  public function _doPackApplicationExtensionNetscape20( $loops = 0 , $index = 0 )
  {
    // note that without this Application Extension, the GIF just loop once
    if( $loops >= 0 )
    {
      // make it integer
      $loops |= 0;
      
      // add a special NETSCAPE 2.0 Application Extension
      // @see http://en.wikipedia.org/wiki/Graphics_Interchange_Format#Animated_GIF
      $this->_gif_pack['Application Extension'.$index] = array(
        'Application Identifier' => 'NETSCAPE' ,
        'Application Authentication Code' => '2.0' ,
        'Data' => pack( 'Cv' , 1 , $loops ) ,
        'Sub-Block Index' => '1' ,
        'Number of Repetitions' => $loops ,
      );
      
      return true;
    }
    
    return false;
  }
  
  /**
   * Populate GIF pack with Trailer data.
   *
   * @param none
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 27
   */
  public function _doPackTrailer()
  {  
    // create Trailer
    $this->_gif_pack['Trailer'] = array();
      
    return true;
  }
}
