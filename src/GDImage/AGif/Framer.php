<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration settings:
 * - "a_gif_frame_interface": \GDImage\Image_Interface to use as frame for animated GIF
 */
if( ! Config::hasAGifFrameInterface() ) {
  Config::setAGifFrameInterface( '\\GDImage\\Image_AGif_Frame' );
}

/**
 * Class to cerate frame instance used over all AGif classes.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage agif
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\AGif_Unpacker_Interface
 */
class AGif_Framer implements AGif_Framer_Interface
{
  
  /**
   * Function used to create a blank frame instance.
   * 
   * @param none
   * @return \GDImage\Image_AGif_Frame
   * @access public
   * @throws \GDImage\Exception_AGif
   * @static
   */
  static public function frame()
  {
    // nothing fantastic
    $class = Config::getAGifFrameInterface();
    return new $class();
  }
}
