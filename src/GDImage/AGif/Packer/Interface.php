<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Interface for GIF packer.
 * 
 * Packer are used to create binary data from a GIF pack.
 * For convenience, keys correspond to GIF specification Block name.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage agif
 * @author     Loops <pierrotevrard@gmail.com>
 * @interface
 */
interface AGif_Packer_Interface
{
  /**
   * Function used to create binary data from a GIF pack.
   * 
   * For convenience, keys correspond to GIF specification Block name.
   * 
   * Each keys contain Block Data with the same name than GIF specification
   * with allways a "_raw" key to represent raw data of the block.
   * 
   * Blocks that can be present more than one time are suffixed by an incremental
   * number used to identify the Block occurence (ie. "Image Descriptor3" for the 
   * fourth "Image Descriptor" Block found).
   * 
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt
   * 
   * @param array GIF pack
   * @return string Binary data
   * @access public
   * @throws \GDImage\Exception_AGif
   * @static
   */
  static public function pack( array $pack );
}
