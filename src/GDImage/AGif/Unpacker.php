<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Class to unpack animated GIF.
 * 
 * An important thing about AGIF delay time is that it can be 0, and the GIF
 * still animated: this is due to browser minimum frame delay and most of the 
 * time, this 0 delay is rounded up to 100ms.
 * @see http://nullsleep.tumblr.com/post/16524517190/animated-gif-minimum-frame-delay-browser-compatibility
 * 
 * It is not clear in the specifications, but if there is multiple Image 
 * Descriptor, a Graphic Control Extension will always preceded each Image 
 * Descriptor.
 * 
 * An important thing about AGIF frame, is that, from the disposal method, 
 * the previous frame is visible behind transparent color of the current one. 
 * Most of loader use this ability to reduce the file size to minimum.
 * 
 * In our case, we want the frame at its visible state, with the previous frame 
 * behind if necessary. The ability to reduce filesize belong to the "AGif_Packer"
 * used to build the AGIF binary.
 *
 * Also, true color GIF use this ablity, combined with Local Color Table. None 
 * of the frames overlap, so you do not see any animation.
 * @see http://phil.ipal.org/tc.html
 * 
 * That's why if the AGIF contains frames with Local Color Table, they have to 
 * be converted in true color resource to keep the previous frame visible. We 
 * do not have any other choices.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage agif
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\AGif_Unpacker_Interface
 */
class AGif_Unpacker implements AGif_Unpacker_Interface
{
  
  /**
   * 
   * For convenience, keys correspond to GIF specification Block name.
   * 
   * Each keys contain Block Data with the same name than GIF specification
   * with allways a "_raw" key to represent raw data of the block.
   * 
   * Blocks that can be present more than one time are suffixed by an incremental
   * number used to identify the Block occurence (ie. "Image Descriptor3" for the 
   * fourth "Image Descriptor" Block found).
   * 
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt
   * 
   * @param string $filepath_or_binary Filepath or binary data
   * @return array GIF pack
   * @access public
   * @throws \GDImage\Exception_AGif
   * @static
   * @implements \GDImage\AGif_Unpacker_Interface
   */
  static public function unpack( $filepath_or_binary )
  {
    // create unpacker instance
    $unpacker = new static();
    // return GIF data
    return $unpacker( $filepath_or_binary );
  }
  
  /**
   * File handle
   * 
   * Note that this handle is allways read forward, never backward.
   * 
   * @var resource
   * @access protected
   */
  public $_handle;
  
  /**
   * Local data storage, also known as GIF pack
   * 
   * @var array
   * @access protected
   */
  public $_gif_pack;
  
  /**
   * In order to keep order of appearence, we need to have counter for each
   * Data Block found in content
   * 
   * @var array
   * @access protected
   */
  public $_counters;

  /**
   * Function used to unpack GIF file or data to a big array of data.
   * 
   * For convenience, keys correspond to GIF specification Block name.
   * 
   * Each keys contain Block Data with the same name than GIF specification
   * with allways a "_raw" key to represent raw data of the block.
   * 
   * Blocks that can be present more than one time are suffixed by an incremental
   * number used to identify the Block occurence (ie. "Image Descriptor3" for the 
   * fourth "Image Descriptor" Block found).
   * 
   * Most of library you can found arround the web are just piece of crap for
   * one or more of these reasons: 
   * - the code is not commented and there is no way to understand it
   * - the code contains publicMethodWithVeryLongName() to do a simple fgetc()
   * - the code does not use unpack(), that is probably the best function to 
   *   read GIF data
   * 
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt
   * 
   * @param string $filepath_or_binary Filepath or binary data
   * @return array GIF pack
   * @access public
   * @throws \GDImage\Exception_AGif
   */
  public function __invoke( $filepath_or_binary )
  {
    // reset data
    $this->_gif_pack = array();

    // we want to keep order of appearance, so reset counters
    $this->_counters = array(
      // these four counters are related
      'Image Descriptor' => -1 ,
      'Local Color Table' => -1 ,
      'Table Based Image Data' => -1 ,
      'Graphic Control Extension' => -1 ,
      // these three counters are not related
      'Comment Extension' => -1 ,
      'Plain Text Extension' => -1 ,
      'Application Extension' => -1 ,
    );
    
    // open file handle
    
    // is it a readable file?
    if( ! $this->_handle = @ fopen( $filepath_or_binary , 'rb' ) )
    {
      // if it is not a file, write it to temporary file
      $this->_handle = tmpfile();
      fwrite( $this->_handle , $filepath_or_binary );
      fseek( $this->_handle , 0 );
      // now we can read it
    }
    
    // try/catch to close handle when fails
    try
    {
      // note that the handle is allways read forward, never backward
      
      // these three block allways comes at first, in this order
      $this->_unpackHeader();
      $this->_unpackLogicalScreenDescriptor();
      $this->_unpackGlobalColorTable();
      
      // then image data or extension may come
      
      // the next byte will determine a lot of things
      // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt Appendix A
      //
      // - "\x21": Extension Introducer
      // - "\x2C": Image Descriptor Separator
      // - "\x3B": Trailer
      // 
      // for extension a second byte will determine the type of extension:
      // - "\xF9": Graphic Control Extension
      // - "\xFE": Comment Extension
      // - "\x01": Plain Text Extension
      // - "\xFF": Application Extension

      do
      {
        // read byte to determine image data or simple data
        $buffer = fgetc( $this->_handle );

        // Image Separator
        // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 20
        if( $buffer === "\x2C" )
        {
          // unpack image description
          $this->_unpackImageDescriptor();
          // then a local color table may follow
          $this->_unpackLocalColorTable();
          // and they should allways be some table base data
          $this->_unpackTableBasedImageData();
        }
        // Extension Introducer
        elseif( $buffer === "\x21" )
        {
          // the next byte will help us to determine which Extension is
          $buffer = fgetc( $this->_handle );

          // Graphic Control Label
          // this one should be associated with the folowing image
          // but we can consider that if one image has a Graphic Control Extension
          // all images will have a Graphic Control Extension
          if( $buffer === "\xF9" )
          {
            $this->_unpackGraphicControlExtension();
          }
          // Comment Label
          elseif( $buffer === "\xFE" )
          {
            $this->_unpackCommentExtension();
          }
          // Plain Text Label
          // we may have something special to do with that
          // because it should be associated with the folowing image
          // but it seems that nobody has implemented it
          // so we just unpack and ignore it
          elseif( $buffer === "\x01" )
          {
            $this->_unpackPlainTextExtension();
          }
          // Application Label
          elseif( $buffer === "\xFF" )
          {
            $this->_unpackApplicationExtension();
          }
          else
          {
            // unknown extension label
            throw new Exception_AGif( array( get_class( $this ) , ord( $buffer ) ) , 6006 );
          }

        }
        // Trailer
        elseif( $buffer === "\x3B" )
        {
          $this->_unpackTrailer();

          // we can stop right now
          break;
        }
        else
        {
          // unknown block
          // invalid extension introducer
          $this->_printGifData();
          throw new Exception_AGif( array( get_class( $this ) , ord( $buffer ) , 0x21 ) , 6005 );
        }

      } while( ! feof( $this->_handle ) );
      
      // close handle
      fclose( $this->_handle );
      
      // return data
      return $this->_gif_pack;
    }
    catch( \Exception $e )
    {
      // close handle
      fclose( $this->_handle );
      // re-throw
      throw $e;
    }
  }
  
  /**
   * Unpack and validate Header data.
   * Must be call at first.
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 17
   */
  public function _unpackHeader()
  {
    // Header
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 17
    $buffer = fread( $this->_handle , 6 );
    
    if( strlen( $buffer ) < 6 )
    {
      // invalid header 
      throw new Exception_AGif( array( get_class( $this ) ) , 6033 );
    }
    
    // raw data
    $this->_gif_pack['Header']['_raw'] = $buffer;

    // unpack
    $format = 'A3Signature' // 3 bytes
            .'/A3Version' // 3 bytes
            ;
    $this->_gif_pack['Header'] += unpack( $format , $buffer );

    // validate
    if( $this->_gif_pack['Header']['Signature'] !== 'GIF' )
    {
      // invalid signature
      throw new Exception_AGif( array( get_class( $this ) , $this->_gif_pack['Header']['Signature'] , 'GIF' ) , 6000 );
    }

    if( $this->_gif_pack['Header']['Version'] !== '87a' && $this->_gif_pack['Header']['Version'] !== '89a' )
    {
      // invalid version
      throw new Exception_AGif( array( get_class( $this ) , $this->_gif_pack['Header']['Version'] ) , 6001 );
    }
  }
  
  /**
   * Unpack and validate Logical Screen Descriptor data.
   * Must be call immediately after _unpackHeader().
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 18
   */
  public function _unpackLogicalScreenDescriptor()
  {
    // Logical Screen Descriptor
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 18
    $buffer = fread( $this->_handle , 7 );
    
    // raw data
    $this->_gif_pack['Logical Screen Descriptor']['_raw'] = $buffer;

    // unpack
    $format = 'vLogical Screen Width' // 2 bytes
            .'/vLogical Screen Height'  // 2 bytes
            .'/CPacked' // 1 byte
            .'/CBackground Color Index' // 1 byte
            .'/CPixel Aspect Ratio' // 1 byte
            ;
    $this->_gif_pack['Logical Screen Descriptor'] += unpack( $format , $buffer );

    // Packed
    // shortcut
    $packed = $this->_gif_pack['Logical Screen Descriptor']['Packed'];
    // get binary
    $this->_gif_pack['Logical Screen Descriptor']['Packed'] = array(
      '_binary' => sprintf( '%08b' , $packed ) , // may help
      'Global Color Table Flag' => ( $packed >> 7 ) & 1 , // read bit OXXXXXXX
      'Color Resolution' => ( $packed >> 4 ) & 7 , // read bits XOOOXXXX
      'Sort Flag' => ( $packed >> 3 ) & 1 , // read bit XXXXOXXX
      'Size of Global Color Table' => ( $packed >> 0 ) & 7 , // read bits XXXXXOOO
    );
  }
  
  /**
   * Unpack and validate Global Color Table data.
   * Must be call after immediately _unpackLogicalScreenDescriptor().
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 19
   */
  public function _unpackGlobalColorTable()
  {
    // Global Color Table
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 19
    if( $this->_gif_pack['Logical Screen Descriptor']['Packed']['Global Color Table Flag'] )
    {
      // fetch 3 bytes for each colors
      $buffer = fread( $this->_handle , 3 * pow( 2 , $this->_gif_pack['Logical Screen Descriptor']['Packed']['Size of Global Color Table'] + 1 ) );

      // raw data
      $this->_gif_pack['Global Color Table']['_raw'] = $buffer;

      // most of time, it is not necessary to unpack the table
      // if you think that you need to read every colors of the table, you may be wrong
      
      $this->_gif_pack['Global Color Table']['Colors'] = array();

      // unpack
      $format = 'C*'; // all bytes are integer

      $raw = unpack( $format , $buffer );

      // each 3 bytes represents a RGB color
      $i = pow( 2 , $this->_gif_pack['Logical Screen Descriptor']['Packed']['Size of Global Color Table'] + 1 );
      while( $i-- )
      {
        $j = 3*$i;
        // note that unpack( 'C*' , $buffer ) return an array that starts at index 1
        $this->_gif_pack['Global Color Table']['Colors'][$i] = array( 'Red' => $raw[$j+1] , 'Green' => $raw[$j+2] , 'Blue' => $raw[$j+3] );
      }
    }
  }
  
  
  /**
   * Unpack and validate Image Descriptor data.
   * Call in __invoke() loop after a Image Separator has been found.
   * 
   * These data use counter.
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 20
   */
  public function _unpackImageDescriptor()
  {
    // at first create key helping counter
    $key = 'Image Descriptor'.(++$this->_counters['Image Descriptor']);
    
    // Image Descriptor
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 20

    $buffer = fread( $this->_handle , 9 );
    
    // raw data, with Image Separator
    $this->_gif_pack[$key]['_raw'] = "\x2C".$buffer;
    
    // unpack
    $format = 'vImage Left Position' // 2 bytes
            .'/vImage Top Position' // 2 bytes
            .'/vImage Width' // 2 bytes
            .'/vImage Height' // 2 bytes
            .'/CPacked' // 1 byte
            ;
    $this->_gif_pack[$key] += unpack( $format , $buffer );

    // Packed
    // shortcut
    $packed = $this->_gif_pack[$key]['Packed'];
    // get binary
    $this->_gif_pack[$key]['Packed'] = array(
      '_binary' => sprintf( '%08b' , $packed ) , // may help
      'Local Color Table Flag' => ( $packed >> 7 ) & 1 , // read bit OXXXXXXX
      'Interlace Flag' => ( $packed >> 6 ) & 1 , // read bit XOXXXXXX
      'Sort Flag' => ( $packed >> 5 ) & 1 , // read bit XXOXXXXX
      'Reserved' => ( $packed >> 3 ) & 3 , // read bits XXXOOXXX
      'Size of Local Color Table' => ( $packed >> 0 ) & 7 , // read bits XXXXXOOO
    );
  }
  
  
  /**
   * Unpack and validate Local Color Table data.
   * Must be call immediately after _unpackImageDescriptor().
   * 
   * These data use counter.
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 21
   */
  public function _unpackLocalColorTable()
  {
    // at first create key helping counter
    // even if this key may not be used, we want to increment it
    $key = 'Local Color Table'.(++$this->_counters['Local Color Table']);
    
    // Local Color Table
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 21

    // look for corresponding Image Descriptor data
    $desc_key = 'Image Descriptor'.($this->_counters['Image Descriptor']);
    if( $this->_gif_pack[$desc_key]['Packed']['Local Color Table Flag'] )
    {
      // fetch 3 bytes for each colors
      $buffer = fread( $this->_handle , 3 * pow( 2 , $this->_gif_pack[$desc_key]['Packed']['Size of Local Color Table'] + 1 ) );

      // raw data
      $this->_gif_pack[$key]['_raw'] = $buffer;

      // most of time, it is not necessary to unpack the table
      // if you think that you need to read every colors of the table, you may be wrong
      
      $this->_gif_pack[$key]['Colors'] = array();
      
      // unpack
      $format = 'C*'; // all bytes are integer
      $raw = unpack( $format , $buffer );

      // each 3 bytes represents a RGB color
      $i = pow( 2 , $this->_gif_pack[$desc_key]['Packed']['Size of Local Color Table'] + 1 );
      while( $i-- )
      {
        $j = 3*$i;
        // note that unpack( 'C*' , $buffer ) return an array that starts at index 1
        $this->_gif_pack[$key]['Colors'][$i] = array( 'Red' => $raw[$j+1] , 'Green' => $raw[$j+2] , 'Blue' => $raw[$j+3] );
        // $this->_gif_pack[$key]['Colors'][$i] = new Color( array( $raw[$j+1] , $raw[$j+2] , $raw[$j+3] ) );
      }
    }
  }
  
  
  /**
   * Unpack and validate Table Based Image Data data.
   * Must be call immediately after _unpackLocalColorTable().
   * 
   * These data use counter.
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 22
   */
  public function _unpackTableBasedImageData()
  {
    // at first create key helping counter
    $key = 'Table Based Image Data'.(++$this->_counters['Table Based Image Data']);
    
    // Table Based Image Data
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 22

    // LZW Minimum Code Size
    $buffer = fgetc( $this->_handle );
    
    // raw data
    $this->_gif_pack[$key]['_raw'] = $buffer;
    
    // unpack
    $this->_gif_pack[$key]['LZW Minimum Code Size'] = ord( $buffer );

    // Data Sub-blocks 
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 15
    // Block Terminator 
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 16
    
    // initialize data
    $this->_gif_pack[$key]['Data'] = '';
    
    while( ( $buffer = fgetc( $this->_handle ) ) !== "\x00" )
    {
      $part = fread( $this->_handle , ord( $buffer ) );
      
      // update raw data, with Block Size
      $this->_gif_pack[$key]['_raw'] .= $buffer.$part;
      
      // add data
      $this->_gif_pack[$key]['Data'] .= $part;
    }
    
    // add Block Terminator to raw data
    $this->_gif_pack[$key]['_raw'] .= "\x00";

    // no, you do not want to apply LZW uncompression
  }
  
  
  /**
   * Unpack and validate Graphic Control Extension data.
   * Call in __invoke() loop after a Extension Introducer and a 
   * Graphic Control Label have been found.
   * 
   * These data use counter.
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  public function _unpackGraphicControlExtension()
  {
    // at first create key helping counter
    $key = 'Graphic Control Extension'.(++$this->_counters['Graphic Control Extension']);

    // Graphic Control Extension
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23

    // Graphic Control Extension
    // - a special Graphic Control Block (fixed size to 4 bytes)

    $buffer = fgetc( $this->_handle );

    // validate
    if( ord( $buffer ) !== 4 )
    {
      // invalid block size
      throw new Exception_AGif( array( get_class( $this ) , ord( $buffer ) , 4 ) , 6015 );
    }

    $buffer = fread( $this->_handle , 4 );
    
    // raw data, with Extension Introducer, Graphic Control Label and Block Size
    $this->_gif_pack[$key]['_raw'] = "\x21\xF9\x04".$buffer;

    // unpack
    $format = 'CPacked' // 1 bytes
            .'/vDelay Time' // 2 bytes
            .'/CTransparent Color Index' // 1 byte
            ;
    $this->_gif_pack[$key] += unpack( $format , $buffer );

    // Packed
    // shortcut
    $packed = $this->_gif_pack[$key]['Packed'];
    // get binary
    $this->_gif_pack[$key]['Packed'] = array(
      '_binary' => sprintf( '%08b' , $packed ) , // may help
      'Reserved' => ( $packed >> 5 ) & 7 , // read bits OOOXXXXX
      'Disposal Method' => ( $packed >> 2 ) & 7 , // read bits XXXOOOXX
      'User Input Flag' => ( $packed >> 1 ) & 1 , // read bit XXXXXXOX
      'Transparent Color Flag' => ( $packed >> 0 ) & 1 , // read bit XXXXXXXO
    );

    // Block Terminator 
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 16
    if( ( $buffer = fgetc( $this->_handle ) ) !== "\x00" )
    {
      // invalid block terminator
      throw new Exception_AGif( array( get_class( $this ) , ord( $buffer ) , 0x00 ) , 6016 );
    }
    
    // add Block Terminator to raw data
    $this->_gif_pack[$key]['_raw'] .= "\x00";
  }
  
  
  /**
   * Unpack and validate Comment Extension data.
   * Call in __invoke() loop after a Extension Introducer and a Comment Label 
   * have been found.
   * 
   * These data use counter.
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 24
   */
  public function _unpackCommentExtension()
  {
    // at first create key helping counter
    $key = 'Comment Extension'.(++$this->_counters['Comment Extension']);

    // Comment Extension
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 24

    // Comment Extension is composed by
    // - 0 to N Data Sub-Blocks
    // - a Block Terminator ("\x00")
    
    // initialize raw data with Extension Introducer and Comment Label
    $this->_gif_pack[$key]['_raw'] = "\x21\xFE";

    // Data Sub-blocks 
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 15
    // Block Terminator 
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 16
    
    // initialize data
    $this->_gif_pack[$key]['Data'] = '';
    
    while( ( $buffer = fgetc( $this->_handle ) ) !== "\x00" )
    {
      $part = fread( $this->_handle , ord( $buffer ) );
      
      // update raw data, with Block Size
      $this->_gif_pack[$key]['_raw'] .= $buffer.$part;
      
      // add data
      $this->_gif_pack[$key]['Data'] .= $part;
    }
    
    // add Block Terminator to raw data
    $this->_gif_pack[$key]['_raw'] .= "\x00";
  }
  
  
  /**
   * Unpack and validate Plain Text Extension data.
   * Call in __invoke() loop after a Extension Introducer and a Plain Text Label 
   * have been found.
   * 
   * These data use counter.
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 25
   */
  public function _unpackPlainTextExtension()
  {
    // at first create key helping counter
    $key = 'Plain Text Extension'.(++$this->_counters['Plain Text Extension']);

    // Plain Text Extension
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 25

    // Plain Text Extension is composed by
    // - a special Plain Text Block (fixed size to 12 bytes)
    // - 0 to N Data Sub-Blocks
    // - a Block Terminator ("\x00")
    
    $buffer = fgetc( $this->_handle );

    if( ord( $buffer ) !== 12 )
    {
      // invalid block size
      throw new Exception_AGif( array( get_class( $this ) , ord( $buffer ) , 12 ) , 6020 );
    }

    $buffer = fread( $this->_handle , 12 );
    
    // raw data, with Extension Introducer, Plain Text Label and Block Size
    $this->_gif_pack[$key]['_raw'] = "\x21\x01\x0C".$buffer;
    
    // unpack
    $format = 'vText Grid Left Position' // 2 bytes
            .'/vText Grid Left Position' // 2 bytes
            .'/vText Grid Width' // 2 bytes
            .'/vText Grid Height' // 2 bytes
            .'/CCharacter Cell Width' // 1 byte
            .'/CCharacter Cell Height' // 1 byte
            .'/CText Foreground Color Index' // 1 byte
            .'/CText Background Color Index' // 1 byte
            ;
    $this->_gif_pack[$key] += unpack( $format , $buffer );

    // Data Sub-blocks 
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 15
    // Block Terminator 
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 16
    
    // initialize data
    $this->_gif_pack[$key]['Data'] = '';
    
    while( ( $buffer = fgetc( $this->_handle ) ) !== "\x00" )
    {
      $part = fread( $this->_handle , ord( $buffer ) );
      
      // update raw data, with Block Size
      $this->_gif_pack[$key]['_raw'] .= $buffer.$part;
      
      // add data
      $this->_gif_pack[$key]['Data'] .= $part;
    }
    
    // add Block Terminator to raw data
    $this->_gif_pack[$key]['_raw'] .= "\x00";
  }
  
  
  /**
   * Unpack and validate Application Extension data.
   * Call in __invoke() loop after a Extension Introducer and a Application Label 
   * have been found.
   * 
   * These data use counter.
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 26
   */
  public function _unpackApplicationExtension()
  {
    // at first create key helping counter
    $key = 'Application Extension'.(++$this->_counters['Application Extension']);

    // Application Extension
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 26

    // Application Extension is composed by
    // - a special Application Block (fixed size to 12 bytes)
    // - 0 to N Data Sub-Blocks
    // - a Block Terminator ("\x00")

    $buffer = fgetc( $this->_handle );

    if( ord( $buffer ) !== 11 )
    {
      // invalid block size
      throw new Exception_AGif( array( get_class( $this ) , ord( $buffer ) , 11 ) , 6010 );
    }

    $buffer = fread( $this->_handle , 11 );
    
    // raw data, with Extension Introducer, Application Label and Block Size
    $this->_gif_pack[$key]['_raw'] = "\x21\xFF\x0B".$buffer;

    // unpack
    $format = 'A8Application Identifier' // 2 bytes
            .'/A3Application Authentication Code' // 2 bytes
            ;
    $this->_gif_pack[$key] += unpack( $format , $buffer );

    // it seems that some application does not follow Data Sub-Blocks mecanism
    // especially "XMP DataXMP"
    
    $method = '_unpackApplicationExtensionFor'.Config::camelize( $buffer );
    
    if( method_exists( $this , $method ) )
    {
      // apply this method
      $this->$method();
    }
    else
    {
      // do it standard way

      // Data Sub-blocks 
      // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 15
      // Block Terminator 
      // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 16

      // initialize data
      $this->_gif_pack[$key]['Data'] = '';

      while( ( $buffer = fgetc( $this->_handle ) ) !== "\x00" )
      {
        $part = fread( $this->_handle , ord( $buffer ) );

        // update raw data, with Block Size
        $this->_gif_pack[$key]['_raw'] .= $buffer.$part;

        // add data
        $this->_gif_pack[$key]['Data'] .= $part;
      }

      // add Block Terminator to raw data
      $this->_gif_pack[$key]['_raw'] .= "\x00";
    }
  }
  
  
  /**
   * Unpack and validate Application Extension data for Application XMP Data XMP.
   * Call in _unpackApplicationExtension() when a XMP Data XMP application has 
   * been detected.
   * 
   * !!! These data use last Application Extension counter !!!
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://wwwimages.adobe.com/content/dam/Adobe/en/devnet/xmp/pdfs/XMPSpecificationPart3.pdf
   */
  public function _unpackApplicationExtensionForXmpDataXmp()
  {
    // at first create key helping counter
    // DO NOT INCREMENT THIS COUNTER HERE
    $key = 'Application Extension'.($this->_counters['Application Extension']);

    // XMP Data
    // @see http://wwwimages.adobe.com/content/dam/Adobe/en/devnet/xmp/pdfs/XMPSpecificationPart3.pdf

    // this format does not specify the number of bytes to read...
    // very smart, isn't it?
    // especially when the specification is entirely based on this concept
    // Adobe is fantastic

    // there is a "magic" trailer probably done by David Coperfield
    // that looks like "\x01xFFxFE" ... 252 bytes ... "\x01\x00\x00"

    // also XMP Data start with something like <?xpacket ...>
    // and always end with something like <?xpacket ...>

    // another approach would be to assume that data do not contains "\x00" - for 
    // some XML shits, seems reasonable - except at the end of the "magic" trailer... 

    $this->_gif_pack[$key]['Data'] = '';
    while( ( $buffer = fgetc( $this->_handle ) ) !== "\x00" )
    {
      $this->_gif_pack[$key]['Data'] .= $buffer;
    }
    // add the last "\x00" - that is not a correct Block Terminator - to data
    $this->_gif_pack[$key]['Data'] .= "\x00";

    // Block Terminator 
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 16
    if( ( $buffer = fgetc( $this->_handle ) ) !== "\x00" )
    {
      // invalid block terminator
      throw new Exception_AGif( array( get_class( $this ) , ord( $buffer ) , 0x00 ) , 6030 );
    }
    
    // update raw data with an exact copy of Data and real Block terminator
    $this->_gif_pack[$key]['_raw'] .= $this->_gif_pack[$key]['Data']."\x00";
  }
  
  
  /**
   * Unpack and validate Application Extension data for Application NETSCAPE 2.0.
   * Call in _unpackApplicationExtension() when a NETSCAPE 2.0 application has 
   * been detected.
   * 
   * !!! These data use last Application Extension counter !!!
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://en.wikipedia.org/wiki/Graphics_Interchange_Format#Animated_GIF
   */
  public function _unpackApplicationExtensionForNetscape20()
  {
    // at first create key helping counter
    // DO NOT INCREMENT THIS COUNTER HERE
    $key = 'Application Extension'.($this->_counters['Application Extension']);

    // do it standard way

    // Data Sub-blocks 
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 15
    // Block Terminator 
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 16
    
    // initialize data
    $this->_gif_pack[$key]['Data'] = '';
    
    while( ( $buffer = fgetc( $this->_handle ) ) !== "\x00" )
    {
      $part = fread( $this->_handle , ord( $buffer ) );
      
      // update raw data, with Block Size
      $this->_gif_pack[$key]['_raw'] .= $buffer.$part;
      
      // add data
      $this->_gif_pack[$key]['Data'] .= $part;
    }
    
    // add Block Terminator to raw data
    $this->_gif_pack[$key]['_raw'] .= "\x00";
    
    
    // Application Extension Data for NETSCAPE 2.0 are composed like this:
    // - Block Size, 1 byte, fixed to 3
    // - Sub-Block Index, 1 byte
    // - Number of repeatitions, 1 byte
    // @see http://en.wikipedia.org/wiki/Graphics_Interchange_Format#Animated_GIF
    
    if( strlen( $this->_gif_pack[$key]['Data'] ) !== 3 )
    {
      // invalid data block
      throw new Exception_AGif( array( get_class( $this ) , strlen( $this->_gif_pack[$key]['Data'] ) , 3 ) , 6031 );
    }

    // first byte should allways be 1
    if( $this->_gif_pack[$key]['Data']{0} !== "\x01" )
    {
      // invalid data block
      throw new Exception_AGif( array( get_class( $this ) , ord( $this->_gif_pack[$key]['Data']{0} ) , 0x01 ) , 6032 );
    }

    // first byte is sub-block index
    // second is number of repetition (0 means infinite)
    $format = 'CSub-Block Index' // 1 byte
            .'/vNumber of Repetitions'; // 2 bytes

    $this->_gif_pack[$key] += unpack( $format , $this->_gif_pack[$key]['Data'] );
  }
  
  
  /**
   * Unpack and validate Trailer data.
   * Call in __invoke() loop method after a Trailer has been found.
   * 
   * The handle should not contains any data after this trailer.
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  public function _unpackTrailer()
  {
    // Trailer
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 27

    // it seems that some GIF files, especially ones coming from ajaxload.info, does
    // not respect Trailer specification, so we cannot throw an exception in that cases

    // // look for EOF
    // // note feof() will return true because the end of the file has not been reach
    // if( ( $buffer = fgetc( $this->_handle ) ) !== false )
    // {
    //   // invalid block
    //   throw new Exception_AGif( array( get_class( $this ) , ord( $buffer ) ) , 6025 );
    // }
    
    // raw data
    $this->_gif_pack['Trailer']['_raw'] = "\x3B";
  }
}
