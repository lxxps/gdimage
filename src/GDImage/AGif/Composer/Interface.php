<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Interface for GIF composer.
 * 
 * Composer are used to create a GIF pack from an array of 
 * \GDImage\Image_Interface. The frames may be manipulated to fit expected 
 * results.
 * 
 * 
 * For convenience, keys in pack correspond to GIF specification Block name.
 * 
 * Each keys contain Block Data with the same name than GIF specification
 * and maybe a "_raw" key to represent raw data of the block.
 *  
 * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt
 * 
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage agif
 * @author     Loops <pierrotevrard@gmail.com>
 * @interface
 */
interface AGif_Composer_Interface
{
  /**
   * Method to create a GIF pack from an array of \GDImage\Image_Interface.
   * 
   * 
   * @param array $frames Array of \GDImage\Image_Interface
   * @return array GIF pack
   * @access public
   * @throws \GDImage\Exception_AGif
   * @static
   */
  static public function compose( array $frames );
  
}
