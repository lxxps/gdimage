<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Interface for GIF framer.
 * 
 * Framer are used to create frame instance used over all AGif classes.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage agif
 * @author     Loops <pierrotevrard@gmail.com>
 * @interface
 */
interface AGif_Framer_Interface
{
  /**
   * Function used to create a blank frame instance.
   * 
   * @param none
   * @return \GDImage\Image_AGif_Frame
   * @access public
   * @throws \GDImage\Exception_AGif
   * @static
   */
  static public function frame();
}
