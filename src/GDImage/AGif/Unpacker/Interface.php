<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Interface for GIF unpacker.
 * 
 * Unpacker are used to create an array of data (GIF pack) from GIF file or 
 * binary data. The pack will be used by a GDImage\AGif_Decomposer_Interface to 
 * create GIF frame.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage agif
 * @author     Loops <pierrotevrard@gmail.com>
 * @interface
 */
interface AGif_Unpacker_Interface
{
  /**
   * Function used to unpack GIF file or data to a big array of data.
   * 
   * For convenience, keys correspond to GIF specification Block name.
   * 
   * Each keys contain Block Data with the same name than GIF specification
   * with allways a "_raw" key to represent raw data of the block.
   * 
   * Blocks that can be present more than one time are suffixed by an incremental
   * number used to identify the Block occurence (ie. "Image Descriptor3" for the 
   * fourth "Image Descriptor" Block found).
   * 
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt
   * 
   * @param string $filepath_or_binary Filepath or binary data
   * @return array GIF pack
   * @access public
   * @throws \GDImage\Exception_AGif
   * @static
   */
  static public function unpack( $filepath_or_binary );
}
