<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Class to create frames from a animated GIF pack.
 * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt
 * @see \GDImage\AGif_Unpacker
 * 
 * An important thing about AGIF delay time is that it can be 0, and the GIF
 * still animated: this is due to browser minimum frame delay and most of the 
 * time, this 0 delay is rounded up to 100ms.
 * @see http://nullsleep.tumblr.com/post/16524517190/animated-gif-minimum-frame-delay-browser-compatibility
 * 
 * It is not clear in the specifications, but if there is multiple Image 
 * Descriptor, a Graphic Control Extension will always preceded each Image 
 * Descriptor.
 * 
 * An important thing about AGIF frame, is that, from the disposal method, 
 * the previous frame is visible behind transparent color of the current one. 
 * Most of loader use this ability to reduce the file size to minimum.
 * 
 * In our case, we want the frame at its visible state, with the previous frame 
 * behind if necessary. The ability to reduce filesize belong to the
 * "AGif_Composer" used to build the AGIF binary.
 *
 * Also, true color GIF use this ablity, combined with Local Color Table. None 
 * of the frames overlap, so you do not see any animation.
 * @see http://phil.ipal.org/tc.html
 * 
 * That's why if the AGIF contains frames with Local Color Table, they have to 
 * be converted in true color resource to keep the previous frame visible. We 
 * do not have any other choices.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage agif
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\AGif_Decomposer_Interface
 */
class AGif_Decomposer implements AGif_Decomposer_Interface
{
  
  /**
   * Method to create an array of \GDImage\Image_AGif_Frame
   * from an animated GIF pack.
   * 
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt
   * 
   * @param array GIF pack from \GDImage\AGif_Unpacker
   * @return array Array of \GDImage\Image_AGif_Frame
   * @access public
   * @throws \GDImage\Exception_AGif
   * @static
   * @implements \GDImage\AGif_Decomposer_Interface
   */
  static public function decompose( array $pack )
  {
    // create instance
    $decomposer = new static();
    // invoke it and return frames
    return $decomposer( $pack );
  }
  
  /**
   * Current GIF pack
   * 
   * @see \GDImage\AGif_Unpacker
   * @var array
   * @access protected
   */
  public $_gif_pack;
  
  /**
   * Array of extracted image
   * 
   * @var array Array of \GDImage\Image_AGif_Frame
   * @access protected
   */
  public $_frames;
  
  /**
   * Method to create an array of \GDImage\Image_AGif_Frame
   * from an animated GIF pack.
   *
   * @param array $pack
   * @return array Array of \GDImage\Image_AGif_Frame
   * @access public
   * @throws \GDImage\Exception_AGif
   */
  public function __invoke( array $pack )
  {
    // reset frames array
    $this->_frames = array();
    // assign pack
    $this->_gif_pack = $pack;
    
    // make some minimal checks on pack
    if( empty($this->_gif_pack['Logical Screen Descriptor']) )
    {
      throw new Exception_AGif( array( get_class( $this ) ) , 6058 );
    }
    
    // global color table check
    if( $this->_gif_pack['Logical Screen Descriptor']['Packed']['Global Color Table Flag']
        && empty($this->_gif_pack['Global Color Table']) )
    {
      // missing global color table
      throw new Exception_AGif( array( get_class( $this ) ) , 6053 );
    }
    
    // make a check on Graphic Control Extension
    // if one image has Graphic Control Extension
    // all images must have Graphic Control Extension
    $graphic_count = null;
    $frame_count = 0;
    while( isset($this->_gif_pack['Image Descriptor'.$frame_count]) )
    {
      if( isset($this->_gif_pack['Graphic Control Extension'.$frame_count]) )
      {
        // start or increment counter
        $graphic_count++;
      }
        
      $frame_count++;
    }
    
    if( $graphic_count !== null && $graphic_count !== $frame_count )
    {
      // oups, no graphic control extension for all images
      throw new Exception_AGif( array( get_class( $this ) , $frame_count , $graphic_count ) , 6055 );
    }
    
    // everything looks fine, process creation
    for( $i = 0; $i < $frame_count; $i++ )
    {
      $this->_frames[] = $this->_doFrame( $i );
    }
    
    return $this->_frames;
  }
  
  /**
   * Method to create an \GDImage\Image_AGif_Frame instance 
   * from animated GIF pack.
   *
   * @param integer $index Image index in the pack
   * @return \GDImage\Image_AGif_Frame
   * @access protected
   * @throws \GDImage\Exception_AGif
   */
  public function _doFrame( $index )
  {
    // create frame instance
    $frame = AGif_Factory::frame();
    
    // create GD resource from binary, only if previous stuff is ok
    if( $this->_doFrameBinary( $frame , $index ) )
    {
      // do frame upgrade to fit "visual" appearence
      // also update frame information
      $this->_doFrameUpgrade( $frame , $index );
      
      return $frame;
    }
    
    // an error occurs on binary creation
    throw new Exception_AGif( array( get_class( $this ) , $index ) , 6056 );
  }
  
  /**
   * Create binary resource from pack
   *
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean Success
   * @access protected
   * @throws \GDImage\Exception_AGif
   */
  public function _doFrameBinary( Image_AGif_Frame $frame , $index )
  {
    // make some minimal checks on pack
    if( empty($this->_gif_pack['Image Descriptor'.$index]) )
    {
      throw new Exception_AGif( array( get_class( $this ) , $index ) , 6050 );
    }
    if( empty($this->_gif_pack['Table Based Image Data'.$index]) )
    {
      throw new Exception_AGif( array( get_class( $this ) , $index ) , 6051 );
    }
    
    // local color table check
    if( $this->_gif_pack['Image Descriptor'.$index]['Packed']['Local Color Table Flag'] 
        && empty($this->_gif_pack['Local Color Table'.$index]) )
    {
      // missing local color table
      throw new Exception_AGif( array( get_class( $this ) , $index ) , 6052 );
    }
    
    // global color table check
    if( ( ! $this->_gif_pack['Image Descriptor'.$index]['Packed']['Local Color Table Flag'] )
        && empty($this->_gif_pack['Global Color Table']) )
    {
      // missing local color table
      throw new Exception_AGif( array( get_class( $this ) , $index ) , 6053 );
    }
    
    $binary = '';
    
    // Header
    $binary .= $this->_gif_pack['Header']['_raw'];
            
    // Logical Screen Descriptor
    $binary .= $this->_gif_pack['Logical Screen Descriptor']['_raw'];
            
    // Global Color Table
    if( isset($this->_gif_pack['Global Color Table']) )
    {
      $binary .= $this->_gif_pack['Global Color Table']['_raw'];
    }
    
    // now look for a Graphic Control Extension for this index
    if( isset($this->_gif_pack['Graphic Control Extension'.$index]) )
    {
      $binary .= $this->_gif_pack['Graphic Control Extension'.$index]['_raw'];
    }
    
    // add Image Descriptor
    $binary .= $this->_gif_pack['Image Descriptor'.$index]['_raw'];
    
    // look for a Local Color Table for this index
    if( isset($this->_gif_pack['Local Color Table'.$index]) )
    {
      $binary .= $this->_gif_pack['Local Color Table'.$index]['_raw'];
    }
    
    // add Table Based Image Data
    $binary .= $this->_gif_pack['Table Based Image Data'.$index]['_raw'];
    
    // add Trailer
    $binary .= $this->_gif_pack['Trailer']['_raw'];
    
    // return flag
    return $frame->fromBinary( $binary );
  }
  
  /**
   * Upgrade resource from pack information.
   * 
   * GD has some interesting abilities with GIF:
   * - on import, not used colors will be removed and the transparent color 
   *   may be lost;
   * - it ignores the logical screen height and width.
   * 
   * We want the frame to be complete, so when Disposal Method is 1 or 3,
   * we want to complete transparent pixels with the previous frame image.
   * 
   * Because of that "residual" image ability, some animated GIF with local 
   * color table may generate frames with true color resource.
   * 
   * This method return true if something has been done on the frame, that does 
   * not mean that false is an error.
   *
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something have been done, false otherwise
   * @access protected
   * @throws \GDImage\Exception_AGif
   */
  public function _doFrameUpgrade( Image_AGif_Frame $frame , $index )
  {
    $flag = false;
    
    // order is important!
    // upgrade Interlace Flag
    $flag = $this->_doFrameUpgradeInterlaceFlag( $frame , $index ) || $flag;
    // upgrade Delay Time
    $flag = $this->_doFrameUpgradeDelayTime( $frame , $index ) || $flag;
    // upgrade Transparent Color
    $flag = $this->_doFrameUpgradeTransparentColor( $frame , $index ) || $flag;
    // upgrade Background Color
    $flag = $this->_doFrameUpgradeBackgroundColor( $frame , $index ) || $flag;
    // upgrade Logical Screen
    $flag = $this->_doFrameUpgradeLogicalScreen( $frame , $index ) || $flag;
    // upgrade Disposal Method
    $flag = $this->_doFrameUpgradeDisposalMethod( $frame , $index ) || $flag;
    
    return $flag;
  }
  
  /**
   * Assign Interlace Flag from pack information.
   * 
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   */
  public function _doFrameUpgradeInterlaceFlag( Image_AGif_Frame $frame , $index )
  {
    // look for interlace flag
    if( ! $this->_gif_pack['Image Descriptor'.$index]['Packed']['Interlace Flag'] ) return false;
    
    // activate interlace mmode
    $frame->getResource()->setMode( Resource_Abstract::MODE_INTERLACE );
      
    return true;
  }
  
  /**
   * Assign Delay Time from pack information.
   * 
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   */
  public function _doFrameUpgradeDelayTime( Image_AGif_Frame $frame , $index )
  {
    // look for a Graphic Control Extension for this index
    if( empty($this->_gif_pack['Graphic Control Extension'.$index]) ) return false;
    
    // do some assignement
    $frame->setDelayTime( $this->_gif_pack['Graphic Control Extension'.$index]['Delay Time'] );

    return true;
  }
  
  /**
   * Restore Background Color from pack information.
   * 
   * GD does not support background color that are not at index 0.
   * 
   * To avoid that, we must extract background color information from 
   * global color table and make sure it is restored on the frame 
   * resource.
   * 
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   */
  public function _doFrameUpgradeBackgroundColor( Image_AGif_Frame $frame , $index )
  {
    // if background color is 0, we are OK
    if( $this->_gif_pack['Logical Screen Descriptor']['Background Color Index'] === 0 ) return false;
    
    // if not, we have to update that
    
    // step 1: extract background color information
    
    $bg_index = $this->_gif_pack['Logical Screen Descriptor']['Background Color Index'];
      
    // look for which color table to use
    if( $this->_gif_pack['Image Descriptor'.$index]['Packed']['Local Color Table Flag'] )
    {
      // use local table
      $table = $this->_gif_pack['Local Color Table'.$index]['_raw'];
    }
    else
    {
      // use global table
      $table = $this->_gif_pack['Global Color Table']['_raw'];
    }
    
    // we can extract the three caracters that indicate color information
    // note that unpack( C* ) return an array that starts at index 1
    list(  , $r , $g , $b ) = array( 0 => null ) + unpack( 'C*' , substr( $table , $bg_index * 3 , 3 ) );
    
    $bg = new Color( array( $r , $g , $b ) );
    
    // step 2: create the resource
    
    // create an empty resource (not blank)
    $dst = Resource_PaletteColor::createFromSize( $frame->getWidth() , $frame->getHeight() );
    
    // assign background color at first
    $dst->setColor( $bg );
    
    // we must be sure that GD resource is awaken before to play with colors
    $frame->getResource()->awakeGdResource();
    
    // propagate transparent
    if( $transparent = $frame->getResource()->getTransparent() )
    {
      // for transparent, we want to fill entire picture
      imagefill( $dst->getGdResource() , 0 , 0 , $dst->setTransparent( $transparent ) );
    }
    
    // now we can copy entire frame to the new resource
    // if fail, return false
    if( ! imagecopy( $dst->getGdResource() , $frame->getResource()->getGdResource() , 0 , 0 , 0 , 0 , $frame->getWidth() , $frame->getHeight() ) ) return false;
    
    // propagate modes
    $dst->setModes( $frame->getResource()->getModes() );
    
    // set new resource
    $frame->setResource( $dst );
    
    return true;
  }
  
  /**
   * Restore Transparent Color from pack information.
   * 
   * GD rearrange color index that can result to a transparent color loses.
   * 
   * To avoid that, we must extract transparent color information from 
   * global/local color table and make sure it is restored on the frame 
   * resource.
   * 
   * In fact, GD create a color palette only for colors that have been used, so 
   * if the transparent color was not used for the given frame area, it results 
   * to a transparent color loses.
   * 
   * To avoid that, we just assing the transparent color if not done by GD.
   * 
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   */
  public function _doFrameUpgradeTransparentColor( Image_AGif_Frame $frame , $index )
  {
    // look for a Graphic Control Extension for this index
    if( empty($this->_gif_pack['Graphic Control Extension'.$index]) ) return false;
    
    // look for Transparent Color Flag
    if( ! $this->_gif_pack['Graphic Control Extension'.$index]['Packed']['Transparent Color Flag'] ) return false;
    
    // ok, we can use Transparent Color Index
    $transparent_index = $this->_gif_pack['Graphic Control Extension'.$index]['Transparent Color Index'];
    
    // now look for which color table to use
    if( $this->_gif_pack['Image Descriptor'.$index]['Packed']['Local Color Table Flag'] )
    {
      // use local table
      $table = $this->_gif_pack['Local Color Table'.$index]['_raw'];
    }
    else
    {
      // use global table
      $table = $this->_gif_pack['Global Color Table']['_raw'];
    }
    
    // now, we can extract the three caracters that indicate color information
    // note that unpack( C* ) return an array that starts at index 1
    list(  , $r , $g , $b ) = array( 0 => null ) + unpack( 'C*' , substr( $table , $transparent_index * 3 , 3 ) );
    
    $transparent = new Color( array( $r , $g , $b , Color::ALPHA_TRANSPARENT ) );
    
    // we must be sure that GD resource is awaken before to play with colors
    $frame->getResource()->awakeGdResource();
    
    // look for a exact match
    // if found, there is nothing to do
    if( $frame->getResource()->findColor( $transparent , true ) !== false ) return false;
    
    // at this point we can be sure that there is less colors on the image 
    // palette than on the original table, so we can safely assign the 
    // transparent color and return success flag
    
    // looks like a bug of GD, but imagecolortransparent() return an index that
    // is not in the palette and cannot be deallocated... 
    // Error Control Operator (@) exists for that kind of freaky bug
    return @ $frame->getResource()->setTransparent( $transparent );
  }
  
  /**
   * Adjust image to fit Logical Screen Width and Height from pack information.
   *
   * In order to stay able to resize correctly
   * we want the new Resource to fit Logical Screen Width and Logical Screen Height
   * 
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   */
  public function _doFrameUpgradeLogicalScreen( Image_AGif_Frame $frame , $index )
  {
    $dw = $this->_gif_pack['Logical Screen Descriptor']['Logical Screen Width'];
    $dh = $this->_gif_pack['Logical Screen Descriptor']['Logical Screen Height'];
    $sw = $this->_gif_pack['Image Descriptor'.$index]['Image Width'];
    $sh = $this->_gif_pack['Image Descriptor'.$index]['Image Height'];
    $dx = $this->_gif_pack['Image Descriptor'.$index]['Image Left Position'];
    $dy = $this->_gif_pack['Image Descriptor'.$index]['Image Top Position'];
    
    // same size and position, nothing to do
    if( $dw === $sw && $dh === $sh && $dx === 0 && $dy === 0 ) return false;
    
    // create blank resource
    $dst = $frame->getResource()->blank( $dw , $dh );
    
    // DO NOT DO THIS, it does not work and it is very slow
    // imagepalettecopy( $dst->getGdResource() , $frame->getResource()->getGdResource() );

    // copy source to expected position, if false we fail
    if( ! imagecopy( $dst->getGdResource() , $frame->getResource()->getGdResource() , $dx , $dy , 0 , 0 , $sw , $sh ) ) return false;
    
    // propagate modes
    $dst->setModes( $frame->getResource()->getModes() );
    
    // set new resource
    $frame->setResource( $dst );
    
    // we did it!
    return true;
  }
  
  /**
   * Assign Disposal Method from pack information.
   * Also adjust frame content from the PREVIOUS frame Disposal Method.
   * 
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   * @see http://www.theimage.com/animation/pages/disposal.html
   * @see http://www.theimage.com/animation/pages/disposal2.html
   * @see http://www.theimage.com/animation/pages/disposal3.html
   * @see http://the-labs.com/GIFMerge/
   */
  public function _doFrameUpgradeDisposalMethod( Image_AGif_Frame $frame , $index )
  {
    // look for a Graphic Control Extension for this index
    if( empty($this->_gif_pack['Graphic Control Extension'.$index]) ) return false;
    
    // do some assignement
    $frame->setDisposalMethod( $this->_gif_pack['Graphic Control Extension'.$index]['Packed']['Disposal Method'] );
    
    // at this point, we must do some stuff from disposal method
    
    // from specification: 
    // Values :    0 -   No disposal specified. The decoder is
    //                   not required to take any action.
    //             1 -   Do not dispose. The graphic is to be left
    //                   in place.
    //             2 -   Restore to background color. The area used by the
    //                   graphic must be restored to the background color.
    //             3 -   Restore to previous. The decoder is required to
    //                   restore the area overwritten by the graphic with
    //                   what was there prior to rendering the graphic.
    //          4-7 -    To be defined.
    
    // so to know how area behind frame has to be restored, we must know the 
    // Disposal Method of the PREVIOUS frame
    
    // first frame, nothing to do
    if( $index === 0 ) return true;
    
    $previous_frame = $this->_frames[$index-1];
    
    $method = '_doFrameUpgradeDisposalMethod'.$previous_frame->getDisposalMethod();
    
    if( ! method_exists( $this , $method ) )
    {
      // Unknown Disposal Method
      throw new Exception_AGif( array( get_class( $this ) , $previous_frame->getDisposalMethod() , $index-1 ) , 6057 );
    }
    
    return $this->$method( $frame , $index );
  }
  
  
  /**
   * Adjust frame content from Disposal Method 0 (unspecified).
   * Keep in mind that the Disposal Method is fetch from the 
   * PREVIOUS frame (index-1).
   * 
   * From specification:
   * 0 - No disposal specified. The decoder is not required to take any action.
   * 
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   * @see http://the-labs.com/GIFMerge/#manipulation_offset_position
   */
  public function _doFrameUpgradeDisposalMethod0( Image_AGif_Frame $frame , $index )
  {
    // it seems that disposal method to 0 has same meaning thatn disposal method to 1
    // @see http://the-labs.com/GIFMerge/#manipulation_offset_position
    return $this->_doFrameUpgradeDisposalMethod1( $frame , $index );
    
//    // nothing to do
//    return false;
  }
  
  /**
   * Adjust frame content from Disposal Method 1 (no).
   * Keep in mind that the Disposal Method is fetch from the 
   * PREVIOUS frame (index-1).
   * 
   * From specification:
   * 1 - Do not dispose. The graphic is to be left in place.
   * 
   * That's means that we have to keep the previous frame behind the current one.
   * 
   * In that case, we may have to create true color resource, especially if the 
   * two resource has distinct Local Color Table.
   * 
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  public function _doFrameUpgradeDisposalMethod1( Image_AGif_Frame $frame , $index )
  {
    // attemp merge behind with previous frame
    return $this->_doFrameUpgradeDisposalTransparentToBehind( $frame , $index , $index-1 );
  }
  
  /**
   * Adjust frame content from Disposal Method 2 (background).
   * Keep in mind that the Disposal Method is fetch from the 
   * PREVIOUS frame (index-1).
   * 
   * From specification:
   * 2 - Restore to background color. The area used by the graphic must be 
   *     restored to the background color.
   * 
   * The specification are not clear but it seems than we have to use the 
   * background color or the transparent one as background for the frame.
   * 
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   * @see http://the-labs.com/GIFMerge/#manipulation_offset_position
   */
  public function _doFrameUpgradeDisposalMethod2( Image_AGif_Frame $frame , $index )
  {
    // if current frame has transparent color, we have nothing to do
    if( $frame->getResource()->getTransparent() !== false ) return false;
    
    // NOTE: Since I do not have any sample to test Disposal Method 2 with a 
    // frame without Transparent Color and with specific Image Left Position
    // and Image Top Position, I can't extrapolate this part
    return false;
  }
  
  /**
   * Adjust frame content from Disposal Method 3 (previous).
   * Keep in mind that the Disposal Method is fetch from the 
   * PREVIOUS frame (index-1).
   * 
   * From specification:
   * 3 - Restore to previous. The decoder is required to restore the area 
   *     overwritten by the graphic with what was there prior to rendering 
   *     the graphic.
   * 
   * This case is the most complex, we have to look for a previous 
   * frame with a Disposal Method set to 1 (do not dispose) and copy new frame 
   * above this one.
   * 
   * In that case, we may have to create true color resource, especially if the 
   * two resource has distinct Local Color Table.
   * 
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  public function _doFrameUpgradeDisposalMethod3( Image_AGif_Frame $frame , $index )
  {
    // look for a previous frame with Disposal Method to 1
    $previous_index = $index;
    while( $previous_index-- )
    {
      if( $this->_frames[$previous_index]->getDisposalMethod() === Image_AGif_Frame::DISPOSAL_NO )
      {
        // found!
        break;
      }
    }
    
    // not found :-(
    if( $previous_index < 0 ) return false;
    
    // attemp merge behind
    return $this->_doFrameUpgradeDisposalTransparentToBehind( $frame , $index , $previous_index );
  }
  
  /**
   * Merge a frame at specified index BEHIND current one.
   * 
   * Used in _doFrameUpgradeDisposalMethod1() and _doFrameUpgradeDisposalMethod3().
   * 
   * We may have to create true color resource, especially if the 
   * two resource has distinct Local Color Table.
   * 
   * @param \GDImage\Image_AGif_Frame $frame Current frame
   * @param integer $index Frame index in the pack
   * @param integer $behind_index Index of the frame to merge behind
   * @return boolean True if something has been done
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  public function _doFrameUpgradeDisposalTransparentToBehind( Image_AGif_Frame $frame , $index , $behind_index )
  {
    // clone behind frame resource to not alter it
    $behind_rsc = clone $this->_frames[$behind_index]->getResource();
    
    // at first, determine if we have to use a true color image or not
    
    // if the previous resource was true color, the current frame will 
    // also become true color
    $truecolor = $behind_rsc->isTrueColor();
    
    if( ! $truecolor )
    {
      // if one of them use local color table, we must compare colors to 
      // determine if we have to use true color resource
      if( isset($this->_gif_pack['Local Color Table'.$index]) || isset($this->_gif_pack['Local Color Table'.$behind_index]) )
      {
        if( isset($this->_gif_pack['Local Color Table'.$index]) )
        {
          // use local table
          $current_table = $this->_gif_pack['Local Color Table'.$index]['_raw'];
        }
        else
        {
          // use global table
          $current_table = $this->_gif_pack['Global Color Table']['_raw'];
        }

        if( isset($this->_gif_pack['Local Color Table'.$behind_index]) )
        {
          // use local table
          $behind_table = $this->_gif_pack['Local Color Table'.$behind_index]['_raw'];
        }
        else
        {
          // use global table
          $behind_table = $this->_gif_pack['Global Color Table']['_raw'];
        }
        
        // now we can compare the two table to determine how many distinct color we have
        // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 21
        // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 19
        
        // split tables every three chars give us array of colors representation
        $current_table = str_split( $current_table , 3 );
        $behind_table  = str_split( $behind_table , 3 );
        
        // a special thing must be done on transparent color
        // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
        
        // note that there is no need to do isset() because Disposal Method come from Graphic Control Exension
        if( $this->_gif_pack['Graphic Control Extension'.$index]['Packed']['Transparent Color Flag'] )
        {
          // just add an extra character to distinct this color from others
          $current_table[ $this->_gif_pack['Graphic Control Extension'.$index]['Transparent Color Index'] ] .= 'T';
        }
        
        // I'm not sure about that, but we may ignore the transparent color of the previous frame
        // because it should be replaced by the transparent color of the current frame
        
        // note that there is no need to do isset() because Disposal Method come from Graphic Control Exension
        // and if one image has Graphic Control Extension, all images must have Graphic Control Extension
        if( ! empty($this->_gif_pack['Graphic Control Extension'.$behind_index]['Packed']['Transparent Color Flag']) )
        {
          // just remove it, it will be replace by the transparent color of the current frame
          unset( $behind_table[ $this->_gif_pack['Graphic Control Extension'.$behind_index]['Transparent Color Index'] ] );
        }
//        if( ! empty($this->_gif_pack['Graphic Control Extension'.$behind_index]['Packed']['Transparent Color Flag']) )
//        {
//          // just add an extra character to distinct this color from others
//          $behind_table[$this->_gif_pack['Graphic Control Extension'.$behind_index]['Transparent Color Index']] .= 'T';
//        }
        
        // now fetch only keys to get all available colors (000 color may be duplicate to fit table size)
        $merge_table = array_flip( $current_table ) + array_flip( $behind_table );
        
        // if the merge table contains more that 256 colors, we must use true color
        $truecolor = count( $merge_table ) > 256;
      }
      else
      {
        // both table use Global Color Table but 
        // - if the current table has a Transparent Color
        // - and if the behind table has not a Transparent Color
        // - and if the behind table already has 256 colors
        // - and if the Transparent Color of current table represent a real 
        //   color of the behind table
        // we may loose color information on the new frame
        
//        // this case represents a minority of animated GIF (only pervert GIF) and
//        // provides more errors on other cases
//        if( $behind_rsc->countColors() > 255 && ( ! $behind_rsc->getTransparent() ) && $frame->getResource()->getTransparent() )
//        {
//          // we do not have enough place to add the frame transparent color
//          // to the merged resource, we have to use true color
//          $truecolor = true;
//        }
      }
    }
    
    // ok, we know if we have to use true color
    if( $truecolor )
    {
      $behind_rsc = $behind_rsc->toTrueColor();
    
      // enable alpha blending to merge transparent color (useless in palette color case)
      $behind_rsc->setMode( Resource_Abstract::MODE_ALPHABLENDING );
    }
    
    // we want to copy current frame resource to the new resource
    
    // note that helping _doFrameUpgradeLogicalScreen, the two resource have the same size
    // but we want to be sure that we copy only the expected area to the new location
    // this avoid frame without transparent color to overwrote entire area with background color
    
    $sw = $this->_gif_pack['Image Descriptor'.$index]['Image Width'];
    $sh = $this->_gif_pack['Image Descriptor'.$index]['Image Height'];
    $dx = $this->_gif_pack['Image Descriptor'.$index]['Image Left Position'];
    $dy = $this->_gif_pack['Image Descriptor'.$index]['Image Top Position'];
    
    // if copy failed, return false
    // do not worry about transparent color, GD will do the job if possible
    if( ! imagecopy( $behind_rsc->getGdResource() , $frame->getResource()->getGdResource() , $dx , $dy , $dx , $dy , $sw , $sh ) ) return false;
    
    // if the behind resource does not have transparent color
    // and if the frame has one
    // we must have a transparent color on the merged resource

    // note that in a few cases, we may loose color information
    // but these cases are so few that it is acceptable 
    if( ( ! $behind_rsc->getTransparent() ) && ( $transparent = $frame->getResource()->getTransparent() ) )
    {
      // if we fail, we fail
      $behind_rsc->setTransparent( $transparent );
    }
    
    // propagate modes
    $behind_rsc->setModes( $frame->getResource()->getModes() );
    
    // set new resource
    $frame->setResource( $behind_rsc );
    
    // That's all forks
    return true;
  }
}
