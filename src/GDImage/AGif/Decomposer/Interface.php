<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Interface for GIF decomposer.
 * 
 * Decomposer are used to create an array of \GDImage\Image_AGif_Frame
 * from a GIF pack. The pack comes from a \GDImage\AGif_Unpacker_Interface.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage agif
 * @author     Loops <pierrotevrard@gmail.com>
 * @interface
 */
interface AGif_Decomposer_Interface
{
  /**
   * Method to create an array of \GDImage\Image_AGif_Frame
   * from an AGIF pack.
   * 
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt
   * 
   * @param array $pack GIF pack from \GDImage\AGif_Unpacker_Interface
   * @return array Array of \GDImage\Image_AGif_Frame
   * @access public
   * @throws \GDImage\Exception_AGif
   * @static
   */
  static public function decompose( array $pack );
  
}
