# Animated GIF (AGIF) Notes


## Specifications

By convenience, when packing/unpackng AGIF, we try to use same key names that
names that can be found in specifications.  
see http://www.w3.org/Graphics/GIF/spec-gif89a.txt


## Delay Time

An important thing about AGIF delay time is that it can be 0, and the GIF
still animated: this is due to browser minimum frame delay and most of the 
time, this 0 delay is rounded up to 100ms.  
see http://nullsleep.tumblr.com/post/16524517190/animated-gif-minimum-frame-delay-browser-compatibility


## Graphic Control Extension
 
It is not clear in the specifications, but if there is multiple Image 
Descriptor, a Graphic Control Extension will always preceded each Image 
Descriptor.


## Disposal Method
 
An important thing about AGIF frame, is that, from the disposal method, 
the previous frame is visible behind transparent color of the current one. 
Most of AGIF use this ability to reduce the file size to minimum.

In our case, we want each frames at its visible state, with the previous frame 
behind if necessary. The ability to reduce filesize belong to the "AGif_Packer"
used to build the AGIF binary. 


## Local Color Table

True color GIF use Local Color Table for each frame with the Disposal Method 
ability. And because we want each frames at its visible state, we may have to 
convert all frames to true color.  
@see http://phil.ipal.org/tc.html


## Plain Text Extension

The Plain Text Extension may be misunderstood over all our AGif implementation.
The initial purpose was to render a text overlayed on the following image.
Whatever, it seems that nobody has implemented it, so we just ignore that.
@see http://giflib.sourceforge.net/whatsinagif/bits_and_bytes.html


## Application Extension NETSCAPE 2.0

This Application Extension should be present immediately after Global Color Table
but from the GIF specifications point of view, it is not mandatory.
Note that without this Application Extension, the GIF will loop only once.
Combined with the Disposal Method 1, this ability is used to generate true color 
GIF where all frames stay at their final state.
@see http://phil.ipal.org/tc.html