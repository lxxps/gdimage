<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration settings:
 * - "a_gif_factory_framer": \GDImage\AGif_Framer_Interface class name in use
 * - "a_gif_factory_unpacker": \GDImage\AGif_Unpacker_Interface class name in use
 * - "a_gif_factory_decomposer": \GDImage\AGif_Decomposer_Interface class name in use
 * - "a_gif_factory_composer": \GDImage\AGif_Composer_Interface class name in use
 * - "a_gif_factory_packer": \GDImage\AGif_Packer_Interface class name in use
 */
if( ! Config::hasAGifFactoryFramer() ) {
  Config::setAGifFactoryFramer( '\\GDImage\\AGif_Framer' );
}
if( ! Config::hasAGifFactoryUnpacker() ) {
  Config::setAGifFactoryUnpacker( '\\GDImage\\AGif_Unpacker' );
}
if( ! Config::hasAGifFactoryDecomposer() ) {
  Config::setAGifFactoryDecomposer( '\\GDImage\\AGif_Decomposer' );
}
if( ! Config::hasAGifFactoryComposer() ) {
  Config::setAGifFactoryComposer( '\\GDImage\\AGif_Composer' );
}
if( ! Config::hasAGifFactoryPacker() ) {
  Config::setAGifFactoryPacker( '\\GDImage\\AGif_Packer' );
}

/**
 * Class to manage animated GIF unpacker, packer, decomposer and composer.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage agif
 * @author     Loops <pierrotevrard@gmail.com>
 */
class AGif_Factory
{
  
  // animated GIF frame management
  
  /**
   * Create a blank frame instance and return it.
   * 
   * @param [...] Extra arguments
   * @return \GDImage\Image_AGif_Frame
   * @access public
   * @throws \GDImage\Exception_AGif
   * @static
   */
  static public function frame()
  {
    $args = func_get_args();
    return call_user_func_array( array( Config::getAGifFactoryFramer() , 'frame' ) , $args );
  }
  
  // processing animated GIF
  
  /**
   * Call unpack method.
   * 
   * @param string $filepath_or_binary Filepath or binary data
   * @param [...] Extra arguments
   * @return array GIF pack
   * @access public
   * @throws \GDImage\Exception_AGif
   * @static
   */
  static public function unpack( $filepath_or_binary )
  {
    $args = func_get_args();
    return call_user_func_array( array( Config::getAGifFactoryUnpacker() , 'unpack' ) , $args );
  }
  
  /**
   * Call decompose method.
   * 
   * @param array $pack GIF pack
   * @param [...] Extra arguments
   * @return array Array of Image_AGif_Frame
   * @access public
   * @throws \GDImage\Exception_AGif
   * @static
   */
  static public function decompose( array $pack )
  {
    $args = func_get_args();
    return call_user_func_array( array( Config::getAGifFactoryDecomposer() , 'decompose' ) , $args );
  }
  
  /**
   * Call compose method.
   * 
   * @param array $frames Array of Image_AGif_Frame
   * @param [...] Extra arguments
   * @return array GIF Pack
   * @access public
   * @throws \GDImage\Exception_AGif
   * @static
   */
  static public function compose( array $frames )
  {
    $args = func_get_args();
    return call_user_func_array( array( Config::getAGifFactoryComposer() , 'compose' ) , $args );
  }
  
  /**
   * Call pack method.
   * 
   * @param array $pack GIF Pack
   * @param [...] Extra arguments
   * @return string Binary data
   * @access public
   * @throws \GDImage\Exception_AGif
   * @static
   */
  static public function pack( array $pack )
  {
    $args = func_get_args();
    return call_user_func_array( array( Config::getAGifFactoryPacker() , 'pack' ) , $args );
  }
  
  // debug
  
  static public function ___GifPackRec( $data )
  {
    if( is_array( $data ) )
    {
      foreach( $data as &$value )
      {
        // recursive
        $value = static::___GifPackRec( $value );
      }
    }

    if( is_string( $data ) )
    {
      // do not encode from " " to "~"
      $data = htmlspecialchars( preg_replace_callback( '~[^ -\\~]~' , function( $m ){ return urlencode( $m[0] ); } , $data ) );
    }

    return $data;
  }
  
  static public function printGifPack()
  {
    $args = func_get_args();
    for( $i = 0, $imax = count( $args ); $i < $imax; $i++ )
    {
      print '<pre>';
      print_r( static::___GifPackRec( $args[$i] ) );
      print '</pre>';
    }
  }
}
