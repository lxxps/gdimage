<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Class to create binary data from a GIF pack.
 * For convenience, keys correspond to GIF specification Block name.
 * 
 * For performance reason, we do not validate every single piece of the pack.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage agif
 * @author     Loops <pierrotevrard@gmail.com>
 * @abstract
 * @implements \GDImage\AGif_Packer_Interface
 */
class AGif_Packer implements AGif_Packer_Interface
{
  
  /**
   * Function used to create binary data from a GIF pack.
   * 
   * For convenience, keys correspond to GIF specification Block name.
   * 
   * Each keys contain Block Data with the same name than GIF specification
   * with allways a "_raw" key to represent raw data of the block. 
   * This "_raw" key will not be validated so use it with caution.
   * 
   * Blocks that can be present more than one time are suffixed by an incremental
   * number used to identify the Block occurence (ie. "Image Descriptor3" for the 
   * fourth "Image Descriptor" Block found).
   * 
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt
   * 
   * @param array GIF pack
   * @return string Binary data
   * @access public
   * @throws \GDImage\Exception_AGif
   * @static
   * @implements \GDImage\AGif_Packer_Interface
   */
  static public function pack( array $pack )
  {
    // create instance
    $packer = new static();
    // invoke it and return binary data
    return $packer( $pack );
  }
  
  /**
   * Binary data.
   * 
   * @var string
   * @access protected
   */
  public $_binary;
  
  /**
   * Current GIF pack
   * 
   * @var array
   * @access protected
   */
  public $_gif_pack;

  /**
   * Function used to pack GIF array to binary data.
   * 
   * For convenience, keys correspond to GIF specification Block name.
   * 
   * Note that not some keys may contains a "_raw" value, this value will be 
   * prefereed in priority before any Data-Block generation.
   * 
   * Blocks that can be present more than one time are suffixed by an incremental
   * number used to identify the Block occurence (ie. "Image Descriptor3" for the 
   * fourth "Image Descriptor" Block found).
   * 
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt
   * 
   * @param array GIF pack
   * @return string Binary data
   * @access public
   * @throws \GDImage\Exception_AGif
   */
  public function __invoke( array $pack )
  {
    // reset binary
    $this->_binary = '';
    
    // assign pack
    $this->_gif_pack = $pack;
    
    // do the job
    
    // these three block allways comes at first, in this order
    $this->_packHeader();
    $this->_packLogicalScreenDescriptor();
    $this->_packGlobalColorTable();
    
    // for Application Extension, pack them after Global Color Table
    $i = 0;
    while( isset( $this->_gif_pack['Application Extension'.$i] ) )
    {
      $this->_packApplicationExtension( $i );
      $i++;
    }
    // Then extensions
    $i = 0;
    while( isset( $this->_gif_pack['Comment Extension'.$i] ) )
    {
      $this->_packCommentExtension( $i );
      $i++;
    }
//    // the plain text extension may be misunderstood over all our AGif implementation
//    // whatever, it seems than nobody want to implement it
//    // @see http://giflib.sourceforge.net/whatsinagif/bits_and_bytes.html
//    $i = 0;
//    while( isset( $this->_gif_pack['Plain Text Extension'.$i] ) )
//    {
//      $this->_packPlainTextExtension( $i );
//      $i++;
//    }
    
    // then pack frames
    $i = 0;
    while( isset( $this->_gif_pack['Image Descriptor'.$i] ) )
    {
      // graphic control must come first
      // keep in mind that if one image has Graphic Control Extension
      // all images must have Graphic Control Extension
      $this->_packGraphicControlExtension( $i );
      $this->_packImageDescriptor( $i );
      $this->_packLocalColorTable( $i );
      $this->_packTableBasedImageData( $i );
      
      $i++;
    }
    
    // pack trailer at the end
    $this->_packTrailer();
    
    return $this->_binary;
  }
  
  /**
   * Pack Header data.
   * Must be call at first.
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 17
   */
  public function _packHeader()
  {
    // raw data
    if( isset($this->_gif_pack['Header']['_raw']) )
    {
      $this->_binary .= $this->_gif_pack['Header']['_raw'];
    }
    else
    {
      // and pack
      $this->_binary .= pack( 'A3A3' 
        , $this->_gif_pack['Header']['Signature'] 
        , $this->_gif_pack['Header']['Version'] 
      );
    }
  }
  
  /**
   * Pack Logical Screen Descriptor data.
   * Must be call immediately after _packHeader().
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 18
   */
  public function _packLogicalScreenDescriptor()
  {
    // raw data
    if( isset($this->_gif_pack['Logical Screen Descriptor']['_raw']) )
    {
      $this->_binary .= $this->_gif_pack['Logical Screen Descriptor']['_raw'];
    }
    else
    {
      // shortcut
      $subpack = $this->_gif_pack['Logical Screen Descriptor'];
      
      // pack
      $this->_binary .= pack( 'vvCCC' 
        , $subpack['Logical Screen Width'] // 2 bytes
        , $subpack['Logical Screen Height'] // 2 bytes
        , // Packed, 1 byte
            ( ( $subpack['Packed']['Global Color Table Flag'] & 1 ) << 7 ) // write bit OXXXXXXX
          + ( ( $subpack['Packed']['Color Resolution'] & 7 ) << 4 ) // write bits XOOOXXXX
          + ( ( $subpack['Packed']['Sort Flag'] & 1 ) << 3 ) // write bit XXXXOXXX
          + ( ( $subpack['Packed']['Size of Global Color Table'] & 7 ) << 0 ) // write bits XXXXXOOO
        , $subpack['Background Color Index'] // 1 byte
        , $subpack['Pixel Aspect Ratio'] // 1 byte    
      );
    }
  }
  
  /**
   * Pack Global Color Table data.
   * Must be call after immediately _packLogicalScreenDescriptor().
   * 
   * If "_raw" is not set, each colors must be represented with an array
   * of three keys "Red", "Green", "Blue" in a "Colors".
   * 
   * These data are optional.
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 19
   */
  public function _packGlobalColorTable()
  {
    if( $this->_gif_pack['Logical Screen Descriptor']['Packed']['Global Color Table Flag'] )
    {
      if( isset($this->_gif_pack['Global Color Table']['_raw']) )
      {
        $this->_binary .= $this->_gif_pack['Global Color Table']['_raw'];
      }
      else
      {
        // shortcut to colors
        $colors = $this->_gif_pack['Global Color Table']['Colors'];
        
        $size = pow( 2 , $this->_gif_pack['Logical Screen Descriptor']['Packed']['Size of Global Color Table'] + 1 );
        
        $imax = count($colors);
        if( $imax > $size ) $imax = $size;
        
        for( $i = 0; $i < $imax; $i++ )
        {
          $this->_binary .= pack( 'CCC'
            , $colors[$i]['Red']
            , $colors[$i]['Green']
            , $colors[$i]['Blue']
          );
        }
        
        // we must care about table size to fill table with dummy color 
        
        // create dummy color
        $color = new Color();
        list( $r , $g , $b ) = $color->toArray();
        
        for( ; $i < $size; $i++ )
        {
          $this->_binary .= pack( 'CCC' , $r , $g , $b );
        }
        
        // done
      }
    }
  }
  
  
  /**
   * Pack Image Descriptor data.
   * Call in __invoke() loop after a Graphic Control Extension.
   * 
   * These data use index.
   * 
   * @param integer $index
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 20
   */
  public function _packImageDescriptor( $index )
  {
    // at first create key helping index
    $key = 'Image Descriptor'.$index;
    
    if( isset($this->_gif_pack[$key]['_raw']) )
    {
      $this->_binary .= $this->_gif_pack[$key]['_raw'];
    }
    else
    {
      // shortcut
      $subpack = $this->_gif_pack[$key];
      
      // pack
      $this->_binary .= pack( 'CvvvvC' 
        , 0x2C // Image Separator
        , $subpack['Image Left Position'] // 2 bytes
        , $subpack['Image Top Position'] // 2 bytes
        , $subpack['Image Width'] // 2 bytes
        , $subpack['Image Height'] // 2 bytes
              
        , // Packed, 1 byte
            ( ( $subpack['Packed']['Local Color Table Flag'] & 1 ) << 7 ) // write bit OXXXXXXX
          + ( ( $subpack['Packed']['Interlace Flag'] & 1 ) << 6 ) // write bit XOXXXXXX
          + ( ( $subpack['Packed']['Sort Flag'] & 1 ) << 5 ) // write bit XXOXXXXX
          + ( ( $subpack['Packed']['Reserved'] & 3 ) << 3 ) // write bits XXXOOXXX
          + ( ( $subpack['Packed']['Size of Local Color Table'] & 7 ) << 0 ) // write bits XXXXXOOO
      );
    }
  }
  
  
  /**
   * Pack Local Color Table data.
   * Must be call immediately after _packImageDescriptor().
   * 
   * If "_raw" is not set, each colors must be represented with an array
   * of three keys "Red", "Green", "Blue" in a "Colors".
   * 
   * These data are optional.
   * 
   * These data use index.
   * 
   * @param integer $index
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 21
   */
  public function _packLocalColorTable( $index )
  {
    if( $this->_gif_pack['Image Descriptor'.$index]['Packed']['Local Color Table Flag'] )
    {
      // at first create key helping index
      $key = 'Local Color Table'.$index;

      if( isset($this->_gif_pack[$key]['_raw']) )
      {
        $this->_binary .= $this->_gif_pack[$key]['_raw'];
      }
      else
      {
        // shortcut to colors
        $colors = $this->_gif_pack[$key]['Colors'];
        
        $size = pow( 2 , $this->_gif_pack['Image Descriptor'.$index]['Packed']['Size of Local Color Table'] + 1 );
        
        $imax = count($colors);
        if( $imax > $size ) $imax = $size;
        
        for( $i = 0; $i < $imax; $i++ )
        {
          $this->_binary .= pack( 'CCC'
            , $colors[$i]['Red']
            , $colors[$i]['Green']
            , $colors[$i]['Blue']
          );
        }
        
        // we must care about table size to fill table with dummy color 
        
        // create dummy color
        $color = new Color();
        list( $r , $g , $b ) = $color->toArray();
        
        for( ; $i < $size; $i++ )
        {
          $this->_binary .= pack( 'CCC' , $r , $g , $b );
        }
        
        // done
      }
    }
  }
  
  
  /**
   * Pack Table Based Image Data data.
   * Must be call immediately after _packLocalColorTable().
   * 
   * These data use index.
   * 
   * @param integer $index
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 22
   */
  public function _packTableBasedImageData( $index )
  {
    // at first create key helping index
    $key = 'Table Based Image Data'.$index;

    if( isset($this->_gif_pack[$key]['_raw']) )
    {
      $this->_binary .= $this->_gif_pack[$key]['_raw'];
    }
    else
    {
      // append LZW Minimum Code Size
      $this->_binary .= pack( 'C' 
        , $this->_gif_pack[$key]['LZW Minimum Code Size'] // 1 byte
      ); 
      
      // no, you do not want to apply LZW compression
      
      // then pack Data

      // Data Sub-blocks 
      // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 15
      
      // shortcut
      $data = $this->_gif_pack[$key]['Data'];
      
      // 255 (0xFF) is the maximum number of caracter that we can store in one 
      // Data Sub-block
      for( $i = 0xFF, $imax = strlen( $data ); $i <= $imax; $i += 0xFF )
      {
        // extract and pack this part
        $this->_binary .= "\xFF".substr( $data , $i - 0xFF , 0xFF ); // append Data AS IS
      }
      
      // now look for other caracters
      $k = $imax - $i - 0xFF;
      if( $k > 0 )
      {
        // extract and pack last part
        $this->_binary .= pack( 'C' 
          , $k // 1 byte
        ).substr( $data , $i - 0xFF ); // append Data AS IS
      }
      
      // Block Terminator 
      // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 16
      $this->_binary .= "\x00";
    }
  }
  
  
  /**
   * Pack Graphic Control Extension data.
   * Call in __invoke() loop BEFORE _packImageDescriptor().
   * 
   * These data are optional.
   * 
   * These data use index.
   * 
   * @param integer $index
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  public function _packGraphicControlExtension( $index )
  {
    // at first create key helping index
    $key = 'Graphic Control Extension'.$index;

    if( isset($this->_gif_pack[$key]) )
    {
      if( isset($this->_gif_pack[$key]['_raw']) )
      {
        $this->_binary .= $this->_gif_pack[$key]['_raw'];
      }
      else
      {

        // Graphic Control Extension
        // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23

        // Graphic Control Extension
        // - a special Graphic Control Block (fixed size to 4 bytes)
        
        // shortcut
        $subpack = $this->_gif_pack[$key];

        // pack first block
        $this->_binary .= pack( 'CCCCvCC' 
          , 0x21 // Extension Introducer, 1 byte
          , 0xF9 // Graphic Control Label, 1 byte
          , 4 // Block Size, fixed to 4, 1 byte
                
          , // Packed, 1 byte
              ( ( $subpack['Packed']['Reserved'] & 1 ) << 5 ) // write bits OOOXXXXX
            + ( ( $subpack['Packed']['Disposal Method'] & 7 ) << 2 ) // write bits XXXOOOXX
            + ( ( $subpack['Packed']['User Input Flag'] & 1 ) << 1 ) // write bit XXXXXXOX
            + ( ( $subpack['Packed']['Transparent Color Flag'] & 1 ) << 0 ) // write bit XXXXXXXO
                
          , $subpack['Delay Time'] // 2 bytes
          , $subpack['Transparent Color Index'] // 1 byte
                
          , 0 // Block terminator, 1 byte
        );
      }
    }
  }
  
  
  /**
   * Pack Comment Extension data.
   * 
   * These data use index.
   * 
   * @param integer $index
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 24
   */
  public function _packCommentExtension( $index )
  {
    // at first create key helping index
    $key = 'Comment Extension'.$index;

    if( isset($this->_gif_pack[$key]['_raw']) )
    {
      $this->_binary .= $this->_gif_pack[$key]['_raw'];
    }
    else
    {
    
      // Comment Extension
      // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 24

      // Comment Extension is composed by
      // - 0 to N Data Sub-Blocks
      // - a Block Terminator ("\x00")
    
      // pack first block
      $this->_binary .= pack( 'CC' 
        , 0x21 // Extension Introducer, 1 byte
        , 0xFE // Comment Label, 1 byte
      ); 
      
      // then pack Data

      // Data Sub-blocks 
      // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 15
      
      // shortcut
      $data = $this->_gif_pack[$key]['Data'];
      
      // 255 (0xFF) is the maximum number of caracter that we can store in one 
      // Data Sub-block
      for( $i = 0xFF, $imax = strlen( $data ); $i <= $imax; $i += 0xFF )
      {
        // extract and pack this part
        $this->_binary .= "\xFF".substr( $data , $i - 0xFF , 0xFF ); // append Data AS IS
      }
      
      // now look for other caracters
      $k = $imax - $i - 0xFF;
      if( $k > 0 )
      {
        // extract and pack last part
        $this->_binary .= pack( 'C' 
          , $k // 1 byte
        ).substr( $data , $i - 0xFF ); // append Data AS IS
      }
      
      // Block Terminator 
      // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 16
      $this->_binary .= "\x00";
    }
  }
  
  
  /**
   * Pack Plain Text Extension data.
   * 
   * These data use index.
   * 
   * @param integer $index
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 25
   */
  public function _packPlainTextExtension( $index )
  {
    // at first create key helping index
    $key = 'Plain Text Extension'.$index;

    if( isset($this->_gif_pack[$key]['_raw']) )
    {
      $this->_binary .= $this->_gif_pack[$key]['_raw'];
    }
    else
    {

      // Plain Text Extension
      // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 25

      // Plain Text Extension is composed by
      // - a special Plain Text Block (fixed size to 12 bytes)
      // - 0 to N Data Sub-Blocks
      // - a Block Terminator ("\x00")
    
      // shortcut 
      $subpack = $this->_gif_pack[$key];
      
      // pack first block
      $this->_binary .= pack( 'CCCvvvvCCCC' 
        , 0x21 // Extension Introducer, 1 byte
        , 0x01 // Plain Text Label, 1 byte
        , 12 // Block Size, fixed to 12, 1 byte
              
        , $subpack['Text Grid Left Position'] // 2 bytes
        , $subpack['Text Grid Left Position'] // 2 bytes
        , $subpack['Text Grid Width'] // 2 bytes
        , $subpack['Text Grid Height'] // 2 bytes
        , $subpack['Character Cell Width'] // 1 byte
        , $subpack['Character Cell Height'] // 1 byte
        , $subpack['Text Foreground Color Index'] // 1 byte
        , $subpack['Text Background Color Index'] // 1 byte
      ); 
      
      // then pack Data

      // Data Sub-blocks 
      // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 15
      
      // shortcut
      $data = $this->_gif_pack[$key]['Data'];
      
      // 255 (0xFF) is the maximum number of caracter that we can store in one 
      // Data Sub-block
      for( $i = 0xFF, $imax = strlen( $data ); $i <= $imax; $i += 0xFF )
      {
        // extract and pack this part
        $this->_binary .= "\xFF".substr( $data , $i - 0xFF , 0xFF ); // append Data AS IS
      }
      
      // now look for other caracters
      $k = $imax - $i - 0xFF;
      if( $k > 0 )
      {
        // extract and pack last part
        $this->_binary .= pack( 'C' 
          , $k // 1 byte
        ).substr( $data , $i - 0xFF ); // append Data AS IS
      }
      
      // Block Terminator 
      // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 16
      $this->_binary .= "\x00";
    }
  }
  
  
  /**
   * Pack Application Extension data.
   * Call immediately AFTER _packGlobalColorTable().
   * 
   * These data use index.
   * 
   * @param integer $index
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 26
   */
  public function _packApplicationExtension( $index )
  {
    // at first create key helping index
    $key = 'Application Extension'.$index;

    if( isset($this->_gif_pack[$key]['_raw']) )
    {
      $this->_binary .= $this->_gif_pack[$key]['_raw'];
    }
    else
    {
      // Application Extension
      // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 26

      // Application Extension is composed by
      // - a special Application Block (fixed size to 12 bytes)
      // - 0 to N Data Sub-Blocks
      // - a Block Terminator ("\x00")

      // shortcut 
      $subpack = $this->_gif_pack[$key];
      
      // pack first block
      $this->_binary .= pack( 'CCCA8A3' 
        , 0x21 // Extension Introducer, 1 byte
        , 0xFF // Application Label, 1 byte
        , 11 // Block Size, fixed to 11, 1 byte
              
        , $subpack['Application Identifier'] // 8 bytes
        , $subpack['Application Authentication Code'] // 3 bytes
      ); 
      
      // it seems that some application does not follow Data Sub-Blocks mecanism
      // especially "XMP DataXMP"
    
      $method = '_packApplicationExtensionFor'.Config::camelize( $subpack['Application Identifier'].$subpack['Application Authentication Code'] );
    
      if( method_exists( $this , $method ) )
      {
        // apply this method
        $this->$method( $index );
      }
      else
      {
        // do it standard way

        // Data Sub-blocks 
        // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 15

        // shortcut
        $data = $this->_gif_pack[$key]['Data'];

        // 255 (0xFF) is the maximum number of caracter that we can store in one 
        // Data Sub-block
        for( $i = 0xFF, $imax = strlen( $data ); $i <= $imax; $i += 0xFF )
        {
          // extract and pack this part
          $this->_binary .= "\xFF".substr( $data , $i - 0xFF , 0xFF ); // append Data AS IS
        }

        // now look for other caracters
        $k = $imax - $i - 0xFF;
        if( $k > 0 )
        {
          // extract and pack last part
          $this->_binary .= pack( 'C' 
            , $k // 1 byte
          ).substr( $data , $i - 0xFF ); // append Data AS IS
        }

        // Block Terminator 
        // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 16
        $this->_binary .= "\x00";
      }
    }
  }
  
  /**
   * Pack Application Extension data for Application XMP Data XMP.
   * Call in _packApplicationExtension() when a XMP Data XMP application has 
   * been detected.
   * 
   * These data use last Application Extension index
   * 
   * @param integer $index
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://wwwimages.adobe.com/content/dam/Adobe/en/devnet/xmp/pdfs/XMPSpecificationPart3.pdf
   */
  public function _packApplicationExtensionForXmpDataXmp( $index )
  {
    // at first create key helping index
    $key = 'Application Extension'.$index;

    // XMP Data
    // @see http://wwwimages.adobe.com/content/dam/Adobe/en/devnet/xmp/pdfs/XMPSpecificationPart3.pdf

    // we do not care about all Adobe shits
    // just focus on Data that MUST contains the magic trailer
    
    // we just add the Block Terminator

    $this->_binary .= $this->_gif_pack[$key]['Data']."\x00";
  }
  
  
  /**
   * Pack Application Extension data for Application NETSCAPE 2.0.
   * Call in _packApplicationExtension() when a NETSCAPE 2.0 application has 
   * been detected.
   * 
   * These data use last Application Extension index
   * 
   * @param integer $index
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://en.wikipedia.org/wiki/Graphics_Interchange_Format#Animated_GIF
   */
  public function _packApplicationExtensionForNetscape20( $index )
  {
    // at first create key helping index
    $key = 'Application Extension'.$index;
    
    // Application Extension Data for NETSCAPE 2.0 are composed like this:
    // - Block Size, 1 byte, fixed to 3
    // - Sub-Block Index, 1 byte
    // - Number of repeatitions, 1 byte
    // @see http://en.wikipedia.org/wiki/Graphics_Interchange_Format#Animated_GIF

    // pack block
    $this->_binary .= pack( 'CCvC' 
      , 3 // Block Size, fixed to 3, 1 byte

      , $this->_gif_pack[$key]['Sub-Block Index'] // 1 byte
      , $this->_gif_pack[$key]['Number of Repetitions'] // 2 bytes
            
      , 0 // Block Terminator
    ); 
    
  }
  
  /**
   * Pack Trailer data.
   * Call at the end.
   * 
   * @param none
   * @return void
   * @access protected
   * @throws \GDImage\Exception_AGif
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  public function _packTrailer()
  {
    // Trailer
    // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 27
    
    $this->_binary .= "\x3B";
  }
}
