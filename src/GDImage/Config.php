<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration setting:
 * - "temporary_directory": Directory where to store temporary files, in some 
 *   special case, we may want to be able to manage it
 * - "temporary_prefix": Prefix to use on temporary files creation
 */
if( ! Config::hasTemporaryDirectory() ) {
  Config::setTemporaryDirectory( sys_get_temp_dir() );
}
if( ! Config::hasTemporaryPrefix() ) {
  Config::setTemporaryPrefix( 'GDI' );
}

/**
 * Class to manage GDImage configuration.
 * 
 * You can add any configuration and access it easly helping magic methods.
 * 
 * Set:
 * \GDImage\Config::getInstance()->my_custom_setting = 'foo';
 * \GDImage\Config::getInstance()->setMyCustomSetting( 'foo' );
 * \GDImage\Config::setMyCustomSetting( 'foo' );
 * 
 * Get:
 * $c = \GDImage\Config::getInstance()->my_custom_setting;
 * $c = \GDImage\Config::getInstance()->getMyCustomSetting(();
 * $c = \GDImage\Config::getMyCustomSetting(();
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage config
 * @author     Loops <pierrotevrard@gmail.com>
 * @version    SVN: $Id: \GDImage\Config.class.php 170 2010-08-26 11:23:35Z loops $
 */
class Config
{
  /**
   * Current \GDImage\Config instance.
   *
   * @var \GDImage\Config Current instance
   * @access protected
   */
  public static $_instance;
	
  /**
   * Configuration array.
   *
   * @var array
   * @access protected
   */
  public $_config = array();
  
  /**
   * Method to retrieve \GDImage\Config instance.
   *
   * @param none
   * @return \GDImage\Config Current instance
   * @access public
   * @static
   */ 
  public static function getInstance()
  {
  	if( ! static::$_instance )
  	{
  		static::$_instance = new static();
  	}
  	return static::$_instance;
  }
  
  /**
   * Require helper file.
   * Will make function gd_image() available.
   * 
   * @see inc/helper.inc.php
   *
   * @param none
   * @return void
   * @access public
   * @static
   */ 
  public static function helper()
  {
    include_once dirname(__FILE__).'/inc/helper.inc.php';
  }
  
  /**
   * Require mbstring_overload file.
   * This file is used to create \GDImage string function when
   * mbstring.func_overlaod is enabled.
   * 
   * @see inc/mbstring_overload.inc.php
   *
   * @param none
   * @return void
   * @access public
   * @static
   */ 
  public static function mbstring_overload()
  {
    include_once dirname(__FILE__).'/inc/mbstring_overload.inc.php';
  }
  
  /**
   * Instance constructor.
   * Protected constructor to force use of getInstance() static method.
   * Keep in mind POOP pattern: the constructor should be protected, 
   * but it is not.
   *
   * @param none
   * @return void
   * @access protected
   */ 
  public function __construct()
  {
  }
  
  /**
   * Method to camelize a word for GDImage configuration.
   * foo-bar => FooBar
   *
   * @param string $str String
   * @return string $str Camelized string
   * @access public
   * @static
   */ 
  public static function camelize( $str )
  {
    // apply camelize
    $str = preg_replace_callback( '~(^|[^0-9a-z]+)([0-9a-z])([0-9a-z]*)~i' , function( array $matches ){
  	  return strtoupper($matches[2]) . strtolower($matches[3]);
    } , $str );
    // remove unexpected characters
  	return preg_replace( '~[^0-9a-z]+~i' , '' , $str );
  }
  
  /**
   * Method to dash a word for GDImage configuration.
   * FooBar => foo-bar
   *
   * @param string $str String
   * @return string $str Dashed string
   * @access public
   * @static
   */ 
  public static function dash( $str )
  {
    // apply dash
  	$str = preg_replace_callback( '~([0-9A-Z])([0-9a-z]*)~' , function( array $matches ) {
      return '-' . strtolower($matches[1].$matches[2]);
    } , $str );
    // remove unexpected characters
  	return trim( preg_replace( '~[^0-9a-z]+~' , '-' , $str ) , '-' );
  }

  /**
   * Method to underscore a word for GDImage configuration.
   * FooBar => foo_bar
   *
   * @param string $str String
   * @return string $str Dashed string
   * @access public
   * @static
   */
  public static function underscore( $str )
  {
    // apply underscore
  	$str = preg_replace_callback( '~([0-9A-Z])([0-9a-z]*)~' , function( array $matches ) {
      return '_' . strtolower($matches[1].$matches[2]);
    } , $str );
    // remove unexpected characters
  	return trim( preg_replace( '~[^0-9a-z]+~' , '_' , $str ) , '_' );
  }
  
  /**
   * Magic method to emulate static access to configuration attributes.
   *
   * @param string $method Method name
   * @param array $args Arguments array
   * @access public
   * @static
   */ 
  public static function __callStatic( $method , $args )
  {
  	return call_user_func_array( array( static::getInstance() , $method ) , $args );
  }
  
  /**
   * Magic method to retrieve configuration attribute.
   *
   * @param string $proprety Proprety name
   * @return mixed Proprety value
   * @access public
   */
  public function __get( $proprety )
  {
    $cam = static::camelize( $proprety );
  	if( method_exists( $this , 'get'.$cam ) )
  	{
  		return $this->{'get'.$cam}();
  	}
    // note that we use array_key_exists instead of 
    // isset to allow null configuration
  	elseif( array_key_exists( $proprety , $this->_config ) )
  	{
  		return $this->_config[$proprety];
  	}
  	else
  	{
  		// Restore Notice
  		trigger_error( sprintf( 'Undefined property: %s::$%s' , get_class( $this ) , $proprety ) , E_USER_NOTICE );
  	}
  }
  
  /**
   * Magic method to assign configuration attribute.
   *
   * @param string $proprety Proprety name
   * @param mixed $value Proprety value
   * @return void
   * @access public
   */
  public function __set( $proprety , $value )
  {
    $cam = static::camelize( $proprety );
  	if( method_exists( $this , 'set'.$cam ) )
  	{
  		$this->{'set'.$cam}( $value );
  	}
  	else
  	{
  		$this->_config[$proprety] = $value;
  	}
  }
  
  /**
   * Magic method to detect configuration attribute.
   *
   * @param string $proprety Proprety name
   * @return boolean
   * @access public
   */
  public function __isset( $proprety )
  {
    $cam = static::camelize( $proprety );
  	if( method_exists( $this , 'has'.$cam ) )
  	{
  		$this->{'has'.$cam}();
  	}
  	else
  	{
      // note that we use array_key_exists instead of 
      // isset to allow null configuration
  		return array_key_exists( $proprety , $this->_config );
  	}
  }
  
  /**
   * Magic method to emulate public methods to assign and
   * retrieve configuration attributes.
   *
   * @param string $method Method name
   * @param array $args Arguments array
   * @return mixed
   * @access public
   */
  public function __call( $method , $args )
  {
  	if( strpos( $method , 'set' ) === 0 )
  	{
  		// Magic call to __set()
  		$this->{static::underscore( substr( $method , 3 ) )} = $args[0];
  	}
  	elseif( strpos( $method , 'get' ) === 0 )
  	{
  		// Magic call to __get()
  		return $this->{static::underscore( substr( $method , 3 ) )};
  	}
  	elseif( strpos( $method , 'has' ) === 0 )
  	{
  		// Magic call to __isset()
  		return isset( $this->{static::underscore( substr( $method , 3 ) )} );
  	}
  	else
  	{
  		// Restore Fatal Error
  		trigger_error( sprintf( 'Call to undefined method %s::%s()' , get_class( $this ) , $method ) , E_USER_ERROR );
  	}
  }
  
}

// we want to make sure str functions are protected
// by calling it here, it will be done over 90% of library files
Config::mbstring_overload();
