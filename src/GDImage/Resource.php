<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * A shortcut to \GDImage\Resource_Abstract.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage resource
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends    \GDImage\Resource_Abstract
 * @abstract
 */
abstract class Resource extends Resource_Abstract
{
}
