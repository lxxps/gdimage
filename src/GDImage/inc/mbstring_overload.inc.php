<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Prevent issues when "mbstring.func_overload" is done on string functions.
 * @see http://php.net/manual/en/mbstring.overload.php
 * 
 * Because we never use qualified namespace for global PHP functions, during 
 * execution, PHP looks on \GDImage namespace THEN on global namespace.
 * @see http://php.net/manual/en/language.namespaces.rules.php
 * 
 * That behavior gives us ability to create the functions in \GDImage namespace 
 * only if they have been overwrote on global namespace and unqualified calls to 
 * str*() will be ok over entire \GDImage namespace.
 */

// documentation claims to never do that, but we cannot underestimate 
// the human's stupidity
if( ini_get( 'mbstring.func_overload' ) & 2 )
{
  // If eval() is the answer, you're almost certainly asking the
  // wrong question. -- Rasmus Lerdorf, BDFL of PHP
  
  // Ok, so I have to write hundred lines of codes...
  // Fortunately, I'm a copy/paste master.
  
  // strlen
  function strlen()
  {
    // get previous encoding
    $enc = mb_internal_encoding();
    // change it to ASCII
    mb_internal_encoding( 'ASCII' );
    // get arguments 
    $args = func_get_args();
    // call global function
    $ret = call_user_func_array( '\\strlen' , $args );
    // restore previous encoding
    mb_internal_encoding( $enc );
    // get result back
    return $ret;
  }
  
  // strpos
  function strpos()
  {
    // get previous encoding
    $enc = mb_internal_encoding(); 
    // change it to ASCII
    mb_internal_encoding( 'ASCII' );
    // get arguments 
    $args = func_get_args();
    // call global function
    $ret = call_user_func_array( '\\strpos' , $args );
    // restore previous encoding
    mb_internal_encoding( $enc );
    // get result back
    return $ret;
  }
  
  // strrpos
  function strrpos()
  {
    // get previous encoding
    $enc = mb_internal_encoding(); 
    // change it to ASCII
    mb_internal_encoding( 'ASCII' );
    // get arguments 
    $args = func_get_args();
    // call global function
    $ret = call_user_func_array( '\\strrpos' , $args );
    // restore previous encoding
    mb_internal_encoding( $enc );
    // get result back
    return $ret;
  }
  
  // substr
  function substr()
  {
    // get previous encoding
    $enc = mb_internal_encoding(); 
    // change it to ASCII
    mb_internal_encoding( 'ASCII' );
    // get arguments 
    $args = func_get_args();
    // call global function
    $ret = call_user_func_array( '\\substr' , $args );
    // restore previous encoding
    mb_internal_encoding( $enc );
    // get result back
    return $ret;
  }
  
  // strtolower
  function strtolower()
  {
    // get previous encoding
    $enc = mb_internal_encoding(); 
    // change it to ASCII
    mb_internal_encoding( 'ASCII' );
    // get arguments 
    $args = func_get_args();
    // call global function
    $ret = call_user_func_array( '\\strtolower' , $args );
    // restore previous encoding
    mb_internal_encoding( $enc );
    // get result back
    return $ret;
  }
  
  // strtoupper
  function strtoupper()
  {
    // get previous encoding
    $enc = mb_internal_encoding(); 
    // change it to ASCII
    mb_internal_encoding( 'ASCII' );
    // get arguments 
    $args = func_get_args();
    // call global function
    $ret = call_user_func_array( '\\strtoupper' , $args );
    // restore previous encoding
    mb_internal_encoding( $enc );
    // get result back
    return $ret;
  }
  
  // stripos
  function stripos()
  {
    // get previous encoding
    $enc = mb_internal_encoding(); 
    // change it to ASCII
    mb_internal_encoding( 'ASCII' );
    // get arguments 
    $args = func_get_args();
    // call global function
    $ret = call_user_func_array( '\\stripos' , $args );
    // restore previous encoding
    mb_internal_encoding( $enc );
    // get result back
    return $ret;
  }
  
  // strripos
  function strripos()
  {
    // get previous encoding
    $enc = mb_internal_encoding(); 
    // change it to ASCII
    mb_internal_encoding( 'ASCII' );
    // get arguments 
    $args = func_get_args();
    // call global function
    $ret = call_user_func_array( '\\strripos' , $args );
    // restore previous encoding
    mb_internal_encoding( $enc );
    // get result back
    return $ret;
  }
  
  // strstr
  function strstr()
  {
    // get previous encoding
    $enc = mb_internal_encoding(); 
    // change it to ASCII
    mb_internal_encoding( 'ASCII' );
    // get arguments 
    $args = func_get_args();
    // call global function
    $ret = call_user_func_array( '\\strstr' , $args );
    // restore previous encoding
    mb_internal_encoding( $enc );
    // get result back
    return $ret;
  }
  
  // stristr
  function stristr()
  {
    // get previous encoding
    $enc = mb_internal_encoding(); 
    // change it to ASCII
    mb_internal_encoding( 'ASCII' );
    // get arguments 
    $args = func_get_args();
    // call global function
    $ret = call_user_func_array( '\\stristr' , $args );
    // restore previous encoding
    mb_internal_encoding( $enc );
    // get result back
    return $ret;
  }
  
  // strrchr
  function strrchr()
  {
    // get previous encoding
    $enc = mb_internal_encoding(); 
    // change it to ASCII
    mb_internal_encoding( 'ASCII' );
    // get arguments 
    $args = func_get_args();
    // call global function
    $ret = call_user_func_array( '\\strrchr' , $args );
    // restore previous encoding
    mb_internal_encoding( $enc );
    // get result back
    return $ret;
  }
  
  // substr_count
  function substr_count()
  {
    // get previous encoding
    $enc = mb_internal_encoding(); 
    // change it to ASCII
    mb_internal_encoding( 'ASCII' );
    // get arguments 
    $args = func_get_args();
    // call global function
    $ret = call_user_func_array( '\\substr_count' , $args );
    // restore previous encoding
    mb_internal_encoding( $enc );
    // get result back
    return $ret;
  }
}