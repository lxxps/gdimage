<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

// GDImage library Helper

// global namespace
namespace {

  /**
   * gd_image() function, helps to generate pictures from many different ways.
   * 
   * @param string $stuff Image to import
   * @param mixed $export Export parameters
   * @param [mixed] $transform Transformation to apply
   * @param [mixed] $failure Callback to execute or value to return on failure
   * @return mixed Expected stuff or failure
   * 
   * 
   * $stuff can be anything handle by Factory::import() method: a file path, 
   * binary data, base64 data, data URI, Image instance... 
   * The difference with Factory::import() is that there is no MIME Type cast.
   * 
   * 
   * $export can be anything used by Factory::export() method: a file path,
   * a driver key or an array of export parameters.
   * The difference with Factory::export() is that there is no MIME Type cast.
   * 
   * Compare to Factory::export(), $export has extra behaviors:
   * - a null $export will result on a file export
   * - if $export is a string starting with "-" or "_", this string will be 
   *   considered as a suffix for a file export
   * - if $stuff is a file path and if a file export does not specify a 
   *   file name, file export will use same file name
   * - if $stuff is not a file path and if a file export does not specify a 
   *   file name, file export will use MD5 hash of $stuff as file name
   * - if $transform is not null and if a file export does not specify a 
   *   file name or a suffix, the transformation key - or the underscored 
   *   class name - prefixed by an underscore "_" will be used as suffix for the 
   *   file name
   * - if $stuff is a file path and if file export does not specify a directory,
   *   file export will use same directory
   * - if $stuff is not a file path and if file export does not specify a 
   *   directory, file export will use system temporary directory
   * - if $stuff is a file path with creation date, and if file export result 
   *   to a file that already exists, the new file will not be generated if its 
   *   creation date is higher than $stuff
   * 
   * 
   * $transform can be: a registered transformation key, a transformation class 
   * name (fully declared or in \GDImage namespace), a \GDImage transformation 
   * class suffix (\GDImage\Transform_*) or a transformation instance.
   * 
   * Note that transformation class name that do not start with "\" may correspond
   * to a \GDImage transformation instead of a global transformation.
   * 
   * 
   * If everything goes fine, this function will return expected result: the 
   * final filepath, the data URI...
   * 
   * If not, a E_USER_WARNING error will be triggered with explaination about 
   * what went wrong and false will be returned. 
   * 
   * This behavior can be customize helping $failure argument:
   * - if `$failure` is callable, it will be executed and its result will be sent 
   *   back by the `gd_image()` helper - arguments of this callback are: the 
   *   \GDImage\Exception instance thrown, $stuff argument, $export argument and $transform
   *   argument;
   * - if `$failure` is not callable and is not null, it will be used as value to 
   *   return in case of failure - the error will still triggered.
   * 
   */
  function gd_image( $stuff , $export , $transform = null , $failure = null )
  {
    return \GDImage\gd_image( $stuff , $export , $transform , $failure );
  }

} // end of global namespace


// GDImage namespace
namespace GDImage {
  
  // here comes real things
  function gd_image( $stuff , $export , $transform = null , $failure = null )
  {
    try
    {
      // init some stuff
      $dirname = null;
      $filename = null;
      $filesuffix = null; // in a first time, will be updated with transformation
      $fileext = null;
      
      
      //---------- transform lookup
      
      if( $transform !== null )
      {
        // also initialize suffix
        if( is_string( $transform ) )
        {
          // look for transformation key
          try
          {
            $transform = Transform_Collection::get( $transform );
            // if we successfully found a transformation heping this key,
            // update suffix to this transformation key
            $filesuffix = '_'.Config::underscore( func_get_arg( 2 ) );
          }
          catch( Exception_Transform $e )
          {
            if( $e->getCode() !== 5000 )
            {
              // not the "transformation does not exist" code, re-throw
              throw $e;
            }

            // look for class name
            if( $transform{0} === '\\' )
            {
              // fully qualified
              if( class_exists( $transform , true ) )
              {
                $transform = new $transform();
              }
            }
            else
            {
              // look for gd suffix first
              if( class_exists( '\\GDImage\\Transform_'.$transform , true ) )
              {
                $transform = '\\GDImage\\Transform_'.$transform;
                $transform = new $transform();
              }
              elseif( class_exists( '\\GDImage\\'.$transform , true ) )
              {
                $transform = '\\GDImage\\'.$transform;
                $transform = new $transform();
              }
            }
          }
        }
        
        // if the user submit anything else, do a warning
        if( ! is_object($transform) )
        {
          throw new Exception( array( __FUNCTION__ , func_get_arg( 2 ) ) , 9130 ); 
        }
        
        // if suffix is empty, fill it
        if( ! $filesuffix )
        {
          $filesuffix = '_'.Config::underscore( get_class( $transform ) );
        }
      }
      
      
      //---------- file export smarties
      
      // is it a file?
      $pathinfo = array( 'dirname' => null , 'filename' => null , 'extension' => null );
      if( Factory_ImportMorpher_File::detect( $stuff ) ) $pathinfo = pathinfo( $stuff ) + $pathinfo;
        
      // use same directory or temp directory
      $dirname = $pathinfo['dirname'] ? $pathinfo['dirname'].'/' : Config::getTemporaryDirectory().'/';
      // use filename or md5 hash
      $filename = $pathinfo['filename'] ? $pathinfo['filename'] : md5( $stuff );
      // use extension, if provided, or determine extension
      // extension can be empty string if $stuff ends with a dot
      $fileext = empty($pathinfo['extension']) ? null : '.'.$pathinfo['extension'];
//      // if there is an extension, even empty one, undo filesuffix
//      if( isset($pathinfo['extension']) ) $filesuffix = null;
      
      // transform export to expected stuff
      if( $export === null )
      {
        // rebuild export
        $export = $dirname.'/'.$filename.$filesuffix.$fileext;
      }
      // look for directory export
      elseif( is_string( $export ) && @is_dir( $export ) )
      {
        $dirname = rtrim( $export , '/\\' ).'/';
        // rebuild export
        $export = $dirname.$filename.$filesuffix.$fileext;
      }
      // we should never encounter binary or base64 data starting with these characters
      // to be safe, the suffix must not contains any "/" or "\"
      elseif( is_string( $export ) 
       && ( $export{0} === '-' || $export{0} === '_' )
       && strpos( $export , '/' ) === false 
       && strpos( $export , '\\' ) === false )
      {
        // update suffix
        $filesuffix = $export;
        // rebuild export
        $export = $dirname.$filename.$filesuffix.$fileext;
      }
      
      
      //---------- file time lookup
      
      if( is_string( $export ) && Factory_ImportMorpher_File::detect( $stuff ) && Factory_ExportDriver_File::detect( $export ) ) // from file to file
      {
        // to enable filetime lookup, we must know the full path to export
        // @see Factory_ExportDriver_File::__invoke()
        // @see Factory_ImportMorpher_File::getMimeType()
        // note that if we cannot determine an extension, we will never be able
        // to compare time
        if( ( ! pathinfo( $export , PATHINFO_EXTENSION ) ) && ( $mime = MimeTypeGuesser_Collection::fromFile( $stuff ) ) )
        {
          // we can determine a MIME Type from this extension
          $guesser = new MimeTypeGuesser_FromFile_Extension();

          // do reverse look up
          $ext = $guesser->reverse( $mime );

          // complete $export (avoid double dot)
          $export = rtrim( $export , '.' ).'.'.$ext;
        }
        
        // if export is a file it will have filemtime
        // is_file() will validate stat family
        if( @is_file( $export ) )
        {
          $export_time = filemtime( $export );
          
          $import_time = null;
          
          // we want to look on filetime
          // enable HTTP lookup
          
          // is_file() will validate stat family
          if( @is_file( $stuff ) )
          {
            $import_time = filemtime( $stuff );
          }
          else
          {
            $parse = parse_url( $stuff );
            if( isset($parse['scheme']) && isset($parse['host']) 
                && $parse['scheme'] === 'http' || $parse['scheme'] === 'https' )
            {
              // do it with get_headers
              $headers = get_headers( $stuff , 1 );
              // also look on status
              if( isset( $headers['Last-Modified'] ) && isset( $headers[0] ) && ( strpos( $headers[0] , '200' ) !== false ) )
              {
                $import_time = strtotime( $headers['Last-Modified'] );
              }
            }
          }
          
          // hope that we will not have any file from 1st January 1970
          if( $import_time && ( $export_time > $import_time ) )
          {
            // DO NOT PROCESS
            return $export;
          }
        }
      }

      
      //---------- final
      
      // in other case, there is nothing special to do
      $image = Factory::import( $stuff );
      if( $transform ) $image->apply( $transform );
      return Factory::export( $image , $export );
      
      // I wanna runaway, just U and I, I, I, I, I
    }
    catch( Exception $e )
    {
      if( is_callable( $failure ) )
      {
        // fetch back argument
        // note that in PHP7, arguments will get modified state, but we do not 
        // care of that for now
        $args = func_get_args();
        // remove failure callback
        array_pop( $args );
        // prepend exception
        array_unshift( $args , $e );
        // callback
        return call_user_func_array( $failure , $args );
      }
      
      // not callable, trigger error
      trigger_error( $e->getMessage() , E_USER_WARNING );
      
      // return false or failure argument
      if( $failure !== null ) return $failure;
      
      return false;
    }
  }

} // end of GDImage namespace
