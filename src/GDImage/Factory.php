<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration settings:
 * - "default_mime_type": MIME Type to use on new picture
 * - "factory_image_interfaces": available \GDImage\Image_Interface class names;
 *   used on instanciation to determine best \GDImage/Image_Interface to use for 
 *   a given MIME Type, from a file, binary data and/or \GDImage/Image_interface
 *   !!! ORDER IS IMPORTANT !!!
 * - "factory_import_morphers": available \GDImage\Factory_ImportMorpher_Interface
 *   class names; used to determine if something to import can be considered as 
 *   a file, binary data and/or an \GDImage\Image_Interface
 *   !!! ORDER IS VERY IMPORTANT !!!
 * - "factory_export_drivers": available \GDImage\Factory_ExportDriver_Interface 
 *   class names; used to export an \GDImage\Image_Interface to a file, binary 
 *   data, data URI, output...
 */

if( ! Config::hasDefaultMimeType() ) {
  Config::setDefaultMimeType( 'image/png' );
}

// image interfaces
// !!! ORDER IS IMPORTANT !!!
if( ! Config::hasFactoryImageInterfaces() ) {
  Config::setFactoryImageInterfaces( array(
    '\\GDImage\\Image_Jpeg' ,
    '\\GDImage\\Image_Gif' ,
    // if we do not know if the image is true color or 
    // palette color, we want to use \GDImage\Image_Png
    '\\GDImage\\Image_Png' , 
    '\\GDImage\\Image_Png8' ,
    '\\GDImage\\Image_Png24' ,
    // animated image
    '\\GDImage\\Image_AGif' ,
    '\\GDImage\\Image_APng' , 
  ) );
}

// import morphers
// !!! ORDER IS VERY IMPORTANT !!!
if( ! Config::hasFactoryImportMorphers() ) {
  Config::setFactoryImportMorphers( array(
    // object/resource morphers
    '\\GDImage\\Factory_ImportMorpher_Image' ,
    '\\GDImage\\Factory_ImportMorpher_Resource' ,
    '\\GDImage\\Factory_ImportMorpher_GdResource' ,
    // string morphers
    '\\GDImage\\Factory_ImportMorpher_File' ,
    '\\GDImage\\Factory_ImportMorpher_Base64' ,
    '\\GDImage\\Factory_ImportMorpher_DataUri' ,
    // this one should allways be used as last string morpher
    '\\GDImage\\Factory_ImportMorpher_Binary' ,
  ) );
}

// export drivers
if( ! Config::hasFactoryExportDrivers() ) {
  Config::setFactoryExportDrivers( array(
    '\\GDImage\\Factory_ExportDriver_File' ,
    '\\GDImage\\Factory_ExportDriver_Base64' ,
    '\\GDImage\\Factory_ExportDriver_DataUri' ,
    '\\GDImage\\Factory_ExportDriver_Binary' ,
    '\\GDImage\\Factory_ExportDriver_Output' ,
    '\\GDImage\\Factory_ExportDriver_GdResource' ,
    '\\GDImage\\Factory_ExportDriver_Resource' ,
    '\\GDImage\\Factory_ExportDriver_Image' ,
  ) );
}

/**
 * Factory class for Image_Interface create/instance/import/export.
 *
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage factory
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Factory
{

  /**
   * Current \GDImage\Factory instance.
   *
   * @var \GDImage\Factory Current instance
   * @access protected
   */
  public static $_instance;

  /**
   * Method to retrieve \GDImage\Factory instance.
   *
   * @param none
   * @return \GDImage\Factory
   * @access public
   * @static
   */
  public static function getInstance()
  {
  	if( ! static::$_instance )
  	{
  		static::$_instance = new static();
  	}
  	return static::$_instance;
  }

  /**
   * Method to create an \GDImage\Image_Interface instance from
   * width and height.
   *
   * By default create image helping true color, whatever the
   * expected MIME Type is.
   *
   * @param integer $w
   * @param integer $h
   * @param [string] $mime Expected MIME Type
   * @return \GDImage\Image Image instance
   * @access public
   * @throws \GDImage\Exception
   * @static
   */
  public static function create( $w , $h , $mime = null )
  {
    $rsc = Resource_Abstract::createFromSize( $w , $h );

    return static::getInstance()->importation( $rsc , $mime );
  }

  /**
   * Method to create an \GDImage\Image_Interface instance from
   * nothing.
   *
   * By default create image helping true color, whatever the
   * expected MIME Type is.
   *
   * @param integer $w
   * @param integer $h
   * @param [string] $mime Expected MIME Type
   * @return \GDImage\Image Image instance
   * @access public
   * @throws \GDImage\Exception
   * @static
   */
  public static function instance( $mime = null )
  {
    // for instance creation, we need a mime type
    $mime === null && ( $mime = Config::getDefaultMimeType() );

    return static::getInstance()->instanciation( $mime );
  }

  /**
   * Method to create an \GDImage\Image instance.
   *
   * - If first argument is a GD resource or Resource_Abstract, image will be
   *   created from this resource.
   * - If first argument is a string, image will be imported from a filepath,
   *   a data URI scheme, a base64 string or binary data.
   *   The detection is done in this order.
   *
   * Last argument, if any, is allways the expected MIME Type.
   *
   * @param mixed $stuff
   * @param [string] $mime Expected MIME Type
   * @return \GDImage\Image_Interface Image instance
   * @access public
   * @throws \GDImage\Exception_Factory
   * @static
   */
  public static function import( $stuff , $mime = null )
  {
    return static::getInstance()->importation( $stuff , $mime );
  }


  /**
   * Method to export an \GDImage\Image_Interface instance with specific
   * parameters.
   *
   * Second argument can be a string, this string will pass over each
   * \GDImage\Factory_ExportDriver to determine best one to use.
   *
   * If argument is an array, a "driver" key must be specify to know wich
   * driver must be used. This driver can be the suffix of a
   * \GDImage\Factory_ExportDriver_* class, another class is \GDImage namespace
   * or any other class name.
   *
   * Some of these drivers have named parameters (by example: "base64" for
   * the \GDImage\Factory_ExportDriver_DataUri, that will disable/enable base64
   * encoding on binary data).
   * Most of them will consider numeric keys as extra arguments to pass
   * to the \GDImage\Image_Interface->toFile()/toBinary() method. Depending
   * of the Image_Interface to use on export, the order should never change:
   * - quality (JPEG/PNG8/PNG24)
   * - filters (PNG8/PNG24)
   * - number of colors (PNG8/GIF)
   * - dither (PNG8/GIF)
   *
   * Note that it is recommanded to play with configuration when you need to
   * apply setting over all images.
   *
   * Note that the \GDImage\Factory_ExportDriver_File is detected when the
   * second argument looks like a file path, other driver needs some specific
   * string to be detected (one of self::DRIVER_* constant).
   *
   * A third argument considered as a MIME type can be passed in order to
   * convert the image to another format.
   *
   * Specificities of the \GDImage\Factory_ExportDriver_File:
   * - if the file path has no extension, the extension will be added from the
   *   current image instance MIME Type.
   * - if there is an extension on file path, it has priority over the
   *   current image instance (not over MIME Type argument) to determine
   *   which Image_* class will be used for export;
   * - if there is a MIME Type, and if the file path has no extension, it has
   *   priority to determine which Image_* class will be used for export.
   *
   * This method return a string that represents the result of the export.
   *
   * Basic examples:
   *
   * - Factory::export( Image_Gif , 'path_to_file.jpg' )
   *   Convert GIF image to JPEG and place it to 'path_to_file.jpg'.
   *
   * - Factory::export( Image_Png8 , 'path_to_file' );
   *   Place PNG8 image to 'path_to_file.png'.
   *
   * - Factory::export( Image_Png8 , 'path_to_file' , 'image/jpeg' );
   *   Convert PNG8 image to JPEG and place it to 'path_to_file.jpg'.
   *
   * - Factory::export( Image_Png8 , 'path_to_file.gif' , 'image/jpeg' );
   *   Convert PNG8 image to JPEG and place it to 'path_to_file.gif' (yeap,
   *   wrong extension but expected result).
   *
   * - Factory::export( Image_Png8 , 'data' , 'image/jpeg' );
   *   Convert PNG8 image to JPEG and return the data URI representation of the
   *   image with MIME Type and base 64 encoded (defaults parameters).
   *
   * - Factory::export( Image_Jpeg , 'base64' );
   *   Return the base 64 representation of the image.
   *
   * - Factory::export( Image_Jpeg , 'path_to_file' )
   *   Place image to 'path_to_file.jpeg' (by convenience, the default
   *   extension for "image/jpeg" is jpeg, that is the first one found in
   *   mime.types file).
   *
   * Advanced examples:
   *
   * - Factory::export( Image_Jpeg , array(
   *     'export' => 'File' ,
   *     'file' => 'path_to_file.jpg' ,
   *     95 ,
   *   ) );
   *   Export JPEG to 'path_to_file.jpg' with quality 95.
   *   @see http://php.net/manual/en/function.imagejpeg.php
   *
   * - Factory::export( Image_Gif , array(
   *     'export' => 'Output' ,
   *     128 ,
   *     false ,
   *   ) , 'image/png' );
   *   Convert GIF to PNG8 and export it to output.
   *   If the GIF resource is a true color resource, will create palette color
   *   resource with 128 colors and no dither.
   *   @see http://php.net/manual/en/function.imagetruecolortopalette.php
   *
   * - Factory::export( Image_Png8 , array(
   *     'export' => 'DataUri' ,
   *     'base64' => false ,
   *     'mimetype' => false ,
   *     5 ,
   *     PNG_FILTER_AVG | PNG_FILTER_UP ,
   *     16 ,
   *     true
   *   ) );
   *   Export PNG8 image with quality 5 and filters to
   *   PNG_FILTER_AVG | PNG_FILTER_UP and return the data URI representation
   *   of the image without MIME Type and not base64 encoded.
   *   If the PNG8 resource is a true color resource, will create palette color
   *   resource with 16 colors and dither.
   *   @see http://php.net/manual/en/function.imagepng.php
   *   @see http://php.net/manual/en/function.imagetruecolortopalette.php
   *
   *
   * @param \GDImage\Image_Interface $image
   * @param mixed    $stuff
   * @param [string] $mime MIME Type
   * @return mixed   Result of the export
   * @access public
   * @throws \GDImage\Exception_Factory
   * @static
   */
  public static function export( Image_Interface $image , $stuff , $mime = null )
  {
    return static::getInstance()->exportation( $image , $stuff , $mime );
  }

  /**
   * \GDImage\Factory constructor.
   *
   * @param none
   * @return void
   * @access public
   */
  public function __construct()
  {
  }

  /**
   * Create an \GDImage\Image_Interface from MIME Type, file path,
   * binary data and/or \GDImage\Image_Interface.
   *
   * @param string $mime
   * @param [string] $filepath
   * @param [string] $binary
   * @param [\GDImage\Image_Interface] Image
   * @return \GDImage\Image_Interface Image instance
   * @access protected
   * @throws \GDImage\Exception_Factory
   * @access public
   */
  public function instanciation( $mime , $filepath = null , $binary = null , Image_Interface $image = null )
  {
    // fetch interfaces 
    $interfaces = Config::getFactoryImageInterfaces();
    
    $class = null;
    $priority = 0;
    for( $i = 0, $imax = count( $interfaces ); $i < $imax; $i++ )
    {
      $l_priority = call_user_func( array( $interfaces[$i] , 'detect' ) , $mime , $filepath , $binary , $image );

      if( $l_priority >= Image_Interface::MUSTBE )
      {
        $priority = $l_priority;
        $class = $interfaces[$i];
        // this class must be used, there is nothing else to check
        break;
      }

      // if there is already a class with the same priority
      // do not consider this new class, in other cases, we want to use it
      if( $l_priority > $priority )
      {
        $priority = $l_priority;
        $class = $interfaces[$i];
        // we want to continue checking
      }
    }

    if( ! $class )
    {
      // format arguments
      $filepath = $filepath ? substr( $filepath , 0 , 32 ).'[...]' : 'null';
      $binary = $binary ? substr( $binary , 0 , 32 ).'[...]' : 'null';
      $image = $image ? get_class( $image ) : 'null';

      throw new Exception_Factory( array( get_class( $this ) , $mime , $filepath , $binary , $image ) , 3050 );
    }

    // yes, we did it!
    $import_image = new $class();

    // create resource
    if( $filepath ) $import_image->fromFile( $filepath );
    elseif( $binary ) $import_image->fromBinary( $binary );
    elseif( $image ) $import_image->fromImage( $image );

    // finally return it
    return $import_image;
  }

  // Import section

  /**
   * Process import
   *
   * @param mixed $stuff
   * @param [string] $mime Expected MIME Type
   * @return \GDImage\Image_Interface Image instance
   * @access public
   * @throws \GDImage\Exception_Factory
   * @access public
   */
  public function importation( $stuff , $mime = null )
  {
    // fetch morphers
    $morphers = Config::getFactoryImportMorphers();
    
    for( $i = 0, $imax = count( $morphers ); $i < $imax; $i++ )
    {
      if( call_user_func( array( $morphers[$i] , 'detect' ) , $stuff ) )
      {
        $morpher = new $morphers[$i]( $stuff );
        break;
      }
    }

    if( empty( $morpher ) )
    {
      // fail
      throw new Exception_Factory( array( get_class( $this ) , substr( func_get_arg(0) , 0 , 32 ).'[...]' ) , 3000 );
    }

    // is there any expected mime type
    if( $mime === null )
    {
      // not? give me one morpher fucker!
      $mime = $morpher->getMimeType();
    }

    // shortcuts
    $f = $morpher->getFile();
    $b = $morpher->getBinary();
    $i = $morpher->getImage();

    // WTF?
    if( ! ( $f || $b || $i ) )
    {
      // fail
      throw new Exception_Factory( array( get_class( $this ) , get_class( $morpher ) ) , 3040 );
    }

    // now that we have a morpher, we want to look for the best
    // \GDImage\Image_Interface to use
    return $this->instanciation( $mime , $f , $b , $i );
  }

  // Export section
  
  // Export driver constants
  // They are not necessary and you do not need one 
  // if you create a new driver.
  // @see \GDImage\Factory_ExportDriver_Interface

  /**
   * Constant to represent a file export.
   *
   * @var string
   * @const
   */
  const DRIVER_FILE = 'file';

  /**
   * Constant to represent a base64 export.
   *
   * @var string
   * @const
   */
  const DRIVER_BASE64 = 'base64';

  /**
   * Constant to represent a base64 export.
   *
   * @var string
   * @const
   */
  const DRIVER_DATAURI = 'data';

  /**
   * Constant to represent a binary export.
   *
   * @var string
   * @const
   */
  const DRIVER_BINARY = 'binary';

  /**
   * Constant to represent a direct output export.
   *
   * @var string
   * @const
   */
  const DRIVER_OUTPUT = 'output';

  /**
   * Constant to represent a GD resource export.
   *
   * Note that in case of multiple resource image, only the first resource
   * will be exported.
   *
   * @var string
   * @const
   */
  const DRIVER_GD = 'gd';

  /**
   * Constant to represent a \GDImage\Resource_Abstract export.
   *
   * Note that in case of multiple resource image, only the first resource
   * will be exported.
   *
   * @var string
   * @const
   */
  const DRIVER_RESOURCE = 'resource';

  /**
   * Constant to represent a \GDImage\Image_Interface export.
   *
   * @var string
   * @const
   */
  const DRIVER_IMAGE = 'image';

  /**
   * Process export
   *
   * @param \GDImage\Image_Interface $image
   * @param mixed $stuff
   * @param [string] $mime Expected MIME Type
   * @return mixed Expected result
   * @access public
   * @throws \GDImage\Exception_Factory
   * @access public
   */
  public function exportation( \GDImage\Image_Interface $image , $stuff , $mime = null )
  {
    // fetch drivers
    $drivers = Config::getFactoryExportDrivers();
    
    if( is_string( $stuff ) )
    {
      // look for best driver to use
      for( $i = 0, $imax = count( $drivers ); $i < $imax; $i++ )
      {
        if( call_user_func( array( $drivers[$i] , 'detect' ) , $stuff ) )
        {
          $driver = $drivers[$i];
          // stop right now
          break;
        }
      }
    
      if( empty($driver) )
      {
        // fail
        throw new Exception_Factory( array( get_class( $this ) , $stuff ) , 3070 );
      }
    }
    
    if( is_array( $stuff ) && isset($stuff['driver']) )
    {
      // may apply ucfirst?
      $driver_name = $stuff['driver'];
      unset( $stuff['driver'] );
      
      // full qualified driver detection
      if( class_exists( $driver_name , true ) )
      {
        $driver = $driver_name;
      }
      // driver under \GDImage namespace
      elseif( class_exists( '\\GDImage\\'.$driver_name , true ) )
      {
        $driver = '\\GDImage\\'.$driver_name;
      }
      // driver like \GDImage\Factory_ExportDriver_*
      elseif( class_exists( '\\GDImage\\Factory_ExportDriver_'.ucfirst( $driver_name ) , true ) )
      {
        $driver = '\\GDImage\\Factory_ExportDriver_'.ucfirst( $driver_name );
      }
      // argh, lets do it another way
      else
      {
        // look for best driver to use using driver_name
        for( $i = 0, $imax = count( $drivers ); $i < $imax; $i++ )
        {
          if( call_user_func( array( $drivers[$i] , 'detect' ) , $driver_name ) )
          {
            $driver = $drivers[$i];
            // stop right now
            break;
          }
        }
      }
    
      if( empty($driver) )
      {
        // fail
        throw new Exception_Factory( array( get_class( $this ) , $driver_name ) , 3070 );
      }
    }
    
    // global exception
    if( empty($driver) )
    {
      // fail
      throw new Exception_Factory( array( get_class( $this ) , gettype( $stuff ) ) , 3070 );
    }
    
    // build the driver
    $driver = new $driver( $stuff );
    
    // now, we need to find Image_Interface to use with this driver
    
    // not any expected MIME type, look for driver one
    if( $mime === null ) $mime = $driver->getMimeType();
    
    // still null, use the same than current Image_Interface
    // yes, we call a static method in a not static context and it works like a charm
    if( $mime === null ) $mime = $image->getMimeType();
    
    
    // export image look up
    $export_image = null;
    
    
    // look for color conversion
    // not that we do not want to alter current image resource
    if( is_array( $stuff ) )
    {
      $stuff += array( 'palettecolor' => false , 'truecolor' => false , );
      if( $image instanceof Image_Abstract_Single )
      {
        if( $image->getResource()->isTrueColor() && $stuff['palettecolor'] )
        {
          // create a new instance to not alter original one
          $class = get_class( $image );
          $export_image = new $class();
          // create a dummy palette color resource
          $export_image->setResource( Resource_PaletteColor::createFromSize( 1 , 1 ) );
          
          // now get best Image_Interface for export
          $export_image = $this->instanciation( $mime , null , null , $export_image );
          
          // and finally assign current resource
          $export_image->setResource( $image->getResource() );
        }
        elseif( $image->getResource()->isPaletteColor() && $stuff['truecolor'] )
        {
          // create a new instance to not alter original one
          $class = get_class( $image );
          $export_image = new $class();
          // create a dummy true color resource
          $export_image->setResource( Resource_TrueColor::createFromSize( 1 , 1 ) );
          
          // now get best Image_Interface for export
          $export_image = $this->instanciation( $mime , null , null , $export_image );
          
          // and finally assign current resource
          $export_image->setResource( $image->getResource() );
        }
      }
      elseif( $image instanceof Image_Abstract_Multiple )
      {
        // sounds too complicated for now
        // Image_AGif support only palette color
        // Image_APng support both
      }
    }
    
    // not any export_image
    if( ! $export_image )
    {
      // if mime types are identical and the color type remains the same, no conversion is needed
      if( $mime !== $image->getMimeType() )
      {
        // we have to find a better Image_Interface for that
        $export_image = $this->instanciation( $mime , null , null , $image );
      }
      else
      {
        // do not clone, it is useless
        $export_image = $image;
      }
    }
    
    // invoke driver
    return $driver( $export_image );
  }
}
