<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * A shortcut to \GDImage\Image_Interface.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage image
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements    \GDImage\Image_Interface
 * @abstract
 */
abstract class Image implements Image_Interface
{
}
