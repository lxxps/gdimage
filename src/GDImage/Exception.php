<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;
use GDImage;

/**
 * \GDImage\Exception specific exception class.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage exception
 * @author     Loops <pierrotevrard@gmail.com>
 * @version    SVN: $Id: GDImageException.class.php 170 2010-08-26 11:23:35Z loops $
 */
class Exception extends \Exception
{
  /**
   * Array of exceptions messages by code.
   * Range of message are:
   *  - from X to Y: DEFINITION exceptions
   * 
   * @var array
   * @access protected
   */
  public $_messages = array(
    // From 9130 to 9139: gd_image() exceptions
    9130 => '%s: Transformation "%s" is not a valid transformation key, class instance, class name or class suffix.' ,
  );
  
  
  /**
   * \GDImage\Exception constructor.
   * 
   * Used with code attribute, message can be a replacement string
   * or an array of replacements used with sprintf.
   * If the code does not exists in messages proprety, just the string or
   * the first value of the array will be used.
   *
   * @param mixed $message Message
   * @param integer $code Code
   * @param Exception $previous Previous Exception (since PHP 5.3.0)
   * @access public
   */
  public function __construct( $message = '' , $code = 0 , \Exception $previous = null )
  {
		if( isset($this->_messages[$code]) )
		{
			if( is_array( $message ) )
			{
				array_unshift( $message , $this->_messages[$code] );
				$message = call_user_func_array( 'sprintf' , $message );
			}
			else
			{
				$message = sprintf( $this->_messages[$code] , $message );
			}
		}
		elseif( is_array( $message ) )
		{
			$message = current($message);
		}
    
  	parent::__construct( $message , $code , $previous );
  }
}