<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Apply mirror transformation on resource.
 * 
 * Mirror can be done on left, right, top and/or bottom.
 * 
 * Requiring a top and a left mirror provide an additionnal "corner" mirror.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Transform_Interface
 */
class Transform_Mirror implements Transform_Interface
{
	/**
	 * Bitwise value to represent left mirror.
	 *
	 * @var integer
	 * @const
	 */
	const LEFT = 1;
  // shortcut
	const L = 1;
  
	/**
	 * Bitwise value to represent right mirror.
	 *
	 * @var integer
	 * @const
	 */
	const RIGHT = 2;
  // shortcut
	const R = 2;
  
	/**
	 * Bitwise value to represent top mirror.
	 *
	 * @var integer
	 * @const
	 */
	const TOP = 4;
  // shortcut
	const T = 4;
  
	/**
	 * Bitwise value to represent bottom mirror.
	 *
	 * @var integer
	 * @const
	 */
	const BOTTOM = 8;
  // shortcut
	const B = 8;
  
  /**
   * Mirror mode, bitwise value.
   *
   * @var integer
   * @access protected
   */
  public $_mode = 0;
  
	/**
	 * Method to set mirror mode.
   * String alowed while it corresponds to a class constant.
	 *
	 * @param mixed $mode
   * @return void
   * @access public
	 */
	public function setMode( $mode )
	{
    // constant lookup
    if( is_string( $mode ) ) $mode = constant( 'self::'.strtoupper( $mode ) );
    
		$this->_mode = $mode & 15;
	}
  
	/**
	 * Constructor
	 *
	 * @param [mixed] $mode
   * @return void
   * @access public
	 */
	public function __construct( $mode = null )
	{
    // for class extension, it is preferable to never automatically set properties on constructor
		if( $mode !== null ) $this->setMode( $mode );
	}
  	
  /**
   * Apply transformation to a resource.
   * Return false if the transformation fails.
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Transform_Interface
   */
  public function __invoke( Resource_Abstract &$rsc )
  {
    // no mode, cannot apply
    if( ! $this->_mode ) return false;
    
    $flag = true;
    
    
    // prepare flipped resource
    $flip_v = null;
    $flip_h = null;
    $flip_b = null;
    // require flip transform
    $flip = new Transform_Flip();
    
    if( $this->_mode & ( self::LEFT | self::RIGHT ) )
    {
      $flip->setMode( Transform_Flip::HORIZONTAL );
      $flip_h = clone $rsc; // clone resource
      $flag = $flip( $flip_h ) && $flag; // apply flip
    }
    
    if( $this->_mode & ( self::TOP | self::BOTTOM ) )
    {
      $flip->setMode( Transform_Flip::VERTICAL );
      $flip_v = clone $rsc; // clone resource
      $flag = $flip( $flip_v ) && $flag; // apply flip
    }
    
    if( ( $this->_mode & ( self::LEFT | self::RIGHT ) ) && ( $this->_mode & ( self::TOP | self::BOTTOM ) ) )
    {
      $flip->setMode( Transform_Flip::BOTH );
      $flip_b = clone $rsc; // clone resource
      $flag = $flip( $flip_b ) && $flag; // apply flip
    }
    
    
    // prepare blank
    
    $w = $rsc->getWidth();
    $h = $rsc->getHeight();
    
    $x_lft = 0;
    if( $this->_mode & self::LEFT ) $x_lft = $w;
    $x_rgt = 0;
    if( $this->_mode & self::RIGHT ) $x_rgt = $w;
    $y_top = 0;
    if( $this->_mode & self::TOP ) $y_top = $h;
    $y_bot = 0;
    if( $this->_mode & self::BOTTOM ) $y_bot = $h;
    
    $tmp = $rsc->blank( $x_lft + $w + $x_rgt , $y_top + $h + $y_bot );
    
    
    // then copy
    
    // adjust these values before copy
    $x_rgt += $x_lft;
    $y_bot += $y_top;
    
    // copy original
    $flag = imagecopy( $tmp->getGdResource() , $rsc->getGdResource() , $x_lft , $y_top , 0 , 0 , $w , $h ) && $flag;
    
    // left and right
    if( $this->_mode & self::LEFT )
    {
      $flag = imagecopy( $tmp->getGdResource() , $flip_h->getGdResource() , 0 , $y_top , 0 , 0 , $w , $h ) && $flag; 
    }
    if( $this->_mode & self::RIGHT )
    {
      $flag = imagecopy( $tmp->getGdResource() , $flip_h->getGdResource() , $x_rgt , $y_top , 0 , 0 , $w , $h ) && $flag; 
    }
    // top and bottom
    if( $this->_mode & self::TOP )
    {
      $flag = imagecopy( $tmp->getGdResource() , $flip_v->getGdResource() , $x_lft , 0 , 0 , 0 , $w , $h ) && $flag; 
    }
    if( $this->_mode & self::BOTTOM )
    {
      $flag = imagecopy( $tmp->getGdResource() , $flip_v->getGdResource() , $x_lft , $y_bot , 0 , 0 , $w , $h ) && $flag; 
    }
    // corners
    if( ! ( ( $this->_mode & ( self::LEFT | self::TOP ) ) ^ ( self::LEFT | self::TOP ) ) )
    {
      $flag = imagecopy( $tmp->getGdResource() , $flip_b->getGdResource() , 0 , 0 , 0 , 0 , $w , $h ) && $flag; 
    }
    if( ! ( ( $this->_mode & ( self::RIGHT | self::TOP ) ) ^ ( self::RIGHT | self::TOP ) ) )
    {
      $flag = imagecopy( $tmp->getGdResource() , $flip_b->getGdResource() , $x_rgt , 0 , 0 , 0 , $w , $h ) && $flag; 
    }
    if( ! ( ( $this->_mode & ( self::LEFT | self::BOTTOM ) ) ^ ( self::LEFT | self::BOTTOM ) ) )
    {
      $flag = imagecopy( $tmp->getGdResource() , $flip_b->getGdResource() , 0 , $y_bot , 0 , 0 , $w , $h ) && $flag; 
    }
    if( ! ( ( $this->_mode & ( self::RIGHT | self::BOTTOM ) ) ^ ( self::RIGHT | self::BOTTOM ) ) )
    {
      $flag = imagecopy( $tmp->getGdResource() , $flip_b->getGdResource() , $x_rgt , $y_bot , 0 , 0 , $w , $h ) && $flag; 
    }
    
    // will destroy previous GD resource
    $rsc = $tmp;
    
    // done
    return $flag;
  }
}
