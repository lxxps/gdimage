<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Transformation used to rotate an image.
 * For convenience, we allways disable the ignore transparent flag.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage image
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Transform_Interface
 */
class Transform_Rotate implements Transform_Interface
{
	/**
	 * Rotation degrees. 
	 *
	 * @var float Degrees
	 * @access protected
	 */
	public $_degrees = 0;
	
	/**
	 * Method to assign rotation degrees.
	 *
	 * @param float $degrees Degrees
	 * @return void
	 * @access public
	 */
	public function setDegrees( $degrees )
	{
		$this->_degrees = (float)$degrees;
	}
	
	/**
	 * Preserve edge. 
	 *
	 * @var boolean
	 * @access protected
	 */
	public $_edge = false;
	
	/**
	 * Method to assign edge preservation.
	 *
	 * @param boolean $edge
	 * @return void
	 * @access public
	 */
	public function setPreserveEdge( $edge )
	{
		$this->_edge = (bool)$edge;
	}
  
	/**
	 * \GDImage\Color instance used as background color.
	 *
	 * @var \GDImage\Color
	 * @access protected
	 */
	public $_bg;
	
	/**
	 * Method to assign background color.
   * Set it to null to use transparent color.
	 *
	 * @param mixed $bg Color
	 * @return void
	 * @access public
	 */
	public function setBackground( $bg = null )
	{
    // null
    if( $bg === null ) $this->_bg = null;
    // assign instance
    elseif( $bg instanceof Color ) $this->_bg = $bg;
    // do it mixed
    else $this->_bg = new Color( $bg );
	}
	
	/**
	 * Alias for setBackground()
   * 
   * @deprecated
	 * @param mixed $bg Color
	 * @return void
	 * @access public
	 */
	public function setColor( $bg = null )
	{
    $this->setBackground( $bg );
  }
  
	/**
	 * Constructor
	 *
	 * @param [float] $degrees Degrees
	 * @param [mixed] $bg Background color
	 * @param [boolean] $edge Preserve edge flag
   * @return void
   * @access public
	 */
	public function __construct( $degrees = null , $bg = null , $edge = null )
	{
    // for class extension, it is preferable to never automatically set properties on constructor
		if( $degrees !== null ) $this->setDegrees( $degrees );
    if( $bg !== null ) $this->setBackground( $bg );
		if( $edge !== null ) $this->setPreserveEdge( $edge );
	}
	
	/**
	 * Magic method call on clone.
	 *
	 * @param none
	 * @return void
	 * @access public
	 */
	public function __clone()
	{
		if( $this->_bg ) $this->_bg = clone $this->_bg;
	}
  	
  /**
   * Apply transformation to a resource.
   * Return false if the transformation fails.
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Transform_Interface
   */
  public function __invoke( Resource_Abstract &$rsc )
  {
    // no degrees, cannot apply
    if( ! $this->_degrees ) return false;
    
    // determine background color
    $bg = $this->_bg;
    // nothing, look for transparent
    if( ! $bg )
    {
      $bg = $rsc->getTransparent();
    }
    // nothing, look for default color and assign it as transparent
    if( ! $bg ) 
    {
      $bg = new Color();
      $bg->setAlpha( Color::ALPHA_TRANSPARENT );
      $rsc->setTransparent( $bg ); // no matter if it fails
    }
    
    // convert to true color for better result
    $tmp1 = $rsc->toTrueColor();
    
    // compare object property
    // if the resource is true color, we must be sure the background color to 
    // use will also be a background color for the original picture
    // if the resource is palette color, and if transparent color is the same, 
    // we do not need to do that
    if( $rsc->isTrueColor() || $bg != $rsc->getTransparent() )
    {
      // blank it
      $tmp1 = $tmp1->blank();
    
      // enable alphablending
      $tmp1->setMode( Resource_Abstract::MODE_ALPHABLENDING );

      imagefill( $tmp1->getGdResource() , 0 , 0 , $tmp1->setColor( $bg ) );

      // copy from palette to true color is always OK
      imagecopy( $tmp1->getGdResource() , $rsc->getGdResource() , 0 , 0 , 0 , 0 , $rsc->getWidth() , $rsc->getHeight() );
    }
    
    // enable antialias (experimental)
    $tmp1->setMode( Resource_Abstract::MODE_ANTIALIAS );
    
    // rotate resource
    // the last parameter seems useless because we work on true color resource
    if( ! $tmp2 = imagerotate( $tmp1->getGdResource() , $this->_degrees , $tmp1->setColor( $bg ) , 0 ) ) return false;
    
    // create Resource object
    $tmp2 = Resource_Abstract::createFromGdResource( $tmp2 );
    
    // restore palette color ability
    if( $rsc->isPaletteColor() )
    {
      $tmp1 = $tmp2->toPaletteColor( 256 , false );
      
      // if we were unable to find any transparent color anymore
      // we may have trouble with AGIF manipulation
      if( $rsc->getTransparent() && ( ! $tmp1->getTransparent() ) )
      {
        if( $tmp1->countColors() === 256 )
        {
          // redo palette conversion with one slot for the transparent color
          $tmp1 = $tmp2->toPaletteColor( 255 , false );
        }
        
        // assign transparent
        $tmp1->setTransparent( $rsc->getTransparent() );
      }
      
      // reassign tmp2
      $tmp2 = $tmp1;
    }
    
    // do not preserve edge
    if( ! $this->_edge )
    {
      // asign new resurce
      $rsc = $tmp2;
      
      // then stop
      return true;
    }
    
    // preserve edge expected

    // create a blank resource from temporary stuff with expected width and height
    // this will preserve palette ability and transparent stuff
    $tmp1 = $tmp2->blank( $rsc->getWidth() , $rsc->getHeight() );

    // disable alpha blending
    $tmp1->unsetMode( Resource_Abstract::MODE_ALPHABLENDING );

    // determine copy parameters

    $dx = ( $tmp1->getWidth() - $tmp2->getWidth() ) / 2;
    $sx = 0;
    if( $dx < 0 )
    {
      $sx = -1 * $dx;
      $dx = 0;
    }

    $dy = ( $tmp1->getHeight() - $tmp2->getHeight() ) / 2;
    $sy = 0;
    if( $dy < 0 )
    {
      $sy = -1 * $dy;
      $dy = 0;
    }

    if( ! imagecopy( $tmp1->getGdResource() , $tmp2->getGdResource() , 
                     $dx , $dy , $sx , $sy , 
                     $tmp2->getWidth() - ( 2 * $sx ) , $tmp2->getHeight() - ( 2 * $sy ) ) ) return false;

    // asign new resurce
    $rsc = $tmp1;
    
    // done
    return true;
  }
}
