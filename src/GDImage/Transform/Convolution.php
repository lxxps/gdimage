<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * An alias for \GDImage\Transform_Convolution_Basic
 *
 * @package    GDImage
 * @subpackage transform
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends    \GDImage\Transform_Convolution_Basic
 * @implements \GDImage\Transform_Interface
 */
class Transform_Convolution extends Transform_Convolution_Basic
{

}
