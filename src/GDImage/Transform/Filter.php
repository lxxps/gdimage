<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Base transformation used to apply filters.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Transform_Interface
 */
class Transform_Filter implements Transform_Interface
{
	/**
	 * Color operation 4x4 matrix
	 *
	 * @var array Matrix
	 * @access protected
	 */
	public $_matrix = array(
	  //     R   G   B   A
	  array( 1 , 0 , 0 , 0 ), // R
	  array( 0 , 1 , 0 , 0 ), // G
	  array( 0 , 0 , 1 , 0 ), // B
	  array( 0 , 0 , 0 , 1 ), // A
	);
	
  /**
   * Set the color Matrix
   * 
   * By convenience, if there is some missing values, we will reproduce the 
   * default matrix.
   *
   * @param array $matrix
   * @return void
   * @access public
   */
  public function setMatrix( array $matrix )
  {
    // make sure all keys exists and are array
    $matrix += array_fill( 0 , 4 , array() );
    
    // for the second level, if it is not number, we do not care
    $i = 4;
    
    $base = array_fill( 0 , 4 , 0 );
    while( $i-- )
    {
      $base[$i] = 1;
      $matrix[$i] += $base;
      $base[$i] = 0;
    }
    
    $this->_matrix = $matrix;
  }
  
	/**
	 * Constructor
	 *
	 * @param [array] $matrix Color matrix
   * @return void
   * @access public
	 */
	public function __construct( array $matrix = null )
	{
    // for class extension, it is preferable to never automatically set properties on constructor
		if( $matrix !== null ) $this->setMatrix( $matrix );
	}
	
  /**
   * Apply transformation to the image resource.
   * Return false if the transformation fails.
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access public
   */
  public function __invoke( Resource_Abstract &$rsc )
  {
    // determine function to use
    if( $rsc->isTrueColor() )
    {
      return $this->_filterTrueColor( $rsc );
    }
    else
    {
      return $this->_filterPaletteColor( $rsc );
    }
  }
  
  /**
   * Method used to apply matrix on color.
   *
   * @param \GDImage\Color $color
   * @return \GDImage\Color $color
   * @access protected
   */
  public function _applyMatrix( Color $color )
  {
    // shortcut to data
    list( $r , $g , $b , $a ) = $color->toArray();
    
    // create new color
    $new = new Color();
    
    // make calculation
    $new->setRed( ( $r * $this->_matrix[0][0] ) + ( $g * $this->_matrix[0][1] ) + ( $b * $this->_matrix[0][2] ) + ( $a * $this->_matrix[0][3] ) );
    $new->setGreen( ( $r * $this->_matrix[1][0] ) + ( $g * $this->_matrix[1][1] ) + ( $b * $this->_matrix[1][2] ) + ( $a * $this->_matrix[1][3] ) );
    $new->setBlue( ( $r * $this->_matrix[2][0] ) + ( $g * $this->_matrix[2][1] ) + ( $b * $this->_matrix[2][2] ) + ( $a * $this->_matrix[2][3] ) );
    $new->setAlpha( ( $r * $this->_matrix[3][0] ) + ( $g * $this->_matrix[3][1] ) + ( $b * $this->_matrix[3][2] ) + ( $a * $this->_matrix[3][3] ) );
    
  	return $new;
  }
	
  /**
   * Method used to apply filter transformation on true color image.
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access protected
   */
  public function _filterTrueColor( Resource_Abstract &$rsc )
  {
		// each pixel needs to be transform in pixel of new color
    
    // disable alpha blending
  	$rsc->unsetMode( Resource_Abstract::MODE_ALPHABLENDING );
    
    // register calculation once by color, may not a good idea for memory, 
    // but a very good idea for performance)
    
    // as a counterpart, the memory will not be free but mark as available for 
    // the rest of the script
    
    $calc = array();
    
		$flag = true;
    $x = $rsc->getWidth();
    $ymax = $rsc->getHeight();
    while( $x-- )
    {
      $y = $ymax;
      while( $y-- )
      {
        // fetch pixel color
        $c = $rsc->getColorAt( $x , $y );
        
        // color key
        $k = $c->toInteger();
        
        // populate array if necessary
        if( empty($calc[$k]) ) $calc[$k] = $this->_applyMatrix( $c );
        
        // assign new color
		  	$flag = $rsc->setColorAt( $calc[$k] , $x , $y ) && $flag;
        
//        // assign new color
//		  	$flag = $rsc->setColorAt( $this->_applyMatrix( $c ) , $x , $y ) && $flag;
      }
    }
    
		return $flag;
  }
	
  /**
   * Method used to apply filter transformation on palette color image.
   * For palette image there is no need to transform every pixels, 
   * we just transform the color palette.
   * 
   * Note: On PHP 5.4, we can use imagecolorset() with alpha channel.
   * @see http://php.net/manual/en/function.imagecolorset.php
   *
   * @param &\GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access protected
   */
  public function _filterPaletteColor( Resource_Abstract &$rsc )
  {
    // if we can use imagecolorset() with alpha channel, just do it
    if( version_compare( PHP_VERSION , '5.4.0' , '>=' ) )
    {
      for( $i = 0, $imax = $rsc->countColors(); $i < $imax; $i++ )
      {
        // apply matrix
        $c = $this->_applyMatrix( $rsc->getColor( $i ) );

        // assign it at same index
        imagecolorset( $rsc->getGdResource() , $i , $c->getRed() , $c->getGreen() , $c->getBlue() , $c->getAlpha() );
      }
      
      return true;
    }
    
    
    // if not, we may be able to use imagecolorset(), but only if there is one
    // color with alpha channel (the transparent one) and if there is no
    // calculation on alpha channel (0 can be accepted as calculation)
    if( $this->_matrix[3] === array( 0 , 0 , 0 , 1 ) || $this->_matrix[3] === array( 0 , 0 , 0 , 0 ) )
    {
      // keep in mind transparent color index
      $t = imagecolortransparent( $rsc->getGdResource() );

      $alpha = false;
      for( $i = 0, $imax = $rsc->countColors(); $i < $imax; $i++ )
      {
        if( $i === $t ) continue;

        // fetch color
        $c = $rsc->getColor( $i );

        if( $c->getAlpha() )
        {
          $alpha = true;
          break;
        }
      }

      if( ! $alpha )
      {
        // we can use imagecolorset() because there is only one transparent color
        // and there is no calculation on alpha channel

        for( $i = 0, $imax = $rsc->countColors(); $i < $imax; $i++ )
        {
          // apply matrix
          $c = $this->_applyMatrix( $rsc->getColor( $i ) );

          // assign it at same index
          imagecolorset( $rsc->getGdResource() , $i , $c->getRed() , $c->getGreen() , $c->getBlue() );
        }

        return true;
      }
    }
    
    
    // using color deallocation and reallocation does not provide good results:
    // it seems that GD automatically detect if the color is not present in the 
    // palette and will not allocate an existing color, a bug may occurs with 
    // this color non-allocation
    
    // doing a palette copy/paste also fails:
    // sometimes, GD reassing pixels color, that is really unexpected
    
    // via true color, the palette may be reduced to a minimal set, and we do 
    // not want that
    
    // the only way to achieve a strong palette manipulation is to create the 
    // new palette, then copying each pixel index to a brand new resource
    
    $flag = true;
    
    // create temporary resource
    $tmp = Resource_PaletteColor::createFromSize( $rsc->getWidth() , $rsc->getHeight() );
    
    
    // create the new palette on this innocent resource
    
    // do not process calculation twice
    $calc = array();
    $flag = true;
    
    // preserve color indexes in the palette
    for( $i = 0, $imax = $rsc->countColors(); $i < $imax; $i++ )
    {
      // apply matrix
      $c = $this->_applyMatrix( $rsc->getColor( $i ) );
      
      // strangely, duplicate colors seems to get new color index on this innocent resource...
      $flag = ( imagecolorallocatealpha( $tmp->getGdResource() , $c->getRed() , $c->getGreen() , $c->getBlue() , $c->getAlpha() ) !== false ) && $flag;
    }
    
    // propagate transparent color
    if( ( $t = imagecolortransparent( $rsc->getGdResource() ) ) > -1 )
    {
      // do it with index of the transparent color
      $flag = ( imagecolortransparent( $tmp->getGdResource() , $t ) > -1 ) && $flag;
    }
 
    
    // then copy each pixels to their destination
    
    $x = $rsc->getWidth();
    $ymax = $rsc->getHeight();
    while( $x-- )
    {
      $y = $ymax;
      while( $y-- )
      {
        // fetch color index
        $i = imagecolorat( $rsc->getGdResource() , $x , $y );
        
        // assign color index
		  	$flag = imagesetpixel( $tmp->getGdResource() , $x , $y , $i ) && $flag;
      }
    }
    
    // replace original resource
    $rsc = $tmp;
    
    // Des que je fais ce morceau, ca part en couille
		return $flag;
  }
}
