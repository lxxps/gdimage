<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Interface used for image manipulation.
 *
 * @package    GDImage
 * @subpackage transform
 * @author     Loops <pierrotevrard@gmail.com>
 */
interface Transform_Interface
{
  /**
   * Apply transformation to a resource.
   * Return false if the transformation fails.
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access public
   */
  public function __invoke( Resource_Abstract &$rsc );
  
}
