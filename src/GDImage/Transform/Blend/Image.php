<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Base transformation used to apply blend mode over an background image.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends \GDImage\Transform_Blend_Abstract
 * @implements \GDImage\Transform_Interface
 */
class Transform_Blend_Image extends Transform_Blend_Abstract
{
  
	/**
	 * \GDImage\Image_Interface instance to use
	 *
	 * @var \GDImage\Image_Interface
	 * @access protected
	 */
	public $_image = null;
	
	/**
	 * Method to assign background image.
   * Allow anything that can be import.
	 *
	 * @param mixed $image Image
	 * @return void
	 * @access public
	 */
	public function setImage( $image )
	{
    // assign instance
    if( $image instanceof Image_Interface ) $this->_image = $image;
    // do it mixed
    else $this->_image = Factory::import( $image );
	}
  
	/**
	 * Constructor
	 *
	 * @param [mixed] $image Image
	 * @param [mixed] $mode Blend mode
   * @return void
   * @access public
	 */
	public function __construct( $image = null , $mode = null )
	{
    // for class extension, it is preferable to never automatically set properties on constructor
		if( $image !== null ) $this->setImage( $image );
    if( $mode !== null ) $this->setMode( $mode );
	}
	
	/**
	 * Magic method call on clone.
	 *
	 * @param none
	 * @return void
	 * @access public
	 */
	public function __clone()
	{
		if( $this->_image ) $this->_image = clone $this->_image;
	}
  	
  /**
   * Apply transformation to a resource.
   * Return false if the transformation fails.
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Transform_Interface
   */
  public function __invoke( Resource_Abstract &$rsc )
  {
    // no image, cannot apply
    if( ! $this->_image ) return false;
    
    $image = $this->_image;
    
    if( ! ( $image instanceof Image_Single_Abstract ) )
    {
      // do it with default stuff
      $image = Factory::instance();
      // let the original image decide what we should get has single image
      $image->fromImage( $this->_image );
    }
    
    // no resource, cannot apply
    if( ! $image->getResource() ) return false;
    
    // fetching color is quicker on true color
    $bg = $image->getResource()->toTrueColor();
    
    // clone resource to be safe
    $tmp = clone $rsc;
    
    // the job must be done with true color
    $tmp = $tmp->toTrueColor();
    
    // determine area to blend
    // in that case, use min() to not call imagesx/y twice
    $xmax = min( $rsc->getWidth() , $bg->getWidth() );
    $ymax = min( $rsc->getHeight() , $bg->getHeight() );
    
    // do not process calculation twice
    $calc = array();
    
		$flag = true;
    $x = $xmax;
    while( $x-- )
    {
      $y = $ymax;
      while( $y-- )
      {
        // fetch pixel colors
        $c1 = $tmp->getColorAt( $x , $y );
        $c2 = $bg->getColorAt( $x , $y );
      
        $key = $c2->getRed().','.$c1->getRed();
        if( empty($calc[$key]) ) $calc[$key] = call_user_func( $this->_mode , $c2->getRed() , $c1->getRed() );
        $c1->setRed( $calc[$key] );
      
        $key = $c2->getGreen().','.$c1->getGreen();
        if( empty($calc[$key]) ) $calc[$key] = call_user_func( $this->_mode , $c2->getGreen() , $c1->getGreen() );
        $c1->setGreen( $calc[$key] );
      
        $key = $c2->getBlue().','.$c1->getBlue();
        if( empty($calc[$key]) ) $calc[$key] = call_user_func( $this->_mode , $c2->getBlue() , $c1->getBlue() );
        $c1->setBlue( $calc[$key] );
        
        // assign new color
		  	$flag = $tmp->setColorAt( $c1 , $x , $y ) && $flag;
      }
    }
    
    // resource is a palette color
    if( $rsc->isPaletteColor() )
    {
      // in that case, make destination a palette color again
      $tmp1 = $tmp->toPaletteColor( 256 , false );
      
      // if we were unable to find any transparent color anymore
      // we may have trouble with AGIF manipulation
      if( ( $c1 = $rsc->getTransparent() ) && ( ! $tmp1->getTransparent() ) )
      {
        if( $tmp1->countColors() === 256 )
        {
          // redo palette conversion with one slot for the transparent color
          $tmp1 = $tmp->toPaletteColor( 255 , false );
        }
        
        // blend this transparent color with the background transparent color
        if( ! ( $c2 = $bg->getTransparent() ) )
        {
          // get the one from temporary true color resource
          $c2 = $tmp->getTransparent();
        }
        
        // blend the two colors
        $key = $c2->getRed().','.$c1->getRed();
        if( empty($calc[$key]) ) $calc[$key] = call_user_func( $this->_mode , $c2->getRed() , $c1->getRed() );
        $c1->setRed( $calc[$key] );
      
        $key = $c2->getGreen().','.$c1->getGreen();
        if( empty($calc[$key]) ) $calc[$key] = call_user_func( $this->_mode , $c2->getGreen() , $c1->getGreen() );
        $c1->setGreen( $calc[$key] );
      
        $key = $c2->getBlue().','.$c1->getBlue();
        if( empty($calc[$key]) ) $calc[$key] = call_user_func( $this->_mode , $c2->getBlue() , $c1->getBlue() );
        $c1->setBlue( $calc[$key] );
        
        // assign transparent
        $tmp1->setTransparent( $c1 );
      }
      
      // finally assign new destination
      $tmp = $tmp1;
    }
    
    // assign resource
    $rsc = $tmp;
    
    // I'm gonna swing from the chandelier, from the chandelier
    return $flag;
  }
  
}
