<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Base transformation used to apply blend mode.
 * 
 * For all available formulas, see
 * http://www.deepskycolors.com/archivo/2010/04/21/formulas-for-Photoshop-blending-modes.html
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Transform_Interface
 * @abstract
 */
abstract class Transform_Blend_Abstract implements Transform_Interface
{
	/**
	 * Blend mode to apply, in fact a color operation.
   * 
   * Each color operation is applied on a single color channel (red, green or blue).
   * 
   * For convenience, color channel value are not converted to value beetween 0 and 1.
   * So each variable in formulas must be divide by 255 and the result must be multiply by 255.
   * We simplify formulas in that way.
   * 
   * The operation receieved two arguments:
   *  - color channel value for bottom layer
   *  - color channel value for top layer
	 *
	 * @var callable
	 * @access protected
	 */
  public $_mode = array( __CLASS__ , 'blendModeNormal' );
	
  /**
   * Set the blend mode to apply
   * 
   * Can be a string, or a callable function.
   *
   * @param mixed $mode
   * @return void
   * @access public
   */
  public function setMode( $mode )
  {
    if( is_string( $mode ) )
    {
      $method = sprintf( 'blendMode%s' , Config::camelize( $mode ) );
      if( method_exists( $this , $method ) )
      {
        $this->_mode = array( $this , $method );
      }
      elseif( method_exists( $this , $mode ) )
      {
        $this->_mode = array( $this , $mode );
      }
      else
      {
        // constants lookup
        if( defined( 'self::'.strtoupper($mode) ) )
        {
          $this->_mode = array( $this , sprintf( 'blendMode%s' , Config::camelize( constant( 'self::'.strtoupper($mode) ) ) ) );
        }
        else
        {
          // must be a valid method
          throw new Exception_Transform( array( get_class( $this ) , $mode ) , 5031 );
        }
      }
    }
    elseif( is_callable( $mode ) )
    {
      $this->_mode = $mode; 
    }
    else
    {
      // must be callable
      throw new Exception_Transform( array( get_class( $this ) , var_export( $mode, true ) ) , 5030 );
    }
  }
  
  // Blend modes
  
  /**
   * Constant for normal mode
   * 
   * @var string
   * @const
   */
  const NORMAL = 'normal';
  
  /**
   * Normal blend mode
   * 
   * @param integer $a Color channel value for bottom layer
   * @param integer $b Color channel value for top layer
   * @return integer New Color channel value
   * @static
   */
  static public function blendModeNormal( $a , $b )
  {
    return $b; 
  }
  
  /**
   * Constant for darken mode
   * 
   * @var string
   * @const
   */
  const DARKEN = 'darken';
  
  /**
   * Darken blend mode
   * 
   * @param integer $a Color channel value for bottom layer
   * @param integer $b Color channel value for top layer
   * @return integer New Color channel value
   * @static
   */
  static public function blendModeDarken( $a , $b )
  {
    if( $a < $b ) return $a;
    return $b; 
  }
  
  /**
   * Constant for multiply mode
   * 
   * @var string
   * @const
   */
  const MULTIPLY = 'multiply';
  
  /**
   * Multiply blend mode
   * 
   * @param integer $a Color channel value for bottom layer
   * @param integer $b Color channel value for top layer
   * @return integer New Color channel value
   * @static
   */
  static public function blendModeMultiply( $a , $b )
  {
    return $a * $b / 255;
  }
  
  /**
   * Constant for color burn mode
   * 
   * @var string
   * @const
   */
  const COLORBURN = 'color_burn';
  
  /**
   * Color burn blend mode
   * 
   * @param integer $a Color channel value for bottom layer
   * @param integer $b Color channel value for top layer
   * @return integer New Color channel value
   * @static
   */
  static public function blendModeColorBurn( $a , $b )
  {
    // avoid 0 divisor
    if( $b === 0 ) return 0;
    
    return 255 - ( ( 255 - $a ) * 255 / $b );
  }
  
  /**
   * Constant for linear burn mode
   * 
   * @var string
   * @const
   */
  const LINEARBURN = 'linear_burn';
  
  /**
   * Linear burn blend mode
   * 
   * @param integer $a Color channel value for bottom layer
   * @param integer $b Color channel value for top layer
   * @return integer New Color channel value
   * @static
   */
  static public function blendModeLinearBurn( $a , $b )
  {
    return $a + $b - 255;
  }
  
  /**
   * Constant for lighten mode
   * 
   * @var string
   * @const
   */
  const LIGHTEN = 'lighten';
  
  /**
   * Lighten blend mode
   * 
   * @param integer $a Color channel value for bottom layer
   * @param integer $b Color channel value for top layer
   * @return integer New Color channel value
   * @static
   */
  static public function blendModeLighten( $a , $b )
  {
    if( $a > $b ) return $a;
    return $b; 
  }
  
  /**
   * Constant for screen mode
   * 
   * @var string
   * @const
   */
  const SCREEN = 'screen';
  
  /**
   * Screen blend mode
   * 
   * @param integer $a Color channel value for bottom layer
   * @param integer $b Color channel value for top layer
   * @return integer New Color channel value
   * @static
   */
  static public function blendModeScreen( $a , $b )
  {
    return 255 - ( ( 255 - $a ) * ( 255 - $b ) / 255 ); 
  }
  
  /**
   * Constant for color dodge mode
   * 
   * @var string
   * @const
   */
  const COLORDODGE = 'color_dodge';
  
  /**
   * Color dodge blend mode
   * 
   * @param integer $a Color channel value for bottom layer
   * @param integer $b Color channel value for top layer
   * @return integer New Color channel value
   * @static
   */
  static public function blendModeColorDodge( $a , $b )
  {
    // avoid 0 divisor
    if( $b === 255 ) return 255;
    
    return 255 * $a / ( 255 - $b ); 
  }
  
  /**
   * Constant for linear dodge mode
   * 
   * @var string
   * @const
   */
  const LINEARDODGE = 'linear_dodge';
  
  /**
   * Linear dodge blend mode
   * 
   * @param integer $a Color channel value for bottom layer
   * @param integer $b Color channel value for top layer
   * @return integer New Color channel value
   * @static
   */
  static public function blendModeLinearDodge( $a , $b )
  {
    return $a + $b; 
  }
  
  /**
   * Constant for overlay mode
   * 
   * @var string
   * @const
   */
  const OVERLAY = 'overlay';
  
  /**
   * Overlay blend mode
   * 
   * @param integer $a Color channel value for bottom layer
   * @param integer $b Color channel value for top layer
   * @return integer New Color channel value
   * @static
   */
  static public function blendModeOverlay( $a , $b )
  {
    if( $a > 127 ) return 255 - ( 2 * ( 255 - $a ) * ( 255 - $b ) / 255 );
    return 2 * $a * $b / 255;
  }
  
  /**
   * Constant for soft light
   * 
   * @var string
   * @const
   */
  const SOFTLIGHT = 'soft_light';
  
  /**
   * Soft light blend mode
   * 
   * @param integer $a Color channel value for bottom layer
   * @param integer $b Color channel value for top layer
   * @return integer New Color channel value
   * @static
   */
  static public function blendModeSoftLight( $a , $b )
  {
    if( $b > 127 ) return 255 - ( ( 255 - $a ) * ( 255 + 127.5 - $b ) / 255 );
    return $a * ( $b + 127.5 ) / 255;
  }
  
  /**
   * Constant for hard light
   * 
   * @var string
   * @const
   */
  const HARDLIGHT = 'hard_light';
  
  /**
   * Hard light blend mode
   * 
   * @param integer $a Color channel value for bottom layer
   * @param integer $b Color channel value for top layer
   * @return integer New Color channel value
   * @static
   */
  static public function blendModeHardLight( $a , $b )
  {
    return static::blendModeOverlay( $b , $a );
  }
  
  /**
   * Constant for vivid light
   * 
   * @var string
   * @const
   */
  const VIVIDLIGHT = 'vivid_light';
  
  /**
   * Vivid light blend mode
   * 
   * The second formula is not the one implemented in Photoshop...
   * Unable to found correct one.
   * 
   * @param integer $a Color channel value for bottom layer
   * @param integer $b Color channel value for top layer
   * @return integer New Color channel value
   * @static
   */
  static public function blendModeVividLight( $a , $b )
  {
    // note that b cannot be 127.5, so we will never have 0 divisor
    
    if( $b > 127 ) return 255 - ( ( 255 - $a ) / ( ( 2 * $b ) - 255 ) );
    return 255 * $a / ( 255 - ( 2 * $b ) );
  }
  
  /**
   * Constant for linear light
   * 
   * @var string
   * @const
   */
  const LINEARLIGHT = 'linear_light';
  
  /**
   * Linear light blend mode
   * 
   * The both formulas have same resolutions. 
   * A + 2 ( B - 1/2 ) = A + 2B - 1
   * Probably a result of approximative calculation...
   * 
   * @param integer $a Color channel value for bottom layer
   * @param integer $b Color channel value for top layer
   * @return integer New Color channel value
   * @static
   */
  static public function blendModeLinearLight( $a , $b )
  {
    return $a + ( 2 * $b ) - 255;
  }
  
  /**
   * Constant for pin light
   * 
   * @var string
   * @const
   */
  const PINLIGHT = 'pin_light';
  
  /**
   * Pin light blend mode
   * 
   * @param integer $a Color channel value for bottom layer
   * @param integer $b Color channel value for top layer
   * @return integer New Color channel value
   * @static
   */
  static public function blendModePinLight( $a , $b )
  {
    if( $b > 127 )
    {
      $c = ( 2 * $b ) - 255;
      if( $c < $a ) return $a;
      return $c;
    }
    else
    {
      $c = 2 * $b;
      if( $c > $a ) return $a;
      return $c;
    }
  }
  
  /**
   * Constant for difference mode
   * 
   * @var string
   * @const
   */
  const DIFFERENCE = 'difference';
  
  /**
   * Difference blend mode
   * 
   * @param integer $a Color channel value for bottom layer
   * @param integer $b Color channel value for top layer
   * @return integer New Color channel value
   * @static
   */
  static public function blendModeDifference( $a , $b )
  {
    if( $a > $b ) return $a - $b;
    return $b - $a;
  }
  
  /**
   * Constant for exclusion mode
   * 
   * @var string
   * @const
   */
  const EXCLUSION = 'exclusion';
  
  /**
   * Exclusion blend mode
   * 
   * @param integer $a Color channel value for bottom layer
   * @param integer $b Color channel value for top layer
   * @return integer New Color channel value
   * @static
   */
  static public function blendModeExclusion( $a , $b )
  {
    return 127.5 - ( ( ( 2 * $a ) - 255 ) * ( ( 2 * $b ) - 255 ) / ( 2 * 255 ) );
  }
}
