<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Base transformation used to apply blend mode over a simple color.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends \GDImage\Transform_Blend_Abstract
 * @implements \GDImage\Transform_Interface
 */
class Transform_Blend_Color extends Transform_Blend_Abstract
{
  
	/**
	 * \GDImage\Color instance to use
	 *
	 * @var \GDImage\Color
	 * @access protected
	 */
	public $_color = null;
	
	/**
	 * Method to assign blend color.
	 *
	 * @param mixed $color Color
	 * @return void
	 * @access public
	 */
	public function setColor( $color )
	{
    // assign instance
    if( $color instanceof Color ) $this->_color = $color;
    // do it mixed
    else $this->_color = new Color( $color );
	}
  
	/**
	 * Constructor
	 *
	 * @param [mixed] $color Color
	 * @param [mixed] $mode Blend mode
   * @return void
   * @access public
	 */
	public function __construct( $color = null , $mode = null )
	{
    // for class extension, it is preferable to never automatically set properties on constructor
		if( $color !== null ) $this->setColor( $color );
    if( $mode !== null ) $this->setMode( $mode );
	}
	
	/**
	 * Magic method call on clone.
	 *
	 * @param none
	 * @return void
	 * @access public
	 */
	public function __clone()
	{
		if( $this->_color ) $this->_color = clone $this->_color;
	}
  	
  /**
   * Apply transformation to a resource.
   * Return false if the transformation fails.
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Transform_Interface
   */
  public function __invoke( Resource_Abstract &$rsc )
  {
    // no color, cannot apply
    if( ! $this->_color ) return false;
    
    // determine function to use
    if( $rsc->isTrueColor() )
    {
      return $this->_blendTrueColor( $rsc );
    }
    else
    {
      return $this->_blendPaletteColor( $rsc );
    }
  }
	
  /**
   * Method used to apply blend transformation on true color resource.
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access protected
   */
  public function _blendTrueColor( Resource_Abstract &$rsc )
  {
		// each pixel needs to be transform in pixel of new color
    
    // disable alpha blending
  	$rsc->unsetMode( Resource_Abstract::MODE_ALPHABLENDING );
    
    // shortcuts
    list( $r , $g , $b ) = $this->_color->toArray();
    
    // do not process calculation twice
    $calc = array();
    
		$flag = true;
    $x = $rsc->getWidth();
    $ymax = $rsc->getHeight();
    while( $x-- )
    {
      $y = $ymax;
      while( $y-- )
      {
        // fetch pixel color
        $c = $rsc->getColorAt( $x , $y );
      
        $key = $r.','.$c->getRed();
        if( empty($calc[$key]) ) $calc[$key] = call_user_func( $this->_mode , $r , $c->getRed() );
        $c->setRed( $calc[$key] );

        $key = $g.','.$c->getGreen();
        if( empty($calc[$key]) ) $calc[$key] = call_user_func( $this->_mode , $g , $c->getGreen() );
        $c->setGreen( $calc[$key] );

        $key = $b.','.$c->getBlue();
        if( empty($calc[$key]) ) $calc[$key] = call_user_func( $this->_mode , $b , $c->getBlue() );
        $c->setBlue( $calc[$key] );
        
        // assign new color
		  	$flag = $rsc->setColorAt( $c , $x , $y ) && $flag;
      }
    }
    
		return $flag;
  }
	
  /**
   * Method used to apply blend transformation on palette color resource.
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access protected
   */
  public function _blendPaletteColor( Resource_Abstract &$rsc )
  {
    // shortcuts
    list( $r , $g , $b ) = $this->_color->toArray();
    
    
    // if we can use imagecolorset() with alpha channel, just do it
    if( version_compare( PHP_VERSION , '5.4.0' , '>=' ) )
    {
      // do not process calculation twice
      $calc = array();
      
      for( $i = 0, $imax = $rsc->countColors(); $i < $imax; $i++ )
      {
        // fetch color
        $c = $rsc->getColor( $i );

        // transform it
        $key = $r.','.$c->getRed();
        if( empty($calc[$key]) ) $calc[$key] = call_user_func( $this->_mode , $r , $c->getRed() );
        $c->setRed( $calc[$key] );

        $key = $g.','.$c->getGreen();
        if( empty($calc[$key]) ) $calc[$key] = call_user_func( $this->_mode , $g , $c->getGreen() );
        $c->setGreen( $calc[$key] );

        $key = $b.','.$c->getBlue();
        if( empty($calc[$key]) ) $calc[$key] = call_user_func( $this->_mode , $b , $c->getBlue() );
        $c->setBlue( $calc[$key] );

        // assign it at same index
        imagecolorset( $rsc->getGdResource() , $i , $c->getRed() , $c->getGreen() , $c->getBlue() , $c->getAlpha() );
      }
      
      return true;
    }
    
    
    // if not, we may be able to use imagecolorset(), but only if there is one
    // color with alpha channel: the transparent one
    
    // keep in mind transparent color index
    $t = imagecolortransparent( $rsc->getGdResource() );
    
    $alpha = false;
    for( $i = 0, $imax = $rsc->countColors(); $i < $imax; $i++ )
    {
      if( $i === $t ) continue;
      
      // fetch color
      $c = $rsc->getColor( $i );
      
      if( $c->getAlpha() )
      {
        $alpha = true;
        break;
      }
    }
    
    if( ! $alpha )
    {
      // we can use imagecolorset() because there is only one transparent color
      // note that this is applyable because alpha channel is not modified by blend mode
      
      // do not process calculation twice
      $calc = array();
      
      for( $i = 0, $imax = $rsc->countColors(); $i < $imax; $i++ )
      {
        // fetch color
        $c = $rsc->getColor( $i );

        // transform it
        $key = $r.','.$c->getRed();
        if( empty($calc[$key]) ) $calc[$key] = call_user_func( $this->_mode , $r , $c->getRed() );
        $c->setRed( $calc[$key] );

        $key = $g.','.$c->getGreen();
        if( empty($calc[$key]) ) $calc[$key] = call_user_func( $this->_mode , $g , $c->getGreen() );
        $c->setGreen( $calc[$key] );

        $key = $b.','.$c->getBlue();
        if( empty($calc[$key]) ) $calc[$key] = call_user_func( $this->_mode , $b , $c->getBlue() );
        $c->setBlue( $calc[$key] );

        // assign it at same index
        imagecolorset( $rsc->getGdResource() , $i , $c->getRed() , $c->getGreen() , $c->getBlue() );
      }
      
      return true;
    }
    
    
    // using color deallocation and reallocation does not provide good results:
    // it seems that GD automatically detect if the color is not present in the 
    // palette and will not allocate an existing color, a bug may occurs with 
    // this color non-allocation
    
    // doing a palette copy/paste also fails:
    // sometimes, GD reassing pixels color, that is really unexpected
    
    // via true color, the palette may be reduced to a minimal set, and we do 
    // not want that
    
    // the only way to achieve a strong palette manipulation is to create the 
    // new palette, then copying each pixel index to a brand new resource
    
    $flag = true;
    
    // create temporary resource
    $tmp = Resource_PaletteColor::createFromSize( $rsc->getWidth() , $rsc->getHeight() );
    
    
    // create the new palette on this innocent resource
    
    // do not process calculation twice
    $calc = array();
    $flag = true;
    
    // preserve color indexes in the palette
    for( $i = 0, $imax = $rsc->countColors(); $i < $imax; $i++ )
    {
      // fetch current color
      $c = $rsc->getColor( $i );
      
      $key = $r.','.$c->getRed();
      if( empty($calc[$key]) ) $calc[$key] = call_user_func( $this->_mode , $r , $c->getRed() );
      $c->setRed( $calc[$key] );
      
      $key = $g.','.$c->getGreen();
      if( empty($calc[$key]) ) $calc[$key] = call_user_func( $this->_mode , $g , $c->getGreen() );
      $c->setGreen( $calc[$key] );
      
      $key = $b.','.$c->getBlue();
      if( empty($calc[$key]) ) $calc[$key] = call_user_func( $this->_mode , $b , $c->getBlue() );
      $c->setBlue( $calc[$key] );
      
      // strangely, duplicate colors seems to get new color index on this innocent resource...
      $flag = ( imagecolorallocatealpha( $tmp->getGdResource() , $c->getRed() , $c->getGreen() , $c->getBlue() , $c->getAlpha() ) !== false ) && $flag;
    }
    
    // propagate transparent color
    if( ( $t = imagecolortransparent( $rsc->getGdResource() ) ) > -1 )
    {
      // do it with index of the transparent color
      $flag = ( imagecolortransparent( $tmp->getGdResource() , $t ) > -1 ) && $flag;
    }
 
    
    // then copy each pixels to their destination
    
    $x = $rsc->getWidth();
    $ymax = $rsc->getHeight();
    while( $x-- )
    {
      $y = $ymax;
      while( $y-- )
      {
        // fetch color index
        $i = imagecolorat( $rsc->getGdResource() , $x , $y );
        
        // assign color index
		  	$flag = imagesetpixel( $tmp->getGdResource() , $x , $y , $i ) && $flag;
      }
    }
    
    // replace original resource
    $rsc = $tmp;
    
    // Quand tu serres ton corps, tout contre mon corps
		return $flag;
  }
  
}
