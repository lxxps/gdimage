<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Transformation used to apply a crop resizing on a image.
 * A crop resizing resize and crop image to make it cover the entierly
 * resize area.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @implements \GDImage\Transform_Interface
 * @extends    \GDImage\Transform_Resize_Abstract
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Transform_Resize_Crop extends Transform_Resize_Abstract
{
	
  /**
   * Method used to calculate and set resizing propreties helping
   * source resource.
   *
   * @param \GDImage\Resource_Abstract $rsc
   * @return void
   * @access protected
   * @implements \GDImage\Transform_Resize_Abstract
   */
  public function _parameterization( Resource_Abstract $rsc )
  {
  	// Set destination propreties
  	$this->__dx = 0;
  	$this->__dy = 0;
  	$this->__dw = $this->_w;
  	$this->__dh = $this->_h;
  	
  	// Determine source propreties
  	$ow = $rsc->getWidth();
  	$oh = $rsc->getHeight();
    
  	$ratio = min( $oh / $this->_h , $ow / $this->_w );
		$this->__sx = ceil( ( ( $ow / $ratio ) - $this->_w ) / 2 * $ratio );
		$this->__sy = ceil( ( ( $oh / $ratio ) - $this->_h ) / 2 * $ratio );
		$this->__sw = $this->_w * $ratio;
		$this->__sh = $this->_h * $ratio;
  }
  
}
