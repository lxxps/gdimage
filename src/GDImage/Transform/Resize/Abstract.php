<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration setting "resize_default_resampling".
 */
if( ! Config::hasResizeDefaultResampling() ) {
  Config::setResizeDefaultResampling( /** / 'imagecopyresized' /**/ 'imagecopyresampled' /**/ );
}

/**
 * Base transformation used to resize an image.
 * 
 * Method _prepareParameters( $src ) is abstracted and should manipulate __dx,
 * __dy, __dw, __dh, __sx, __sy, __sw, __sh properties in order to apply resizing 
 * function (@see http://php.net/manual/fr/function.imagecopyresampled.php
 * and http://php.net/manual/fr/function.imagecopyresized.php).
 * 
 * Method _createDestination( $src ) is abstracted and should assign __dst property 
 * with a resource used as destination.
 * 
 * Default resizing method is imagecopyresampled(), but it can be changed 
 * using setResampling() method.
 * 
 * For simplification, this method accept any callable argument. Arguments 
 * passed to this callable will be the same than imagecopyresampled() and 
 * imagecopyresized() function.
 * 
 * After a lot of reflection, it appears that no strategy pattern can be used
 * on various resizing because of additionnal parameter that should be added.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Transform_Interface
 */
abstract class Transform_Resize_Abstract implements Transform_Interface
{
	/**
	 * Constant to represent used of imagecopyresized() function
	 *
	 * @var callable Resampling method
	 * @const
	 */
	const RESAMPLING_NONE = 'imagecopyresized';
	
	/**
	 * Constant to represent used of imagecopyresampled() function.
	 * Note that resampling alpha channels colors can alter these colors.
	 *
	 * @var callable Resampling method
	 * @const
	 */
	const RESAMPLING_NORMAL = 'imagecopyresampled';
	
  /**
   * Resize width.
   *
   * @var integer Width
   * @access protected
   */
  public $_w;
   
  /**
   * Resize height.
   *
   * @var integer Height
   * @access protected
   */
  public $_h;
   
  /**
   * Resampling method.
   *
   * @var callable Resampling method
   * @access protected
   */
  public $_resampling;
  
  /**
   * Temporary resource.
   *
   * @var \GDImage\Resource_Abstract Resource
   * @access protected
   */
  public $__dst;
  
  /**
   * Temporary destination X position.
   *
   * @var integer X position
   * @access protected
   */
  public $__dx;
  
  /**
   * Temporary destination Y position.
   *
   * @var integer Y position
   * @access protected
   */
  public $__dy;
  
  /**
   * Temporary source X position.
   *
   * @var integer X position
   * @access protected
   */
  public $__sx;
  
  /**
   * Temporary source Y position.
   *
   * @var integer Y position
   * @access protected
   */
  public $__sy;
  
  /**
   * Temporary destination width.
   *
   * @var integer Width
   * @access protected
   */
  public $__dw;
   
  /**
   * Temporary destination height.
   *
   * @var integer Height
   * @access protected
   */
  public $__dh;
  
  /**
   * Temporary source width.
   *
   * @var integer Width
   * @access protected
   */
  public $__sw;
   
  /**
   * Temporary source height.
   *
   * @var integer Height
   * @access protected
   */
  public $__sh;
  
	/**
	 * Method to set width and height resize.
	 *
	 * @param integer $w Resize width
	 * @param integer $h Resize height
   * @return void
   * @access public
	 */
	public function setSize( $w , $h )
	{
		$this->_w = $w | 0; // force to int
		$this->_h = $h | 0; // force to int
	}
  
	/**
	 * Method to set resampling callback.
	 *
	 * @param string $resampling Resampling callback
	 * @return void
	 * @access public
	 */
	public function setResampling( $resampling )
	{
    if( ! is_callable( $resampling ) )
    {
      throw new Exception_Transform( array( get_class( $this ) ,  $resampling ) , 5010 );
    }
		$this->_resampling = $resampling;
	}
  
	/**
	 * Constructor
	 *
	 * @param [integer] $w Resize width
	 * @param [integer] $h Resize height
   * @return void
   * @access public
	 */
	public function __construct( $w = null , $h = null )
	{
    // for class extension, it is preferable to never automatically set properties on constructor
		if( $w !== null || $h !== null ) $this->setSize( $w , $h );
    
    // initialize default value from configuration
    $this->setResampling( Config::getResizeDefaultResampling() );
	}
  	
  /**
   * Apply transformation to a resource.
   * Return false if the transformation fails.
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Transform_Interface
   */
  public function __invoke( Resource_Abstract &$rsc )
  {
    // no width or height, cannot apply
    if( ! ( $this->_w > 0 && $this->_h > 0 ) ) return false;
    
    // prepare parameters
    
    $this->_parameterization( $rsc );
  
    // prepare destination
    
    $this->_prepareDestination( $rsc );
    
    // resize
    
    // apply resizing helping method
    $resampling = $this->_resampling;
    
    if( $resampling( $this->__dst->getGdResource() , $rsc->getGdResource() , 
                     $this->__dx , $this->__dy , $this->__sx , $this->__sy , 
                     $this->__dw , $this->__dh , $this->__sw , $this->__sh ) )
  	{
      $this->_postpareDestination( $rsc );
      
      // will destroy previous GD resource
  		$rsc = $this->__dst;
    
      // free resource
      unset( $this->__dst );
      
  		return true;
  	}
    
    // free resource
    unset( $this->__dst );
    
    // nothing to do
    return false;
  }
	
  /**
   * Method used to calculate and set resizing propreties helping
   * source resource.
   *
   * @param \GDImage\Resource_Abstract $rsc
   * @return void
   * @access protected
   * @abstract
   */
  abstract public function _parameterization( Resource_Abstract $rsc );
  
  /**
   * Method used to create destination resource.
   * This method is call after calculation of resize propreties.
   *
   * @param \GDImage\Resource_Abstract $rsc
   * @return void
   * @access protected
   */
  public function _prepareDestination( Resource_Abstract $rsc )
  {
    // create blank destination that will preserve transparent color as background
    $this->__dst = $rsc->blank( $this->__dw , $this->__dh );
    
    // in order to optimize resizing/resampling, we must be sure that the 
    // destination is a true color resource
    // we will restore palette color ability after resizing/resampling
    $this->__dst = $this->__dst->toTrueColor();
    
    // enable alpha blending
    $this->__dst->setMode( Resource_Abstract::MODE_ALPHABLENDING );
  }
  
  /**
   * Method used to repare some stuff on destination.
   * May be usefull.
   *
   * @param \GDImage\Resource_Abstract $rsc
   * @return void
   * @access protected
   */
  public function _postpareDestination( Resource_Abstract $rsc )
  {
    // resource is a palette color
    if( $rsc->isPaletteColor() )
    {
      // in that case, make destination a palette color again
      $tmp = $this->__dst->toPaletteColor( 256 , false );
      
      // if we were unable to find any transparent color anymore
      // we may have trouble with AGIF manipulation
      if( $rsc->getTransparent() && ( ! $tmp->getTransparent() ) )
      {
        if( $tmp->countColors() === 256 )
        {
          // redo palette conversion with one slot for the transparent color
          $tmp = $this->__dst->toPaletteColor( 255 , false );
        }
        
        // assign transparent
        $tmp->setTransparent( $rsc->getTransparent() );
      }
      
      // finally assign new destination
      $this->__dst = $tmp;
    }
  }
  
}
