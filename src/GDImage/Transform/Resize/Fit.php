<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Transformation used to apply a fit resizing on a image.
 * A fit resizing resize image to make it enter entierly into the
 * resize area. Blank area are cropped.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @implements \GDImage\Transform_Interface
 * @extends    \GDImage\Transform_Resize_Abstract
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Transform_Resize_Fit extends Transform_Resize_Abstract
{
	
  /**
   * Method used to calculate and set resizing propreties helping
   * source resource.
   *
   * @param \GDImage\Resource_Abstract $rsc
   * @return void
   * @access protected
   * @implements \GDImage\Transform_Resize_Abstract
   */
  public function _parameterization( Resource_Abstract $rsc )
  {
  	// Determine destination propreties
  	$ow = $rsc->getWidth();
  	$oh = $rsc->getHeight();
    
    $ratio = max( $oh / $this->_h , $ow / $this->_w );
		$this->__dx = 0;
		$this->__dy = 0;
  	$this->__dw = min( ceil( $ow / $ratio ) , $this->_w );
		$this->__dh = min( ceil( $oh / $ratio ) , $this->_h );
  	
  	// Set source propreties
		$this->__sx = 0;
		$this->__sy = 0;
		$this->__sw = $ow;
		$this->__sh = $oh;
  }
  
}
