<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Transformation used to apply a full resizing on a image.
 * A full resizing resize image to make it enter entierly into the
 * resize area. Blank area are keeped colored (transparent by default).
 * 
 * Note that alpha blending is turned on before resampling/resizing.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @implements \GDImage\Transform_Interface
 * @extends    \GDImage\Transform_Resize_Fit
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Transform_Resize_FitNFill extends Transform_Resize_Fit
{
	/**
	 * \GDImage\Color instance used for blank area.
	 *
	 * @var \GDImage\Color
	 * @access protected
	 */
	public $_bg;
	
	/**
	 * Method to assign background color.
   * Set it to null to use transparent color.
	 *
	 * @param mixed $bg Color
	 * @return void
	 * @access public
	 */
	public function setBackground( $bg = null )
	{
    // null
    if( $bg === null ) $this->_bg = null;
    // assign instance
    elseif( $bg instanceof Color ) $this->_bg = $bg;
    // do it mixed
    else $this->_bg = new Color( $bg );
	}
	
	/**
	 * Alias for setBackground()
   * 
   * @deprecated
	 * @param mixed $bg Color
	 * @return void
	 * @access public
	 */
	public function setColor( $bg = null )
	{
    $this->setBackground( $bg );
  }
  
	/**
	 * Constructor
	 *
	 * @param [integer] $w Resize width
	 * @param [integer] $h Resize height
	 * @param [mixed] $bg Color to use for fill
   * @return void
   * @access public
	 */
	public function __construct( $w = null , $h = null , $bg = null  )
	{
    // call parent
    parent::__construct( $w , $h );
    // then set color
    // for class extension, it is preferable to never automatically set properties on constructor
    if( $bg !== null ) $this->setBackground( $bg );
	}
	
	/**
	 * Magic method call on clone.
	 *
	 * @param none
	 * @return void
	 * @access public
	 */
	public function __clone()
	{
		if( $this->_bg ) $this->_bg = clone $this->_bg;
	}
	
  /**
   * Method used to calculate and set resizing propreties helping
   * source resource.
   *
   * @param \GDImage\Resource_Abstract $rsc
   * @return void
   * @access protected
   * @implements \GDImage\Transform_Resize_Abstract
   */
  public function _parameterization( Resource_Abstract $rsc )
  {
  	parent::_parameterization( $rsc );
  	
  	// change destination propreties
  	$this->__dx = ceil( ( $this->_w - $this->__dw ) / 2 );
  	$this->__dy = ceil( ( $this->_h - $this->__dh ) / 2 );
  }
  
  /**
   * Method used to create destination resource.
   * This method is call after calculation of resize propreties.
   *
   * @param \GDImage\Resource_Abstract $rsc
   * @return void
   * @access protected
   */
  public function _prepareDestination( Resource_Abstract $rsc )
  {
    // create blank destination that will preserve transparent color as background
    $this->__dst = $rsc->blank( $this->_w , $this->_h );
    
    // disable alpha blending
    $this->__dst->unsetMode( Resource_Abstract::MODE_ALPHABLENDING );
    
    $bg = $this->_bg;
    // nothing, look for transparent
    if( ! $bg )
    {
      $bg = $rsc->getTransparent();
    }
    // nothing, look for default color and assign it as transparent
    if( ! $bg ) 
    {
      $bg = new Color();
      $bg->setAlpha( Color::ALPHA_TRANSPARENT );
      $this->__dst->setTransparent( $bg );
    }
    
    // fill with color
    imagefill( $this->__dst->getGdResource() , 0 , 0 , $this->__dst->setColor( $bg ) );
    
    // in order to optimize resizing/resampling, we must be sure that the 
    // destination is a true color resource
    // we will restore palette color ability after resizing/resampling
    $this->__dst = $this->__dst->toTrueColor();
    
    // enable alpha blending
    $this->__dst->setMode( Resource_Abstract::MODE_ALPHABLENDING );
  }
  
//  /**
//   * Method used to repare some stuff on destination.
//   * May be usefull.
//   *
//   * @param \GDImage\Resource_Abstract $rsc
//   * @return void
//   * @access protected
//   */
//  public function _postpareDestination( Resource_Abstract $rsc )
//  {
//    // resource is a palette color
//    if( $rsc->isPaletteColor() )
//    {
//      // in that case, make destination a palette color again
//      $this->__dst = $this->__dst->toPaletteColor( 256 , false );
//      
//      // this feature provide unexpected result, we may want to reduce palette 
//      // color to 255 size and add original transparent color, if available
//      // need feedback about that
//      
////      // for this resizing, we may want to preserve the transparent color ability
////      // even if the color to use is not transparent
////      // resource has a transparent color but not destination... may not expected
////      if( ( ! $this->__dst->getTransparent() ) && $transparent = $rsc->getTransparent() )
////      {
////        // reduce palette 
////        
////        // we need to restore this transparent color but
////        // resizing or resampling may change some color information
////        // so the transparent color may not be found again
////        // hopefully we can look for a not exact color
////        if( ( $i = $this->__dst->findColor( $transparent , false ) ) !== false )
////        {
////          // set this color as transparent
////          $this->__dst->setTransparent( $this->__dst->getColor( $i ) );
////        }
////      }
//      
//    }
//  }
  
}
