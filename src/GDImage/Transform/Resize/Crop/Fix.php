<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Transformation used to apply a crop resizing on a image.
 * A crop resizing resize and crop image to make it cover the entierly
 * resize area.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @implements \GDImage\Transform_Interface
 * @extends    \GDImage\Transform_Resize_Crop
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Transform_Resize_Crop_Fix extends Transform_Resize_Crop
{
  /**
   * Bitwise value for static from top
   * 
	 * @var integer
	 * @const
   */
  const TOP = 1;
  
  /**
   * Bitwise value for static from bottom
   * 
	 * @var integer
	 * @const
   */
  const BOTTOM = 2;
  
  /**
   * Bitwise value for static from left
   * 
	 * @var integer
	 * @const
   */
  const LEFT = 4;
  
  /**
   * Bitwise value for static from right
   * 
	 * @var integer
	 * @const
   */
  const RIGHT = 8;

  /**
   * Bitwise value for position
   *
   * @var integer
   * @access protected
   */
  protected $_position = 0;
  
  /**
   * Method to set the position bitwise.
   *
   * @param integer $position Bitwise value for static position
   * @return void
   * @access public
   */
  public function setPosition( $position )
  {
    $this->_position = $position | 0;
  }
  
	/**
	 * Constructor
	 *
	 * @param [integer] $w Resize width
	 * @param [integer] $h Resize height
	 * @param [integer] $position Bitwise value for static position
   * @return void
   * @access public
	 */
	public function __construct( $w = null , $h = null , $position = null )
	{
    // call parent
    parent::__construct( $w , $h );
    // then set color
    // for class extension, it is preferable to never automatically set properties on constructor
    if( $position !== null ) $this->setPosition( $position );
	}
	
  /**
   * Method used to calculate and set resizing propreties helping
   * source resource.
   *
   * @param \GDImage\Resource_Abstract $rsc
   * @return void
   * @access protected
   * @implements \GDImage\Transform_Resize_Abstract
   */
  public function _parameterization( Resource_Abstract $rsc )
  {
    parent::_parameterization( $rsc );
    
    // changes source properties
    
    // if there is both left and right, we cannot do anything
    if( ! ( ( $this->_position & self::LEFT ) && ( $this->_position & self::RIGHT ) ) )
    {
      if( $this->_position & self::LEFT )
      {
        $this->__sx = 0;
      }
      if( $this->_position & self::RIGHT )
      {
        $this->__sx = max( $rsc->getWidth() - $this->__sw , 0 );
      }
    }
    
    // if there is both top and bottom, we cannot do anything
    if( ! ( ( $this->_position & self::TOP ) && ( $this->_position & self::BOTTOM ) ) )
    {
      if( $this->_position & self::TOP )
      {
        $this->__sy = 0;
      }
      if( $this->_position & self::BOTTOM )
      {
        $this->__sy = max( $rsc->getHeight() - $this->__sh , 0 );
      }
    }
  }
  
}
