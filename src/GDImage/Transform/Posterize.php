<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Apply posterize transformation on resource.
 * 
 * Note that GD does not support more than 256 colors in a palette.
 * Also, transparent color is one of the posterized colors.
 * 
 * Also, the resource will automatically be converted to a Resource_PaletteColor.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Transform_Interface
 */
class Transform_Posterize implements Transform_Interface
{
  
  /**
   * Number of colors.
   *
   * @var integer
   * @access protected
   */
  public $_colors = 256;
  
	/**
	 * Set number of colors
	 *
	 * @param integer $nb Number of colors to use
   * @return void
   * @access public
	 */
	public function setColors( $nb )
	{
    // force integer
		$this->_colors = $nb | 0;
    // GD silently does not allow more that 256 colors
    if( $this->_colors > 256 ) $this->_colors = 256;
	}
  
	/**
	 * Constructor
	 *
	 * @param [integer] $nb Number of colors to use
   * @return void
   * @access public
	 */
	public function __construct( $nb = null )
	{
    // for class extension, it is preferable to never automatically set properties on constructor
		if( $nb !== null ) $this->setColors( $nb );
	}
  	
  /**
   * Apply transformation to a resource.
   * Return false if the transformation fails.
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Transform_Interface
   */
  public function __invoke( Resource_Abstract &$rsc )
  {
    // no colors, cannot apply
    if( ! ( $this->_colors > 0 ) ) return false;
    
    // in that case, there is nothing to do
    if( $rsc->isPaletteColor() && $rsc->countColors() <= $this->_colors ) return true;
    
    // GD has some bad habits: after manipulation, the same color may 
    // be used more than one time in the palette, that will grow up the palette 
    // with unexpected values, especially if you want to reduce to a small amount 
    // of colors
    
    // so it cannot be done directly from true color to the expected number of
    // colors
    
    // shortcut
    $w = $rsc->getWidth(); $h = $rsc->getHeight();
      
    // make a little approximation about generated colors that will be unusable
    // this initial value make low level posterization slower but more accurate
    $nb = $this->_colors + ( ( ( 256 - $this->_colors ) / 7.5 ) | 0 ); // "| 0" is quicker than floor()
    
    // make sure we won't exceed 256 colors
    if( $nb > 256 ) $nb = 256;
    
    // from true color, always give a first try with 256 colors
    if( $rsc->isTrueColor() )
    {
      $rsc = $rsc->toPaletteColor( 256 , false );
      
      // reduce number of colors
      if( $rsc->countColors() - 1 < $nb ) $nb = $rsc->countColors() - 1;
    }
    
    // check and continue if necessary
    while( $rsc->countColors() > $this->_colors )
    {
      $rsc = $rsc->toTrueColor()->toPaletteColor( $nb , false );
      
      // reduce number of colors
      $nb = $rsc->countColors() - 1;
    }
    
    // done
    return true;
  }
}
