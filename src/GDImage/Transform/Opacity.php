<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Apply an opacity effect on resource.
 * 
 * In fact imagecopymerge() does not work at all, so we cannot place the image 
 * on a fully transparent one.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Transform_Interface
 */
class Transform_Opacity implements Transform_Interface
{
  
  /**
   * Opacity percentage.
   *
   * @var integer
   * @access protected
   */
  public $_opacity = 100;
  
	/**
	 * Method to set opacity.
	 *
	 * @param integer $opacity
   * @return void
   * @access public
	 */
	public function setOpacity( $opacity )
	{
		$this->_opacity = $opacity | 0;
    
    if( $this->_opacity > 100 ) $this->_opacity = 100;
    if( $this->_opacity < 0 ) $this->_opacity = 0;
	}
  
	/**
	 * Constructor
	 *
	 * @param [integer] $opacity
   * @return void
   * @access public
	 */
	public function __construct( $opacity = null )
	{
    // for class extension, it is preferable to never automatically set properties on constructor
		if( $opacity !== null ) $this->setOpacity( $opacity );
	}
  	
  /**
   * Apply transformation to a resource.
   * Return false if the transformation fails.
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Transform_Interface
   */
  public function __invoke( Resource_Abstract &$rsc )
  {
    // 100%, nothing to do
    if( $this->_opacity === 100 ) return true;
    
    // in fact imagecopymerge() does not work has expected, so we cannot paste
    // the picture with a certain opacity to a blank area
    
//    // create blank resource
//    $tmp = $rsc->blank();
//    
//    // not sure about that
//    $tmp->unsetMode( Resource_Abstract::MODE_ALPHABLENDING );
//    
//    // if it fail, we fail
//    if( ! imagecopymerge( $tmp->getGdResource() , $rsc->getGdResource() , 0 , 0 , 0 , 0 , $rsc->getWidth() , $rsc->getHeight() , $this->_opacity ) ) return false;
//    
//    // will destroy previous GD resource
//    $rsc = $tmp;
//    
//    // done
//    return true;
    
    // determine function to use
    if( $rsc->isTrueColor() )
    {
      return $this->_opacityTrueColor( $rsc );
    }
    else
    {
      return $this->_opacityPaletteColor( $rsc );
    }
  }
	
  /**
   * Method used to apply opacity transformation on true color image.
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access protected
   */
  public function _opacityTrueColor( Resource_Abstract &$rsc )
  {
		// each pixel needs to be transform in pixel of new color
    
    // disable alpha blending
  	$rsc->unsetMode( Resource_Abstract::MODE_ALPHABLENDING );
   
    $xmax = $rsc->getWidth();
    $ymax = $rsc->getHeight();
    
    // a first loop to determine min alpha
    $max = Color::ALPHA_TRANSPARENT;
    $min = $max;
    
    $x = $xmax;
    while( $x-- )
    {
      $y = $ymax;
      while( $y-- )
      {
        // fetch pixel color
        $c = $rsc->getColorAt( $x , $y );
        
        if( $c->getAlpha() < $min ) $min = $c->getAlpha();
        
        // if alpha is 0, we can stop rght now
        if( ! $min ) break 2;
      }
    }
    
    // now, we can manage alpha calculation
    
    // the entire picture is transparent, nothing to do
    if( $min === $max ) return true;
    
		$flag = true;
    // for the calculation
    // @see http://php.net/manual/fr/function.imagecopymerge.php#88456
    $ratio = $max * ( $this->_opacity / 100 ) / ( $max - $min );
    
    $x = $xmax;
    while( $x-- )
    {
      $y = $ymax;
      while( $y-- )
      {
        // fetch pixel color
        $c = $rsc->getColorAt( $x , $y );
        
        // fully transparent, nothing to do
        if( $c->getAlpha() === $max ) continue;
        
        $c->setAlpha( $max + ( $ratio * ( $c->getAlpha() - $max ) ) );
        
        $flag = $rsc->setColorAt( $c , $x , $y );
      }
    }
    
		return $flag;
  }
	
  /**
   * Method used to apply opacity transformation on palette color image.
   * For palette image there is no need to transform every pixels, 
   * we just transform the color palette.
   * 
   * Note: On PHP 5.4, we can use imagecolorset() with alpha channel.
   * @see http://php.net/manual/en/function.imagecolorset.php
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access protected
   */
  public function _opacityPaletteColor( Resource_Abstract &$rsc )
  {
    // a first loop to determine min alpha
    $max = Color::ALPHA_TRANSPARENT;
    $min = $max;
    
    // keep color indexes in the palette
    for( $i = 0, $imax = $rsc->countColors(); $i < $imax; $i++ )
    {
      // fetch color
      $c = $rsc->getColor( $i );
        
      if( $c->getAlpha() < $min ) $min = $c->getAlpha();
        
      // if alpha is 0, we can stop rght now
      if( ! $min ) break;
    }
    
    // now, we can manage alpha calculation
    
    // the entire picture is transparent, nothing to do
    if( $min === $max ) return true;
    
    // for the calculation
    // @see http://php.net/manual/fr/function.imagecopymerge.php#88456
    $ratio = $max * ( $this->_opacity / 100 ) / ( $max - $min );
    
    
    // if we can use imagecolorset() with alpha channel, just do it
    if( version_compare( PHP_VERSION , '5.4.0' , '>=' ) )
    {
      // do not process calculation twice
      $calc = array();
      
      for( $i = 0, $imax = $rsc->countColors(); $i < $imax; $i++ )
      {
        // fetch color
        $c = $rsc->getColor( $i );
        
        // determine new alpha
        $c->setAlpha( $max + ( $ratio * ( $c->getAlpha() - $max ) ) );

        // assign it at same index
        imagecolorset( $rsc->getGdResource() , $i , $c->getRed() , $c->getGreen() , $c->getBlue() , $c->getAlpha() );
      }
      
      return true;
    }
    
    
    // using color deallocation and reallocation does not provide good results:
    // it seems that GD automatically detect if the color is not present in the 
    // palette and will not allocate an existing color, a bug may occurs with 
    // this color non-allocation
    
    // doing a palette copy/paste also fails:
    // sometimes, GD reassing pixels color, that is really unexpected
    
    // via true color, the palette may be reduced to a minimal set, and we do 
    // not want that
    
    // the only way to achieve a strong palette manipulation is to create the 
    // new palette, then copying each pixel index to a brand new resource
    
    $flag = true;
    
    // create temporary resource
    $tmp = Resource_PaletteColor::createFromSize( $rsc->getWidth() , $rsc->getHeight() );
    
    
    // create the new palette on this innocent resource
    
    // do not process calculation twice
    $calc = array();
    
    // preserve color indexes in the palette
    for( $i = 0, $imax = $rsc->countColors(); $i < $imax; $i++ )
    {
      // fetch current color
      $c = $rsc->getColor( $i );
        
      // determine new alpha
      $c->setAlpha( $max + ( $ratio * ( $c->getAlpha() - $max ) ) );
      
      // strangely, duplicate colors seems to get new color index on this innocent resource...
      $flag = ( imagecolorallocatealpha( $tmp->getGdResource() , $c->getRed() , $c->getGreen() , $c->getBlue() , $c->getAlpha() ) !== false ) && $flag;
    }
    
    // propagate transparent color
    if( ( $t = imagecolortransparent( $rsc->getGdResource() ) ) > -1 )
    {
      // do it with index of the transparent color
      $flag = ( imagecolortransparent( $tmp->getGdResource() , $t ) > -1 ) && $flag;
    }
 
    
    // then copy each pixels to their destination
    
    $x = $rsc->getWidth();
    $ymax = $rsc->getHeight();
    while( $x-- )
    {
      $y = $ymax;
      while( $y-- )
      {
        // fetch color index
        $i = imagecolorat( $rsc->getGdResource() , $x , $y );
        
        // assign color index
		  	$flag = imagesetpixel( $tmp->getGdResource() , $x , $y , $i ) && $flag;
      }
    }
    
    // replace original resource
    $rsc = $tmp;
    
    // And she will be loved
		return $flag;
  }
}
