<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Transformation used to apply basic sharpen effect.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Transform_Interface
 * @extends    \GDImage\Transform_Convolution
 */
class Transform_Convolution_Sharpen extends Transform_Convolution
{
	/**
	 * Convolution 3x3 matrix
	 *
	 * @var array Matrix
	 * @access protected
	 */
	public $_matrix = array(
	  array( -1 , -1 , -1 ),
	  array( -1 , 9 , -1 ),
	  array( -1 , -1 , -1 ),
	);
}
