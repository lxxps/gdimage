<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Transformation used to apply negate effect.
 * 
 * Note that we do not use systematically imagefilter(), in order
 * to stay compatible with child of this class (no, we will not use final 
 * keyword).
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Transform_Interface
 * @extends    \GDImage\Transform_Convolution
 */
class Transform_Convolution_Negate extends Transform_Convolution
{
	/**
	 * Convolution divisor
	 *
	 * @var float Divisor
	 * @access protected
	 */
	public $_divisor = -1;
	
	/**
	 * Convolution automatic divisor flag
	 *
	 * @var boolean
	 * @access protected
	 */
	public $_auto_divisor = false;
	
	/**
	 * Convolution offset
	 *
	 * @var float Offset
	 * @access protected
	 */
	public $_offset = 255;
  
  /**
   * Apply convolution over the entire picture.
   *
   * @param \GDImage\Resource_Abstract $src Source
   * @return boolean Success flag
   * @access protected
   */
  public function _convolution( Resource_Abstract $src )
  {
    // look if it is really a negate convolution
    // note that there is no need to check automatic divisor
    if( $this->_offset === 255 
        // all part of the matrix, except center must be 0
     && $this->_matrix[0][0] === 0 && $this->_matrix[0][1] === 0 && $this->_matrix[0][2] === 0  
     && $this->_matrix[1][0] === 0                               && $this->_matrix[1][2] === 0   
     && $this->_matrix[2][0] === 0 && $this->_matrix[2][1] === 0 && $this->_matrix[2][2] === 0 
        // center must be equal to -1 * divisor
     &&  $this->_matrix[1][1] === -1 * $this->_divisor )
    {
      // ok, we can use imagefilter()
      return imagefilter( $this->__tmp->getGdResource() , \IMG_FILTER_NEGATE ); 
    }
    
    // fallback to parent method
    return parent::_convolution( $src );
  }
}
