<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration settings "convolution_default_alpha"
 * and "convolution_default_edge".
 */
if( ! Config::hasConvolutionDefaultAlphaMode() ) {
  Config::setConvolutionDefaultAlphaMode( 'calculate' );
}
if( ! Config::hasConvolutionDefaultEdgeMode() ) {
  Config::setConvolutionDefaultEdgeMode( 'extend' );
}

/**
 * Real transformation used to manage image convolution.
 *
 * Provide folowing edge mode that are apply when the matrix takes pixels
 * out of image:
 *  - EDGE_EXTEND: study pixels from the initial row or column (default);
 *  - EDGE_WRAP: study pixels from the opposite edge;
 *  - EDGE_CROP: if have to study pixel out of image,
 *    crop the initial pixel instead;
 *  - EDGE_TRANSPARENT: if have to study pixel out of image,
 *    apply transparent color instead.
 *
 * Provide folowing alpha channels mode for convolution:
 *  - ALPHA_NONE: to not apply alpha correction, do not use this mode on image with alpha channel;
 *  - ALPHA_NO: alpha channels are removed;
 *  - ALPHA_PRESERVE: alpha channels still the same that initial pixel;
 *  - ALPHA_CALCULATE: alpha channels are calculated helping convolution (default).
 * 
 * This class is huge and should probably get some Strategy pattern, but they are 
 * pretty difficult to implement for the reason that each edge and alpha interact
 * beetween each other.
 * As far as we go, we identify 6 distinct methods :
 * - _convolutionForEdge*: Apply the edge manipulation
 * - _prepareSourceColorsForEdge* (with second argument to true): Prepare a 2D color 
 *   representation for the edge only, used on alpha "preserve" and "calculate" cases,
 *   also used on 
 * - _prepareSourceColorsForEdge* (with second argument to false): Prepare a 2D color 
 *   representation for the content and the edge, used on alpha "preserve" and 
 *   "calculate" cases
 * - _convolutionForAlpha*: Apply alpha channel correction on the entire picture
 * - _applyMatrixForAlpha*: Apply alpha calculation on a colors matrix, used on 
 *   edge "wrap" case ("extend" case is the default imageconvolution() behavior, 
 *   "crop" and "transparent" does not need calculation)
 * - _applyMatrixAtForAlpha*: Apply alpha calculation on a colors matrix determinated 
 *   by a position, used on edge "extend", "crop" and "transparent" that requires only 
 *   an alpha correction on edge, also used on alpha "calculate" case
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends    \GDImage\Transform_Convolution_Basic
 * @implements \GDImage\Transform_Interface
 */
class Transform_Convolution_Advanced extends Transform_Convolution_Basic
{
  
  // edge and alpha
  
	/**
	 * Constant to represent extended edge mode
   * (study pixels from the current edge)
	 *
	 * @var string Mode
	 * @const
	 */
	const EDGE_EXTEND = 'extend';
	
	/**
	 * Constant to represent wrapped edge mode
   * (study pixels from the opposite edge)
	 *
	 * @var string Mode
	 * @const
	 */
	const EDGE_WRAP = 'wrap';
	
	/**
	 * Constant to represent cropped edge mode
   * (crop pixels that study out)
	 *
	 * @var string Mode
	 * @const
	 */
	const EDGE_CROP = 'crop';
	
	/**
	 * Constant to represent transparent edge mode
   * (pixels that study out are replaced by the transparent color)
	 *
	 * @var string Mode
	 * @const
	 */
	const EDGE_TRANSPARENT = 'transparent';
	
//	/**
//	 * Constant to represent weird alpha channels mode
//	 * (imagecovolution alpha channel management, do some strange thing
//   * but quicker on image without alpha channel)
//	 *
//	 * @var string Mode
//	 * @const
//	 */
//	const ALPHA_GD = 'gd';
	
	/**
	 * Constant to represent no alpha channels mode
	 * (alpha channels are removed)
	 *
	 * @var string Mode
	 * @const
	 */
	const ALPHA_NO = 'no';
	
	/**
	 * Constant to represent preserved alpha channels mode
	 * (alpha channels still the same that initial pixel)
	 *
	 * @var string Mode
	 * @const
	 */
	const ALPHA_PRESERVE = 'preserve';
	
	/**
	 * Constant to represent calculated alpha channels mode
	 * (alpha channels are calculate helping current convolution,
	 * where offset is just divided by two)
	 *
	 * @var string Mode
	 * @const
	 */
	const ALPHA_CALCULATE = 'calculate';
	
	/**
	 * Convolution edge mode
	 *
	 * @var string Mode
	 * @access protected
	 */
	public $_edge;
	
	/**
	 * Convolution alpha channel mode
	 *
	 * @var string Mode
	 * @access protected
	 */
	public $_alpha;
	
	/**
	 * Method to assign edge mode.
	 *
	 * @param string $mode Mode
	 * @return void
	 */
	public function setEdgeMode( $mode )
	{
    if( ! method_exists( $this , '_convolutionForEdge'.Config::camelize( $mode ) ) )
    {
      throw new Exception_Transform( array( get_class( $this ) , Config::camelize( $mode ) , $mode ) , 5025 );
    }
    if( ! method_exists( $this , '_prepareSourceColorsForEdge'.Config::camelize( $mode ) ) )
    {
      throw new Exception_Transform( array( get_class( $this ) , Config::camelize( $mode ) , $mode ) , 5026 );
    }
		$this->_edge = $mode;
	}
	
	/**
	 * Method to assign alpha channel mode.
	 *
	 * @param string $mode Mode
	 * @return void
	 */
	public function setAlphaMode( $mode )
	{
    if( ! method_exists( $this , '_convolutionForAlpha'.Config::camelize( $mode ) ) )
    {
      throw new Exception_Transform( array( get_class( $this ) , Config::camelize( $mode ) , $mode ) , 5020 );
    }
    if( ! method_exists( $this , '_applyMatrixForAlpha'.Config::camelize( $mode ) ) )
    {
      throw new Exception_Transform( array( get_class( $this ) , Config::camelize( $mode ) , $mode ) , 5021 );
    }
    if( ! method_exists( $this , '_applyMatrixAtForAlpha'.Config::camelize( $mode ) ) )
    {
      throw new Exception_Transform( array( get_class( $this ) , Config::camelize( $mode ) , $mode ) , 5022 );
    }
		$this->_alpha = $mode;
	}
  
	/**
	 * Constructor
	 *
	 * @param [array] $matrix Convolution matrix
	 * @param [integer] $divisor Convolution divisor
	 * @param [integer] $offset Convolution offset
	 * @param [string] $edge Convolution egde
	 * @param [string] $alpha Convolution alpha
   * @return void
   * @access public
	 */
	public function __construct( array $matrix = null , $divisor = null , $offset = null , $edge = null , $alpha = null )
	{
    // call parent
    parent::__construct( $matrix , $divisor , $offset );
    
    // initialize default values from argument or configuration
    if( $edge === null ) $this->setEdgeMode( Config::getConvolutionDefaultEdgeMode() ); 
    else $this->setEdgeMode( $edge );
    if( $alpha === null ) $this->setAlphaMode( Config::getConvolutionDefaultAlphaMode() ); 
    else $this->setAlphaMode( $alpha );
  }
  
  /**
   * Apply convolution over the entire picture.
   *
   * @param \GDImage\Resource_Abstract $src Source
   * @return boolean Success flag
   * @access protected
   * @extends 
   */
  public function _convolution( Resource_Abstract $src )
  {
    $flag = true;
    
    // unset alpha blending for better results
    $this->__tmp->unsetMode( Resource_Abstract::MODE_ALPHABLENDING );
    
    // to make sure image convolution will not sucks, remove all alpha channel
    // yes, IT SAVES OUR ASS!
    // the great fact, is that on most true color pictures, a full transparent
    // color is black (0,0,0) so it will not interfeer with the convolution
    
    // now restore alpha on every pixels
    $xmax = $this->__tmp->getWidth();
    $ymax = $this->__tmp->getHeight();
    
    $x = $xmax;
    while( $x-- )
    {
      $y = $ymax;
      while( $y-- )
      {
        $c1 = $this->__tmp->getColorAt( $x , $y );
        
        if( $c1->getAlpha() !== 0 )
        {
          $c1->setAlpha( 0 );
          $flag = $this->__tmp->setColorAt( $c1 , $x , $y ) && $flag;
        }
      }
    }
    
    // then apply convolution
    $flag = imageconvolution( $this->__tmp->getGdResource() , $this->_matrix , $this->_divisor , $this->_offset ) && $flag;
    
    // apply edge first
    $flag = $this->_convolutionEdge( $src ) && $flag;
    
    // then apply alpha
    $flag = $this->_convolutionAlpha( $src ) && $flag;
    
    // reset source colors
    $this->__src_colors = null;
    
    return $flag;
  }
  
  // prepare colors
	
	/**
	 * 2D array of all colors by pixels from source
	 *
	 * @var string Mode
	 * @access protected
	 */
	public $__src_colors = null;
  
  /**
   * Method used to fetch a 2D representation of all colors of the image, 
   * including edge out of images when necessary.
   * 
   * Is second argument is true, the array will contains only edge.
   *
   * @param Resource_Abstract $src
   * @param boolean [$only_edge]
   * @return array 2D representation of edge pixels
   * @access protected
   */
  public function _prepareSourceColors( Resource_Abstract $src , $only_edge = false )
  {
    if( ( ! is_array( $this->__src_colors ) ) || 
        // we do not want only edge and pixel at 2,2 is not set
        ( ( ! $only_edge ) && ( ! isset($this->__src_colors[2][2]) ) ) )
    {
      $this->__src_colors = $this->{'_prepareSourceColorsForEdge'.Config::camelize($this->_edge)}( $src , $only_edge );
      
//      print '<pre>';
//      print __CLASS__.' at '.'_prepareSourceColorsForEdge'.Config::camelize($this->_edge)."\n";
//      print '</pre>';
//      $this->_printSourceColors();
    }
    
  	return $this->__src_colors;
  }
  
  /**
   * Method used to fetch a 2D representation of all colors of the image.
   * On this representation, edge are extended (study pixels from current side).
   * 
   * Is second argument is true, the array will contains only edge.
   *
   * @param Resource_Abstract $src
   * @param boolean [$only_edge]
   * @return array 2D representation of pixels
   * @access protected
   */
  public function _prepareSourceColorsForEdgeExtend( Resource_Abstract $src , $only_edge = false )
  {
    // fetch previous colors
    $pixs = $this->__src_colors;
    
    $xmax = $src->getWidth() - 1; // -1 to get last X-position
    $ymax = $src->getHeight() - 1; // -1 to get last Y-position
    
    // look if edge has not been allready processed
    if( ! isset($pixs[0][0]) )
    {
      // process corners

      // top-left
      $pixs[-1][-1] = $pixs[-1][0] = $pixs[0][-1] = $pixs[0][0] = $src->getColorAt( 0 , 0 );
      // bottom-left
      $pixs[-1][$ymax+1] = $pixs[-1][$ymax] = $pixs[0][$ymax+1] = $pixs[0][$ymax] = $src->getColorAt( 0 , $ymax );
      // top-right
      $pixs[$xmax+1][-1] = $pixs[$xmax+1][0] = $pixs[$xmax][-1] = $pixs[$xmax][0] = $src->getColorAt( $xmax , 0 );
      // bottom-right
      $pixs[$xmax+1][$ymax+1] = $pixs[$xmax+1][$ymax] = $pixs[$xmax][$ymax+1] = $pixs[$xmax][$ymax] = $src->getColorAt( $xmax , $ymax );

      // process borders, excluding corners
      $x = $xmax; // remember that xmax has already been decremented one time
      while( ( --$x ) > 0 ) // process for 0,1,2,3,4,5 result to 4,2,3,1
      {
        // top
        $pixs[$x][-1] = $pixs[$x][0] = $src->getColorAt( $x , 0 );
        $pixs[$x][1] = $src->getColorAt( $x , 1 );

        // bottom
        $pixs[$x][$ymax-1] = $src->getColorAt( $x , $ymax-1 );
        $pixs[$x][$ymax] = $pixs[$x][$ymax+1] = $src->getColorAt( $x , $ymax );
      }

      $y = $ymax; // remember that ymax has already been decremented one time
      while( ( --$y ) > 0 ) // process for 0,1,2,3,4,5 result to 4,2,3,1
      {
        // left
        $pixs[-1][$y] = $pixs[0][$y] = $src->getColorAt( 0 , $y );
        $pixs[1][$y] = $src->getColorAt( 1 , $y );

        // right
        $pixs[$xmax-1][$y] = $src->getColorAt( $xmax-1 , $y );
        $pixs[$xmax][$y] = $pixs[$xmax+1][$y] = $src->getColorAt( $xmax , $y );
      }
    }
    
    if( ( ! $only_edge ) && ( ! isset($pixs[2][2]) ) )
    {
      // decrement xmax and ymax a second time
      $xmax--; $ymax--;
      
      // process content, excluding edge
      $x = $xmax; // remember that xmax has already been decremented twice
      while( ( --$x ) > 1 ) // process for 0,1,2,3,4,5 result to 2,3
      {
        $y = $ymax; // remember that ymax has already been decremented twice
        while( ( --$y ) > 1 ) // process for 0,1,2,3,4,5 result to 2,3
        {
          $pixs[$x][$y] = $src->getColorAt( $x , $y );
        }
      }
    }
    
    // should be fine now
//    $d = $pixs;
//    ksort( $d , SORT_NUMERIC );
//    foreach( $d as &$d2 ) ksort( $d2 , SORT_NUMERIC );
//    print '<pre>';
//    print __FILE__.' at '.__LINE__."\n";
//    print __CLASS__.' at '.__METHOD__."\n";
//    print_r( $d );
//    print '</pre>';
    
  	return $pixs;
  }
  
  /**
   * Method used to fetch a 2D representation of all colors of the image.
   * On this representation, edge are wrapped (study pixels from opposite side).
   * 
   * Is second argument is true, the array will contains only edge.
   *
   * @param Resource_Abstract $src
   * @param boolean [$only_edge]
   * @return array 2D representation of pixels
   * @access protected
   */
  public function _prepareSourceColorsForEdgeWrap( Resource_Abstract $src , $only_edge = false )
  {
    // fetch previous colors
    $pixs = $this->__src_colors;
    
    $xmax = $src->getWidth() - 1; // -1 to get last X-position
    $ymax = $src->getHeight() - 1; // -1 to get last Y-position
    
    // look if edge has not been allready processed
    if( ! isset($pixs[0][0]) )
    {
      // process corners

      // top-left
      $pixs[$xmax+1][$ymax+1] = $pixs[$xmax+1][0] = $pixs[0][$ymax+1] = $pixs[0][0] = $src->getColorAt( 0 , 0 );
      // bottom-left
      $pixs[$xmax+1][-1] = $pixs[0][-1] = $pixs[$xmax+1][$ymax] = $pixs[0][$ymax] = $src->getColorAt( 0 , $ymax );
      // top-right
      $pixs[-1][$ymax+1] = $pixs[$xmax][$ymax+1] = $pixs[-1][0] = $pixs[$xmax][0] = $src->getColorAt( $xmax , 0 );
      // bottom-right
      $pixs[-1][-1] = $pixs[-1][$ymax] = $pixs[$xmax][-1] = $pixs[$xmax][$ymax] = $src->getColorAt( $xmax , $ymax );

      // process borders, excluding corners
      $x = $xmax; // remember that xmax has already been decremented one time
      while( ( --$x ) > 0 ) // process for 0,1,2,3,4,5 result to 4,2,3,1
      {
        // top
        $pixs[$x][$ymax+1] = $pixs[$x][0] = $src->getColorAt( $x , 0 );
        $pixs[$x][1] = $src->getColorAt( $x , 1 );

        // bottom
        $pixs[$x][$ymax-1] = $src->getColorAt( $x , $ymax-1 );
        $pixs[$x][$ymax] = $pixs[$x][-1] = $src->getColorAt( $x , $ymax );
      }

      $y = $ymax; // remember that ymax has already been decremented one time
      while( ( --$y ) > 0 ) // process for 0,1,2,3,4,5 result to 4,2,3,1
      {
        // left
        $pixs[$xmax+1][$y] = $pixs[0][$y] = $src->getColorAt( 0 , $y );
        $pixs[1][$y] = $src->getColorAt( 1 , $y );

        // right
        $pixs[$xmax-1][$y] = $src->getColorAt( $xmax-1 , $y );
        $pixs[$xmax][$y] = $pixs[-1][$y] = $src->getColorAt( $xmax , $y );
      }
    }
    
    if( ( ! $only_edge ) && ( ! isset($pixs[2][2]) ) )
    {
      // decrement xmax and ymax a second time
      $xmax--; $ymax--;
      
      // process content, excluding edge
      $x = $xmax; // remember that xmax has already been decremented twice
      while( ( --$x ) > 1 ) // process for 0,1,2,3,4,5 result to 2,3
      {
        $y = $ymax; // remember that ymax has already been decremented twice
        while( ( --$y ) > 1 ) // process for 0,1,2,3,4,5 result to 2,3
        {
          $pixs[$x][$y] = $src->getColorAt( $x , $y );
        }
      }
    }
    
    // should be fine now
//    $d = $pixs;
//    ksort( $d , SORT_NUMERIC );
//    foreach( $d as &$d2 ) ksort( $d2 , SORT_NUMERIC );
//    print '<pre>';
//    print __FILE__.' at '.__LINE__."\n";
//    print __CLASS__.' at '.__METHOD__."\n";
//    print_r( $d );
//    print '</pre>';
    
  	return $pixs;
  }
  
  /**
   * Method used to fetch a 2D representation of all colors of the image.
   * On this representation, edge are cropped (study pixels out result to crop).
   * 
   * For this method, we need to calculate alpha on edge, so we need 
   * to shift the entire array representation and fill extra edge with dummy 
   * color (these colors information will be multiply by 0).
   * 
   *                      XXXXX
   * ABC   000            XABCX
   * DEF * 010  result to XDEFX 
   * GHI   000            XGHIX
   *                      XXXXX
   * 
   *                      XXXX
   * ABC   000            ABCX
   * DEF * 100  result to DEFX 
   * GHI   000            GHIX
   *                      XXXX
   * 
   *                      XABC
   * ABC   001            XDEF
   * DEF * 000  result to XGHI 
   * GHI   000            XXXX
   * 
   * and so on...
   * 
   *
   * @param Resource_Abstract $src
   * @param boolean [$only_edge]
   * @return array 2D representation of pixels
   * @access protected
   */
  public function _prepareSourceColorsForEdgeCrop( Resource_Abstract $src , $only_edge = false )
  {
    // fetch previous colors
    $pixs = $this->__src_colors;
    
    $xmax = $this->__tmp->getWidth() - 1; // -1 to get last X-position
    $ymax = $this->__tmp->getHeight() - 1; // -1 to get last Y-position
    
    // edge and content will process the full stuff
    if( ! isset($pixs[0][0]) )
    {
      // on content, we will have to shift
      // pixels representation
      
      // initialize source property for shift
      // these property are relative to image after convolution
      // so with all matrix values to 0, we have nothing to remove
      // and we must add dummy color arround the picture
      // we have two extra position -1 and max+1
      $src_xdelta = 0;
      $src_ydelta = 0;

      // look on the matrix if there is non-zero study
      // top edge
      if( $this->_matrix[0][0] || $this->_matrix[0][1] || $this->_matrix[0][2] )
      {
        $src_ydelta--;
      }
      // left edge
      if( $this->_matrix[0][0] || $this->_matrix[1][0] || $this->_matrix[2][0] )
      {
        $src_xdelta--;
      }
      
      // we want to work with source max
      $src_xmax = $src->getWidth() - 1; // -1 to get last X-position
      $src_ymax = $src->getHeight() - 1; // -1 to get last Y-position
      
      // top and bottom
      $x = $src_xmax + 1; // get original width
      while( $x-- )
      {
        // top
        $pixs[$x+$src_xdelta][$src_ydelta] = $src->getColorAt( $x , 0 );
        $pixs[$x+$src_xdelta][1+$src_ydelta] = $src->getColorAt( $x , 1 );
        $pixs[$x+$src_xdelta][2+$src_ydelta] = $src->getColorAt( $x , 2 );
        // bottom
        $pixs[$x+$src_xdelta][$src_ymax-2+$src_ydelta] = $src->getColorAt( $x , $src_ymax-2 );
        $pixs[$x+$src_xdelta][$src_ymax-1+$src_ydelta] = $src->getColorAt( $x , $src_ymax-1 );
        $pixs[$x+$src_xdelta][$src_ymax+$src_ydelta] = $src->getColorAt( $x , $src_ymax );
      }
      
      // left and right
      $y = $src_ymax + 1; // get original height
      while( $y-- ) 
      {
        // left
        $pixs[$src_xdelta][$y+$src_ydelta] = $src->getColorAt( 0 , $y );
        $pixs[1+$src_xdelta][$y+$src_ydelta] = $src->getColorAt( 1 , $y );
        $pixs[2+$src_xdelta][$y+$src_ydelta] = $src->getColorAt( 2 , $y );
        // right
        $pixs[$src_xmax-2+$src_xdelta][$y+$src_ydelta] = $src->getColorAt( $src_xmax-2 , $y );
        $pixs[$src_xmax-1+$src_xdelta][$y+$src_ydelta] = $src->getColorAt( $src_xmax-1 , $y );
        $pixs[$src_xmax+$src_xdelta][$y+$src_ydelta] = $src->getColorAt( $src_xmax , $y );
      }
      
      // once content has been placed in correct position
      // we want to insert dummy color where necessary
      $dummy = new Color();
      
      // we won't clone this color because it should not be used
      
      // initialize temporary max
      // these property are relative to image after convolution
      // so with all matrix values to 0, we have nothing to remove
      // and we must add dummy color arround the picture
      // we have two extra position -1 and max+1
      $tmp_xmax = $xmax + 1;
      $tmp_ymax = $ymax + 1;
      
      // process corners
      // top-left
      if( empty($pixs[-1][-1]) ) $pixs[-1][-1] = $dummy;
      // bottom-left
      if( empty($pixs[-1][$tmp_ymax]) ) $pixs[-1][$tmp_ymax] = $dummy;
      // top-right
      if( empty($pixs[$tmp_xmax][-1]) ) $pixs[$tmp_xmax][-1] = $dummy;
      // bottom-right
      if( empty($pixs[$tmp_xmax][$tmp_ymax]) ) $pixs[$tmp_xmax][$tmp_ymax] = $dummy;
      
      // process borders
      // top border
      if( empty($pixs[0][-1]) )
      {
        // not set? fill this edge
        $x = min( $xmax + 1 , $tmp_xmax );
        while( $x-- )
        {
          $pixs[$x][-1] = $dummy;
        }
      }
      // bottom border
      if( empty($pixs[0][$tmp_ymax]) )
      {
        // not set? fill this edge
        $x = min( $xmax + 1 , $tmp_xmax );
        while( $x-- )
        {
          $pixs[$x][$tmp_ymax] = $dummy;
        }
      }
      // left border
      if( empty($pixs[-1][0]) )
      {
        // not set? fill this edge
        $y = min( $ymax + 1 , $tmp_ymax );
        while( $y-- )
        {
          $pixs[-1][$y] = $dummy;
        }
      }
      // right border
      if( empty($pixs[$tmp_xmax][0]) )
      {
        // not set? fill this edge
        $y = min( $ymax + 1 , $tmp_ymax );
        while( $y-- )
        {
          $pixs[$tmp_xmax][$y] = $dummy;
        }
      }
      
    }
    
    // fetch for content
    // for this kind of matrix, the position 2,2 may be filled for border
    if( ( ! $only_edge ) && ( ! isset($pixs[3][3]) ) )
    {
      // on content, we will have to shift
      // pixels representation
      
      // initialize source property for shift
      $tmp_xdelta = 0;
      $tmp_ydelta = 0;

      // look on the matrix if there is non-zero study
      // top border
      if( $this->_matrix[0][0] || $this->_matrix[0][1] || $this->_matrix[0][2] )
      {
        $tmp_ydelta--;
      }
      // left border
      if( $this->_matrix[0][0] || $this->_matrix[1][0] || $this->_matrix[2][0] )
      {
        $tmp_xdelta--;
      }
      
      // decrement xmax and ymax a second and third time
      $xmax -= 2; $ymax -= 2;
      
      $x = $xmax; // remember that xmax has already been decremented three time
      while( (--$x) > 2 ) // process for 0,1,2,3,4,5,6,7 result to 3,4
      {
        $y = $ymax;
        while( (--$y) > 2 )
        {
          $pixs[$x+$tmp_xdelta][$y+$tmp_ydelta] = $src->getColorAt( $x , $y );
        }
      }
    }
    
    // should be fine now
//    $d = $pixs;
//    ksort( $d , SORT_NUMERIC );
//    foreach( $d as &$d2 ) ksort( $d2 , SORT_NUMERIC );
//    print '<pre>';
//    print __FILE__.' at '.__LINE__."\n";
//    print __CLASS__.' at '.__METHOD__."\n";
//    print_r( $d );
//    print '</pre>';
      
  	return $pixs;
  }
  
  /**
   * Method used to fetch a 2D representation of all colors of the image.
   * On this representation, edge are cropped (study pixels out result to transparent).
   * 
   * For this method, there is no calculation to do on "out" pixels, so nothing
   * special should be done except.
   * 
   *
   * @param Resource_Abstract $src
   * @param boolean [$only_edge]
   * @return array 2D representation of pixels
   * @access protected
   */
  public function _prepareSourceColorsForEdgeTransparent( Resource_Abstract $src , $only_edge = false )
  {
    // fetch previous colors
    $pixs = $this->__src_colors;
    
    $xmax = $src->getWidth() - 1; // -1 to get last X-position
    $ymax = $src->getHeight() - 1; // -1 to get last Y-position
    
    // fetch for edge
    if( ( ! $only_edge ) && ( ! isset($pixs[0][0]) ) )
    {
      // we just need dummy pixel arround picture
      // in case of alpha calculate
      $dummy = new Color();
      
      // no manipulation will be done on this color, so there is no need to clone it
      
      // process corners
      // top-left
      $pixs[-1][-1] = $pixs[-1][0] = $pixs[0][-1] = $dummy;
      $pixs[0][0] = $src->getColorAt( 0 , 0 );
      // bottom-left
      $pixs[-1][$ymax+1] = $pixs[-1][$ymax] = $pixs[0][$ymax+1] = $dummy;
      $pixs[0][$ymax] = $src->getColorAt( 0 , $ymax );
      // top-right
      $pixs[$xmax+1][-1] = $pixs[$xmax+1][0] = $pixs[$xmax][-1] = $dummy;
      $pixs[$xmax][0] = $src->getColorAt( $xmax , 0 );
      // bottom-right
      $pixs[$xmax+1][$ymax+1] = $pixs[$xmax+1][$ymax] = $pixs[$xmax][$ymax+1] = $dummy;
      $pixs[$xmax][$ymax] = $src->getColorAt( $xmax , $ymax );

      // process borders, excluding corners
      $x = $xmax; // remember that xmax has already been decremented one time
      while( ( --$x ) > 0 ) // process for 0,1,2,3,4,5 result to 4,2,3,1
      {
        // top
        $pixs[$x][-1] = $dummy;
        $pixs[$x][0] = $src->getColorAt( $x , 0 );
        $pixs[$x][1] = $src->getColorAt( $x , 1 );

        // bottom
        $pixs[$x][$ymax-1] = $src->getColorAt( $x , $ymax-1 );
        $pixs[$x][$ymax] = $src->getColorAt( $x , $ymax );
        $pixs[$x][$ymax+1] = $dummy;
      }

      $y = $ymax; // remember that ymax has already been decremented one time
      while( ( --$y ) > 0 ) // process for 0,1,2,3,4,5 result to 4,2,3,1
      {
        // left
        $pixs[-1][$y] = $dummy;
        $pixs[0][$y] = $src->getColorAt( 0 , $y );
        $pixs[1][$y] = $src->getColorAt( 1 , $y );

        // right
        $pixs[$xmax-1][$y] = $src->getColorAt( $xmax-1 , $y );
        $pixs[$xmax][$y] = $src->getColorAt( $xmax , $y );
        $pixs[$xmax+1][$y] = $dummy;
      }
      
    }
    
    // fetch for content
    if( ( ! $only_edge ) && ( ! isset($pixs[2][2]) ) )
    {
      // decrement xmax and ymax a second time
      $xmax--; $ymax--;
      
      // process content, excluding edge
      $x = $xmax; // remember that xmax has already been decremented twice
      while( ( --$x ) > 1 ) // process for 0,1,2,3,4,5 result to 2,3
      {
        $y = $ymax; // remember that ymax has already been decremented twice
        while( ( --$y ) > 1 ) // process for 0,1,2,3,4,5 result to 2,3
        {
          $pixs[$x][$y] = $src->getColorAt( $x , $y );
        }
      }
    }
    
    // should be fine now
//    $d = $pixs;
//    ksort( $d , SORT_NUMERIC );
//    foreach( $d as &$d2 ) ksort( $d2 , SORT_NUMERIC );
//    print '<pre>';
//    print __FILE__.' at '.__LINE__."\n";
//    print __CLASS__.' at '.__METHOD__."\n";
//    print_r( $d );
//    print '</pre>';
      
  	return $pixs;
  }
  
  // convolution edge
  
  /**
   * Apply edge mode after convolution.
   * 
   * @param \GDImage\Resource_Abstract $src Source
   * @return boolean Success flag
   * @access protected
   */
  public function _convolutionEdge( Resource_Abstract $src )
  {
    return $this->{'_convolutionForEdge'.Config::camelize( $this->_edge )}( $src );
  }
  
  /**
   * Method used to fetch apply convolution for edge extend.
   * 
   * @param Resource_Abstract $src
   * @return boolean True on success
   * @access protected
   */
  public function _convolutionForEdgeExtend( Resource_Abstract $src )
  {
    $flag = true;
    
    // there is nothing to do on color calculation
    // but we still need to correct alpha channel when necessary
    
    $alpha_callback = array( $this , '_applyMatrixAtForAlpha'.Config::camelize( $this->_alpha ) );
    
    $xmax = $this->__tmp->getWidth() - 1; // -1 to get last X-position
    $ymax = $this->__tmp->getHeight() - 1; // -1 to get last Y-position
    
    // process corner
    // top-left
    $color = $this->__tmp->getColorAt( 0 , 0 );
    $alpha = call_user_func( $alpha_callback , $src , 0 , 0 );
    if( $alpha !== $color->getAlpha() )
    {
      $color->setAlpha( $alpha );
      $flag = $this->__tmp->setColorAt( $color , 0 , 0 ) && $flag;
    }
    // bottom-left
    $color = $this->__tmp->getColorAt( 0 , $ymax );
    $alpha = call_user_func( $alpha_callback , $src , 0 , $ymax );
    if( $alpha !== $color->getAlpha()  )
    {
      $color->setAlpha( $alpha );
      $flag = $this->__tmp->setColorAt( $color , 0 , $ymax ) && $flag;
    }
    // top-right
    $color = $this->__tmp->getColorAt( $xmax , 0 );
    $alpha = call_user_func( $alpha_callback , $src , $xmax , 0 );
    if( $alpha !== $color->getAlpha()  )
    {
      $color->setAlpha( $alpha );
      $flag = $this->__tmp->setColorAt( $color , $xmax , 0 ) && $flag;
    }
    // bottom-right
    $color = $this->__tmp->getColorAt( $xmax , $ymax);
    $alpha = call_user_func( $alpha_callback , $src , $xmax , $ymax );
    if( $alpha !== $color->getAlpha()  )
    {
      $color->setAlpha( $alpha );
      $flag = $this->__tmp->setColorAt( $color , $xmax , $ymax ) && $flag;
    }
    
    // then apply on edge
    
    $x = $xmax; // remember that xmax has already been decremented one time
    while( ( --$x ) > 0 ) // process for 0,1,2,3,4,5 result to 4,2,3,1
    {
      // top
      $color = $this->__tmp->getColorAt( $x , 0 );
      $alpha = call_user_func( $alpha_callback , $src , $x , 0 );
      if( $alpha !== $color->getAlpha() )
      {
        $color->setAlpha( $alpha );
        $flag = $this->__tmp->setColorAt( $color , $x , 0 ) && $flag;
      }
      
      // bottom
      $color = $this->__tmp->getColorAt( $x , $ymax );
      $alpha = call_user_func( $alpha_callback , $src , $x , $ymax );
      if( $alpha !== $color->getAlpha() )
      {
        $color->setAlpha( $alpha );
        $flag = $this->__tmp->setColorAt( $color , $x , $ymax ) && $flag;
      }
    }
    
    $y = $ymax; // remember that xmax has already been decremented one time
    while( ( --$y ) > 0 ) // process for 0,1,2,3,4,5 result to 4,2,3,1
    {
      // left
      $color = $this->__tmp->getColorAt( 0 , $y );
      $alpha = call_user_func( $alpha_callback , $src ,  0 , $y );
      
//      print '<pre>'.'0'.' '.$y.'</pre>';
//      $this->_printSourceColors2( $rgb );
//      print '<pre>'.$color->getAlpha().' vs '.$alpha.'</pre>';
      
      if( $alpha !== $color->getAlpha() )
      {
        $color->setAlpha( $alpha );
        $flag = $this->__tmp->setColorAt( $color , 0 , $y ) && $flag;
      }
      
      // right
      $color = $this->__tmp->getColorAt( $xmax , $y );
      $alpha = call_user_func( $alpha_callback , $src , $xmax , $y );
      
      if( $alpha !== $color->getAlpha() )
      {
        $color->setAlpha( $alpha );
        $flag = $this->__tmp->setColorAt( $color , $xmax , $y ) && $flag;
      }
    }
    
    return $flag;
  }
  
  /**
   * Method used to fetch apply convolution for edge wrap.
   * 
   * @param Resource_Abstract $src
   * @return boolean True on success
   * @access protected
   */
  public function _convolutionForEdgeWrap( Resource_Abstract $src )
  {
    $flag = true;
    
    // recalculate all edge color and alpha
    
    $xmax = $this->__tmp->getWidth() - 1; // -1 to get last X-position
    $ymax = $this->__tmp->getHeight() - 1; // -1 to get last Y-position
    
    // process corner
    // top-left
    $color = $this->_applyMatrixAt( $src , 0 , 0 );
    $flag = $this->__tmp->setColorAt( $color , 0 , 0 ) && $flag;
    // bottom-left
    $color = $this->_applyMatrixAt( $src , 0 , $ymax );
    $flag = $this->__tmp->setColorAt( $color , 0 , $ymax ) && $flag;
    // top-right
    $color = $this->_applyMatrixAt( $src , $xmax , 0 );
    $flag = $this->__tmp->setColorAt( $color , $xmax , 0 ) && $flag;
    // bottom-right
    $color = $this->_applyMatrixAt( $src , $xmax , $ymax );
    $flag = $this->__tmp->setColorAt( $color , $xmax , $ymax ) && $flag;
    
    // then apply on edge
    
    $x = $xmax; // remember that xmax has already been decremented one time
    while( ( --$x ) > 0 ) // process for 0,1,2,3,4,5 result to 4,2,3,1
    {
      // top
      $color = $this->_applyMatrixAt( $src , $x , 0 );
      $flag = $this->__tmp->setColorAt( $color , $x , 0 ) && $flag;
      
      // bottom
      $color = $this->_applyMatrixAt( $src , $x , $ymax );
      $flag = $this->__tmp->setColorAt( $color , $x , $ymax ) && $flag;
    }
    
    $y = $ymax; // remember that xmax has already been decremented one time
    while( ( --$y ) > 0 ) // process for 0,1,2,3,4,5 result to 4,2,3,1
    {
      // left
      $color = $this->_applyMatrixAt( $src , 0 , $y );
      $flag = $this->__tmp->setColorAt( $color , 0 , $y ) && $flag;
      
      // right
      $color = $this->_applyMatrixAt( $src , $xmax , $y );
      $flag = $this->__tmp->setColorAt( $color , $xmax , $y ) && $flag;
    }
    
    return $flag;
  }
  
  /**
   * Method used to fetch apply convolution for edge crop.
   * 
   * @param Resource_Abstract $src
   * @return boolean True on success
   * @access protected
   */
  public function _convolutionForEdgeCrop( Resource_Abstract $src )
  {
    // initialize source property for copy
    $sx = 0;
    $sy = 0;
    $sw = $this->__tmp->getWidth();
    $sh = $this->__tmp->getHeight();
    
    // look on the matrix if there is non-zero study
    // top border
    if( $this->_matrix[0][0] || $this->_matrix[0][1] || $this->_matrix[0][2] )
    {
      $sy = 1;
      $sh--;
    }
    // bottom border
    if( $this->_matrix[2][0] || $this->_matrix[2][1] || $this->_matrix[2][2] )
    {
      $sh--;
    }
    // left border
    if( $this->_matrix[0][0] || $this->_matrix[1][0] || $this->_matrix[2][0] )
    {
      $sx = 1;
      $sw--;
    }
    // right border
    if( $this->_matrix[0][2] || $this->_matrix[1][2] || $this->_matrix[2][2] )
    {
      $sw--;
    }
    
    // At this point, we know the new size of the image
    if( $sw < 1 && $sh < 1 ) return false;
    
    // we can do the job

    // create new destination
    $dst = imagecreatetruecolor( $sw , $sh );
    // preserve alpha channel
    imagealphablending( $dst , false );
    // do copy
    if( ! imagecopy( $dst , $this->__tmp->getGdResource() , 0 , 0 , $sx , $sy , $sw , $sh ) )
    {
      // something goes wrong
      return false;
    }
    
    // assign new resource
    $this->__tmp->setGdResource( $dst );
    
    // now process alpha correction on edge
    
    // fortunately, we can call the edge extend method that just apply alpha 
    // correction on edge
    return $this->_convolutionForEdgeExtend( $src );
  }
  
  /**
   * Method used to fetch apply convolution for edge transparent.
   * 
   * @param Resource_Abstract $src
   * @return boolean True on success
   * @access protected
   */
  public function _convolutionForEdgeTransparent( Resource_Abstract $src )
  {
    $flag = true;
    
    // edge to process
    // bitwise: 1: top, 2: bottom, 4: left: 8: right
    $edge = 0;
    
    // look-up corners
    // top-left
    if( $this->_matrix[0][0] )
    {
      $edge |= ( 1 + 4 );
    }
    // bottom-left
    if( $this->_matrix[2][0] )
    {
      $edge |= ( 2 + 4 );
    }
    // top-right
    if( $this->_matrix[0][2] )
    {
      $edge |= ( 1 + 8 );
    }
    // bottom-right
    if( $this->_matrix[2][2] )
    {
      $edge |= ( 2 + 8 );
    }
    
    // look-up borders
    // top
    if( $this->_matrix[0][1] )
    {
      $edge |= 1;
    }
    // bottom
    if( $this->_matrix[2][1] )
    {
      $edge |= 2;
    }
    // left
    if( $this->_matrix[1][0] )
    {
      $edge |= 4;
    }
    // right
    if( $this->_matrix[1][2] )
    {
      $edge |= 8;
    }
    
    // then process
    // note that corners may be processed twice
    
    $xmax = $this->__tmp->getWidth() - 1;
    $ymax = $this->__tmp->getHeight() - 1;
    
    // look for transparent color
    $transparent = null;
    if( $src->isPaletteColor() && ( $transparentbefore = $src->getTransparent() ) )
    {
      // apply matrix on transparent color, to get the new transparent color

      // if the divisor is equal to the automatic divisor and if the offset is 0
      // we do not have to make calculation
      if( ! $this->_offset )
      {
        $div = array_sum( array_map( 'array_sum' , $this->_matrix ) );
        if( $div === $this->_divisor )
        {
          // there is no sense to apply the matrix
          $transparent = $transparentbefore;
        }
      }

      if( ! $transparent )
      {
        // for calculation, we just apply matrix as if every pixels are
        // the transparent color.

        // note that we must use _applyMatrix() from this class to not
        // provide unexpected result that can comes with extension
        // especially on alpha channel calculation
        // to do that, we use self in a not-static context (yes, it is possible)
        $transparent = self::_applyMatrix( array(
          array( $transparentbefore , $transparentbefore , $transparentbefore ),
          array( $transparentbefore , $transparentbefore , $transparentbefore ),
          array( $transparentbefore , $transparentbefore , $transparentbefore ),
        ) );
      }
      
      // job is done!
    }
    
    if( ! $transparent )
    {
      // not found, create a dummy one
      $transparent = new Color();
      $transparent->setAlpha( Color::ALPHA_TRANSPARENT );
    }
    
    // top
    if( $edge & 1 )
    {
      $x = $xmax + 1; // process entire border
      while( $x-- )
      {
        $flag = $this->__tmp->setColorAt( $transparent , $x , 0 ) && $flag;
      }
    }
    // bottom
    if( $edge & 2 )
    {
      $x = $xmax + 1; // process entire border
      while( $x-- )
      {
        $flag = $this->__tmp->setColorAt( $transparent , $x , $ymax ) && $flag;
      }
    }
    // left
    if( $edge & 4 )
    {
      $y = $ymax + 1; // process entire border
      while( $y-- )
      {
        $flag = $this->__tmp->setColorAt( $transparent , 0 , $y ) && $flag;
      }
    }
    // right
    if( $edge & 8 )
    {
      $y = $ymax + 1; // process entire border
      while( $y-- )
      {
        $flag = $this->__tmp->setColorAt( $transparent , $xmax , $y ) && $flag;
      }
    }
    
    // now process alpha correction on edge
    
    // unfortunately, we can call the edge extend method becaus eit will modify 
    // the transparent color
    // also we can exclude some pixels from calculation to optimize performance
    
    
    // there is nothing to do on color calculation
    // but we still need to correct alpha channel when necessary
    
    $alpha_callback = array( $this , '_applyMatrixAtForAlpha'.Config::camelize( $this->_alpha ) );
    
    $xmax = $this->__tmp->getWidth() - 1; // -1 to get last X-position
    $ymax = $this->__tmp->getHeight() - 1; // -1 to get last Y-position
    
    // process corner
    // top-left
    if( ! ( $edge & ( 1 + 4 ) ) )
    {
      $color = $this->__tmp->getColorAt( 0 , 0 );
      $alpha = call_user_func( $alpha_callback , $src , 0 , 0 );
      if( $alpha !== $color->getAlpha() )
      {
        $color->setAlpha( $alpha );
        $flag = $this->__tmp->setColorAt( $color , 0 , 0 ) && $flag;
      }
    }
    // bottom-left
    if( ! ( $edge & ( 2 + 4 ) ) )
    {
      $color = $this->__tmp->getColorAt( 0 , $ymax );
      $alpha = call_user_func( $alpha_callback , $src , 0 , $ymax );
      if( $alpha !== $color->getAlpha()  )
      {
        $color->setAlpha( $alpha );
        $flag = $this->__tmp->setColorAt( $color , 0 , $ymax ) && $flag;
      }
    }
    // top-right
    if( ! ( $edge & ( 1 + 8 ) ) )
    {
      $color = $this->__tmp->getColorAt( $xmax , 0 );
      $alpha = call_user_func( $alpha_callback , $src , $xmax , 0 );
      if( $alpha !== $color->getAlpha()  )
      {
        $color->setAlpha( $alpha );
        $flag = $this->__tmp->setColorAt( $color , $xmax , 0 ) && $flag;
      }
    }
    // bottom-right
    if( ! ( $edge & ( 2 + 8 ) ) )
    {
      $color = $this->__tmp->getColorAt( $xmax , $ymax);
      $alpha = call_user_func( $alpha_callback , $src , $xmax , $ymax );
      if( $alpha !== $color->getAlpha()  )
      {
        $color->setAlpha( $alpha );
        $flag = $this->__tmp->setColorAt( $color , $xmax , $ymax ) && $flag;
      }
    }
    
    // then apply on edge
    if( ! ( $edge & ( 1 + 2 ) ) )  
    {
      // process top and bottom
      $x = $xmax; // remember that xmax has already been decremented one time
      while( ( --$x ) > 0 ) // process for 0,1,2,3,4,5 result to 4,2,3,1
      {
        // top
        $color = $this->__tmp->getColorAt( $x , 0 );
        $alpha = call_user_func( $alpha_callback , $src , $x , 0 );
        if( $alpha !== $color->getAlpha() )
        {
          $color->setAlpha( $alpha );
          $flag = $this->__tmp->setColorAt( $color , $x , 0 ) && $flag;
        }

        // bottom
        $color = $this->__tmp->getColorAt( $x , $ymax );
        $alpha = call_user_func( $alpha_callback , $src , $x , $ymax );
        if( $alpha !== $color->getAlpha() )
        {
          $color->setAlpha( $alpha );
          $flag = $this->__tmp->setColorAt( $color , $x , $ymax ) && $flag;
        }
      }
    }
    elseif( ! ( $edge & 1 ) )
    {
      // process top 
      $x = $xmax; // remember that xmax has already been decremented one time
      while( ( --$x ) > 0 ) // process for 0,1,2,3,4,5 result to 4,2,3,1
      {
        // top
        $color = $this->__tmp->getColorAt( $x , 0 );
        $alpha = call_user_func( $alpha_callback , $src , $x , 0 );
        if( $alpha !== $color->getAlpha() )
        {
          $color->setAlpha( $alpha );
          $flag = $this->__tmp->setColorAt( $color , $x , 0 ) && $flag;
        }
      }
    }
    elseif( ! ( $edge & 2 ) )
    {
      // process bottom 
      $x = $xmax; // remember that xmax has already been decremented one time
      while( ( --$x ) > 0 ) // process for 0,1,2,3,4,5 result to 4,2,3,1
      {
        // bottom
        $color = $this->__tmp->getColorAt( $x , $ymax );
        $alpha = call_user_func( $alpha_callback , $src , $x , $ymax );
        if( $alpha !== $color->getAlpha() )
        {
          $color->setAlpha( $alpha );
          $flag = $this->__tmp->setColorAt( $color , $x , $ymax ) && $flag;
        }
      }
    }
    
    if( ! ( $edge & ( 4 + 8 ) ) )  
    {
      // process left and right
      $y = $ymax; // remember that xmax has already been decremented one time
      while( ( --$y ) > 0 ) // process for 0,1,2,3,4,5 result to 4,2,3,1
      {
        // left
        $color = $this->__tmp->getColorAt( 0 , $y );
        $alpha = call_user_func( $alpha_callback , $src ,  0 , $y );

        if( $alpha !== $color->getAlpha() )
        {
          $color->setAlpha( $alpha );
          $flag = $this->__tmp->setColorAt( $color , 0 , $y ) && $flag;
        }

        // right
        $color = $this->__tmp->getColorAt( $xmax , $y );
        $alpha = call_user_func( $alpha_callback , $src , $xmax , $y );

        if( $alpha !== $color->getAlpha() )
        {
          $color->setAlpha( $alpha );
          $flag = $this->__tmp->setColorAt( $color , $xmax , $y ) && $flag;
        }
      }
    }
    elseif( ! ( $edge & 4 ) )
    {
      // process left and right
      $y = $ymax; // remember that xmax has already been decremented one time
      while( ( --$y ) > 0 ) // process for 0,1,2,3,4,5 result to 4,2,3,1
      {
        // left
        $color = $this->__tmp->getColorAt( 0 , $y );
        $alpha = call_user_func( $alpha_callback , $src ,  0 , $y );

        if( $alpha !== $color->getAlpha() )
        {
          $color->setAlpha( $alpha );
          $flag = $this->__tmp->setColorAt( $color , 0 , $y ) && $flag;
        }
      }
    }
    elseif( ! ( $edge & 8 ) )
    {
      // process left and right
      $y = $ymax; // remember that xmax has already been decremented one time
      while( ( --$y ) > 0 ) // process for 0,1,2,3,4,5 result to 4,2,3,1
      {
        // right
        $color = $this->__tmp->getColorAt( $xmax , $y );
        $alpha = call_user_func( $alpha_callback , $src , $xmax , $y );

        if( $alpha !== $color->getAlpha() )
        {
          $color->setAlpha( $alpha );
          $flag = $this->__tmp->setColorAt( $color , $xmax , $y ) && $flag;
        }
      }
    }
    
    return $flag;
  }
  
  // apply matrix
  
  /**
   * Method used to apply convolution matrix on an array of RGB colors.
   *
   * @param array $rgb Colors array
   * @return \GDImage\Color
   * @access protected
   */
  public function _applyMatrix( array $rgb )
  {
    // create new color
    $new = parent::_applyMatrix( $rgb );
    
    $new->setAlpha( $this->{'_applyMatrixForAlpha'.Config::camelize( $this->_alpha )}( $rgb ) );
    
  	return $new;
  }
  
//  /**
//   * Return alpha "gd" calculation from matrix.
//   * 
//   * @param array $rgb Colors array
//   * @return integer Alpha channel
//   * @access protected
//   */
//  public function _applyMatrixForAlphaGd( array $rgb )
//  {
//    // default behavior is:
//    // - pixel on corners does not get alpha channel
//    // - pixels on bottom keep alpha channel
//    // - other pixels get alpha channel from pixels below
//    
//    // too much complicate, just do it like preserve
//    return $rgb[1][1]->getAlpha();
//  }
  
  /**
   * Return alpha "no" calculation from matrix.
   * 
   * @param array $rgb Colors array
   * @return integer Alpha channel
   * @access protected
   */
  public function _applyMatrixForAlphaNo( array $rgb )
  {
    return 0;
  }
  
  /**
   * Return alpha "preserve" calculation from matrix.
   * 
   * @param array $rgb Colors array
   * @return integer Alpha channel
   * @access protected
   */
  public function _applyMatrixForAlphaPreserve( array $rgb )
  {
    return $rgb[1][1]->getAlpha();
  }
  
  /**
   * Return alpha "calculate" calculation from matrix.
   * 
   * @param array $rgb RGB array
   * @return integer Alpha channel
   * @access protected
   */
  public function _applyMatrixForAlphaCalculate( array $rgb )
  {
    $alpha = ( ( ( $rgb[0][0]->getAlpha() * $this->_matrix[0][0] ) + ( $rgb[0][1]->getAlpha() * $this->_matrix[0][1] ) + ( $rgb[0][2]->getAlpha() * $this->_matrix[0][2] )
              + ( $rgb[1][0]->getAlpha() * $this->_matrix[1][0] ) + ( $rgb[1][1]->getAlpha() * $this->_matrix[1][1] ) + ( $rgb[1][2]->getAlpha() * $this->_matrix[1][2] )
              + ( $rgb[2][0]->getAlpha() * $this->_matrix[2][0] ) + ( $rgb[2][1]->getAlpha() * $this->_matrix[2][1] ) + ( $rgb[2][2]->getAlpha() * $this->_matrix[2][2] ) )
             / $this->_divisor ) + ( $this->_offset / 2 );
    
    // reduce it for comparison
    return max( 0 , min( $alpha | 0 , 0x7F ) );
  }
  
  
  /**
   * Method used to apply convolution matrix on from given position.
   *
   * @param \GDImage\Resource_Abstract $src
   * @param integer $x
   * @param integer $y
   * @return \GDImage\Color
   * @access protected
   */
  public function _applyMatrixAt( Resource_Abstract $src , $x , $y ) 
  {
    $this->_prepareSourceColors( $src , false );
            
    // create new color
    $new = parent::_applyMatrix( array(
      array( $this->__src_colors[$x-1][$y-1] , $this->__src_colors[$x][$y-1] , $this->__src_colors[$x+1][$y-1] ) ,
      array( $this->__src_colors[$x-1][$y] , $this->__src_colors[$x][$y] , $this->__src_colors[$x+1][$y] ) ,
      array( $this->__src_colors[$x-1][$y+1] , $this->__src_colors[$x][$y+1] , $this->__src_colors[$x+1][$y+1] ) ,
    ) );
    
    $new->setAlpha( $this->{'_applyMatrixAtForAlpha'.Config::camelize( $this->_alpha )}( $src , $x , $y ) );
    
  	return $new;
  }
  
//  /**
//   * Return alpha "gd" calculation from matrix.
//   * 
//   * @param \GDImage\Resource_Abstract $src
//   * @param integer $x
//   * @param integer $y
//   * @return integer Alpha channel
//   * @access protected
//   */
//  public function _applyMatrixAtForAlphaGd( Resource_Abstract $src , $x , $y )
//  {
//    // get value from tmp
//    return $this->__tmp->getColorAt( $x , $y )->getAlpha();
//  }
  
  /**
   * Return alpha "no" calculation from matrix.
   * 
   * @param \GDImage\Resource_Abstract $src
   * @param integer $x
   * @param integer $y
   * @return integer Alpha channel
   * @access protected
   */
  public function _applyMatrixAtForAlphaNo( Resource_Abstract $src , $x , $y )
  {
    return 0;
  }
  
  /**
   * Return alpha "preserve" calculation from matrix.
   * 
   * @param \GDImage\Resource_Abstract $src
   * @param integer $x
   * @param integer $y
   * @return integer Alpha channel
   * @access protected
   */
  public function _applyMatrixAtForAlphaPreserve( Resource_Abstract $src , $x , $y )
  {
    // prepare source, necessary in crop case
    $this->_prepareSourceColors( $src , false );
    
    return $this->__src_colors[$x][$y]->getAlpha();
  }
  
  /**
   * Return alpha "calculate" calculation from matrix.
   * 
   * @param \GDImage\Resource_Abstract $src
   * @param integer $x
   * @param integer $y
   * @return integer Alpha channel
   * @access protected
   */
  public function _applyMatrixAtForAlphaCalculate( Resource_Abstract $src , $x , $y )
  {
    // prepare source
    $this->_prepareSourceColors( $src , false );
            
    // return calculation from matrix
    return $this->_applyMatrixForAlphaCalculate( array(
      array( $this->__src_colors[$x-1][$y-1] , $this->__src_colors[$x][$y-1] , $this->__src_colors[$x+1][$y-1] ) ,
      array( $this->__src_colors[$x-1][$y] , $this->__src_colors[$x][$y] , $this->__src_colors[$x+1][$y] ) ,
      array( $this->__src_colors[$x-1][$y+1] , $this->__src_colors[$x][$y+1] , $this->__src_colors[$x+1][$y+1] ) ,
    ) );
    
  }
  
  // convolution alpha
  
  /**
   * Apply alpha mode after convolution.
   * 
   * This convolution apply after edge calculation, so there is no need to correct edge
   * at this point.
   * 
   * @param \GDImage\Resource_Abstract $src Source
   * @return boolean Success flag
   * @access protected
   */
  public function _convolutionAlpha( Resource_Abstract $src )
  {
    return $this->{'_convolutionForAlpha'.Config::camelize( $this->_alpha )}( $src );
  }
  
//  /**
//   * Apply alpha "gd" convolution.
//   * 
//   * @param \GDImage\Resource_Abstract $src Source
//   * @return boolean Success flag
//   * @access protected
//   */
//  public function _convolutionForAlphaGd( Resource_Abstract $src )
//  {
//    // nothing to do
//    return true;
//  }
  
  /**
   * Apply alpha "no" convolution.
   * 
   * This method is applied after edge
   * 
   * @param \GDImage\Resource_Abstract $src Source
   * @return boolean Success flag
   * @access protected
   */
  public function _convolutionForAlphaNo( Resource_Abstract $src )
  {
    // nothing to do, @see $this->_convolution()
    return true;
    
//    $flag = true;
//    
//    // now restore alpha on every pixels
//    $xmax = $this->__tmp->getWidth()-1;
//    $ymax = $this->__tmp->getHeight()-1;
//    
//    // do not apply correction on border
//    $x = $xmax;
//    while( (--$x) > 0 )
//    {
//      $y = $ymax;
//      while( (--$y) > 0 )
//      {
//        $color = $this->__tmp->getColorAt( $x , $y );
//        
//        if( $color->getAlpha() !== 0 )
//        {
//          $color->setAlpha( 0 );
//          $flag = $this->__tmp->setColorAt( $color , $x , $y ) && $flag;
//        }
//      }
//    }
//    
//    // job should be done, nothing else
//    return $flag;
  }
  
  /**
   * Apply alpha "preserve" convolution.
   * 
   * @param \GDImage\Resource_Abstract $src Source
   * @return boolean Success flag
   * @access protected
   */
  public function _convolutionForAlphaPreserve( Resource_Abstract $src )
  {
    // prepare source colors
    $this->_prepareSourceColors( $src , false );
    
    $flag = true;
    
    // now restore alpha on every pixels
    $xmax = $this->__tmp->getWidth()-1;
    $ymax = $this->__tmp->getHeight()-1;
    
    // do not apply correction on border
    $x = $xmax;
    while( (--$x) > 0 )
    {
      $y = $ymax;
      while( (--$y) > 0 )
      {
        $color = $this->__tmp->getColorAt( $x , $y );
        // fetch alpha from source
        $alpha = $this->__src_colors[$x][$y]->getAlpha();
        
        if( $color->getAlpha() !== $alpha )
        {
          $color->setAlpha( $alpha );
          $flag = $this->__tmp->setColorAt( $color , $x , $y ) && $flag;
        }
      }
    }
    
    // job should be done, nothing else
    return $flag;
  }
  
  /**
   * Apply alpha "calculate" convolution.
   * 
   * Note that edge has been calculated before
   * 
   * @param \GDImage\Resource_Abstract $src Source
   * @return boolean Success flag
   * @access protected
   */
  public function _convolutionForAlphaCalculate( Resource_Abstract $src )
  {
    $flag = true;
    
    // now restore alpha on every pixels
    $xmax = $this->__tmp->getWidth()-1;
    $ymax = $this->__tmp->getHeight()-1;
    
    // do not apply correction on border
    $x = $xmax;
    while( (--$x) > 0 )
    {
      $y = $ymax;
      while( (--$y) > 0 )
      {
        $color = $this->__tmp->getColorAt( $x , $y );
        
        $alpha = $this->_applyMatrixAtForAlphaCalculate( $src , $x , $y );
        
        if( $color->getAlpha() !== $alpha )
        {
          $color->setAlpha( $alpha );
          $flag = $this->__tmp->setColorAt( $color , $x , $y ) && $flag;
        }
      }
    }
    
    // job should be done, nothing else
    return $flag;
  }

  // debug
  
  public function _printSourceColors( array $pixs = null )
  {
    if( $pixs === null ) $pixs = $this->__src_colors;
    
    // keep in mind that $x represent columns (width) and y rows (height)
    print '<table style="width:auto;border:1px black solid;" border="0" cellspacing="0" cellpadding="0">';
    for( $y = -1, $xmax = $this->__tmp->getWidth() + 1, $ymax = $this->__tmp->getHeight() + 1; $y < $ymax; $y++ )
    {
      print '<tr>';
      for( $x = -1; $x < $xmax; $x++ )
      {
        if( isset($pixs[$x][$y]) )
        {
          $c = $pixs[$x][$y];
          print sprintf( '<td style="padding:0;width:10px;height:10px;background:rgba(%d,%d,%d,%f);" data="'.$x.','.$y.'"></td>' , $c->getRed() , $c->getGreen() , $c->getBlue() , ( 127 - $c->getAlpha() ) / 127 );
        }
        else
        {
          print '<td style="padding:0;width:10px;height:10px;font-size:4px;" data="'.$x.','.$y.'">x</td>';
        }
      }
      print '</tr>';
    }
    print '</table>';
  }
}
