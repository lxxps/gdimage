<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Basic transformation used to manage image convolution.
 * Note that new image is created as overlayer of black image
 * and alpha channels are completly losed on convolution martix.
 * Looks like abstraction leaks of imageconvolution().
 * 
 * So, if you have very deal with alpha channel, we advice to use 
 * \GDImage\Transform_Convolution.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Transform_Interface
 */
class Transform_Convolution_Basic implements Transform_Interface
{
	
	/**
	 * Convolution 3x3 matrix
	 *
	 * @var array Matrix
	 * @access protected
	 */
	public $_matrix = array(
	  array( 0 , 0 , 0 ),
	  array( 0 , 1 , 0 ),
	  array( 0 , 0 , 0 ),
	);
	
	/**
	 * Convolution divisor
	 *
	 * @var float Divisor
	 * @access protected
	 */
	public $_divisor = 1;
	
	/**
	 * Convolution automatic divisor flag
	 *
	 * @var boolean
	 * @access protected
	 */
	public $_auto_divisor = true;
	
	/**
	 * Convolution offset
	 *
	 * @var integer Offset
	 * @access protected
	 */
	public $_offset = 0;
  
  /**
   * Temporary resource.
   *
   * @var \GDImage\Resource_Abstract Resource
   * @access protected
   */
  public $__tmp;
	
  /**
   * Set the color Matrix
   * 
   * By convenience, if there is some missing values, we will reproduce the 
   * default matrix.
   * 
   * Will need to be overwrote if you want a bigger matrix.
   *
   * @param array $matrix
   * @return void
   * @access public
   */
  public function setMatrix( array $matrix )
  {
    // make sure all keys exists and are array
    $matrix += array_fill( 0 , 3 , array() );
    
    // for the second level, if it is not number, we do not care
    $matrix[0] += array( 0 , 0 , 0 );
    $matrix[1] += array( 0 , 1 , 0 );
    $matrix[2] += array( 0 , 0 , 0 );
    
    // reduce matrix values in order to simplify automatic divisor
    $i = 3;
    while( $i-- )
    {
      $chunks = array_chunk( $matrix[$i] , 3 , false );
      $matrix[$i] = $chunks[0];
    }
    
    $this->_matrix = $matrix;
  }
	
  /**
   * Set the convolution divisor
   * This will disable automatic divisor
   *
   * @param float $divisor
   * @return void
   * @access public
   */
  public function setDivisor( $divisor )
  {
    $this->_divisor = (float)$divisor;
    // disable automatic divisor
    $this->setAutomaticDivisor( false );
  }
	
  /**
   * Set the convolution offset
   *
   * @param integer $offset
   * @return void
   * @access public
   */
  public function setOffset( $offset )
  {
    $this->_offset = (int)$offset;
  }
	
  /**
   * Set automatic divisor flag
   *
   * @param boolean $auto
   * @return void
   * @access public
   */
  public function setAutomaticDivisor( $auto )
  {
    $this->_auto_divisor = (bool)$auto;
  }
  
	/**
	 * Constructor
	 *
	 * @param [array] $matrix Convolution matrix
	 * @param [integer] $divisor Convolution divisor
	 * @param [integer] $offset Convolution offset
   * @return void
   * @access public
	 */
	public function __construct( array $matrix = null , $divisor = null , $offset = null )
	{
    // for class extension, it is preferable to never automatically set properties on constructor
		if( $matrix !== null ) $this->setMatrix( $matrix );
		if( $divisor !== null ) $this->setDivisor( $divisor );
		if( $offset !== null ) $this->setOffset( $offset );
	}
  
  /**
   * Apply transformation to a resource.
   * Return false if the transformation fails.
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access public
   */
  public function __invoke( Resource_Abstract &$rsc )
  {
    // automatic divisor
    $this->_auto_divisor && $this->_automaticDivisor();
    
    // clone resource to be safe
    $this->__tmp = clone $rsc;
    
    // tranform to true color for better results
    if( $rsc->isPaletteColor() )
    {
      $this->__tmp = $this->__tmp->toTrueColor();
    }
  
    // apply convolution
    $flag = $this->_convolution( $rsc );
    
    if( $rsc->isPaletteColor() )
    {
      // make destination a palette color again
      $tmp = $this->__tmp->toPaletteColor( 256 , false );
      
      // if we were unable to find any transparent color anymore
      // we may have trouble with AGIF manipulation
      if( $rsc->getTransparent() && ( ! $tmp->getTransparent() ) )
      {
        if( $tmp->countColors() === 256 )
        {
          // redo palette conversion with one slot for the transparent color
          $tmp = $this->__tmp->toPaletteColor( 255 , false );
        }
        
        // calculate transparent color
        $transparent = $rsc->getTransparent();
          
        // note that we must use _applyMatrix() from this class to not
        // provide unexpected result that can comes with extension
        // especially on alpha channel calculation
        // to do that, we use self in a not-static context (yes, it is possible)
        $transparent = self::_applyMatrix( array(
          array( $transparent , $transparent , $transparent ),
          array( $transparent , $transparent , $transparent ),
          array( $transparent , $transparent , $transparent ),
        ) );
        
        // assing it
        $tmp->setTransparent( $transparent );
      }
      
      // finally assign new temporary resource
      $this->__tmp = $tmp;
    }
    
    // assign new ressource
    $rsc = $this->__tmp;
    
    // free resource
    unset( $this->__tmp );
    
    return $flag;
  }
  
  /**
   * Apply convolution over the entire picture.
   *
   * @param \GDImage\Resource_Abstract $src Source
   * @return boolean Success flag
   * @access protected
   */
  public function _convolution( Resource_Abstract $src )
  {
    $flag = true;
    
    // unset alpha blending for better results
    $this->__tmp->unsetMode( Resource_Abstract::MODE_ALPHABLENDING );
    
    // to make sure image convolution will not sucks, remove all alpha channel
    // yes, IT SAVES OUR ASS!
    // the great fact, is that on most true color pictures, a full transparent
    // color is black (0,0,0) so it will not interfeer with the convolution
    
    // a flag to detect if there is some alpha channel on source
    $src_alpha = false;
    
    // now restore alpha on every pixels
    $xmax = $this->__tmp->getWidth();
    $ymax = $this->__tmp->getHeight();
    
    $x = $xmax;
    while( $x-- )
    {
      $y = $ymax;
      while( $y-- )
      {
        $c1 = $this->__tmp->getColorAt( $x , $y );
        
        if( $c1->getAlpha() !== 0 )
        {
          $src_alpha = true;
          $c1->setAlpha( 0 );
          $flag = $this->__tmp->setColorAt( $c1 , $x , $y ) && $flag;
        }
      }
    }
    
    // then apply convolution
    $flag = imageconvolution( $this->__tmp->getGdResource() , $this->_matrix , $this->_divisor , $this->_offset ) && $flag;
    
    // now restore alpha on every pixels
    // do not do it if they was not any alpha channel on source
    if( $src_alpha )
    {
      $xmax = $this->__tmp->getWidth();
      $ymax = $this->__tmp->getHeight();
      
      // it is really quicker to observe color on a true color resource
      // but it is not quicker to save these colors in an array
      $src = $src->toTrueColor();

      $x = $xmax;
      while( $x-- )
      {
        $y = $ymax;
        while( $y-- )
        {
          $c1 = $src->getColorAt( $x , $y );

          if( $c1->getAlpha() !== 0 ) // keep in mind that we previously remove alpha channel
          {
            $c2 = $this->__tmp->getColorAt( $x , $y );
            $c2->setAlpha( $c1->getAlpha() );
            $flag = $this->__tmp->setColorAt( $c2 , $x , $y ) && $flag;
          }
        }
      }
    }
    
    // job should be done, nothing else
    return $flag;
  }
	
  /**
   * Method used to determine automatic divisor.
   * Will need to be overwrote if you want a bigger matrix.
   *
   * @param none
   * @return void
   * @access protected
   */
  public function _automaticDivisor()
  {
    $this->_divisor = array_sum( array_map( 'array_sum' , $this->_matrix ) );
  }
  
  /**
   * Method used to apply convolution matrix on an array of RGB colors.
   *
   * @param array $rgb RGB array
   * @return array RGB array
   * @access protected
   */
  public function _applyMatrix( array $rgb )
  {
    // create new color
    $new = new Color();
    
    // make calculation
    
    // red value
    $new->setRed( 
  	  ( ( ( $rgb[0][0]->getRed() * $this->_matrix[0][0] ) + ( $rgb[0][1]->getRed() * $this->_matrix[0][1] ) + ( $rgb[0][2]->getRed() * $this->_matrix[0][2] )
	      + ( $rgb[1][0]->getRed() * $this->_matrix[1][0] ) + ( $rgb[1][1]->getRed() * $this->_matrix[1][1] ) + ( $rgb[1][2]->getRed() * $this->_matrix[1][2] )
	      + ( $rgb[2][0]->getRed() * $this->_matrix[2][0] ) + ( $rgb[2][1]->getRed() * $this->_matrix[2][1] ) + ( $rgb[2][2]->getRed() * $this->_matrix[2][2] ) )
	    / $this->_divisor ) + $this->_offset
    );
    // green value
    $new->setGreen( 
  	  ( ( ( $rgb[0][0]->getGreen() * $this->_matrix[0][0] ) + ( $rgb[0][1]->getGreen() * $this->_matrix[0][1] ) + ( $rgb[0][2]->getGreen() * $this->_matrix[0][2] )
	      + ( $rgb[1][0]->getGreen() * $this->_matrix[1][0] ) + ( $rgb[1][1]->getGreen() * $this->_matrix[1][1] ) + ( $rgb[1][2]->getGreen() * $this->_matrix[1][2] )
	      + ( $rgb[2][0]->getGreen() * $this->_matrix[2][0] ) + ( $rgb[2][1]->getGreen() * $this->_matrix[2][1] ) + ( $rgb[2][2]->getGreen() * $this->_matrix[2][2] ) )
	    / $this->_divisor ) + $this->_offset
    );
	  // blue value
    $new->setBlue( 
  	  ( ( ( $rgb[0][0]->getBlue() * $this->_matrix[0][0] ) + ( $rgb[0][1]->getBlue() * $this->_matrix[0][1] ) + ( $rgb[0][2]->getBlue() * $this->_matrix[0][2] )
	      + ( $rgb[1][0]->getBlue() * $this->_matrix[1][0] ) + ( $rgb[1][1]->getBlue() * $this->_matrix[1][1] ) + ( $rgb[1][2]->getBlue() * $this->_matrix[1][2] )
	      + ( $rgb[2][0]->getBlue() * $this->_matrix[2][0] ) + ( $rgb[2][1]->getBlue() * $this->_matrix[2][1] ) + ( $rgb[2][2]->getBlue() * $this->_matrix[2][2] ) )
	    / $this->_divisor ) + $this->_offset
    );
    
    // default mode is alpha preserved
    $new->setAlpha( $rgb[1][1]->getAlpha() );
    
  	return $new;
  }
  
}
