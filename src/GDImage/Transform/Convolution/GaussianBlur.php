<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Transformation used to apply Gaussian blur effect.
 * 
 * Note that imagefilter( $rsc , IMAGE_FILTER_GAUSSIAN_BLUR ) and have same 
 * leaks than imageconvolution( $rsc , $matrix , 16 , 0 ), so it is not a good 
 * idea to use it. Also, it does not seems to speed up process, so there is no 
 * reason to use it.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Transform_Interface
 * @extends    \GDImage\Transform_Convolution
 */
class Transform_Convolution_GaussianBlur extends Transform_Convolution
{
	/**
	 * Convolution 3x3 matrix
	 *
	 * @var array Matrix
	 * @access protected
	 */
	public $_matrix = array(
	  array( 1 , 2 , 1 ),
	  array( 2 , 4 , 2 ),
	  array( 1 , 2 , 1 ),
	);
  
//  /**
//   * Apply convolution over the entire picture.
//   *
//   * @param \GDImage\Resource_Abstract $src Source
//   * @return boolean Success flag
//   * @access protected
//   */
//  public function _convolution( Resource_Abstract $src )
//  {
//    // look if it is really a gaussian blur convolution
//    // note that there is no need to check automatic divisor
//    if( $this->_offset === 0 
//        // corners/divisor must equals to 1/16
//     && ( $this->_matrix[0][0] / $this->_divisor ) === 0.0625 && ( $this->_matrix[0][2] / $this->_divisor ) === 0.0625  
//     && ( $this->_matrix[2][0] / $this->_divisor ) === 0.0625 && ( $this->_matrix[2][2] / $this->_divisor ) === 0.0625  
//        // borders/divisor must equals to 1/8
//     && ( $this->_matrix[0][1] / $this->_divisor ) === 0.125
//     && ( $this->_matrix[1][0] / $this->_divisor ) === 0.125 && ( $this->_matrix[1][2] / $this->_divisor ) === 0.125 
//     && ( $this->_matrix[2][1] / $this->_divisor ) === 0.125 
//        // center/divisor must equal to 1/4    
//     && ( $this->_matrix[1][1] / $this->_divisor ) === 0.25 )
//    {
//      // ok, we can use imagefilter()
//      return imagefilter( $this->__tmp->getGdResource() , IMG_FILTER_GAUSSIAN_BLUR );
//    }
//    
//    // fallback to parent method
//    return parent::_convolution( $src );
//  }
  
}
