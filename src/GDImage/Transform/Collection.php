<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Class to manage image transformations instances.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage collection
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Transform_Collection
{
  /**
   * Current \GDImage\Transform_Collection instance.
   *
   * @var \GDImage\Transform_Collection Current instance
   * @access protected
   */
  public static $_instance;
	
  /**
   * Available image transformations by custom key.
   *
   * @var array Array of transformations
   * @access protected
   */
  public $_transforms = array();
  
  /**
   * Method to retrieve \GDImage\Transform_Collection instance.
   *
   * @param none
   * @return \GDImage\Transform_Collection Current instance
   * @access public
   * @static
   */ 
  public static function getInstance()
  {
  	if( ! static::$_instance )
  	{
      static::$_instance = new static();
  	}
  	return static::$_instance;
  }
  
  /**
   * Method to set a new GDImageTransform to a certain key.
   * Set class to null to unset transformation for a key.
   *
   * @param string $key Transform key
   * @param \GDImage\Transform_Interface $transform Instance to use
   * @return void
   * @alias  $this->setTransform()
   * @access public
   * @static
   */ 
  public static function set( $key , Transform_Interface $transform = null )
  {
  	static::getInstance()->setTransform( $key , $transform );
  }
  
  /**
   * Method to retrieve \GDImage\Transform_Interface for a key.
   * Note that we do not clone the instance, do it yourself when it is
   * really necessary.
   *
   * @param string $key Transform key
   * @return \GDImage\Transform_Interface Instance to use
   * @throws \GDImage\Exception_Transform
   * @alias  $this->getTransform()
   * @access public
   * @static
   */ 
  public static function get( $key )
  {
  	return static::getInstance()->getTransform( $key );
  }
  
  /**
   * Instance constructor. Initialize transforms array.
   * Protected constructor to force use of getInstance() static method.
   * Keep in mind POOP pattern: the constructor should be protected, 
   * but it is not.
   *
   * @param none
   * @return void
   * @access protected
   */ 
  public function __construct()
  {
  }
  
  /**
   * Method to set a new GDImageTransform to a certain key.
   * Set class to null to unset transformation for a key.
   * 
   * Note that we do not clone the instance, do it yourself when it is 
   * really necessary.
   *
   * @param string $key Transform key
   * @param \GDImage\Transform_Interface $transform Instance to use
   * @return void
   * @access public
   */ 
  public function setTransform( $key , Transform_Interface $transform = null )
  {
  	$this->_transforms[$key] = $transform;
  }
  
  /**
   * Method to retrieve \GDImage\Transform_Interface for a key.
   * Note that we do not clone the instance, do it yourself when it is
   * really necessary.
   *
   * @param string $key Transform key
   * @return \GDImage\Transform_Interface Instance to use
   * @throws \GDImage\Exception_Transform
   * @access public
   */ 
  public function getTransform( $key )
  {
  	if( empty($this->_transforms[$key]) )
  	{
  		throw new Exception_Transform( array( get_class( $this ) , $key ) , 5000 );
  	}
  	return $this->_transforms[$key];
  }
  
}