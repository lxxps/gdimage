<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * A transformation for multiple transformations.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @implements \GDImage\Transform_Interface
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Transform_Multiple implements Transform_Interface
{
	/**
	 * Tranformations array
	 *
	 * @var array Array of \GDImage\Transform_Interface
	 * @access protected
	 */
	public $_transforms = array();

	/**
	 * Method to add a new transformation.
   * 
   * On PHP 5.6 we will be able to use ... declaration.
   * @see http://php.net/manual/en/functions.arguments.php#functions.variable-arg-list
	 *
	 * @param \GDImage\Transform_Interface $transform
   * @param [\GDImage\Transform_Interface ...] Other transformations to add
	 * @return void
	 * @access public
	 */
	public function add( Transform_Interface $transform /** /, Transform_Interface ...$transforms /**/ )
	{
		$this->_transforms[] = $transform;
    
    // add extra transformations
    // we will never add more than 20 transformations at the same time, so this
    // process is not optimal but fast enough
    for( $i = 1, $imax = func_num_args(); $i < $imax; $i++ )
    {
      $this->add( func_get_arg( $i ) );
    }
	}

	/**
	 * Method to reset transformations.
	 *
	 * @param none
	 * @return void
	 * @access public
	 */
	public function reset()
	{
		$this->_transforms = array();
	}

	/**
	 * Clone.
	 *
	 * @param none
	 * @return void
	 * @access public
	 */
	public function __clone()
	{
    // do it with reference
		foreach( $this->_transforms as &$transform )
    {
      // clone and replace
      $transform = clone $transform;
    }
	}
  
	/**
	 * Constructor
	 *
	 * @param [...] Transformations to apply
   * @return void
   * @access public
	 */
	public function __construct()
	{
    $args = func_get_args();
    if( count( $args ) > 1 ) call_user_func_array( array( $this , 'add' ) , $args );
  }
  
  /**
   * Apply transformation to a resource.
   * Return false if the transformation fails.
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access public
   */
  public function __invoke( Resource_Abstract &$rsc )
  {
    $flag = true;
    
    foreach( $this->_transforms as $transform )
    {
      $flag = $flag && $transform( $rsc );
    }

  	return $flag;
  }
  
}
