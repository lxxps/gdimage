<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Transformation used to colorize an image with imagefilter().
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage image
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Transform_Interface
 */
class Transform_Colorize implements Transform_Interface
{
  
	/**
	 * \GDImage\Color instance used for colorization.
	 *
	 * @var \GDImage\Color
	 * @access protected
	 */
	public $_color;
	
	/**
	 * Method to assign background color.
   * Set it to null to use transparent color.
	 *
	 * @param mixed $color Color
	 * @return void
	 * @access public
	 */
	public function setColor( $color = null )
	{
    // null
    if( $color === null ) $this->_color = null;
    // assign instance
    elseif( $color instanceof Color ) $this->_color = $color;
    // do it mixed
    else $this->_color = new Color( $color );
	}
  
	/**
	 * Constructor
	 *
	 * @param [mixed] $color Color
   * @return void
   * @access public
	 */
	public function __construct( $color = null )
	{
    // for class extension, it is preferable to never automatically set properties on constructor
    if( $color !== null ) $this->setColor( $color );
	}
	
	/**
	 * Magic method call on clone.
	 *
	 * @param none
	 * @return void
	 * @access public
	 */
	public function __clone()
	{
		if( $this->_color ) $this->_color = clone $this->_color;
	}
  	
  /**
   * Apply transformation to a resource.
   * Return false if the transformation fails.
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Transform_Interface
   */
  public function __invoke( Resource_Abstract &$rsc )
  {
    // no color, cannot apply
    if( ! $this->_color ) return false;
    
    // determine function to use
    if( $rsc->isTrueColor() )
    {
      return $this->_colorizeTrueColor( $rsc );
    }
    else
    {
      return $this->_colorizePaletteColor( $rsc );
    }
  }
	
  /**
   * Method used to apply colorize transformation on true color image.
   *
   * @param \GDImage\Resource_TrueColor &$rsc
   * @return boolean Success flag
   * @access protected
   */
  public function _colorizeTrueColor( Resource_TrueColor &$rsc )
  {
    // yeap, simply that
    return imagefilter( $rsc->getGdResource() , \IMG_FILTER_COLORIZE , $this->_color->getRed() , $this->_color->getGreen() , $this->_color->getBlue() , $this->_color->getAlpha() );
  }
	
  /**
   * Method used to apply colorize transformation on palette color image.
   * For palette image, if we want to use imagefilter(), we have to do it in true
   * color and preserve transparency... just boring stuff.
   * 
   * Colorize is a simple addition of color information, nothing else.
   * 
   * Note: On PHP 5.4, we can use imagecolorset() with alpha channel.
   * @see http://php.net/manual/en/function.imagecolorset.php
   *
   * @param \GDImage\Resource_PaletteColor &$rsc
   * @return boolean Success flag
   * @access protected
   */
  public function _colorizePaletteColor( Resource_PaletteColor &$rsc )
  {
    // shortcuts
    list( $r , $g , $b , $a ) = $this->_color->toArray();
    
    
    // if we can use imagecolorset() with alpha channel, just do it
    if( version_compare( PHP_VERSION , '5.4.0' , '>=' ) )
    {
      // do not process calculation twice
      $calc = array();
      
      for( $i = 0, $imax = $rsc->countColors(); $i < $imax; $i++ )
      {
        // fetch color
        $c = $rsc->getColor( $i );
      
        // assigning the color automatically adjust them
        $c->setRed( $c->getRed()+ $r );
        $c->setGreen( $c->getGreen() + $g ); 
        $c->setBlue( $c->getBlue() + $b );  
        $c->setAlpha( $c->getAlpha()+ $a );

        // assign it at same index
        imagecolorset( $rsc->getGdResource() , $i , $c->getRed() , $c->getGreen() , $c->getBlue() , $c->getAlpha() );
      }
      
      return true;
    }
    
    
    // if not, we may be able to use imagecolorset(), but only if there is one
    // color with alpha channel (the transparent one) and if there is no
    // manipulation on alpha channel
    if( $a === 0 )
    {
      // keep in mind transparent color index
      $t = imagecolortransparent( $rsc->getGdResource() );

      $alpha = false;
      for( $i = 0, $imax = $rsc->countColors(); $i < $imax; $i++ )
      {
        if( $i === $t ) continue;

        // fetch color
        $c = $rsc->getColor( $i );

        if( $c->getAlpha() )
        {
          $alpha = true;
          break;
        }
      }

      if( ! $alpha )
      {
        // we can use imagecolorset() because there is only one transparent color
        // and there is no calculation on alpha channel

        for( $i = 0, $imax = $rsc->countColors(); $i < $imax; $i++ )
        {
          // fetch color
          $c = $rsc->getColor( $i );
          
          // assigning the color automatically adjust them
          $c->setRed( $c->getRed()+ $r );
          $c->setGreen( $c->getGreen() + $g ); 
          $c->setBlue( $c->getBlue() + $b );  

          // assign it at same index
          imagecolorset( $rsc->getGdResource() , $i , $c->getRed() , $c->getGreen() , $c->getBlue() );
        }

        return true;
      }
    }
    
    
    // using color deallocation and reallocation does not provide good results:
    // it seems that GD automatically detect if the color is not present in the 
    // palette and will not allocate an existing color, a bug may occurs with 
    // this color non-allocation
    
    // doing a palette copy/paste also fails:
    // sometimes, GD reassing pixels color, that is really unexpected
    
    // via true color, the palette may be reduced to a minimal set, and we do 
    // not want that
    
    // the only way to achieve a strong palette manipulation is to create the 
    // new palette, then copying each pixel index to a brand new resource
    
    $flag = true;
    
    // create temporary resource
    $tmp = Resource_PaletteColor::createFromSize( $rsc->getWidth() , $rsc->getHeight() );
    
    
    // create the new palette on this innocent resource
    
    // do not process calculation twice
    $calc = array();
    $flag = true;
    
    // preserve color indexes in the palette
    for( $i = 0, $imax = $rsc->countColors(); $i < $imax; $i++ )
    {
      // fetch color
      $c = $rsc->getColor( $i );

      // assigning the color automatically adjust them
      $c->setRed( $c->getRed()+ $r );
      $c->setGreen( $c->getGreen() + $g ); 
      $c->setBlue( $c->getBlue() + $b ); 
      $c->setAlpha( $c->getAlpha()+ $a ); 
      
      // strangely, duplicate colors seems to get new color index on this innocent resource...
      $flag = ( imagecolorallocatealpha( $tmp->getGdResource() , $c->getRed() , $c->getGreen() , $c->getBlue() , $c->getAlpha() ) !== false ) && $flag;
    }
    
    // propagate transparent color
    if( ( $t = imagecolortransparent( $rsc->getGdResource() ) ) > -1 )
    {
      // do it with index of the transparent color
      $flag = ( imagecolortransparent( $tmp->getGdResource() , $t ) > -1 ) && $flag;
    }
 
    
    // then copy each pixels to their destination
    
    $x = $rsc->getWidth();
    $ymax = $rsc->getHeight();
    while( $x-- )
    {
      $y = $ymax;
      while( $y-- )
      {
        // fetch color index
        $i = imagecolorat( $rsc->getGdResource() , $x , $y );
        
        // assign color index
		  	$flag = imagesetpixel( $tmp->getGdResource() , $x , $y , $i ) && $flag;
      }
    }
    
    // replace original resource
    $rsc = $tmp;
    
    // Africa ! J'ai envie de danser comme toi !
		return $flag;
  }
}
