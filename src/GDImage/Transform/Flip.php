<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Apply flip transformation on resource.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Transform_Interface
 */
class Transform_Flip implements Transform_Interface
{
	/**
	 * Bitwise value to represent horizontal flip.
   * Same value than \IMG_FLIP_HORIZONTAL.
	 *
	 * @var integer
	 * @const
	 */
	const HORIZONTAL = 1;
  // shortcut
	const H = 1;
	
	/**
	 * Bitwise value to represent vertical flip
   * Same value than \IMG_FLIP_VERTICAL.
	 *
	 * @var integer
	 * @const
	 */
	const VERTICAL = 2;
  // shortcut
	const V = 2;
	
	/**
	 * Bitwise value to represent horizontal and vertical flip
   * Same value than \IMG_FLIP_BOTH.
	 *
	 * @var integer
	 * @const
	 */
	const BOTH = 3;
  // shortcut
	const HV = 3;
	const VH = 3;
	
  /**
   * Flip mode, bitwise value.
   *
   * @var integer
   * @access protected
   */
  public $_mode = 0;
  
	/**
	 * Method to set flip mode.
   * String alowed while it corresponds to a class constant.
	 *
	 * @param mixed $mode
   * @return void
   * @access public
	 */
	public function setMode( $mode )
	{
    // constant lookup
    if( is_string( $mode ) ) $mode = constant( 'self::'.strtoupper( $mode ) );
    
		$this->_mode = $mode & self::BOTH;
	}
  
	/**
	 * Constructor
	 *
	 * @param [mixed] $mode
   * @return void
   * @access public
	 */
	public function __construct( $mode = null )
	{
    // for class extension, it is preferable to never automatically set properties on constructor
		if( $mode !== null ) $this->setMode( $mode );
	}
  	
  /**
   * Apply transformation to a resource.
   * Return false if the transformation fails.
   *
   * @param \GDImage\Resource_Abstract &$rsc
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Transform_Interface
   */
  public function __invoke( Resource_Abstract &$rsc )
  {
    // no mode, cannot apply
    if( ! $this->_mode ) return false;
    
    if( function_exists( 'imageflip' ) )
    {
      // sounds ok
      return imageflip( $rsc->getGdResource() , $this->_mode );
    }
    
    // do it the old fashion way, we could copy palette but we won't
    $tmp = $rsc->blank();

    $xmax = $rsc->getWidth();
    $ymax = $rsc->getHeight();

    $flag = true;

    switch( $this->_mode )
    {
      case self::BOTH:
        $x = $xmax;
        while( $x-- )
        {
          $y = $ymax;
          while( $y-- )
          {
            $flag = $tmp->setColorAt( $rsc->getColorAt( $x , $y ) , $xmax - 1 - $x , $ymax - 1 - $y ) && $flag;
          }
        }
      break;
      case self::VERTICAL:
        $x = $xmax;
        while( $x-- )
        {
          $y = $ymax;
          while( $y-- )
          {
            $flag = $tmp->setColorAt( $rsc->getColorAt( $x , $y ) , $x , $ymax - 1 - $y ) && $flag;
          }
        }
      break;
      case self::HORIZONTAL:
        $x = $xmax;
        while( $x-- )
        {
          $y = $ymax;
          while( $y-- )
          {
            $flag = $tmp->setColorAt( $rsc->getColorAt( $x , $y ) , $xmax - 1 - $x , $y ) && $flag;
          }
        }
      break;
    }
    
    // will destroy previous GD resource
    $rsc = $tmp;

    // done
    return $flag;
  }
}
