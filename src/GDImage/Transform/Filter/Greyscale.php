<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Alias for \GDImage\Transform_Filter_Grayscale
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Transform_Interface
 * @extends    \GDImage\Transform_Filter
 */
class Transform_Filter_Greyscale extends Transform_Filter_Grayscale
{
}
