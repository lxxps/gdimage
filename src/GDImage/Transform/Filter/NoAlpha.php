<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Transformation used to remove all alpha channel.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Transform_Interface
 * @extends    \GDImage\Transform_Filter
 */
class Transform_Filter_NoAlpha extends Transform_Filter
{
	/**
	 * Color operation 4x4 matrix
	 *
	 * @var array Matrix
	 * @access protected
	 */
	public $_matrix = array(
	  //     R   G   B   A
	  array( 1 , 0 , 0 , 0 ), // R
	  array( 0 , 1 , 0 , 0 ), // G
	  array( 0 , 0 , 1 , 0 ), // B
	  array( 0 , 0 , 0 , 0 ), // A
	);
	
}
