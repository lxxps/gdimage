<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Transformation used to convert a image into grayscale.
 * In this class, matrix is used to preserve transparent color.
 * 
 * Note that we do not use systematically imagefilter(), in order
 * to stay compatible with child of this class (no, we will not use final 
 * keyword).
 * 
 * Also, imagefilter() on palette color results to a loss of transparent color.
 * The transparent color is not grayscaled, and pixels from transparent color 
 * are replaced by a new color that correspond to transparent color grayscaled
 * but that is not the transparent color... so bad
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage transform
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements \GDImage\Transform_Interface
 * @extends    \GDImage\Transform_Filter
 */
class Transform_Filter_Grayscale extends Transform_Filter
{
  /**
   * Matrix representation of imagefilter() 
   * with IMG_FILTER_GRAYSCALE flag.
   * 
   * Help to earn some precious microseconds on
   * true color transformation.
   * 
   * @var array
   * @access private
   * @static
   */
  public static $__imagefilter_matrix = array(
	  //     R       G       B       A
	  array( 0.299 , 0.587 , 0.114 , 0 ), // R
	  array( 0.299 , 0.587 , 0.114 , 0 ), // G
	  array( 0.299 , 0.587 , 0.114 , 0 ), // B
	  array( 0     , 0     , 0     , 1 ), // A
	);
  
	/**
	 * Color operation 4x4 matrix
	 *
	 * @var array Matrix
	 * @access protected
	 */
	public $_matrix = array(
	  //     R       G       B       A
	  array( 0.299 , 0.587 , 0.114 , 0 ), // R
	  array( 0.299 , 0.587 , 0.114 , 0 ), // G
	  array( 0.299 , 0.587 , 0.114 , 0 ), // B
	  array( 0     , 0     , 0     , 1 ), // A
	);
	
  /**
   * Method used to apply filter transformation on true color image.
   *
   * @param \GDImage\Image_TrueColor $image
   * @return boolean Success flag
   * @access protected
   */
  public function _filterTrueColor( Resource_Abstract &$rsc )
  {
    // if matrixes looks the same
    if( $this->_matrix === self::$__imagefilter_matrix )
    {
      // we can apply imagefilter( IMG_FILTER_GRAYSCALE )
      // @see http://php.net/manual/en/function.imagefilter.php
      return imagefilter( $rsc->getGdResource() , \IMG_FILTER_GRAYSCALE );
    }
    
    // if not, call parent method
    return parent::_filterTrueColor( $rsc );
  }
}
