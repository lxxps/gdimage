<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * \GDImage\Exception_APng specific exception class.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage exception
 * @extends    \GDImage\Exception
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Exception_APng extends Exception
{
  /**
   * Array of exceptions messages by code.
   * Range of message are:
   *  - from 7000 to 7024: Unpacker exceptions
   *  - from 7025 to 7049: Decomposer exceptions
   *  - from 7050 to 7074: Composer exceptions
   *  - from 7075 to 7099: Packer exceptions
   * 
   * @var array
   * @access protected
   */
  public $_messages = array(
    // Unpacker
    7000 => '%s: Invalid Signature.' ,
    7001 => '%s: Invalid Signature "%s" ("%s" expected).' ,
    7005 => '%s: Invalid %s Chunk Length "0x%04X" ("0x%04X" expected).' ,
    7006 => '%s: Invalid %s Chunk Length "0x%04X" ("0x%04X" minimal length).' ,
    7007 => '%s: Invalid IHDR Chunk Coulour Type "%2$d" for %1$s Chunk.' ,
    // Decomposer
    7025 => '%s: Missing IHDR Chunk.' ,
    7026 => '%s: Missing IDAT Chunk.' ,
    7027 => '%s: All fdAT Chunks must have a fcTL Chunk (%d fdAT Chunks against %d fcTL Chunks).' ,
    7028 => '%s: Cannot create GD resource from binary data for IDAT frame.' ,
    7029 => '%s: Cannot create GD resource from binary data for frame %d.' ,
    7030 => '%s: Unknown Dispose option %d for frame %d.' ,
    7031 => '%s: Invalid Coulour Type "%d" for frame %d.' ,
    // Composer
    7061 => '%s: Unknown Dispose Option %d for frame %d.' ,
    7062 => '%s: Unknown Blend Option %d for frame %d.' ,
    // Packer
  );
}