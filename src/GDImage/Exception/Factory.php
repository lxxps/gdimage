<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * \GDImage\Exception_Factory specific exception class.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage exception
 * @extends    \GDImage\Exception
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Exception_Factory extends Exception
{
  /**
   * Array of exceptions messages by code.
   * 
   * @var array
   * @access protected
   */
  public $_messages = array(
    // From 3000 to 3100: \\GDImage\\Factory usage exceptions
    3000 => '%s: Unable to create image instance (%s).' ,
    3010 => '%s: Invalid data URI (%s).' ,
    3020 => '%s: Invalid base64 string (%s).' ,
    3030 => '%s: File does not exists (%s).' ,
    3040 => '%s: Morpher "%s" must give a file, some binary data or an image.' ,
    3050 => '%s: Unable to determine \\GDImage\\Image_Interface to use from these arguments (MIME Type: %s, file path: %s, binary data: %s, resource: %s).' ,
    3060 => '%s: "%s" setting must be set.' ,
    3070 => '%s: Unable to determine \\GDImage\\Factory_ExportDriver_Interface to use from "%s".' ,
  );
}