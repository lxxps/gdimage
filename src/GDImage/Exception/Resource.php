<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * \GDImage\Exception_Resource specific exception class.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage exception
 * @extends    \GDImage\Exception
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Exception_Resource extends Exception
{
  /**
   * Array of exceptions messages by code.
   * 
   * @var array
   * @access protected
   */
  public $_messages = array(
    // From 4000 to 4100: \\GDImage\\Resource usage exceptions
    4000 => '%s: Invalid resource (%s).' ,
    4010 => '%s: Invalid resource (palette color resource).' ,
    4020 => '%s: Invalid resource (true color resource).' ,
    4030 => '%s: Invalid width and height (%s;%s).' ,
    4040 => '%s: Invalid binary data (checksum: %s).' ,
  );
}