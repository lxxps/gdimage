<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * \GDImage\Exception_Transform specific exception class.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage exception
 * @extends    \GDImage\Exception
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Exception_Memory extends Exception
{
  /**
   * Array of exceptions messages by code.
   * Range of message are:
   *  - from 5000 to 6000: Usage exceptions
   * 
   * @var array
   * @access protected
   */
  public $_messages = array(
    8000 => '%s: Not enough memory to release %s (limit: %s, usage: %s, require: %s).' ,
    8010 => '%s: Unable to determine width and height from "%s".' ,
  );
}