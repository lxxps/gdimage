<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * \GDImage\Exception_AGif specific exception class.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage exception
 * @extends    \GDImage\Exception
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Exception_AGif extends Exception
{
  /**
   * Array of exceptions messages by code.
   * Range of message are:
   *  - from 6000 to 6049: Unpacker exceptions
   *  - from 6050 to 6059: Decomposer exceptions
   *  - from 6060 to 6069: Composer exceptions
   *  - from 6070 to 6099: Packer exceptions
   * 
   * @var array
   * @access protected
   */
  public $_messages = array(
    // Unpacker
    6000 => '%s: Invalid Signature "%s" ("%s" expected).' ,
    6001 => '%s: Unknown Version "%s".' ,
    6005 => '%s: Invalid Extension Introducer "0x%02X" ("0x%02X" expected).' ,
    6006 => '%s: Unknown Extension Label "0x%02X".' ,
    6010 => '%s: Invalid Block Size %d bytes for Application Extension (%d bytes expected).' ,
    6015 => '%s: Invalid Block Size %d bytes for Graphic Control Extension (%d expected).' ,
    6016 => '%s: Invalid Block Terminator for Graphic Control Extension "0x%02X" ("0x%02X" expected).' ,
    6020 => '%s: Invalid Block Size %d bytes for Plain Text Extension (%d bytes expected).' ,
    6025 => '%s: Invalid Trailer "0x%02X" (EOF expected).' ,
    6030 => '%s: Invalid Block Terminator for Application Extension XMP Data/XMP "0x%02X" ("0x%02X" expected).' ,
    6031 => '%s: Invalid Block Size %d for Application Extension NETSCAPE/2.0 (%d bytes expected).' ,
    6032 => '%s: Invalid Sub-Block Index "0x%02X" for Application Extension NETSCAPE/2.0 ("0x%02X" expected).' ,
    6033 => '%s: Invalid Header.' ,
    // Decomposer
    6050 => '%s: Missing Image Descriptor for index %d.' ,
    6051 => '%s: Missing Table Based Image Data for index %d.' ,
    6052 => '%s: Missing Local Color Table for index %d.' ,
    6053 => '%s: Missing Global Color Table.' ,
    6055 => '%s: All Image Descriptor must have a Graphic Control Extension (%d Image Descriptor against %d Graphic Control Extension).' ,
    6056 => '%s: Cannot create GD resource from binary data for frame %d.' ,
    6057 => '%s: Unknown Disposal Method %d for frame %d.' ,
    6058 => '%s: Missing Logical Screen Descriptor.' ,
    // Composer
    6060 => '%s: Cannot create GIF pack for frame %d' ,
    6061 => '%s: Unknown Disposal Method %d for frame %d.' ,
  );
}