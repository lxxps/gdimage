<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * \GDImage\Exception_Transform specific exception class.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage exception
 * @extends    \GDImage\Exception
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Exception_Transform extends Exception
{
  /**
   * Array of exceptions messages by code.
   * Range of message are:
   *  - from 5000 to 6000: Usage exceptions
   * 
   * @var array
   * @access protected
   */
  public $_messages = array(
    5000 => '%s: Transformation for key "%s" does not exist.' ,
    5010 => '%s: Resampling "%s" must be callable.' ,
    5020 => '%s: Missing method %1$s->_convolutionForAlpha%s() for alpha mode "%s".' ,
    5021 => '%s: Missing method %1$s->_applyMatrixForAlpha%s() for alpha mode "%s".' ,
    5022 => '%s: Missing method %1$s->_applyMatrixAtForAlpha%s() for alpha mode "%s".' ,
    5025 => '%s: Missing method %1$s->_convolutionForEdge%s() for edge mode "%s".' ,
    5026 => '%s: Missing method %1$s->_prepareSourceColorsForEdge%s() for edge mode "%s".' ,
    5030 => '%s: Blend mode "%s" must be callable.' ,
    5031 => '%s: Blend mode "%s" must be a valid method.' ,
  );
}