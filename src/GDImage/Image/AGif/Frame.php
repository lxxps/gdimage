<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration settings:
 * - "image_a_gif_delay_time": default Delay Time for AGIF frame
 * - "image_a_gif_disposal_method": default Disposal Method for AGIF frame
 * 
 * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt
 */
if( ! Config::hasImageAGifDelayTime() ) {
  Config::setImageAGifDelayTime( 0 ); // 0 means "as fast as possible"
}
if( ! Config::hasImageAGifDisposalMethod() ) {
  Config::setImageAGifDisposalMethod( 2 ); // 2 means "restore to background"
}

/**
 * Class to represent a GIF frame for animated GIF.
 * 
 * This class extends Image_Gif with a delay and a disposal method.
 * 
 * An important thing about AGIF delay time is that it can be 0, and the GIF
 * still animated: this is due to browser minimum frame delay and most of the 
 * time, this 0 delay is rounded up to 100ms.
 * @see http://nullsleep.tumblr.com/post/16524517190/animated-gif-minimum-frame-delay-browser-compatibility
 * 
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage agif
 * @author     Loops <pierrotevrard@gmail.com>
 * @abstract
 * @implements \GDImage\Image_Interface
 * @extends    \GDImage\Image_Gif
 */
class Image_AGif_Frame extends Image_Gif
{
  
  /**
   * Delay time in 1/100th of a second
   * 0 means as fast as possible
   * 
   * @var integer
   * @access protected
   */
  public $_delay;
  
  /**
   * Set delay time in 1/100th of a second
   * 
   * @param integer $delay
   * @return void
   * @access public
   */
  public function setDelayTime( $delay )
  {
    // force integer and limit to 2 bytes
    $this->_delay = $delay & 0xFFFF;
  }
  
  /**
   * Alias for setDelayTime
   * 
   * @deprecated
   * @param integer $delay
   * @return void
   * @access public
   */
  public function setDelay( $delay )
  {
    $this->setDelayTime( $delay );
  }
  
  /**
   * Get delay time in 1/100th of a second
   * 
   * @param none
   * @return integer
   * @access public
   */
  public function getDelayTime()
  {
    return $this->_delay;
  }
  
  /**
   * Alias for getDelayTime
   * 
   * @deprecated
   * @param none
   * @return integer
   * @access public
   */
  public function getDelay()
  {
    return $this->getDelayTime();
  }
  
  /**
   * Disposal method
   * 
   * @var integer
   * @access protected
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  public $_disposal;
  
  /**
   * Constants for disposal method "unspecified"
   * 
   * From specification:
   * 0 - No disposal specified. The decoder is not required to take any action.
   * 
   * @var integer
   * @const
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   */
  const DISPOSAL_UNSPECIFIED = 0;
  
  /**
   * Constants for disposal method "no"
   * 
   * From specification:
   * 1 - Do not dispose. The graphic is to be left in place.
   * 
   * @var integer
   * @const
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   * @see http://www.theimage.com/animation/pages/disposal.html
   * @see http://www.theimage.com/animation/pages/disposal2.html
   * @see http://www.theimage.com/animation/pages/disposal3.html
   */
  const DISPOSAL_NO = 1;
  
  /**
   * Constants for disposal method "background"
   * 
   * From specification:
   * 2 - Restore to background color. The area used by the graphic must be 
   *     restored to the background color.
   * 
   * @var integer
   * @const
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   * @see http://www.theimage.com/animation/pages/disposal.html
   * @see http://www.theimage.com/animation/pages/disposal2.html
   * @see http://www.theimage.com/animation/pages/disposal3.html
   */
  const DISPOSAL_BACKGROUND = 2;
  
  /**
   * Constants for disposal method "previous"
   * 
   * From specification:
   * 3 - Restore to previous. The decoder is required to restore the area 
   *     overwritten by the graphic with what was there prior to rendering 
   *     the graphic.
   * 
   * @var integer
   * @const
   * @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
   * @see http://www.theimage.com/animation/pages/disposal.html
   * @see http://www.theimage.com/animation/pages/disposal2.html
   * @see http://www.theimage.com/animation/pages/disposal3.html
   */
  const DISPOSAL_PREVIOUS = 3;
  
  /**
   * Set disposal method
   * 
   * @param integer $disposal
   * @return void
   * @access public
   */
  public function setDisposalMethod( $disposal )
  {
    // force integer and limit to 3 bits
    $this->_disposal = $disposal & 7;
  }
  
  /**
   * Alias for setDisposalMethod()
   * 
   * @deprecated
   * @param integer $disposal
   * @return void
   * @access public
   */
  public function setDisposal( $disposal )
  {
    $this->setDisposalMethod( $disposal );
  }
  
  /**
   * Get disposal method
   * 
   * @param none
   * @return integer
   * @access public
   */
  public function getDisposalMethod()
  {
    return $this->_disposal;
  }
  
  /**
   * Alias for getDisposalMethod()
   * 
   * @deprecated
   * @param none
   * @return integer
   * @access public
   */
  public function getDisposal()
  {
    return $this->getDisposalMethod();
  }
  
  // Extends
  
  /**
   * Create the resource to another \GDImage\Image_Interface.
   * Note that there is no need to clone image, let the developer decides.
   *
   * @param Image_Interface $image
   * @return boolean Success flag
   * @access public
   * @throws \GDImage\Exception_Resource
   * @implements \GDImage\Image_Interface
   * @extends \GDImage\Image_Abstract_Single
   */
  public function toImage( Image_Interface $image )
  {
    $flag = parent::toImage( $image );
    
    // AGif frame to AGif frame
    if( $image instanceof self )
    {
      $image->setDelayTime( $this->getDelayTime() );
      $image->setDisposalMethod( $this->setDisposalMethod() );
    }
    
    // AGif frame to APng frame
    if( $image instanceof Image_APng_Frame )
    {
      // update delay
      $image->setDelayNumerator( $this->getDelayTime() );
      $image->setDelayDenominator( 100 );
      
      // APng Dispose Option is relatively similar to AGif Disposal Method
      // @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
      // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
      switch( $this->getDisposalMethod() )
      {
        case self::DISPOSAL_UNSPECIFIED: // "unspecified" can be considered as "no"
        case self::DISPOSAL_NO: 
          $image->setDisposeOption( Image_APng_Frame::DISPOSE_NO );
        break;
        case self::DISPOSAL_BACKGROUND: 
          $image->setDisposeOption( Image_APng_Frame::DISPOSE_BACKGROUND );
        break;
        case self::DISPOSAL_PREVIOUS: 
          $image->setDisposeOption( Image_APng_Frame::DISPOSE_PREVIOUS );
        break;
      }
      
      // because GIF does not have semi-transparency, Blend Option can be "over"
      $image->setBlendOption( Image_APng_Frame::BLEND_OVER );
    }
  }
  
  /**
   * Reset the image to its default state
   * 
   * @param none
   * @return void
   * @access public
   * @implements \GDImage\Image_Interface
   * @extends \GDImage\Image_Abstract_Single
   */
  public function reset()
  {
    // assing defaults
    $this->setDelayTime( Config::getImageAGifDelayTime() );
    $this->setDisposalMethod( Config::getImageAGifDisposalMethod() );
    
    // unset resource
    $this->unsetResource();
  }
}
