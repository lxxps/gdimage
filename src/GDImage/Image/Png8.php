<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration settings:
 * - "image_png8_true_color_palette": number of colors allowed when PNG8
 *   is exported from a \GDImage\Resource_TrueColor
 * - "image_png8_true_color_dither": dither flag to use when PNG8
 *   is exported from a \GDImage\Resource_TrueColor
 * 
 * @see http://php.net/manual/en/function.imagetruecolortopalette.php
 */
if( ! Config::hasImagePng8TrueColorPalette() ) {
  Config::setImagePng8TrueColorPalette( 256 );
}
if( ! Config::hasImagePng8TrueColorDither() ) {
  Config::setImagePng8TrueColorDither( false );
}

/**
 * Class for single PNG8 resource.
 * PNG8 works with \GDImage\Resource_PaletteColor by default.
 * PNG8 can store alpha channel on indexed color.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage image
 * @author     Loops <pierrotevrard@gmail.com>
 * @abstract
 * @implements \GDImage\Image_Interface
 * @extends    \GDImage\Image_Png
 */
class Image_Png8 extends Image_Png
{
  
  // Implements
  
  /**
   * Detect if the class may be used.
   * Return an integer value that will be used as priority for import/creation.
   * If the value returned is greater or equal than self::MUSTBE, not any other 
   * classes will be detected.
   *
   * @param string $mimetype                 Expected MIME Type
   * @param string $filepath                 File path, may be null
   * @param string $binary                   Binary data, may be null
   * @param \GDImage\Image_Interface $image  Image, may be null
   * @return integer 
   * @access public
   * @static
   * @implements \GDImage\Image_Interface
   */
  public static function detect( $mimetype , $filepath = null , $binary = null , Image_Interface $image = null )
  {
    // we do not need to call parent method
    if( $mimetype !== static::getMimeType() ) return static::NOTBE;
    
    // this class MAY BE used (not 100% sure)
    $priority = static::MAYBE;
    
    // only if MIME Type match
    if( $image && $image instanceof Image_Abstract_Single  )
    {
      // this class MAY BE used (not 100% sure, but more probable)
      $priority += static::MAYBE;

      // only with single resource
      if( $image->getResource() && $image->getResource()->isPaletteColor() )
      {
        // this class MAY BE used (not 100% sure, but more probable)
        $priority += static::MAYBE;
      }
    }
    
    return $priority;
  }
  
  /**
   * Export resource to a file path.
   * 
   * Custom compression level can be set for this export.
   * Custom PNG filters can be set for this export.
   * 
   * Colors and dither are only used when PNG8 has to be created from a 
   * \GDImage\Resource_TrueColor.
   *
   * @param  string $filepath Filepath to export
   * @param  [integer] $compression PNG compression level
   * @param  [integer] $filters PNG filters
   * @param  [integer] $colors Number of colors
   * @param  [boolean] $dither Use dither
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Image_Interface
   */
  public function toFile( $filepath , $compression = null , $filters = null , $colors = null , $dither = null )
  {
  	$compression = $compression === null ? Config::getImagePngCompression() : max( 0 , min( $compression | 0 , 9 ) );
  	$filters = $filters === null ? Config::getImagePngFilters() : $filters | 0;
    
    // we want to sanitize resource without modify original one
    $clean_rsc = $this->_getSanitizedResource( $colors  , $dither );
    
    // automatic filters
    if( $filters === self::AUTOMATIC_FILTERS ) $filters = $this->_getAutomaticFilters( $clean_rsc );
    
    return imagepng( $clean_rsc->getGdResource() , $filepath , $compression , $filters );
  }
  
  /**
   * Export resource to binary data.
   * 
   * Custom compression level can be set for this export.
   * Custom PNG filters can be set for this export.
   * 
   * Colors and dither are only used when PNG8 has to be created from a 
   * \GDImage\Resource_TrueColor.
   *
   * @param  [integer] $compression PNG compression level
   * @param  [integer] $filters PNG filters
   * @param  [integer] $colors Number of colors
   * @param  [boolean] $dither Use dither
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Image_Interface
   */
  public function toBinary( $compression = null , $filters = null , $colors = null , $dither = null )
  {
  	$compression = $compression === null ? Config::getImagePngCompression() : max( 0 , min( $compression | 0 , 9 ) );
  	$filters = $filters === null ? Config::getImagePngFilters() : $filters | 0;
    
    // we want to sanitize resource without modify original one
    $clean_rsc = $this->_getSanitizedResource( $colors  , $dither );
    
    // automatic filters
    if( $filters === self::AUTOMATIC_FILTERS ) $filters = $this->_getAutomaticFilters( $clean_rsc );
    
    // start buffering
    ob_start();
    $flag = imagepng( $clean_rsc->getGdResource() , null , $compression , $filters );
    $binary = ob_get_contents();
    ob_end_clean();
    
    if( $flag ) return $binary;
    // failure
    return false;
  }
  
  /**
   * Create the resource from width and height.
   *
   * @param integer $w
   * @param integer $h
   * @return boolean Success flag
   * @access public
   * @throws \GDImage\Exception_Resource
   * @implements \GDImage\Image_Interface
   */
  public function fromSize( $w , $h )
  {
    // restore default
    $this->reset();
      
    $this->setResource( Resource_PaletteColor::createFromSize( $w , $h ) );
    
    return true;
  }
  
  /**
   * Method to fetch a sanitized Resource from current one.
   * 
   * It appears than after some manipulation, alpha channel on 
   * transparent color may be lost.
   * For GIF, it is not an issue because GIF make transparent color as 
   * transparent.
   * But on PNG8, the transparent color is just considered as another color
   * and alpha channel is drawn as is.
   * 
   * Parameters are only used when PNG8 has to be created from a 
   * \GDImage\Resource_TrueColor.
   *
   * @param  [integer] $colors Number of colors
   * @param  [boolean] $dither Use dither
   * @return \GDImage\Resource_PaletteColor
   * @access public
   */
  public function _getSanitizedResource( $colors = null , $dither = null )
  {
    // make sure alpha is saved
    $this->_rsc->setMode( Resource_Abstract::MODE_SAVEALPHA );
    
    if( $this->_rsc->isTrueColor() )
    {
      $colors = $colors === null ? Config::getImagePng8TrueColorPalette() : max( 0 , $nb | 0 );
      $dither = $dither === null ? Config::getImagePng8TrueColorDither() : (bool)$dither;
      
      return $this->_rsc->toPaletteColor( $colors , $dither );
    }
    
    // on palette color, we want to make sure transparent color has 
    // an alpha channel of 127
    
    // before to manipulate colors, make sure resource is awaken
    $this->_rsc->awakeGdResource();
    
    // there is a transparent color
    if( $color = $this->_rsc->getTransparent() )
    {
      // and the transparent color needs to be sanitize
      if( $color->getAlpha() !== Color::ALPHA_TRANSPARENT )
      {
        // change alpha
        $color->setAlpha( Color::ALPHA_TRANSPARENT );
        // clone original resource
        $rsc = clone $this->_rsc;
        // assign new transparent color
        $rsc->setTransparent( $color );
        // return the clone
        return $rsc;
      }
    }
    
    // probably nothing to do
    return $this->_rsc;
  }

}
