<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration settings:
 * - "image_a_gif_loops": default number of loops (0 means infinite)
 * 
 * @see http://en.wikipedia.org/wiki/Graphics_Interchange_Format#Animated_GIF
 */
if( ! Config::hasImageAGifLoops() ) {
  Config::setImageAGifLoops( 0 ); // 0 means infinite
}

/**
 * Class for animated GIF resources.
 * Frames are automatically converted to \GDImage\Image_AGif_Frame instances.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage image
 * @author     Loops <pierrotevrard@gmail.com>
 * @abstract
 * @implements \GDImage\Image_Interface
 * @extends    \GDImage\Image_Abstract_Multiple
 */
class Image_AGif extends Image_Abstract_Multiple
{
  
  /**
   * Number of repeatitions (NETSCAPE 2.0 specific).
   * 0 means infinite.
   * -1 means no loop.
   * 
   * @var integer
   * @access protected
   * @see http://en.wikipedia.org/wiki/Graphics_Interchange_Format#Animated_GIF
   */
  public $_loops = 0;
  
  /**
   * Set number of repeatitions (NETSCAPE 2.0 specific).
   * 0 means infinite.
   * 
   * @param integer $nb
   * @return void
   * @access public
   * @see http://en.wikipedia.org/wiki/Graphics_Interchange_Format#Animated_GIF
   */
  public function setLoops( $nb )
  {
    // no loops
    if( $nb < 0 ) $this->_loops = self::LOOPS_NONE;
    // force integer and limit to 2 bytes
    else $this->_loops = $nb & 0xFFFF;
  }
  
  /**
   * Get number of repeatitions (NETSCAPE 2.0 specific).
   * 0 means infinite.
   * 
   * @param none
   * @return integer
   * @access public
   * @see http://en.wikipedia.org/wiki/Graphics_Interchange_Format#Animated_GIF
   */
  public function getLoops()
  {
    return $this->_loops;
  }
  
  /**
   * Constants for inifinite loops.
   * 
   * @var integer
   * @const
   * @see http://en.wikipedia.org/wiki/Graphics_Interchange_Format#Animated_GIF
   */
  const LOOPS_INFINITE = 0;
  
  /**
   * Constants to represent no loop.
   * 
   * @var integer
   * @const
   */
  const LOOPS_NONE = -1;
  
  
  // Implements
  
  /**
   * Method to prepare a frame before to add it to frames array.
   * 
   * To be able to manipulate frames healthy, all frame must extends the
   * same class. Sounds logical, not?
   *
   * @param Image_Interface $image
   * @return Image_Interface
   * @access protected
   * @implements \GDImage\Image_Abstract_Multiple
   */
  public function _prepareFrame( Image_Interface $image )
  {
    // create an empty frame
    $frame = AGif_Factory::frame();
    
    // instanceof? ok
    if( $image instanceof $frame ) return $image;
    
    // create frame instance from image
    // keep in mind that this method will not clone resource
    $frame->fromImage( $image );
    
    // finally return the frame instance
    return $frame;
  }
  
  /**
   * Return expected MIME Type for this kind of image.
   *
   * @param none
   * @return string 
   * @access public
   * @static
   * @implements \GDImage\Image_Interface
   */
  public static function getMimeType()
  {
    return 'image/gif';
  }
  
  /**
   * Static array for cache lookup on unpack method.
   * 
   * Even with smart detection, we cannot be sure that a GIF is really a AGIF
   * without unpacking it. So the unpack process must be done on detection and 
   * creation methods.
   * 
   * This property store valid packs to not call the unpack process twice.
   * 
   * @var array
   * @access protected
   * @static
   */
  static public $_pack_cache = array();
  
  /**
   * Detect if the class may be used.
   * Return an integer value that will be used as priority for import/creation.
   * If the value returned is greater or equal than self::MUSTBE, not any other 
   * classes will be detected.
   *
   * @param string $mimetype                 Expected MIME Type
   * @param string $filepath                 File path, may be null
   * @param string $binary                   Binary data, may be null
   * @param \GDImage\Image_Interface $image  Image, may be null
   * @return integer 
   * @access public
   * @static
   * @implements \GDImage\Image_Interface
   */
  public static function detect( $mimetype , $filepath = null , $binary = null , Image_Interface $image = null )
  {
    if( $mimetype !== static::getMimeType() ) return static::NOTBE;
    
    // this class MAY BE used (not 100% sure)
    $priority = static::MAYBE;
    
    // only if MIME Type match
    if( $image && $image instanceof Image_Abstract_Multiple  )
    {
      // this class MAY BE used (not 100% sure, but more probable)
      $priority += static::MAYBE;
    }
      
    // filepath check
    if( $filepath )
    {
      try
      {
        // we may not want to create a big string just to detect 
        // Graphic Control Extension stuff
        
        // parse GIF to check if there is multiple frame
        $pack = AGif_Factory::unpack( $filepath );

        // if this key is set, that means that we have at least two frames
        if( isset( $pack['Image Descriptor1'] ) )
        {
          // this class MAY BE used (not 100% sure, but more probable)
          $priority += static::MAYBE;
          
          // store the pack in static array
          
          // create cache key
          $cache_key = md5( $filepath );
          // store the pack
          static::$_pack_cache[$cache_key] = $pack;
        }
      }
      catch( Exception_AGif $e )
      {
        // do not throw this exception
      }
    }

    // binary check
    if( $binary )
    {
      try
      {
        // before to unpack, just do a simple strpos on specific string
        
        // we know that AGIF must have at least two Graphic Control Extension
        // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
        $gce = pack( 'CCCC'
          , 0 // Block terminator, 1 byte
          , 0x21 // Extension Introducer, 1 byte
          , 0xF9 // Graphic Control Label, 1 byte
          , 4 // Block Size, fixed to 4, 1 byte
        );
        
        if( ( $pos = strpos( $binary , $gce ) ) // cannot be the first string found
         && strpos( $binary , $gce , $pos + 8 ) ) // 4 bytes for $gce + 4 bytes from Block Size
        {
          // of course never trust a string check on binary data, this check 
          // does not mean that the file is AGIF, but that it may be AGIF
          
          // parse GIF to check if there is multiple frame
          $pack = AGif_Factory::unpack( $binary );

          // if this key is set, that means that we have at least two frames
          if( isset( $pack['Image Descriptor1'] ) )
          {
            // this class MAY BE used (not 100% sure, but more probable)
            $priority += static::MAYBE;

            // store the pack in static array

            // create cache key
            $cache_key = md5( $binary );
            // store the pack
            static::$_pack_cache[$cache_key] = $pack;
          }
        }
      }
      catch( Exception_AGif $e )
      {
        // do not throw this exception
      }
    }
    
    return $priority;
  }
  
  /**
   * Import resource from a file path
   *
   * @param string $filepath File path to import
   * @return boolean True if success
   * @access public
   * @throws \GDImage\Exception_AGif
   * @implements \GDImage\Image_Interface
   */
  public function fromFile( $filepath )
  {
    // restore default
    $this->reset();
    
    try
    {
      // do cache lookup
      $cache_key = md5( $filepath );
      if( isset( static::$_pack_cache[$cache_key] ) )
      {
        // fetch
        $pack = static::$_pack_cache[$cache_key];
        // then delete
        unset( static::$_pack_cache[$cache_key] );
      }
      else
      {
        // unpack data
        $pack = AGif_Factory::unpack( $filepath );
      }
    
      // now extract frame
      $this->setFrames( AGif_Factory::decompose( $pack ) );

      // look for the first Application Extension if it has Number of Repeatitions

      // this is the default value on import, not the default value on construction
      $this->setLoops( self::LOOPS_NONE );

      // this Application Extension should be the first one set but we never know...
      $i = 0;
      while( isset( $pack['Application Extension'.$i] ) )
      {
        if( isset( $pack['Application Extension'.$i]['Number of Repetitions'] ) )
        {
          $this->setLoops( $pack['Application Extension'.$i]['Number of Repetitions'] );
          break;
        }
        $i++;
      }

      return true;
    }
    catch( Exception_AGif $e )
    {
      // do not throw this exception
      return false;
    }
  }
  
  /**
   * Export resource to a file path.
   *
   * @param string $filepath File path to export
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Image_Interface
   */
  public function toFile( $filepath )
  {
    if( count($this->_frames) > 1 )
    {
      $pack = AGif_Factory::compose( $this->_frames , $this->_loops );

      $binary = AGif_Factory::pack( $pack );
      
      // free memory
      unset( $pack );

      return file_put_contents( $filepath , $binary );
    }
    
    // only one picture?
    // GIF lookup
    return Factory::export( reset( $this->_frames ) , array( 
      'driver' => Factory::DRIVER_FILE ,
      'file' => $filepath ,
    ) , static::getMimeType() );
  }
  
  /**
   * Import resource from binary data.
   *
   * @param string $binary
   * @return boolean Success flag
   * @access public
   * @throws \GDImage\Exception_AGif
   * @implements \GDImage\Image_Interface
   */
  public function fromBinary( $binary )
  {
    // restore default
    $this->reset();
    
    try
    {
      // do cache lookup
      $cache_key = md5( $binary );
      if( isset( static::$_pack_cache[$cache_key] ) )
      {
        // fetch
        $pack = static::$_pack_cache[$cache_key];
        // then delete
        unset( static::$_pack_cache[$cache_key] );
      }
      else
      {
        // unpack data
        $pack = AGif_Factory::unpack( $binary );
      }
    
      // now extract frame
      $this->setFrames( AGif_Factory::decompose( $pack ) );

      // look for the first Application Extension if it has Number of Repeatitions

      // this is the default value on import, not the default value on construction
      $this->setLoops( self::LOOPS_NONE );

      // this Application Extension should be the first one set but we never know...
      $i = 0;
      while( isset( $pack['Application Extension'.$i] ) )
      {
        if( isset( $pack['Application Extension'.$i]['Number of Repetitions'] ) )
        {
          $this->setLoops( $pack['Application Extension'.$i]['Number of Repetitions'] );
          break;
        }
        $i++;
      }

      return true;
    }
    catch( Exception_AGif $e )
    {
      // do not throw this exception
      return false;
    }
  }
  
  /**
   * Export resource to binary data.
   *
   * @param none
   * @return string
   * @access public
   * @implements \GDImage\Image_Interface
   */
  public function toBinary()
  {
    if( count($this->_frames) > 1 )
    {
      $pack = AGif_Factory::compose( $this->_frames , $this->_loops );
    
      return AGif_Factory::pack( $pack );
    }
    
    // only one picture?
    // GIF lookup
    return Factory::export( reset( $this->_frames ) , array( 
      'driver' => Factory::DRIVER_BINARY ,
    ) , static::getMimeType() );
    
  }
  
  /**
   * Create the resource from width and height.
   *
   * @param integer $w
   * @param integer $h
   * @return boolean Success flag
   * @access public
   * @throws \GDImage\Exception_Resource
   * @implements \GDImage\Image_Interface
   */
  public function fromSize( $w , $h )
  {
    // restore default
    $this->reset();
    
    // create one image with this size
    $frame = AGif_Factory::frame();
    $frame->fromSize( $w , $h );
    // reset image and assign first one
    $this->setFrames( array( $frame ) );
    
    return true;
  }
  
  /**
   * Get width of the image.
   * 
   * The first frame of animated GIF always fits Logical Screen Descriptor.
   * So, to stay close to this behavior, we return the width of the first frame.
   *
   * @param none
   * @return integer Width
   * @access public
   * @implements \GDImage\Image_Interface
   * @extends \GDImage\Image_Abstract_Multiple
   */ 
  public function getWidth()
  {
    if( isset($this->_frames[0]) )
    {
  	  return $this->_frames[0]->getWidth();
    }
    
    return null;
  }
  
  /**
   * Get height of the image.
   * 
   * The first frame of animated GIF always fits Logical Screen Descriptor.
   * So, to stay close to this behavior, we return the height of the first frame.
   *
   * @param none
   * @return integer Height
   * @access public
   * @implements \GDImage\Image_Interface
   * @extends \GDImage\Image_Abstract_Multiple
   */ 
  public function getHeight()
  {
    if( isset($this->_frames[0]) )
    {
  	  return $this->_frames[0]->getHeight();
    }
    
    return null;
  }
  
  /**
   * Reset the image to its default state
   *
   * @param none
   * @return void
   * @access public
   * @implements \GDImage\Image_Interface
   * @extends \GDImage\Image_Abstract_Multiple
   */
  public function reset()
  {
    // reset loops
    $this->setLoops( Config::getImageAGifLoops() );
    // unset frames
    $this->unsetFrames();
  }

}
