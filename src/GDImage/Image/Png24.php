<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Class for single PNG24 resource.
 * PNG24 works with \GDImage\Resource_TrueColor by default.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage image
 * @author     Loops <pierrotevrard@gmail.com>
 * @abstract
 * @implements \GDImage\Image_Interface
 * @extends    \GDImage\Image_Png
 */
class Image_Png24 extends Image_Png
{
  
  // Implements
  
  /**
   * Detect if the class may be used.
   * Return an integer value that will be used as priority for import/creation.
   * If the value returned is greater or equal than self::MUSTBE, not any other 
   * classes will be detected.
   *
   * @param string $mimetype                 Expected MIME Type
   * @param string $filepath                 File path, may be null
   * @param string $binary                   Binary data, may be null
   * @param \GDImage\Image_Interface $image  Image, may be null
   * @return integer 
   * @access public
   * @static
   * @implements \GDImage\Image_Interface
   */
  public static function detect( $mimetype , $filepath = null , $binary = null , Image_Interface $image = null )
  {
    // we do not need to call parent method
    if( $mimetype !== static::getMimeType() ) return static::NOTBE;
    
    // this class MAY BE used (not 100% sure)
    $priority = static::MAYBE;
    
    // only if MIME Type match
    if( $image && $image instanceof Image_Abstract_Single )
    {
      // this class MAY BE used (not 100% sure, but more probable)
      $priority += static::MAYBE;

      // only with single resource
      if( $image->getResource() && $image->getResource()->isTrueColor() )
      {
        // this class MAY BE used (not 100% sure, but more probable)
        $priority += static::MAYBE;
      }
    }
    
    return $priority;
  }
  
  /**
   * Create the resource from width and height.
   *
   * @param integer $w
   * @param integer $h
   * @return boolean Success flag
   * @access public
   * @throws \GDImage\Exception_Resource
   * @implements \GDImage\Image_Interface
   */
  public function fromSize( $w , $h )
  {
    // restore default
    $this->reset();
      
    $this->setResource( Resource_TrueColor::createFromSize( $w , $h ) );
    
    return true;
  }
  
  /**
   * Method to fetch a sanitized Resource from current one.
   *
   * @param  none
   * @return \GDImage\Resource_TrueColor
   * @access public
   */
  public function _getSanitizedResource()
  {
    // make sure alpha is saved
    $this->_rsc->setMode( Resource_Abstract::MODE_SAVEALPHA );
    
    if( $this->_rsc->isPaletteColor() )
    {
      // assign transparent color as background with alpha to 127
      return $this->_rsc->toTrueColor();
    }
    
    // probably nothing to do on TrueColor
    return $this->_rsc;
  }

}
