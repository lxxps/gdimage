<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration settings:
 * - "image_jpeg_quality": default quality to apply on JPEG export (from 0 to 100)
 * 
 * @see http://php.net/manual/en/function.imagepng.php
 */
if( ! Config::hasImageJpegQuality() ) {
  Config::setImageJpegQuality( 75 );
}

/**
 * Class for single JPEG resource.
 * JPEG works with \GDImage\Resource_PaletteColor by default.
 * On output and export, JPEG remove alpha information from the resource.
 * Also, on JPEG, transparent color can't be preserved and is rendered without 
 * alpha channel.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage image
 * @author     Loops <pierrotevrard@gmail.com>
 * @abstract
 * @implements \GDImage\Image_Interface
 * @extends    \GDImage\Image_Abstract_Single
 */
class Image_Jpeg extends Image_Abstract_Single
{
  
  /**
   * Return expected MIME Type for this kind of image.
   *
   * @param none
   * @return string 
   * @access public
   * @static
   * @implements \GDImage\Image_Interface
   */
  public static function getMimeType()
  {
    return 'image/jpeg';
  }
  
  // Implements
  
  /**
   * Detect if the class may be used.
   * Return an integer value that will be used as priority for import/creation.
   * If the value returned is greater or equal than self::MUSTBE, not any other 
   * classes will be detected.
   *
   * @param string $mimetype                 Expected MIME Type
   * @param string $filepath                 File path, may be null
   * @param string $binary                   Binary data, may be null
   * @param \GDImage\Image_Interface $image  Image, may be null
   * @return integer 
   * @access public
   * @static
   * @implements \GDImage\Image_Interface
   */
  public static function detect( $mimetype , $filepath = null , $binary = null , Image_Interface $image = null )
  {
    // we do not need to call parent method
    if( $mimetype !== static::getMimeType() ) return static::NOTBE;
    
    // this class MUST BE used (100% sure, there is no animated JPEG for now)
    return static::MUSTBE;
  }
  
  /**
   * Import resource from a file path
   *
   * @param string $filepath File path to import
   * @return boolean True if success
   * @access public
   * @throws \GDImage\Exception_Resource
   * @implements \GDImage\Image_Interface
   */
  public function fromFile( $filepath )
  {
    // release memory, may throw exception
    MemoryHandler::release( $filepath );
    
    if( $rsc = imagecreatefromjpeg( $filepath ) )
    {
      // restore default
      $this->reset();
      
      $this->setResource( Resource_TrueColor::createFromGdResource( $rsc ) );
      return true;
    }
    
    return false;
  }
  
  /**
   * Export resource to a file path.
   * 
   * A custom quality can be set for this export.
   *
   * @param  string $filepath File path to export
   * @param  [integer] $quality JPEG quality
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Image_Interface
   */
  public function toFile( $filepath , $quality = null )
  {
  	$quality = $quality === null ? Config::getImageJpegQuality() : max( 0 , min( $quality | 0 , 100 ) );
    
    // we want to sanitize resource without modify original one
    $clean_rsc = $this->_getSanitizedResource();
    
    return imagejpeg( $clean_rsc->getGdResource() , $filepath , $quality );
  }
  
  /**
   * Export resource to binary data.
   * 
   * A custom quality can be set for this export.
   *
   * @param  [integer] $quality JPEG quality
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Image_Interface
   */
  public function toBinary( $quality = null )
  {
  	$quality = $quality === null ? Config::getImageJpegQuality() : max( 0 , min( $quality | 0 , 100 ) );
    
    // we want to sanitize resource without modify original one
    $clean_rsc = $this->_getSanitizedResource();
    
    // start buffering
    ob_start();
    $flag = imagejpeg( $clean_rsc->getGdResource() , null , $quality );
    $binary = ob_get_contents();
    ob_end_clean();
    
    if( $flag ) return $binary;
    // failure
    return false;
  }
  
  /**
   * Create the resource from width and height.
   *
   * @param integer $w
   * @param integer $h
   * @return boolean Success flag
   * @access public
   * @throws \GDImage\Exception_Resource
   * @implements \GDImage\Image_Interface
   */
  public function fromSize( $w , $h )
  {
    // restore default
    $this->reset();
      
    $this->setResource( Resource_TrueColor::createFromSize( $w , $h ) );
    
    return true;
  }
  
  /**
   * Method to fetch a sanitized Resource from current one.
   * 
   * Nothing to do for JPEG.
   *
   * @param  none
   * @return \GDImage\Resource_Abstract
   * @access public
   */
  public function _getSanitizedResource()
  {
    if( $this->_rsc->isPaletteColor() )
    {
      // assign transparent color as background with alpha to 127
      return $this->_rsc->toTrueColor();
    }
    
    // probably nothing to do on TrueColor
    return $this->_rsc;
  }

}
