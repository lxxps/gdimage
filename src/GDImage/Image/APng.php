<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration settings:
 * - "image_a_png_loops": default number of loops (0 means infinite)
 * 
 * @see https://wiki.mozilla.org/APNG_Specification#.60acTL.60:_The_Animation_Control_Chunk
 */
if( ! Config::hasImageAPngLoops() ) {
  Config::setImageAPngLoops( 0 ); // 0 means infinite
}

/**
 * Class for animated PNG resources.
 * Frames are automatically converted to \GDImage\Image_APng_Frame instances.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage image
 * @author     Loops <pierrotevrard@gmail.com>
 * @abstract
 * @implements \GDImage\Image_Interface
 * @extends    \GDImage\Image_Abstract_Multiple
 */
class Image_APng extends Image_Abstract_Multiple
{
  
  /**
   * Number of plays.
   * 0 means infinite.
   * 
   * @var integer
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#.60acTL.60:_The_Animation_Control_Chunk
   */
  public $_loops = 0;
  
  /**
   * Set number of plays.
   * 0 means infinite.
   * 
   * @param integer $nb
   * @return void
   * @access public
   * @see https://wiki.mozilla.org/APNG_Specification#.60acTL.60:_The_Animation_Control_Chunk
   */
  public function setLoops( $nb )
  {
    // force integer and limit to 4 bytes
    $this->_loops = $nb & 0xFFFFFFFF;
  }
  
  /**
   * Get number of plays.
   * 0 means infinite.
   * 
   * @param none
   * @return integer
   * @access public
   * @see https://wiki.mozilla.org/APNG_Specification#.60acTL.60:_The_Animation_Control_Chunk
   */
  public function getLoops()
  {
    return $this->_loops;
  }
  
  /**
   * Constants for inifinite loops.
   * 
   * @var integer
   * @const
   * @see https://wiki.mozilla.org/APNG_Specification#.60acTL.60:_The_Animation_Control_Chunk
   */
  const LOOPS_INFINITE = 0;
  
  
  // a particularity of APNG, is the default image
  
  /**
   * Default image instance.
   * May be null.
   * 
   * @var \GDImage\Image_Abstract_Single
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#Structure
   */
  public $_df_image;
  
  /**
   * Set default image instance.
   * Note that this method will not clone instance.
   * 
   * @param \GDImage\Image_Abstract_Single $image
   * @return void
   * @access public
   * @see https://wiki.mozilla.org/APNG_Specification#.60acTL.60:_The_Animation_Control_Chunk
   */
  public function setDefaultImage( Image_Abstract_Single $image )
  {
    $this->_df_image = $image;
  }
  
  /**
   * Unset default image instance.
   * 
   * @param none
   * @return void
   * @access public
   * @see https://wiki.mozilla.org/APNG_Specification#.60acTL.60:_The_Animation_Control_Chunk
   */
  public function unsetDefaultImage()
  {
    $this->_df_image = null;
  }
  
  /**
   * Get default image instance.
   * 
   * @param none
   * @return \GDImage\Image_Abstract_Single $image
   * @access public
   * @see https://wiki.mozilla.org/APNG_Specification#.60acTL.60:_The_Animation_Control_Chunk
   */
  public function getDefaultImage()
  {
    return $this->_df_image;
  }
  
  /**
   * Return true if instance has a default image.
   * 
   * @param none
   * @return boolean
   * @access public
   * @see https://wiki.mozilla.org/APNG_Specification#.60acTL.60:_The_Animation_Control_Chunk
   */
  public function hasDefaultImage()
  {
    return (bool)$this->_df_image;
  }
  
  // Implements
  
  /**
   * Method to prepare a frame before to add it to frames array.
   * 
   * To be able to manipulate frames healthy, all frame must extends the
   * same class. Sounds logical, not?
   *
   * @param Image_Interface $image
   * @return Image_Interface
   * @access protected
   * @implements \GDImage\Image_Abstract_Multiple
   */
  public function _prepareFrame( Image_Interface $image )
  {
    // create an empty frame
    $frame = APng_Factory::frame();
    
    // instanceof? ok
    if( $image instanceof $frame ) return $image;
    
    // create frame instance from image
    // keep in mind that this method will not clone resource
    $frame->fromImage( $image );
    
    // finally return the frame instance
    return $frame;
  }
  
  /**
   * Return expected MIME Type for this kind of image.
   *
   * @param none
   * @return string 
   * @access public
   * @static
   * @implements \GDImage\Image_Interface
   */
  public static function getMimeType()
  {
    return 'image/png';
  }
  
  /**
   * Static array for cache lookup on unpack method.
   * 
   * Even with smart detection, we cannot be sure that a PNG is really a APNG
   * without unpacking it. So the unpack process must be done on detection and 
   * creation methods.
   * 
   * This property store valid packs to not call the unpack process twice.
   * 
   * @var array
   * @access protected
   * @static
   */
  static public $_pack_cache = array();
  
  /**
   * Detect if the class may be used.
   * Return an integer value that will be used as priority for import/creation.
   * If the value returned is greater or equal than self::MUSTBE, not any other 
   * classes will be detected.
   *
   * @param string $mimetype                 Expected MIME Type
   * @param string $filepath                 File path, may be null
   * @param string $binary                   Binary data, may be null
   * @param \GDImage\Image_Interface $image  Image, may be null
   * @return integer 
   * @access public
   * @static
   * @implements \GDImage\Image_Interface
   */
  public static function detect( $mimetype , $filepath = null , $binary = null , Image_Interface $image = null )
  {
    if( $mimetype !== static::getMimeType() ) return static::NOTBE;
    
    // this class MAY BE used (not 100% sure)
    $priority = static::MAYBE;
    
    // only if MIME Type match
    if( $image && $image instanceof Image_Abstract_Multiple  )
    {
      // this class MAY BE used (not 100% sure, but more probable)
      $priority += static::MAYBE;
    }
      
    // filepath check
    if( $filepath )
    {
      try
      {
        // we may not want to create a big string just to detect acTL stuff
        
        // parse PNG to check if there is multiple frame
        $pack = APng_Factory::unpack( $filepath );

        // if this key is set, we are in animated PNG case
        if( isset( $pack['acTL'] ) )
        {
          // this class MAY BE used (not 100% sure, but more probable)
          $priority += static::MAYBE;
          
          // store the pack in static array
          
          // create cache key
          $cache_key = md5( $filepath );
          // store the pack
          static::$_pack_cache[$cache_key] = $pack;
        }
      }
      catch( Exception_APng $e )
      {
        // do not throw this exception
      }
    }

    // binary check
    if( $binary )
    {
      try
      {
        // before to unpack, just do a simple strpos on specific string
        
        // we know that APNG must have a acTL Chunk that is 8 Length
        // @see https://wiki.mozilla.org/APNG_Specification#.60acTL.60:_The_Animation_Control_Chunk
        if( strpos( $binary , pack( 'NA4' 
          , 8 // Chunk length, fixed to 8
          , 'acTL' // Chunk name
        ) ) ) // it cannot be first string found
        {
          // of course never trust a string check on binary data, this check 
          // does not mean that the file is APNG, but that it may be APNG
        
          // parse PNG to check if there is multiple frame
          $pack = APng_Factory::unpack( $binary );

          // if this key is set, that means that we have at least two frames
          if( isset( $pack['acTL'] ) )
          {
            // this class MAY BE used (not 100% sure, but more probable)
            $priority += static::MAYBE;
          
            // store the pack in static array

            // create cache key
            $cache_key = md5( $binary );
            // store the pack
            static::$_pack_cache[$cache_key] = $pack;
          }
        }
      }
      catch( Exception_APng $e )
      {
        // do not throw this exception
      }
    }
    
    return $priority;
  }
  
  /**
   * Import resource from a file path
   *
   * @param string $filepath File path to import
   * @return boolean True if success
   * @access public
   * @throws \GDImage\Exception_AGif
   * @implements \GDImage\Image_Interface
   */
  public function fromFile( $filepath )
  {
    // reset image to its default state
    $this->reset();
    
    try
    {
      // do cache lookup
      $cache_key = md5( $filepath );
      if( isset( static::$_pack_cache[$cache_key] ) )
      {
        // fetch
        $pack = static::$_pack_cache[$cache_key];
        // then delete
        unset( static::$_pack_cache[$cache_key] );
      }
      else
      {
        // unpack data
        $pack = APng_Factory::unpack( $filepath );
      }

      // now extract frame
      $frames = APng_Factory::decompose( $pack );

      // keep in mind that frame at index -1 has to be considered as default image
      // so in that case, add it and shift frames array
      if( isset( $frames[-1] ) )
      {
        // we cannot be sure of frames order, so do not use array_shift()
        $this->setDefaultImage( $frames[-1] );
        unset( $frames[-1] );
      }

      // now we can set frames
      $this->setFrames( $frames );

      // look for the acTL Number of plays
      // add a check for convenience
      if( isset($pack['acTL']['Number of plays']) ) $this->setLoops( $pack['acTL']['Number of plays'] );

      // done
      return true;
    }
    catch( Exception_APng $e )
    {
      // do not throw this exception
      return false;
    }
  }
  
  /**
   * Export resource to a file path.
   *
   * @param  string $filepath File path to export
   * @param  [integer] $compression PNG compression level
   * @param  [integer] $filters PNG filters
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Image_Interface
   */
  public function toFile( $filepath , $compression = null , $filters = null )
  {
    $frames = array();
    
    if( $this->_df_image ) $frames[-1] = $this->_df_image;
    
    $frames += $this->_frames;
    
    if( count($frames) > 1 )
    {
      $pack = APng_Factory::compose( $frames , $this->_loops , $compression , $filters );

      $binary = APng_Factory::pack( $pack );
      
      // free memory
      unset( $pack );

      return file_put_contents( $filepath , $binary );
    }
    
    // only one picture?
    // note that default image may not be a PNG one
    return Factory::export( reset( $frames ) , array( 
      'driver' => Factory::DRIVER_FILE ,
      'file' => $filepath ,
      $compression , // unnamed parameter
      $filters , // unnamed parameter
    ) , static::getMimeType() );
  }
  
  /**
   * Import resource from binary data.
   *
   * @param string $binary
   * @return boolean Success flag
   * @access public
   * @throws \GDImage\Exception_AGif
   * @implements \GDImage\Image_Interface
   */
  public function fromBinary( $binary )
  {
    // reset image to its default state
    $this->reset();
    
    try
    {
      // do cache lookup
      $cache_key = md5( $binary );
      if( isset( static::$_pack_cache[$cache_key] ) )
      {
        // fetch
        $pack = static::$_pack_cache[$cache_key];
        // then delete
        unset( static::$_pack_cache[$cache_key] );
      }
      else
      {
        // unpack data
        $pack = APng_Factory::unpack( $binary );
      }
    
      // now extract frame
      $frames = APng_Factory::decompose( $pack );

      // keep in mind that frame at index -1 has to be considered as default image
      // so in that case, add it and shift frames array
      if( isset( $frames[-1] ) )
      {
        // we cannot be sure of frames order, so do not use array_shift()
        $this->setDefaultImage( $frames[-1] );
        unset( $frames[-1] );
      }

      // now we can set frames
      $this->setFrames( $frames );

      // look for the acTL Number of plays
      // add a check for convenience
      if( isset($pack['acTL']['Number of plays']) ) $this->setLoops( $pack['acTL']['Number of plays'] );

      // done
      return true;
    }
    catch( Exception_APng $e )
    {
      // do not throw this exception
      return false;
    }
  }
  
  /**
   * Export resource to binary data.
   *
   * @param  [integer] $compression PNG compression level
   * @param  [integer] $filters PNG filters
   * @return string
   * @access public
   * @implements \GDImage\Image_Interface
   */
  public function toBinary( $compression = null , $filters = null )
  {
    $frames = array();
    
    if( $this->_df_image ) $frames[-1] = $this->_df_image;
    
    $frames += $this->_frames;
    
    if( count($frames) > 1 )
    {
      $pack = APng_Factory::compose( $frames , $this->_loops , $compression , $filters );

      return APng_Factory::pack( $pack );
    }
    
    // only one picture?
    // note that default image may not be a PNG one
    return Factory::export( reset( $frames ) , array( 
      'driver' => Factory::DRIVER_BINARY ,
      $compression , // unnamed parameter
      $filters , // unnamed parameter
    ) , static::getMimeType() );
  }
  
  /**
   * Create the resource to another \GDImage\Image_Interface.
   * Note that there is no need to clone image, let the developer decides.
   *
   * @param Image_Interface $image
   * @return boolean Success flag
   * @access public
   * @throws \GDImage\Exception_Resource
   * @implements \GDImage\Image_Interface
   * @extends \GDImage\Image_Abstract_Multiple
   */
  public function toImage( Image_Interface $image )
  {
    // multiple to single
    if( $image instanceof Image_Abstract_Single )
    {
      if( $this->hasDefaultImage() || $this->hasFrame( 0 ) )
      {
        // for APng to single, we want to use default image in priority
        $frame = $this->getDefaultImage();
        if( ! $frame ) $frame = $this->getFrame( 0 );
        
        $image->setResource( $frame->getResource() );
//        $image->setResource( clone $frame->getResource() );

        return true;
      }
    }
      
    // multiple to multiple
    if( $image instanceof Image_Abstract_Multiple )
    {
      if( $this->getFrames() )
      {
        // call setFrames() to reset and prepare frame
        // passing data from one frame type to another belong to the frame class
        $image->setFrames( $this->getFrames() );
//        $i = $this->countFrames();
//        while( $i-- )
//        {
//          $image->addFrame( clone $this->getFrame( $i ) , 0 );
//        }

        return true;
      }
      
      // additionnal check for another APng
      if( $image instanceof self )
      {
        if( $this->hasDefaultImage() )
        {
          $image->setDefaultImage( $this->getDefaultImage() );
        }
        else
        {
          $image->unsetDefaultImage(); // useless but not wrong
        }
      }
    }
    
    return false;
  }
  
  /**
   * Create the resource from width and height.
   *
   * @param integer $w
   * @param integer $h
   * @return boolean Success flag
   * @access public
   * @throws \GDImage\Exception_Resource
   * @implements \GDImage\Image_Interface
   */
  public function fromSize( $w , $h )
  {
    // restore default
    $this->reset();
    
    // create one image with this size
    $frame = APng_Factory::frame();
    $frame->fromSize( $w , $h );
    $this->setFrames( array( $frame ) );
    
    return true;
  }
  
  /**
   * Get width of the image.
   * 
   * We want to get width of default image in priority.
   *
   * @param none
   * @return integer Width
   * @access public
   * @implements \GDImage\Image_Interface
   */ 
  public function getWidth()
  {
    if( $this->_df_image )
    {
  	  return $this->_df_image->getWidth();
    }
    
    if( isset($this->_frames[0]) )
    {
  	  return $this->_frames[0]->getWidth();
    }
    
    return null;
  }
  
  /**
   * Get height of the image.
   * 
   * We want to get height of default image in priority.
   *
   * @param none
   * @return integer Height
   * @access public
   * @implements \GDImage\Image_Interface
   */ 
  public function getHeight()
  {
    if( $this->_df_image )
    {
  	  return $this->_df_image->getHeight();
    }
    
    if( isset($this->_frames[0]) )
    {
  	  return $this->_frames[0]->getHeight();
    }
    
    return null;
  }
  
  /**
   * Apply \GDImage\Transform_Interface to the image
   * 
   * On PHP 5.6 we will be able to use ... declaration.
   * @see http://php.net/manual/en/functions.arguments.php#functions.variable-arg-list
   *
   * @param \GDImage\Transform_Interface $transform
   * @param [\GDImage\Transform_Interface ...] Other transformations to apply
   * @return boolean True on success
   * @access public
   * @implements \GDImage\Image_Interface
   */ 
  public function apply( Transform_Interface $transform /** /, Transform_Interface ...$transforms /**/ )
  {
    $flag = true;
    // apply transform on default image
    if( $this->_df_image )
    {
      $flag = call_user_func_array( array( $this->_df_image , 'apply' ) , func_get_args() );
    }
    
    return parent::apply( $transform ) && $flag;
  }
  
  /**
   * Reset the image to its default state
   *
   * @param none
   * @return void
   * @access public
   * @implements \GDImage\Image_Interface
   * @extends \GDImage\Image_Abstract_Multiple
   */
  public function reset()
  {
    // reset loops
    $this->setLoops( Config::getImageAPngLoops() );
    // unset default image
    $this->unsetDefaultImage();
    // unset frames
    $this->unsetFrames();
  }

  // Special clone
  
  /**
   * Clone frames to not modify original frames.
   * Also clone default image instance, if any.
   * 
   * @param none
   * @return void
   * @access public
   */
  public function __clone()
  {
    parent::__clone();
    
    // default image?
    if( $this->_df_image ) $this->_df_image = clone $this->_df_image;
  }

}
