<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration settings:
 * - "image_a_png_delay_numerator": default Delay Numerator for APNG frame
 * - "image_a_png_delay_denominator": default Delay Denominator for APNG frame
 * - "image_a_png_dispose_option": default Dispose Option for APNG frame
 * - "image_a_png_blend_option": default Blend Option for APNG frame
 * 
 * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
 */
if( ! Config::hasImageAPngDelayNumerator() ) {
  Config::setImageAPngDelayNumerator( 0 ); // 0 means "as fast as possible"
}
if( ! Config::hasImageAPngDelayDenominator() ) {
  Config::setImageAPngDelayDenominator( 100 ); // 1/100th a second
}
if( ! Config::hasImageAPngDisposeOption() ) {
  Config::setImageAPngDisposeOption( 1 ); // 1 means "restore to background"
}
if( ! Config::hasImageAPngBlendOption() ) {
  Config::setImageAPngBlendOption( 0 ); // 0 means "no blending"
}

/**
 * Class to represent a PNG frame for animated PNG.
 * 
 * This class extends Image_Png with a delay, a Dispose option and a Blend option.
 * 
 * An important thing about APNG delay time is that it can be 0, and the PNG
 * still animated: the browser determione itself the minimal delay to use.
 * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
 * 
 * 
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage apng
 * @author     Loops <pierrotevrard@gmail.com>
 * @abstract
 * @implements \GDImage\Image_Interface
 * @extends    \GDImage\Image_Png
 */
class Image_APng_Frame extends Image_Png
{
  
  /**
   * Delay numerator, depends of the delay denominator
   * 0 means as fast as possible
   * 
   * @var integer
   * @access protected
   */
  public $_delay;
  
  /**
   * Set delay numerator
   * 
   * @param integer $delay
   * @return void
   * @access public
   */
  public function setDelayNumerator( $delay )
  {
    // force integer and limit to 2 bytes
    $this->_delay = $delay & 0xFFFF;
  }
  
  /**
   * Alias for setDelayNumerator()
   * 
   * @deprecated
   * @param integer $delay
   * @return void
   * @access public
   */
  public function setDelay( $delay )
  {
    $this->setDelayNumerator( $delay );
  }
  
  /**
   * Get delay numerator
   * 
   * @param none
   * @return integer
   * @access public
   */
  public function getDelayNumerator()
  {
    return $this->_delay;
  }
  
  /**
   * Alias for getDelayNumerator()
   * 
   * @deprecated
   * @param none
   * @return integer
   * @access public
   */
  public function getDelay()
  {
    return $this->getDelayNumerator();
  }
  
  /**
   * Delay denominator
   * 0 means is equivalent to 1/100th s
   * 
   * @var integer
   * @access protected
   */
  public $_delay_denominator;
  
  /**
   * Set delay denominator
   * 0 means is automatically converted to 1/100th s
   * 
   * @param integer $delay
   * @return void
   * @access public
   */
  public function setDelayDenominator( $delay )
  {
    // force integer and limit to 2 bytes
    $this->_delay_denominator = $delay & 0xFFFF;
    
    if( ! $this->_delay_denominator ) $this->_delay_denominator = 100;
  }
  
  /**
   * Get delay denominator
   * 
   * @param none
   * @return integer
   * @access public
   */
  public function getDelayDenominator()
  {
    return $this->_delay_denominator;
  }
  
  /**
   * Dispose option
   * For convenience, we choose the same name than Image_AGif_Frame.
   * 
   * @var integer
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  public $_disposal;
  
  /**
   * Constants for Dispose option "no"
   * 
   * From specification:
   * 0: no disposal is done on this frame before rendering the next; the 
   *    contents of the output buffer are left as is.
   * 
   * @var integer
   * @const
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  // to look like Image_AGif constants
  const DISPOSAL_NO = 0;
  // to look like APNG specification
  const DISPOSE_NO = 0;
  
  /**
   * Constants for Dispose option "background"
   * 
   * From specification:
   * 1: the frame's region of the output buffer is to be cleared to fully 
   *    transparent black before rendering the next frame. 
   * 
   * @var integer
   * @const
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  // to look like Image_AGif constants
  const DISPOSAL_BACKGROUND = 1;
  // to look like APNG specification
  const DISPOSE_BACKGROUND = 1;
  
  /**
   * Constants for Dispose option "previous"
   * 
   * From specification:
   * 2: the frame's region of the output buffer is to be reverted to the 
   *    previous contents before rendering the next frame.
   * 
   * @var integer
   * @const
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  // to look like Image_AGif constants
  const DISPOSAL_PREVIOUS = 2;
  // to look like APNG specification
  const DISPOSE_PREVIOUS = 2;
  
  /**
   * Set Dispose option
   * 
   * @param integer $disposal
   * @return void
   * @access public
   */
  public function setDisposeOption( $disposal )
  {
    // force integer and limit to 1 byte
    $this->_disposal = $disposal & 0xFF;
  }
  
  /**
   * Alias for setDisposeOption().
   * 
   * @deprecated
   * @param integer $disposal
   * @return void
   * @access public
   */
  public function setDisposal( $disposal )
  {
    $this->setDisposeOption( $disposal );
  }
  
  /**
   * Get Dispose option
   * 
   * @param none
   * @return integer
   * @access public
   */
  public function getDisposeOption()
  {
    return $this->_disposal;
  }
  
  /**
   * Alias for getDisposeOption().
   * 
   * @deprecated
   * @param none
   * @return integer
   * @access public
   */
  public function getDisposal()
  {
    return $this->getDisposeOption();
  }
  
  /**
   * Blend option
   * 
   * @var integer
   * @access protected
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  public $_blending;
  
  /**
   * Constants for Blend option "no"
   * 
   * From specification:
   * 0: all color components of the frame, including alpha, overwrite the 
   *    current contents of the frame's output buffer region.
   * 
   * @var integer
   * @const
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  const BLEND_NO = 0;
  
  /**
   * Constants for Blend option "over"
   * 
   * From specification:
   * 1: the frame should be composited onto the output buffer based on its alpha, 
   *    using a simple OVER operation as described in the "Alpha Channel 
   *    Processing" section of the PNG specification. 
   * 
   * @var integer
   * @const
   * @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
   */
  const BLEND_OVER = 1;
  
  /**
   * Set Blend option
   * 
   * @param integer $blend
   * @return void
   * @access public
   */
  public function setBlendOption( $blend )
  {
    // force integer and limit to 1 byte
    $this->_blending = $blend & 0xFF;
  }
  
  /**
   * Get Blend option
   * 
   * @param none
   * @return integer
   * @access public
   */
  public function getBlendOption()
  {
    return $this->_blending;
  }
  
  // Extends
  
  /**
   * Create the resource to another \GDImage\Image_Interface.
   * Note that there is no need to clone image, let the developer decides.
   *
   * @param Image_Interface $image
   * @return boolean Success flag
   * @access public
   * @throws \GDImage\Exception_Resource
   * @implements \GDImage\Image_Interface
   * @extends \GDImage\Image_Abstract_Single
   */
  public function toImage( Image_Interface $image )
  {
    $flag = parent::toImage( $image );
    
    // AGif frame to AGif frame
    if( $image instanceof self )
    {
      $image->setDelayNumerator( $this->getDelayNumerator() );
      $image->setDelayDenominator( $this->getDelayDenominator() );
      $image->setDisposeOption( $this->getDisposeOption() );
      $image->setBlendOption( $this->getBlendOption() );
    }
    
    // APng frame to AGif frame
    if( $image instanceof Image_AGif_Frame )
    {
      // update delay
      // AGif only support delay in 1/100th of a second
      // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt
      $image->setDelayTime( round( ( $this->getDelayNumerator() / $this->getDelayDenominator() ) * 100 ) );
      
      // APng Dispose Option is relatively similar to AGif Disposal Method
      // @see https://wiki.mozilla.org/APNG_Specification#.60fcTL.60:_The_Frame_Control_Chunk
      // @see http://www.w3.org/Graphics/GIF/spec-gif89a.txt 23
      switch( $this->getDisposeOption() )
      {
        case self::DISPOSE_NO:
          $image->setDisposalMethod( Image_AGif_Frame::DISPOSAL_NO );
        break;
        case self::DISPOSE_BACKGROUND: 
          $image->setDisposalMethod( Image_AGif_Frame::DISPOSAL_BACKGROUND );
        break;
        case self::DISPOSE_PREVIOUS: 
          $image->setDisposalMethod( Image_AGif_Frame::DISPOSAL_PREVIOUS );
        break;
      }
    }
  }
  
  /**
   * Reset the image to its default state
   * 
   * @param none
   * @return void
   * @access public
   * @implements \GDImage\Image_Interface
   * @extends \GDImage\Image_Abstract_Single
   */
  public function reset()
  {
    // assing defaults
    $this->setDelayNumerator( Config::getImageAPngDelayNumerator() );
    $this->setDelayDenominator( Config::getImageAPngDelayDenominator() );
    $this->setDisposeOption( Config::getImageAPngDisposeOption() );
    $this->setBlendOption( Config::getImageAPngBlendOption() );
    
    // unset resource
    $this->unsetResource();
  }
}
