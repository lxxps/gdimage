<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Interface for \GDImage\Image_* instance
 *
 * @package    GDImage
 * @subpackage image
 * @author     Loops <pierrotevrard@gmail.com>
 * @interface
 */
interface Image_Interface
{
  /**
   * Constant to represent that the class must not be used.
   * 
   * @var integer
   * @const
   */
  const NOTBE = 0;
  
  /**
   * Constant to represent that the class may be used.
   * 
   * @var integer
   * @const
   */
  const MAYBE = 1;
  
  /**
   * Constant to represent that the class must be used.
   * Note that ten self::MAYBE are equivalent to one self::MUSTBE.
   * 
   * @var integer
   * @const
   */
  const MUSTBE = 10;
  
  /**
   * Return expected MIME Type for this kind of image.
   *
   * @param none
   * @return string 
   * @access public
   * @static
   */
  public static function getMimeType();
  
  /**
   * Detect if the class may be used.
   * Return an integer value that will be used as priority for import/creation.
   * If the value returned is greater or equal than self::MUSTBE, not any other 
   * classes will be detected.
   *
   * @param string $mimetype                 Expected MIME Type
   * @param string $filepath                 File path, may be null
   * @param string $binary                   Binary data, may be null
   * @param \GDImage\Image_Interface $image  Image, may be null
   * @return integer 
   * @access public
   * @static
   */
  public static function detect( $mimetype , $filepath = null , $binary = null , Image_Interface $image = null );
  
  /**
   * Import resource from a file path
   *
   * @param string $filepath File path to import
   * @return boolean True if success
   * @access public
   * @throws \GDImage\Exception_Resource
   */
  public function fromFile( $filepath );
  
  /**
   * Export resource to a file path.
   * May have extra parameters to pass to the export function.
   *
   * @param string $filepath File path to export
   * @return boolean Success flag
   * @access public
   */
  public function toFile( $filepath );
  
  /**
   * Import resource from binary data.
   *
   * @param string $binary
   * @return boolean Success flag
   * @access public
   */
  public function fromBinary( $binary );
  
  /**
   * Export resource to binary data.
   * May have extra parameters to pass to the export function.
   *
   * @param none
   * @return string
   * @access public
   */
  public function toBinary();
  
  /**
   * Create the resource from another \GDImage\Image_Interface.
   * Note that there is no need to clone image, let the developer decides.
   *
   * @param Image_Interface $image
   * @return boolean Success flag
   * @access public
   * @throws \GDImage\Exception_Resource
   */
  public function fromImage( Image_Interface $image );
  
  /**
   * Create the resource to another \GDImage\Image_Interface.
   * Note that there is no need to clone image, let the developer decides.
   *
   * @param Image_Interface $image
   * @return boolean Success flag
   * @access public
   * @throws \GDImage\Exception_Resource
   */
  public function toImage( Image_Interface $image );
  
  /**
   * Create the resource from width and height.
   *
   * @param integer $w
   * @param integer $h
   * @return boolean Success flag
   * @access public
   * @throws \GDImage\Exception_Resource
   */
  public function fromSize( $w , $h );
  
  /**
   * Get width of the image.
   *
   * @param none
   * @return integer
   * @access public
   */
  public function getWidth();
  
  /**
   * Get height of the image.
   *
   * @param none
   * @return integer
   * @access public
   */
  public function getHeight();
  
  /**
   * Apply \GDImage\Transform_Interface to the image
   *
   * @param \GDImage\Transform_Interface $transform
   * @param [\GDImage\Transform_Interface ...] Other transformations to apply
   * @return boolean True on success
   * @access public
   */
  public function apply( Transform_Interface $transform /** /, Transform_Interface ...$transforms /**/ );
  
  /**
   * Reset the image to its default state
   *
   * @param none
   * @return void
   * @access public
   */
  public function reset();

}
