<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration settings:
 * - "image_png_compression": default compression level to apply on PNG export (from 0 to 9)
 * - "image_png_filters": default filters to apply on PNG export ('auto' means 
 *   automatically determined from the resource color type)
 * - "image_png_filters_palette_color": automatic filters to apply on PNG export with
 *   palette color resource and automatic detection (PNG_FILTER_NONE)
 * - "image_png_filters_true_color": automatic filters to apply on PNG export with
 *   true color resource and automatic detection (PNG_ALL_FILTERS)
 * 
 * @see http://php.net/manual/en/function.imagepng.php
 */
if( ! Config::hasImagePngCompression() ) {
  Config::setImagePngCompression( 4 );
}
if( ! Config::hasImagePngFilters() ) {
  Config::setImagePngFilters( 'auto' );
}
if( ! Config::hasImagePngFiltersPaletteColor() ) {
  // @see http://www.w3.org/TR/PNG/#12Filter-selection
  Config::setImagePngFiltersPaletteColor( \PNG_FILTER_NONE );
}
if( ! Config::hasImagePngFiltersTrueColor() ) {
  // @see http://www.w3.org/TR/PNG/#12Filter-selection
  // 
  // PNG_FILTER_ALL enable imagepng() to encode each scanlines 
  // separately but it longer
  // @see http://www.w3.org/TR/PNG/#7Filtering
  // 
  // \PNG_FILTER_AVG provide good results and is quicker than \PNG_FILTER_PAETH
  Config::setImagePngFiltersTrueColor( /**/ \PNG_ALL_FILTERS /**/ /** / \PNG_FILTER_PAETH /** / \PNG_FILTER_AVG /**/ );
}

/**
 * Class for single PNG24 or PNG8 resource.
 * PNG works with \GDImage\Resource_TrueColor by default.
 * This class can work with PNG24 and PNG8, it is adaptative on these 
 * two types of image.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage image
 * @author     Loops <pierrotevrard@gmail.com>
 * @abstract
 * @implements \GDImage\Image_Interface
 * @extends    \GDImage\Image_Abstract_Single
 */
class Image_Png extends Image_Abstract_Single
{
  
  /**
   * Constants to represent automatic filters.
   * 
   * @var boolean
   * @const
   */
  const AUTOMATIC_FILTERS = 'auto';
  
  /**
   * Return expected MIME Type for this kind of image.
   *
   * @param none
   * @return string 
   * @access public
   * @static
   * @implements \GDImage\Image_Interface
   */
  public static function getMimeType()
  {
    return 'image/png';
  }
  
  // Implements
  
  /**
   * Import resource from a file path
   *
   * @param string $filepath File path to import
   * @return boolean True if success
   * @access public
   * @throws \GDImage\Exception_Resource
   * @implements \GDImage\Image_Interface
   */
  public function fromFile( $filepath )
  {
    // release memory, may throw exception
    MemoryHandler::release( $filepath );
    
    if( $rsc = imagecreatefrompng( $filepath ) )
    {
      // restore default
      $this->reset();
      
      $this->setResource( Resource_Abstract::createFromGdResource( $rsc ) );
      return true;
    }
    
    return false;
  }
  
  /**
   * Export resource to a file path.
   * 
   * Custom compression level can be set for this export.
   * Custom PNG filters can be set for this export.
   *
   * @param  string $filepath File path to export
   * @param  [integer] $compression PNG compression level
   * @param  [integer] $filters PNG filters
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Image_Interface
   */
  public function toFile( $filepath , $compression = null , $filters = null )
  {
  	$compression = $compression === null ? Config::getImagePngCompression() : max( 0 , min( $compression | 0 , 9 ) );
  	$filters = $filters === null ? Config::getImagePngFilters() : $filters | 0;
    
    // we want to sanitize resource without modify original one
    $clean_rsc = $this->_getSanitizedResource();
    
    // automatic filters
    if( $filters === self::AUTOMATIC_FILTERS ) $filters = $this->_getAutomaticFilters( $clean_rsc );
    
    return imagepng( $clean_rsc->getGdResource() , $filepath , $compression , $filters );
  }
  
  /**
   * Export resource to binary data.
   * 
   * Custom compression level can be set for this export.
   * Custom PNG filters can be set for this export.
   *
   * @param  [integer] $compression PNG compression level
   * @param  [integer] $filters PNG filters
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Image_Interface
   */
  public function toBinary( $compression = null , $filters = null )
  {
  	$compression = $compression === null ? Config::getImagePngCompression() : max( 0 , min( $compression | 0 , 9 ) );
  	$filters = $filters === null ? Config::getImagePngFilters() : $filters | 0;
    
    // we want to sanitize resource without modify original one
    $clean_rsc = $this->_getSanitizedResource();
    
    // automatic filters
    if( $filters === self::AUTOMATIC_FILTERS ) $filters = $this->_getAutomaticFilters( $clean_rsc );
    
    // start buffering
    ob_start();
    $flag = imagepng( $clean_rsc->getGdResource() , null , $compression , $filters );
    $binary = ob_get_contents();
    ob_end_clean();
    
    if( $flag ) return $binary;
    
    // failure
    return false;
  }
  
  /**
   * Create the resource from width and height.
   *
   * @param integer $w
   * @param integer $h
   * @return boolean Success flag
   * @access public
   * @throws \GDImage\Exception_Resource
   * @implements \GDImage\Image_Interface
   */
  public function fromSize( $w , $h )
  {
    // restore default
    $this->reset();
      
    $this->setResource( Resource_TrueColor::createFromSize( $w , $h ) );
    
    return true;
  }
  
  /**
   * Method to fetch a sanitized Resource from current one.
   * 
   * It adapts stuff on expected result.
   *
   * @param  none
   * @return \GDImage\Resource_Abstract
   * @access public
   * @see \GDImage\Image_Png8::_getSanitizedResource()
   * @see \GDImage\Image_Png24::_getSanitizedResource()
   */
  public function _getSanitizedResource()
  {
    // make sure alpha is saved
    $this->_rsc->setMode( Resource_PaletteColor::MODE_SAVEALPHA );
    
    if( $this->_rsc->isPaletteColor() )
    {
      // on palette color, we want to make sure transparent color has 
      // an alpha channel of 127, even if it is not necessary for PNG8
      
      // before to manipulate colors, make sure resource is awaken
      $this->_rsc->awakeGdResource();

      // there is a transparent color
      if( $color = $this->_rsc->getTransparent() )
      {
        // and the transparent color needs to be sanitize
        if( $color->getAlpha() !== 127 )
        {
          // change alpha
          $color->setAlpha( 127 );
          // clone original resource
          $rsc = clone $this->_rsc;
          // assign new transparent color
          $rsc->setTransparent( $color );
          // return the clone
          return $rsc;
        }
      }
    }
    
    // probably nothing to do on TrueColor
    return $this->_rsc;
  }
  
  /**
   * Method to get best PNG filters for export.
   * Useless in this context, but not on Image_Png24 and Image_Png8
   *
   * @param  Resource_Abstract $clean_rsc
   * @return integer A PNG filter constant
   * @access public
   */
  public function _getAutomaticFilters( Resource_Abstract $clean_rsc )
  {
    // we have detected two cases
    
    // one of resources is a palette color
    if( $clean_rsc->isPaletteColor() || $this->_rsc->isPaletteColor() )
    {
      return Config::getImagePngFiltersPaletteColor();
    }
    
    // true color to true color case
    return Config::getImagePngFiltersTrueColor();
  }

}
