<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration settings:
 * - "image_gif_true_color_palette": number of colors allowed when GIF
 *   is exported from a \GDImage\Resource_TrueColor
 * - "image_gif_true_color_dither": dither flag to use when GIF
 *   is exported from a \GDImage\Resource_TrueColor
 * 
 * @see http://php.net/manual/en/function.imagetruecolortopalette.php
 */
if( ! Config::hasImageGifTrueColorPalette() ) {
  Config::setImageGifTrueColorPalette( 256 );
}
if( ! Config::hasImageGifTrueColorDither() ) {
  Config::setImageGifTrueColorDither( false );
}

/**
 * Class for single GIF resource.
 * GIF works with \GDImage\Resource_PaletteColor by default.
 * On output and export, GIF remove alpha information from the resource.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage image
 * @author     Loops <pierrotevrard@gmail.com>
 * @abstract
 * @implements \GDImage\Image_Interface
 * @extends    \GDImage\Image_Abstract_Single
 */
class Image_Gif extends Image_Abstract_Single
{
  
  /**
   * Return expected MIME Type for this kind of image.
   *
   * @param none
   * @return string 
   * @access public
   * @static
   * @implements \GDImage\Image_Interface
   */
  public static function getMimeType()
  {
    return 'image/gif';
  }
  
  // Implements
  
  /**
   * Import resource from a file path
   *
   * @param string $filepath File path to import
   * @return boolean True if success
   * @access public
   * @throws \GDImage\Exception_Resource
   * @implements \GDImage\Image_Interface
   */
  public function fromFile( $filepath )
  {
    // release memory, may throw exception
    MemoryHandler::release( $filepath );
    
    if( $rsc = imagecreatefromgif( $filepath ) )
    {
      // restore default
      $this->reset();
      
      $this->setResource( Resource_PaletteColor::createFromGdResource( $rsc ) );
      return true;
    }
    
    return false;
  }
  
  /**
   * Export resource to a file path.
   * 
   * Extra parameters are only used when GIF has to be created from a 
   * \GDImage\Resource_TrueColor.
   *
   * @param string $filepath File path to export
   * @param  [integer] $colors Number of colors
   * @param  [boolean] $dither Use dither
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Image_Interface
   */
  public function toFile( $filepath , $colors = null , $dither = null )
  {
    // we want to sanitize resource without modify original one
    $clean_rsc = $this->_getSanitizedResource( $colors  , $dither );
    
    return imagegif( $clean_rsc->getGdResource() , $filepath );
  }
  
  /**
   * Export resource to binary data.
   * 
   * Extra parameters are only used when GIF has to be created from a 
   * \GDImage\Resource_TrueColor.
   *
   * @param none
   * @param  [integer] $colors Number of colors
   * @param  [boolean] $dither Use dither
   * @return string
   * @access public
   * @implements \GDImage\Image_Interface
   */
  public function toBinary( $colors = null , $dither = null )
  {
    // we want to sanitize resource without modify original one
    $clean_rsc = $this->_getSanitizedResource( $colors  , $dither );
    
    // start buffering
    ob_start();
    $flag = imagegif( $clean_rsc->getGdResource() );
    $binary = ob_get_contents();
    ob_end_clean();
    
    if( $flag ) return $binary;
    // failure
    return false;
  }
  
  /**
   * Create the resource from width and height.
   *
   * @param integer $w
   * @param integer $h
   * @return boolean Success flag
   * @access public
   * @throws \GDImage\Exception_Resource
   * @implements \GDImage\Image_Interface
   */
  public function fromSize( $w , $h )
  {
    // restore default
    $this->reset();
    
    $this->setResource( Resource_PaletteColor::createFromSize( $w , $h ) );
    
    return true;
  }
  
  /**
   * Method to fetch a sanitized Resource from current one.
   *
   * Parameters are only used when GIF has to be created from a 
   * \GDImage\Resource_TrueColor.
   * 
   * @param  [integer] $colors Number of colors
   * @param  [boolean] $dither Use dither
   * @return \GDImage\Resource_PaletteColor
   * @access public
   */
  public function _getSanitizedResource( $colors = null , $dither = null )
  {
    if( $this->_rsc->isTrueColor() )
    {
      $colors = $colors === null ? Config::getImageGifTrueColorPalette() : max( 0 , $colors | 0 );
      $dither = $dither === null ? Config::getImageGifTrueColorDither() : (bool)$dither;
      
      return $this->_rsc->toPaletteColor( $colors , $dither );
    }
    
    return $this->_rsc;
  }

}
