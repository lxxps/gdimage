<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Abstract class for \GDImage\Image_* instance with a single resource.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage image
 * @author     Loops <pierrotevrard@gmail.com>
 * @abstract
 * @implements \GDImage\Image_Interface
 */
abstract class Image_Abstract_Single implements Image_Interface
{
  
  /**
   * Constructor
   * 
   * Reset the image to its default state.
   * 
   * @param none
   * @return void
   * @access public
   */
  public function __construct()
  {
    // restore default
    $this->reset();
  }
  
  /**
   * Destructor
   * 
   * Make sure resource is not observed anymore
   * 
   * @param none
   * @return void
   * @access public
   */
  public function __destruct()
  {
    // unset resource
    $this->unsetResource();
  }
  
  // Resource methods
  
  /**
   * Current image resource.
   *
   * @var \GDImage\Resource_Abstract
   * @access protected
   */
  public $_rsc;
  
  /**
   * Memory handler identifier for the resource.
   * 
   * Identifier cannot be 0.
   *
   * @var integer
   * @access protected
   */
  public $_mem_id;
  
  /**
   * Assign a new image resource.
   * Check if a resource has already be assign to destroy it.
   * This method just assign resource link identifier, so the
   * submitted variable and the class proprety work together
   * on same resource.
   * Keep in mind that we cannot determine mime type from a GD resource.
   *
   * @param \GDImage\Resource_Abstract $rsc
   * @return void
   * @access public
   */ 
  public function setResource( Resource_Abstract $rsc )
  {
    // unregister previous memory handler
    if( $this->_mem_id ) MemoryHandler::unregister( $this->_mem_id );
    
    // assign resource
  	$this->_rsc = $rsc;
    
    // register memory handler
    $this->_mem_id = MemoryHandler::register( $this->_rsc );
  }
  
  /**
   * Unset current image resource.
   * 
   * Note that this method will not destroy the GD resource unless there 
   * is no more reference to the \GDImage\Resource_Abstract instance.
   *
   * @param none
   * @return boolean Success flag
   * @access public
   */ 
  public function unsetResource()
  {
    // unregister memory handler to clear reference to this resource
    if( $this->_mem_id )
    {
      MemoryHandler::unregister( $this->_mem_id );
  	  $this->_mem_id = null;
    }
    
  	$this->_rsc = null;
    
    return true;
  }
  
  /**
   * Retrieve current image resource.
   *
   * @param none
   * @return \GDImage\Resource_Abstract Image resource
   * @access public
   */ 
  public function getResource()
  {
    if( $this->_rsc )
    {
      // return it
  	  return $this->_rsc;
    }
    
    // throw an exception? trigger an error?
    return null;
  }
  
  /**
   * Returns true is the image has a resource
   *
   * @param none
   * @return boolean
   * @access public
   */ 
  public function hasResource()
  {
  	return (bool)$this->_rsc;
  }
  
  // Implements
  
  /**
   * Detect if the class may be used.
   * Return an integer value that will be used as priority for import/creation.
   * If the value returned is greater or equal than self::MUSTBE, not any other 
   * classes will be detected.
   *
   * @param string $mimetype                 Expected MIME Type
   * @param string $filepath                 File path, may be null
   * @param string $binary                   Binary data, may be null
   * @param \GDImage\Image_Interface $image  Image, may be null
   * @return integer 
   * @access public
   * @static
   * @implements \GDImage\Image_Interface
   */
  public static function detect( $mimetype , $filepath = null , $binary = null , Image_Interface $image = null )
  {
    if( $mimetype !== static::getMimeType() ) return static::NOTBE;
    
    // this class MAY BE used (not 100% sure)
    $priority = static::MAYBE;
    
    // only if MIME Type match
    if( $image && $image instanceof Image_Abstract_Single )
    {
      // this class MAY BE used (not 100% sure, but more probable)
      $priority += static::MAYBE;
    }
    
    return $priority;
  }
  
  /**
   * Import resource from binary data.
   *
   * @param string $binary
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Image_Interface
   */
  public function fromBinary( $binary )
  {
    // release memory, may throw exception
    MemoryHandler::release( $binary );
    
    if( $rsc = imagecreatefromstring( $binary ) )
    {
      // restore default
      $this->reset();
      
      $this->setResource( Resource_Abstract::createFromGdResource( $rsc ) );
      return true;
    }
    
    return false;
  }
  
  /**
   * Create the resource from another \GDImage\Image_Interface.
   * Note that there is no need to clone image, let the developer decides.
   *
   * @param Image_Interface $image
   * @return boolean Success flag
   * @access public
   * @throws \GDImage\Exception_Resource
   * @implements \GDImage\Image_Interface
   */
  public function fromImage( Image_Interface $image )
  {
    // restore default
    $this->reset();
    
    return $image->toImage( $this );
  }
  
  /**
   * Create the resource to another \GDImage\Image_Interface.
   * Note that there is no need to clone image, let the developer decides.
   *
   * @param Image_Interface $image
   * @return boolean Success flag
   * @access public
   * @implements \GDImage\Image_Interface
   */
  public function toImage( Image_Interface $image )
  {
    if( $this->hasResource() )
    {
      // single to single
      if( $image instanceof Image_Abstract_Single )
      {
        $image->setResource( $this->getResource() );
//        $image->setResource( clone $this->getResource() );

        return true;
      }
      
      // single to multiple
      if( $image instanceof Image_Abstract_Multiple )
      {
        // call setFrames() to reset and prepare frame
        // passing data from one the image to the frame belong to the frame class
        $image->setFrames( array( $this ) );
//        $image->setFrames( array( clone $this ) );

        return true;
      }
    }
    
    return false;
  }
  
  /**
   * Get width of the image.
   *
   * @param none
   * @return integer Width
   * @access public
   * @implements \GDImage\Image_Interface
   */ 
  public function getWidth()
  {
    if( $this->_rsc )
    {
  	  return $this->_rsc->getWidth();
    }
    
    return null;
  }
  
  /**
   * Get height of the image.
   *
   * @param none
   * @return integer Height
   * @access public
   * @implements \GDImage\Image_Interface
   */ 
  public function getHeight()
  {
    if( $this->_rsc )
    {
  	  return $this->_rsc->getHeight();
    }
    
    return null;
  }
  
  /**
   * Apply \GDImage\Transform_Interface to the image
   * 
   * On PHP 5.6 we will be able to use ... declaration.
   * @see http://php.net/manual/en/functions.arguments.php#functions.variable-arg-list
   *
   * @param \GDImage\Transform_Interface $transform
   * @param [\GDImage\Transform_Interface ...] Other transformations to apply
   * @return boolean True on success
   * @access public
   * @implements \GDImage\Image_Interface
   */ 
  public function apply( Transform_Interface $transform /** /, Transform_Interface ...$transforms /**/ )
  {
    if( $this->_rsc )
    {
      // unregister memory handler, to be sure the resource will not sleep anymore
      if( $this->_mem_id ) MemoryHandler::unregister( $this->_mem_id );
      
      // wake up GD resource
      $this->_rsc->awakeGdResource();
      
      $flag = $transform( $this->_rsc );
    
      // apply extra transformations
      
      // we must care about case where 10 transformations have to be apply on 
      // an animated GIF with 50 frames, we must optimize it a little bit
      // in PHP 5.6 the ... declaration solve that
      for( $i = 1, $imax = func_num_args(); $i < $imax; $i++ )
      {
        $transform = func_get_arg( $i );
        // a valid transform?
        if( $transform instanceof Transform_Interface )
        {
          // apply it
          $flag = $transform( $this->_rsc ) && $flag;
        }
        else
        {
          // trigger PHP Fatal error, not mine
          $this->apply( $transform );
        }
      }
      
      // register memory handler again
      $this->_mem_id = MemoryHandler::register( $this->_rsc );
      
      return $flag;
    }
    
    return false;
  }
  
  /**
   * Reset the image to its default state
   *
   * @param none
   * @return void
   * @access public
   * @implements \GDImage\Image_Interface
   */
  public function reset()
  {
    $this->unsetResource();
  }

  // Special clone
  
  /**
   * Clone resource to not modify original resource.
   * 
   * @param none
   * @return void
   * @access public
   */
  public function __clone()
  {
    if( $this->_rsc )
    {
      $this->_rsc = clone $this->_rsc;
      
      // also assign new mem_id
      $this->_mem_id = MemoryHandler::register( $this->_rsc );
    }
  }
}
