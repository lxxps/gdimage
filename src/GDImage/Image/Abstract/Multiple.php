<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Abstract class for \GDImage\Image_* instance with multiple images.
 * In that case, images are animated.
 * 
 * To avoid any confusion we use the term "frame" to represent images in the 
 * animated image.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage image
 * @author     Loops <pierrotevrard@gmail.com>
 * @abstract
 * @implements \GDImage\Image_Interface
 */
abstract class Image_Abstract_Multiple implements Image_Interface
{
  
  /**
   * Constructor
   * 
   * Reset the image to its default state.
   * 
   * @param none
   * @return void
   * @access public
   */
  public function __construct()
  {
    // restore default
    $this->reset();
  }
  
  // Frames methods
  
  /**
   * Current frames.
   *
   * @var array Array of \GDImage\Image_Interface
   * @access protected
   */
  public $_frames = array();
  
  /**
   * Method to prepare a frame before to add it to frames array.
   * 
   * To be able to manipulate frames healthy, all frame must extends the
   * same class. Sounds logical, not?
   *
   * @param Image_Interface $image
   * @return Image_Interface
   * @access protected
   * @abstract
   */
  abstract public function _prepareFrame( Image_Interface $image );
  
  /**
   * Method to assign a frame helping a position.
   * Note that this method does not add a frame, but replace an existing one.
   * To add frame, use append/prepend method.
   * 
   * - If position is positive, items goes at the expected position.
   *     * 0: first position
   *     * 1: second position
   *     * ...
   * - If position is negative, position is determined from the end of the array.
   *     * -1: last position 
   *     * -2: second last position
   *     * ...
   * 
   * I order to keep the same behavior than array_splice(), we do not trigger 
   * error if position is out of range.
   * 
   * Examples:
   * - $i->setFrame( $frame , 0 ): $frame replace first frame
   * - $i->setFrame( $frame , 2 ): $frame replace third frame
   * - $i->setFrame( $frame , -1 ): $frame replace last frame
   *
   * @param \GDImage\Image_Interface $frame
   * @param integer $pos
   * @return void
   * @access public
   */ 
  public function setFrame( Image_Interface $frame , $pos )
  {
    // prepare frame before insertion
    $frame = $this->_prepareFrame( $frame );
    
    // force integer
    $pos = $pos | 0;
    $max = count( $this->_frames );
    
    // adjust negative position
    if( $pos < 0 ) $pos = $max - $pos;
    
    // out of range
    if( $pos < 0 ) $pos = 0;
    // out of range, add it to the end
    if( $pos > ( --$max ) ) $pos = $max;
    
    // assign it
  	$this->_frames[$pos] = $frame;
  }
  
  /**
   * Method to add a frame.
   * 
   * - If position is omitted or null, add frame at the end of frames array.
   * - If position is 0, add frame at the begin of frames array.
   * - If position is positive, add frame at the expected position.
   * - If position is negative, add frame at the expected position
   *   from the end of the array.
   * 
   * This method uses array_splice() that do not trigger error if position 
   * is out of range.
   *
   * @param \GDImage\Image_Interface $frame
   * @param integer $pos
   * @return void
   * @access public
   */ 
  public function addFrame( Image_Interface $frame , $pos = null )
  {
    if( $pos === null ) $pos = count($this->_frames);
    
    // prepare frame before insertion
    $frame = $this->_prepareFrame( $frame );
    
    // just call array_splice() method
    // use array to be sure that object will be inserted as is and not like an array
    array_splice( $this->_frames , $pos , 0 , array( $frame ) );
  }
  
  /**
   * Method to unset a frame.
   *
   * @param integer|string $pos
   * @return boolean Success flag
   * @access public
   */ 
  public function unsetFrame( $pos )
  {
    // force integer
    $pos = $pos | 0;
    
    if( isset($this->_frames[$pos]) )
    {
      // remove this frame and rearrange indexes
      array_splice( $this->_frames , $pos , 1 );
      return true;
    }
    
  	return false;
  }
  
  /**
   * Method to retrieve frame.
   *
   * @param integer|string $pos
   * @return \GDImage\Image_Interface
   * @access public
   */ 
  public function getFrame( $pos )
  {
    if( isset($this->_frames[$pos]) )
    {
      return $this->_frames[$pos];
    }
    
  	return null;
  }
  
  /**
   * Returns true is the image has frame at expected position.
   *
   * @param none
   * @return boolean
   * @access public
   */ 
  public function hasFrame( $pos )
  {
  	return isset($this->_frames[$pos|0]);
  }
  
  /**
   * Assign all frames.
   * This method will unset previous frames.
   * 
   * Note that indexes are rearranged.
   * 
   * @param array $frames Array of \GDImage\Image_Interface
   * @return void
   * @access public
   */
  public function setFrames( array $frames )
  {
    $this->unsetFrames();
    
    $frames = array_values( $frames );
    $i = count( $frames );
    while( $i-- )
    {
      // add frame at the begining of the array
      $this->addFrame( $frames[$i] , 0 );
    }
  }
  
  /**
   * Unset all frames.
   * 
   * @param array $frames Array of \GDImage\Image_Interface
   * @return boolean Success flag
   * @access public
   */
  public function unsetFrames()
  {
    $this->_frames = array();
    return true;
  }
  
  /**
   * Get all frames.
   * 
   * @param none
   * @return array Array of \GDImage\Image_Interface
   * @access public
   */
  public function getFrames()
  {
    return $this->_frames;
  }
  
  /**
   * Return true if the image has at least one frame.
   * 
   * @param none
   * @return integer
   * @access public
   */
  public function hasFrames()
  {
    return isset( $this->_frames[0] );
  }
  
  /**
   * Get the number of frames.
   * 
   * @param none
   * @return integer
   * @access public
   */
  public function countFrames()
  {
    return count( $this->_frames );
  }
  
  // Implements
  
  /**
   * Detect if the class may be used.
   * Return an integer value that will be used as priority for import/creation.
   * If the value returned is greater or equal than self::MUSTBE, not any other 
   * classes will be detected.
   *
   * @param string $mimetype                 Expected MIME Type
   * @param string $filepath                 File path, may be null
   * @param string $binary                   Binary data, may be null
   * @param \GDImage\Image_Interface $image  Image, may be null
   * @return integer 
   * @access public
   * @static
   * @implements \GDImage\Image_Interface
   */
  public static function detect( $mimetype , $filepath = null , $binary = null , Image_Interface $image = null )
  {
    if( $mimetype !== static::getMimeType() ) return static::NOTBE;
    
    // this class MAY BE used (not 100% sure)
    $priority = static::MAYBE;
    
    // only if MIME Type match
    if( $image && $image instanceof Image_Abstract_Multiple )
    {
      // this class MAY BE used (not 100% sure, but more probable)
      $priority += static::MAYBE;
    }
    
    return $priority;
  }
  
  /**
   * Create the resource from another \GDImage\Image_Interface.
   * Note that there is no need to clone image, let the developer decides.
   *
   * @param Image_Interface $image
   * @return boolean Success flag
   * @access public
   * @throws \GDImage\Exception_Resource
   * @implements \GDImage\Image_Interface
   */
  public function fromImage( Image_Interface $image )
  {
    return $image->toImage( $this );
  }
  
  /**
   * Create the resource to another \GDImage\Image_Interface.
   * Note that there is no need to clone image, let the developer decides.
   *
   * @param Image_Interface $image
   * @return boolean Success flag
   * @access public
   * @throws \GDImage\Exception_Resource
   * @implements \GDImage\Image_Interface
   */
  public function toImage( Image_Interface $image )
  {
    if( $this->hasFrame( 0 ) )
    {
      // multiple to single
      if( $image instanceof Image_Abstract_Single )
      {
        $image->setResource( $this->getFrame( 0 )->getResource() );
//        $image->setResource( clone $this->getFrame( 0 )->getResource() );

        return true;
      }
      
      // multiple to multiple
      if( $image instanceof Image_Abstract_Multiple )
      {
        // call setFrames() to reset and prepare frame
        // passing data from one frame type to another belong to the frame class
        $image->setFrames( $this->getFrames() );
//        $i = $this->countFrames();
//        while( $i-- )
//        {
//          $image->addFrame( clone $this->getFrame( $i ) , 0 );
//        }

        return true;
      }
    }
    
    return false;
  }
  
  /**
   * Get width of the image.
   * 
   * For covenience, get height of the first frame.
   *
   * @param none
   * @return integer Width
   * @access public
   * @implements \GDImage\Image_Interface
   */ 
  public function getWidth()
  {
    if( isset($this->_frames[0]) )
    {
  	  return $this->_frames[0]->getWidth();
    }
    
    return null;
  }
  
  /**
   * Get height of the image.
   * 
   * For covenience, get height of the first frame.
   *
   * @param none
   * @return integer Height
   * @access public
   * @implements \GDImage\Image_Interface
   */ 
  public function getHeight()
  {
    if( isset($this->_frames[0]) )
    {
  	  return $this->_frames[0]->getHeight();
    }
    
    return null;
  }
  
  /**
   * Apply \GDImage\Transform_Interface to the image
   * 
   * On PHP 5.6 we will be able to use ... declaration.
   * @see http://php.net/manual/en/functions.arguments.php#functions.variable-arg-list
   *
   * @param \GDImage\Transform_Interface $transform
   * @param [\GDImage\Transform_Interface ...] Other transformations to apply
   * @return boolean True on success
   * @access public
   * @implements \GDImage\Image_Interface
   */ 
  public function apply( Transform_Interface $transform /** /, Transform_Interface ...$transforms /**/ )
  {
    $i = count( $this->_frames );
    // initialize flag from frame count
    $flag = (bool)$i;
    // fetch all arguments
    $args = func_get_args();
    // apply them over all frames
    while( $i-- )
    {
      $flag = call_user_func_array( array( $this->_frames[$i] , 'apply' ) , $args ) && $flag;
    }
    // finally return flag
    return $flag;
  }
  
  /**
   * Reset the image to its default state
   *
   * @param none
   * @return void
   * @access public
   * @implements \GDImage\Image_Interface
   */
  public function reset()
  {
    $this->unsetFrames();
  }

  // Special clone
  
  /**
   * Clone frames to not modify original frames.
   * 
   * @param none
   * @return void
   * @access public
   */
  public function __clone()
  {
    $i = count( $this->_frames );
    while( $i-- )
    {
      $this->_frames[$i] = clone $this->_frames[$i];
    }
  }
}
