<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Class for GD resource with palette color.
 * 
 * Palette color image works entirely differently that true color,
 * especially on color deallocation that remove the color from palette.
 * 
 * Palette color image can have a transparent color that is used when image
 * is converted to an opaque one.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage resource
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends    \GDImage\Resource
 * @implements \GDImage\Resource_Abstract
 */
class Resource_PaletteColor extends Resource
{
  
  /**
   * Method to create an \GDImage\Resource_PaletteColor instance 
   * from width and height.
   *
   * @param integer $w
   * @param integer $h
   * @return \GDImage\Resource_PaletteColor
   * @access public
   * @throws \GDImage\Exception_Resource
   * @static
   */
  public static function createFromSize( $w , $h )
  {
    // force integer
    $w = $w | 0; $h = $h | 0;
    if( $w < 1 || $h < 1 )
    {
      throw new Exception_Resource( array( get_called_class() , func_get_arg(0) , func_get_arg(1) ) , 4030 );
    }
    
    // release memory, may throw exception
    MemoryHandler::release( $w.'x'.$h );
    
    return static::createFromGdResource( imagecreate( $w , $h ) );
  }
  
  /**
   * Return true if resource is a true color resource
   * 
   * @param none
   * @return boolean
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function isTrueColor()
  {
    return false;
  }
  
  /**
   * Return true if resource is a palette color resource
   * 
   * @param none
   * @return boolean
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function isPaletteColor()
  {
    return true;
  }
  
  /**
   * Return a resource representation with true color.
   * Can return a new instance or the instance itself.
   * 
   * @param none
   * @return \GDImage\Resource_TrueColor
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function toTrueColor()
  {
    // release memory
    MemoryHandler::release( $this->_mem );
    
    // we need to work direclty on resource
    $rsc = imagecreatetruecolor( $this->_w , $this->_h );
      
    // disable alpha blending to make sure color information are transmitted
    // to final pixel
    imagealphablending( $rsc , false );
    
    // before to manage resource color, we must be sure that the resource is awaken
    $this->awakeGdResource();
    
    // look for transparent color
    if( $transparent = $this->getTransparent() )
    {
      // make sure it is fully transparent
      $transparent->setAlpha( Color::ALPHA_TRANSPARENT );
      
      // fill resource with this color
      imagefill( $rsc , 0 , 0 , $transparent->toInteger() );
    }
    // no transparency, look for background color instead
    elseif( $background = $this->getBackground() )
    {
      // make sure it is fully transparent
      $background->setAlpha( Color::ALPHA_TRANSPARENT );
      
      // fill resource with this color
      imagefill( $rsc , 0 , 0 , $background->toInteger() );
    }
    // no transparency or background, fill it with defaut color as transparent
    else
    {
      $color = new Color();
      // make sure it is fully transparent
      $color->setAlpha( Color::ALPHA_TRANSPARENT );

      // fill resource with this color
      imagefill( $rsc , 0 , 0 , $color->toInteger() );
    }
    
    // copy resource
    imagecopy( $rsc , $this->getGdResource() , 0 , 0 , 0 , 0 , $this->_w , $this->_h );
    
    // create instance
    $instance = static::createFromGdResource( $rsc );
    // propagate modes
    $instance->setModes( $this->_modes );
    // propagate interpolation
    $instance->setInterpolation( $this->_interpolation );
    // return instance
    return $instance;
  }
  
  /**
   * Return a resource representation with palette color.
   * Can return a new instance or the instance itself.
   * 
   * @param [integer] $nb Number of colors of the palette
   * @param [boolean] $dither Dither flag
   * @return \GDImage\Resource_PaletteColor
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function toPaletteColor( $nb = 256 , $dither = false )
  {
    return $this;
  }
  
  // Resource methods
      
  /**
   * Validate GD resource.
   *
   * @param resource $rsc GD resource
   * @return resource
   * @access protected
   * @throws \GDImage\Exception_Resource
   */
  public function _validateGdResource( $rsc )
  {
    parent::_validateGdResource( $rsc );
    
    if( imageistruecolor( $rsc ) )
    {
      throw new Exception_Resource( get_class( $this ) , 4020 );
    }
  }
  
  /**
   * Get a exact copy of the GD resource.
   * 
   * Use it when you want to fetch the GD resource of an instance 
   * that is going to be destroyed.
   * 
   * Use it with caution because this resource will not be 
   * automatically destroyed.
   *
   * @param void
   * @return resource
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function cloneGdResource()
  {
    // do not do that here
    // at this point, we may have two instances that rely on the same GD resource
    // so there is a risk that the cloned instance will sleep GD resource used 
    // by the clone instance
    
//    // release memory
//    MemoryHandler::release( $this->_mem );
    
    // we must work on a direct resource
    $rsc = imagecreate( $this->_w , $this->_h );
    
    // copy palette
    imagepalettecopy( $rsc , $this->getGdResource() );

    // look for transparent color
    if( ( $i = imagecolortransparent( $this->__rsc ) ) !== -1 )
    {
      imagecolortransparent( $rsc , $i );
      
      // fill resource with this color
      imagefill( $rsc , 0 , 0 , $i );
    }
    
    // copy resource
    imagecopy( $rsc , $this->__rsc , 0 , 0 , 0 , 0 , $this->_w , $this->_h );
    
    
    // we want to apply current modes on the new resource
    // we never know future of these function, so force boolean
    // @see Resource_Abstract->setModes()
    // this one is useless on palette color
    // imagealphablending( $rsc , (bool)( $this->_modes & self::MODE_ALPHABLENDING ) );
    imagesavealpha( $rsc , (bool)( $this->_modes & self::MODE_SAVEALPHA ) );
    imageantialias( $rsc , (bool)( $this->_modes & self::MODE_ANTIALIAS ) );
    imageinterlace( $rsc , (bool)( $this->_modes & self::MODE_INTERLACE ) );
    
    // we want to apply current interpolation on the new resource
    // @see Resource_Abstract->setInterpolation()
    imagesetinterpolation( $rsc , $this->_interpolation );
    
    // finally return it
    return $rsc;
  }
  
  // Copy methods
  
  /**
   * Return a new Resource_Abstract instance that is a copy
   * the current instance with only background color.
   *
   * @param [integer] $w Width of resource
   * @param [integer] $h Height of resource
   * @param [boolean] $palette Flag for palette copy
   * @return \GDImage\Resource_Abstract
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function blank( $w = null , $h = null , $palette = false )
  {
    if( $w === null ) $w = $this->getWidth();
    if( $h === null ) $h = $this->getHeight();

    $blank = static::createFromSize( $w , $h );
    
    // before to manage resource colors, we must be sure that resource is awaken
    $this->awakeGdResource();
    
    if( $palette )
    {
      // as simple as that
      imagepalettecopy( $blank->getGdResource() , $this->getGdResource() );
      // register background color
      $background = $blank->getBackground();
    }
    else
    {
      // if we do not want to copy the color palette, we want to keep background color
      // yeap, on palette color the first allocated color will be background color
      // so it has a special meaning
      // @see http://php.net/manual/en/function.imagecolorallocate.php
      if( $background = $this->getBackground() )
      {
        $blank->setColor( $background );
      }
    }
    
    // we want to propagate transparent color
    if( $transparent = $this->getTransparent() )
    {
      // the result will never be false because we have copy the palette, or 
      // enough empty color slot in the palette
      $i = $blank->setTransparent( $transparent );
      
      // if there was a background, we want to fill image with transparent color
      if( $background )
      {
        imagefill( $blank->getGdResource() , 0 , 0 , $i );
      }
    }
    
    return $blank;
  }
  
  // Color methods
  // For performance reasons, on color methods, we consider that the GD 
  // resource is awaken
  
  /**
   * Get a color from its identifier
   * 
   * On palette color, identifier is an integer that represents the index 
   * of the color in the palette.
   * 
   * Note that if the color is not on the index, imagecolorsforindex()
   * will provide a warning. WE WANT TO KEEP THIS WARNING.
   *
   * @param integer $identifier
   * @return \GDImage\Color
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function getColor( $identifier )
  {
    if( $data = imagecolorsforindex( $this->__rsc , $identifier ) )
    {    
      return new Color( array_values( $data ) );
    }
    
    return false;
  }
  
  /**
   * Get corresponding color identifier from a \GDImage\Color
   * 
   * On palette color, a color may not be found except if it is
   * on the palette.
   * 
   * This method accept a second argument that enable color approximation.
   * In this case the method cannot return false.
   *
   * @param \GDImage\Color $color
   * @param [boolean] $exact (default to true)
   * @return integer Color identifier
   * @access public
   * @implements \GDImage\Resource_Abstract
   * @see http://php.net/manual/en/function.imagecolorexactalpha.php
   * @see http://php.net/manual/en/function.imagecolorresolvealpha.php
   */
  public function findColor( Color $color , $exact = true )
  {
    list( $r , $g , $b , $a ) = $color->toArray();
    
    if( $exact )
    {
      $i = imagecolorexactalpha( $this->__rsc , $r , $g , $b , $a );
      
      // not found
      if( $i === -1 ) 
      {
        return false;
      }
      
      return $i;
    }
    
    // never use imagecolorresolvealpha() because it seems to allocate the 
    // color if possible and you do not want that on findColor()
    
    // imagecolorclosestalpha() guarantee that an identifier will be found
    // also, this function does not take care about unused color from the 
    // palette, that is good
    // @see http://php.net/manual/en/function.imagecolorclosest.php
    return imagecolorclosestalpha( $this->__rsc , $r , $g , $b , $a );
  }
  
  /**
   * Set a color
   * 
   * On palette color image, we can allocate only if there is some index 
   * available in the color palette.
   * 
   * This method accept a second argument that disable color approximation.
   * In this case the method can return false.
   *
   * @param \GDImage\Color $color
   * @param [boolean] $exact (default to false)
   * @return integer Color identifier
   * @access public
   * @implements \GDImage\Resource_Abstract
   * @see http://php.net/manual/en/function.imagecolorallocatealpha.php
   * @see http://php.net/manual/en/function.imagecolorclosestalpha.php
   */
  public function setColor( Color $color , $exact = false )
  {
    // before to allocate a new color, look if it already exists
    if( ( $id = $this->findColor( $color , true ) ) !== false )
    {
      return $id;
    }
    
    // extract info
    list( $r , $g , $b , $a ) = $color->toArray();
    
    // not found, attemp to allocate it
    $id = imagecolorallocatealpha( $this->__rsc , $r , $g , $b , $a );
    
    // not allocated or exact color expected, return id, even if it is false
    if( $id !== false || $exact )
    {
      return $id;
    }
    
    // look for closest color in palette
    // imagecolorclosestalpha() guarantee that an identifier will be found
    // @see http://php.net/manual/en/function.imagecolorclosestalpha.php
    return imagecolorclosestalpha( $this->__rsc , $r , $g , $b , $a );
  }
  
  /**
   * Unset a color
   *
   * On palette color image, deallocate a color will remove it from the palette.
   * Pixels of this color won't be drawn anymore.
   * 
   * @param \GDImage\Color $color
   * @return boolean
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function unsetColor( Color $color )
  {
    // yes, we want an exact match
    if( ( $i = $this->findColor( $color , true ) ) !== false )
    {
      return imagecolordeallocate( $this->__rsc , $i );
    }
    
    return false;
  }
  
  /**
   * Get a color from pixel location
   * 
   * imagecolorat() generate a warning if study pixel is out of bounds.
   * WE WANT TO KEEP IT.
   *
   * @param integer $x X-pos
   * @param integer $y Y-pos
   * @return \GDImage\Color
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function getColorAt( $x , $y )
  {
    if( ( $id = imagecolorat( $this->__rsc , $x , $y ) ) !== false )
    {
      return $this->getColor( $id );
    }
    
    return false;
  }
  
  /**
   * Set a color to pixel location
   * 
   * This method accept a second argument that disable color approximation.
   * In this case the method can return false.
   *
   * @param \GDImage\Color $color
   * @param integer $x X-pos
   * @param integer $y Y-pos
   * @param [boolean] $exact (default to false)
   * @return boolean True on success
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function setColorAt( Color $color , $x , $y , $exact = false )
  {
    if( ( $id = $this->setColor( $color , $exact ) ) !== false )
    {
      return imagesetpixel( $this->__rsc , $x , $y , $id );
    }
    return false;
  }
  
  /**
   * Count the number of available colors
   *
   * @param none
   * @return integer Number of colors
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function countColors()
  {
    return imagecolorstotal( $this->__rsc );
  }

  /**
   * Return transparent color
   * Transparent is very important for conversion from Resource_PaletteColor
   * to Resource_TrueColor image without alpha information (like Jpeg).
   *
   * @param none
   * @return \GDImage\Color
   * @access public
   * @implements \GDImage\Resource_Abstract
   */ 
  public function getTransparent()
  {
    if( ( $i = imagecolortransparent( $this->__rsc ) ) !== -1 )
    {
      return $this->getColor( $i );
    }
    
    return false;
  }
  
  /**
   * Assign transparent color
   * Transparent is very important for conversion from Resource_PaletteColor
   * to Resource_TrueColor image without alpha information (like Jpeg).
   *
   * @param \GDImage\Color $color
   * @return integer Index of the new transparent color
   * @access public
   * @implements \GDImage\Resource_Abstract
   */ 
  public function setTransparent( Color $color )
  {
    // deallocate previous transparent color, if any
    if( ( $i = imagecolortransparent( $this->__rsc ) ) !== -1 )
    {
      // deallocate this color
      imagecolordeallocate( $this->__rsc , $i );
    }
    
    // assign a new transparent color
    // note that the identifier should be the same
    // than the transparent color removed above
    // we want an exact match
    if( ( $i = $this->setColor( $color , true ) ) !== false )
    {
      return imagecolortransparent( $this->__rsc  , $i );
    }
    
    // fail
    return false;
  }

  /**
   * Return background color
   * Documentation clains that backgournd color is the firest color in palette.
   * 
   * @param none
   * @return \GDImage\Color
   * @access public
   * @implements \GDImage\Resource_Abstract
   */ 
  public function getBackground()
  {
    if( $this->countColors() > 0 )
    {
      return $this->getColor( 0 );
    }
    
    return false;
  }
  
}
