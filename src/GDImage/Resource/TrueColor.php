<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration settings:
 * - "resource_true_color_to_palette_alpha_tolerance": Minimum alpha channel
 *   value to consider a color as possible transparent color for the palette 
 *   color on true color to palette color conversion
 */
if( ! Config::hasResourceTrueColorToPaletteAlphaTolerance() ) {
  Config::setResourceTrueColorToPaletteAlphaTolerance( 123 );
}

/**
 * Class for GD resource with true color.
 * 
 * True color image works entirely differently than palette color:
 * you can allocate any number of colors, any colors is always found with 
 * exact match, and color deallocation that do not remove the color from 
 * the image.
 * 
 * Also each time a new color has to be wrote, the previous color has to 
 * be deallocate.
 * 
 * True color image does not have transparent ability.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage resource
 * @author     Loops <pierrotevrard@gmail.com>
 * @extends    \GDImage\Resource
 * @implements \GDImage\Resource_Abstract
 */
class Resource_TrueColor extends Resource
{
  
  /**
   * Method to create an \GDImage\Resource_TrueColor instance 
   * from width and height.
   *
   * @param integer $w
   * @param integer $h
   * @return \GDImage\Resource_TrueColor
   * @access public
   * @throws \GDImage\Exception_Resource
   * @static
   */
  public static function createFromSize( $w , $h )
  {
    // force integer
    $w = $w | 0; $h = $h | 0;
    if( $w < 1 || $h < 1 )
    {
      throw new Exception_Resource( array( get_called_class() , func_get_arg(0) , func_get_arg(1) ) , 4030 );
    }
    
    // release memory
    MemoryHandler::release( $w.'x'.$h );
    
    $image = static::createFromGdResource( imagecreatetruecolor( $w , $h ) );
    
    // fill it with defaut color as transparent
    $color = new Color();
    // with full alpha
    $color->setAlpha( Color::ALPHA_TRANSPARENT );
    
    imagefill( $image->getGdResource() , 0 , 0 , $color->toInteger() );
    
    // finally return instance
    return $image;
  }
  
  /**
   * Clone method.
   * 
   * On clone, an exact copy of the resource must be done.
   *
   * @param none
   * @return void
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function __clone()
  {
    // deallocated last color
    ( $this->__rsc !== null ) && $this->_unsetLastColor();
    // call parent method
    parent::__clone();  
  }
  
  /**
   * Return true if resource is a true color resource
   * 
   * @param none
   * @return boolean
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function isTrueColor()
  {
    return true;
  }
  
  /**
   * Return true if resource is a palette color resource
   * 
   * @param none
   * @return boolean
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function isPaletteColor()
  {
    return false;
  }
  
  /**
   * Return a resource representation with true color.
   * Can return a new instance or the instance itself.
   * 
   * @param none
   * @return \GDImage\Resource_TrueColor
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function toTrueColor()
  {
    return $this;
  }
  
  /**
   * Return a resource representation with palette color.
   * Can return a new instance or the instance itself.
   * 
   * @param [integer] $nb Number of colors of the palette
   * @param [boolean] $dither Dither flag
   * @param [Color]   $background Background color to use
   * @return \GDImage\Resource_PaletteColor
   * @access public
   * @implements \GDImage\Resource_Abstract
   * @see http://php.net/manual/en/function.imagetruecolortopalette.php
   */
  public function toPaletteColor( $nb = 256 , $dither = false , Color $background = null )
  {
    // release memory
    MemoryHandler::release( $this->_mem );
    
    // we need to work direclty on resource
    $rsc = imagecreatetruecolor( $this->_w , $this->_h );

    // disable alpha blending
    imagealphablending( $rsc , false );
    
    if( $background )
    {
      // fill it with transparent
      imagefill( $rsc , 0 , 0 , $background->toInteger() );

      // enable alpha blending
      imagealphablending( $rsc , true );
    }
    
    // copy resource
    imagecopy( $rsc , $this->getGdResource() , 0 , 0 , 0 , 0 , $this->getWidth() , $this->getHeight() );
    
    // transform it to palette with
    imagetruecolortopalette( $rsc , $dither , $nb );
    // then apply a match that gives better results
    // especially with alpha channels
    imagecolormatch( $this->getGdResource() , $rsc );
    
    // create instance
    $instance = static::createFromGdResource( $rsc );
    // propagate modes
    $instance->setModes( $this->_modes );
    // propagate interpolation
    $instance->setInterpolation( $this->_interpolation );
    
    // GD has some bad habits: after manipulation, the same color may 
    // be used more than one time in the palette, that will grow up the palette 
    // with unexpected values, especially if you want to reduce to a small amount 
    // of colors
      
    // copy to reduce palette color to its minimal
    $tmp = $instance->blank();
    imagecopy( $tmp->getGdResource() , $instance->getGdResource() , 0 , 0 , 0 , 0 , $instance->getWidth() , $instance->getHeight() );
    // destroy previous resource
    $instance = $tmp;
    
    // we want a transparent color if available
    // especially on GIF picture

    // if imagetruecolortopalette has done the job correctly
    // it has merge all transparent colors to the same one
    // unfortunately, it appears that the color may not be treated as fully
    // transparent (alpha channel to 120 or more)
    // 
    // so we want to look for a color with the maximum alpha channel
    
    $transparent = array();
    
    // get min alpha value from configuration
    $tolerance = Config::getResourceTrueColorToPaletteAlphaTolerance();
    
    // look on the palette if any color has a full alpha
    $i = $instance->countColors();
    while( $i-- )
    {
      $c = $instance->getColor( $i );
      
      // not interesting
      if( $c->getAlpha() < $tolerance ) continue;
      
      // no transparent, assign
      if( ! $transparent )
      {
        $transparent = array( $c );
        continue;
      }
      
      // look if the color has a greather alpha channel
      if( $c->getAlpha() > $transparent[0]->getAlpha() )
      {
        $transparent = array( $c );
        continue;
      }
      
      // if transparent is equal, we cannot determine which one is better
      if( $c->getAlpha() === $transparent[0]->getAlpha() )
      {
        // add it to transparent array
        $transparent[] = $c;
      }
    }
    
    // look if all colors are identicals
    // sometimes, the same color is present twice
    // hopefully, array_unique do not strict match so we can use 
    // it on an array of objects
    $transparent = array_unique( $transparent , SORT_REGULAR );
    
    // to be sure that this color can be considered as transparent color
    // we must have found a unique transparent color
    if( count( $transparent ) === 1 )
    {
      $instance->setTransparent( $transparent[0] );
    }
    
    // return instance
    return $instance;
  }
  
  // Resource methods
      
  /**
   * Validate GD resource.
   *
   * @param resource $rsc GD resource
   * @return resource
   * @access protected
   * @throws \GDImage\Exception_Resource
   */
  public function _validateGdResource( $rsc )
  {
    parent::_validateGdResource( $rsc );
    
    if( ! imageistruecolor( $rsc ) )
    {
      throw new Exception_Resource( get_class( $this ) , 4010 );
    }
  } 
  
  /**
   * Get a exact copy of the GD resource.
   * 
   * Use it when you want to fetch the GD resource of an instance 
   * that is going to be destroyed.
   * 
   * Use it with caution because this resource will not be 
   * automatically destroyed.
   *
   * @param void
   * @return resource
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function cloneGdResource()
  {
    // do not do that here
    // at this point, we may have two instances that rely on the same GD resource
    // so there is a risk that the cloned instance will sleep GD resource used 
    // by the clone instance
//    // release memory
//    MemoryHandler::release( $this->_mem );
    
    // we must work on a direct resource
    $rsc = imagecreatetruecolor( $this->_w , $this->_h );

    // disable alpha blending for copy
    imagealphablending( $rsc , false );
    // copy resource
    imagecopy( $rsc , $this->getGdResource() , 0 , 0 , 0 , 0 , $this->_w , $this->_h );
    
    // we want to apply current modes on the new resource
    // we never know future of these function, so force boolean
    // @see Resource_Abstract->setModes()
    imagealphablending( $rsc , (bool)( $this->_modes & self::MODE_ALPHABLENDING ) );
    imagesavealpha( $rsc , (bool)( $this->_modes & self::MODE_SAVEALPHA ) );
    imageantialias( $rsc , (bool)( $this->_modes & self::MODE_ANTIALIAS ) );
    imageinterlace( $rsc , (bool)( $this->_modes & self::MODE_INTERLACE ) );
    
    // we want to apply current interpolation on the new resource
    // @see Resource_Abstract->setInterpolation()
    imagesetinterpolation( $rsc , $this->_interpolation );
    
    // finally return it
    return $rsc;
  }
  
  /**
   * Destroy the GD resource.
   * 
   * You want to call it on __destruct() magic method.
   *
   * @param none
   * @return boolean Success flag
   * @access protected
   */
  public function _unsetGdResource()
  {
    // deallocated last color
    ( $this->__rsc !== null ) && $this->_unsetLastColor();
    // call parent method
    return parent::_unsetGdResource();
  }
  
  /**
   * Save the GD resource in a temporary file to balance memory usage.
   *
   * @param none
   * @return boolean Success flag
   * @access protected
   */
  public function sleepGdResource()
  {
    // deallocated last color
    ( $this->__rsc !== null ) && $this->_unsetLastColor();
    // call parent method
    return parent::sleepGdResource();
  }
  
  // Copy methods
  
  /**
   * Return a new Resource_Abstract instance that is a copy
   * the current instance with only background color.
   *
   * @param [integer] $w Width of resource
   * @param [integer] $h Height of resource
   * @return \GDImage\Resource_Abstract
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function blank( $w = null , $h = null )
  {
    if( $w === null ) $w = $this->_w;
    if( $h === null ) $h = $this->_h;

    return static::createFromSize( $w , $h );
  }
  
  // Color methods
  // For performance reasons, on color methods, we consider that the GD 
  // resource is awaken
  
  /**
   * Last allocated color identifier.
   * On true color image, we want to allocate one color at the same time.
   * 
   * @var integer Color identifier 
   * @access protected
   */
  public $_last_color_id;
  
  /**
   * Deallocate last allocated color
   * On true color image, we want to allocate one color at the same time.
   *
   * @param none
   * @return boolean Success flag
   * @access public
   */ 
  public function _unsetLastColor()
  {
    if( $this->_last_color_id !== null )
    {
      $flag = imagecolordeallocate( $this->__rsc , $this->_last_color_id );
      $this->_last_color_id = null;
      return $flag;
    }
    
    return false;
  }
  
  /**
   * Get a color from its identifier
   * 
   * On true color, id is an integer that represents the color 
   * with bitwise stuff.
   *
   * @param integer $identifier
   * @return \GDImage\Color
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function getColor( $identifier )
  {
    return new Color( $identifier | 0 );
  }
  
  /**
   * Get corresponding color identifier from a \GDImage\Color
   * 
   * On true color resource, a color can always be found.
   *
   * @param \GDImage\Color $color
   * @return integer Color identifier
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function findColor( Color $color )
  {
    return $color->toInteger();
  }
  
  /**
   * Set a color
   * 
   * On true color image, we want to allocate one color at the same time,
   * but we always found the color.
   *
   * @param \GDImage\Color $color
   * @return integer Color identifier
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function setColor( Color $color )
  {
    $i = $color->toInteger();
    
    if( $i !== $this->_last_color_id )
    {
      $this->_unsetLastColor();
      $this->_last_color_id = $i;
    }
    
    return $i;
    
//    $i = $color->toInteger();
//    
//    if( $i !== $this->_last_color_id )
//    {
//      $this->_unsetLastColor();
//      $this->_last_color_id = $i;
//      imagecolorallocatealpha( $this->__rsc , $color->getRed() , $color->getGreen() , $color->getBlue() , $color->getAlpha() );
//    }
  }
  
  /**
   * Unset a color
   *
   * @param \GDImage\Color $color
   * @return boolean
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function unsetColor( Color $color )
  {
    $i = $color->toInteger();
    
    // unset last color if necessary
    if( $i === $this->_last_color_id ) $this->_last_color_id = null;
      
    return imagecolordeallocate( $this->__rsc , $i );
  }
  
  /**
   * Get a color from pixel location
   * 
   * imagecolorat() generate a warning if study pixel is out of bounds.
   * WE WANT TO KEEP IT.
   *
   * @param integer $x X-pos
   * @param integer $y Y-pos
   * @return \GDImage\Color
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function getColorAt( $x , $y )
  {
    // watch out! it may return 0
    if( ( $id = imagecolorat( $this->__rsc , $x , $y ) ) !== false )
    {
      return $this->getColor( $id );
    }
    
    return false;
  }
  
  /**
   * Set a color to pixel location
   *
   * @param \GDImage\Color $color
   * @param integer $x X-pos
   * @param integer $y Y-pos
   * @return boolean True on success
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function setColorAt( Color $color , $x , $y )
  {
    // watch out! it may return 0
    if( ( $id = $this->setColor( $color ) ) !== false )
    {
      // return imagefilledrectangle( $this->__rsc , $x , $y , $x + 1 , $y + 1 , $id );
      return imagesetpixel( $this->__rsc , $x , $y , $id );
    }
    return false;
  }
  
  /**
   * Count the number of available colors
   * 
   * imagecolorstotal() function will returns 0 for true color resource
   * but is is wrong.
   *
   * @param none
   * @return integer Number of colors
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function countColors()
  {
    // return 0 for true color resource
    // return imagecolorstotal( $this->__rsc );
    
    // this value looks a lot better
    return ( 0xFF << 16 ) + ( 0xFF << 8 ) + ( 0xFF << 0 ) + ( 0x7F << 24 );
  }

  /**
   * Return transparent color.
   * 
   * For Resource_TrueColor, returns a transparent color from default color.
   *
   * @param none
   * @return \GDImage\Color
   * @access public
   * @implements \GDImage\Resource_Abstract
   */ 
  public function getTransparent()
  {
    // use default color
    $color = new Color();
    // set alpha to maximum
    $color->setAlpha( Color::ALPHA_TRANSPARENT );
    // return it
    return $color;
  }
  
  /**
   * Assign transparent color
   * 
   * For Resource_TrueColor, it is just an alias of setColor().
   *
   * @param \GDImage\Color $color
   * @return integer Index of the new transparent color
   * @access public
   * @implements \GDImage\Resource_Abstract
   */ 
  public function setTransparent( Color $color )
  {
    // just set it as any other color
    return $this->setColor( $color );
  }

  /**
   * Return background color.
   * 
   * For Resource_TrueColor, returns the transparent color.
   *
   * @param none
   * @return \GDImage\Color
   * @access public
   * @implements \GDImage\Resource_Abstract
   */ 
  public function getBackground()
  {
    return $this->getTransparent();
  }
}
