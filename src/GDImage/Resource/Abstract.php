<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration settings:
 * - "resource_true_color_class": Class to use for true color GD resource
 * - "resource_palette_color_class": Class to use for palette color GD 
 * - "resource_default_interpolation": Default interpolation to use on GD resource
 * - "resource_true_color_gd_overhead": An approximative ratio to determine GD 
 *   resource memory usage for true color resource from width and height
 * - "resource_palette_color_gd_overhead": An approximative ratio to determine GD 
 *   resource memory usage for palette color resource from width and height
 */
if( ! Config::hasResourceTrueColorClass() ) {
  Config::setResourceTrueColorClass( '\\GDImage\\Resource_TrueColor' );
}
if( ! Config::hasResourcePaletteColorClass() ) {
  Config::setResourcePaletteColorClass( '\\GDImage\\Resource_PaletteColor' );
}
if( ! Config::hasResourceDefaultInterpolation() ) {
  Config::setResourceDefaultInterpolation( 3 ); // 3 is the value for \IMG_BILINEAR_FIXED
}
if( ! Config::hasResourceTrueColorGdOverhead() ) {
  Config::setResourceTrueColorGdOverhead( 5.0846 ); // determined from PNG24 resource
}
if( ! Config::hasResourcePaletteColorGdOverhead() ) {
  Config::setResourcePaletteColorGdOverhead( 2.0846 ); // determined from PNG8 resource
}

/**
 * Abstract class for GD resource.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage resource
 * @author     Loops <pierrotevrard@gmail.com>
 * @abstract
 */
abstract class Resource_Abstract
{
  
  /**
   * Method to create an \GDImage\Resource_Abstract instance from resource
   *
   * @param resource $rsc
   * @return \GDImage\Resource_Abstract
   * @access public
   * @throws \GDImage\Exception_Resource
   * @static
   */
  public static function createFromGdResource( $rsc )
  {
    if( get_resource_type( $rsc ) !== 'gd' )
    {
      throw new Exception_Resource( array( get_called_class() , get_resource_type( $rsc ) ) , 4000 );
    }
    
    // seems unecessary
//    // release memory, may throw exception
//    MemoryHandler::release( imagesx( $rsc ).'x'.imagesy( $rsc ) );
    
    if( imageistruecolor( $rsc ) )
    {
      $class = Config::getResourceTrueColorClass();
      return new $class( $rsc );
    }
    else
    {
      $class = Config::getResourcePaletteColorClass();
      return new $class( $rsc );
    }
  }
  
  /**
   * Method to create an \GDImage\Resource_Abstract instance from 
   * binary data.
   * 
   * By default create image helping TrueColor child class.
   *
   * @param string $binary
   * @return \GDImage\Resource_Abstract
   * @access public
   * @throws \GDImage\Exception_Resource
   * @static
   */
  public static function createFromBinary( $binary )
  {
    // release memory, may throw exception
    MemoryHandler::release( $binary );
    
    if( ! ( $rsc = imagecreatefromstring( $binary ) ) )
    {
      throw new Exception_Resource( array( get_called_class() , crc32( func_get_arg(0) ) ) , 4040 );
    }
    return static::createFromGdResource( $rsc );
  }
  
  /**
   * Method to create an \GDImage\Resource_Abstract instance from width and height.
   * 
   * By default create image helping TrueColor child class.
   *
   * @param integer $w
   * @param integer $h
   * @return \GDImage\Image Image instance
   * @access public
   * @throws \GDImage\Exception
   * @static
   */
  public static function createFromSize( $w , $h )
  {
    return call_user_func( array( static::getTrueColorClass() , 'createFromSize' ) , $w , $h );
  }
  
  /**
   * Current image resource.
   * Please, do not touch it.
   *
   * @var resource Image resource
   * @access protected
   */
  public $__rsc;
  
  /**
   * Temporary GD file used to balance memory usage when necessary.
   * Please, do not touch it.
   *
   * @var string Filepath
   * @access protected
   */
  public $__tmp;
  
  /**
   * Width of the image.
   * Readonly.
   *
   * @var integer
   * @access protected
   */
  public $_w;
  
  /**
   * Height of the image.
   * Readonly.
   *
   * @var integer
   * @access protected
   */
  public $_h;
  
  /**
   * Approximative memory usage.
   * Readonly.
   *
   * @var integer
   * @access protected
   */
  public $_mem;
  
  /**
   * Construct method.
   *
   * @param resource $rsc GD Resource
   * @return void
   * @access public
   * @throws \GDImage\Exception
   */
  public function __construct( $rsc )
  {
    // look for interpolation configuration
    $this->_interpolation = Config::getResourceDefaultInterpolation();
    
    // set resource
    $this->setGdResource( $rsc );
  }
  
  /**
   * Destruct method.
   * 
   * On destruction, we want to destroy resource.
   *
   * @param none
   * @return void
   * @access public
   */
  public function __destruct()
  {
    $this->_unsetGdResource();
  }
  
  /**
   * Magic __toString, may be usefull.
   *
   * @param none
   * @return void
   * @access public
   */
  public function __toString()
  {
    if( $this->__rsc )
    {
      return (string)$this->__rsc;
    }
    
    return null;
  }
  
  /**
   * Clone method.
   * 
   * On clone, an exact copy of the resource must be done.
   *
   * @param none
   * @return void
   * @access public
   * @implements \GDImage\Resource_Abstract
   */
  public function __clone()
  {
    // clone resource
    if( $this->__rsc !== null )
    {
      // at this point, the clone is already processed, and we have two
      // Resource that work on the same GD resource
      
      // so freeing memory may destroy GD resource from previous instance 
      // (the cloned one) and result to Unknown resource error on current 
      // instance (the clone one)... The Clone Wars begins
      
      // assign new resource
      $this->__rsc = $this->cloneGdResource();
    }
    // copy temporary file
    if( $this->__tmp !== null )
    {
      // copy resource to new filename
      $tmp = tempnam( Config::getTemporaryDirectory() , Config::getTemporaryPrefix() );
      copy( $this->__tmp , $tmp );
      $this->__tmp = $tmp;
    }
  }
  
  /**
   * Return true if resource is a true color resource
   * 
   * @param none
   * @return boolean
   * @access public
   */
  abstract public function isTrueColor();
  
  /**
   * Return true if resource is a palette color resource
   * 
   * @param none
   * @return boolean
   * @access public
   */
  abstract public function isPaletteColor();
  
  /**
   * Return a resource representation with true color.
   * Can return a new instance or the instance itself.
   * 
   * @param none
   * @return \GDImage\Resource_TrueColor
   * @access public
   */
  abstract public function toTrueColor();
  
  /**
   * Return a resource representation with palette color.
   * Can return a new instance or the instance itself.
   * 
   * @param [integer] $nb Number of colors of the palette
   * @param [boolean] $dither Dither flag
   * @return \GDImage\Resource_PaletteColor
   * @access public
   * @see http://php.net/manual/en/function.imagetruecolortopalette.php
   */
  abstract public function toPaletteColor( $nb = 256 , $dither = false );
  
  /**
   * Get width of the resource.
   *
   * @param void
   * @return integer
   * @access public
   */
  public function getWidth()
  {
  	return $this->_w;
  }
  
  /**
   * Get height of the resource.
   *
   * @param void
   * @return integer
   * @access public
   */
  public function getHeight()
  {
  	return $this->_h;
  }
  
  /**
   * Get approximative memory usage.
   * Second parameter determine real memory usage: if the resource is not awaken, 
   * result will be 0.
   *
   * @param [boolean] $real
   * @return integer
   * @access public
   */
  public function getMemoryUsage( $real = true )
  {
    if( $real && $this->__rsc === null ) return 0;
    
  	return $this->_mem;
  }
  
  /**
   * Alias for getMemoryUsage().
   *
   * @param [boolean] $real
   * @return integer
   * @access public
   */
  public function getMemory( $real = true )
  {
  	return $this->getMemoryUsage( $real );
  }
  
  // Resource methods
  
  /**
   * Get the GD resource.
   * 
   * Use it with caution because the GD resource may be destroyed if there 
   * is no more reference to the current instance.
   *
   * @param void
   * @return resource
   * @access public
   */
  public function getGdResource()
  {
    // make sure the resource is awaken
    if( $this->__rsc === null && ( ! $this->awakeGdResource() ) ) return null;
    
  	return $this->__rsc;
  }
     
  /**
   * Validate GD resource.
   *
   * @param resource $rsc GD resource
   * @return resource
   * @access protected
   * @throws \GDImage\Exception_Resource
   */
  public function _validateGdResource( $rsc )
  {
    if( get_resource_type( $rsc ) !== 'gd' )
    {
      throw new Exception_Resource( array( get_class( $this ) , get_resource_type( $rsc ) ) , 4000 );
    }
  }   
  
  /**
   * Set the GD resource.
   * 
   * Check if a GD resource has already be assign to destroy it.
   * 
   * This method just assign resource link identifier, so the
   * submitted variable and the class proprety work together
   * on same resource.
   * 
   * Keep in mind that we cannot determine mime type from a GD resource.
   *
   * @param resource $rsc GD resource
   * @return void
   * @access public
   * @throws \GDImage\Exception_Resource
   */ 
  public function setGdResource( $rsc )
  {
    $this->_validateGdResource( $rsc );
    
  	$this->_unsetGdResource();
  	$this->__rsc = $rsc;
    $this->_w = imagesx( $this->__rsc );
    $this->_h = imagesy( $this->__rsc );
    $this->_mem = $this->_w * $this->_h * ( $this->isPaletteColor() ? Config::getResourcePaletteColorGdOverhead() : Config::getResourceTrueColorGdOverhead() );
    
    // apply current modes
    $this->setModes( $this->_modes );
    // apply current interpolation
    $this->setInterpolation( $this->_interpolation );
  }
  
  /**
   * Destroy the GD resource.
   * 
   * You want to call it on __destruct() magic method.
   *
   * @param none
   * @return boolean Success flag
   * @access protected
   */
  public function _unsetGdResource()
  {
  	$this->_w = null;
  	$this->_h = null;
    
    $flag = true;
    if( $this->__rsc !== null )
    {
      $flag = imagedestroy( $this->__rsc ) && $flag;
      $this->__rsc = null;
    }
    if( $this->__tmp !== null )
    {
      $flag = unlink( $this->__tmp ) && $flag;
      $this->__tmp = null;
    }
  	return $flag;
  }
  
  /**
   * Save the GD resource in a temporary file to balance memory usage.
   *
   * @param none
   * @return boolean Success flag
   * @access protected
   */
  public function sleepGdResource()
  {
    if( $this->__rsc === null ) return false;
    
    // create temporary file
    $this->__tmp = tempnam( Config::getTemporaryDirectory() , Config::getTemporaryPrefix() );
    
    // create temporary PNG file and free memory
    // we must us PNG file to preserve true and palette color abilities
    imagesavealpha( $this->__rsc , true );
    // do it as fast as possible
    if( imagepng( $this->__rsc , $this->__tmp , 0 , PNG_NO_FILTER ) && imagedestroy( $this->__rsc ) )
    {
      $this->__rsc = null;
      
      // notify memory handler that it has to remove memory usage for this ressource
      MemoryHandler::touch( $this , -1 );
      
      return true;
    }
    
    // fail
    unlink( $this->__tmp );
    $this->__tmp = null;
    return false;
  }
  
  /**
   * Restore the GD resource from temporary file.
   *
   * @param none
   * @return boolean Success flag
   * @access protected
   * @throw \GDImage\Exception_Memory
   */
  public function awakeGdResource()
  {
    if( $this->__tmp === null ) return false;
    
    // release memory, may throw an exception
    MemoryHandler::release( $this->_mem );
    
    if( $this->__rsc = imagecreatefrompng( $this->__tmp ) )
    {
      // remove temporary file
      unlink( $this->__tmp );
      $this->__tmp = null;
      
      // apply current modes
      $this->setModes( $this->_modes );
      // apply current interpolation
      $this->setInterpolation( $this->_interpolation );
      
      // notify memory handler that it has to add memory usage for this ressource
      MemoryHandler::touch( $this , 1 );
      
      return true;
    }
    
    // fail
    $this->__rsc = null;
    return false;
  }
  
  /**
   * Get a exact copy of the GD resource.
   * 
   * Use it when you want to fetch the GD resource of an instance 
   * that is going to be destroyed.
   * 
   * Use it with caution because this resource will not be 
   * automatically destroyed.
   *
   * @param void
   * @return resource
   * @access public
   */
  abstract public function cloneGdResource();
  
  // Copy methods
  
  /**
   * Return a new Resource instance that is the exact copy
   * the current instance.
   * 
   * Just an alias for clone...
   *
   * @param void
   * @return \GDImage\Resource_Abstract
   * @access public
   */
  public function copy()
  {
    // yes it is as simple
    return clone $this;
  }
  
  /**
   * Return a new Resource_Abstract instance that is a copy
   * the current instance with only background color.
   *
   * @param [integer] $w Width of resource
   * @param [integer] $h Height of resource
   * @return \GDImage\Resource_Abstract
   * @access public
   */
  abstract public function blank( $w = null , $h = null );
  
  // Color methods
  // For performance reasons, on color methods, we consider that the GD 
  // resource is awaken
  
  /**
   * Get a color from its identifier
   *
   * @param integer $identifier
   * @return \GDImage\Color
   * @access public
   */
  abstract public function getColor( $identifier );
  
  /**
   * Get corresponding color identifier from a \GDImage\Color
   *
   * @param \GDImage\Color $color
   * @return integer Color identifier
   * @access public
   */
  abstract public function findColor( Color $color );
  
  /**
   * Set a color
   *
   * @param \GDImage\Color $color
   * @return integer Color identifier
   * @access public
   */
  abstract public function setColor( Color $color );
  
  /**
   * Unset a color
   *
   * @param \GDImage\Color $color
   * @return boolean
   * @access public
   */
  abstract public function unsetColor( Color $color );
  
  /**
   * Get a color from pixel location
   *
   * @param integer $x X-pos
   * @param integer $y Y-pos
   * @return \GDImage\Color
   * @access public
   */
  abstract public function getColorAt( $x , $y );
  
  /**
   * Set a color to pixel location
   *
   * @param \GDImage\Color $color
   * @param integer $x X-pos
   * @param integer $y Y-pos
   * @return boolean True on success
   * @access public
   */
  abstract public function setColorAt( Color $color , $x , $y );
  
  /**
   * Count the number of available colors
   *
   * @param none
   * @return integer Number of colors
   * @access public
   */
  abstract public function countColors();
  
  /**
   * Get all colors in an 2D array to represent each pixels.
   *
   * @param none
   * @return array
   * @access public
   */
  public function getColorsAtAll()
  {
    $pixs = array();
      
    // read all pixels
    $x = $this->_w;
    while( $x-- )
    {
      $y = $this->_h;
      while( $y-- )
      {
        $pixs[$x][$y] = $this->getColorAt( $x , $y );
      }
    }
    
    return $pixs;
  }
  
  /**
   * Return transparent color
   * Even if it is used only on Resource_PaletteColor, we want it to 
   * be declared.
   *
   * @param none
   * @return \GDImage\Color
   * @access public
   */ 
  abstract public function getTransparent();
  
  /**
   * Assign transparent color
   * Even if it is used only on Resource_PaletteColor, we want it to 
   * be declared.
   *
   * @param \GDImage\Color $color
   * @return integer Index of the new transparent color
   * @access public
   */ 
  abstract public function setTransparent( Color $color );
  
  /**
   * Return background color
   * Even if it is used only on Resource_PaletteColor, we want it to 
   * be declared.
   *
   * @param none
   * @return \GDImage\Color
   * @access public
   */ 
  abstract public function getBackground();
  
  // Mode methods
  
  /**
   * Mode bitwise for alpha blending
   * 
   * @var integer
   * @const
   * @see http://php.net/manual/en/function.imagealphablending.php
   */
  const MODE_ALPHABLENDING = 1;
  
  /**
   * Mode bitwise for save alpha
   * 
   * @var integer
   * @const
   * @see http://php.net/manual/en/function.imagesavealpha.php
   */
  const MODE_SAVEALPHA = 2;
  
  /**
   * Mode bitwise for antialias
   * 
   * @var integer
   * @const
   * @see http://php.net/manual/en/function.imageantialias.php
   */
  const MODE_ANTIALIAS = 4;
  
  /**
   * Mode bitwise for interlace
   * 
   * @var integer
   * @const
   * @see http://php.net/manual/en/function.imageinterlace.php
   */
  const MODE_INTERLACE = 8;
  
  /**
   * Current modes.
   *
   * @var integer
   * @access protected
   */
  public $_modes = 0;
  
  /**
   * Set bitwise flag for a mode
   * 
   * Mode can be a string that will lookup on self::MODE_* constants.
   *
   * @param mixed $mode
   * @return void
   * @access public
   */
  public function setMode( $mode )
  {
    // string lookup
    if( is_string( $mode ) )
    {
      $mode = 'self::MODE_'.strtoupper( $mode );
      
      // if the constants does not exist, stop right now
      if( ! defined( $mode ) ) return false; // invalid string
      
      // if the constant exists, use it
      $mode = constant( $mode );
    }
    
    $this->setModes( $this->_modes | $mode );
  }
  
  /**
   * Alias for setMode()
   *
   * @param mixed $mode
   * @return void
   * @access public
   */
  public function enable( $mode )
  {
    $this->setMode( $mode );
  }
  
  /**
   * Unset bitwise flag for a mode
   * 
   * Mode can be a string that will lookup on self::MODE_* constants.
   *
   * @param mixed $mode
   * @return void
   * @access public
   */
  public function unsetMode( $mode )
  {
    // string lookup
    if( is_string( $mode ) )
    {
      $mode = 'self::MODE_'.strtoupper( $mode );
      
      // if the constants does not exist, stop right now
      if( ! defined( $mode ) ) return false; // invalid string
      
      // if the constant exists, use it
      $mode = constant( $mode );
    }
    
    $this->setModes( $this->_modes & ~$mode );
  }
  
  /**
   * Alias for unsetMode()
   *
   * @param mixed $mode
   * @return void
   * @access public
   */
  public function disable( $mode )
  {
    $this->unsetMode( $mode );
  }
  
  /**
   * Get boolean value for a mode flag
   *
   * @param integer $mode
   * @return integer
   * @access public
   */
  public function getMode( $mode )
  {
    return $this->_modes & $mode;
  }
  
  /**
   * Set bitwise flags for modes
   *
   * @param integer $mode
   * @return boolean
   * @access public
   */
  public function setModes( $mode )
  {
    $this->_modes = $mode | 0; // force integer
    
    if( $this->__rsc === null ) return false;
    
    // apply modes
    // we never know future of these function, so force boolean
    imagealphablending( $this->__rsc , (bool) ( $this->_modes & self::MODE_ALPHABLENDING ) );
    imagesavealpha( $this->__rsc , (bool) ( $this->_modes & self::MODE_SAVEALPHA ) );
    imageantialias( $this->__rsc , (bool) ( $this->_modes & self::MODE_ANTIALIAS ) );
    imageinterlace( $this->__rsc , (bool) ( $this->_modes & self::MODE_INTERLACE ) );
    
    return true;
  }
  
  /**
   * Get bitwise flags for modes
   *
   * @param none
   * @return integer
   * @access public
   */
  public function getModes()
  {
    return $this->_modes;
  }
  
  // Interpolation
  
  /**
   * Current interpolation.
   *
   * @var integer
   * @access protected
   * @see http://php.net/manual/en/function.imagesetinterpolation.php
   */
  public $_interpolation = 3; // 3 is the value for \IMG_BILINEAR_FIXED
  
  /**
   * Set the interpolation method.
   * Interpolation is available since PHP 5.5.0
   * 
   * Method can be a string that will lookup on IMG_* constants.
   *
   * @param mixed $method One of IMG_* constants
   * @return boolean
   * @access public
   * @see http://php.net/manual/en/function.imagesetinterpolation.php
   */
  public function setInterpolation( $method )
  {
    // string lookup
    if( is_string( $method ) )
    {
      $method = '\\IMG_'.strtoupper( $method );
      
      // if the constants does not exist, stop right now
      if( ! defined( $method ) ) return false; // invalid string
      
      // if the constant exists, use it
      $method = constant( $method );
    }
    
    // assign interpolation method
    $this->_interpolation = $method | 0; // force integer
    
    // no resource, cannot apply
    if( $this->__rsc === null ) return false;
    
    // roger roll
    return imagesetinterpolation( $this->__rsc , $this->_interpolation );
  }
  
  /**
   * Get the interpolation method.
   *
   * @param none
   * @return integer
   * @access public
   * @see http://php.net/manual/en/function.imagesetinterpolation.php
   */
  public function getInterpolation()
  {
    return $this->_interpolation;
  }
}

/**
 * fix issue #2, thanks to @flashek
 * 
 * imageantialias() may not exists on all GD installation
 * @see http://stackoverflow.com/questions/5756144/imageantialias-call-to-undefined-function-error-with-gd-installed
 * 
 * Because we never use qualified namespace for global PHP functions, during 
 * execution, PHP looks on \GDImage namespace THEN on global namespace.
 * @see http://php.net/manual/en/language.namespaces.rules.php
 * 
 * That behavior gives us ability to create the function in \GDImage namespace 
 * only if it is not available on global namespace and unqualified calls to 
 * imageantialias() will be ok over entire \GDImage namespace.
 */
if( ! ( function_exists( '\\imageantialias' ) || function_exists( __NAMESPACE__.'\\imageantialias' ) ) )
{
  // create dummy function 
  // @see http://php.net/manual/en/function.imageantialias.php
  function imageantialias( $image , $enabled ){ return false; }
}

/**
 * imagesetinterpolation() may not exists on PHP version lower than 5.5.0
 * @see http://php.net/manual/en/function.imagesetinterpolation.php
 * 
 * But we can make a imagesetinterpolation() function available over entire 
 * GD namespace.
 */
if( ! ( function_exists( '\\imagesetinterpolation' ) || function_exists( __NAMESPACE__.'\\imagesetinterpolation' ) ) )
{
  // create dummy function 
  // 3 is the value for \IMG_BILINEAR_FIXED
  // @see http://php.net/manual/en/function.imagesetinterpolation.php
  function imagesetinterpolation( $image , $method = 3 ){ return false; }
}


//function my_debug_backtrace()
//{
//  $trace = debug_backtrace();
//  for( $i = 1, $imax = min( 11 , count($trace) ); $i < $imax; $i++ )
//  {
//    print '<pre>';
//    isset($trace[$i]['class']) && isset($trace[$i]['function']) && print $trace[$i]['class'].'::'.$trace[$i]['function'].'()'."\n";
//    empty($trace[$i]['class']) && isset($trace[$i]['function']) && print $trace[$i]['function'].'()'."\n";
//    print $trace[$i]['file']. ' at '.$trace[$i]['line']."\n";
//    print '</pre>';
//  }
//}