<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Class to register autoloading function for GDImage stuff.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Autoloader
{
  /**
   * Current \GDImage\Autoloader instance.
   *
   * @var \GDImage\Autoloader Current instance
   * @access protected
   * @static
   */
  public static $_instance;
  
  /**
   * Root directory.
   *
   * @var string
   * @access protected
   */
  public $_root;
  
  /**
   * Method to retrieve \GDImage\Autoloader instance.
   *
   * @param none
   * @return \GDImage\Autoloader Current instance
   * @access public
   * @static
   */ 
  public static function getInstance()
  {
  	if( ! static::$_instance )
  	{
  		static::$_instance = new static();
  	}
  	return static::$_instance;
  }
  
  /**
   * \GDImage\Autoloader constructor.
   * Protected constructor to force use of getInstance() static method.
   *
   * @param string $dir Base directory
   * @return void
   * @access protected
   */
  public function __construct()
  {
  	$this->_root = dirname( dirname( __FILE__ ) );
  }
  
  /**
   * Register autoloading.
   *
   * @param none
   * @return void
   * @access public
   */
  public function register()
  {
  	spl_autoload_register( array( $this , '_autoload' ) );
  }
  
  /**
   * Method to autoload class.
   *
   * @param string $class Class name
   * @return void
   * @access protected
   */
  public function _autoload( $class )
  {
    if( strpos( $class , 'GDImage\\' ) === 0 && is_file( $file = $this->_root.DIRECTORY_SEPARATOR.str_replace( array( '\\' , '_' ) , DIRECTORY_SEPARATOR , $class ).'.php' ) )
    {
      require $file;
    }
  }

}