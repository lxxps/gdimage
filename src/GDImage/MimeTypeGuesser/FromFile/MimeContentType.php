<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Guess MIME Type from file helping mime_content_type().
 * 
 * Note that mime_content_type() is deprecated so this guesser should be 
 * used as last solution.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage mimetypeguesser
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements MimeTypeGuesser_Interface
 */
class MimeTypeGuesser_FromFile_MimeContentType implements MimeTypeGuesser_Interface
{
  
  /**
   * Invoke \GDImage\MimeTypeGuesser_Interface on something.
   * 
   * In that case consider stuff as file path.
   *
   * @param string $stuff
   * @return string MIME Type or false on failure
   * @access public
   */
  public function __invoke( $stuff )
  {
    return mime_content_type( $stuff );
  }
  
}

/**
 * Make sure mime_content_type() is available in current namespace.
 * This function is deprecated and may disappear in future.
 */
if( ! ( function_exists( '\\mime_content_type' ) || function_exists( __NAMESPACE__.'\\mime_content_type' ) ) )
{
  // create dummy function 
  // @see http://php.net/manual/en/function.mime-content-type.php
  function mime_content_type( $filename ){ return false; }
}
