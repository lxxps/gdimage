<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Guess MIME Type from file helping finfo.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage mimetypeguesser
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements MimeTypeGuesser_Interface
 */
class MimeTypeGuesser_FromFile_Finfo implements MimeTypeGuesser_Interface
{

  /**
   * Invoke \GDImage\MimeTypeGuesser_Interface on something.
   * 
   * In that case consider stuff as file path.
   *
   * @param string $stuff
   * @return string MIME Type or false on failure
   * @access public
   */
  public function __invoke( $stuff )
  {
    // no, we do not want notice
    if( ! ( $finfo = @ finfo_open( FILEINFO_MIME ) ) )
    {
      return false;
    }

    // no, we do not want notice
    $type = @ finfo_file( $finfo , $stuff );
    
    // free resource
    finfo_close( $finfo );

    // remove charset
    if( ( $pos = strpos($type,';') ) !== false )
    {
      $type = substr( $type , 0 , $pos );
    }

    return $type;
  }
  
}

/**
 * Make sure finfo_open() is available in current namespace.
 * Fileinfo is not part of default PHP installation.
 */
if( ! ( function_exists( '\\finfo_open' ) || function_exists( __NAMESPACE__.'\\finfo_open' ) ) )
{
  // create dummy function 
  // 0 is probably the value for \FILEINFO_NONE
  // @see http://php.net/manual/en/function.finfo-open.php
  function finfo_open( $options = 0 , $magic_file = null ){ return false; }
}
