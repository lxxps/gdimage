<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration setting "mime_type_guesser_mime_types_file"
 * 
 * This file should looks like Apache mime.types file.
 */
if( ! Config::hasMimeTypeGuesserMimeTypesFile() ) {
  Config::setMimeTypeGuesserMimeTypesFile( dirname(__FILE__).'/data/apache/mime.types'  );
}

/**
 * Guess MIME Type from file path helping extension.
 * 
 * If you have more extensions to check, just call 
 * MimeTypeGuesser_FromFile_Extension::setup() static method with a path to your
 * own mime.types file.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage mimetypeguesser
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements MimeTypeGuesser_Interface
 */
class MimeTypeGuesser_FromFile_Extension implements MimeTypeGuesser_Interface
{
  
  /**
   * Array of extensions by MIME Type.
   * Used to determine MIME Type by extension when there is no 
   * other choice.
   *
   * @var array
   * @access protected
   * @static
   */
  public static $_mimetype_to_extension;
	
  /**
   * Array of MIME Type by extensions.
   * Used to determine extension by MIME Type when there is no 
   * other choice.
   *
   * @var array
   * @access protected
   * @static
   */
  public static $_extension_to_mimetype;
  
  /**
   * Load MIME Type to extension array and extension to MIME Type array 
   * from a file.
   * The file must looks like mime.types file provided by Apache.
   * 
   * Note that arrays are reset before to perform this operation.
   * 
   * @param [string] $filename
   * @return void
   * @access public
   * @static
   */
  public static function setup( $filename = null )
  {
    // load mime/types stuff
    ( $filename === null ) && $filename = Config::getMimeTypeGuesserMimeTypesFile();
    
    static::$_mimetype_to_extension = array();
    static::$_extension_to_mimetype = array();
    $lines = file( $filename );
    $i = count( $lines );
    while( $i-- )
    {
      // we are just interested by image/ MIME Type, not?
      if( strpos( $lines[$i] , 'image/' ) !== 0 ) continue;
      
      $data = preg_split( '~\\s+~u' , trim( $lines[$i] ) );
      
      // first index is the mime type
      $mime_type = array_shift( $data );
      
      // this array is populated once
      static::$_mimetype_to_extension[$mime_type] = $data[0];
      
      // this array is populated for each extensions
      $j = count( $data );
      while( $j-- )
      {
        static::$_extension_to_mimetype[$data[$j]] = $mime_type;
      }
    }
  }
  
  
  /**
   * \GDImage\Factory constructor.
   * 
   * @param none
   * @return void
   * @access public
   */
  public function __construct()
  {
    // look for initialization
    if( static::$_mimetype_to_extension === null && static::$_extension_to_mimetype === null )
    {
      static::setup();
    }
  }

  /**
   * Invoke \GDImage\MimeTypeGuesser_Interface on something.
   * 
   * In that case consider stuff as file path.
   *
   * @param string $stuff
   * @return string MIME Type or false on failure
   * @access public
   */
  public function __invoke( $stuff )
  {
    // if it is only a .extension, remove the dot
    if( strpos( '.' , $stuff ) === 0 ) $stuff = substr( $stuff , 1 );
    // look likes a file path
    if( strpos( '.' , $stuff ) ) $stuff = pathinfo( $stuff , \PATHINFO_EXTENSION );
    
    
    if( isset(static::$_extension_to_mimetype[$stuff]) )
    {
      return static::$_extension_to_mimetype[$stuff];
    }
    
    return false;
  }

  /**
   * Reverse look up to return an extension from a MIME Type.
   * This is the only guesser that has this method and it is used for 
   * \GDImage\Factory_ExportDriver_File.
   *
   * @param string $mime
   * @return string Extension or false on failure
   * @access public
   */
  public function reverse( $mime )
  {
    if( isset(static::$_mimetype_to_extension[$mime]) )
    {
      return static::$_mimetype_to_extension[$mime];
    }
    
    return false;
  }
  
}
