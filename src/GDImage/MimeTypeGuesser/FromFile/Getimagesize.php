<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Guess MIME Type from file helping getimagesize().
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage mimetypeguesser
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements MimeTypeGuesser_Interface
 */
class MimeTypeGuesser_FromFile_Getimagesize implements MimeTypeGuesser_Interface
{
  
  /**
   * Invoke \GDImage\MimeTypeGuesser_Interface on something.
   * 
   * In that case consider stuff as file path.
   *
   * @param string $stuff
   * @return string MIME Type or false on failure
   * @access public
   */
  public function __invoke( $stuff )
  {
    // no, we do not want notice
    if( $data = @ getimagesize( $stuff ) )
    {
      return $data['mime'];
    }

    return false;
  }
  
}
