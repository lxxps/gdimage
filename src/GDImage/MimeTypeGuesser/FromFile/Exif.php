<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Guess MIME Type from file helping exif_imagetype().
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage mimetypeguesser
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements MimeTypeGuesser_Interface
 */
class MimeTypeGuesser_FromFile_Exif implements MimeTypeGuesser_Interface
{
  
  /**
   * Invoke \GDImage\MimeTypeGuesser_Interface on something.
   * 
   * In that case consider stuff as file path.
   *
   * @param string $stuff
   * @return string MIME Type or false on failure
   * @access public
   */
  public function __invoke( $stuff )
  {
    // no, we do not want notice
    if( $type = @ exif_imagetype( $stuff ) )
    {
      return image_type_to_mime_type( $type );
    }

    return false;
  }
  
}

/**
 * Make sure exif_imagetype() is available in current namespace.
 * Exif is not part of default PHP installation.
 */
if( ! ( function_exists( '\\exif_imagetype' ) || function_exists( __NAMESPACE__.'\\exif_imagetype' ) ) )
{
  // create dummy function 
  // @see http://php.net/manual/en/function.exif-imagetype.php
  function exif_imagetype( $filename ){ return false; }
}
