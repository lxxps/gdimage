<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Initalize configuration setting "mime_type_guesser_bytes_table"
 */
if( ! Config::hasMimeTypeGuesserBytesTable() ) {
  Config::setMimeTypeGuesserBytesTable( array(
    "\xFF\xD8\xFF" => 'image/jpeg' , 
    "\x89\x50\x4E\x47\x0D\x0A\x1A\x0A" => 'image/png' , 
    'GIF89a' => 'image/gif' ,
    'GIF87a' => 'image/gif' ,
    'BM' => 'image/bmp' , 
    '8BPS' => 'image/vnd.adobe.photoshop' , 
    'FWS' => 'application/x-shockwave-flash' , 
    'II' => 'image/tiff' ,
    'MM' => 'image/tiff' ,
  ) );
}

/**
 * Guess MIME Type from binary data helping bytes.
 * 
 * If you have more bytes to check, just call 
 * MimeTypeGuesser_FromBinary_Bytes::setup() static method with your own table.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage mimetypeguesser
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements MimeTypeGuesser_Interface
 */
class MimeTypeGuesser_FromBinary_Bytes implements MimeTypeGuesser_Interface
{
  
  /**
   * Array of detection bytes to MIME Type.
   * Used to determine MIME Type by binary data.
   *
   * @var array
   * @access protected
   * @static
   */
  public static $_bytes_to_mimetype;
  
  /**
   * Load bytes array to MIME Type from an array of bytes in hexadecimal 
   * representation to MIME Type.
   * 
   * Note that array is reset before to perform this operation.
   * 
   * @param [array] $table
   * @return void
   * @access public
   */
  public function setup( array $table = null )
  {
    // load mime/types stuff
    ( $table === null ) && $table = Config::getMimeTypeGuesserBytesTable();
    
    static::$_bytes_to_mimetype = $table;
  }
  
  
  /**
   * \GDImage\Factory constructor.
   * 
   * @param none
   * @return void
   * @access public
   */
  public function __construct()
  {
    // look for initialization
    if( static::$_bytes_to_mimetype === null )
    {
      static::setup();
    }
  }

  /**
   * Invoke \GDImage\MimeTypeGuesser_Interface on something.
   * 
   * In that case consider stuff as binary data.
   *
   * @param string $stuff
   * @return string MIME Type or false on failure
   * @access public
   */
  public function __invoke( $stuff )
  {
    $mime = false;
    
    // look only on little portion of the binary file
    // get 16 bytes even if the all binary headers are smaller
    // we cannot predict future image type
    $stuff = substr( $stuff , 0 , 16 );
      
    // to be safe
    reset( static::$_bytes_to_mimetype );
    while( list( $bytes , $mime ) = each( static::$_bytes_to_mimetype ) )
    {
      // if detected, stop right now
      if( strpos( $stuff , $bytes ) === 0 ) break;
      // unset mime
      $mime = false;
    }
    // for the next developer that will use this array
    reset( static::$_bytes_to_mimetype );
    
    return $mime;
  }
  
}
