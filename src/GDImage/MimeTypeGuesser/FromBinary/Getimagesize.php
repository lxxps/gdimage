<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Guess MIME Type from binary data helping getimagesizefromstring().
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage mimetypeguesser
 * @author     Loops <pierrotevrard@gmail.com>
 * @implements MimeTypeGuesser_Interface
 */
class MimeTypeGuesser_FromBinary_Getimagesize implements MimeTypeGuesser_Interface
{
  
  /**
   * Invoke \GDImage\MimeTypeGuesser_Interface on something.
   * 
   * In that case consider stuff as binary data.
   *
   * @param string $stuff
   * @return string MIME Type or false on failure
   * @access public
   */
  public function __invoke( $stuff )
  {
    // no, we do not want notice
    if( $data = @ getimagesizefromstring( $stuff ) )
    {
      return $data['mime'];
    }

    return false;
  }
  
}

/**
 * Make sure getimagesizefromstring() is available in current namespace.
 * getimagesizefromstring() is available since PHP 5.4.0.
 */
if( ! ( function_exists( '\\getimagesizefromstring' ) || function_exists( __NAMESPACE__.'\\getimagesizefromstring' ) ) )
{
  // for this one, a fallback to getimagesize() is feasible (and easy)
  // @see http://php.net/manual/en/function.getimagesizefromstring.php
  function getimagesizefromstring( $imagedata , &$imageinfo = array() )
  {
    // create temporary file
    $tmp = tempnam( Config::getTemporaryDirectory() , Config::getTemporaryPrefix() );
    
    if( file_put_contents( $tmp , $imagedata ) !== false )
    {
      // fallback to getimagesize()
      $ret = getimagesize( $tmp , $imageinfo );
      
      // destroy temporary file
      unlink( $tmp );
      
      // return result
      return $ret;
    }
      
    // destroy temporary file
    unlink( $tmp );
    
    // fails to put contents
    return false;
  }
}
