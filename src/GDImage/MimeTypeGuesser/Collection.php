<?php

/*
 * This file is part of the GDImage package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GDImage;

/**
 * Collection of MIME Type guesser
 * 
 * For now there is no getters/setters for guessers lists because it seems 
 * useless. These properties are just array of MimeTypeGuesser_Interface class 
 * names, nothing else.
 * 
 * Whatever, if you want to modify guessers, there is two ways:
 * - The POOP way, quick and dirty: helping POOP pattern, you can directly 
 *   access to MimeTypeGuesser_Collection::getInstance()->_file_guessers 
 *   and/or MimeTypeGuesser_Collection::getInstance()->_binary_guessers in 
 *   order to modify them.
 * - The clean way, long and boring: you can extend this class and overwrote 
 *   $this->_file_guessers and/or $this->_binary_guessers properties. After 
 *   that, just call My_MimeTypeGuesser_Collection::getInstance() static 
 *   method once and the job will be done.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    GDImage
 * @subpackage mimetypeguesser
 * @author     Loops <pierrotevrard@gmail.com>
 */
class MimeTypeGuesser_Collection
{
  
  /**
   * Current \GDImage\MimeTypeGuesser_Collection instance.
   *
   * @var \GDImage\MimeTypeGuesser_Collection Current instance
   * @access protected
   */
  public static $_instance;
  
  /**
   * Method to retrieve \GDImage\MimeTypeGuesser_Collection instance.
   *
   * @param none
   * @return \GDImage\MimeTypeGuesser_Collection
   * @access public
   * @static
   */ 
  public static function getInstance()
  {
    if( ! static::$_instance )
    {
        static::$_instance = new static();
    }
    return static::$_instance;
  }
  
  /**
   * Method to retrieve MIME Type from a file path.
   *
   * @param string $filepath
   * @return string MIME Type or false
   * @access public
   * @static
   */ 
  public static function fromFile( $filepath )
  {
    return static::getInstance()->guessFromFile( $filepath );
  }
  
  /**
   * Method to retrieve MIME Type from binary data.
   *
   * @param string $binary
   * @return string MIME Type or false
   * @access public
   * @static
   */ 
  public static function fromBinary( $binary )
  {
    return static::getInstance()->guessFromBinary( $binary );
  }
  
  /**
   * File guessers list.
   * Array of MimeTypeGuesser_Interface.
   * Order is important.
   * 
   * @var array
   * @protected
   */
  public $_file_guessers = array(
    '\\GDImage\\MimeTypeGuesser_FromFile_Finfo' ,
    '\\GDImage\\MimeTypeGuesser_FromFile_Exif' ,
    '\\GDImage\\MimeTypeGuesser_FromFile_Getimagesize' ,
    '\\GDImage\\MimeTypeGuesser_FromFile_MimeContentType' ,
    '\\GDImage\\MimeTypeGuesser_FromFile_Extension' ,
  );

  /**
   * Get guesser classes to use.
   * 
   * @param none 
   * @return array
   * @access public
   */
  public function getFileGuessers()
  {
    return $this->_file_guessers;
  }
  
  /**
   * Binary guessers list.
   * Array of MimeTypeGuesser_Interface.
   * Order is important.
   * 
   * @var array
   * @protected
   */
  public $_binary_guessers = array(
    '\\GDImage\\MimeTypeGuesser_FromBinary_Finfo' ,
    '\\GDImage\\MimeTypeGuesser_FromBinary_Getimagesize' ,
    '\\GDImage\\MimeTypeGuesser_FromBinary_Bytes' ,
  );

  /**
   * Get guesser classes to use.
   * Order is important.
   * All of these classes should implements MimeTypeGuesser_Interface.
   * 
   * @param none 
   * @return array
   * @access protected
   */
  public function getBinaryGuessers()
  {
    return $this->_binary_guessers;
  }

  /**
   * Guess MIME type of a file
   * 
   * @param string $filepath
   * @return string MIME Type or false
   * @access public
   */
  public function guessFromFile( $filepath )
{
    // Check file exists
    // We cannot use is_file() or something like that because
    // some wrappers does not support the stat() family
    // @see http://www.php.net/manual/en/wrappers.http.php
    // So, use fopen() with read flag
    if( false === ( $h = @ fopen( $filepath , 'rb' ) ) )
    {
      // File does not exists, provide a warning and return false
      trigger_error( sprintf( 'File "%s" does not exist, unable to determine MIME Type' , $filepath ) , E_USER_WARNING );
      return false;
    }
    // close handle right now
    fclose( $h );
    
    $guessers = $this->getFileGuessers();
      
    for( $i = 0, $imax = count( $guessers ); $i < $imax; $i++ )
    {
      // construct guesser
      $guesser = new $guessers[$i]();
      
      // invoke it
      if( $mime = $guesser( $filepath ) )
      {
        // we can stop right now
        return $mime;
      }
    }
    
    return false;
  }
  

  /**
   * Guess MIME type of binary data
   * 
   * @param string $binary
   * @return string MIME Type or false
   * @access public
   */
  public function guessFromBinary( $binary )
  {
    $guessers = $this->getBinaryGuessers();
      
    for( $i = 0, $imax = count( $guessers ); $i < $imax; $i++ )
    {
      // construct guesser
      $guesser = new $guessers[$i]();
      
      // invoke it
      if( $mime = $guesser( $binary ) )
      {
        // we can stop right now
        return $mime;
      }
    }
    
    return false;
  }
}